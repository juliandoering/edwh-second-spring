package net.nkg.nitro.edwh.model.common;

import java.time.LocalDate;

public interface ValidityPeriod {

    LocalDate getValidityPeriodFrom();

    LocalDate getValidityPeriodTo();

}
