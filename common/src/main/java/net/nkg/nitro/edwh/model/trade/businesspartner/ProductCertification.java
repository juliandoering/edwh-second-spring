package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.common.ValidityPeriod;
import net.nkg.nitro.edwh.model.quality.ProductCertificationProgram;

public interface ProductCertification extends BaseModel, ValidityPeriod {

    ProductCertificationProgram getCertification();

    void setCertification(ProductCertificationProgram certification);

    String getThirdPartyIdNumber();

    void setThirdPartyIdNumber(String idNumber);

    LocalDate getValidityPeriodFrom();

    void setValidityPeriodFrom(LocalDate date);

    LocalDate getValidityPeriodTo();

    void setValidityPeriodTo(LocalDate date);
}
