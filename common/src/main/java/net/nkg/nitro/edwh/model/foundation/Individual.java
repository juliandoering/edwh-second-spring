package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.BaseModel;

import java.time.LocalDate;
import java.util.List;

/**
 * An individual person to be used within the EDWH.
 */
public interface Individual extends BaseModel {

    /**
     * @return the addressing form, i.e. Mr or Mrs
     */
    String getAddressingForm();

    void setAddressingForm(String addressingForm);

    /**
     * @return the AcademicTitlet, i.e. Dr
     */
    String getAcademicTitle();

    void setAcademicTitle(String academicTitle);

    /**
     * @return the last name, the family name
     */
    String getLastName();

    void setLastName(String lastName);

    /**
     * @return the first name, the given name
     */
    String getFirstName();

    void setFirstName(String firstName);

    /**
     * @return an identification counter to make the business key unique.
     */
    Integer getIdentificationCounter();

    void setIdentificationCounter(Integer identificationCounter);

    /**
     * @return the gender of the individual
     */
    Gender getGender();

    void setGender(Gender gender);

    LocalDate getDateOfBirth();

    void setDateOfBirth(LocalDate dateOfBirth);

    Location getPlaceOfBirth();

    void setPlaceOfBirth(Location placeOfBirth);

    /**
     * @return the citizenship of the individual. A location object with only country code and no location code.
     */
    Location getCitizenship();

    void setCitizenship(Location country);

    /**
     * @return the postal address
     */
    PostalAddress getPostalAddress();

    void setPostalAddress(PostalAddress address);

    /**
     * @return the personal identification number
     */
    String getPersonalIdNumber();

    void setPersonalIdNumber(String personalIdNumber);

    /**
     * @return the tax identification number
     */
    String getTaxNumber();

    void setTaxNumber(String taxIdNumber);

    /**
     * @return a list of assigned contacts, i.e. phone numbers, email addresses, etc
     */
    List<Contact> getContacts();

    void setContacts(List<Contact> contacts);

    /**
     * @return the type how the individual has been identified.
     */
    IdentificationType getIdentificationType();

    void setIdentificationType(IdentificationType identificationType);

    /**
     * @return a comment for a better identification for individuals with same or similar names etc.
     */
    String getComment();

    void setComment(String comment);

    /**
     * @return If the individual is pending. Is always pending, if the individual wasn't created by an EDWH-Administrator and can only be changed by an EDWH-Administrator.
     */
    boolean isPending();

    void setPending(boolean pending);

}
