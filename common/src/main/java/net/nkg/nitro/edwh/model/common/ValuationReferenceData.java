package net.nkg.nitro.edwh.model.common;

import java.time.LocalDate;

/**
 * This the basic model interface for valuation reference data,
 * this data should be maintained directly in EDWH but not as master data.
 *
 * These Data are for reports only.
 * 
 * @author walther
 */
public interface ValuationReferenceData extends BaseModel {
    
    /**
     * Return the business validity from date (valid to is implicit by the next valid from date of the same business key).
     * @return
     */
    LocalDate getValidityFrom();
    
    /**
     * Set the business validity valid from (valid to is implicit by the next valid from date of the same business key).
     * @param validityFrom business validity from date
     * @return
     */
    void setValidityFrom(LocalDate validityFrom);
    
    /**
     * Return whether the object is locked (not editable) or not (editable).
     * @return
     */
    boolean isLocked();
    
    /**
     * Set whether the object is locked (not editable) or not (editable).
     * @return
     */
    void setLocked(boolean locked);

    /**
    * Return the version of the Object or <b>null</b> if the Object is not versioned.
    * @return
    */
    Long getVrdVersion();

}
