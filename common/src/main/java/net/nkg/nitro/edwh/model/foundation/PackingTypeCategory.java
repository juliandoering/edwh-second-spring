package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.ReferenceData;

public interface PackingTypeCategory extends ReferenceData {

    /**
     * Returns a comment what the category is including.
     * @return
     */
    String getComment();

    void setComment(String comment);
}
