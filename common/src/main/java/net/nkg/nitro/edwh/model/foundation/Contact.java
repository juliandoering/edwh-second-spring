package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Provice contact information and contact type.
 * <p>
 * E.g. 
 * <ul>
 * <li>Contact information: nf@hbt.de</li>
 * <li>Contact type: email</li>
 * </ul>
 * @author nf
 *
 */
public interface Contact extends BaseModel {

    String getValue();

    void setValue(String info);

    Type getType();

    void setType(Type type);

    public enum Type {

        EMAIL("EMAIL"),
        TELEPHONE("TELEPHONE"),
        MOBILE("MOBILE"),
        FAX("FAX"),
        WEBSITE("WEBSITE");

        private final String contactTypeString;

        private Type(final String text) {
            this.contactTypeString = text;
        }

        /**
         * Returns the enum type to a string or null if the string does not
         * match any known enum type. The method is case insensitive
         * 
         * @param contactTypeString
         * @return
         */
        public static Type getType(String contactTypeString) {

            if (contactTypeString != null) {
                // check all possible enum values if they match
                for (Type contactTypeEnumVal : Type.values()) {
                    // test if the enum-value matches our given string
                    if (contactTypeString.equalsIgnoreCase(contactTypeEnumVal.contactTypeString)) {
                        // we found the right enum-value
                        return contactTypeEnumVal;
                    }
                }
            }
            return null;
        }
    }

}
