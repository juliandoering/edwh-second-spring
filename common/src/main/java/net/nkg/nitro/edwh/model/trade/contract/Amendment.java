package net.nkg.nitro.edwh.model.trade.contract;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This sections allows the user to record the amendments they are making in structured detail. Here the user will record
 * the date of the amendment, the field they amended and the version of the contract they amended.
 */
public interface Amendment extends BaseModel {

    /**
     * The date upon which a document is considered as being changed.<p/>
     * Example: For an amendment to a Documentary Credit, this is the date upon which the Issuing Bank considers the credit
     * as being amended.
     */
    LocalDate getDate();

    void setDate(LocalDate date);

    /**
     * The user enters the name of the field that they have amended.
     */
    String getField();

    void setField(String field);

    /**
     * This 'Version Number' allows a user who is not using the Bolero system to indicate a version of this document, over and above the Bolero standard fields.
     */
    String getVersionNumber();

    void setVersionNumber(String versionNumber);
}
