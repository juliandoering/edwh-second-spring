package net.nkg.nitro.edwh.model.logistics;

import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyGroup;

public interface CarrierTradeRoute extends BaseModel {

    List<OceanFreightRate> getOceanFreightRates();

    void setOceanFreightRates(List<OceanFreightRate> oceanFreightRates);

    /**
     * @return the frequency of the provided route
     */
    Integer getFrequency();

    void setFrequency(Integer frequency);

    /**
     * @return the transshipment if present
     */
    String getTransShipment();

    void setTransShipment(String transShipment);

    /**
     * @return the starting date of the validity period
     */
    LocalDate getValidFrom();

    void setValidFrom(LocalDate validFrom);

    /**
     * @return the end date of the validity period
     */
    LocalDate getValidTo();

    void setValidTo(LocalDate validTo);

    /**
     * @return the trade name
     */
    TradeName getTradeName();

    void setTradeName(TradeName tradeName);

    /**
     * @return the BTP group name of the provider
     */
    BusinessThirdPartyGroup getProvider();

    void setProvider(BusinessThirdPartyGroup provider);

    Location getPortOfLoading();

    void setPortOfLoading(Location portOfLoading);

    Location getPortOfDischarge();

    void setPortOfDischarge(Location portOfDischarge);
}
