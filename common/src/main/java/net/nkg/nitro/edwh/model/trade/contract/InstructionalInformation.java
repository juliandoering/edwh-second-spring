package net.nkg.nitro.edwh.model.trade.contract;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.quality.SampleHandling;
import net.nkg.nitro.edwh.model.trade.FixationOption;
import net.nkg.nitro.edwh.model.trade.PositionOfSale;

/**
 * This contains the instructional information for the coffee contract, such as the duties, responsibilities and parameters for
 * the variables such as price.
 */
public interface InstructionalInformation extends BaseModel {

    /**
     * In the event of a shipper being identified in the document, either the buyer or the seller must be liable for the
     * commission due to the shipper.
     */
    ContractRoleEnum getContractRole();

    void setContractRole(ContractRoleEnum contractRole);

    /**
     * The Futures ID describes the market, month and year in which the commodity is destined for sale. This is mandatory if the price
     * has not been entered.
     */
    FuturesId getFuturesId();

    void setFuturesId(FuturesId futuresId);

    /**
     * This is a float representing the number of lots which will be applied to in this case.<p/>
     * In this case of the Contract this will be the number of lots which will be fixed by the contract.<p/>
     * In the case of the Price Fix Rolling Letter, this will be the number of lots being 'rolled' from one Future ID to another.<p/>
     * In case of a sub lot PTBF contract real numbers 0 < number of lots < 1 are allowed.<p/>
     * Recommended Format: ---,---,---,---,--9 or 0.4
     */
    BigDecimal getNumberOfLots();

    void setNumberOfLots(BigDecimal numberOfLots);

    /**
     * The differential describes the price change in effect from the price indicated on a document and may be a positive or
     * negative decimal, and is measured in the same currency as the price.<p/>
     * In the Contract this is mandatory if the price has not been entered above.<p/>
     * In the case of the Price Fix Rolling Letter it represents the number applied to the future average to determine outright price and may differ from the contract differential.<p/>
     * Recommended Format: ---,---,---,---,--9.999999
     */
    BigDecimal getDifferential();

    void setDifferential(BigDecimal differential);

    /**
     * The party whose is responsible to fix the contract must be identified and is mandatory if the price has not been entered
     * on the contract itself, in this case it is either Buyer or Seller with or without margin payable.
     */
    FixationOption getFixationOption();

    void setFixationOption(FixationOption fixationOption);

    /**
     * The contract may not be fixed before this date, i.e. the price may not be set.
     */
    LocalDate getEarliestFixationDate();

    void setEarliestFixationDate(LocalDate earliestFixationDate);

    /**
     * The contract must be fixed by this date, i.e. the price must be set. This is mandatory if the user has not entered a price above.
     */
    LocalDate getLatestFixationDate();

    void setLatestFixationDate(LocalDate latestFixationDate);

    /**
     * This describes both the responsibility and method used to determine the weight of the coffee.
     */
    CoffeeWeights getCoffeeWeights();

    void setCoffeeWeights(CoffeeWeights coffeeWeights);

    /**
     * This describes the period in which the coffee will be moved or delivered, by way of start and end date.
     */
    MoveOrDeliverPeriod getMoveOrDeliverPeriod();

    void setMoveOrDeliverPeriod(MoveOrDeliverPeriod moveOrDeliverPeriod);

    /**
     * Position defines the conditions under which the commodity is actually sold.
     */
    PositionOfSale getPositionOfSale();

    void setPositionOfSale(PositionOfSale positionOfSale);

    /**
     * The Arbitration party will be indicated here, the user selecting from the following code list:
     * 'GCA' - Green Coffee Association
     * 'ECC' - European Contract for Coffee
     * 'PCCA' - Pacific Coast Coffee Association
     * 'SCAA' - Specialty Coffee Association
     */
    String getContractArbitration();

    void setContractArbitration(String contractArbitration);

    /**
     * The 'Location' section is used to identify a location (code or name) as having a certain role within the document. For
     * example, 'New York' could be selected and qualfied as being the 'Place of Delivery'.
     * A geographical area or a place. It must be clearly identifiable since no street address is given. For example, a town, a
     * city, a code for an airport, or a place. It is represented by a name and a location code defined by Bolero.
     */
    Location getLocation();

    void setLocation(Location location);

    /**
     * These approval terms from an industry standard list from which the user can select the approval terms that apply in this contract.
     */
    SampleHandling getApprovalTerms();

    void setApprovalTerms(SampleHandling approvalTerms);

    /**
     * Should the user have selected 'Other' in the Approval terms code list, they may choose to enter the specific Approval terms here.
     * <p/>
     * A string or line of text containing alphanumeric characters.<p/>
     * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
     */
    List<OtherApprovalTerms> getOtherApprovalTerms();

    void setOtherApprovalTerms(List<OtherApprovalTerms> otherApprovalTerms);
}
