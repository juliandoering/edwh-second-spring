/**
 * This package holds the interfaces of the business objects.
 * The interfaces are used to ensure that all objects representing the underlying objects.
 * This is especially used to shadow the "Model" of activeJdbc from the (base)Service layer.
 * For the Rest layer marshalling is needed as jackson2 otherwise uses all getters of the models, etc.
 * as reflection is used there.
 */
package net.nkg.nitro.edwh.model;
