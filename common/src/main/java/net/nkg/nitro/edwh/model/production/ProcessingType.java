package net.nkg.nitro.edwh.model.production;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Type of how coffee can be processed.
 */
public interface ProcessingType extends ReferenceData {

    /**
     * Special ProcessingType's required for validation
     * @author sl
     *
     */
    public enum ProcessingTypeEnum {
        TRANSFER("TRANSFER");

        private final String type;

        private ProcessingTypeEnum(String type) {
            this.type = type;
        }

        /**
         * Find the correct enum value for a String.
         * @param name the String value of the enum value.
         * @return the instance of ProcessingTypeEnum that matches the name, or null if name is unknown.
         */
        public static ProcessingTypeEnum getProcessingTypeEnumValue(String name) {
            if (name != null) {
                // check all possible enum values if they match
                for (ProcessingTypeEnum processingTypeEnumVal : ProcessingTypeEnum.values()) {
                    // test if the enum-value matches our given string
                    if (name.equalsIgnoreCase(processingTypeEnumVal.type)) {
                        // we found the right enum-value
                        return processingTypeEnumVal;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return type;
        }
    }
}
