package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.logistics.CoffeeWeightsCode;

/**
 * This describes both the responsibility and method used to determine the weight of the coffee.
 */
public interface CoffeeWeights extends BaseModel {

    /**
     * Responsibility and weighing describes who was responsible for carrying out the operation to weight the coffee. This may be the Buyer or the Seller.
     */
    ContractRoleEnum getResponsibility();

    void setResponsibility(ContractRoleEnum contractRole);

    /**
     * Description of method used to weigh goods. The user will select the method of weighing from an industry standard list.
     */
    CoffeeWeightsCode getCoffeeWeightsCode();

    void setCoffeeWeightsCode(CoffeeWeightsCode coffeeWeightsCode);
}
