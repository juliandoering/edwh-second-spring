package net.nkg.nitro.edwh.authorization.check;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;
import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.profile.UserGroup;

/**
 * For most of the informations stored in EDWH only the EDWH admin is authorized to edit informations, but every user may retrieve data.
 */
public class BaseAuthorization<T extends BaseModel> implements Authorization<T> {

    private CurrentUserProvider userProvider;

    public BaseAuthorization(CurrentUserProvider userProvider) {
        this.userProvider = userProvider;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        switch (operation) {
            case RETRIEVE:
                UserGroup assignedGroup = this.userProvider.getCurrentUser() != null
                    ? this.userProvider.getCurrentUser().getAssignedGroup()
                    : null;
                return assignedGroup != null;

            default:
                return this.userProvider.isAdmin();
        }
    }
}
