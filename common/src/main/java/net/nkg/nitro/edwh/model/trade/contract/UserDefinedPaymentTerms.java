package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Terms of payment which are specifically defined by the user.<p/>
 * Example: Payment within 15 days and 3% cash discount or 30 days net.
 */
public interface UserDefinedPaymentTerms extends BaseModel {

    /**
     * A string or line of text containing alphanumeric characters.<p/>
     * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
     */
    String getLine();

    void setLine(String line);
}
