package net.nkg.nitro.edwh.model.valuation;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.ValuationReferenceData;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

public interface FreightRate extends ValuationReferenceData {

    LocalDate getValidityFrom();

    void setValidityFrom(LocalDate validityFrom);

    BigDecimal getRate();

    void setRate(BigDecimal rate);

    Location getFrom();

    void setFrom(Location from);

    Location getTo();

    void setTo(Location to);

    CurrencyUnit getCurrency();

    void setCurrency(CurrencyUnit currency);

    QuantityUnit getWeightUnit();

    void setWeightUnit(QuantityUnit weightUnit);

}
