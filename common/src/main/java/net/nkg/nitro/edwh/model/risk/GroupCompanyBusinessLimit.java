package net.nkg.nitro.edwh.model.risk;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

public interface GroupCompanyBusinessLimit extends BaseModel {

    BigDecimal getLimitLowEnd();

    void setLimitLowEnd(BigDecimal limitLowEnd);

    BigDecimal getLimitHighEnd();

    void setLimitHighEnd(BigDecimal limitHighEnd);

    LocalDate getValidFrom();

    void setValidFrom(LocalDate validFrom);

    LocalDate getValidTo();

    void setValidTo(LocalDate validTo);

    LimitType getLimitType();

    void setLimitType(LimitType limitType);

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);

    LimitUnit getLimitUnit();

    void setLimitUnit(LimitUnit limitUnit);
}
