package net.nkg.nitro.edwh.model.foundation;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Created by nf on 02.11.2016.
 */
public interface Packing extends ReferenceData {

    /**
     * Tare or unladen weight is the weight of an empty receptacle, vehicle or container.
     * The value is specified in kg.
     * @return
     */
    BigDecimal getTare();

    void setTare(BigDecimal tare);

    /**
     * Unit Size or net weight is calculated by subtracting the tare from the laden weight (gross weight).
     * The value is specified in kg.
     * @return
     */
    BigDecimal getUnitSize();

    void setUnitSize(BigDecimal unitSize);

    /**
     * A category used to map all individual packings of the group companies to a unified naming.
     * @return
     */
    PackingTypeCategory getCategory();

    void setCategory(PackingTypeCategory category);
}
