package net.nkg.nitro.edwh.model.hedging;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Contains informtion about the last trading day of robusta and arabica in London and New York.
 *
 */
public interface LastTradingDay extends BaseModel {
    /**
     * @return the {@link LocalDate} of the last trading day
     */
    LocalDate getLastTradingDay();

    /**
     * @param firstNoticeDay sets the last trading day
     */
    void setLastTradingDay(LocalDate lastTradingDay);
}
