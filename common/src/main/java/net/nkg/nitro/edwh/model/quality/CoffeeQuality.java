package net.nkg.nitro.edwh.model.quality;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;

import java.util.List;

/**
 * Created by nf on 25.10.2016.
 * Updated by nf on 30.03.2017 as part of DWH-121
 */
public interface CoffeeQuality extends BaseModel {

    enum CoffeeQualityFieldNames {
        NAME("name"),
        TYPE("type"),
        ORIGIN("origin"),
        ORIGINTYPE("originType"),
        VARIETY("variety"),

        GRADE("grade"),
        DEFECTCOUNT("defectCount"),
        HUMIDITY("humidity"),
        CUPPROFILE("cupProfile"),
        COMMERCIALNAME("commercialName");

        private String fieldName;

        CoffeeQualityFieldNames(String fieldName) {
            this.fieldName = fieldName;
        }

        public static CoffeeQualityFieldNames getCoffeeQualityFieldName(String fieldName) {
            if (fieldName != null) {
                // check all possible enum values if they match
                for (CoffeeQualityFieldNames coffeeQualityFieldName : CoffeeQualityFieldNames.values()) {
                    // test if the enum-value matches our given string
                    if (fieldName.equalsIgnoreCase(coffeeQualityFieldName.fieldName)) {
                        return coffeeQualityFieldName;
                    }
                }
            }
            return null;
        }
    }

    /**
     * The name of the Coffee Quality
     * @return
     */
    String getName();

    /**
     * Set the name of the Coffee Quality.
     * @param name
     */
    void setName(String name);

    /**
     * Get the type, preparation or state of the coffee.
     * <p/>
     * Valid values are for example:
     * <ul>
     * <li>Wet Cherry</li>
     * <li>Dry Cherry</li>
     * <li>Wet Parchment</li>
     * <li>Dry Parchment</li>
     * <li>Wet Unprocessed Green</li>
     * <li>...</li>
     * </ul>
     * @return
     */
    CoffeeType getCoffeeType();

    /**
     * <ul>
     * <li>Wet Cherry</li>
     * <li>Dry Cherry</li>
     * <li>Wet Parchment</li>
     * <li>Dry Parchment</li>
     * <li>Wet Unprocessed Green</li>
     * <li>...</li>
     * </ul>
     * @param coffeeType
     */
    void setCoffeeType(CoffeeType coffeeType);

    /**
     * Get the variety of this coffee qualioty and its basic preparation.
     * <p/>
     * Valid values are:
     * <ul>
     * <li>Washed Arabica</li>
     * <li>Semiwashed Arabica</li>
     * <li>Unwashed Arabica</li>
     * <li>Washed Robusta</li>
     * <li>Unwashed Robusta</li>
     * <li>Arabica-Robusta blend</li>
     * </ul>
     * @return
     */
    CoffeeVariety getCoffeeVarietyType();

    /**
     * Set the variety of this coffee quality and its basic preparation.
     * <p/>
     * Valid values are:
     * <ul>
     * <li>Washed Arabica</li>
     * <li>Semiwashed Arabica</li>
     * <li>Unwashed Arabica</li>
     * <li>Washed Robusta</li>
     * <li>Unwashed Robusta</li>
     * <li>Arabica-Robusta blend</li>
     * </ul>
     * @param coffeeVarietyType
     */
    void setCoffeeVarietyType(CoffeeVariety coffeeVarietyType);

    /**
     * Set the grade specification.
     * @return
     */
    String getGradeSpecification();

    /**
     * Get the grade specification.
     * @param gradeSpec
     */
    void setGradeSpecification(String gradeSpec);

    /**
     * 
     * @return
     */
    String getDefectCountSpecification();

    /**
     * 
     * @param defectCountSpec
     */
    void setDefectCountSpecification(String defectCountSpec);

    /**
     * 
     * @return
     */
    String getHumiditySpecification();

    /**
     * 
     * @param humiditySpec
     */
    void setHumiditySpecification(String humiditySpec);

    /**
     * 
     * @return
     */
    String getCupProfileSpecification();

    /**
     * Set the information about the cup profile.
     * @param cupProfile
     */
    void setCupProfileSpecification(String cupProfile);

    /**
     * Get the origin type of the Coffee Quality.
     * <p/>
     * An origin type is for example:
     * <ul>
     * <li>Single Origin</li>
     * <li>Basket</li>
     * <li>Blend</li>
     * </li>Unspecified</li>
     * </ul>
     * Some origin types are hard wired as they are used for business logic, see the constants.
     *
     * @return
     */
    CoffeeQualityOriginType getOriginType();

    /**
     * Set the origin type of the Coffee Quality.
     * <p/>
     * An origin type is for example:
     * <ul>
     * <li>Single Origin</li>
     * <li>Basket</li>
     * <li>Blend</li>
     * </li>Unspecified</li>
     * </ul>
     *
     * @param originType
     */
    void setOriginType(CoffeeQualityOriginType originType);

    /**
     * Get the origin of the Coffee Quality
     *
     * @return
     */
    List<Location> getOrigins();

    /**
     * Set the Origins of a Coffee Quality.
     * Each Value should be Master Data.
     *
     * @param origins
     */
    void setOrigins(List<Location> origins);

    void setConversionToGradedDistribution(List<CoffeeQualityPercentage> conversionToGradedDistribution);

    List<CoffeeQualityPercentage> getConversionToGradedDistribution();

}
