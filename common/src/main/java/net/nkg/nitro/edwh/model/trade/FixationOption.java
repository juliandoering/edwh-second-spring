package net.nkg.nitro.edwh.model.trade;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * The party whose is responsible to fix the contract must be identified and is mandatory if the price has not been entered
 * on the contract itself, in this case it is either Buyer or Seller with or without margin payable.
 */
public interface FixationOption extends ReferenceData {

}
