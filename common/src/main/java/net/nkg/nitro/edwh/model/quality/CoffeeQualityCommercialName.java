package net.nkg.nitro.edwh.model.quality;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

/**
 * Quality's commercial names.
 * @author niklas
 *
 */
public interface CoffeeQualityCommercialName extends BaseModel {

    String getName();

    void setName(String name);

    CoffeeQuality getCoffeeQuality();

    void setCoffeeQuality(CoffeeQuality coffeeQuality);

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);
}
