package net.nkg.nitro.edwh.model.quality;

import net.nkg.nitro.edwh.model.common.ReferenceData;

public interface CoffeeQualityOriginType extends ReferenceData {

    class Code {
        public static final String SO = "SO"; // Single Origin
        public static final String BMO = "BMO"; // Basket/Multiple Origin
        public static final String BL = "BL"; // Blend

        private Code() {}
    }
}
