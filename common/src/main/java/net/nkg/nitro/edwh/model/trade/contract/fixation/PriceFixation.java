package net.nkg.nitro.edwh.model.trade.contract.fixation;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

/**
 * Interface specifying price fixation objects (of price fixation events) .
 * <p>
 *
 * Used for Contracts with pricing types:
 * <li>Price To Be Fixed (PTBF)</li>
 * <li>Differential To Be Fixed (DTBF)</li>
 * <li>Price to Follow (PTF)</li>
 *
 * Introduced in [DWH-92].
 * Refactored in [DWH-151] "Adjust pricing type and related attributes in Contract" due to
 * [DWH-91] - "Refining the definition of Contract Data".
 *
 * @author nf
 *
 */
public interface PriceFixation extends BaseModel {

    /**
     * Gets the contract this is for.
     *
     * @return the contract this is for.
     */
    Contract getContract();

    void setContract(Contract contract);

    /**
     * Return the price value of the fixation.
     * PTBF: the 'terminal level', meaning the price in $/MT (London) or ct/LBS (NY).
     * DTBF: the differential
     * PTF: the full price in the contract currency
     * @return
     */
    BigDecimal getFixation();

    /**
     * Set the price value of the fixation.
     * @param fixation The actual value
     */
    void setFixation(BigDecimal fixation);

    /**
     * Set the date of when the fixation was set.
     * @param date
     */
    void setFixationDate(LocalDate date);

    /**
     * Get the date of the fixation.
     * @return
     */
    LocalDate getFixationDate();

    /**
     * Return the number of the fixation. "a sequential chronological numbering".
     * @return
     */
    Long getFixationNumber();

    /**
     * Set the number of a fixation manually. This should be "a sequential chronological numbering".
     */
    void setFixationNumber(Long fixationId);

    /**
     * Set how many price fixation units this fixation should cover.
     * This way multiple fixations can be set at once.
     * PTBF: the number of terminal lots to be fixed.
     * DTBF/PTF: the volume in the quantity unit of the contracts (e.g. kg, 60kg Bags, MT...)
     */
    void setQuantity(BigDecimal quanitiy);

    /**
     * Returns how many price fixation units this fixation should cover.
     * @return
     */
    BigDecimal getQuantity();

}
