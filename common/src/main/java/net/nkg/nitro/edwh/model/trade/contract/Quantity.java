package net.nkg.nitro.edwh.model.trade.contract;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

/**
 * When used in the 'Destination Declaration' document in the 'Declaration Information' section, this Line quantity is the
 * actual quantity of the commodity which is to be delivered to the associated location.
 * In the Destination Declaration, Price Fix Letter, and Price Fix Rolling Letter, in the Original Contract Details section, the
 * entire quantity from the Contract will be entered.
 */
public interface Quantity extends BaseModel {

    /**
     * This is the actual quantity of the commodity, being referenced.<p/>
     * Recommended Format: ---,---,---,---,--9.999999
     */
    BigDecimal getValue();

    void setValue(BigDecimal value);

    /**
     * The user can select a industry standard quantity unit, in which the associated commodity is measured.
     */
    QuantityUnit getUnit();

    void setUnit(QuantityUnit unit);
}
