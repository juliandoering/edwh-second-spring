package net.nkg.nitro.edwh.model.trade.contract.shipmentadvice;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface Container extends BaseModel {

    /**
     * Gets the container number.
     * 
     * @return the container number.
     */
    String getContainerNumber();

    void setContrainerNumber(String containerNumber);

    /**
     * Gets the seal number.
     * 
     * @return the seal number.
     */
    String getSealNumber();

    void setSealNumber(String sealNumber);

    /**
     * Gets the shippers seal number.
     * 
     * @return the shippers seal number.
     */
    String getShippersSealNumber();

    void setShippersSealNumber(String sealNumber);
}
