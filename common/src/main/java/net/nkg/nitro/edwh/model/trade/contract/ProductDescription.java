package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This section is used to provide a detailed technical description of the commodity, with details such as moisture content,
 * grade and roast, applying in the context of coffee.
 */
public interface ProductDescription extends BaseModel {

    /**
     * A string or line of text containing alphanumeric characters.<p/>
     * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
     */
    String getLine();

    void setLine(String line);
}
