package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Should the user have selected 'Other' in the Approval terms code list, they may choose to enter the specific Approval terms here.
 * <p/>
 * A string or line of text containing alphanumeric characters.<p/>
 * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
 */
public interface OtherApprovalTerms extends BaseModel {

    /**
     * A string or line of text containing alphanumeric characters.<p/>
     * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
     */
    String getLine();

    void setLine(String line);
}
