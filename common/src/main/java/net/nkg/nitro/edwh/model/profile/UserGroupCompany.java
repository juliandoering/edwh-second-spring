package net.nkg.nitro.edwh.model.profile;

public interface UserGroupCompany {

    Long getLongId();

    String getUuid();
}
