package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Identification of a related concept through identification marks. For example, identification of a package or
 * identification of a piece of equipment.<p/>
 * Example: 123-RTF.
 */
public interface MarkOrNumber extends BaseModel {

    /**
     * Identification of a related concept through identification marks. For example, identification of a package or
     * identification of a piece of equipment.<p/> 
     * Example: 123-RTF.
     */
    String getLine();

    void setLine(String line);
}
