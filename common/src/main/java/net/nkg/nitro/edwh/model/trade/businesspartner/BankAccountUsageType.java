package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A bank account usage type is defined by one of those codes:
 * PC, PS
 */
public interface BankAccountUsageType extends ReferenceData {

}
