package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

public interface MaximumOpenPosition extends BaseModel {

    QuantityUnit getQuantityUnit();

    void setQuantityUnit(QuantityUnit quantityUnit);

    BigDecimal getQuantity();

    void setQuantity(BigDecimal quantity);

    Long getForwardPeriod();

    void setForwardPeriod(Long days);

    LocalDate getValidFrom();

    void setValidFrom(LocalDate from);

    LocalDate getValidTo();

    void setValidTo(LocalDate to);
}
