package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A shipment must have a certain size of container as described in [DWH-492], i.e.  20ft DC, 40ft DC or 40ft HC
 * 
 * @author sl
 *
 */
public interface ContainerSize extends ReferenceData {

}
