package net.nkg.nitro.edwh.model.quality;

/**
 * Matches a CoffeeQuality to a theoretical graded conversion factor. This is used to calculate the conversion factor
 * to graded coffee for coffee qualities.
 */
public interface ContractCoffeeQualityPercentage extends CoffeeQualityPercentage {

}
