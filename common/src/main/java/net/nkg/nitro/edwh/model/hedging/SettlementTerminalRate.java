package net.nkg.nitro.edwh.model.hedging;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Contains the coffee futures settlement value based for set of Terminals, Positions & Dates.
 */
public interface SettlementTerminalRate extends BaseModel {

    /**
     * Futures market which corresponds to the futures market
     * of contracts futures ID.
     */
    FutureMarket getFuturesMarket();

    void setFuturesMarket(FutureMarket market);

    /**
     * Futures month which corresponds to the futures month
     * of contracts futures ID.
     */
    FutureMonth getFuturesMonth();

    void setFuturesMonth(FutureMonth month);

    /**
     * Futures year which corresponds to the futures year
     * of contracts futures ID.
     */
    Integer getFuturesYear();

    void setFuturesYear(Integer year);

    /**
     * The date when this settlement terminal rate was valid.
     */
    LocalDate getSettlementDate();

    void setSettlementDate(LocalDate date);

    /**
     * The settlement value based for the set of Terminal, Position & Date.
     */
    BigDecimal getSettlementValue();

    void setSettlementValue(BigDecimal value);

    /**
     * The total number of futures/options contracts long or short of the clearing house in a delivery month or market that has been entered into
     * and not yet liquidated by an offsetting transaction or fulfilled by delivery.
     */
    Integer getOpenInterest();

    void setOpenInterest(Integer openInterest);
}
