package net.nkg.nitro.edwh.model.trade.contract;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Contractual requirements comprising of general terms and conditions and any specific non-standard clauses peculiar to
 * a business document. The general terms and conditions may be identified by a link to a location on a website or to a
 * document which holds the specific terms for a particular party.
 */
public interface TermsAndConditions extends BaseModel {

    /**
     * Unique reference to applicable terms and conditions. This may be a web address at which the relevant document
     * terms and conditions are available. This could also be a reference to a legal agreement or other recognised source.
     * On paper, these terms and conditions are normally found on the back of a document.<p/>
     * Example: http://www.bolero.net/xyz/123
     */
    String getReference();

    void setReference(String reference);

    /**
     * Terms and conditions in addition to standard terms and conditions or any special terms and conditions.<p/>
     * Example: NMA Clause 23B.
     */
    List<Clause> getClauses();

    void setClauses(List<Clause> clauses);

    /**
     * Here the user can enter any further conditions or exceptions that may apply in this case.
     */
    List<ExceptionsAndOtherConditions> getExceptionsAndOtherConditions();

    void setExceptionsAndOtherConditions(List<ExceptionsAndOtherConditions> exceptionsAndOtherConditions);

}
