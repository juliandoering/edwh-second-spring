package net.nkg.nitro.edwh.model.common;

/**
 * Base interfaces for all model interfaces, which are used for reporting.
 * This is used for being more precise for generics.
 * 
 * @author elv
 */
public interface AnalyticalData extends BaseModel {

    /**
     * Flag to indicate if the quality check failed.
     * Reports can use this flag to decide if all data or only correct data is used
     * or it can be used to create a quality index about the data quality.
     * @return
     */
    boolean isErroneousData();

    void setErroneousData(boolean erroneousData);
}
