package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.ReferenceData;

public interface BusinessThirdPartyReferenceIdType extends ReferenceData {

    String CODE_PERSONAL_IDENTIFICATION = "PIN";

    String DESCRIPTION_PERSONAL_IDENTIFICATION = "Personal Identification Number";

    boolean getIdSupplierMandatory();

    void setIdSupplierMandatory(boolean idSupplierMandatory);

    default boolean isPersonalIdentificationNumber() {
        return CODE_PERSONAL_IDENTIFICATION.equalsIgnoreCase(getCode());
    }
}
