package net.nkg.nitro.edwh.model.foundation;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    private final String genderString;

    private Gender(final String text) {
        this.genderString = text;
    }

    /**
     * Returns the enum type to a string or null if the string does
     * not match any known enum type.
     * The method is case insensitive
     * @param genderString
     * @return
     */
    public static Gender getGender(String genderString) {
        if (genderString != null) {
            // check all possible enum values if they match
            for (Gender genderVal : Gender.values()) {
                // test if the enum-value matches our given string
                if (genderString.equalsIgnoreCase(genderVal.toString())) {
                    // we found the right enum-value
                    return genderVal;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return genderString;
    }
}
