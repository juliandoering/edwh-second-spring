package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This section is used to detail all the pertinent information to do with products or commodities being trade, such as the
 * commodity description, quantities, prices, characteristics etc.
 */
public interface Consignment extends BaseModel {

    /**
     * When used in the 'Destination Declaration' document in the 'Declaration Information' section, this Line quantity is the
     * actual quantity of the commodity which is to be delivered to the associated location.
     * In the Destination Declaration, Price Fix Letter, and Price Fix Rolling Letter, in the Original Contract Details section, the
     * entire quantity from the Contract will be entered.
     */
    Quantity getQuantity();

    void setQuantity(Quantity quantity);

    /**
     * This section is used to specify the price per unit of a product or commodity. A price, a currency and a unit of measure to
     * which that price applies will be entered. The price is mandatory should a Future ID not be entered below.
     */
    UnitPrice getUnitPrice();

    void setUnitPrice(UnitPrice unitPrice);

    /**
     * The specific details of the commodity are entered in this section, for example the product code, packaging, country of
     * origin, crop year, and product description.
     */
    ProductQuality getProductQuality();

    void setProductQuality(ProductQuality productQuality);
}
