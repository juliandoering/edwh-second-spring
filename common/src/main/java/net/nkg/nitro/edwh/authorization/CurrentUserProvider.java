package net.nkg.nitro.edwh.authorization;

import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.profile.UserRole.EdwhUserRole;
import net.nkg.nitro.edwh.model.profile.Userprofile;

/**
 * The general authentication implementation does not now about the current user and so needs a provider
 * implement this interfaces to access the current users {@link Userprofile}.
 */
public interface CurrentUserProvider {

    /**
     * Returns the {@link Userprofile} of the current user.
     * @return the {@link Userprofile} of the current user.
     */
    Userprofile getCurrentUser();

    String getToken();

    /**
     * Returns if the current user is an EDWH admin.
     * @return {@code true} if the current user is an EDWH admin, otherwise {@code false}.
     */
    default boolean isAdmin() {
        return getCurrentUser() != null
            && getCurrentUser().isAdmin();
    }

    default boolean isRoleAssigned(EdwhUserRole edwhRole) {
        return getCurrentUser() != null
            && getCurrentUser().isRoleAssigned(edwhRole);
    }

    default boolean isGroupCompanyAssigned(GroupCompany groupCompany) {
        return getCurrentUser() != null
            && getCurrentUser().isGroupCompanyAssigned(groupCompany);
    }
}
