package net.nkg.nitro.edwh.model.logistics;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;

/**
 * Group of port of loadings of a {@link FreightRateTrade}.
 *
 */
public interface FreightRateLoading extends BaseModel {

    /**
     * The name of the group, e.g. Atlantic coast.
     */
    String getName();

    void setName(String name);

    /**
     * A list of ports this is composed of.
     * @return
     */
    List<Location> getPortsOfLoading();

    void setPortsOfLoading(List<Location> portsOfLoading);

    /**
     * Available shipping lines for the this ports of loading.
     * @return
     */
    List<FreightRateLine> getFreightRateLine();

    void setFreightRateLine(List<FreightRateLine> freightRateLine);

}
