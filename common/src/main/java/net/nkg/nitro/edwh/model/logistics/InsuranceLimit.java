package net.nkg.nitro.edwh.model.logistics;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;

public interface InsuranceLimit extends BaseModel {
    /**
     * Max insured value.
     * @return
     */
    BigDecimal getLimit();

    void setLimit(BigDecimal limit);

    /**
     * The currency of the value {@link #getLimit()}.
     * @return
     */
    CurrencyUnit getCurrency();

    void setCurrency(CurrencyUnit currency);

    /**
     * The date this limit replaced a previous.
     * @return
     */
    LocalDate getValidityFrom();

    void setValidityFrom(LocalDate validityFrom);
    
    /**
     * The date this limit is valid to.
     * @return
     */
    LocalDate getValidityTo();

    void setValidityTo(LocalDate validityTo);
}
