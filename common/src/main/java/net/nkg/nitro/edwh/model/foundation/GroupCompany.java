package net.nkg.nitro.edwh.model.foundation;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Created by nf on 02.11.2016.
 */
public interface GroupCompany extends BaseModel {

    PostalAddress getPostalAddress();

    void setPostalAddress(PostalAddress postalAddress);

    List<Individual> getManagers();

    void setManagers(List<Individual> managers);

    List<Contact> getContacts();

    void setContacts(List<Contact> contacts);

    String getLegalName();

    void setLegalName(String legalName);

    String getAcronym();

    void setAcronym(String acronym);

    String getTagetikCode();

    void setTagetikCode(String tageticCode);

    BusinessFunction getBusinessFunction();

    void setBusinessFunction(BusinessFunction businessFunction);
}
