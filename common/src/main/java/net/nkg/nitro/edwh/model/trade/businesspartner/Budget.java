package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

public interface Budget extends BaseModel {

    /**
     * The quantity unit in which the budget is specified .
     * @return
     */
    QuantityUnit getQuantityUnit();

    /**
     * The quantity unit in which the budget is specified .
     * @param quantityUnit
     */
    void setQuantityUnit(QuantityUnit quantityUnit);

    /**
     * The amount of <i>QuantityUnit</i> that is the budget.
     * @return
     */
    BigDecimal getVolume();

    /**
     * The amount of <i>QuantityUnit</i> that is the budget.
     * @param volume
     */
    void setVolume(BigDecimal volume);

    /**
     * Get the date from when on the budget is valid.
     * @return
     */
    LocalDate getValidFrom();

    /**
     * Set the date from when on the budget is valid.
     * @param validFromDate
     */
    void setValidFrom(LocalDate validFromDate);

    /**
     * Get the date till when the budget is valid.
     * @return
     */
    LocalDate getValidTo();

    /**
     * Set the date till when the budget is valid.
     * @param validToDate
     */
    void setValidTo(LocalDate validToDate);

}
