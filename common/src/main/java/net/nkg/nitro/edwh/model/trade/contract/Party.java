package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;

public interface Party extends BaseModel {

    /**
     * Allows for unique organization referencing. This consists of an identifier and type.
     */
    GroupCompany getCompany();

    void setCompany(GroupCompany company);

    /**
     * Allows for unique organization referencing. This consists of an identifier and type.
     */
    BusinessThirdParty getThirdParty();

    void setThirdParty(BusinessThirdParty thirdParty);

    /**
     * Allows for referencing of a transaction, which enables the party concerned to consolidate all related data. This is
     * generally for internal company use.
     */
    String getTransactionReference();

    void setTransactionReference(String transactionReference);
}
