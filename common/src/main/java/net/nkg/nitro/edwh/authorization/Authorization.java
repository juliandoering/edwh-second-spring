package net.nkg.nitro.edwh.authorization;

@FunctionalInterface
public interface Authorization<T> {

    /**
     * Checks if a model authorizations must be checked.
     *
     * @param model to be checked. May be used in overriding methods.
     * @return {@code true} if the user may see the model, otherwise {@code false}.
     */
    boolean isAuthorized(T model, RestOperation operation);

    default Authorization<T> or(Authorization<T> other) {
        return new OrAuthorization<>(this, other);
    }

    default Authorization<T> and(Authorization<T> other) {
        return new AndAuthorization<>(this, other);
    }

}
