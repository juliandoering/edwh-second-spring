package net.nkg.nitro.edwh.model.trade.contract;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This describes the period in which the coffee will be moved or delivered, by way of start and end date.
 */
public interface MoveOrDeliverPeriod extends BaseModel {

    /**
     * Date on which an activity commences or a concept becomes valid. For example, the start date of an insurance policy.
     */
    LocalDate getStartDate();

    void setStartDate(LocalDate startDate);

    /**
     * Date on which an activity ends or a concept becomes invalid.
     */
    LocalDate getEndDate();

    void setEndDate(LocalDate endDate);
}
