package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A shipment must have a certain mode of transport, i.e. Port to Port, Port to Door, Door to Port or Door to Door
 */
public interface ModeOfTransport extends ReferenceData {

}
