package net.nkg.nitro.edwh.model.trade;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

/**
 * Storing exchange rate of a coffee contract, where the 'currency from' is the one of the contract and the 'currency to' is USD.
 */
public interface CoffeeContractExchangeRate extends AnalyticalData {

    /**
     * @return the coffee contract the exchange rate is for
     */
    Contract getContract();

    /**
     * Set the coffee contract the exchange rate is for
     * @param contract
     */
    void setContract(Contract contract);

    /**
     * @return the exchange rate of the coffee contract.
     */
    BigDecimal getExchangeRate();

    /**
     * Set the exchange rate for the coffee contract.
     * @param exchangeRate a positive decimal value.
     */
    void setExchangeRate(BigDecimal exchangeRate);

}
