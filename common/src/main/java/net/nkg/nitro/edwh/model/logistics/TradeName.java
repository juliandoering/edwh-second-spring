package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A trade name for Ocean Freight Rates.
 * @see FreightRateTrade
 */
public interface TradeName extends ReferenceData {
}
