package net.nkg.nitro.edwh.model.logistics;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyGroup;

/**
 * A shipping line available for trade routes.
 */
public interface FreightRateLine extends BaseModel {

    /**
     * @return the shipping line represented by the linked {@link BusinessThirdPartyGroup}.
    
     */
    BusinessThirdPartyGroup getProvider();

    void setProvider(BusinessThirdPartyGroup provider);

    /**
     * The trade routes this provider is available for.
     * @return
     */
    List<FreightRateTradeRoute> getFreightRateTradeRoute();

    void setFreightRateTradeRoute(List<FreightRateTradeRoute> freightRateTradeRoute);

}
