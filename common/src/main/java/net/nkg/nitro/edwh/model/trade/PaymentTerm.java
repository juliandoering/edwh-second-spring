package net.nkg.nitro.edwh.model.trade;

import net.nkg.nitro.edwh.model.common.ReferenceData;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

/**
 * Code identifying the type of payment.ap/>
 * Example: CREDOC - Irrevocable d.c. - unconfirmed, period of time for payment unspecified
 * Referenced from {@link BusinessThirdParty} and {@link Contract}
 * In contrast to {@Link PaymentTerms}, this class represents the concrete terms, instead of being a container for
 * different kinds of terms.
 */
public interface PaymentTerm extends ReferenceData {

    boolean isViaBank();
    
    void setViaBank(boolean viaBank);
    
}
