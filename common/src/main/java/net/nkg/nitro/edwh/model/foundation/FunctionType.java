package net.nkg.nitro.edwh.model.foundation;

/**
 * Enumeration of location function types, based on the UN/LOCODE standard
 * http://www.unece.org/cefact/locode
 */
public enum FunctionType {
    PORT("Port"),
    RAIL_TERMINAL("Rail Terminal"),
    ROAD_TERMINAL("Road Terminal"),
    AIRPORT("Airport"),
    POSTAL_EXCHANGE_OFFICE("Postal Exchange Office"),
    RESERVED_MULTIMODAL("Reserved Multimodal"),
    RESERVED_FIXED("Reserved Fixed"),
    BORDER_CROSSING("Border Crossing"),
    NOT_KNOWN("Not known");

    private final String functionTypeString;

    private FunctionType(final String text) {
        this.functionTypeString = text;
    }

    /**
     * Returns the enum type to a string or null if the string does
     * not match any known enum type.
     * The method is case insensitive
     * @param functionString
     * @return
     */
    public static FunctionType getFunctionType(String functionString) {
        if (functionString != null) {
            // check all possible enum values if they match
            for (FunctionType functionType : FunctionType.values()) {
                // test if the enum-value matches our given string
                if (functionString.equalsIgnoreCase(functionType.functionTypeString)
                    || functionString.equalsIgnoreCase(functionType.name())) {
                    // we found the right enum-value
                    return functionType;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return functionTypeString;
    }
}
