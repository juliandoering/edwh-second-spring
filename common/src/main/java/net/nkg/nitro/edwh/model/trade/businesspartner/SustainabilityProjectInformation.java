package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.common.ValidityPeriod;
import net.nkg.nitro.edwh.model.trade.SustainabilityProject;

public interface SustainabilityProjectInformation extends BaseModel, ValidityPeriod {

    SustainabilityProject getSustainabilityProject();

    void setSustainabilityProject(SustainabilityProject certification);

    String getThirdPartyIdNumber();

    void setThirdPartyIdNumber(String idNumber);

    LocalDate getValidityPeriodFrom();

    void setValidityPeriodFrom(LocalDate date);

    LocalDate getValidityPeriodTo();

    void setValidityPeriodTo(LocalDate date);
}
