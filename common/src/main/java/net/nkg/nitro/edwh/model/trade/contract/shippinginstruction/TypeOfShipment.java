package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A shipment must have a certain type of shipment, i.e. In Bags, In Bulk or In Big Bags
 */
public interface TypeOfShipment extends ReferenceData {

}
