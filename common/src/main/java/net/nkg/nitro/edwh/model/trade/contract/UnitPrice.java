package net.nkg.nitro.edwh.model.trade.contract;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

/**
 * This section is used to specify the price per unit of a product or commodity. A price, a currency and a unit of measure to
 * which that price applies will be entered. The price is mandatory should a Future ID not be entered below.
 */
public interface UnitPrice extends BaseModel {

    /**
     * A number which can be associated with a monetary amount, quantity, duration or a unit of measurement. For example,
     * an amount of United States Dollars, a number of Cubic Metres or a number of Kilograms.<p/>
     * Recommended Format: ---,---,---,---,--9.999999
     */
    BigDecimal getValue();

    void setValue(BigDecimal value);

    /**
     * Code which uniquely identifies the currency related to a monetary unit. Please note that symbols such as '$' cannot be entered.<p/>
     * Example: USD = United States Dollar, GBP = Pound Sterling.
     */
    CurrencyUnit getCurrency();

    void setCurrency(CurrencyUnit currency);

    /**
     * The user will select the unit against which this price applies, if appropriate. They can select the units from an industry
     * standard code list.
     */
    QuantityUnit getUnit();

    void setUnit(QuantityUnit unit);
}
