package net.nkg.nitro.edwh.authorization.check;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;

public class AdminAuthorization<T> implements Authorization<T> {

    private CurrentUserProvider userProvider;
    private RestOperation[] allowedOperations;

    public AdminAuthorization(CurrentUserProvider userProvider, RestOperation... allowedOperations) {
        this.userProvider = userProvider;
        this.allowedOperations = allowedOperations;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        return userProvider.isAdmin() && isAllowedOperation(operation);
    }

    private boolean isAllowedOperation(RestOperation operation) {
        if (allowedOperations == null || allowedOperations.length == 0) {
            return true;
        }
        for (RestOperation allowedOperation : allowedOperations) {
            if (allowedOperation == operation) {
                return true;
            }
        }
        return false;
    }
}
