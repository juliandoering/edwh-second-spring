package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface TaxId extends BaseModel {

    String getTaxId();

    void setTaxId(String taxId);
}
