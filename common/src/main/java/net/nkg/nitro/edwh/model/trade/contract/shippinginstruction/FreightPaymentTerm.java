package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A shipment must have a certain freight payment term as described in [DWH-492], i.e. Collect or Prepaid
 */
public interface FreightPaymentTerm extends ReferenceData {

}
