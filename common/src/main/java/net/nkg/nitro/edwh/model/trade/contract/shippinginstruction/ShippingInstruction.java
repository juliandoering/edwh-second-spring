package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

/**
 * Shipping instructions as first defined in [DWH-82].
 *
 * A buying group company can upload shipping instructions/destination declarations to a concluded contract.
 * <p>
 * Shipping instructions can include a list of so called "Additional Documents" (see {@link AnyOtherInstruction}) which are a set of
 * undefined documents or notes or insturctions that may need to be attacked e.g. because of legal reasons.
 * @author nf
 *
 */
public interface ShippingInstruction extends BaseModel {

    /**
     * Gets the contract this is for.
     *
     * @return the contract this is for.
     */
    Contract getContract();

    void setContract(Contract contract);

    /**
     * Get the sequential number of this shipping instruction within a contract.
     * @return the sequential number or <b>null</b> if this object is not associated with a sequential number (e.g. value object or DTO)
     */
    Long getShippingInstructionNumber();

    /**
     * Set the sequential number of this shipping instruction within a contract.
     */
    void setShippingInstructionNumber(Long shippingInstructionNumber);

    /**
     * Quantity to ship
     * @return
     */
    BigDecimal getQuantity();

    /**
     * Set Quantity to ship
     * @param quantity
     */
    void setQuantity(BigDecimal quantity);

    /**
     * Set Container Quantity to ship
     * @param containerQuantity
     */
    void setContainerQuantity(Integer containerQuantity);

    /**
     * Container Quantity to ship
     * @return
     */
    Integer getContainerQuantity();

    /**
     * Quantity Unit
     * @return
     */
    QuantityUnit getQuantityUnit();

    /**
     * Set Quantity Unit
     * @param quantityUnit
     */
    void setQuantityUnit(QuantityUnit quantityUnit);

    /**
     * Package Unit ({@link Packing})
     * @return
     */
    Packing getPackageUnit();

    /**
     * Set Package Unit ({@link Packing})
     * @param packageUnit
     */
    void setPackageUnit(Packing packageUnit);

    /**
     * port of shipment/port of loading
     * @return
     */
    Location getPortOfLoading();

    /**
     * Set port of shipment/port of loading
     * @param portOfLoading
     */
    void setPortOfLoading(Location portOfLoading);

    /**
     * place of receipt
     * @return
     */
    Location getPlaceOfReceipt();

    /**
     * Set place of receipt
     * @param placeOfReceipt
     */
    void setPlaceOfReceipt(Location placeOfReceipt);

    /**
     * Get place of delivery
     * @return
     */
    Location getPlaceOfDelivery();

    /**
     * Set place of delivery
     * @param placeOfDelivery
     */
    void setPlaceOfDelivery(Location placeOfDelivery);

    /**
     * Get port of discharge
     * @return port of discharge
     */
    Location getPortOfDischarge();

    /**
     * Set port of discharge
     * @param portOfDischarge
     */
    void setPortOfDischarge(Location portOfDischarge);

    /**
     * Return 'any other instructions' of this shipping instruction.
     * @return
     */
    List<AnyOtherInstruction> getAnyOtherInstructions();

    /**
     * Set 'any other instructions' of this shipping instruction.
     * @param anyOtherInstructions
     */
    void setAnyOtherInstructions(List<AnyOtherInstruction> anyOtherInstructions);

    /**
     * Container Movement
     * @return
     */
    ContainerMovement getContainerMovement();

    /**
     * Set Container Movement
     * @param containerMovement
     */
    void setContainerMovement(ContainerMovement containerMovement);

    /**
     * Container Size
     * @return
     */
    ContainerSize getContainerSize();

    /**
     * Set Container Size
     * @param containerSize
     */
    void setContainerSize(ContainerSize containerSize);

    /**
     * Type Of Shipment
     * @return
     */
    TypeOfShipment getTypeOfShipment();

    /**
     * Set Type Of Shipment
     * @param typeOfShipment
     */
    void setTypeOfShipment(TypeOfShipment typeOfShipment);

    /**
     * Mode Of Transport
     * @return
     */
    ModeOfTransport getModeOfTransport();

    /**
     * Set Mode Of Transport
     * @param modeOfTransport
     */
    void setModeOfTransport(ModeOfTransport modeOfTransport);

    /**
     * Shipment Period From Date
     * @return
     */
    LocalDate getShipmentPeriodFromDate();

    /**
     * Set Shipment Period From Date
     * @param fromDate
     */
    void setShipmentPeriodFromDate(LocalDate fromDate);

    /**
     * Shipment Period To Date
     * @return
     */
    LocalDate getShipmentPeriodToDate();

    /**
     * Set Shipment Period To Date
     * @param toDate
     */
    void setShipmentPeriodToDate(LocalDate toDate);

    /**
     * Business Third Party which is the Shipping Line
     * @return
     */
    BusinessThirdParty getShippingLine();

    /**
     * Set Business Third Party which is the Shipping Line
     * @param shippingLine
     */
    void setShippingLine(BusinessThirdParty shippingLine);

    /**
     * Service Agreement Number
     * @return
     */
    String getServiceAgreementNumber();

    /**
     * Set Service Agreement Number
     * @param serviceAgreementNumber
     */
    void setServiceAgreementNumber(String serviceAgreementNumber);

    /**
     * Freight Payable by Group Company or Business Third Party
     * @return
     */
    PayingParty getFreightPayableBy();

    /**
     * Set Freight Payable by Group Company or Business Third Party
     * @param freightPayableBy
     */
    void setFreightPayableBy(PayingParty freightPayableBy);

    /**
     * Freight Payment Term
     * @return
     */
    FreightPaymentTerm getFreightPaymentTerm();

    /**
     * Set Freight Payment Term
     * @param freightPaymentTerm
     */
    void setFreightPaymentTerm(FreightPaymentTerm freightPaymentTerm);

}
