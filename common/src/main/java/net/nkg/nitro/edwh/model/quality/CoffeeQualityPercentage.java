package net.nkg.nitro.edwh.model.quality;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Matches a CoffeeQuality to a graded conversion factor. This is used to calculate the standard conversion factor
 * to graded coffee for coffee qualities.
 */
public interface CoffeeQualityPercentage extends BaseModel {

    /**
     * Set the coffee quality that is matched.
     * @param quality the coffee quality.
     */
    void setCoffeeQuality(CoffeeQuality coffeeQuality);

    /**
     * @return the coffee quality that is matched.
     */
    CoffeeQuality getCoffeeQuality();

    /**
     * Set the conversion factor for the matched coffee quality.
     * @param gradedConversionFactor a positive decimal value.
     */
    void setGradedConversionFactor(BigDecimal gradedConversionFactor);

    /**
     * @return the conversion factor for the matched coffee quality.
     */
    BigDecimal getGradedConversionFactor();

}
