package net.nkg.nitro.edwh.model.common;

import java.io.Serializable;
import java.util.UUID;

/**
 * Base interface for all other model interfaces used in EDWH.
 */
public interface BaseModel extends Serializable {

    String getId();

    long getVersion();

}
