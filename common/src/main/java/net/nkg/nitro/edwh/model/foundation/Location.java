package net.nkg.nitro.edwh.model.foundation;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 *
 * Interface telling how to interact with Locations.
 * Created by nf on 02.11.2016 as part of [DWH-22] and later on changed by af as part of the UnLocode story.
 */
public interface Location extends BaseModel {

    /**
     * Convenience method to get country and location code in one shot.
     * @return the five letter code XX XXX of the UN/LOCODE standard for this location
     */
    String getUnLocode();

    /**
     * @return the location code (format XXX)
     */
    String getLocationCode();

    /**
     * Set the location code of the location (format XXX)
     * @param locationCode
     */
    void setLocationCode(String locationCode);

    /**
     * @return the iso country code of the location (format XX)
     */
    String getCountryCode();

    /**
     * Set the ISO country code of the location (format XX)
     * @param countryCode
     */
    void setCountryCode(String countryCode);

    /**
     * @return the location's name
     */
    String getName();

    void setName(String name);

    /**
     * This contains the geographical coordinates (latitude/longitude) of the location, if there is any.
     * In order to avoid unnecessary use of non-standard characters and space, the following standard presentation is used:<br>
     * 0000lat 00000long<br>
     * (lat - Latitude: N or S ; long – Longitude: W or E, only one digit, capital letter)<br>
     * Where the last two rightmost digits refer to minutes and the first two or three digits refer to the degrees for latitude and longitude respectively. In addition, you must specify N or S for latitude and W or E for longitude, as appropriate.
     *
     * @return the location's latitude
     */
    String getLatitude();

    void setLatitude(String latitude);

    /**
     * This contains the geographical coordinates (latitude/longitude) of the location, if there is any.
     * In order to avoid unnecessary use of non-standard characters and space, the following standard presentation is used:<br>
     * 0000lat 00000long<br>
     * (lat - Latitude: N or S ; long – Longitude: W or E, only one digit, capital letter)<br>
     * Where the last two rightmost digits refer to minutes and the first two or three digits refer to the degrees for latitude and longitude respectively. In addition, you must specify N or S for latitude and W or E for longitude, as appropriate.
     *
     * @return the location's longitude
     */
    String getLongitude();

    void setLongitude(String longitude);

    /**
     * @return a list {@link LocationFunction}s
     */
    List<LocationFunction> getFunctions();

    void setFunctions(List<LocationFunction> functions);
}
