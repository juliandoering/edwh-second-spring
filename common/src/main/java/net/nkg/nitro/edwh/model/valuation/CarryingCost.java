package net.nkg.nitro.edwh.model.valuation;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ValuationReferenceData;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

public interface CarryingCost extends ValuationReferenceData {

    /**
     * The interest rate in the given time span.
     *
     * @return BigDecimal
     */
    BigDecimal getInterestRate();

    void setInterestRate(BigDecimal interestRate);

    /**
     * The insurance rate for the lot.
     *
     * @return BigDecimal
     */
    BigDecimal getInsuranceRate();

    void setInsuranceRate(BigDecimal insuranceRate);

    /**
     * The monthly cost of the warehousing rate
     *
     * @return BigDecimal
     */
    BigDecimal getWarehousingRate();

    void setWarehousingRate(BigDecimal warehousingRate);

    /**
     * The currency of the carrying cost.
     *
     * @return CurrencyUnit
     */
    CurrencyUnit getCurrency();

    void setCurrency(CurrencyUnit currency);

    /**
     * The weight unit of the stored lot.
     *
     * @return QuantityUnit
     */
    QuantityUnit getWeightUnit();

    void setWeightUnit(QuantityUnit weightUnit);
}
