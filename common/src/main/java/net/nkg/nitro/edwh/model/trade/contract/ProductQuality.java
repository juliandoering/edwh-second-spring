package net.nkg.nitro.edwh.model.trade.contract;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.quality.CoffeeQuality;
import net.nkg.nitro.edwh.model.quality.ProductCertificationProgram;

/**
 * The specific details of the commodity are entered in this section, for example the product code, packaging, country of
 * origin, crop year, and product description.
 */
public interface ProductQuality extends BaseModel {

    /**
     * This allows the user to select the appropriate product code from the list of US Harmonized Product Codes.
     */
    CoffeeQuality getProductCode();

    void setProductCode(CoffeeQuality productCode);

    /**
     * The user selects the type of packaging in which the commodity will be packed, from an industry standard list.
     */
    Packing getPackingType();

    void setPackingType(Packing packingType);

    /**
     * The year(s) in which the commodity was harvested is entered here.
     */
    String getCropYear();

    void setCropYear(String cropYear);

    /**
     * This section is used to provide a detailed technical description of the commodity, with details such as moisture content,
     * grade and roast, applying in the context of coffee.
     */
    List<ProductDescription> getProductDescriptions();

    void setProductDescriptions(List<ProductDescription> productDescriptions);

    /**
     * Identification of a related concept through identification marks. For example, identification of a package or
     * identification of a piece of equipment.<p/>
     * Example: 123-RTF.
     */
    List<MarkOrNumber> getMarkOrNumbers();

    void setMarkOrNumbers(List<MarkOrNumber> markOrNumbers);

    /**
     * Product certifications of the {@link CoffeeQuality}.
     */
    List<ProductCertificationProgram> getCertifications();

    void setCertifications(List<ProductCertificationProgram> certifications);

}
