package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.hedging.FutureMarket;
import net.nkg.nitro.edwh.model.hedging.FutureMonth;

/**
 * The Futures ID describes the market, month and year in which the commodity is destined for sale. 
 */
public interface FuturesId extends BaseModel {

    /**
     * The futures market is the particular futures market in which the commodity will be sold and has an industry specific list.
     */
    FutureMarket getFuturesMarket();

    void setFuturesMarket(FutureMarket futuresMarket);

    /**
     * The futures month is the month in which the commodity will be sold, and has an industry specific list.
     */
    FutureMonth getFuturesMonth();

    void setFuturesMonth(FutureMonth futureMonth);

    /**
     * The futures year is the year in which the commodity will be sold, and is a 2 digit field. The user may select the year from a list.<p/>
     * This is the last two digits of the year.
     */
    Integer getFuturesYear();

    void setFuturesYear(Integer futureYear);
}
