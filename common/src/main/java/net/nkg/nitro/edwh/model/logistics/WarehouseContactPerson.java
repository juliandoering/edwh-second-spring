package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Individual;

public interface WarehouseContactPerson extends BaseModel {

    /**
     * The role the individual has for the warehouse.
     * @return
     */
    String getRole();

    void setRole(String role);

    /**
     * The individual listed as a contact for the warehouse.
     * @return
     */
    Individual getIndividual();

    void setIndividual(Individual individual);
}
