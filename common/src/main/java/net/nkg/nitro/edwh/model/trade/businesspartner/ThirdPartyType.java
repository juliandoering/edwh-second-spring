package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Interface to get/set what the business is doing.
 * As of [DWH-40](Define business third party) these are:
* <ul>
* <li>SERVICE_BUYER</li>
* <li>SERVICE_SELLER</li>
* <li>COFFEE_BUYER</li>
* <li>COFFEE_SELLER</li>
* <li>GOODS_BUYER</li>
* <li>GOODS_SELLER</li>
* </ul>
* */
public interface ThirdPartyType extends ReferenceData {

    void setCategory(String category);

    String getCategory();
}
