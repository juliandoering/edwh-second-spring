package net.nkg.nitro.edwh.model.hedging;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ReferenceData;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

/**
 * The futures market is the particular futures market in which the commodity will be sold and has an industry specific list.
 */
public interface FutureMarket extends ReferenceData {

    /**
     * The futures market size of a lot.
     * @return
     */
    BigDecimal getLotSize();

    void setLotSize(BigDecimal lotSize);

    /**
     * The futures market quantity unit of a lot size.
     * @return
     */
    QuantityUnit getLotSizeUnit();

    void setLotSizeUnit(QuantityUnit lotSizeUnit);

    /**
     * The futures market count of trading months.
     * @return
     */
    BigDecimal getTradingMonths();

    void setTradingMonths(BigDecimal tradingMonths);

    /**
     * The futures market trading price interval.
     * @return
     */
    BigDecimal getPriceInterval();

    void setPriceInterval(BigDecimal priceInterval);

    /**
     * The futures market trading days before end (Working days following last trading day to end of delivery month).
     * @return
     */
    int getDaysBeforeEnd();

    void setDaysBeforeEnd(int daysBeforeEnd);

    /**
     * The first notice day has different rules for the calculation for London and New York.
     * @return
     */
    boolean isFirstNoticeDayLondon();

    void setFirstNoticeDayLondon(boolean firstNoticeDayLondon);
}
