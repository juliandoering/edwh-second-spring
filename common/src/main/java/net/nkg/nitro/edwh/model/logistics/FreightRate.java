package net.nkg.nitro.edwh.model.logistics;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.trade.contract.shippinginstruction.ContainerMovement;
import net.nkg.nitro.edwh.model.trade.contract.shippinginstruction.ContainerSize;

/**
 * Freight Rates MD (mainly ocean freights) available to all group companies and negociated by ICL.
 */
public interface FreightRate extends BaseModel {

    /**
     * The rate for the defined route.
     * @return
     */
    BigDecimal getRate();

    void setRate(BigDecimal rate);

    /**
     * Costs of the inland transports after arriving at the destination port.
     * @return
     */
    BigDecimal getInlandCost();

    void setInlandCost(BigDecimal inlandCost);

    /**
     * Indicates if the inland costs are included in the related rates.
     * @return
     */
    boolean getInlandCostIncluded();

    void setInlandCostIncluded(boolean inlandCostIncluded);

    /**
     * The route of cargo after destination port. 
     * @return
     */
    String getInlandTransport();

    void setInlandTransport(String inlandTransport);

    /**
     * Transit time between port of shipment and port of destination in days.
     * @return
     */
    Integer getTransitTime();

    void setTransitTime(Integer transitTime);

    /**
     * Terminal Handling Charges.
     * @return
     */
    BigDecimal getThc();

    void setThc(BigDecimal thc);

    /**
     * Flag to indicate that terminal handling charges are included.
     * @return
     */
    boolean getThcIncluded();

    void setThcIncluded(boolean thcIncluded);

    /**
     * Flag if the shipping rate is preferred.
     * @return
     */
    boolean getPreference();

    void setPreference(boolean preference);

    /**
     * Additional comment of the rate
     * @return
     */
    String getComment();

    void setComment(String comment);

    /**
     * Validity date this rate is valid from.
     * @return
     */
    LocalDate getValidFrom();

    void setValidFrom(LocalDate validFrom);

    /**
     * Validity date until this rate is valid.
     * @return
     */
    LocalDate getValidTo();

    void setValidTo(LocalDate validTo);

    /**
     * Standard container sizes, e.g. 20ft, 40ft.
     * @return
     */
    ContainerSize getContainerSize();

    void setContainerSize(ContainerSize containerSize);

    /**
     * Container handover points.
     * @return
     */
    ContainerMovement getContainerMovement();

    void setContainerMovement(ContainerMovement containerMovement);

    /**
     * Currency Unit of the inland cost value.
     * @return
     */
    CurrencyUnit getInlandCostCurrency();

    void setInlandCostCurrency(CurrencyUnit inlandCostCurrency);

    /**
     * Currency Unit for this rate.
     * @return
     */
    CurrencyUnit getOceanFreightCurrency();

    void setOceanFreightCurrency(CurrencyUnit oceanFreightCurrency);

    /**
     * Unit of the THC value.
     * @return
     */
    CurrencyUnit getThcCurrency();

    void setThcCurrency(CurrencyUnit thcCurrency);
}
