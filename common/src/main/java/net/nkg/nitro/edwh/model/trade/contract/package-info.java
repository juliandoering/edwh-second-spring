/**
 * This includes interfaces to describe the logical model of a coffee trade contract.
 */
package net.nkg.nitro.edwh.model.trade.contract;
