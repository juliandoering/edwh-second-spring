package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Offers addressees and their email addresses to be notified if new {@link FreightRateTradeNotificationMail}s have been uploaded or modified.
 */
public interface FreightRateTradeNotificationMail extends BaseModel {

    String getContactPersonName();

    void setContactPersonName(String contactPersonName);

    String getEmailAddress();

    void setEmailAddress(String emailAddress);
}
