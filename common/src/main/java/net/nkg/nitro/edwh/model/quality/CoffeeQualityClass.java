package net.nkg.nitro.edwh.model.quality;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;

import java.util.List;

/**
 * Coffee quality class and its origin type and grade conversion factors.
 *
 * @author sl
 */
public interface CoffeeQualityClass extends BaseModel {

    // fixme   this interface hides the identity of the coffee quality. it is not possible to write a good error message or to used debug one of the hundreds of qualities that are processed.
    // todo     add the name or something that made tracing easier

    /**
     * Get the origin type of the Coffee Quality.
     * <p/>
     * An origin type is for example:
     * <ul>
     * <li>Single Origin</li>
     * <li>Basket</li>
     * <li>Blend</li>
     * </li>Unspecified</li>
     * </ul>
     * Some origin types are hard wired as they are used for business logic, see the constants.
     *
     * @return
     */
    CoffeeQualityOriginType getOriginType();

    /**
     * Set the origin type of the Coffee Quality.
     * <p/>
     * An origin type is for example:
     * <ul>
     * <li>Single Origin</li>
     * <li>Basket</li>
     * <li>Blend</li>
     * </li>Unspecified</li>
     * </ul>
     *
     * @param originType
     */
    void setOriginType(CoffeeQualityOriginType originType);

    /**
     * Get the origin of the Coffee Quality
     *
     * @return
     */
    List<Location> getOrigins();

    /**
     * Set the Origins of a Coffee Quality.
     * Each Value should be Master Data.
     *
     * @param origins
     */
    void setOrigins(List<Location> origins);

    void setConversionToGradedDistribution(List<CoffeeQualityPercentage> conversionToGradedDistribution);

    List<CoffeeQualityPercentage> getConversionToGradedDistribution();

}
