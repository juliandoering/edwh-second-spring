package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.compliance.ComplianceRatingType;

/**
 * Compliance rating (for third parties).
 * <p/>
 * Introduced in DWH-40.
 * @author nf
 *
 */
public interface ComplianceRating extends BaseModel {

    // Rating: {one of}

    /**
     *
     * @return {one of} APPROVED/DISAPPROVED
     */
    ComplianceRatingType getComplianceRatingType();

    void setComplianceRatingType(ComplianceRatingType complianceRatingType);

    // Validity Period from: Date
    LocalDate getDateOfRating();

    void setDateOfRating(LocalDate fromDate);
}
