package net.nkg.nitro.edwh.authorization.check;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;
import net.nkg.nitro.edwh.model.profile.UserRole.EdwhUserRole;

public class RoleAuthorization<T> implements Authorization<T> {

    private CurrentUserProvider userProvider;
    private EdwhUserRole userRole;
    private RestOperation[] allowedOperations;

    /**
     * @param userProvider provider for the current user (mandatory)
     * @param userRole role the current user should have (mandatory)
     * @param allowedOperations restriction to certain operations, if not provided or empty any operation is allowed.
     */
    public RoleAuthorization(CurrentUserProvider userProvider, EdwhUserRole userRole, RestOperation... allowedOperations) {
        this.userProvider = userProvider;
        this.userRole = userRole;
        this.allowedOperations = allowedOperations;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        return this.userProvider.isRoleAssigned(userRole) && isAllowedOperation(operation);
    }

    private boolean isAllowedOperation(RestOperation operation) {
        if (allowedOperations == null || allowedOperations.length == 0) {
            return true;
        }
        for (RestOperation allowedOperation : allowedOperations) {
            if (allowedOperation == operation) {
                return true;
            }
        }
        return false;
    }
}
