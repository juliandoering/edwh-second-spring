package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

/**
 * "Store the particular commercial limit information we have with Business Third Parties
 * so controls and risk assessment can be done".
 * 
 * As discussed with LCR at 19.04.'17, 14:34, the BusinessKey is (BusinessThirdParty + GroupCompany).
 * Only the latest and newest information is relevant.
 * That only the latest version is show is part of the versioning and/or queries.
 * 
 * @author niklas
 *
 */
public interface BusinessThirdPartyCommercialLimit extends BaseModel {

    /**
     * Set the date of when the Commercial Limit was 'signed' or created.
     * As discussed with LCR, there may occur cases when for short time periods changes occur.
     * The newerly signed is the valid one for that time period then.
     * @param date
     */
    public void setSignatureDate(LocalDate date);

    public LocalDate getSignatureDate();

    /**
     * The Group Company the commercial Limit is for.
     * @return
     */
    GroupCompany getGroupCompany();

    /**
     * Set the group company for which the limit is.
     * @param groupCompany
     */
    void setGroupCompany(GroupCompany groupCompany);

    /**
     * The Business Third Party the commercial limit is assigned to.
     * @return
     */
    BusinessThirdParty getBusinessThirdParty();

    void setBusinessThirdParty(BusinessThirdParty businessThirdParty);

    /**
     * In case that the paymentterm involves a commercial credit, then this field is set, otherwise <b>null</b>.
     * Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable).
     * Optional.
     * @return a commercial credit term if applicable or <b>null</b>
     */
    CommercialCreditTerms getCommercialCreditTerms();

    /**
     * In case that the paymentterm involves a commercial credit, then this field is set, otherwise <b>null</b>.
     */
    void setCommercialCreditTerms(CommercialCreditTerms commercialCreditTerms);

    /**
     * Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable).
     * Validity period should be Business Plan period in general.  Shorter if so required.
     * @return
     */
    InternalCreditLimit getInternalCreditLimit();

    void setInternalCreditLimit(InternalCreditLimit limit);

    /**
     * Only needed when an Internal Credit Limit is set.
     * Optional.
     * @return
     */
    List<Collateral> getCollaterals();

    void setCollaterals(List<Collateral> collaterals);

    /**
     * Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable).
     * Validity period should be Business Plan period in general.  Shorter if so required.
     * @return
     */
    MaximumOpenPosition getMaximumOpenPosition();

    void setMaximumOpenPosition(MaximumOpenPosition maximumOpenPosition);

    /**
     * Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable).
     * Validity period should be Business Plan period in general.  Shorter if so required.
     * @return
     */
    MaximumOpenPricedPosition getMaximumOpenPricedPosition();

    void setMaximumOpenPricedPosition(MaximumOpenPricedPosition maximumOpenPricedPosition);

    Budget getBudget();

    void setBudget(Budget budget);
}
