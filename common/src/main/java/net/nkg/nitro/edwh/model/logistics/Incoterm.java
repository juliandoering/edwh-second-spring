package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * The type of contract in this case is identified from a list which are standard in the coffee industry, and the user will select from the following code list.
 */
public interface Incoterm extends ReferenceData {

}
