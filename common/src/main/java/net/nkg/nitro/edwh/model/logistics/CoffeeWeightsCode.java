package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Description of method used to weigh goods. The user will select the method of weighing from an industry standard list.
 */
public interface CoffeeWeightsCode extends ReferenceData {

}
