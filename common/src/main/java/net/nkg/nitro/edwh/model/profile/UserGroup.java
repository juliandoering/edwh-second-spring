package net.nkg.nitro.edwh.model.profile;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.profile.UserRole.EdwhUserRole;

public interface UserGroup extends BaseModel {

    /**
     * @return the unique name of this group.
     */
    String getGroupName();

    void setGroupName(String name);

    /**
     * @return the assigned roles of this user group.
     */
    List<UserRole> getAssignedRoles();

    void setAssignedRoles(List<UserRole> roles);

    default boolean isAdmin() {
        return getAssignedRoles() != null
            && getAssignedRoles().stream()
                .anyMatch(UserRole::isAdmin);
    }

    default boolean isRoleAssigned(EdwhUserRole edwhRole) {
        return getAssignedRoles() != null && edwhRole != null
            && getAssignedRoles().stream()
                .anyMatch(role -> role.getRole().equalsIgnoreCase(edwhRole.getRoleName()));
    }
}
