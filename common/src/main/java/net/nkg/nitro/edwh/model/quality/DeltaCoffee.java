package net.nkg.nitro.edwh.model.quality;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

/**
 * A coffee quality traded as delta coffee by a group company with attributes assigned from the given group company.
 * @author niklas
 *
 */
public interface DeltaCoffee extends BaseModel {

    /**
     * Get the group company that is trading this delta coffee with the respective delta factor.
     * @return
     */
    GroupCompany getTradingGroupCompany();

    /**
     * Set the group company that is trading this delta coffee with the respective delta factor.
     * @param tradingGroupCompany
     */
    void setTradingGroupCompany(GroupCompany tradingGroupCompany);

    /**
     * Get the coffee quality that this delta coffee is representing.
     * @return
     */
    CoffeeQuality getCoffeeQuality();

    /**
     * Set the coffee quality that this delta coffee shoulf represent.
     * @param coffeeQuality
     */
    void setCoffeeQuality(CoffeeQuality coffeeQuality);

    /**
     * Get the type of delta coffee (high, low,...)
     * @return
     */
    DeltaCoffeeType getDeltaType();

    /**
     * Set the type of delta coffee (high, low,...)
     * @param deltaType
     */
    void setDeltaType(DeltaCoffeeType deltaType);

    /**
     * Get the delta coffee factor (0-99.9%)
     * @return
     */
    BigDecimal getDeltaFactor();

    /**
     * Set the delta coffee factor (0-99.9%)
     * @param deltaFactor
     */
    void setDeltaFactor(BigDecimal deltaFactor);

}
