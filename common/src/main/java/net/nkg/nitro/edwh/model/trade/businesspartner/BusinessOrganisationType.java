package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.ReferenceData;

import java.util.function.Predicate;

/**
 * Type of business of third party companies.
 * @author nf
 *
 */
public interface BusinessOrganisationType extends ReferenceData {
    static final String SOLE_PROPRIETORSHIP_CODE = "SP";

    enum ExpectedRange {
        NONE(0),
        ONE(1),
        ONE_OR_MORE(1, Integer.MAX_VALUE),
        ANY_NUMBER(0, Integer.MAX_VALUE),
        ;

        private Predicate<Integer> checkSize;

        ExpectedRange(int expectedSize) {
            this.checkSize = size -> expectedSize == size;
        }

        ExpectedRange(int expectedLowest, int expectedHighest) {
            this.checkSize = size -> expectedLowest <= size && expectedHighest >= size;
        }

        public boolean isInRange(int size) {
            return this.checkSize.test(size);
        }
    }

    String getCategory();

    void setCategory(String category);

    ExpectedRange getCheckOwner();

    void setCheckOwner(ExpectedRange checkOwner);

    ExpectedRange getCheckUltimateBeneficiaryOwner();

    void setCheckUltimateBeneficiaryOwner(ExpectedRange checkUltimateBeneficiaryOwner);

    ExpectedRange getCheckRepresentative();

    void setCheckRepresentative(ExpectedRange checkRepresentative);

    ExpectedRange getCheckContactPerson();

    void setCheckContactPerson(ExpectedRange checkContactPerson);

    default boolean isSoleProprietorship() {
        return SOLE_PROPRIETORSHIP_CODE.equalsIgnoreCase(getCode());
    }
}
