package net.nkg.nitro.edwh.authorization.check;

import java.util.function.Function;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.profile.Userprofile;
import net.nkg.nitro.edwh.model.trade.contract.Contract;
import net.nkg.nitro.edwh.model.trade.contract.ContractRoleEnum;
import net.nkg.nitro.edwh.model.trade.contract.Party;

public class ContractPartyAuthorization<T> implements Authorization<T> {

    private CurrentUserProvider userProvider;
    private Function<T, Contract> contractAccessor;

    public ContractPartyAuthorization(CurrentUserProvider userProvider, Function<T, Contract> contractAccessor) {
        this.userProvider = userProvider;
        this.contractAccessor = contractAccessor;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        Contract contract = contractAccessor.apply(model);

        switch (operation) {
            case CREATE:
            case UPDATE:
                return hasValidCreateOrUpdateRole(contract) && isBuyerOrSellerOrAgentOfContract(contract);
            case DELETE:
                return isBuyerOrSellerOrAgentOfContract(contract);
            case RETRIEVE:
                return isBuyerOrSellerOrAgentOfContract(contract);
            default:
                return false;
        }
    }

    /**
     * Returns true if the given user(profile) is involved into the contract either as buyer or seller or agent.
     * @param contract
     * @return true if the user is associated with the groupcompany as buyer, seller or agent
     */
    public boolean isBuyerOrSellerOrAgentOfContract(Contract contract) {
        if (contract == null) {
            return false;
        }

        Userprofile currentUser = userProvider.getCurrentUser();
        GroupCompany agent = getGroupCompanyFromParty(contract.getAgent());

        return currentUser.isGroupCompanyAssigned(agent) || isBuyerOrSellerOfContract(contract);
    }

    /**
     * Returns true if the given user(profile) is involved into the contract either as buyer or seller.
     * @param contract the contract to check.
     * @return true if the user is buyer or seller, else false.
     */
    public boolean isBuyerOrSellerOfContract(Contract contract) {
        if (contract == null) {
            return false;
        }

        Userprofile currentUser = userProvider.getCurrentUser();
        GroupCompany buyer = getGroupCompanyFromParty(contract.getBuyer());
        GroupCompany seller = getGroupCompanyFromParty(contract.getSeller());

        return currentUser.isGroupCompanyAssigned(buyer)
            || currentUser.isGroupCompanyAssigned(seller);
    }

    /**
     * Determines whether the uploading user occupies roles in the contract that allow him to upload the contract.
     * Forbidden combinations are buyer + shipper.
     * @param contract the contract to be uploaded.
     * @return true if the user is allowed to upload the contract, false otherwise
     */
    public boolean hasValidCreateOrUpdateRole(Contract contract) {
        Userprofile currentUser = userProvider.getCurrentUser();

        boolean isShipper = currentUser.isGroupCompanyAssigned(getGroupCompanyFromParty(contract.getShipper()));
        boolean isBuyer = currentUser.isGroupCompanyAssigned(getGroupCompanyFromParty(contract.getBuyer()));
        boolean isAgent = currentUser.isGroupCompanyAssigned(getGroupCompanyFromParty(contract.getAgent()));
        boolean isInterCompanyContract = getGroupCompanyFromParty(contract.getBuyer()) != null && getGroupCompanyFromParty(contract.getSeller()) != null;

        // user cannot be buyer and shipper
        return !(isShipper && isBuyer) && !(isAgent && isInterCompanyContract);
    }

    /**
     * Determines which contract party is the uploading party.
     * @param contract the contract that is being uploaded.
     * @return the ContractRole specifying the uploading party
     *     or {@code null} if the uploader occupies no role in the contract.
     */
    public ContractRoleEnum determineUploadingParty(Contract contract) {
        Userprofile currentUser = userProvider.getCurrentUser();

        GroupCompany seller = getGroupCompanyFromParty(contract.getSeller());
        if (currentUser.isGroupCompanyAssigned(seller)) {
            return ContractRoleEnum.SELLER;
        }

        GroupCompany buyer = getGroupCompanyFromParty(contract.getBuyer());
        if (currentUser.isGroupCompanyAssigned(buyer)) {
            return ContractRoleEnum.BUYER;
        }

        GroupCompany agent = getGroupCompanyFromParty(contract.getAgent());
        if (currentUser.isGroupCompanyAssigned(agent)) {
            return ContractRoleEnum.AGENT;
        }

        GroupCompany shipper = getGroupCompanyFromParty(contract.getShipper());
        if (currentUser.isGroupCompanyAssigned(shipper)) {
            return ContractRoleEnum.SHIPPER;
        }

        return null;
    }

    private GroupCompany getGroupCompanyFromParty(Party party) {
        return party != null ? party.getCompany() : null;
    }
}
