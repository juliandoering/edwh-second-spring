package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Interface to value objects and entities of shipping instruction's "Any Other Instruction" (formerly: additional documents)
 * as described first in [DWH-82] and amended by [DWH-492].
 * <p>
 * Any Other Instruction are just plain String instructions of misc. stuff added to 
 * shipping instructions (see {@link ShippingInstruction}) of a contract.
 * The type of attachments differ from country to country because e.g. of legal reasons.
 * @author nf
 *
 */
public interface AnyOtherInstruction extends BaseModel {

    /**
     * Get the shipping instruction id to which this document belongs to.
     * @return
     */
    Long getShippingInstructionId();

    /**
     * Set the shipping instruction id to which this document belongs to.
     * @param shippinginstruction
     */
    void setShippingInstructionId(Long shippinginstruction);

    /**
     * Set the instruction's content.
     * @param anyOtherInstruction
     */
    void setAnyOtherInstruction(String anyOtherInstruction);

    /**
     * Get the content of the instruction.
     * @return
     */
    String getAnyOtherInstruction();

}
