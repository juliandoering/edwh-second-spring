package net.nkg.nitro.edwh.model.risk;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface CoffeeRiskPositionRowMapping extends BaseModel {

    String getRowIdentifier();

    void setRowIdentifier(String rowIdentifier);

    String getDescription();

    void setDescription(String description);

}
