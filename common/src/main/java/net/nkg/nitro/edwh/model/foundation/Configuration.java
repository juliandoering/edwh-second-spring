package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This class is intended to offer mapping configuration for special purposes.
 * It is used to i.e. identify currencies for ETL processes.
 */
public interface Configuration extends BaseModel {

    String getCode();

    void setCode(String code);

    String getConfigValue();

    void setConfigValue(String configValue);

    String getDescription();

    void setDescription(String description);
}
