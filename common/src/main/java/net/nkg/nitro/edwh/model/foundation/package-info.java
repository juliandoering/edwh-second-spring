/**
 * This package includes models which are used by other master data 
 * and are rarely changed.
 */
package net.nkg.nitro.edwh.model.foundation;
