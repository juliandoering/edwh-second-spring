package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;

/**
 * The business partner can be either a group company or a business third party but not both at the same time.
 * @author elv
 */
public interface BusinessPartner extends BaseModel {

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);

    BusinessThirdParty getBusinessThirdParty();

    void setBusinessThirdParty(BusinessThirdParty businessThirdParty);
}
