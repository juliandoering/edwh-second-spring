package net.nkg.nitro.edwh.authorization.check;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;
import net.nkg.nitro.edwh.model.profile.UserRole.EdwhUserRole;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;

public class SoleProprietorshipAuthorization implements Authorization<BusinessThirdParty> {

    private CurrentUserProvider userProvider;

    public SoleProprietorshipAuthorization(CurrentUserProvider userProvider) {
        this.userProvider = userProvider;
    }

    @Override
    public boolean isAuthorized(BusinessThirdParty model, RestOperation operation) {
        return this.userProvider.isRoleAssigned(EdwhUserRole.EDWH_TECHNICAL)
            && model.isSoleProprietorship() && operation == RestOperation.CREATE;
    }
}
