package net.nkg.nitro.edwh.model.risk;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

public interface CoffeeRiskPosition extends AnalyticalData {

    String getRiskPositionType();

    void setRiskPositionType(String riskPositionType);

    String getGroupCompanyName();

    void setGroupCompanyName(String groupCompanyName);

    String getCoffeeType();

    void setCoffeeType(String coffeeType);

    LocalDate getRiskPositionDate();

    void setRiskPositionDate(LocalDate riskPositionDate);

    String getForwardPositionGroup();

    void setForwardPositionGroup(String forwardPositionGroup);

    String getForwardPosition();

    void setForwardPosition(String forwardPosition);

    Integer getForwardPositionOrdinal();

    void setForwardPositionOrdinal(Integer forwardPositionOrdinal);

    BigDecimal getRiskPositionValue();

    void setRiskPositionValue(BigDecimal riskPositionValue);

    BigDecimal getWeightUnit();

    void setWeightUnit(BigDecimal weightUnit);

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);

    CoffeeRiskPositionRowMapping getRowIdentifier();

    void setRowIdentifier(CoffeeRiskPositionRowMapping rowIdentifier);

}
