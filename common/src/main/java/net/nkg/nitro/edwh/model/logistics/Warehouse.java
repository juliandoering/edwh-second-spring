package net.nkg.nitro.edwh.model.logistics;

import java.math.BigDecimal;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.BusinessPartner;
import net.nkg.nitro.edwh.model.foundation.Contact;
import net.nkg.nitro.edwh.model.foundation.PostalAddress;

/**
 * A warehouse storing coffee for processing or further transport.
 */
public interface Warehouse extends BaseModel {

    /**
     * Ibero Insurance Warehouse code.
     * @return
     */
    Integer getIisCode();

    void setIisCode(Integer iisCode);

    /**
     * Max insured value, currency and validity date.
     * @return
     */
    InsuranceLimit getInsuranceLimit();

    void setInsuranceLimit(InsuranceLimit insuranceLimit);

    /**
     * The warehouse owner.
     * @return
     */
    BusinessPartner getOwner();

    void setOwner(BusinessPartner owner);

    /**
     * Individual name of warehouse.
     * @return
     */
    String getName();

    void setName(String name);

    /**
     * Address details, ZIP or Area Code, Location code from UN-Locode.
     * @return
     */
    PostalAddress getAddress();

    void setAddress(PostalAddress address);

    /**
     * @return the latitude of the warehouses GPS coordinates (decimal degrees).
     */
    BigDecimal getLatitudeDecimal();

    void setLatitudeDecimal(BigDecimal latitudeDecimal);

    /**
     * @return the longitude of the warehouses GPS coordinates (decimal degrees).
     */
    BigDecimal getLongitudeDecimal();

    void setLongitudeDecimal(BigDecimal longitudeDecimal);

    /**
     * @return the comment of the warehouse.
     */
    String getComment();

    void setComment(String comment);

    /**
     * Get the contact persons of the warehouse.
     * @return
     */
    List<WarehouseContactPerson> getContacts();

    void setContacts(List<WarehouseContactPerson> contacts);

    /**
     * Get the contact data of the warehouse.
     * @return
     */
    List<Contact> getContactData();

    void setContactData(List<Contact> contactData);

    /**
     * @return Whether the warehouse is active or not.
     */
    Boolean getActive();

    void setActive(Boolean active);
}
