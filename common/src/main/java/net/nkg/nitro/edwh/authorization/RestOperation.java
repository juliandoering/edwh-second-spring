package net.nkg.nitro.edwh.authorization;

public enum RestOperation {
    CREATE,
    RETRIEVE,
    UPDATE,
    DELETE;

    private String unauthorizedExceptionMessage;

    static {
        CREATE.unauthorizedExceptionMessage = "authorization.data.post";
        RETRIEVE.unauthorizedExceptionMessage = "authorization.data.get";
        UPDATE.unauthorizedExceptionMessage = "authorization.data.put";
        DELETE.unauthorizedExceptionMessage = "authorization.data.delete";
    }

    public String getUnauthorizedExceptionMessageKey() {
        return unauthorizedExceptionMessage;
    }
}
