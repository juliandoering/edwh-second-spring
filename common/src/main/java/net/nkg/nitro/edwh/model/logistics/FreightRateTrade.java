package net.nkg.nitro.edwh.model.logistics;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Freight Rates MD (mainly ocean freights) available to all group companies and negociated by ICL.
 */
public interface FreightRateTrade extends BaseModel {

    /**
     * Unique name of this.
     * @return
     */
    TradeName getTradeName();

    void setTradeName(TradeName tradeName);

    /**
     * All groups of port of loadings.
     * @return
     */
    List<FreightRateLoading> getFreightRateLoading();

    void setFreightRateLoading(List<FreightRateLoading> freightRateLoading);

}
