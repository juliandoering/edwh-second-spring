package net.nkg.nitro.edwh.model.trade;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Interface specifying trade types.
 * <p>
 * 
 * Used for contracts and its related futures market transactions: 
 * <li>Sales</li>
 * <li>Purchase</li> 
 * 
 * Introduced in [DWH-206].
 */
public interface TradeType extends ReferenceData {

}
