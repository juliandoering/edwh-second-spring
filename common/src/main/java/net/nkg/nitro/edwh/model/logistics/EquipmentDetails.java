package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Details on Containers in the shipment.
 */
public interface EquipmentDetails extends BaseModel {
    /**
     * Sequential number
     */
    Integer getLineNumber();

    void setLineNumber(Integer lineNumber);

    /**
     * The number of the container.
     */
    String getContainerNumber();

    void setContainerNumber(String containerNumber);

    /**
     * Either "Full" or "Empty"
     */
    String getLoadType();

    void setLoadType(String loadType);

    /**
     * Type of container (can contain any ISO equipment type code)
     */
    String getContainerType();

    void setContainerType(String containerType);
}
