package net.nkg.nitro.edwh.model.trade.contract;

import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.logistics.Incoterm;
import net.nkg.nitro.edwh.model.trade.CoffeeContractType;
import net.nkg.nitro.edwh.model.trade.PriceFixationType;

/**
 * A model for contracts roughly based on the xml presented in the 'Contract terms and conditions' of the
 * 'Green coffee Association' that is copyrighted by Bolero International Ltd.
 * <p/>
 * The copyright terms of the GCA document state:
 * <p/>
 *  <i>"Copyright Bolero International Limited 2000 Bolero International Limited hereby grants
 *  to any person using or considering the use of the information in this publication a
 *  non-exclusive, royalty-free, worldwide, and revocable license
 *  <ul>
 *  <li>(1) to use and
 *  implement that information, including document type definitions and other specifications, and </li>
 *  <li>(2) to copy all or part of this
 *  publication in any form, provided that all such copies clearly retain the foregoing copyright notice. </li>
 *  </ul>
 *  Bolero International Limited makes no warranty or representation of any kind regarding these specifications or the right
 *  of any person to implement them free of infringement claims by others in accordance with the above licence.
 *  The above licence shall be void if the licensee alleges against Bolero International Limited that the information in this
 *  publication infringes a patent held or claimed by the licensee or a company controlled by or under common control with the licensee"</i>
 *
 * URL: {@link @http://www.greencoffeeassociation.org/images/uploads/resources/PROFESSIONAL_RESOURCES_-_Contract_Terms_Conditions.pdf}
 */
public interface Contract extends BaseModel {

    /**
     * The date upon which this document is issued.
     */
    LocalDate getDateOfIssue();

    void setDateOfIssue(LocalDate dateOfIssue);

    /**
     * The latest upload date of this contract.
     */
    LocalDate getDateOfUpload();

    void setDateOfUpload(LocalDate dateOfUpload);

    /**
     * The type of contract in this case is identified from a list which are standard in the coffee industry.
     */
    Incoterm getContractType();

    void setContractType(Incoterm incoterm);

    /**
     * The sellers data and the seller contract ID.
     */
    Party getSeller();

    void setSeller(Party seller);

    /**
     * The buyers data and the buyers contract ID.
     */
    Party getBuyer();

    void setBuyer(Party buyer);

    /**
     * The agents data and the agents contract ID.
     */
    Party getAgent();

    void setAgent(Party agent);

    /**
     * The shipper and the shippers transaction reference.
     */
    Party getShipper();

    void setShipper(Party shipper);

    /**
     * This section is used to detail all the pertinent information to do with products or commodities being trade,
     * such as the commodity description, quantities, prices, characteristics etc.
     */
    Consignment getConsignment();

    void setConsignment(Consignment consignment);

    /**
     * This contains the instructional information for the coffee contract, such as the duties,
     * responsibilities and parameters for the variables such as price.
     */
    InstructionalInformation getInstructionalInformation();

    void setInstructionalInformation(InstructionalInformation instructionalInformation);

    /**
     * Information related to the routing and transportation of goods. This may include detail such as conveyance
     * identification (for example, the vessel name), the associated transportation route (for example, port of loading), etc.
     */
    RoutingSummary getRoutingSummary();

    void setRoutingSummary(RoutingSummary routingSummary);

    /**
     * Structured information regarding the financial settlement of a purchase. For example,
     * the number of days and method by which payment is due.
     */
    PaymentTerms getPaymentTerms();

    void setPaymentTerms(PaymentTerms paymentTerms);

    /**
     * Contractual requirements comprising of general terms and conditions and any specific non-standard clauses peculiar to
     * a business document. The general terms and conditions may be identified by a link to a location on a website or
     * to a document which holds the specific terms for a particular party.
     */
    TermsAndConditions getTermsAndConditions();

    void setTermsAndConditions(TermsAndConditions termsAndConditions);

    /**
     * This sections allows the user to record the amendments they are making in structured detail. Here the user will record
     * the date of the amendment, the field they amended and the version of the contract they amended.
     */
    List<Amendment> getAmendments();

    void setAmendments(List<Amendment> amendments);

    /**
     * This is a section intended for information that may not be catered for in the rest of the document. It must be noted that
     * use of this field will not allow automatic processing for the data concerned.
     * If the information is significant for the operation of the concerned transaction, and it is felt that the document should be
     * amended, please contact the boleroXML team.
     * {Rules applicable per document
     * . Documentary Credit Application
     * This is additional information that the applicant may provide to the bank.
     */
    List<AdditionalInformation> getAdditionalInformations();

    void setAdditionalInformations(List<AdditionalInformation> additionalInformations);

    /**
     * Set the type of contract that we are dealing with.
     * <p/>
     * Up to now a string of the following can be set:
     * <ul>
     * <li>'PTBF' - for price to be fixed</li>
     * <li>'DTBF' - For differential to be fixed</li>
     * <li>'PTF' - For price to follow</li>
     * <li>'Outright' - For outright contracts</li>
     * </ul>
     * ([DWH-151] Adjust pricing type and related attributes in Contract.)
     * @param contractPricingType: PriceFixationType
     */
    void setPriceFixationType(PriceFixationType contractPricingType);

    /**
     * Get the type of contract we are dealing with, see {@link @setContractTypeMD} or null if it has not been set.
     * @return PriceFixationType
     */
    PriceFixationType getPriceFixationType();

    /**
     * Set the type of coffee contract that we are dealing with.
     * <p/>
     * Up to now a string of the following can be set:
     * <ul>
     * <li>'ESCC' - European Standard Contract for Coffee</li>
     * <li>'GCA' - Green Coffee Association</li>
     * </ul>
     * @param contractPricingType: PriceFixationType
     */
    void setCoffeeContractType(CoffeeContractType coffeeContractType);

    /**
     * Get the type of coffee contract we are dealing with, see {@link @setCoffeeContractType} or null if it has not been set.
     * @return CoffeeContractType
     */
    CoffeeContractType getCoffeeContractType();

}
