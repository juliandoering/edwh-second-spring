package net.nkg.nitro.edwh.model.profile;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface UserRole extends BaseModel {

    public enum EdwhUserRole {
        EDWH_ADMIN("edwh_admin"),
        EDWH_TECHNICAL("edwh_technical"),
        EDWH_FINAL("edwh_final"),
        EDWH_FINAL_OCEAN_FRIGHT_RATE("edwh_final_ocean_freight_rate"),
        EDWH_COFFEE_RISK_POSITION_ANALYST("coffee_risk_position_analyst"),
        EDWH_COFFEE_RISK_POSITION_GROUP_COMPANY_UPLOADER("coffee_risk_position_group_company_uploader"),
        ;

        private String roleName;

        /**
         * Returns the enum type to a string or null if the string does
         * not match any known enum type.
         * The method is case insensitive
         * @param edwhUserRoleString
         * @return
         */
        public static EdwhUserRole getEdwhUserRole(String edwhUserRoleString) {
            if (edwhUserRoleString != null) {
                // check all possible enum values if they match
                for (EdwhUserRole edwhUserRoleVal : EdwhUserRole.values()) {
                    // test if the enum-value matches our given string
                    if (edwhUserRoleString.equalsIgnoreCase(edwhUserRoleVal.toString())) {
                        // we found the right enum-value
                        return edwhUserRoleVal;
                    }
                }
            }
            return null;
        }

        private EdwhUserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    void setRole(String rolename);

    public String getRole();

    void setAdmin(boolean isAdmin);

    public boolean isAdmin();
}
