package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;

/**
 * Wrapper interface to handle the role of the paying party, which can be {@link GroupCompany} or {@link BusinessThirdParty}.
 * Intentionally only one party (GroupCompany or BusinessThirdParty) should be set.
 * @author sl
 *
 */
public interface PayingParty extends BaseModel {

    /**
     * Paying party Group Company 
     * @return
     */
    GroupCompany getCompany();

    /**
     * Set paying party Group Company
     * @param company
     */
    void setCompany(GroupCompany company);

    /**
     * Paying party Business Third Party
     * @return
     */
    BusinessThirdParty getThirdParty();

    /**
     * Set Paying party Business Third Party
     * @param thirdParty
     */
    void setThirdParty(BusinessThirdParty thirdParty);
}
