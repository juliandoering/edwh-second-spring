package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.PaymentTerm;

/**
 * Terms of payment which specify a coded payment type with optional additional information.
 */
public interface StandardPaymentTerms extends BaseModel {

    /**
     * Code identifying the type of payment.ap/>
     * Example: CREDOC - Irrevocable d.c. - unconfirmed, period of time for payment unspecified
     */
    PaymentTerm getPaymentType();

    void setPaymentType(PaymentTerm paymentType);

    /**
     * This is used to provide further information on the chosen payment type.<p/>
     * Example: 90 days from date of Bill of Lading (for a Documentary Collection)
     */
    String getPaymentTypeDetails();

    void setPaymentTypeDetails(String paymentTypeDetails);
}
