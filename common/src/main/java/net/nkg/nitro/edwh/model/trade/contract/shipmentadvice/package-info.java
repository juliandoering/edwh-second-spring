/**
 * This package holds the shipment information of a {@link net.nkg.nitro.edwh.interfaces.Contract}.
 */
package net.nkg.nitro.edwh.model.trade.contract.shipmentadvice;
