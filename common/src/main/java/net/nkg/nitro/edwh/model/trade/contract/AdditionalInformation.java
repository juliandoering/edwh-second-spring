package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This is a section intended for information that may not be catered for in the rest of the document. It must be noted that
 * use of this field will not allow automatic processing for the data concerned.
 * If the information is significant for the operation of the concerned transaction, and it is felt that the document should be
 * amended, please contact the boleroXML team.
 */
public interface AdditionalInformation extends BaseModel {

    /**
     * A string or line of text containing alphanumeric characters.
     */
    String getLine();

    void setLine(String line);
}
