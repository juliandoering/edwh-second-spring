package net.nkg.nitro.edwh.authorization.check;

import java.util.function.Function;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.profile.Userprofile;

public class GroupCompanyAuthorization<T> implements Authorization<T> {

    private CurrentUserProvider userProvider;
    private Function<T, GroupCompany> groupCompanyAccessor;
    private RestOperation[] allowedOperations;

    public GroupCompanyAuthorization(CurrentUserProvider userProvider, Function<T, GroupCompany> groupCompanyAccessor, RestOperation... allowedOperations) {
        this.userProvider = userProvider;
        this.groupCompanyAccessor = groupCompanyAccessor;
        this.allowedOperations = allowedOperations;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        Userprofile user = userProvider.getCurrentUser();
        GroupCompany groupCompany = groupCompanyAccessor.apply(model);
        return user.isGroupCompanyAssigned(groupCompany) && isAllowedOperation(operation);
    }

    private boolean isAllowedOperation(RestOperation operation) {
        if (allowedOperations == null || allowedOperations.length == 0) {
            return true;
        }
        for (RestOperation allowedOperation : allowedOperations) {
            if (allowedOperation == operation) {
                return true;
            }
        }
        return false;
    }
}
