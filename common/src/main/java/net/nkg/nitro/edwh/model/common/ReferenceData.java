package net.nkg.nitro.edwh.model.common;

/**
 * This the basic model interface for reference data,
 * which are rare changing lists of code and description. 
 * 
 * @author elv
 */
public interface ReferenceData extends BaseModel {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
