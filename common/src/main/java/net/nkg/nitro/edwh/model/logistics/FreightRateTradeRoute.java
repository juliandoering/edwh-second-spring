package net.nkg.nitro.edwh.model.logistics;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;

/**
 * Freight Rates MD (mainly ocean freights) available to all group companies and negociated by ICL
 */
public interface FreightRateTradeRoute extends BaseModel {

    /**
     * Regularity of departures.
     * @return
     */
    Integer getFrequency();

    void setFrequency(Integer frequency);

    /**
     * Port where the cargo is destined.
     * @return
     */
    Location getPortOfDischarge();

    void setPortOfDischarge(Location portOfDischarge);

    /**
     * Ports in which the freight is reloaded to ocean vessel (for information only).
     * @return
     */
    String getTransShipment();

    void setTransShipment(String transShipment);

    /**
     * List of the possible freight rates.
     * @return
     */
    List<FreightRate> getFreightRate();

    void setFreightRate(List<FreightRate> freightRate);

}
