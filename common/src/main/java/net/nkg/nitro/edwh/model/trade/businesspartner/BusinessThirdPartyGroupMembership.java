package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface BusinessThirdPartyGroupMembership extends BaseModel {

    LocalDate getEntryDate();

    void setEntryDate(LocalDate entryDate);

    LocalDate getExitDate();

    void setExitDate(LocalDate exitDate);

    BusinessThirdParty getBusinessThirdParty();

    void setBusinessThirdParty(BusinessThirdParty businessThirdParty);

    BusinessThirdPartyGroup getBusinessThirdPartyGroup();

    void setBusinessThirdPartyGroup(BusinessThirdPartyGroup group);
}
