package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.BusinessPartner;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

public interface BusinessThirdPartyReferenceId extends BaseModel {

    String getReferenceId();

    void setReferenceId(String referenceId);

    BusinessThirdPartyReferenceIdType getTypeOfReferenceId();

    void setTypeOfReferenceId(BusinessThirdPartyReferenceIdType typeOfReferenceId);

    BusinessThirdParty getSupplierBusinessThirdParty();

    void setSupplierBusinessThirdParty(BusinessThirdParty supplierBusinessThirdParty);

    GroupCompany getSupplierGroupCompany();

    void setSupplierGroupCompany(GroupCompany supplierGroupCompany);

    default boolean isPersonalIdentificationNumber() {
        return getTypeOfReferenceId() != null
            && getTypeOfReferenceId().isPersonalIdentificationNumber();
    }
}
