package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Type how {@link Individual} identified themselves.
 */
public interface IdentificationType extends ReferenceData {

}
