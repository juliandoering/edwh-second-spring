package net.nkg.nitro.edwh.model.risk;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

public interface LimitUnit extends BaseModel {

    QuantityUnit getLimitUnitWeight();

    void setLimitUnitWeight(QuantityUnit limitUnitWeight);

    CurrencyUnit getLimitUnitCurrency();

    void setLimitUnitCurrency(CurrencyUnit limitUnitCurrency);

}
