package net.nkg.nitro.edwh.model.hedging;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

/**
 * Represents the valuation for an option trade
 */
public interface TerminalMarketOptionValuation extends AnalyticalData {

    /**
     * terminal market option trade id is the reference to the option
     */
    String getTerminalMarketOptionTradeId();

    void setTerminalMarketOptionTradeId(String id);

    /**
     * Valuation date, is the date when the option is drawn
     * not sooner than OptionTradeDate and not later than OptionTradeExpiration
     */
    LocalDate getValuationDate();

    void setValuationDate(LocalDate settlementDate);

    /**
     * Valuation delta, is a factor
     * cannot be zero
     * can either be positive or negative
     */
    BigDecimal getValuationDelta();

    void setValuationDelta(BigDecimal settlementDelta);

    /**
     * Value value, is the premium you pay when you draw an option
     * always a positive value
     */
    BigDecimal getValuationValue();

    void setValuationValue(BigDecimal settlementValue);

    BigDecimal getTradePrice();

    void setTradePrice(BigDecimal tradePrice);

    BigDecimal getStrikePrice();

    void setStrikePrice(BigDecimal strikePrice);

    int getTradeQuantity();

    void setTradeQuantity(int tradeQuantity);

    LocalDate getDeliveryPeriod();

    void setDeliveryPeriod(LocalDate deliveryPeriod);

    int getWeightedAverage();

    void setWeightedAverage(int weightedAverage);

    FutureMarket getFuturesMarket();

    void setFuturesMarket(FutureMarket futuresMarket);

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);

}
