package net.nkg.nitro.edwh.model.logistics;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.trade.contract.shippinginstruction.ContainerMovement;
import net.nkg.nitro.edwh.model.trade.contract.shippinginstruction.ContainerSize;

public interface OceanFreightRate extends BaseModel {

    /**
     * @return the rate for the defined route.
     */
    BigDecimal getRate();

    void setRate(BigDecimal rate);

    /**
     * @return transit time between port of shipment and port of destination in days.
     */
    BigDecimal getTransitTime();

    void setTransitTime(BigDecimal transitTime);

    /**
     * @return costs of the inland transports after arriving at the destination port.
     */
    BigDecimal getInlandCost();

    void setInlandCost(BigDecimal inlandCost);

    /**
     * @return true if the inland costs are included in the related rates.
     */
    boolean getInlandCostIncluded();

    void setInlandCostIncluded(boolean inlandCostIncluded);

    /**
     * @return the {@link Location} where the cargo gets picked up. 
     */
    Location getPlaceOfReceipt();

    void setPlaceOfReceipt(Location placeOfReceipt);

    /**
     * @return {@link Location} of the route of cargo after destination port. 
     */
    Location getPlaceOfDelivery();

    void setPlaceOfDelivery(Location placeOfDelivery);

    /**
     * @return the route of cargo after destination port. 
     */
    /* Use place of destination instead */
    @Deprecated
    String getInlandTransport();

    /* Use place of destination instead */
    @Deprecated
    void setInlandTransport(String inlandTransport);

    /**
     * @return terminal handling charges.
     */
    BigDecimal getThc();

    void setThc(BigDecimal thc);

    /**
     * Flag to indicate that terminal handling charges are included.
     * @return true if included otherwise false
     */
    boolean getThcIncluded();

    void setThcIncluded(boolean thcIncluded);

    /**
     * Flag if the shipping rate is preferred.
     * @return true if preferred otherwise false.
     */
    boolean getPreference();

    void setPreference(boolean preference);

    /**
     * @return additional comment of the rate
     */
    String getComment();

    void setComment(String comment);

    /**
     * @return the container size,
     * standard container sizes are 20ftdc, 40ftdc, etc.
     */
    ContainerSize getContainerSize();

    void setContainerSize(ContainerSize containerSize);

    /**
     * @return the container movement, i.e. FCL-FCL
     */
    ContainerMovement getContainerMovement();

    void setContainerMovement(ContainerMovement containerMovement);

    /**
     * @return the inland cost currency
     */
    CurrencyUnit getInlandCostCurrency();

    void setInlandCostCurrency(CurrencyUnit inlandCostCurrency);

    /**
     * @return the currency of the ocean freight rate
     */
    CurrencyUnit getOceanFreightCurrency();

    void setOceanFreightCurrency(CurrencyUnit oceanFreightCurrency);

    /**
     * @return the THC currency
     */
    CurrencyUnit getThcCurrency();

    void setThcCurrency(CurrencyUnit thcCurrency);

    /**
     * @return administration comment of the rate, only visible for maintainers.
     */
    String getAdministrationComment();

    void setAdministrationComment(String administrationComment);
}
