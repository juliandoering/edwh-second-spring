package net.nkg.nitro.edwh.model.trade;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.production.PhysicalCoffeeStockProcessing;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

/**
 * Represents an allocation of a physical coffee stock to either a contract (sale/purchase)
 * or a physical coffee stock processing (input to/output from).
 */
public interface Allocation extends BaseModel {

    public enum AllocationType {
        // DB values are used by ETL jobs of DWH Reporting
        PURCHASE("Purchase"),
        SALE("Sale"),
        INPUT_TO_PROCESSING("Input to processing"),
        OUTPUT_FROM_PROCESSING("Output from processing"),
        TRANSFER("In transfer to another warehouse");

        private final String type;

        private AllocationType(String type) {
            this.type = type;
        }

        /**
         * Find the correct enum value for a String.
         * @param name the String value of the enum value.
         * @return the instance of AllocationType that matches the name, or null if name is unknown.
         */
        public static AllocationType getAllocationTypeValue(String name) {
            if (name != null) {
                // check all possible enum values if they match
                for (AllocationType allocationTypeVal : AllocationType.values()) {
                    // test if the enum-value matches our given string
                    if (name.equalsIgnoreCase(allocationTypeVal.type)) {
                        // we found the right enum-value
                        return allocationTypeVal;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    AllocationType getAllocationType();

    void setAllocationType(AllocationType type);

    Contract getAllocatedContract();

    void setAllocatedContract(Contract contract);

    PhysicalCoffeeStockProcessing getAllocatedProcessing();

    void setAllocatedProcessing(PhysicalCoffeeStockProcessing processing);

    LocalDate getAllocationDate();

    void setAllocationDate(LocalDate date);

    BigDecimal getVolume();

    void setVolume(BigDecimal volume);

}
