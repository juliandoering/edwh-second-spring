package net.nkg.nitro.edwh.model.hedging;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * The futures month is the month in which the commodity will be sold, and has an industry specific list.
 */
public interface FutureMonth extends ReferenceData {

    /**
     * The int representation of a month.
     * @return
     */
    int getIntRepresentation();

    void setIntRepresentation(int intRepresentation);

}
