package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Credit terms for third party companies with commercial credit.
 * @author nf
 *
 */
public interface CommercialCreditTerms extends BaseModel {

    // Credit Terms {only in case of commercial credit}
    // Days: amount
    Long getDays();

    void setDays(Long days);

    // From: {one of}
    // Presentation of original documents
    // Presentation of scanned copy of documents
    // Delivery / Receipt of goods / services
    CommercialCreditTermsFromType getCreditTermsFrom();

    void setCreditTermsFrom(CommercialCreditTermsFromType from);

}
