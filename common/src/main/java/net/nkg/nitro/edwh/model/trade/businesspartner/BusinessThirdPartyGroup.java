package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface BusinessThirdPartyGroup extends BaseModel {

    String getName();

    void setName(String name);
}
