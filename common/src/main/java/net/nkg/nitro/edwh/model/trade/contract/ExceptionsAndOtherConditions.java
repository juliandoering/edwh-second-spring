package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Here the user can enter any further conditions or exceptions that may apply in this case.
 */
public interface ExceptionsAndOtherConditions extends BaseModel {

    /**
     * A string or line of text containing alphanumeric characters.<p/>
     * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
     */
    String getLine();

    void setLine(String line);
}
