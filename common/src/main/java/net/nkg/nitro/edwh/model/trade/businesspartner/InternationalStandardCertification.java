package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.common.ValidityPeriod;
import net.nkg.nitro.edwh.model.foundation.InternationalStandardCertificationProgram;

/**
 * Certification of the organisation like "trade partnership against terrorism" or
 * 
 * "ISO","BRC","IFS","HACCP","TCPAT","OHSAS","ISO","BRC","IFS","HACCP","TCPAT","OHSAS"
 *
 */
public interface InternationalStandardCertification extends BaseModel, ValidityPeriod {

    InternationalStandardCertificationProgram getCertification();

    void setCertification(InternationalStandardCertificationProgram certification);

    String getCertificateVersion();

    void setCertificateVersion(String version);

    String getThirdPartyIdNumber();

    void setThirdPartyIdNumber(String idNumber);

    LocalDate getValidityPeriodFrom();

    void setValidityPeriodFrom(LocalDate date);

    LocalDate getValidityPeriodTo();

    void setValidityPeriodTo(LocalDate date);
}
