package net.nkg.nitro.edwh.model.trade.contract;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Location;

/**
 * Information related to the routing and transportation of goods. This may include detail such as conveyance
 * identification (for example, the vessel name), the associated transportation route (for example, port of loading), etc.
 */
public interface RoutingSummary extends BaseModel {

    /**
     * The 'Location' section is used to identify a location (code or name) as having a certain role within the document. For
     * example, 'New York' could be selected and qualified as being the 'Place of Delivery'.
     * A geographical area or a place. It must be clearly identifiable since no street address is given. For example, a town, a
     * city, a code for an airport, or a place. It is represented by a name and a location code defined by Bolero.
     */
    List<Location> getLocations();

    void setLocations(List<Location> locations);

}
