package net.nkg.nitro.edwh.model.foundation;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface PostalAddress extends BaseModel {

    List<PostalAddressType> getAddressTypes();

    void setAddressTypes(List<PostalAddressType> addressTypes);

    String getAddressLine1();

    void setAddressLine1(String line);

    String getAddressLine2();

    void setAddressLine2(String line);

    String getPostalCode();

    void setPostalCode(String postalCode);

    Location getLocation();

    void setLocation(Location location);
}
