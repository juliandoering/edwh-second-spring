package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;

public interface ExternalCreditLimit extends BaseModel {

    /**
     * External Party: Business Third Party (Service Vendor - Credit Insurance) 
     * @return
     */
    BusinessThirdParty getInsurer();

    /**
     * External Party: Business Third Party (Service Vendor - Credit Insurance) 
     * @param insurer
     */
    void setInsurer(BusinessThirdParty insurer);

    /**
     * @return client ID of the insured BTP which has been defined by the insurer.
     */
    String getInsurerId();

    /**
     * Sets client ID of the insured BTP which has been defined by the insurer. 
     * @param insurerId
     */
    void setInsurerId(String insurerId);

    /**
     * The currency in which the credit limit is specified .
     * @return
     */
    CurrencyUnit getCurrency();

    /**
     * The currency in which the credit limit is specified .
     * @param currency
     */
    void setCurrency(CurrencyUnit currency);

    /**
     * The amount of <i>CurrencyUnit</i> that is the limit.
     * @return
     */
    BigDecimal getAmount();

    /**
     * The amount of <i>CurrencyUnit</i> that is the limit.
     * @param currencyQuantity
     */
    void setAmount(BigDecimal currencyQuantity);

    /**
     * Get the date from when on the credit is valid.
     * @return
     */
    LocalDate getValidFrom();

    /**
     * Set the date from when on the credit is valid.
     * @param validFromDate
     */
    void setValidFrom(LocalDate validFromDate);

    /**
     * Get the date till when the credit is valid.
     * @return
     */
    LocalDate getValidTo();

    /**
     * Set the date till when the credit is valid.
     * @param validToDate
     */
    void setValidTo(LocalDate validToDate);

}
