package net.nkg.nitro.edwh.model.valuation;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ValuationReferenceData;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;
import net.nkg.nitro.edwh.model.logistics.Incoterm;
import net.nkg.nitro.edwh.model.quality.CoffeeQuality;
import net.nkg.nitro.edwh.model.trade.contract.FuturesId;

public interface ValuationDifferential extends ValuationReferenceData {

    BigDecimal getDifferential();

    void setDifferential(BigDecimal differential);

    CoffeeQuality getCoffeeQuality();

    void setCoffeeQuality(CoffeeQuality coffeeQuality);

    CurrencyUnit getCurrencyUnit();

    void setCurrencyUnit(CurrencyUnit currencyUnit);

    QuantityUnit getQuantityUnit();

    void setQuantityUnit(QuantityUnit quantityUnit);

    Incoterm getIncoterm();

    void setIncoterm(Incoterm incoterm);

    Packing getPacking();

    void setPacking(Packing packing);

    Location getOrigin();

    void setOrigin(Location origin);

    FuturesId getFuturesId();

    void setFuturesId(FuturesId futuresId);

}
