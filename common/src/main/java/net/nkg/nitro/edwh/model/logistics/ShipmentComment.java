package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Stores a comment associated with a Shipment.
 */
public interface ShipmentComment extends BaseModel {

    /**
     * Type of comment, can only be "General"
     */
    String getType();

    void setType(String type);

    /**
     * The actual comment, up to 70 characters
     */
    String getShipmentComment();

    void setShipmentComment(String shipmentComment);
}
