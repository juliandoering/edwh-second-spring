package net.nkg.nitro.edwh.model.trade.contract;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Terms and conditions in addition to standard terms and conditions or any special terms and conditions.<p/>
 * Example: NMA Clause 23B.
 */
public interface Clause extends BaseModel {

    /**
     * A string or line of text containing alphanumeric characters.<p/>
     * Example: Mr. Jean Michel can be contacted on 00 33 73 48 59 32
     */
    String getLine();

    void setLine(String line);
}
