package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface CommercialId extends BaseModel {

    String getCommercialId();

    void setCommercialId(String commercialId);
}
