package net.nkg.nitro.edwh.authorization;

final class OrAuthorization<T> implements Authorization<T> {

    private Authorization<T> authorization;
    private Authorization<T> otherAuthorization;

    OrAuthorization(Authorization<T> authorization, Authorization<T> otherAuthorization) {
        this.authorization = authorization;
        this.otherAuthorization = otherAuthorization;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        return authorization.isAuthorized(model, operation)
            || otherAuthorization.isAuthorized(model, operation);
    }
}
