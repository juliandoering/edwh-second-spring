package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.Contact;
import net.nkg.nitro.edwh.model.foundation.Individual;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.PostalAddress;
import net.nkg.nitro.edwh.model.logistics.Incoterm;
import net.nkg.nitro.edwh.model.trade.PaymentTerm;

public interface BusinessThirdParty extends BaseModel {

    /**
     * An identification counter to make the business key unique.
     * @return
     */
    Integer getIdentificationCounter();

    void setIdentificationCounter(Integer identificationCounter);

    /**
     * Full legal name in local language as stated in the latest & current articles of association or amendment to the same.
     * In case of sole proprietorship the full legal name of the proprietor.
     * Mandatory.
     * @return
     */
    String getLegalName();

    void setLegalName(String legalName);

    /**
     * Only to be given when Legal name is not written using Latin Alphabet.
     * In case of illegible, translation into English.
     * Optional.
     * @return
     */
    String getLegalNameInLatinAlphabet();

    void setLegalNameInLatinAlphabet(String legalNameEnglish);

    /**
     * Country where the legal name is registered.
     * Mandatory.
     * @return
     */
    Location getLegalCountry();

    void setLegalCountry(Location legalCountry);

    /**
     * Full commercial name in local language as stated in the latest & current certification from the commercial registry of the country.
     * To be specified if different from the Legal Name.
     * Optional.
     * @return
     */
    String getCommercialName();

    void setCommercialName(String commercialName);

    /**
     * Short commercial name or acronym used for abbreviation purposes and/or common identification.
     * To be specified when different from both legal and commercial names.
     * Optional.
     * @return
     */
    String getAcronym();

    void setAcronym(String acronym);

    /**
     * One and only one must be specified.
     * @return
     */
    BusinessOrganisationType getBusinessType();

    void setBusinessType(BusinessOrganisationType businessType);

    /**
     * All addresses associated with the Business Third Party. Every address must have a type specifying its use,
     * e.g. postal address, billing address, etc.
     * @return the list of all addresses.
     */
    List<PostalAddress> getAddresses();

    void setAddresses(List<PostalAddress> addresses);

    /**
     * Telephone numbers, Fax numbers, E-mail addresses, websites of this.
     * Telephone number(s) of the company, not personal ones of the representatives or contact persons.
     * In the case of sole proprietorship this must be the telephone number(s) of the proprietor.
     * Telephone number must include: Country code prefix and regional code prefix when used.
     * Fax number(s) of the company, not personal ones of the representatives or contact persons.
     * In the case of sole proprietorship this would be the fax number(s) of the proprietor.
     * Fax number must include: Country code prefix and regional code prefix when used.
     * E-mail address(es) of the company, not personal ones of the representatives or contact persons.
     * In the case of sole proprietorship this would be the e-mail address(es) of the proprietor.
     * E-mail address must be a valid e-mail address which includes: an “at” (@) character and a dot (.) as part of the domain name.
     * Website address (es) of the company.
     * Website address must be a valid internet address.
     * @return
     */
    List<Contact> getContacts();

    void setContacts(List<Contact> contacts);

    /**
     * In case such identifications exist in the country of operations.  This might be commercial registry number(s), or coffee producer ID, or any other id relevant to the execution of the commercial activity with the group company.
     * More than one can be specified.
     * Optional.
     * @return
     */
    List<CommercialId> getCommercialIds();

    void setCommercialIds(List<CommercialId> commercialIds);

    /**
     * Tax identification number(s) of the company.
     * If separate numbers are given for separate taxes (i.e. one for Income Tax, another for Value Added Tax) all must be stated separately.
     * Optional.
     * @return
     */
    List<TaxId> getTaxIds();

    void setTaxIds(List<TaxId> taxIds);

    /**
     * List of all associations to an individual.
     * @return
     */
    List<BusinessThirdPartyIndividualAssociation> getIndividualAssociations();

    void setIndividualAssociations(List<BusinessThirdPartyIndividualAssociation> individualAssociations);

    /**
     * Complete bank account(s) details including: Name of account, number of account, bank branch name and address, bank legal name.
     * BIC, SWIFT, IBAN should be stated when available.
     * Name of account must match the Legal Name.
     * At least one must be specified.
     * Mandatory.
     * @return
     */
    List<BankAccount> getBankAccounts();

    /**
     * Set the bank account information which is either BIC, IBAN or 'other'.
     * @param bankAccounts
     */
    void setBankAccounts(List<BankAccount> bankAccounts);

    /**
     * Get the types of function this third party has (seller/buyer/service vendor).
     * At least one must be specified.
     * In the case of Coffee Supplier – Producer and Coffee Supplier – Producer/Intermediary additional separate information about the farm is to be provided.
     * Mandatory.
     * @return
     */
    List<ThirdPartyType> getThirdPartyTypes();

    void setThirdPartyTypes(List<ThirdPartyType> thirdPartyTypes);

    /**
     * International standard certifications of the company.
     * Optional.
     * @return
     */
    List<InternationalStandardCertification> getInternationalStandardCertifications();

    void setInternationalStandardCertifications(List<InternationalStandardCertification> certificates);

    /**
     * Product certifications of the companies product.
     * Optional.
     * @return
     */
    List<ProductCertification> getProductCertifications();

    void setProductCertifications(List<ProductCertification> certificates);

    /**
     * Get the compliance rating. <b></b>
     * @return
     */
    ComplianceRating getComplianceRating();

    void setComplianceRating(ComplianceRating complianceRating);

    /**
     * Preferred term o contract.
     * Can be blank = no preferred term.
     * Optional.
     * @return
     */
    Incoterm getContractTerms();

    void setContractTerms(Incoterm incoterm);

    /**
     * Preferred term of pricing for contracts.
     * Can be blank = no preferred fixation Term.
     * @return
     */
    List<PriceFixationTerm> getPriceFixationTerms();

    void setPriceFixationTerms(List<PriceFixationTerm> priceFixationTerms);

    /**
     * Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable).
     * Mandatory.
     * @return
     */
    PresentationOfDocuments getPresentationOfDocuments();

    void setPresentationOfDocuments(PresentationOfDocuments presentationOfDocuments);

    /**
     * Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable).
     * Mandatory.
     * @return
     */
    PaymentTerm getPaymentTerms();

    void setPaymentTerms(PaymentTerm thirdPartyPaymentTerm);

    /**
     * Special terms and conditions specified by the third party, which is free text.
     * @return
     */
    String getPreferredTermsAndConditions();

    void setPreferredTermsAndConditions(String preferredTermsAndConditions);

    /**
     * SustainabilityProject information of the companies product.
     * Optional.
     * @return
     */
    List<SustainabilityProjectInformation> getSustainabilityProjectInformation();

    void setSustainabilityProjectInformation(List<SustainabilityProjectInformation> projectsInformation);

    /**
     * @return a list of {@link @ExternalCreditLimit}s (optional)
     */
    List<ExternalCreditLimit> getExternalCreditLimits();

    void setExternalCreditLimits(List<ExternalCreditLimit> externalCreditLimits);

    /**
     * A code assigned to a BTP and a description by a third party that clearly identifies the BTP to them.
     * Optional.
     * @return
     */
    List<BusinessThirdPartyReferenceId> getBusinessThirdPartyReferenceIds();

    void setBusinessThirdPartyReferenceIds(List<BusinessThirdPartyReferenceId> businessThirdPartyReferenceIds);

    /**
     * @return A comment for giving additional semantic to the business third party, that is not for automated processing.
     */
    String getComment();

    void setComment(String comment);

    /**
     * @return If the business third party is pending. Is always pending, if the business third party wasn't created by an EDWH-Administrator and can only be changed by an EDWH-Administrator.
     */
    boolean isPending();

    void setPending(boolean pending);

    default boolean isSoleProprietorship() {
        return getBusinessType() != null
            && getBusinessType().isSoleProprietorship();
    }
}
