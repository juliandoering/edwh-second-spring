package net.nkg.nitro.edwh.authorization.check;

import java.util.function.Function;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.RestOperation;

public class NullValueAuthorization<T> implements Authorization<T> {

    private Function<T, ?> accessor;

    public NullValueAuthorization(Function<T, ?> accessor) {
        this.accessor = accessor;
    }

    @Override
    public boolean isAuthorized(T model, RestOperation operation) {
        return accessor.apply(model) == null;
    }
}
