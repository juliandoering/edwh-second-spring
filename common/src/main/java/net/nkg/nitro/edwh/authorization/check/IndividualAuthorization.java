package net.nkg.nitro.edwh.authorization.check;

import net.nkg.nitro.edwh.authorization.Authorization;
import net.nkg.nitro.edwh.authorization.CurrentUserProvider;
import net.nkg.nitro.edwh.authorization.RestOperation;
import net.nkg.nitro.edwh.model.foundation.Individual;
import net.nkg.nitro.edwh.model.profile.UserRole.EdwhUserRole;

public class IndividualAuthorization implements Authorization<Individual> {

    private CurrentUserProvider user;

    public IndividualAuthorization(CurrentUserProvider userProvider) {
        this.user = userProvider;
    }

    @Override
    public boolean isAuthorized(Individual model, RestOperation operation) {
        if (this.user.isRoleAssigned(EdwhUserRole.EDWH_TECHNICAL)
            && operation == RestOperation.CREATE) {

            model.setPending(Boolean.TRUE); // TODO move to lifecycle service and replace this with RoleAuthorization
            return true;
        }

        return false;
    }
}
