package net.nkg.nitro.edwh.model.hedging;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.trade.TradeType;

public interface TerminalMarketOptionTrade extends AnalyticalData {

    String getTmtId();

    void setTmtId(String tmtId);

    String getTradeNumber();

    void setTradeNumber(String tradeNumber);

    LocalDate getTradeDate();

    void setTradeDate(LocalDate tradeDate);

    LocalDate getExpirationDate();

    void setExpirationDate(LocalDate expirationDate);

    BigDecimal getTradePrice();

    void setTradePrice(BigDecimal tradePrice);

    BigDecimal getStrikePrice();

    void setStrikePrice(BigDecimal strikePrice);

    Integer getTradeQuantity();

    void setTradeQuantity(Integer tradeQuantity);

    Integer getFuturesYear();

    void setFuturesYear(Integer futuresYear);

    boolean isExcercized();

    void setExcercized(boolean excercized);

    FutureMarket getFuturesMarket();

    void setFuturesMarket(FutureMarket futuresMarket);

    TradeType getTradeType();

    void setTradeType(TradeType tradeType);

    void setOptionTradeCategory(String optionTradeCategory);

    String getOptionTradeCategory();

    FutureMonth getFuturesMonth();

    void setFuturesMonth(FutureMonth futuresMonth);

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);

}
