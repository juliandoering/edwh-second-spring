package net.nkg.nitro.edwh.model.risk;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Information and the current status of an ETL job to upload a coffee risk position excel file.
 */
public interface CoffeeRiskPositionJob {

    String getPdiJobId();

    void setPdiJobId(String pdiJobId);

    String getTagetikCode();

    void setTagetikCode(String tagetikCode);

    LocalDate getReportDate();

    void setReportDate(LocalDate reportDate);

    LocalDateTime getStartedAt();

    void setStartedAt(LocalDateTime startedAt);

    LocalDateTime getStoppedAt();

    void setStoppedAt(LocalDateTime stoppedAt);

    String getUploader();

    void setUploader(String uploader);

    String getStatus();

    void setStatus(String status);

    String getMessage();

    void setMessage(String message);
}
