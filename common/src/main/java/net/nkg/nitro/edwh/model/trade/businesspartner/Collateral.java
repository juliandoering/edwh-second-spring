package net.nkg.nitro.edwh.model.trade.businesspartner;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;

public interface Collateral extends BaseModel {

    CollateralType getCollateralType();

    void setCollateralType(CollateralType type);

    CurrencyUnit getCurrency();

    void setCurrency(CurrencyUnit currency);

    BigDecimal getAmount();

    void setAmount(BigDecimal currencyQuantity);
}
