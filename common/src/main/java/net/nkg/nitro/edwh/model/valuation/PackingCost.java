package net.nkg.nitro.edwh.model.valuation;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ValuationReferenceData;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;

public interface PackingCost extends ValuationReferenceData {

    /**
     * The cost in {@link #getCostCurrency()} for a {@link #getCostUnit()} of {@link #getPackingType()}.
     * 
     * @return
     */
    BigDecimal getCost();

    void setCost(BigDecimal cost);

    /**
     * The type of packing this cost is defined for valuations.
     * For example this can be Jute Bags or Big-Bags.
     *  
     * @return
     */
    Packing getPackingType();

    void setPackingType(Packing packingType);

    /**
     * The currency {@link #getCost()} is defined in.
     * 
     * @return
     */
    CurrencyUnit getCostCurrency();

    void setCostCurrency(CurrencyUnit costCurrency);

    /**
     * The unit of {@link #getPackingType()} the {@link #getCost()} is defined for.
     * 
     * @return
     */
    QuantityUnit getCostUnit();

    void setCostUnit(QuantityUnit costUnit);
}
