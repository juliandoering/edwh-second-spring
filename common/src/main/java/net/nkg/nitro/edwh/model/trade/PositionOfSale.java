package net.nkg.nitro.edwh.model.trade;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Position defines the conditions under which the commodity is actually sold.
 */
public interface PositionOfSale extends ReferenceData {

}
