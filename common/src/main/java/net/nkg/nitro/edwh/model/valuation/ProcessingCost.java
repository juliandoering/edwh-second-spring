package net.nkg.nitro.edwh.model.valuation;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ValuationReferenceData;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;
import net.nkg.nitro.edwh.model.quality.CoffeeQuality;

public interface ProcessingCost extends ValuationReferenceData {

    /**
     * The processing cost in the given currency.
     * @return
     */
    BigDecimal getCost();

    void setCost(BigDecimal cost);

    /**
     * The coffee quality being processed to a new coffee quality.
     * @return
     */
    CoffeeQuality getCoffeeQualityFrom();

    void setCoffeeQualityFrom(CoffeeQuality coffeeQualityFrom);

    /**
     * The resulting coffee quality of the processing.
     * @return
     */
    CoffeeQuality getCoffeeQualityTo();

    void setCoffeeQualityTo(CoffeeQuality coffeeQualityTo);

    /**
     * The currency of this processing cost.
     * @return
     */
    CurrencyUnit getCostCurrency();

    void setCostCurrency(CurrencyUnit costCurrency);

    /**
     * The weigh unit of this cost.
     * @return
     */
    QuantityUnit getCostUnit();

    void setCostUnit(QuantityUnit costUnit);
}
