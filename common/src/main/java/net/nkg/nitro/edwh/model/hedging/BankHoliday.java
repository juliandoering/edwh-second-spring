
package net.nkg.nitro.edwh.model.hedging;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

public interface BankHoliday extends BaseModel {

    LocalDate getHoliday();

    void setHoliday(LocalDate holiday);

    String getDescription();

    void setDescription(String description);

    FutureMarket getFuturesMarket();

    void setFuturesMarket(FutureMarket futuresMarket);

}
