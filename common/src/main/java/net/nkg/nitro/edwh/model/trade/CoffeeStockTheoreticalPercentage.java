package net.nkg.nitro.edwh.model.trade;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.quality.CoffeeQuality;
import net.nkg.nitro.edwh.model.quality.CoffeeQualityPercentage;

/**
 * A coffee stock theoretical conversion factor indicates the theoretical conversion factor of certain {@link CoffeeQuality}
 * which can be processed from the related {@link PhysicalCoffeeStock}.
 *
 */
public interface CoffeeStockTheoreticalPercentage extends CoffeeQualityPercentage {

    /**
     * Set the coffee quality that is matched.
     * @param quality the coffee quality.
     */
    void setCoffeeQuality(CoffeeQuality quality);

    /**
     * @return the coffee quality that is matched.
     */
    CoffeeQuality getCoffeeQuality();

    /**
     * @return the conversion factor for the matched coffee quality. 
     */
    BigDecimal getGradedConversionFactor();

    /**
     * Set the conversion factor for the matched coffee quality.
     * @param gradedConversionFactor a positive decimal value.
     */
    void setGradedConversionFactor(BigDecimal gradedConversionFactor);

    /**
     * @return the related {@link PhysicalCoffeeStock}
     */
    PhysicalCoffeeStock getPhysicalCoffeeStock();

    /**
     * Set the related physical coffee stock.
     * @param the {@link PhysicalCoffeeStock}.
     */
    void setPhysicalCoffeeStock(PhysicalCoffeeStock physicalCoffeeStock);
}
