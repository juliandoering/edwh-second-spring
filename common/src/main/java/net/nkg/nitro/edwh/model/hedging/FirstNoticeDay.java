package net.nkg.nitro.edwh.model.hedging;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Contains information about the first notice date.
 *
 */
public interface FirstNoticeDay extends BaseModel {
    /**
     * @return the first notice date as {@link LocalDate}
     */
    LocalDate getFirstNoticeDay();

    /**
     * @param firstNoticeDay sets the first notice day
     */
    void setFirstNoticeDay(LocalDate firstNoticeDay);
}
