package net.nkg.nitro.edwh.model.trade.contract;

import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * Structured information regarding the financial settlement of a purchase. For example, the number of days and method
 * by which payment is due.
 */
public interface PaymentTerms extends BaseModel {

    /**
     * Terms of payment which are specifically defined by the user.<p/>
     * Example: Payment within 15 days and 3% cash discount or 30 days net.
     */
    List<UserDefinedPaymentTerms> getDefinedPaymentTerms();

    void setDefinedPaymentTerms(List<UserDefinedPaymentTerms> definedPaymentTerms);

    /**
     * Terms of payment which specify a coded payment type with optional additional information.
     */
    StandardPaymentTerms getStandardPaymentTerms();

    void setStandardPaymentTerms(StandardPaymentTerms standardPaymentTerms);

    /**
     * The date by which payment should be made.
     */
    LocalDate getPaymentDueDate();

    void setPaymentDueDate(LocalDate paymentDueDate);

    /**
     * Date by which payment should be received in the bank account of the supplier.
     */
    LocalDate getPaymentReceiptDate();

    void setPaymentReceiptDate(LocalDate paymentReceiptDate);
}
