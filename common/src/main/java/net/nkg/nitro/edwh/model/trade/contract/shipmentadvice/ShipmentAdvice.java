package net.nkg.nitro.edwh.model.trade.contract.shipmentadvice;

import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

/**
 * This describes a shipment advice and its composed information.
 */
public interface ShipmentAdvice extends BaseModel {

    /**
     * Gets the contract this is for.
     *
     * @return the contract this is for.
     */
    Contract getContract();

    void setContract(Contract contract);

    /**
     * Gets the sequential number.
     *
     * @return the sequential number.
     */
    Long getSequentialNumber();

    void setSequentialNumber(Long sequentialNumber);

    /**
     * Gets the name of the vessel.
     *
     * @return the name of the vessel.
     */
    String getVesselName();

    void setVesselName(String name);

    /**
     * Gets the name of the shipping company.
     *
     * @return the name of the shipping company.
     */
    String getShippingCompanyName();

    void setShippingCompanyName(String name);

    /**
     * Gets the list of shippers.
     *
     * @return the list of shippers.
     */
    String getNameOfShippers();

    void setNameOfShippers(String nameOfShippers);

    /**
     * Gets the bill of lading number.
     *
     * @return the bill of lading number.
     */
    String getBillOfLadingNumber();

    void setBillOfLadingNumber(String billOfLadingNumber);

    /**
     * Gets the date the containers have been boarded.
     *
     * @return the boarding date.
     */
    LocalDate getOnBoardDate();

    void setOnBoardDate(LocalDate date);

    /**
     * Gets the date the containers were shipped.
     *
     * @return the shipped date.
     */
    LocalDate getShippedDate();

    void setShippedDate(LocalDate date);

    /**
     * Gets the port of shipment.
     *
     * @return the port of shipment.
     */
    String getPortOfShipment();

    void setPortOfShipment(String port);

    /**
     * Gets the port of destination.
     *
     * @return the port of destination.
     */
    String getPortOfDestination();

    void setPortOfDestination(String port);

    /**
     * Gets the quantity.
     *
     * @return the quantity.
     */
    Long getQuantity();

    void setQuantity(Long quantity);

    /**
     * Gets the description.
     *
     * @return the description.
     */
    String getDescription();

    void setDescription(String description);

    /**
     * Gets the main marks.
     *
     * @return the main marks.
     */
    String getMainMarks();

    void setMainMarks(String mainMarks);

    /**
     * Gets the information about the containers listed on the advice.
     *
     * @return the containers.
     */
    List<Container> getContainers();

    void setContainers(List<Container> containers);
}
