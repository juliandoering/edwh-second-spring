package net.nkg.nitro.edwh.model.trade;

import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;
import net.nkg.nitro.edwh.model.logistics.Warehouse;
import net.nkg.nitro.edwh.model.quality.CoffeeQuality;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.contract.Quantity;

public interface PhysicalCoffeeStock {

    /**
     * Returns the holder of the coffee stock, i.e. who has the coffee stock in their warehouse.
     */
    GroupCompany getHolder();

    /**
     * Set the holder of the coffee stock.
     * @param holder the group company holding the coffee stock.
     */
    void setHolder(GroupCompany holder);

    /**
     * Returns the owner of the coffee stock, if it is a group company.
     */
    GroupCompany getOwnerGroupCompany();

    /**
     * Set the owner of the coffee stock to a group company.
     * @param owner the group company owning the coffee stock.
     */
    void setOwnerGroupCompany(GroupCompany owner);

    /**
     * Returns the owner of the coffee stock, if it is a business third party.
     */
    BusinessThirdParty getOwnerBusinessThirdParty();

    /**
     * Set the owner of the coffee stock to a business third party.
     * @param owner the business third party owning the coffee stock.
     */
    void setOwnerBusinessThirdParty(BusinessThirdParty owner);

    /**
     * Returns the id the holder assigned to this coffee stock.
     */
    String getGroupCompanyInternalId();

    /**
     * Set the id the holder assigned to this coffee stock.
     * @param id the internal id for this coffee stock used by the holder.
     */
    void setGroupCompanyInternalId(String id);

    /**
     * Returns the date of creation of the coffee stock.
     */
    LocalDate getCreationDate();

    /**
     * Set the creation date of the coffee stock.
     * @param creationDate the date of creation.
     */
    void setCreationDate(LocalDate creationDate);

    /**
     * Returns the date this coffee stock disappeared, e.g. by being delivered or consumed in a
     * processing.
     */
    LocalDate getDisappearanceDate();

    /**
     * Set the disappearance date of the coffee stock.
     * @param disappearanceDate the date this coffee stock was delivered or processed.
     */
    void setDisappearanceDate(LocalDate disappearanceDate);

    /**
     * Returns the quantity of coffee contained in this stock.
     */
    QuantityUnit getQuantityUnit();

    /**
     * Set the Quantity of coffee for this stock.
     * @param quantityUnit the Quantity
     */
    void setQuantityUnit(QuantityUnit quantityUnit);

    /**
     * Returns the packing type of the coffee stock.
     */
    Packing getPacking();

    /**
     * Set the type of packing used for the coffee stock.
     * @param packing the packing
     */
    void setPacking(Packing packing);

    /**
     * Returns the quality of the coffee contained in this stock.
     */
    CoffeeQuality getCoffeeQuality();

    /**
     * Set the coffee quality of the coffee stock.
     * @param quality the quality of the coffee.
     */
    void setCoffeeQuality(CoffeeQuality quality);

    /**
     * Returns the crop year of this coffee stock.
     */
    int getCropYear();

    /**
     * Set the crop year of this coffee stock.
     * @param cropYear the year the coffee was harvested.
     */
    void setCropYear(int cropYear);

}
