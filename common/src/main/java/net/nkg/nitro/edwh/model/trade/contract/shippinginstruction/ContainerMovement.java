package net.nkg.nitro.edwh.model.trade.contract.shippinginstruction;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * A container movement is defined by one of those codes:
 * LCL-FCL, LCL-LCL, FCL-LCL, FCL-FCL, CFS-CY, CFS-CFS or CY-CFS / CY-CY
 */
public interface ContainerMovement extends ReferenceData {

}
