package net.nkg.nitro.edwh.model.trade.contract;

public enum ContractRoleEnum {
    AGENT("Agent"),
    BUYER("Buyer"),
    SELLER("Seller"),
    SHIPPER("Shipper"),
    UNSPECIFIED("Unspecified");

    private final String code;

    private ContractRoleEnum(String code) {
        this.code = code;
    }

    public static ContractRoleEnum getContractRoleEnum(String code) {
        if (code != null) {
            // check all possible enum values if they match
            for (ContractRoleEnum contractRole : ContractRoleEnum.values()) {
                // test if the enum-value matches our given string
                if (code.equalsIgnoreCase(contractRole.code)) {
                    // we found the right enum-value
                    return contractRole;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.code;
    }
}
