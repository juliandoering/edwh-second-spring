package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Interface for certificates e.g. "ISO"
 */
public interface InternationalStandardCertificationProgram extends ReferenceData {
}
