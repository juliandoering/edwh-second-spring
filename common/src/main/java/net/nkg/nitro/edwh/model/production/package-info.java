/**
 * This includes all entities of the domain production, which is anything about processing coffee.
 */
package net.nkg.nitro.edwh.model.production;
