package net.nkg.nitro.edwh.model.hedging;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.trade.TradeCategory;
import net.nkg.nitro.edwh.model.trade.TradeType;
import net.nkg.nitro.edwh.model.trade.contract.FuturesId;

/**
 * Interface hedging specific information
 */
public interface TerminalMarketTrade extends AnalyticalData {

    String getTmtId();

    void setTmtId(String tmctId);

    Integer getTradeNumber();

    void setTradeNumber(Integer tradeNumber);

    TradeCategory getTradeCategory();

    void setTradeCategory(TradeCategory tradeCategory);

    LocalDate getTradeDate();

    void setTradeDate(LocalDate tradeDate);

    /**
     * Amount in lots
     * @param amount
     */
    void setTradeQuantity(Integer amount);

    /**
     * Amount in lots
     */
    Integer getTradeQuantity();

    /**
     * The amount is for
     * <li>ICE-NY: 2 decimals
     * <li>ICE-London: 0 decimals
     */
    void setTradePrice(BigDecimal amount);

    /**
     * The amount is for
     * <li>ICE-NY: 2 decimals
     * <li>ICE-London: 0 decimals
     */
    BigDecimal getTradePrice();

    Boolean isClosedOut();

    void setClosedOut(Boolean closedOut);

    FuturesId getFuturesId();

    void setFuturesId(FuturesId futuresId);

    TradeType getTradeType();

    void setTradeType(TradeType tradeType);

    GroupCompany getGroupCompany();

    void setGroupCompany(GroupCompany groupCompany);

    /**
     * A TMT can be closed out against several other TMTs.
     * <br>Example:<ul>
     * <li>TMT1: 5 Lots Purchase</li>
     * <li>TMT2: 3 Lots Sale</li>
     * <li>TMT3: 2 Lots Sale</li>
     * </ul>
     */
    List<TerminalMarketTrade> getCloseoutTrades();

    void setCloseoutTrades(List<TerminalMarketTrade> tmts);
}
