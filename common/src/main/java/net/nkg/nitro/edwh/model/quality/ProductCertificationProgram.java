package net.nkg.nitro.edwh.model.quality;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Created by nf on 25.10.2016.
 * Interface for certificates e.g. "Rainforest Alliance"
 */
public interface ProductCertificationProgram extends ReferenceData {
}
