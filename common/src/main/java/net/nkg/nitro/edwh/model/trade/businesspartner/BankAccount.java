package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.PostalAddress;

/**
 * A bank account.
 * <p/>
 * Models bank account identification.
 * Background is that there are different systems to identify a bank account.
 * Notably 
 * <ul>
 * <li>ISO 9362 (BIC + account number</li>
 * <li>ISO 13616-1:2007 <i>Part 1</i> (IBAN)</li>
 * </ul>
 * but also others that are free text fields and not further described.
 * To have further safety for this sensitive topic, we use a type to have the ability to have further 
 * safety, certainty and checks.
 * @author nf
 *
 */
public interface BankAccount extends BaseModel {

    /**
     * Set the type of the number that is attacked with this object.
     * @param type
     */
    void setBankAccountNumberType(BankAccountNumberType type);

    /**
     * Get the type of the number.
     * @return
     */
    BankAccountNumberType getBankAccountNumberType();

    String getBankAccountDescription();

    void setBankAccountDescription(String name);

    /**Set the number of the bank account.
     * @param number
     */
    void setBankAccountNumber(String number);

    /**Get the number of the bank account
     * @return 
     */
    String getBankAccountNumber();

    String getBankLegalName();

    void setBankLegalName(String name);

    String getBankBranchName();

    void setBankBranchName(String branchName);

    PostalAddress getBankAddress();

    void setBankAddress(PostalAddress address);
    
    /**
     * Set the usage type of the bank account.
     * @param type
     */
    void setBankAccountUsageType(BankAccountUsageType usageType);

    /**
     * Get the usage type of the bank account.
     * @return
     */
    BankAccountUsageType getBankAccountUsageType();
}
