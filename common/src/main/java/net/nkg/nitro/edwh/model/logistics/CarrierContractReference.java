package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyGroup;

public interface CarrierContractReference extends BaseModel {

    /**
     * The carrier providing contract references for freight rate trades.
     * @return
     */
    BusinessThirdPartyGroup getCarrier();

    void setCarrier(BusinessThirdPartyGroup carrier);

    /**
     * The global contract references with a description when to use which.
     * @return
     */
    String getInfoTextGlobal();

    void setInfoTextGlobal(String infoTextGlobal);

    /**
     * The contract references for USA with a description when to use which.
     * @return
     */
    String getInfoTextUsa();

    void setInfoTextUsa(String infoTextUsa);

}
