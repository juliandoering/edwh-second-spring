package net.nkg.nitro.edwh.model.quality;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * There is a definition of three types of Delta qualities currently in use:
 * <ul>
 *     <li>Delta High-Grades</li>
 *     <li>Delta Low-Grades for international trade</li>
 *     <li>Delta Low-Grades for local market</li>
 *     </ul>
 * @author niklas
 *
 */
public interface DeltaCoffeeType extends ReferenceData {

}
