package net.nkg.nitro.edwh.model.production;

import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;

/**
 * DWH-221
 */
public interface PhysicalCoffeeStockProcessing extends AnalyticalData {

    /**
     * Returns the GroupCompany
     * @return
     */
    GroupCompany getGroupCompany();

    /**
     * Set the GroupCompany
     * @param company
     */
    void setGroupCompany(GroupCompany company);

    /**
     * Returns the internal ID of the process, given by the Group Company 
     * @return
     */
    String getGroupCompanyInternalProcessingId();

    /**
     * Set the internal ID of the process, given by the Group Company 
     * @return
     */
    void setGroupCompanyInternalProcessingId(String internalId);

    /**
     * Returns the required start date of a process 
     * @return LocalDate
     */
    LocalDate getStartingDate();

    /**
     * Set the required start date of a process 
     * @return LocalDate
     */
    void setStartingDate(LocalDate startingDate);

    /**
     * Returns the closing date, which is optional {when given should be later or equal to start date}
     * @return LocalDate
     */
    LocalDate getClosingDate();

    /**
     * Set the closing date, which is optional {when given should be later or equal to start date} 
     * @return LocalDate
     */
    void setClosingDate(LocalDate closingDate);

    /**
     * Returns the Processing Type.
     * Examples of Processing Types:
     * <ul><b> 
     *      <li>Pre-Cleaning</li>
     *      <li>Cleaning</li>
     *      <li>De-Stoning</li>
     *      <li>Hulling</li>
     *      <li>Polishing</li>
     *      <li>Grading</li>
     *      <li>Density separation</li>
     *      <li>Color Sorting</li>
     *      <li>Wet Polishing</li>
     *      <li>Blending</li>
     *      <li>Decafeination</li>
     *      <li>Vaporization</li>
     * </b></ul>
     * @return
     */
    ProcessingType getProcessingType();

    /**
     * Set the Processing Type.
     * Examples of Processing Types::
     * <ul><b> 
     *      <li>Pre-Cleaning</li>
     *      <li>Cleaning</li>
     *      <li>De-Stoning</li>
     *      <li>Hulling</li>
     *      <li>Polishing</li>
     *      <li>Grading</li>
     *      <li>Density separation</li>
     *      <li>Color Sorting</li>
     *      <li>Wet Polishing</li>
     *      <li>Blending</li>
     *      <li>Decafeination</li>
     *      <li>Vaporization</li>
     *      <li>Transfer</li>
     * </b></ul>
     * @param processingType
     */
    void setProcessingType(ProcessingType processingType);

}
