package net.nkg.nitro.edwh.model.foundation;

import java.text.Normalizer;
import java.util.Locale;
import java.util.Objects;

public final class IndividualBusinessKey {

    private final String normalizedFirstName;
    private final String normalizedLastName;
    private final String citizenshipCountryCode;
    private final int identificationCounter;

    public IndividualBusinessKey(String firstName, String lastName, String countryCode, Integer identificationCounter) {
        this.normalizedFirstName = normalizeName(firstName);
        this.normalizedLastName = normalizeName(lastName);
        this.citizenshipCountryCode = normalizeCountryCode(countryCode);
        this.identificationCounter = identificationCounter != null ? identificationCounter : 1;
    }

    public static String normalizeCountryCode(String countryCode) {
        if (countryCode == null) {
            return "";
        }
        // Convert to upper case
        countryCode = countryCode.toUpperCase(Locale.ROOT);
        // Remove everything except latin capital letters.
        countryCode = countryCode.replaceAll("[^A-Z]", "");

        return countryCode;
    }

    public static String normalizeName(String name) {
        if (name == null) {
            return "";
        }
        // Remove spacing characters at the start and at the end.
        name = name.trim();
        // Replace contiguous spacing characters with one single whitespace.
        name = name.replaceAll("\\s+", " ");
        // Strip most diacritics, like umlauts, accents, etc.
        name = Normalizer.normalize(name, Normalizer.Form.NFD).replaceAll("\\p{M}", "");
        // Convert to lower case.
        name = name.toLowerCase(Locale.ROOT);

        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndividualBusinessKey that = (IndividualBusinessKey) o;
        return
                normalizedFirstName.equals(that.normalizedFirstName) &&
                normalizedLastName.equals(that.normalizedLastName) &&
                citizenshipCountryCode.equals(that.citizenshipCountryCode) &&
                identificationCounter == that.identificationCounter;

    }

    @Override
    public int hashCode() {
        return Objects.hash(normalizedFirstName, normalizedLastName, citizenshipCountryCode, identificationCounter);
    }

    @Override
    public String toString() {
        return "IndividualBusinessKey:" +
                '\t' + normalizedFirstName +
                '\t' + normalizedLastName +
                '\t' + citizenshipCountryCode +
                '\t' + identificationCounter;
    }
}
