package net.nkg.nitro.edwh.model.foundation;

import net.nkg.nitro.edwh.model.common.BaseModel;

/**
 * This entity holds the information about a location {@link FunctionType}.
 */
public interface LocationFunction extends BaseModel {

    /**
     * @return the location {@link FunctionType}
     */
    FunctionType getFunctionType();

    void setFunctionType(FunctionType functionType);
}
