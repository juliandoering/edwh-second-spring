package net.nkg.nitro.edwh.model.profile;

import java.util.Collection;
import java.util.Objects;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.profile.UserRole.EdwhUserRole;

public interface Userprofile extends BaseModel {

    String getUsername();

    void setUsername(String username);

    void setEmail(String email);

    String getEmail();

    /**
     * Get the UUIDs of GroupCompanies that a user has access rights for
     * @return A collection of UUIDs of GroupCompanies the user has *all* rights (Create,Read,Update,Delete) on.
     */
    Collection<String> getAssociatedGroupCompanyUuids();

    void setAssociatedGroupCompanyUuids(Collection<String> companyUuids);

    /**
     * Get the UUID of the Group Company that a user has access rights for in VRD (Reference data for valuation reports). 
     * @return A UUID of the Group Company the user has generally *all* rights (Create,Read,Update,Delete) on.
     */
    String getGroupCompanyUuidForVrd();

    void setGroupCompanyUuidForVrd(String companyUuid);

    /**
     * @return the assigned group of this user profile.
     */
    UserGroup getAssignedGroup();

    void setAssignedGroup(UserGroup group);

    default boolean isAdmin() {
        return getAssignedGroup() != null
            && getAssignedGroup().isAdmin();
    }

    /**
     * Tells us if a Userprofile is associated with a groupcompany provided.
     * This is is based on the company<->userprofile table, but set in the rest auth part.
     * @param groupCompany the group company to check for.
     * @return true if the user is associated with the groupcompany.
     */
    default boolean isGroupCompanyAssigned(GroupCompany groupCompany) {
        if (groupCompany == null)
            return false;

        Collection<String> assigned = getAssociatedGroupCompanyUuids();
        for (String companyUuid : assigned) {
            if (Objects.equals(companyUuid, groupCompany.getId())) {
                return true;
            }
        }
        return false;
    }

    default boolean isRoleAssigned(EdwhUserRole edwhRole) {
        return getAssignedGroup() != null
            && getAssignedGroup().isRoleAssigned(edwhRole);
    }

    String getSubject();

    void setSubject(String subject);
}
