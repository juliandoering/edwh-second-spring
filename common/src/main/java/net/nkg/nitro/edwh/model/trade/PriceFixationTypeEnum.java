package net.nkg.nitro.edwh.model.trade;

/**
 * Previously named Pricing Type!
 */
public enum PriceFixationTypeEnum {

    /*
     * compare https://nitro.nkg.net/confluence/display/CTS/Web+service+in+ Preparation%3A+Entry+of+Sales+Contract+incl.+Allocation plus Differential To Be Fixed (DTBF)
     */
    PTBF("PTBF"),
    OUTRIGHT("Outright"),
    PTF("PTF"),
    DTBF("DTBF"),
    MTBF("MTBF");

    private final String pricingTypeString;

    private PriceFixationTypeEnum(final String text) {
        this.pricingTypeString = text;
    }

    /**
     * Returns the enum type to a string or null if the string does not
     * match any known enum type. The method is case insensitive
     * 
     * @param pricingTypeString
     * @return
     */
    public static PriceFixationTypeEnum getPricingType(String pricingTypeString) {

        if (pricingTypeString != null) {
            // check all possible enum values if they match
            for (PriceFixationTypeEnum pricingTypeEnumVal : PriceFixationTypeEnum.values()) {
                // test if the enum-value matches our given string
                if (pricingTypeString.equalsIgnoreCase(pricingTypeEnumVal.pricingTypeString)) {
                    // we found the right enum-value
                    return pricingTypeEnumVal;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.pricingTypeString;
    }
}
