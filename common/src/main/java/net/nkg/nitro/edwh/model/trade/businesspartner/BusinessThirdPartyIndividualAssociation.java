package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.foundation.Individual;

public interface BusinessThirdPartyIndividualAssociation {

    Relation getRelation();

    void setRelation(Relation relation);

    BusinessThirdParty getBusinessThirdParty();

    void setBusinessThirdParty(BusinessThirdParty businessThirdParty);

    Individual getIndividual();

    void setIndividual(Individual individual);

    /**
     * <ul>
     *     <li>
     *         UBO:
     *         Ultimate beneficiary owners of the company.
     *         All persons indirectly owing 50% or more of the shares of a corporation or other.
     *         Not to be specified in case of sole proprietorship or partnerships.
     *     </li>
     *     <li>
     *         OWNER:
     *         All owners In the case of Partnerships.
     *         Not to be specified in case of sole proprietorship, corporations or other.
     *     </li>
     *     <li>
     *         REPRESENTATIVE:
     *         Management and/or Legal Representatives.
     *         Full names of members of the management and legal representatives (company authorized signatories).
     *         Not to be specified in case of sole proprietorship.
     *     </li>
     *     <li>
     *         CONTACT:
     *         The person to contact when working with this 3rd party.
     *         Full name of individual with whom business contact is held in case they are not part of the management or no legal representatives.
     *         Not to be specified in case of sole proprietorship.
     *     </li>
     * </ul>
     */
    public enum Relation {
        UBO,
        OWNER,
        REPRESENTATIVE,
        CONTACT
    }

}
