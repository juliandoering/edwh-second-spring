package net.nkg.nitro.edwh.model.trade;

import java.util.List;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.quality.ContractCoffeeQualityPercentage;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

public interface ContractCoffeeQualityTheoreticalPercentage extends AnalyticalData {

    Contract getContract();

    void setContract(Contract contract);

    void setUploader(GroupCompany uploader);

    GroupCompany getUploader();

    List<ContractCoffeeQualityPercentage> getCoffeeQualityPercentages();

    void setCoffeeQualityPercentages(List<ContractCoffeeQualityPercentage> contractCoffeeQualityPercentages);

}
