package net.nkg.nitro.edwh.model.risk;

import java.util.List;

import net.nkg.nitro.edwh.model.common.ReferenceData;
import net.nkg.nitro.edwh.model.quality.CoffeeVariety;

/**
 * Business limit type specifies certain volumes or rates.
 */
public interface LimitType extends ReferenceData {

    List<CoffeeVariety> getCoffeeVarieties();

    void setCoffeeVarieties(List<CoffeeVariety> coffeeVariety);

}
