package net.nkg.nitro.edwh.model.hedging;

import java.math.BigDecimal;
import java.time.LocalDate;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;

/**
 * Contains information about an exchange rate.
 * e.g. 1 USD = 0.8618 EUR.
 */
public interface ExchangeRate extends BaseModel {

    /**
     * The currency that should be changed in an exchange.
     * e.g. USD in the case of '1 USD = 0.8618 EUR'.
     */
    CurrencyUnit getCurrencyFrom();

    void setCurrencyFrom(CurrencyUnit currencyFrom);

    /**
     * The currency that should be the result of an exchange.
     * e.g. EUR in the case of '1 USD = 0.8618 EUR'.
     */
    CurrencyUnit getCurrencyTo();

    void setCurrencyTo(CurrencyUnit currencyTo);

    /**
     * The date when this exchange rate was valid.
     */
    LocalDate getSettlementDate();

    void setSettlementDate(LocalDate settlementDate);

    /**
     * The factor that should be used in an exchange.
     * e.g. 0.8618 in the case of '1 USD = 0.8618 EUR'.
     */
    BigDecimal getExchangeRate();

    void setExchangeRate(BigDecimal exchangeRate);
}
