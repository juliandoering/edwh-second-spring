package net.nkg.nitro.edwh.model.trade.businesspartner;

import net.nkg.nitro.edwh.model.common.BaseModel;
import net.nkg.nitro.edwh.model.trade.FixationOption;
import net.nkg.nitro.edwh.model.trade.PriceFixationType;

public interface PriceFixationTerm extends BaseModel {

    PriceFixationType getPriceFixationType();

    void setPriceFixationType(PriceFixationType priceFixationType);

    FixationOption getFixationOption();

    void setFixationOption(FixationOption fixationOption);
}
