package net.nkg.nitro.edwh.model.logistics;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * The ISA standard event code and description.
 */
public interface EventCode extends ReferenceData {

}
