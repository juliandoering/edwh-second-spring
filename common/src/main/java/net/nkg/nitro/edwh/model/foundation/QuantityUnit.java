package net.nkg.nitro.edwh.model.foundation;

import java.math.BigDecimal;

import net.nkg.nitro.edwh.model.common.ReferenceData;

/**
 * Created by nf on 02.11.2016.
 */
public interface QuantityUnit extends ReferenceData {

    /**
     * Returns this weight (1 unit) in kilogram.
     * This is used as a factor to convert quantity units from one to another.
     * @return
     */
    BigDecimal getKg();

    void setKg(BigDecimal kg);

}
