package functions;

import javassist.ClassPool;
import javassist.CtMethod;
import javassist.LoaderClassPath;
import javassist.NotFoundException;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InterfaceExtractor {

    public static String extractFullFieldDeclaration(Class<?> interfaceType) {
        var fieldTypes = extractFieldTypes(interfaceType);
        var fieldNames = extractFieldNames(interfaceType);
        return StreamFunctions.zip(fieldTypes, fieldNames, (ft, fn) -> String.format("private %s %s;", ft, fn))
                .collect(Collectors.joining("\n\n"));
    }

    public static Stream<String> extractFieldTypes(Class<?> interfaceType) {
        return extractGetMethods(interfaceType)
                .map(method -> {
                    if (method.getReturnType().getSimpleName().equals("List"))
                        return getListType(method);
                    else
                        return getNonListType(method);
                });
    }

    private static String getNonListType(Method method) {
        return method.getReturnType().getSimpleName();
    }

    private static String getListType(Method method) {
        var genericListType = ((ParameterizedType)method.getGenericReturnType()).getActualTypeArguments()[0];
        var typeNameSplit = genericListType.getTypeName().split("\\.");
        return String.format("List<%s>", typeNameSplit[typeNameSplit.length -1]);
    }

    public static Stream<String> extractFieldNames(Class<?> interfaceType) {
        return extractGetMethods(interfaceType)
                .map(Method::getName)
                .map(s -> s.startsWith("get") ? s.substring(3) : s.substring(2))
                .map(s -> s.substring(0, 1).toLowerCase().concat(s.substring(1)));
    }

    public static Stream<Method> extractGetMethods(Class<?> interfaceType) {
        var methodToNames = Arrays.stream(interfaceType.getDeclaredMethods())
                .filter(method -> method.getName().startsWith("get") || method.getName().startsWith("is"))
                .filter(method -> !method.isDefault())
                .collect(Collectors.toMap(Method::getName, m -> m));
        return getOrderedMethods(interfaceType)
                .filter(methodToNames::containsKey)
                .map(methodToNames::get);
    }

    private static Stream<String> getOrderedMethods(Class<?> interfaceType) {
        try {
            var pool = new ClassPool();
            pool.appendClassPath(new LoaderClassPath(interfaceType.getClassLoader()));
            var clazz = pool.get(interfaceType.getName());

            return Arrays.stream(clazz.getDeclaredMethods()).map(CtMethod::getName);
        } catch (NotFoundException e) {
            return Stream.empty();
        }
    }

}
