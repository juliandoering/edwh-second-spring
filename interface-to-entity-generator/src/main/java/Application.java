import functions.InterfaceExtractor;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.hedging.TerminalMarketOptionTrade;
import net.nkg.nitro.edwh.model.logistics.OceanFreightRate;
import net.nkg.nitro.edwh.model.quality.CoffeeQuality;
import net.nkg.nitro.edwh.model.quality.CoffeeQualityPercentage;
import net.nkg.nitro.edwh.model.trade.PhysicalCoffeeStock;
import net.nkg.nitro.edwh.model.trade.businesspartner.*;
import net.nkg.nitro.edwh.model.trade.contract.Contract;

public class Application {

    public static void main(String[] args) {
        var fieldDeclaration = InterfaceExtractor.extractFullFieldDeclaration(Contract.class);
        System.out.println(fieldDeclaration);
    }

}
