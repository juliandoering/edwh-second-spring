package metadata;

import java.util.*;
import java.util.stream.Collectors;

public class TablesMetaData {

    private final String schema;
    private final List<TableMetaData> metaData;
    private final Map<String, TableMetaData> metaDataToTableName;

    public TablesMetaData(String schema, List<TableMetaData> metaData) {
        this.schema = schema;
        this.metaData = metaData;
        metaDataToTableName = metaData.stream().collect(Collectors.toMap(TableMetaData::getName, t -> t));
    }

    public List<TableMetaData> getData() {
        return metaData;
    }

    public boolean containsTable(String tableName) {
        return metaDataToTableName.containsKey(tableName);
    }

    public TableMetaData getByName(String name) {
        return metaDataToTableName.get(name);
    }

    public Collection<String> getNames() {
        return metaDataToTableName.keySet();
    }

    public String getSchema() {
        return schema;
    }
}
