package metadata;

import query.helper.QueryExecuter;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

public class TableMetaDataExtractor {

    private final Connection connection;

    public TableMetaDataExtractor(Connection connection) {
        this.connection = connection;
    }

    public static TablesMetaData extractTableMetaDataOnlyNames(DatabaseMetaData metaData,
                                                               String schema,
                                                               String tableNamePattern) throws SQLException {
        var tables = new ArrayList<TableMetaData>();
        var tablesMetaData = metaData
                .getTables(null, schema, tableNamePattern, new String[]{"TABLE"});
        while (tablesMetaData.next()) {
            var name = tablesMetaData.getString("TABLE_NAME");
            tables.add(new TableMetaData(name));
        }
        return new TablesMetaData(schema, tables);
    }

    public TablesMetaData extractTables(String schema) throws SQLException {
        return extractTables(schema, "%");
    }

    public TablesMetaData extractTables(String schema, String tableNamePattern) throws SQLException {
        var metaData = connection.getMetaData();
        var tablesMetaData = extractTableMetaDataOnlyNames(metaData, schema, tableNamePattern);

        var tableMap = tablesMetaData.getData().stream()
                .collect(Collectors.toMap(TableMetaData::getName, t -> t));

        for (var tableEntry : tableMap.entrySet()) {
            setColumnsMetaData(schema, metaData, tableEntry);

            if (tableEntry.getValue().containsColumn(TableMetaData.ID_COLUMN)) {
                tableEntry.getValue().setMaxId(getMaxId(schema, tableEntry.getKey()));
            }

            setForeignTablesMetaData(schema, metaData, tableMap, tableEntry);
        }

        return tablesMetaData;
    }

    private long getMaxId(String schema, String table) throws SQLException {
        var maxIdQuery = String.format("select max(id) + 1 from %s.%s;", schema, table);
        return QueryExecuter.executeScalar(connection, maxIdQuery, rs -> rs.getLong(1));
    }

    private void setColumnsMetaData(String schema,
                                    DatabaseMetaData metaData,
                                    Map.Entry<String, TableMetaData> tableEntry) throws SQLException {
        var columnsMetaData = metaData
                .getColumns(null, schema, tableEntry.getKey(), "%");
        var tableMetaData = tableEntry.getValue();
        while (columnsMetaData.next()) {
            tableMetaData.addColumnName(columnsMetaData.getString("COLUMN_NAME"));
        }
    }

    private void setForeignTablesMetaData(String schema,
                                          DatabaseMetaData metaData,
                                          Map<String, TableMetaData> tableMap,
                                          Map.Entry<String, TableMetaData> tableEntry) throws SQLException {
        var fkResultSet = metaData.getImportedKeys(null, schema, tableEntry.getKey());
        while (fkResultSet.next()) {
            tableEntry.getValue().addForeignTable(
                    fkResultSet.getString("FKCOLUMN_NAME"),
                    tableMap.get(fkResultSet.getString("PKTABLE_NAME"))
            );
        }
    }
}
