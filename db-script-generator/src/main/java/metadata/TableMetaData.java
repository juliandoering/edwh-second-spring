package metadata;

import java.util.*;
import java.util.stream.Stream;

public class TableMetaData {

    public static final String UUID_COLUMN = "uuid";
    public static final String ID_COLUMN = "id";
    public static final String IS_VERSION_COLUMN = "is_version";
    public static final String PARENT_COLUMN = "parent";
    public static final String DELETED_COLUMN = "deleted";

    private final String name;
    private long maxId;
    private final List<String> columnNames;
    private final HashSet<String> columnSet;

    private Map<String, TableMetaData> foreignTables;

    public TableMetaData(String name) {
        this.name = name;
        columnNames = new ArrayList<>();
        columnSet = new HashSet<>();
        foreignTables = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public long getMaxId() {
        return maxId;
    }

    public void setMaxId(long maxId) {
        this.maxId = maxId;
    }

    public List<String> getColumnNames() {
        return columnNames;
    }

    public void addColumnName(String name) {
        columnNames.add(name);
        columnSet.add(name);
    }

    public boolean containsColumn(String name) {
        return columnSet.contains(name);
    }

    public String getIdType() {
        return containsColumn(UUID_COLUMN) ? "uuid" : "id";
    }

    public Map<String, TableMetaData> getForeignTables() {
        return foreignTables;
    }

    public void addForeignTable(String foreignKeyColumn, TableMetaData referencedTableMetaData) {
        foreignTables.put(foreignKeyColumn, referencedTableMetaData);
    }
}
