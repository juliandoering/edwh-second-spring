import query.ExecutionPhase;

public class Application {

    private static final String table = "individual";
    private static final String mdmDir = "C:\\Projekte\\edwh-second-spring\\mdm";
    private static final String dwhDir = "C:\\Projekte\\edwh-second-spring\\dwh";

    public static void main(String[] args) {
        var dependencyInjector = new DependencyInjector();
        //writeFilesStepOne(dependencyInjector);
        //writeFilesStepTwo(dependencyInjector);
        //writeFilesStepThree(dependencyInjector);
        dwhWriteFilesStepOne(dependencyInjector);
    }

    private static void writeFilesStepOne(DependencyInjector dependencyInjector) {
        dependencyInjector.writeFiles(mdmDir,
                ExecutionPhase.ONE,
                ExecutionPhase.TWO,
                ExecutionPhase.THREE
        );
    }

    private static void writeFilesStepTwo(DependencyInjector dependencyInjector) {
        dependencyInjector.writeEnversSchemaScript(mdmDir);
    }

    private static void writeFilesStepThree(DependencyInjector dependencyInjector) {
        dependencyInjector.writeFiles(mdmDir,
                ExecutionPhase.FIVE_HIS_PREP,
                ExecutionPhase.SIX_HIS,
                ExecutionPhase.SEVEN_HIS_CLEAN
        );
    }

    private static void dwhWriteFilesStepOne(DependencyInjector dependencyInjector) {
        dependencyInjector.writeFilesDwh(dwhDir);
    }

}
