import metadata.TableMetaDataExtractor;
import query.ExecutionPhase;
import query.builder.DwhTableQueryBuilder;
import query.builder.FkQueryBuilder;
import query.builder.TableQueryBuilder;
import query.collection.HistoricQueriesFactory;
import query.collection.Queries;
import query.collection.QueriesFactory;
import query.collection.TableQueriesFactory;
import query.helper.QueryExecuter;
import query.helper.QueryFileWriter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DependencyInjector {

    private static final String URL = "jdbc:postgresql://edwh-database.private.edwh.nkg.net:5432/edwh?user=mdm&password=mrpersister";
    private static final String MDM = "mdm";
    private static final String DWH = "dwh";

    public void writeFiles(String mdmDir, ExecutionPhase... executionPhases) {
        try {
            var defaultQueries = createMdmQueries();
            var historicQueries = createMdmHistoricQueries();
            var queries = Queries.merge(defaultQueries, historicQueries);
            QueryFileWriter.writeToFiles(mdmDir, queries, executionPhases);
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    public void writeFilesHistoric(String dir) {
        try {
            var historicQueries = createMdmHistoricQueries();
            QueryFileWriter.writeToFiles(dir, historicQueries,
                    ExecutionPhase.FIVE_HIS_PREP, ExecutionPhase.SIX_HIS, ExecutionPhase.SEVEN_HIS_CLEAN
            );
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    public void writeFilesDwh(String dir) {
        try {
            var queries = createDwhQueries();
            QueryFileWriter.writeToFiles(dir, queries, ExecutionPhase.ONE);
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    public void writeEnversSchemaScript(String mdmDir) {
        try {
            QueryFileWriter.writeCurrentEnversHistoricQueryFile(mdmDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printQueriesDefault() {
        try {
            var queries = createMdmQueries();
            System.out.println(queries);
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    public void printQueriesHistoric() {
        try {
            var queries = createMdmHistoricQueries();
            System.out.println(queries);
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    public void printQuery(String table) {
        try {
            var queries = createMdmQueries();
            var query = queries.getForTableName(table);

            query.ifPresent(System.out::println);
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    public void executeQuery(String table, ExecutionPhase executionPhase) {
        try {
            var conn = getConnection();
            var queries = createMdmQueries();
            var query = queries.getForTableName(table);

            query.ifPresent(q -> QueryExecuter.executeInTransaction(conn, q.getExecutionPhaseQueryString(executionPhase)));
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }

    private Queries createMdmQueries() throws SQLException {
        return createMdmTableQueriesFactory().create(MDM);
    }

    private Queries createDwhQueries() throws SQLException {
        return createDwhTableQueriesFactory().create(DWH);
    }

    private Queries createMdmHistoricQueries() throws SQLException {
        return createHistoricQueriesFactory().create(MDM);
    }

    private QueriesFactory createDwhTableQueriesFactory() throws SQLException {
        return new TableQueriesFactory(
                createTableMetaDataExtractor(),
                new DwhTableQueryBuilder()
        );
    }

    private QueriesFactory createMdmTableQueriesFactory() throws SQLException {
        return new TableQueriesFactory(
                createTableMetaDataExtractor(),
                new TableQueryBuilder(),
                new FkQueryBuilder()
        );
    }

    private QueriesFactory createHistoricQueriesFactory() throws SQLException {
        return new HistoricQueriesFactory(createTableMetaDataExtractor());
    }

    private TableMetaDataExtractor createTableMetaDataExtractor() throws SQLException {
        return new TableMetaDataExtractor(getConnection());
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL);
    }
}
