package query;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Query {

    private final String name;
    private final Map<QueryType, List<String>> queriesToType;

    public Query(String name) {
        this.name = name;
        queriesToType = QueryType.stream()
                .collect(Collectors.toMap(t -> t, t -> new ArrayList<>()));
    }

    public void addSubquery(QueryType queryType, String query) {
        queriesToType.get(queryType).add(query);
    }

    public boolean hasEntries(ExecutionPhase executionPhase) {
        return queriesToType.entrySet().stream()
                .filter(e -> e.getKey().getPhase() == executionPhase)
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .findAny()
                .isPresent();
    }

    public boolean hasEntries(QueryType queryType) {
        return !queriesToType.get(queryType).isEmpty();
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return joinSubqueries("\n\n");
    }

    public String joinSubqueries(String delimiter) {
        return QueryType.stream()
                .filter(queriesToType::containsKey)
                .map(queriesToType::get)
                .flatMap(Collection::stream)
                .collect(Collectors.joining(delimiter));
    }

    public String getExecutionPhaseQueryString(ExecutionPhase phase) {
        return getExecutionPhaseQueryStream(phase)
                .collect(Collectors.joining("\n\n"));
    }

    public List<String> getExecutionPhaseQueryLines(ExecutionPhase phase) {
        return getExecutionPhaseQueryStream(phase)
                .map(q -> q.split("\n"))
                .flatMap(Arrays::stream)
                .collect(Collectors.toList());
    }

    private Stream<String> getExecutionPhaseQueryStream(ExecutionPhase phase) {
        return QueryType.stream()
                .filter(queryType -> queryType.getPhase() == phase)
                .filter(queriesToType::containsKey)
                .map(queriesToType::get)
                .flatMap(Collection::stream);
    }

    public static Query merge(Query q1, Query q2) {
        var query = new Query(q1.name);
        for (var entry : q1.queriesToType.entrySet()){
            query.queriesToType.get(entry.getKey()).addAll(entry.getValue());
        }
        for (var entry : q2.queriesToType.entrySet()){
            query.queriesToType.get(entry.getKey()).addAll(entry.getValue());
        }
        return query;
    }
}
