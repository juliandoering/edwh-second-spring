package query;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Values are ordered by the order of execution in their respective execution phase
 */
public enum QueryType {

    CREATE_NEW_TABLE(ExecutionPhase.ONE),
    CHANGE_FK_COLUMN_TYPE_TO_VARCHAR(ExecutionPhase.ONE),
    INSERT_DATA(ExecutionPhase.ONE),
    UPDATE_FK_REFERENCE_TO_UUID(ExecutionPhase.ONE),
    DROP_VERSIONING_COLUMNS(ExecutionPhase.ONE),
    DROP_DELETED(ExecutionPhase.ONE),
    DWH_CREATE_NEW_TABLE(ExecutionPhase.ONE),
    DWH_ADD_ID_SEQUENCE(ExecutionPhase.ONE),
    DWH_DROP_FK_VERSION_COLUMN(ExecutionPhase.ONE),
    DWH_RENAME_FK_COLUMN(ExecutionPhase.ONE),
    DWH_DROP_UUID_COLUMN(ExecutionPhase.ONE),
    DWH_REPLACE_VERSION_COLUMN(ExecutionPhase.ONE),
    REPLACE_ID(ExecutionPhase.TWO),
    ADD_PK(ExecutionPhase.TWO),
    ADD_ID_SEQUENCE(ExecutionPhase.THREE),
    ADD_FK_CONSTRAINT(ExecutionPhase.THREE),
    HIS_GENERAL_PREPARATION(ExecutionPhase.FIVE_HIS_PREP),
    HIS_GENERAL_CREATE_REVINFO_TMP(ExecutionPhase.FIVE_HIS_PREP),
    HIS_GENERAL_UPDATE_REVINFO(ExecutionPhase.FIVE_HIS_PREP),
    HIS_UPDATE_TABLES(ExecutionPhase.SIX_HIS),
    HIS_GENERAL_CLEAN_UP(ExecutionPhase.SEVEN_HIS_CLEAN),
    HIS_FK_JOIN_SUBQUERY(ExecutionPhase.NONE),
    HIS_TABLE_UNION_SUBQUERY(ExecutionPhase.NONE);

    private final ExecutionPhase phase;

    QueryType(ExecutionPhase executionPhase) {
        this.phase = executionPhase;
    }

    public ExecutionPhase getPhase() {
        return phase;
    }

    public static Stream<QueryType> stream() {
        return Arrays.stream(QueryType.values());
    }
}
