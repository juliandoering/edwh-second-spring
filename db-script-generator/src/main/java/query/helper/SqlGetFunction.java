package query.helper;

@FunctionalInterface
public interface SqlGetFunction<T, R, E extends Exception> {
    R apply(T t) throws E;
}
