package query.helper;

import org.apache.commons.io.FileUtils;
import query.ExecutionPhase;
import query.collection.Queries;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class QueryFileWriter {

    private static final String migrationDir = "src\\main\\resources\\scripts\\sql_scripts\\7.0.0_spring_test\\010_migration";
    private static final String revinfoColumn = "revinfo";

    public static void writeToFiles(String baseDir, Queries queries, ExecutionPhase... executionPhases) throws IOException {
        for (var phase : executionPhases) {
            var dir = createDropDirectory(baseDir, phase);

            for (var query : queries.getQueries()) {
                var queryLines = query.getExecutionPhaseQueryLines(phase);

                if (queryLines.size() > 0) {
                    var fileName = String.format("0%d0_migrate_%s.sql", phase.asNumber(), query.getName());
                    var path = Paths.get(dir.toString(), fileName);
                    Files.write(path, queryLines, StandardCharsets.UTF_8);
                    System.out.printf("Successfully written %s\n", fileName);
                }
            }
        }
    }

    public static void writeCurrentEnversHistoricQueryFile(String mdmDir) throws IOException {
        var phase = ExecutionPhase.FOUR_ENVERS;
        var writeToDir = createDropDirectory(mdmDir, phase);
        var hibernateSchemaPath = Paths.get(mdmDir, "build\\generated_scripts\\hibernate_schema.sql");

        var filteredHistoricLines = Files.readAllLines(hibernateSchemaPath).stream()
                .filter(l -> l.contains("mdm_historic"))
                .distinct()
                .map(l -> l.concat(";"))
                .collect(Collectors.toList());
        var linesToTableNameMap = getLinesToTableName(filteredHistoricLines);

        for (var linesToTableName : linesToTableNameMap.entrySet()) {
            var executeRevInfoFirst = linesToTableName.getKey().equals(revinfoColumn) ? 0 : 1;
            var fileName = String.format("0%d%d_create_%s.sql",
                    phase.asNumber(), executeRevInfoFirst, linesToTableName.getKey()
            );
            var writePath = Paths.get(writeToDir.toString(), fileName);
            Files.write(writePath, linesToTableName.getValue(), StandardCharsets.UTF_8);
            System.out.printf("Successfully written %s\n", fileName);
        }
    }

    private static HashMap<String, List<String>> getLinesToTableName(List<String> lines) {
        var linesToTable = new HashMap<String, List<String>>();
        for (var line : lines) {
            var tableName = getTableNameInLine(line);
            if (linesToTable.containsKey(tableName)) {
                linesToTable.get(tableName).add(line);
            } else {
                linesToTable.put(tableName, new ArrayList<>(Arrays.asList(line)));
            }
        }
        return linesToTable;
    }

    private static String getTableNameInLine(String line) {
        var split = line.split("\\."); //first '.' in lines: mdm_historic.tableName
        var startsWithTable = split[1]; //take the second part containing the tableName
        var tableSplit = startsWithTable.split(" "); //splits after tableName
        return tableSplit[0];
    }

    private static Path createDropDirectory(String baseDir, ExecutionPhase phase) throws IOException {
        var dir = Paths.get(baseDir, migrationDir, phase.getName());
        createDirectoryIfNotExists(dir.getParent());
        createDirectoryIfNotExists(dir);
        FileUtils.cleanDirectory(new File(dir.toString()));
        return dir;
    }

    private static void createDirectoryIfNotExists(Path path) throws IOException {
        try {
            Files.createDirectory(path);
        } catch (FileAlreadyExistsException ignored){}
    }

}
