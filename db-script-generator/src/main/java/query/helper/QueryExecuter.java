package query.helper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QueryExecuter {

    public static void executeInTransaction(Connection connection, String query) {
        try (var statement = connection.createStatement()) {
            query = String.format("begin;\n%s\ncommit;", query);
            statement.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static <T> T executeScalar(Connection connection, String query, SqlGetFunction<ResultSet, T, SQLException> getOperation)
            throws SQLException {
        try (var statement = connection.createStatement()) {
            var rs = statement.executeQuery(query);
            rs.next();
            return getOperation.apply(rs);
        }
    }
}
