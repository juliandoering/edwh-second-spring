package query.helper;

public class QueryStrings {
    /*
    Strings to replace

    {schema} = schema
    {table} = original table name
    {table_old} = table name with suffix _old
    {table_new} = table name with suffix _tmp
    {table_his} = table name with suffix _his
    {id_type} = type of the id column (id/uuid)
    {table_max_id} = max value of column id used to recreate id sequence
    {column_names} = table's column names
    {filter_expression} = used if a query needs complicated filters
    {fk_name} = name of the foreign key
    {fk_column} = foreign key column name
    {fk_column_new} = foreign key column new name (replace id with uuid)
    {fk_id_type} = type of the fk column (id/uuid)
    {fk_reference_table} = table referenced by foreign key
     */

    public static final String newSchemaSuffix = "_spring";
    public static final String hisSchemaSuffix = "_audit";
    public static final String hisTableSuffix = "_log";

    //Execution Phase 1

    public static final String mdm_tbl_createNewTable =
            "create table {schema_new}.{table} as table {schema}.{table} with no data;";

    public static final String mdm_tbl_insertData =
            "insert into {schema_new}.{table} select * from {schema}.{table}{filter_expression};";

    public static final String mdm_fk_changeFkColumnTypeToVarchar =
            "alter table {schema_new}.{table} alter column {fk_column} type varchar;";

    public static final String mdm_fk_updateFkReferenceToUuid =
            "update {schema_new}.{table}\n" +
            "set {fk_column} = (select ref.uuid from {schema}.{fk_reference_table} ref where ref.id = {fk_column}::bigint)\n" +
            "where {fk_column} is not null;";

    public static final String mdm_tbl_dropVersioningColumns =
            "alter table {schema_new}.{table}\n" +
            "   drop column if exists is_version,\n" +
            "   drop column if exists parent,\n" +
            "   drop column if exists record_version,\n" +
            "   drop column if exists deleted;";


    public static final String dwh_tbl_createNewTable =
            "create table {schema_new}.{table} as table {schema}.{table};\n" +
            "alter table {schema_new}.{table} add primary key (id);";

    public static final String dwh_tbl_addIdSequence =
            "drop sequence if exists {schema_new}.{table}_id_seq cascade;\n" +
            "create sequence {schema_new}.{table}_id_seq owned by {schema_new}.{table}.id;\n" +
            "select setval('{schema_new}.{table}_id_seq', {table_max_id} + 1, false) FROM {schema_new}.{table};\n" +
            "alter table {schema_new}.{table}\n" +
            "   alter column id set default nextval('{schema_new}.{table}_id_seq');";

    public static final String dwh_fk_dropVersionColumn =
            "alter table {schema_new}.{table} drop column if exists {fk_column_version};";

    public static final String dwh_fk_renameColumn =
            "alter table {schema_new}.{table} rename column {fk_column} to {fk_column_new};";

    public static final String dwh_tbl_dropUuidColumn =
            "alter table {schema_new}.{table} drop column uuid;";

    public static final String dwh_tbl_replaceVersionColumn =
            "alter table {schema_new}.{table} drop column record_version;\n" +
            "alter table {schema_new}.{table} add column version bigint not null default 0;";


    //Execution Phase 2

    public static final String mdm_fk_changeFkColumnTypeToUuid =
            "alter table {schema_new}.{table} alter column {fk_column} type uuid;";

    public static final String mdm_tbl_replaceId =
            "alter table {schema_new}.{table} drop column id;\n" +
            "alter table {schema_new}.{table} rename column uuid to id;";

    public static final String mdm_tbl_addPk =
            "alter table {schema_new}.{table} add primary key (id);";
    //TODO alter column id to type uuid


    //Execution Phase 3

    public static final String mdm_tbl_addIdSequence =
            "drop sequence if exists {schema_new}.{table}_id_seq cascade;\n" +
            "create sequence {schema_new}.{table}_id_seq owned by {schema_new}.{table}.id;\n" +
            "select setval('{schema_new}.{table}_id_seq', {table_max_id} + 1, false) FROM {schema_new}.{table};\n" +
            "alter table {schema_new}.{table}\n" +
            "   alter column id set default nextval('{schema_new}.{table}_id_seq'),\n" +
            "   alter column id set not null;";

    public static final String mdm_fk_addFkConstraint =
            "alter table {schema_new}.{table}\n" +
            "   add constraint {fk_name}\n" +
            "   foreign key ({fk_column})\n" +
            "       references {schema_new}.{fk_reference_table}(id);\n" +
            "drop index if exists idx_{table}_{fk_column} cascade;\n" +
            "create index idx_{table}_{fk_column} on {schema_new}.{table} ({fk_column});";


    //Execution Phase 4: Envers historical schema script


    //Execution Phase 5: Historic preparation

    public static final String mdm_his_gen_preparation =
            "create table {schema_his}.revinfo_tmp (\n" +
            "    id bigint not null,\n" +
            "    uuid varchar not null,\n" +
            "    rev int not null,\n" +
            "    revtype smallint not null,\n" +
            "    timestmp bigint not null\n" +
            ");\n" +
            "create sequence {schema_his}.envers_rev_seq;";

    public static final String mdm_his_gen_createRevInfoTmp =
            "insert into {schema_his}.revinfo_tmp\n" +
            "select nested.id, nested.uuid, nextval('{schema_his}.envers_rev_seq'), nested.revtype, EXTRACT(EPOCH FROM nested.updated_at) from\n" +
            "(\n" +
            "{table_union_subqueries}\n" +
            "order by updated_at) as nested;";

    public static final String mdm_his_tableUnionSubquery =
            "/*{table}*/\n" +
            "select id, uuid, 0 as revtype, updated_at from (\n" +
            "   select id, uuid, updated_at, min(record_version) over (partition by uuid) as min_record_version from {schema}.{table}\n" +
            ") as tmp1 where is_version=true and min_record_version = record_version\n" +
            "union select id, uuid, 1 as revtype, updated_at from (\n" +
            "   select id, uuid, updated_at, min(record_version) over (partition by uuid) as min_record_version from {schema}.{table}\n" +
            ") as tmp2 where deleted=false and min_record_version != record_version\n" +
            "union select id, uuid, 2 as revtype, updated_at from {schema}.{table} where deleted=true";

    public static final String mdm_his_gen_updateRevInfo =
            "insert into {schema_his}.revinfo\n" +
            "select rev, timestmp from {schema_his}.revinfo_tmp;";


    //Execution Phase 6: Historic

    public static final String mdm_his_updateTables =
            "insert into {schema_his}.{table_his}\n" +
            "select old.uuid, ri.rev, ri.revtype, {his_column_names} from {schema}.{table} old\n" +
            "inner join {schema_his}.revinfo_tmp ri on old.id=ri.id and old.uuid=ri.uuid\n" +
            "{his_fk_joins};";

    public static final String mdm_his_updateTable_fkJoinSubquery =
            "left join (select id, uuid from {schema}.{fk_reference_table}) {fk_ref_name}\n" +
            "   on old.{fk_column}={fk_ref_name}.id";


    //Execution Phase 7: Historic clean

    public static final String mdm_his_gen_cleanUpTmp =
            "drop table if exists {schema_his}.revinfo_tmp;\n" +
            "drop sequence if exists {schema_his}.envers_rev_seq;";


    public static String newSchemaNameFor(String schema) {
        return schema.concat(newSchemaSuffix);
    }

    public static String hisSchemaNameFor(String schema) {
        return schema.concat(hisSchemaSuffix);
    }

    public static String hisTableNameFor(String table) {
        return table.concat(hisTableSuffix);
    }

    public static String tableNameWithoutSuffix(String table) {
        return table.replace(hisTableSuffix, "");
    }

    public static String replaceIdWithUuid(String s) {
        return s.replace("_id", "_uuid");
    }

    public static String replaceUuidWithId(String s) {
        return s.replace("_uuid", "_id");
    }

    public static String convertToFkName(String fkColumn) {
        return "fk_".concat(fkColumn.replace("_id", ""));
    }

    public static String convertToFkColumnVersion(String fkColumn) {
        return fkColumn.replace("_uuid", "_version");
    }
}