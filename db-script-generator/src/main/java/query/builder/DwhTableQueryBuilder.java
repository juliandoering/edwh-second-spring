package query.builder;

import metadata.TableMetaData;
import query.Query;
import query.QueryType;
import query.helper.QueryStrings;
import query.parameter.QueryParameter;
import query.parameter.QueryParameterKey;

public class DwhTableQueryBuilder implements QueryBuilder {

    @Override
    public Query build(QueryParameter queryParameter, TableMetaData tableMetaData) {
        var query = new Query(tableMetaData.getName());

        query.addSubquery(QueryType.DWH_CREATE_NEW_TABLE,
                queryParameter.replaceKeys(QueryStrings.dwh_tbl_createNewTable));
        query.addSubquery(QueryType.DWH_ADD_ID_SEQUENCE,
                queryParameter.replaceKeys(QueryStrings.dwh_tbl_addIdSequence));
        query.addSubquery(QueryType.DWH_DROP_UUID_COLUMN,
                queryParameter.replaceKeys(QueryStrings.dwh_tbl_dropUuidColumn));
        query.addSubquery(QueryType.DWH_REPLACE_VERSION_COLUMN,
                queryParameter.replaceKeys(QueryStrings.dwh_tbl_replaceVersionColumn));

        for (var column : tableMetaData.getColumnNames()) {
            if (column.endsWith("_uuid")) {
                queryParameter.put(QueryParameterKey.FK_COLUMN, column);
                queryParameter.put(QueryParameterKey.FK_COLUMN_NEW, QueryStrings.replaceUuidWithId(column));
                queryParameter.put(QueryParameterKey.FK_COLUMN_VERSION, QueryStrings.convertToFkColumnVersion(column));

                query.addSubquery(QueryType.DWH_DROP_FK_VERSION_COLUMN,
                        queryParameter.replaceKeys(QueryStrings.dwh_fk_dropVersionColumn));
                query.addSubquery(QueryType.DWH_RENAME_FK_COLUMN,
                        queryParameter.replaceKeys(QueryStrings.dwh_fk_renameColumn));
            }
        }

        return query;
    }

}
