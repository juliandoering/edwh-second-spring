package query.builder;

import metadata.TableMetaData;
import query.Query;
import query.parameter.QueryParameter;

public interface QueryBuilder {
    Query build(QueryParameter queryParameter, TableMetaData tableMetaData);
}
