package query.builder;

import metadata.TableMetaData;
import query.Query;
import query.parameter.QueryParameter;
import query.parameter.QueryParameterKey;
import query.helper.QueryStrings;
import query.QueryType;

public class FkQueryBuilder implements QueryBuilder {

    @Override
    public Query build(QueryParameter queryParameter, TableMetaData tableMetaData) {
        var query = new Query(tableMetaData.getName());

        for (var foreignTableMetaData : tableMetaData.getForeignTables().entrySet()) {
            var foreignKey = foreignTableMetaData.getKey();
            var referencedTable = foreignTableMetaData.getValue();

            queryParameter.put(QueryParameterKey.FK_NAME, QueryStrings.convertToFkName(foreignKey));
            queryParameter.put(QueryParameterKey.FK_COLUMN, foreignKey);
            queryParameter.put(QueryParameterKey.FK_REFERENCE_TABLE, referencedTable.getName());
            queryParameter.put(QueryParameterKey.FK_ID_TYPE, referencedTable.getIdType());

            if (referencedTable.containsColumn(TableMetaData.UUID_COLUMN)) {
                query.addSubquery(QueryType.CHANGE_FK_COLUMN_TYPE_TO_VARCHAR,
                        getChangeFkColumnTypeToVarcharQuery(queryParameter));
                query.addSubquery(QueryType.UPDATE_FK_REFERENCE_TO_UUID,
                        getUpdateFkReferenceToUuidQuery(queryParameter));
            }

            query.addSubquery(QueryType.ADD_FK_CONSTRAINT, getAddFkConstraintQuery(queryParameter));
        }

        return query;
    }

    private String getChangeFkColumnTypeToVarcharQuery(QueryParameter queryParameter) {
        var query = QueryStrings.mdm_fk_changeFkColumnTypeToVarchar;
        return queryParameter.replaceKeys(query);
    }

    private String getUpdateFkReferenceToUuidQuery(QueryParameter queryParameter) {
        var query = QueryStrings.mdm_fk_updateFkReferenceToUuid;
        return queryParameter.replaceKeys(query);
    }

    private String getAddFkConstraintQuery(QueryParameter queryParameter) {
        var query = QueryStrings.mdm_fk_addFkConstraint;
        return queryParameter.replaceKeys(query);
    }
}
