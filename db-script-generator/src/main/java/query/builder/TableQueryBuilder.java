package query.builder;

import metadata.TableMetaData;
import query.Query;
import query.helper.QueryStrings;
import query.QueryType;
import query.parameter.QueryParameter;
import query.parameter.QueryParameterKey;

import java.util.ArrayList;

public class TableQueryBuilder implements QueryBuilder {

    @Override
    public Query build(QueryParameter queryParameter, TableMetaData tableMetaData) {
        var query = new Query(tableMetaData.getName());

        query.addSubquery(QueryType.CREATE_NEW_TABLE, queryParameter.replaceKeys(QueryStrings.mdm_tbl_createNewTable));
        query.addSubquery(QueryType.ADD_PK, queryParameter.replaceKeys(QueryStrings.mdm_tbl_addPk));
        query.addSubquery(QueryType.INSERT_DATA, getInsertDataQuery(queryParameter, tableMetaData));
        query.addSubquery(QueryType.DROP_VERSIONING_COLUMNS,
                queryParameter.replaceKeys(QueryStrings.mdm_tbl_dropVersioningColumns));

        if (tableMetaData.containsColumn(TableMetaData.UUID_COLUMN)) {
            query.addSubquery(QueryType.REPLACE_ID, queryParameter.replaceKeys(QueryStrings.mdm_tbl_replaceId));
        } else {
            query.addSubquery(QueryType.ADD_ID_SEQUENCE, queryParameter.replaceKeys(QueryStrings.mdm_tbl_addIdSequence));
        }

        return query;
    }

    private String getInsertDataQuery(QueryParameter queryParameter, TableMetaData tableMetaData) {
        var query = QueryStrings.mdm_tbl_insertData;
        queryParameter.put(QueryParameterKey.FILTER_EXPRESSION, getFilterExpression(tableMetaData));
        return queryParameter.replaceKeys(query);
    }

    private String getFilterExpression(TableMetaData tableMetaData) {
        var filterExpressions = new ArrayList<String>();
        if (tableMetaData.containsColumn(TableMetaData.IS_VERSION_COLUMN)) {
            filterExpressions.add("is_version=false");
        }
        if (tableMetaData.containsColumn(TableMetaData.DELETED_COLUMN)) {
            filterExpressions.add("deleted=false");
        }
        if (filterExpressions.size() > 0) {
            return " where ".concat(String.join(" and ", filterExpressions));
        }
        return "";
    }

}
