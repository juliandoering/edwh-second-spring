package query;

import java.util.Arrays;
import java.util.stream.Stream;

public enum ExecutionPhase {
    NONE(0, ""),
    ONE(1, "%s_%d"),
    TWO(2, "%s_%d"),
    THREE(3, "%s_%d"),
    FOUR_ENVERS(4, "%s_%d_envers"),
    FIVE_HIS_PREP(5, "%s_%d_his_prep"),
    SIX_HIS(6, "%s_%d_his"),
    SEVEN_HIS_CLEAN(7, "%s_%d_his_clean");

    private final int phase;
    private final String formatName;

    ExecutionPhase(int phase, String formatName) {
        this.phase = phase;
        this.formatName = formatName;
    }

    public int asNumber() {
        return phase;
    }

    public String getName() {
        return String.format(formatName, "phase", phase);
    }

    public static Stream<ExecutionPhase> stream() {
        return Arrays.stream(ExecutionPhase.values()).filter(e -> e != ExecutionPhase.NONE);
    }
}
