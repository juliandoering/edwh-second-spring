package query.collection;

import metadata.TableMetaDataExtractor;
import metadata.TablesMetaData;
import query.Query;
import query.builder.QueryBuilder;
import query.parameter.QueryParameter;

import java.sql.SQLException;

public class TableQueriesFactory implements QueriesFactory {

    private final TableMetaDataExtractor tableMetaDataExtractor;
    private final QueryBuilder[] queryBuilders;

    public TableQueriesFactory(TableMetaDataExtractor tableMetaDataExtractor, QueryBuilder... queryBuilders) {
        this.tableMetaDataExtractor = tableMetaDataExtractor;
        this.queryBuilders = queryBuilders;
    }

    @Override
    public Queries create(String schema) throws SQLException {
        var queries = new Queries(schema);
        var tablesMetaData = tableMetaDataExtractor.extractTables(schema);

        for (var tableMetaData : tablesMetaData.getData()) {
            var query = new Query(tableMetaData.getName());
            var queryParameter = QueryParameter.fromTableMetaData(tablesMetaData.getSchema(), tableMetaData);

            for (var queryBuilder : queryBuilders) {
                query = Query.merge(query, queryBuilder.build(queryParameter, tableMetaData));
            }
            queries.add(query);
        }

        return queries;
    }

}
