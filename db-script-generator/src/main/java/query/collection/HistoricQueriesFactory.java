package query.collection;

import metadata.TableMetaData;
import metadata.TableMetaDataExtractor;
import query.Query;
import query.QueryType;
import query.helper.QueryStrings;
import query.parameter.QueryParameter;
import query.parameter.QueryParameterKey;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

public class HistoricQueriesFactory implements QueriesFactory {

    private static final HashSet<String> filterColumns = new HashSet<>(Arrays.asList("uuid", "rev", "revtype"));

    private final TableMetaDataExtractor tableMetaDataExtractor;

    public HistoricQueriesFactory(TableMetaDataExtractor tableMetaDataExtractor) {
        this.tableMetaDataExtractor = tableMetaDataExtractor;
    }

    @Override
    public Queries create(String schema) throws SQLException {
        var queries = new Queries(schema);
        var tablesMetaData = tableMetaDataExtractor.extractTables(schema);
        var hisTablesMetaData = tableMetaDataExtractor.extractTables(QueryStrings.hisSchemaNameFor(schema));
        var schemaQueryParameter = QueryParameter.fromSchema(schema);

        var generalQuery = queries.createQuery("his_general");
        generalQuery.addSubquery(QueryType.HIS_GENERAL_PREPARATION,
                schemaQueryParameter.replaceKeys(QueryStrings.mdm_his_gen_preparation));
        generalQuery.addSubquery(QueryType.HIS_GENERAL_UPDATE_REVINFO,
                schemaQueryParameter.replaceKeys(QueryStrings.mdm_his_gen_updateRevInfo));
        generalQuery.addSubquery(QueryType.HIS_GENERAL_CLEAN_UP,
                schemaQueryParameter.replaceKeys(QueryStrings.mdm_his_gen_cleanUpTmp));

        var filteredTablesMetaData = tablesMetaData.getData().stream()
                .filter(tableMetaData -> hisTablesMetaData.containsTable(QueryStrings.hisTableNameFor(tableMetaData.getName())))
                .filter(tableMetaData -> tableMetaData.containsColumn(TableMetaData.UUID_COLUMN))
                .filter(tableMetaData -> tableMetaData.containsColumn(TableMetaData.IS_VERSION_COLUMN))
                .collect(Collectors.toList());
        var tableUnionSubqueries = new Query("table_union_subqueries");

        for (var tableMetaData : filteredTablesMetaData) {
            var tableQuery = queries.createQuery(tableMetaData.getName());
            var tableQueryParameter = QueryParameter
                    .fromTableMetaData(tablesMetaData.getSchema(), tableMetaData);

            tableQuery.addSubquery(
                    QueryType.HIS_UPDATE_TABLES,
                    getUpdateTablesQuery(tableMetaData,
                            hisTablesMetaData.getByName(QueryStrings.hisTableNameFor(tableMetaData.getName())),
                            tableQueryParameter)
            );
            tableUnionSubqueries.addSubquery(QueryType.HIS_TABLE_UNION_SUBQUERY,
                    tableQueryParameter.replaceKeys(QueryStrings.mdm_his_tableUnionSubquery));
        }

        generalQuery.addSubquery(QueryType.HIS_GENERAL_CREATE_REVINFO_TMP,
                getCreateRevinfoTmpQuery(tableUnionSubqueries, schemaQueryParameter));

        return queries;
    }

    private String getCreateRevinfoTmpQuery(Query tableUnionSubqueries, QueryParameter queryParameter) {
        queryParameter.put(QueryParameterKey.HIS_TABLE_UNION_SUBQUERIES,
                tableUnionSubqueries.joinSubqueries("\nunion\n"));
        return queryParameter.replaceKeys(QueryStrings.mdm_his_gen_createRevInfoTmp);
    }

    private String getUpdateTablesQuery(TableMetaData tableMetaData,
                                        TableMetaData hisTableMetaData,
                                        QueryParameter queryParameter) {
        var hisColumns = hisTableMetaData.getColumnNames().stream()
                .filter(c -> !filterColumns.contains(c))
                .collect(Collectors.joining(", "));

        var fkJoinSubqueries = new Query("fk_joins");
        var counter = 1;
        for (var entry : tableMetaData.getForeignTables().entrySet()) {
            var fkColumn = entry.getKey();
            var referencedTable = entry.getValue();
            var refName = String.format("ref%d", counter++);

            queryParameter.put(QueryParameterKey.FK_REFERENCE_TABLE, referencedTable.getName());
            queryParameter.put(QueryParameterKey.FK_COLUMN, fkColumn);
            queryParameter.put(QueryParameterKey.FK_REF_NAME, refName);

            fkJoinSubqueries.addSubquery(QueryType.HIS_FK_JOIN_SUBQUERY,
                    queryParameter.replaceKeys(QueryStrings.mdm_his_updateTable_fkJoinSubquery));
            hisColumns = hisColumns.replace(
                    QueryStrings.replaceIdWithUuid(fkColumn),
                    String.join(".", refName, referencedTable.getIdType())
            );
        }

        queryParameter.put(QueryParameterKey.HIS_COLUMN_NAMES, hisColumns);
        queryParameter.put(QueryParameterKey.HIS_FK_JOINS, fkJoinSubqueries.joinSubqueries("\n"));
        return queryParameter.replaceKeys(QueryStrings.mdm_his_updateTables);
    }

}
