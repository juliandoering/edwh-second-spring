package query.collection;

import query.ExecutionPhase;
import query.Query;
import query.QueryType;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Queries {

    private final String schema;
    private final List<Query> queries = new ArrayList<>();
    private final Map<String, Query> queryToTable = new HashMap<>();
    private final Map<Query, String> queryWithTable = new HashMap<>();

    public Queries(String schema) {
        this.schema = schema;
    }

    public static Queries merge(Queries q1, Queries q2) {
        var queries = new Queries(q1.schema);
        q1.queries.forEach(queries::add);
        q2.queries.forEach(queries::add);
        return queries;
    }

    public void add(Query query) {
        queryWithTable.put(query, query.getName());
        queryToTable.put(query.getName(), query);
        queries.add(query);
    }

    public Query createQuery(String name) {
        var query = new Query(name);
        add(query);
        return query;
    }

    public Optional<Query> getForTableName(String name) {
        return Optional.ofNullable(queryToTable.get(name));
    }

    public List<Query> getQueries() {
        return queries;
    }

    @Override
    public String toString() {
        return ExecutionPhase.stream()
                .map(this::getExecutionPhaseAsQueryString)
                .collect(Collectors.joining());
    }

    private String getExecutionPhaseAsQueryString(ExecutionPhase phase) {
        var stringBuilder = new StringBuilder();

        stringBuilder.append(String.format("/* %s %s */\n", schema, phase.getName()));

        for (var query : queries) {
            if (query.hasEntries(phase)) {
                stringBuilder.append(String.format("\n/* %s */\n", queryWithTable.get(query)));
                stringBuilder.append(query.getExecutionPhaseQueryString(phase));
                stringBuilder.append("\n\n");
            }
        }

        return stringBuilder.toString();
    }
}
