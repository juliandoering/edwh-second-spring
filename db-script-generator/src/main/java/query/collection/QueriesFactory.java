package query.collection;

import metadata.TablesMetaData;

import java.sql.SQLException;

public interface QueriesFactory {
    Queries create(String schema) throws SQLException;
}
