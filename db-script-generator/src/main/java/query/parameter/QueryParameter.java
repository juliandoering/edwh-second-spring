package query.parameter;

import metadata.TableMetaData;
import query.helper.QueryStrings;

import java.util.HashMap;
import java.util.Map;

public class QueryParameter {

    private final Map<QueryParameterKey, String> parameters = new HashMap<>();

    public static QueryParameter fromTableMetaData(String schema, TableMetaData tableMetaData) {
        var queryParameter = new QueryParameter();
        var tableName = QueryStrings.tableNameWithoutSuffix(tableMetaData.getName());
        queryParameter.put(QueryParameterKey.SCHEMA, schema);
        queryParameter.put(QueryParameterKey.SCHEMA_NEW, QueryStrings.newSchemaNameFor(schema));
        queryParameter.put(QueryParameterKey.SCHEMA_HIS, QueryStrings.hisSchemaNameFor(schema));
        queryParameter.put(QueryParameterKey.TABLE, tableName);
        queryParameter.put(QueryParameterKey.TABLE_HIS, QueryStrings.hisTableNameFor(tableName));
        queryParameter.put(QueryParameterKey.ID_TYPE, tableMetaData.getIdType());
        queryParameter.put(QueryParameterKey.TABLE_MAX_ID, String.valueOf(tableMetaData.getMaxId()));
        return queryParameter;
    }

    public static QueryParameter fromSchema(String schema) {
        var queryParameter = new QueryParameter();
        queryParameter.put(QueryParameterKey.SCHEMA, schema);
        queryParameter.put(QueryParameterKey.SCHEMA_NEW, QueryStrings.newSchemaNameFor(schema));
        queryParameter.put(QueryParameterKey.SCHEMA_HIS, QueryStrings.hisSchemaNameFor(schema));
        return queryParameter;
    }

    public void put(QueryParameterKey key, String value) {
        parameters.put(key, value);
    }

    public String replaceKeys(String queryString) {
        for (var parameterEntry : parameters.entrySet()) {
            queryString = queryString.replace(parameterEntry.getKey().getKey(), parameterEntry.getValue());
        }
        return queryString;
    }
}
