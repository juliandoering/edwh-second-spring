package query.parameter;

public enum QueryParameterKey {

    SCHEMA("{schema}"),
    SCHEMA_NEW("{schema_new}"),
    SCHEMA_HIS("{schema_his}"),
    TABLE("{table}"),
    TABLE_HIS("{table_his}"),
    ID_TYPE("{id_type}"),
    TABLE_MAX_ID("{table_max_id}"),
    FILTER_EXPRESSION("{filter_expression}"),
    FK_ID_TYPE("{fk_id_type}"),
    FK_NAME("{fk_name}"),
    FK_COLUMN("{fk_column}"),
    FK_COLUMN_NEW("{fk_column_new}"),
    FK_COLUMN_VERSION("{fk_column_version}"),
    FK_REFERENCE_TABLE("{fk_reference_table}"),
    FK_REF_NAME("{fk_ref_name}"),
    HIS_COLUMN_NAMES("{his_column_names}"),
    HIS_FK_JOINS("{his_fk_joins}"),
    HIS_TABLE_UNION_SUBQUERIES("{table_union_subqueries}");

    private final String key;

    QueryParameterKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
