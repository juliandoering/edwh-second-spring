DROP TABLE IF EXISTS etl.dwh_coffeeriskpositionjob;

CREATE TABLE etl.dwh_coffeeriskpositionjob
(
    id bigserial   NOT NULL,

    -- business fields
    pdijobid varchar,
    tagetikcode varchar,
    reportdate date,
    startedat timestamp,
    stoppedat timestamp,
    uploader varchar,
    status varchar,
    message varchar,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_dwh_coffeeriskpositionjob PRIMARY KEY (id)
);

ALTER TABLE etl.dwh_coffeeriskpositionjob OWNER TO etl;

CREATE INDEX idx_dwh_coffeeriskpositionjob_groupcompany_uuid on etl.dwh_coffeeriskpositionjob(groupcompany_uuid);
