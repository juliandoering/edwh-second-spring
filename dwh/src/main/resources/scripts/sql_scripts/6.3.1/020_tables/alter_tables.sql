ALTER TABLE valuation_differential
DROP created_by;
ALTER TABLE valuation_differential
ADD created_by varchar;

ALTER TABLE valuation_differential
DROP updated_by;
ALTER TABLE valuation_differential
ADD updated_by varchar;

ALTER TABLE valuation_differential
DROP deleted;
ALTER TABLE valuation_differential
DROP version;
ALTER TABLE valuation_differential
DROP is_version;
ALTER TABLE valuation_differential
DROP parent;