-- DWH-232 dwh reporting - coffee risk position report - flags for extracting the correct data for dwh reporting
REVOKE ALL PRIVILEGES ON schema dwh FROM dwh_reporting;

GRANT USAGE ON SCHEMA dwh to dwh_reporting;
GRANT SELECT ON ALL TABLES IN SCHEMA dwh TO dwh_reporting;
