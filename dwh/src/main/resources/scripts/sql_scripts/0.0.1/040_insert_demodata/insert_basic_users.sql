--changeset generated_nf:1497621355
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('1253040', 'rodriguez', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('1254040', 'foede', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('1255040', 'elvers', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('1256040', 'finck', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('1257040', 'lips', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('1258040', 'vatterodt', (select id from userrole where rolename = 'edwh_admin'));

INSERT INTO userrole (uuid, rolename, admin)
    VALUES ('1259040', 'edwh_technical', false);
    
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('1260040', 'comtras', (select id from userrole where rolename = 'edwh_technical'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('1261040', 'vcms', (select id from userrole where rolename = 'edwh_technical'));