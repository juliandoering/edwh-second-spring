INSERT INTO physicalcoffeestock (id, uuid, erroneous_data, created_by, updated_by)
VALUES ('-1','123123124', false, (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'), (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'));
INSERT INTO physicalcoffeestock (id, uuid, erroneous_data, created_by, updated_by)
VALUES ('-2','123123125', false, (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'), (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'));
INSERT INTO physicalcoffeestock (id, uuid, erroneous_data, created_by, updated_by)
VALUES ('-3','123123126', false, (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'), (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'));

INSERT INTO physicalcoffeestock (id, uuid, erroneous_data, created_by, updated_by, groupcompany_internal_id, holder_uuid)
VALUES ('-4','123123130', false, (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'), (SELECT uuid FROM userprofile WHERE username = 'edwh_admin'), 'INTERNAL', 'HOLDER');
