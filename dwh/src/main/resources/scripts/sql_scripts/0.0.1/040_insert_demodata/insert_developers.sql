--changeset generated_nf:1497621355 
-- Give developers the developer role
INSERT INTO userrole (uuid, id, rolename)
	VALUES ('1257040', -1, 'developer');
INSERT INTO userrole (uuid, id, rolename)
	VALUES ('1258040', -2, 'test trader');
	
-- Insert developes as users
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1259040', -1, 'nf', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1260040', -2, 'elv', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1261040', -3, 'af', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1262040', -4, 'app-bric', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1263040', -5, 'app-ceis', -2);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1264040', -6, 'sl', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1265040', -7, 'cva', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('1266040', -8, 'tst', -1);
INSERT INTO userprofile(uuid, id, username, userrole_id)
  VALUES ('1266040', -9, 'app-ngvn', -1);
