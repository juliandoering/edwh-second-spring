CREATE TABLE physicalcoffeestockprocessing
(
    id bigserial,
    uuid varchar,
    /* activeJdbc audit information */
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
	  created_by VARCHAR,
	  updated_by VARCHAR,

    /* optimistic locking */
    record_version integer default 0,
    
    /* quality check */
    erroneous_data boolean default false,

    /* primitive data */
    groupcompany_internal_id varchar,
    starting_date date,
    closing_date date,
    
    /* references (UUID) to MD */
    groupcompany_uuid varchar,
    groupcompany_version bigint,
    processingtype_uuid varchar,
    processingtype_version bigint,
    
    CONSTRAINT PK_physicalcoffeestockprocessing PRIMARY KEY (id)
)