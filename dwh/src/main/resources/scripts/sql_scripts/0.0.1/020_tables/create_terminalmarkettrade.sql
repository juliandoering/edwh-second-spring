--changeset af:1498058286
/* Table for terminal market contract transactions */
CREATE TABLE terminalmarkettrade
(
  id                        BIGSERIAL,
  uuid                      VARCHAR,
	/* activeJdbc audit information */
  created_at                TIMESTAMP DEFAULT current_timestamp,
  updated_at                TIMESTAMP DEFAULT current_timestamp,
	created_by                VARCHAR,
	updated_by                VARCHAR,
	/* optimistic locking */
  record_version            INTEGER   DEFAULT 0,
	
	/* quality check */
  erroneous_data            BOOLEAN   DEFAULT FALSE,
        
	/* payload */
  tmt_id                    VARCHAR,
  trade_date                DATE,
  trade_price               INTEGER,
  trade_number               INTEGER,
  futures_year              INTEGER,
  trade_quantity            DECIMAL,
  closed_out                BOOLEAN   DEFAULT FALSE,
	
    -- lookup references
  futuresmonth_uuid         VARCHAR,
  futuresmonth_version      INTEGER,
  futuresmarket_uuid        VARCHAR,
  futuresmarket_version     INTEGER,
  tradetype_id              BIGINT,
  tradecategory_uuid        VARCHAR,
  tradecategory_version     BIGINT,
  groupcompany_uuid         VARCHAR,
  groupcompany_version      INTEGER,
  referenced_transaction_uuid VARCHAR,
  -- trade type
  referenced_tradetype_uuid VARCHAR,
  referenced_tradetype_version INTEGER,


  CONSTRAINT PK_teminal_market_trade PRIMARY KEY (id)
);
