CREATE TABLE physicalcoffeestock
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	created_by VARCHAR,
	updated_by VARCHAR,

	/* optimistic locking */
	record_version integer default 0,

  /* primitive data */
  groupcompany_internal_id varchar,
  creationdate date,
  disappearancedate date,
  quantityvalue decimal,
  cropyear bigint,
  isunallocated boolean,

  /* references to MD */
  holder_uuid varchar,
  holder_version int,
  owner_groupcompany_uuid varchar,
  owner_gc_version int,
  owner_btp_uuid varchar,
  owner_btp_version int,
  quantityunit_uuid varchar,
  quantityunit_version int,
  coffeequality_uuid varchar,
  coffeequality_version int,
  packingtype_uuid varchar,
  packingtype_version int,
  warehouse_uuid varchar,
  warehouse_version int,
  erroneous_data boolean,

  CONSTRAINT PK_physicalcoffeestock PRIMARY KEY (id)
);