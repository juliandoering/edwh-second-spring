--changeset generated_nf:1497621355 
CREATE TABLE userprofile (
	id bigserial,
	uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    record_version integer default 0,
    username varchar,
    email varchar,
    userrole_id bigint,
    CONSTRAINT PK_userprofile PRIMARY KEY (id)
);
