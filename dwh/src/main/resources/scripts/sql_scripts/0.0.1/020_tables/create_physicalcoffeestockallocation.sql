CREATE TABLE physicalcoffeestockallocation (

  id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	created_by VARCHAR,
	updated_by VARCHAR,

	/* optimistic locking */
	record_version integer default 0,

  physicalcoffeestock_id int,
  allocationtype varchar,
  allocated_contract_uuid varchar,
  allocated_contract_version bigint,
  allocated_processing_uuid varchar,
  allocationdate date,
  volume decimal,
  erroneous_data boolean,

  CONSTRAINT PK_allocation PRIMARY KEY (id)
);