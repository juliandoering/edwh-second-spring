-- DWH-232 dwh reporting - coffee risk position report - find all physical coffee stocks based on a contract
-- usfull indexes due to the ratio <contract coffee stocks>/<all coffee stocks> is very low
create index if not exists idx_physicalcoffeestockallocation_physicalcoffeestock_id on dwh.physicalcoffeestockallocation (physicalcoffeestock_id);
create index if not exists idx_physicalcoffeestockallocation_type_uuid on dwh.physicalcoffeestockallocation (allocationtype, uuid);