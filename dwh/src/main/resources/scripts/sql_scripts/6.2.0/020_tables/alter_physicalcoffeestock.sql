-- Add column
ALTER TABLE physicalcoffeestock ADD COLUMN in_transit BOOLEAN   DEFAULT FALSE;