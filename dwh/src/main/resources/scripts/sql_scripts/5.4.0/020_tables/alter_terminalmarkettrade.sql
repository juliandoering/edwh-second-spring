-- Drop deprecated columns
ALTER TABLE terminalmarkettrade DROP COLUMN tradetype_id;

-- Rename mdm references
ALTER TABLE terminalmarkettrade RENAME COLUMN referenced_tradetype_uuid TO tradetype_uuid;
ALTER TABLE terminalmarkettrade RENAME COLUMN referenced_tradetype_version TO tradetype_version;