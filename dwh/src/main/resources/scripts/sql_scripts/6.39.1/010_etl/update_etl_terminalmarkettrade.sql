DROP TABLE IF EXISTS etl.dwh_terminalmarkettrade;

CREATE TABLE etl.dwh_terminalmarkettrade
(
    id                 bigserial   NOT NULL,
    job_id             varchar,

    -- technical fields
    import_date        timestamp,
    source             varchar,
    source_pk          varchar,

    -- business fields
    tmt_id varchar,
    trade_number varchar,
    trade_date date,
    trade_price decimal,
    trade_quantity numeric,
    futures_year numeric,
    closedout boolean,
    futures_month_uuid varchar,
    futures_month_version bigint,
    futures_market_uuid varchar,
    futures_market_version bigint,
    trade_type_uuid varchar,
    trade_type_version bigint,
    trade_category_uuid varchar,
    trade_category_version bigint,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_dwh_terminalmarkettrade PRIMARY KEY (id)
);

ALTER TABLE etl.dwh_terminalmarkettrade OWNER TO etl;
