ALTER TABLE etl.dwh_terminalmarketoptiontrade drop column IF EXISTS trade_category_uuid;
ALTER TABLE etl.dwh_terminalmarketoptiontrade drop column IF EXISTS trade_category_version;
ALTER TABLE etl.dwh_terminalmarketoptiontrade add column if not exists option_trade_category varchar;

