ALTER TABLE terminalmarketoptiontrade drop column if exists trade_category_uuid;
ALTER TABLE terminalmarketoptiontrade drop column IF EXISTS trade_category_version;
ALTER TABLE terminalmarketoptiontrade add column if not exists option_trade_category varchar;
