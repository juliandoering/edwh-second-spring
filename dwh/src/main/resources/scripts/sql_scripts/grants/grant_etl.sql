--changeset generated_nf:1497621355 
REVOKE ALL PRIVILEGES ON schema dwh FROM etl;

GRANT USAGE ON SCHEMA dwh to etl;
GRANT SELECT ON ALL TABLES IN SCHEMA dwh TO etl;
