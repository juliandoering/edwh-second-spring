DROP TABLE IF EXISTS etl.dwh_terminalmarketoptionvaluation;

CREATE TABLE etl.dwh_terminalmarketoptionvaluation
(
    id                 bigserial   NOT NULL,
    job_id             varchar,

    -- technical fields
    import_date        timestamp,
    source             varchar,
    source_pk          varchar,

    -- business fields
    tmot_id varchar,
    valuation_date date,
    trade_price decimal,
    strike_price decimal,
    trade_quantity numeric,
    valuation_delta numeric,
    valuation_value numeric,
    delivery_period date,
    weighted_average numeric,
    futures_market_uuid varchar,
    futures_market_version bigint,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_dwh_terminalmarketoptionvaluation PRIMARY KEY (id)
);

ALTER TABLE etl.dwh_terminalmarketoptionvaluation OWNER TO etl;



