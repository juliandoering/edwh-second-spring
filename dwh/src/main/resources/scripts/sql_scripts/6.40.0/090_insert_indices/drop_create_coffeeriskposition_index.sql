DROP INDEX dwh.idx_coffeeriskposition_bk;

CREATE INDEX idx_coffeeriskposition_bk ON coffeeriskposition(riskpositiondate, groupcompanyname, riskpositiontype, forwardpositionordinal, coffeeriskpositionrowmapping_id, created_at desc);
