ALTER TABLE terminalmarkettradecloseout DROP COLUMN IF EXISTS erroneous_data;
ALTER TABLE coffeeriskpositionrowmapping DROP COLUMN IF EXISTS erroneous_data;
ALTER TABLE contractcoffeequalitypercentage DROP COLUMN IF EXISTS erroneous_data;
ALTER TABLE physicalcoffeestockallocation DROP COLUMN IF EXISTS erroneous_data;
ALTER TABLE coffeestocktheoreticalpercentage DROP COLUMN IF EXISTS erroneous_data;