# SQL scripts here

Scripts to create the databases and tables we use.

Tablenames are singular per convention.

This directory includes further directories to organize the sql scripts for liquibase.
On this level the next directories are named after the mdm version they are created for.


We want to be able to run just one script instead of each script in each folder inidividually.
Therefore there is a shell script to create two files which run all scripts.
One uses \i syntax and the other plain sql.
The files each contain all information of the scripts in the folders that start with digits in alphabetical order ('ls' order).
In each folder is supposed to be a readme.txt which is used to create comments
to make the file sections and folder content better understandable.
The generated files are in the 'edwh-database-init-setup' folder.



We agreed on the one changing something in the sql to check the generated files in.
This way only one developer has to run the script and others can rely on having the most up to date version.

