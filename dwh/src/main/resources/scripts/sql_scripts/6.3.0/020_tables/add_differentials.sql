CREATE TABLE valuation_differential
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    valid_from timestamp,
    differential_value bigint,
    
    -- references
    coffee_quality_uuid varchar,
    coffee_quality_version integer,
    group_company_uuid varchar,
    group_company_version integer,
    currencyunit_uuid varchar,
    currencyunit_version integer,
    quantity_unit_uuid varchar,
    quantity_unit_version integer,
	incoterm_uuid varchar,
    incoterm_version integer,
    packing_uuid varchar,
    packing_version integer,
    location_uuid varchar,
    location_version integer,
    futuresmarket_uuid varchar,
    futuresmarket_version integer,
    futuresmonth_uuid  varchar,
    futuresmonth_version  integer,
    futures_year integer,
    erroneous_data boolean,

    CONSTRAINT PK_valuation_differential PRIMARY KEY (id)
);
CREATE INDEX idx_valuation_differential_uuid on valuation_differential(uuid);

