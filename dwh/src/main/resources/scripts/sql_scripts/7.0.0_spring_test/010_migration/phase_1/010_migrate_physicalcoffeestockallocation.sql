create table dwh_spring.physicalcoffeestockallocation as table dwh.physicalcoffeestockallocation;
alter table dwh_spring.physicalcoffeestockallocation add primary key (id);
drop sequence if exists dwh_spring.physicalcoffeestockallocation_id_seq cascade;
create sequence dwh_spring.physicalcoffeestockallocation_id_seq owned by dwh_spring.physicalcoffeestockallocation.id;
select setval('dwh_spring.physicalcoffeestockallocation_id_seq', 8 + 1, false) FROM dwh_spring.physicalcoffeestockallocation;
alter table dwh_spring.physicalcoffeestockallocation
   alter column id set default nextval('dwh_spring.physicalcoffeestockallocation_id_seq');
alter table dwh_spring.physicalcoffeestockallocation drop column if exists allocated_contract_version;
alter table dwh_spring.physicalcoffeestockallocation drop column if exists allocated_processing_version;
alter table dwh_spring.physicalcoffeestockallocation rename column allocated_contract_uuid to allocated_contract_id;
alter table dwh_spring.physicalcoffeestockallocation rename column allocated_processing_uuid to allocated_processing_id;
alter table dwh_spring.physicalcoffeestockallocation drop column uuid;
alter table dwh_spring.physicalcoffeestockallocation drop column record_version;
alter table dwh_spring.physicalcoffeestockallocation add column version bigint not null default 0;
