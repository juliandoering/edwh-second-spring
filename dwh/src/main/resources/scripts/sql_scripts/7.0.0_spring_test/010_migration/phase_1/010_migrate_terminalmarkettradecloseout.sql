create table dwh_spring.terminalmarkettradecloseout as table dwh.terminalmarkettradecloseout;
alter table dwh_spring.terminalmarkettradecloseout add primary key (id);
drop sequence if exists dwh_spring.terminalmarkettradecloseout_id_seq cascade;
create sequence dwh_spring.terminalmarkettradecloseout_id_seq owned by dwh_spring.terminalmarkettradecloseout.id;
select setval('dwh_spring.terminalmarkettradecloseout_id_seq', 0 + 1, false) FROM dwh_spring.terminalmarkettradecloseout;
alter table dwh_spring.terminalmarkettradecloseout
   alter column id set default nextval('dwh_spring.terminalmarkettradecloseout_id_seq');
alter table dwh_spring.terminalmarkettradecloseout drop column if exists closeout_terminalmarkettrade_version;
alter table dwh_spring.terminalmarkettradecloseout rename column closeout_terminalmarkettrade_uuid to closeout_terminalmarkettrade_id;
alter table dwh_spring.terminalmarkettradecloseout drop column uuid;
alter table dwh_spring.terminalmarkettradecloseout drop column record_version;
alter table dwh_spring.terminalmarkettradecloseout add column version bigint not null default 0;
