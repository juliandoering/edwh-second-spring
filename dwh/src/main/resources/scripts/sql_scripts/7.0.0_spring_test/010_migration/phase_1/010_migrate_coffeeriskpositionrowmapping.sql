create table dwh_spring.coffeeriskpositionrowmapping as table dwh.coffeeriskpositionrowmapping;
alter table dwh_spring.coffeeriskpositionrowmapping add primary key (id);
drop sequence if exists dwh_spring.coffeeriskpositionrowmapping_id_seq cascade;
create sequence dwh_spring.coffeeriskpositionrowmapping_id_seq owned by dwh_spring.coffeeriskpositionrowmapping.id;
select setval('dwh_spring.coffeeriskpositionrowmapping_id_seq', 65 + 1, false) FROM dwh_spring.coffeeriskpositionrowmapping;
alter table dwh_spring.coffeeriskpositionrowmapping
   alter column id set default nextval('dwh_spring.coffeeriskpositionrowmapping_id_seq');
alter table dwh_spring.coffeeriskpositionrowmapping drop column uuid;
alter table dwh_spring.coffeeriskpositionrowmapping drop column record_version;
alter table dwh_spring.coffeeriskpositionrowmapping add column version bigint not null default 0;
