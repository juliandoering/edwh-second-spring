create table dwh_spring.coffeeriskposition as table dwh.coffeeriskposition;
alter table dwh_spring.coffeeriskposition add primary key (id);
drop sequence if exists dwh_spring.coffeeriskposition_id_seq cascade;
create sequence dwh_spring.coffeeriskposition_id_seq owned by dwh_spring.coffeeriskposition.id;
select setval('dwh_spring.coffeeriskposition_id_seq', 0 + 1, false) FROM dwh_spring.coffeeriskposition;
alter table dwh_spring.coffeeriskposition
   alter column id set default nextval('dwh_spring.coffeeriskposition_id_seq');
alter table dwh_spring.coffeeriskposition drop column if exists groupcompany_version;
alter table dwh_spring.coffeeriskposition rename column groupcompany_uuid to groupcompany_id;
alter table dwh_spring.coffeeriskposition drop column uuid;
alter table dwh_spring.coffeeriskposition drop column record_version;
alter table dwh_spring.coffeeriskposition add column version bigint not null default 0;
