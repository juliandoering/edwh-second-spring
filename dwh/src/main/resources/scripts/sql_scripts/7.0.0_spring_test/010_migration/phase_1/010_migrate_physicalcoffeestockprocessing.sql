create table dwh_spring.physicalcoffeestockprocessing as table dwh.physicalcoffeestockprocessing;
alter table dwh_spring.physicalcoffeestockprocessing add primary key (id);
drop sequence if exists dwh_spring.physicalcoffeestockprocessing_id_seq cascade;
create sequence dwh_spring.physicalcoffeestockprocessing_id_seq owned by dwh_spring.physicalcoffeestockprocessing.id;
select setval('dwh_spring.physicalcoffeestockprocessing_id_seq', 6 + 1, false) FROM dwh_spring.physicalcoffeestockprocessing;
alter table dwh_spring.physicalcoffeestockprocessing
   alter column id set default nextval('dwh_spring.physicalcoffeestockprocessing_id_seq');
alter table dwh_spring.physicalcoffeestockprocessing drop column if exists groupcompany_version;
alter table dwh_spring.physicalcoffeestockprocessing drop column if exists processingtype_version;
alter table dwh_spring.physicalcoffeestockprocessing rename column groupcompany_uuid to groupcompany_id;
alter table dwh_spring.physicalcoffeestockprocessing rename column processingtype_uuid to processingtype_id;
alter table dwh_spring.physicalcoffeestockprocessing drop column uuid;
alter table dwh_spring.physicalcoffeestockprocessing drop column record_version;
alter table dwh_spring.physicalcoffeestockprocessing add column version bigint not null default 0;
