create table dwh_spring.terminalmarketoptionvaluation as table dwh.terminalmarketoptionvaluation;
alter table dwh_spring.terminalmarketoptionvaluation add primary key (id);
drop sequence if exists dwh_spring.terminalmarketoptionvaluation_id_seq cascade;
create sequence dwh_spring.terminalmarketoptionvaluation_id_seq owned by dwh_spring.terminalmarketoptionvaluation.id;
select setval('dwh_spring.terminalmarketoptionvaluation_id_seq', 0 + 1, false) FROM dwh_spring.terminalmarketoptionvaluation;
alter table dwh_spring.terminalmarketoptionvaluation
   alter column id set default nextval('dwh_spring.terminalmarketoptionvaluation_id_seq');
alter table dwh_spring.terminalmarketoptionvaluation drop column if exists futures_market_version;
alter table dwh_spring.terminalmarketoptionvaluation drop column if exists groupcompany_version;
alter table dwh_spring.terminalmarketoptionvaluation rename column futures_market_uuid to futures_market_id;
alter table dwh_spring.terminalmarketoptionvaluation rename column groupcompany_uuid to groupcompany_id;
alter table dwh_spring.terminalmarketoptionvaluation drop column uuid;
alter table dwh_spring.terminalmarketoptionvaluation drop column record_version;
alter table dwh_spring.terminalmarketoptionvaluation add column version bigint not null default 0;
