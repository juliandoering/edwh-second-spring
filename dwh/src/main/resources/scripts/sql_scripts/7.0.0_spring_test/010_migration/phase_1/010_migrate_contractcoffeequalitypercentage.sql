create table dwh_spring.contractcoffeequalitypercentage as table dwh.contractcoffeequalitypercentage;
alter table dwh_spring.contractcoffeequalitypercentage add primary key (id);
drop sequence if exists dwh_spring.contractcoffeequalitypercentage_id_seq cascade;
create sequence dwh_spring.contractcoffeequalitypercentage_id_seq owned by dwh_spring.contractcoffeequalitypercentage.id;
select setval('dwh_spring.contractcoffeequalitypercentage_id_seq', 0 + 1, false) FROM dwh_spring.contractcoffeequalitypercentage;
alter table dwh_spring.contractcoffeequalitypercentage
   alter column id set default nextval('dwh_spring.contractcoffeequalitypercentage_id_seq');
alter table dwh_spring.contractcoffeequalitypercentage drop column if exists coffeequality_version;
alter table dwh_spring.contractcoffeequalitypercentage rename column coffeequality_uuid to coffeequality_id;
alter table dwh_spring.contractcoffeequalitypercentage drop column uuid;
alter table dwh_spring.contractcoffeequalitypercentage drop column record_version;
alter table dwh_spring.contractcoffeequalitypercentage add column version bigint not null default 0;
