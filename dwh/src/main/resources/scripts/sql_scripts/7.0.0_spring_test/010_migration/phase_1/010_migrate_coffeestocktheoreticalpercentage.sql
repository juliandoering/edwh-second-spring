create table dwh_spring.coffeestocktheoreticalpercentage as table dwh.coffeestocktheoreticalpercentage;
alter table dwh_spring.coffeestocktheoreticalpercentage add primary key (id);
drop sequence if exists dwh_spring.coffeestocktheoreticalpercentage_id_seq cascade;
create sequence dwh_spring.coffeestocktheoreticalpercentage_id_seq owned by dwh_spring.coffeestocktheoreticalpercentage.id;
select setval('dwh_spring.coffeestocktheoreticalpercentage_id_seq', 0 + 1, false) FROM dwh_spring.coffeestocktheoreticalpercentage;
alter table dwh_spring.coffeestocktheoreticalpercentage
   alter column id set default nextval('dwh_spring.coffeestocktheoreticalpercentage_id_seq');
alter table dwh_spring.coffeestocktheoreticalpercentage drop column if exists coffeequality_version;
alter table dwh_spring.coffeestocktheoreticalpercentage rename column coffeequality_uuid to coffeequality_id;
alter table dwh_spring.coffeestocktheoreticalpercentage drop column uuid;
alter table dwh_spring.coffeestocktheoreticalpercentage drop column record_version;
alter table dwh_spring.coffeestocktheoreticalpercentage add column version bigint not null default 0;
