create table dwh_spring.coffeecontractexchangerate as table dwh.coffeecontractexchangerate;
alter table dwh_spring.coffeecontractexchangerate add primary key (id);
drop sequence if exists dwh_spring.coffeecontractexchangerate_id_seq cascade;
create sequence dwh_spring.coffeecontractexchangerate_id_seq owned by dwh_spring.coffeecontractexchangerate.id;
select setval('dwh_spring.coffeecontractexchangerate_id_seq', 0 + 1, false) FROM dwh_spring.coffeecontractexchangerate;
alter table dwh_spring.coffeecontractexchangerate
   alter column id set default nextval('dwh_spring.coffeecontractexchangerate_id_seq');
alter table dwh_spring.coffeecontractexchangerate drop column if exists contract_version;
alter table dwh_spring.coffeecontractexchangerate rename column contract_uuid to contract_id;
alter table dwh_spring.coffeecontractexchangerate drop column uuid;
alter table dwh_spring.coffeecontractexchangerate drop column record_version;
alter table dwh_spring.coffeecontractexchangerate add column version bigint not null default 0;
