create table dwh_spring.terminalmarketoptiontrade as table dwh.terminalmarketoptiontrade;
alter table dwh_spring.terminalmarketoptiontrade add primary key (id);
drop sequence if exists dwh_spring.terminalmarketoptiontrade_id_seq cascade;
create sequence dwh_spring.terminalmarketoptiontrade_id_seq owned by dwh_spring.terminalmarketoptiontrade.id;
select setval('dwh_spring.terminalmarketoptiontrade_id_seq', 0 + 1, false) FROM dwh_spring.terminalmarketoptiontrade;
alter table dwh_spring.terminalmarketoptiontrade
   alter column id set default nextval('dwh_spring.terminalmarketoptiontrade_id_seq');
alter table dwh_spring.terminalmarketoptiontrade drop column if exists futures_market_version;
alter table dwh_spring.terminalmarketoptiontrade drop column if exists trade_type_version;
alter table dwh_spring.terminalmarketoptiontrade drop column if exists futures_month_version;
alter table dwh_spring.terminalmarketoptiontrade drop column if exists groupcompany_version;
alter table dwh_spring.terminalmarketoptiontrade rename column futures_market_uuid to futures_market_id;
alter table dwh_spring.terminalmarketoptiontrade rename column trade_type_uuid to trade_type_id;
alter table dwh_spring.terminalmarketoptiontrade rename column futures_month_uuid to futures_month_id;
alter table dwh_spring.terminalmarketoptiontrade rename column groupcompany_uuid to groupcompany_id;
alter table dwh_spring.terminalmarketoptiontrade drop column uuid;
alter table dwh_spring.terminalmarketoptiontrade drop column record_version;
alter table dwh_spring.terminalmarketoptiontrade add column version bigint not null default 0;
