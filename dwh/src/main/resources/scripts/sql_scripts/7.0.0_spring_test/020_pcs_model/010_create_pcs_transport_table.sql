CREATE TABLE dwh_spring.pcs_transport
(
   id                        uuid        PRIMARY KEY DEFAULT uuid_generate_v4(),
   created_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updated_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   created_by                varchar     NOT NULL DEFAULT 'n/a',
   updated_by                varchar     NOT NULL DEFAULT 'n/a',
   from_warehouse_id         varchar     NOT NULL,
   to_warehouse_id           varchar     NOT NULL,
   departure_date            date,
   version                   bigint      DEFAULT 0 NOT NULL
);

ALTER TABLE dwh_spring.pcs_transport OWNER TO dwh;

ALTER TABLE dwh_spring.pcs_transport
   ADD CONSTRAINT fk_from_warehouse
   FOREIGN KEY (from_warehouse_id)
   REFERENCES mdm_spring.warehouse(id);

ALTER TABLE dwh_spring.pcs_transport
   ADD CONSTRAINT fk_to_warehouse
   FOREIGN KEY (to_warehouse_id)
   REFERENCES mdm_spring.warehouse(id);

GRANT REFERENCES, SELECT, TRIGGER, UPDATE, INSERT, TRUNCATE, DELETE ON dwh_spring.pcs_transport TO dwh;