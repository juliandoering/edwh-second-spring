CREATE TABLE dwh_spring.pcs_transition_event
(
   id                        uuid        PRIMARY KEY DEFAULT uuid_generate_v4(),
   created_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updated_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   created_by                varchar     NOT NULL DEFAULT 'n/a',
   updated_by                varchar     NOT NULL DEFAULT 'n/a',
   type                      varchar     NOT NULL,
   submission_time           timestamp   NOT NULL,
   comment                   varchar,
   version                   bigint      DEFAULT 0 NOT NULL
);

ALTER TABLE dwh_spring.pcs_transition_event OWNER TO dwh;

GRANT REFERENCES, SELECT, TRIGGER, UPDATE, INSERT, TRUNCATE, DELETE ON dwh_spring.pcs_transition_event TO dwh;