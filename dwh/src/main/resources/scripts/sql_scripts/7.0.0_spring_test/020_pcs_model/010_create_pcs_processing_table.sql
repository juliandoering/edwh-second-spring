DROP TABLE IF EXISTS dwh_spring.physicalcoffeestockprocessing CASCADE;

CREATE TABLE dwh_spring.pcs_process
(
   id                        uuid        PRIMARY KEY DEFAULT uuid_generate_v4(),
   created_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updated_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   created_by                varchar     NOT NULL DEFAULT 'n/a',
   updated_by                varchar     NOT NULL DEFAULT 'n/a',
   groupcompany_internal_id  varchar,
   starting_date             date,
   closing_date              date,
   groupcompany_id           varchar,
   processingtype_id         varchar     NOT NULL,
   version                   bigint      DEFAULT 0 NOT NULL
);

ALTER TABLE dwh_spring.pcs_process OWNER TO dwh;

ALTER TABLE dwh_spring.pcs_process
   ADD CONSTRAINT fk_groupcompany
   FOREIGN KEY (groupcompany_id)
   REFERENCES mdm_spring.groupcompany(id);

ALTER TABLE dwh_spring.pcs_process
   ADD CONSTRAINT fk_processingtype
   FOREIGN KEY (processingtype_id)
   REFERENCES mdm_spring.processingtype(id);

GRANT REFERENCES, SELECT, TRIGGER, UPDATE, INSERT, TRUNCATE, DELETE ON dwh_spring.pcs_process TO dwh;
