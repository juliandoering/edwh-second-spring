CREATE TABLE dwh_spring.pcs_input_output_log
(
   id                       bigserial   PRIMARY KEY,
   physical_coffee_stock_id   uuid      NOT NULL,
   transition_event_id      uuid        NOT NULL,
   type                     varchar     NOT NULL,
   executed_at              timestamp   NOT NULL,
   warehouse_id             varchar,
   transport_id             uuid,
   process_id               uuid,
   volume                   numeric     NOT NULL,
   version                  bigint      DEFAULT 0 NOT NULL
);

ALTER TABLE dwh_spring.pcs_input_output_log OWNER TO dwh;

ALTER TABLE dwh_spring.pcs_input_output_log
   ADD CONSTRAINT fk_physical_coffee_stock
   FOREIGN KEY (physical_coffee_stock_id)
   REFERENCES dwh_spring.physical_coffee_stock(id);

ALTER TABLE dwh_spring.pcs_input_output_log
   ADD CONSTRAINT fk_transition_event
   FOREIGN KEY (transition_event_id)
   REFERENCES dwh_spring.pcs_transition_event(id);

ALTER TABLE dwh_spring.pcs_input_output_log
   ADD CONSTRAINT fk_warehouse
   FOREIGN KEY (warehouse_id)
   REFERENCES mdm_spring.warehouse(id);

ALTER TABLE dwh_spring.pcs_input_output_log
   ADD CONSTRAINT fk_transport
   FOREIGN KEY (transport_id)
   REFERENCES dwh_spring.pcs_transport(id);

ALTER TABLE dwh_spring.pcs_input_output_log
   ADD CONSTRAINT fk_process
   FOREIGN KEY (process_id)
   REFERENCES dwh_spring.pcs_process(id);

GRANT REFERENCES, SELECT, TRIGGER, UPDATE, INSERT, TRUNCATE, DELETE ON dwh_spring.pcs_input_output_log TO dwh;
