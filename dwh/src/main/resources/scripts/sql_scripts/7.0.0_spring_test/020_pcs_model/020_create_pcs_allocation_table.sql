DROP TABLE IF EXISTS dwh_spring.physicalcoffeestockallocation CASCADE;

CREATE TABLE dwh_spring.pcs_allocation
(
   id                       uuid        PRIMARY KEY DEFAULT uuid_generate_v4(),
   created_at               timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updated_at               timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   created_by               varchar     NOT NULL DEFAULT 'n/a',
   updated_by               varchar     NOT NULL DEFAULT 'n/a',
   physical_coffee_stock_id   uuid        NOT NULL,
   type                     varchar     NOT NULL,
   allocated_contract_id    varchar,
   allocated_process_id     uuid,
   allocation_date          date,
   volume                   numeric     NOT NULL,
   version                  bigint      DEFAULT 0 NOT NULL
);

ALTER TABLE dwh_spring.pcs_allocation OWNER TO dwh;

ALTER TABLE dwh_spring.pcs_allocation
   ADD CONSTRAINT fk_physical_coffee_stock
   FOREIGN KEY (physical_coffee_stock_id)
   REFERENCES dwh_spring.physical_coffee_stock(id);

ALTER TABLE dwh_spring.pcs_allocation
   ADD CONSTRAINT fk_allocated_contract
   FOREIGN KEY (allocated_contract_id)
   REFERENCES mdm_spring.contract(id);

ALTER TABLE dwh_spring.pcs_allocation
   ADD CONSTRAINT fk_allocated_process
   FOREIGN KEY (allocated_process_id)
   REFERENCES dwh_spring.pcs_process(id);

GRANT REFERENCES, SELECT, TRIGGER, UPDATE, INSERT, TRUNCATE, DELETE ON dwh_spring.pcs_allocation TO dwh;
