DROP TABLE IF EXISTS dwh_spring.physicalcoffeestock CASCADE;

CREATE TABLE dwh_spring.physical_coffee_stock
(
   id                        uuid      PRIMARY KEY DEFAULT uuid_generate_v4(),
   created_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   updated_at                timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
   created_by                varchar   NOT NULL DEFAULT 'n/a',
   updated_by                varchar   NOT NULL DEFAULT 'n/a',
   groupcompany_internal_id  varchar,
   appearance_date           timestamp NOT NULL,
   origin_process_id         uuid,
   origin_contract_id        varchar,
   is_consignment            boolean   NOT NULL DEFAULT false,
   disappearance_date        timestamp,
   crop_year                 numeric   NOT NULL,
   holder_groupcompany_id    varchar   NOT NULL,
   owner_groupcompany_id     varchar,
   owner_businessthirdparty_id varchar,
   quantityunit_id           varchar   NOT NULL,
   coffeequality_id          varchar   NOT NULL,
   packing_id                varchar,
   version                   bigint    DEFAULT 0 NOT NULL
);

ALTER TABLE dwh_spring.physical_coffee_stock OWNER TO dwh;

ALTER TABLE dwh_spring.physical_coffee_stock
   ADD CONSTRAINT fk_holder_groupcompany
   FOREIGN KEY (holder_groupcompany_id)
   REFERENCES mdm_spring.groupcompany(id);

ALTER TABLE dwh_spring.physical_coffee_stock
   ADD CONSTRAINT fk_owner_groupcompany
   FOREIGN KEY (owner_groupcompany_id)
   REFERENCES mdm_spring.groupcompany(id);

ALTER TABLE dwh_spring.physical_coffee_stock
   ADD CONSTRAINT fk_owner_businessthirdparty
   FOREIGN KEY (owner_businessthirdparty_id)
   REFERENCES mdm_spring.businessthirdparty(id);

ALTER TABLE dwh_spring.physical_coffee_stock
   ADD CONSTRAINT fk_quantityunit
   FOREIGN KEY (quantityunit_id)
   REFERENCES mdm_spring.quantityunit(id);

ALTER TABLE dwh_spring.physical_coffee_stock
   ADD CONSTRAINT fk_coffeequality
   FOREIGN KEY (coffeequality_id)
   REFERENCES mdm_spring.coffeequality(id);

ALTER TABLE dwh_spring.physical_coffee_stock
   ADD CONSTRAINT fk_packing
   FOREIGN KEY (packing_id)
   REFERENCES mdm_spring.packing(id);

GRANT REFERENCES, SELECT, TRIGGER, UPDATE, INSERT, TRUNCATE, DELETE ON dwh_spring.physical_coffee_stock TO dwh;