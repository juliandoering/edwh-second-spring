CREATE TABLE terminalmarkettradecloseout (

  id bigserial,
  uuid varchar,
  /* activeJdbc audit information */
  created_at timestamp default current_timestamp,
  updated_at timestamp default current_timestamp,
  created_by VARCHAR,
  updated_by VARCHAR,

  /* optimistic locking */
  record_version integer default 0,

  erroneous_data boolean,
  
  terminalmarkettrade_id bigint,
  closeout_terminalmarkettrade_uuid varchar,

  CONSTRAINT PK_terminalmarkettradecloseout PRIMARY KEY (id)
);