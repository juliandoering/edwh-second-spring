CREATE TABLE coffeeriskposition
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    riskpositiontype varchar,
    coffeetype varchar,
    riskpositiondate date,
    forwardpositiongroup varchar,
    forwardposition varchar,
    forwardpositionordinal integer,
    riskpositionvalue decimal,
    groupcompanyname varchar,
    groupcompany_uuid varchar,
    groupcompany_version bigint,
    weightunit decimal,
    coffeeriskpositionrowmapping_id bigint,

    CONSTRAINT PK_coffeeriskposition PRIMARY KEY (id)
);

CREATE INDEX idx_coffeeriskposition_uuid on coffeeriskposition(uuid);

ALTER TABLE coffeeriskposition ADD CONSTRAINT fk_coffeeriskposition_rowidentifier FOREIGN KEY (coffeeriskpositionrowmapping_id) REFERENCES coffeeriskpositionrowmapping;
CREATE INDEX idx_coffeeriskposition_rowidentifier_id on coffeeriskposition(coffeeriskpositionrowmapping_id);