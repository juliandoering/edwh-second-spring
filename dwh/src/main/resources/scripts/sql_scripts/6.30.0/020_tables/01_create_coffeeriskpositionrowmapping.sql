CREATE TABLE coffeeriskpositionrowmapping
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean default false,

    -- business fields
    rowidentifier varchar,
    description varchar,
    fixationstate varchar,
    deliverystate varchar,

    CONSTRAINT PK_coffeeriskpositionrowmapping PRIMARY KEY (id)
);

CREATE INDEX idx_coffeeriskpositionrowmapping_uuid on coffeeriskpositionrowmapping(uuid);