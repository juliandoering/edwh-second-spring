DROP TABLE IF EXISTS etl.dwh_coffeeriskposition;

CREATE TABLE etl.dwh_coffeeriskposition
(
    id                 bigserial   NOT NULL,
    job_id             varchar,

    -- technical fields
    import_date        timestamp,
    source             varchar,
    source_pk          varchar,

    -- business fields
    riskpositiontype varchar,
    groupcompanyname varchar,
    coffeetype varchar,
    riskpositiondate date,
    forwardpositiongroup varchar,
    forwardposition varchar,
    forwardpositionordinal integer,
    riskpositionvalue decimal,
    weightunit decimal,
    groupcompany_uuid varchar,
    groupcompany_version bigint,
    rowidentifier_id bigint,

    CONSTRAINT PK_dwh_coffeeriskposition PRIMARY KEY (id)
);

ALTER TABLE etl.dwh_coffeeriskposition OWNER TO etl;

