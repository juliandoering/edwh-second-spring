DROP TABLE IF EXISTS etl.dwh_terminalmarkettrade;

CREATE TABLE etl.dwh_terminalmarkettrade
(
    id                 bigserial   NOT NULL,
    job_id             varchar,

    -- technical fields
    update_operation   char(1),
    import_date        timestamp,
    source             varchar,
    source_pk          varchar,
    mdm_id             bigint,
    mdm_version        bigint,

    -- business fields
    tmt_id varchar,
    trade_number varchar,
    trade_date date,
    trade_price decimal,
    trade_quantity numeric,
    futures_year numeric,
    futures_market_id bigint,
    trade_type_id bigint,
    trade_category_id bigint,
    futures_month_id bigint,
    groupcompany_id bigint,

    CONSTRAINT PK_dwh_terminalmarkettrade PRIMARY KEY (id)
);

ALTER TABLE etl.dwh_terminalmarkettrade OWNER TO etl;

