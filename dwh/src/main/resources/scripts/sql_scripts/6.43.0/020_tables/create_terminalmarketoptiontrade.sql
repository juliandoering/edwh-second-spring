CREATE TABLE terminalmarketoptiontrade
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    tmt_id varchar,
    trade_number varchar,
    trade_date date,
    expiration_date date,
    trade_price decimal,
    strike_price decimal,
    trade_quantity numeric,
    futures_year numeric,
    excercized boolean,
    futures_market_uuid varchar,
    futures_market_version bigint,
    trade_type_uuid varchar,
    trade_type_version bigint,
    trade_category_uuid varchar,
    trade_category_version bigint,
    futures_month_uuid varchar,
    futures_month_version bigint,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_terminalmarketoptiontrade PRIMARY KEY (id)
);
