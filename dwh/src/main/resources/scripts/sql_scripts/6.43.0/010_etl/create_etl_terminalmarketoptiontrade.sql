DROP TABLE IF EXISTS etl.dwh_terminalmarketoptiontrade;

CREATE TABLE etl.dwh_terminalmarketoptiontrade
(
    id                 bigserial   NOT NULL,
    job_id             varchar,

    -- technical fields
    import_date        timestamp,
    source             varchar,
    source_pk          varchar,

    -- business fields
    tmt_id varchar,
    trade_number varchar,
    trade_date date,
    expiration_date date,
    trade_price decimal,
    strike_price decimal,
    trade_quantity numeric,
    futures_year numeric,
    excercized boolean,
    futures_market_uuid varchar,
    futures_market_version bigint,
    trade_type_uuid varchar,
    trade_type_version bigint,
    trade_category_uuid varchar,
    trade_category_version bigint,
    futures_month_uuid varchar,
    futures_month_version bigint,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_dwh_terminalmarketoptiontrade PRIMARY KEY (id)
);

ALTER TABLE etl.dwh_terminalmarketoptiontrade OWNER TO etl;

