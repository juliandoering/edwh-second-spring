CREATE TABLE coffeestocktheoreticalcompositionpart
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    gradedcompositionfactor decimal,
    physicalcoffeestock_id bigint,
    coffeequality_uuid varchar,
    coffeequality_version bigint,

    CONSTRAINT PK_coffeestocktheoreticalcompositionpart PRIMARY KEY (id)
);

CREATE INDEX idx_coffeestocktheoreticalcompositionpart_uuid on coffeestocktheoreticalcompositionpart(uuid);
