CREATE TABLE contractcoffeequalitytheoreticalcomposition
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    contract_uuid varchar,
    contract_version bigint,

    CONSTRAINT PK_contractcoffeequalitytheoreticalcomposition PRIMARY KEY (id)
);

CREATE INDEX idx_contractcoffeequalitytheoreticalcomposition_uuid on contractcoffeequalitytheoreticalcomposition(uuid);

CREATE TABLE contractcoffeequalitycompositionpart
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    gradedcompositionfactor decimal,
    coffeequality_uuid varchar,
    coffeequality_version bigint,
    contractcoffeequalitytheoreticalcomposition_id bigint,

    CONSTRAINT PK_contractcoffeequalitycompositionpart PRIMARY KEY (id)
);

CREATE INDEX idx_contractcoffeequalitycompositionpart_uuid on contractcoffeequalitycompositionpart(uuid);