DROP TABLE IF EXISTS coffeestocktheoreticalcompositionpart;

CREATE TABLE coffeestocktheoreticalpercentage
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    gradedconversionfactor decimal,
    physicalcoffeestock_id bigint,
    coffeequality_uuid varchar,
    coffeequality_version bigint,

    CONSTRAINT PK_coffeestocktheoreticalpercentage PRIMARY KEY (id)
);

CREATE INDEX idx_coffeestocktheoreticalpercentage_uuid on coffeestocktheoreticalpercentage(uuid);
