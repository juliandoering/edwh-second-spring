DROP TABLE IF EXISTS contractcoffeequalitytheoreticalcomposition;
DROP TABLE IF EXISTS contractcoffeequalitycompositionpart;

CREATE TABLE contractcoffeequalitytheoreticalpercentage
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    contract_uuid varchar,
    contract_version bigint,

    CONSTRAINT PK_contractcoffeequalitytheoreticalpercentage PRIMARY KEY (id)
);

CREATE INDEX idx_contractcoffeequalitytheoreticalpercentage_uuid on contractcoffeequalitytheoreticalpercentage(uuid);

CREATE TABLE contractcoffeequalitypercentage
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    gradedconversionfactor decimal,
    coffeequality_uuid varchar,
    coffeequality_version bigint,
    contractcoffeequalitytheoreticalpercentage_id bigint,

    CONSTRAINT PK_contractcoffeequalitypercentage PRIMARY KEY (id)
);

CREATE INDEX idx_contractcoffeequalitypercentage_uuid on contractcoffeequalitypercentage(uuid);