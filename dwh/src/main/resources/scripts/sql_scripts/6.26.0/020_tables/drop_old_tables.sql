DROP TABLE IF EXISTS carryingcostgroupcompany;
DROP TABLE IF EXISTS carryingcost;

DROP TABLE IF EXISTS freightrategroupcompany;
DROP TABLE IF EXISTS freightrate;

DROP TABLE IF EXISTS packingcostgroupcompany;
DROP TABLE IF EXISTS packingcost;

DROP TABLE IF EXISTS processingcostgroupcompany;
DROP TABLE IF EXISTS processingcost;