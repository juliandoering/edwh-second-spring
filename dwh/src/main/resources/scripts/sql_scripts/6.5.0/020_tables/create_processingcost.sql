CREATE TABLE processingcost
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    cost decimal,
    validity_from date,
    coffeequalityfrom_uuid varchar,
    coffeequalityfrom_version bigint,
    coffeequalityto_uuid varchar,
    coffeequalityto_version bigint,
    costcurrency_uuid varchar,
    costcurrency_version bigint,
    costunit_uuid varchar,
    costunit_version bigint,

    CONSTRAINT PK_processingcost PRIMARY KEY (id)
);

CREATE INDEX idx_processingcost_uuid on processingcost(uuid);

CREATE TABLE processingcostgroupcompany
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    processingcost_id bigint,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_processingcostgroupcompany PRIMARY KEY (id)
);

CREATE INDEX idx_processingcostgroupcompany_uuid on processingcostgroupcompany(uuid);


ALTER TABLE processingcostgroupcompany ADD CONSTRAINT fk_processingcostgroupcompany_ProcessingCost FOREIGN KEY (processingcost_id) REFERENCES ProcessingCost;
CREATE INDEX idx_processingcostgroupcompany_processingcost_id on processingcostgroupcompany(processingcost_id);


