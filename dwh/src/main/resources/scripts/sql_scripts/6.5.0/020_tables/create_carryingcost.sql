CREATE TABLE carryingcost
(
  id bigserial,
  uuid varchar,
  -- technical fields
  created_at timestamp default current_timestamp,
  created_by varchar,
  updated_at timestamp default current_timestamp,
  updated_by varchar,
  record_version integer default 0,
  erroneous_data boolean,
  -- business fields
  validity_from date,
  currency_uuid varchar,
  currency_version bigint,
  weightunit_uuid varchar,
  weightunit_version bigint,
  interest_rate        decimal,
  insurance_rate       decimal,
  warehousing_rate     decimal,
  CONSTRAINT PK_carryingcost PRIMARY KEY (id)
);

CREATE INDEX idx_carryingcost_uuid on carryingcost(uuid);

CREATE TABLE carryingcostgroupcompany
(
  id bigserial,
  uuid varchar,
  -- technical fields
  created_at timestamp default current_timestamp,
  created_by varchar,
  updated_at timestamp default current_timestamp,
  updated_by varchar,
  record_version integer default 0,
  erroneous_data boolean,
  -- business fields
  carryingcost_id bigint,
  groupcompany_uuid varchar,
  groupcompany_version bigint,

  CONSTRAINT PK_carryingcostgroupcompany PRIMARY KEY (id)
);

CREATE INDEX idx_carryingcostgroupcompany_uuid on carryingcostgroupcompany(uuid);


ALTER TABLE carryingcostgroupcompany ADD CONSTRAINT fk_carryingcostgroupcompany_CarryingCost FOREIGN KEY (carryingcost_id) REFERENCES CarryingCost;
CREATE INDEX idx_carryingcostgroupcompany_carryingcost_id on carryingcostgroupcompany(carryingcost_id);
