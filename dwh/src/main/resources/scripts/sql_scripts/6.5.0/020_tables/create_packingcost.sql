CREATE TABLE packingcost
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    cost decimal,
    validity_from date,
    packingtype_uuid varchar,
    packingtype_version bigint,
    costcurrency_uuid varchar,
    costcurrency_version bigint,
    costunit_uuid varchar,
    costunit_version bigint,

    CONSTRAINT PK_packingcost PRIMARY KEY (id)
);

CREATE INDEX idx_packingcost_uuid on packingcost(uuid);

CREATE TABLE packingcostgroupcompany
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    packingcost_id bigint,
    groupcompany_uuid varchar,
    groupcompany_version bigint,

    CONSTRAINT PK_packingcostgroupcompany PRIMARY KEY (id)
);

CREATE INDEX idx_packingcostgroupcompany_uuid on packingcostgroupcompany(uuid);


ALTER TABLE packingcostgroupcompany ADD CONSTRAINT fk_packingcostgroupcompany_PackingCost FOREIGN KEY (packingcost_id) REFERENCES PackingCost;
CREATE INDEX idx_packingcostgroupcompany_packingcost_id on packingcostgroupcompany(packingcost_id);


