CREATE TABLE freightrate
(
  id bigserial,
  uuid varchar,
  -- technical fields
  created_at timestamp default current_timestamp,
  created_by varchar,
  updated_at timestamp default current_timestamp,
  updated_by varchar,
  record_version integer default 0,
  erroneous_data boolean,
  -- business fields
  validity_from date,
  rate decimal,
  from_uuid varchar,
  from_version bigint,
  to_uuid varchar,
  to_version bigint,
  currency_uuid varchar,
  currency_version bigint,
  weightunit_uuid varchar,
  weightunit_version bigint,
  CONSTRAINT PK_freightrate PRIMARY KEY (id)
);

CREATE INDEX idx_freightrate_uuid on freightrate(uuid);

CREATE TABLE freightrategroupcompany
(
  id bigserial,
  uuid varchar,
  -- technical fields
  created_at timestamp default current_timestamp,
  created_by varchar,
  updated_at timestamp default current_timestamp,
  updated_by varchar,
  record_version integer default 0,
  erroneous_data boolean,
  -- business fields
  freightrate_id bigint,
  groupcompany_uuid varchar,
  groupcompany_version bigint,
  CONSTRAINT PK_freightrategroupcompany PRIMARY KEY (id)
);

CREATE INDEX idx_freightrategroupcompany_uuid on freightrategroupcompany(uuid);


ALTER TABLE freightrategroupcompany ADD CONSTRAINT fk_freightrategroupcompany_FreightRate FOREIGN KEY (freightrate_id) REFERENCES FreightRate;
CREATE INDEX idx_freightrategroupcompany_freightrate_id on freightrategroupcompany(freightrate_id);
