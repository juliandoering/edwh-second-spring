CREATE TABLE coffeecontractexchangerate
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    contract_uuid varchar,
    contract_version bigint,
    exchange_rate decimal,

    CONSTRAINT PK_coffeecontractexchangerate PRIMARY KEY (id)
);

CREATE INDEX idx_coffeecontractexchangerate_uuid on coffeecontractexchangerate(uuid);