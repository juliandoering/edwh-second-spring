CREATE TABLE terminalmarketoptionvaluation
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    record_version integer default 0,
    erroneous_data boolean,

    -- business fields
    tmot_id varchar,
    valuation_date date,
    valuation_delta decimal,
    valuation_value decimal,

    CONSTRAINT PK_dwh_terminalmarketoptionvaluation PRIMARY KEY (id)
);
