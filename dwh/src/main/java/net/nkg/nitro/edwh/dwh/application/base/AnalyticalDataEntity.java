package net.nkg.nitro.edwh.dwh.application.base;

import org.hibernate.envers.Audited;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Audited
public class AnalyticalDataEntity<T extends AnalyticalDataEntity<T>> extends BaseEntity<T> {

    private boolean erroneousData;

    public boolean isErroneousData() {
        return erroneousData;
    }

    public void setErroneousData(boolean erroneousData) {
        this.erroneousData = erroneousData;
    }
}
