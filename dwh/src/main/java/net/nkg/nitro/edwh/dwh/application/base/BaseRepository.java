package net.nkg.nitro.edwh.dwh.application.base;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.history.RevisionRepository;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity<T>> extends PagingAndSortingRepository<T, Long> {

}
