package net.nkg.nitro.edwh.dwh.application.hedging;

import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.hedging.FutureMarket;
import net.nkg.nitro.edwh.model.hedging.FutureMonth;
import net.nkg.nitro.edwh.model.trade.TradeType;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TerminalMarketOptionTradeEntity {

    private String tmtId;

    private String tradeNumber;

    private LocalDate tradeDate;

    private LocalDate expirationDate;

    private BigDecimal tradePrice;

    private BigDecimal strikePrice;

    private Integer tradeQuantity;

    private Integer futuresYear;

    private boolean excercized;

    private FutureMarket futuresMarket;

    private TradeType tradeType;

    private String optionTradeCategory;

    private FutureMonth futuresMonth;

    private GroupCompany groupCompany;

}
