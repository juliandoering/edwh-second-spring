package net.nkg.nitro.edwh.dwh.infrastructure.client;

import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence.GroupCompanyEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "mdm")
public interface MdmClient {

    @GetMapping("groupcompanies/{id}")
    EntityModel<GroupCompanyEntity> getGroupCompany(@PathVariable("id") String id);
}
