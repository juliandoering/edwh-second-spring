DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='mdm') THEN CREATE USER mdm ENCRYPTED PASSWORD 'mrpersister'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='dwh') THEN CREATE USER dwh ENCRYPTED PASSWORD 'dwh_pw'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='vrd') THEN CREATE USER vrd ENCRYPTED PASSWORD 'vrd_pw'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='etl') THEN CREATE USER etl ENCRYPTED PASSWORD 'etl_pw'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='etl_pipe') THEN CREATE USER etl_pipe ENCRYPTED PASSWORD 'etl_pipe_pw'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='dwh_reporting') THEN CREATE USER dwh_reporting ENCRYPTED PASSWORD 'dwh_reporting_pw'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='registration') THEN CREATE USER registration ENCRYPTED PASSWORD 'registration_pw'; END IF;
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='staging') THEN CREATE USER staging ENCRYPTED PASSWORD 'staging_pw'; END IF;

  -- create a read-only user who is allowed to read all schemas and to create dumps
  IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname='edwh_backup') THEN CREATE USER edwh_backup SUPERUSER ENCRYPTED PASSWORD 'edwh_backup_pw'; END IF;
  ALTER USER edwh_backup set default_transaction_read_only = on;

  GRANT etl to mdm;
  GRANT etl to dwh;
  GRANT etl to vrd;
  CREATE SCHEMA IF NOT EXISTS mdm AUTHORIZATION mdm;
  CREATE SCHEMA IF NOT EXISTS mdm_spring AUTHORIZATION mdm;
  CREATE SCHEMA IF NOT EXISTS mdm_audit AUTHORIZATION mdm;
  CREATE SCHEMA IF NOT EXISTS dwh AUTHORIZATION dwh;
  CREATE SCHEMA IF NOT EXISTS dwh_spring AUTHORIZATION dwh;
  CREATE SCHEMA IF NOT EXISTS vrd AUTHORIZATION vrd;
  CREATE SCHEMA IF NOT EXISTS etl AUTHORIZATION etl;
  CREATE SCHEMA IF NOT EXISTS etl_pipe AUTHORIZATION etl_pipe;
  CREATE SCHEMA IF NOT EXISTS registration AUTHORIZATION registration;
  CREATE SCHEMA IF NOT EXISTS staging AUTHORIZATION staging;
  
  GRANT USAGE ON SCHEMA mdm to dwh_reporting;
  GRANT SELECT ON ALL TABLES IN SCHEMA mdm TO dwh_reporting;
  GRANT USAGE ON SCHEMA dwh to dwh_reporting;
  GRANT SELECT ON ALL TABLES IN SCHEMA dwh TO dwh_reporting;
  GRANT USAGE ON SCHEMA vrd to dwh_reporting;
  GRANT SELECT ON ALL TABLES IN SCHEMA vrd TO dwh_reporting;
  
  -- Required for "uuid_generate_v1()". Example: SELECT uuid_generate_v1();
  CREATE EXTENSION IF NOT EXISTS "uuid-ossp" schema pg_catalog;
END $$;

