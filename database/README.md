# Demo Database

Database image and container for a database.

Simply run the shell-script and follow the instructions.

	sh createDemoEnv.sh

	5x <Enter>

	docker start delme


**NOTE: ** This image does not use a data volume container - Changes are NOT stored. Manually set volume or use docker-compose.

The container does not anymore include sql scripts besides creating schemas for liquibase, so that liquibase can initialize the database. 

# How To Build Images For Nexus
Login to NITRO Docker Registry:

    docker login repo.nitro.nkg.net:442

#### Build Latest
* `docker build -t repo.nitro.nkg.net:442/edwh/edwh-database .`
* `docker push repo.nitro.nkg.net:442/edwh/edwh-database`

#### Build/Tag Specific Version
* `docker tag [IMAGE_ID] repo.nitro.nkg.net:442/edwh/edwh-database:[VERSION]`
* `docker push repo.nitro.nkg.net:442/edwh/edwh-database:[VERSION]`

# Docker run example
docker run -d -e "POSTGRES_PASSWORD=" -e "POSTGRES_USER=" -p 12602:5432 --restart=unless-stopped --name edwh-database -v db-data:/var/lib/postgresql/data repo.nitro.nkg.net:442/edwh/edwh-database:0.0.1

