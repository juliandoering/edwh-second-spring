#!/bin/bash
# Author: niklas finck <nf@hbt.de>

#read hostport from console
defaultport=5432
read -p "(1/5) Desired Host port to reach the DB (default: $defaultport) " hostport
# set default port if no argument has been provided
hostport=${hostport:-$defaultport}

read -p "(2/5) Name of the container (default: delme)" containername
containername=${containername:-delme}


read -p "(4/5) Please enter a Password " password 
password=${password:-edwh_pw}

read -p "(5/5) What username? " user
user=${user:-edwh}

echo "stop container ..."
docker stop $containername
echo "remove container ..."
docker rm $containername
echo "remove image ..."
docker rmi $containername
echo "build image ..."
docker build -t $containername -f Dockerfile .

docker run -p $hostport:5432 --name $containername -e POSTGRES_PASSWORD=$password -e POSTGRES_USER=$user -d $containername
#docker start delme
