package net.nkg.nitro.edwh.pcs.application.transitionevent;

import net.nkg.nitro.edwh.pcs.application.physicalcoffeestock.PhysicalCoffeeStockEntity;
import net.nkg.nitro.edwh.pcs.infrastructure.error.InvalidRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class InputOutputLogService {

    private final InputOutputLogRepository repository;
    private final TransitionEventToEntitiesMapper eventToEntitiesMapper;

    public InputOutputLogService(InputOutputLogRepository repository,
                                 TransitionEventToEntitiesMapper eventToEntitiesMapper) {
        this.repository = repository;
        this.eventToEntitiesMapper = eventToEntitiesMapper;
    }

    public void createInputFromPcs(PhysicalCoffeeStockEntity physicalCoffeeStockEntity, UUID eventId) {
        var input = new InputOutputLogEntity();
        input.setType(InputOutputLogEntity.Type.INPUT);
        input.setPhysicalCoffeeStockId(physicalCoffeeStockEntity.getId());
        input.setTransitionEventId(eventId);
        input.setWarehouseId(physicalCoffeeStockEntity.getInitialWarehouseId());
        input.setExecutedAt(physicalCoffeeStockEntity.getAppearanceDate());
        input.setVolume(physicalCoffeeStockEntity.getInitialVolume());
        repository.save(input);
    }

    public void createInputOutputsFromTransitionEvent(UUID pcsId, UUID eventId, TransitionEvent transitionEvent) {
        var inputOutputLogEntities = eventToEntitiesMapper.mapToInputOutputLog(
                pcsId, eventId, transitionEvent);
        validateOutput(inputOutputLogEntities, transitionEvent.getTransitionFrom());
        repository.saveAll(inputOutputLogEntities);
    }

    private void validateOutput(List<InputOutputLogEntity> inputOutputs, TransitionEvent.Transition transitionFrom) {
        var output = inputOutputs.stream()
                .filter(entity -> entity.getType() == InputOutputLogEntity.Type.OUTPUT)
                .findFirst();
        if (output.isPresent()) {
            var currentVolume = getCurrentVolume(output.get());
            if (currentVolume < output.get().getVolume()) {
                throw new InvalidRequestException(
                        String.format("Not enough volume available! The %s currently only holds a volume of %d.",
                                transitionFrom, currentVolume),
                        HttpStatus.BAD_REQUEST
                );
            }
        }
    }

    private int getCurrentVolume(InputOutputLogEntity entity) {
        if (entity.getWarehouseId() != null) {
            return getWarehouseVolume(entity.getPhysicalCoffeeStockId(), entity.getWarehouseId());
        }
        if (entity.getTransportId() != null) {
            return getTransportVolume(entity.getPhysicalCoffeeStockId(), entity.getTransportId());
        }
        if (entity.getProcessId() != null) {
            return getProcessVolume(entity.getPhysicalCoffeeStockId(), entity.getProcessId());
        }
        return 0;
    }

    /**
     * Returns the actual volume of the provided PCS. Meaning the volume that this PCS currently holds. Any
     * volume of this PCS that is an input into a process will not be considered.
     */
    public int getPcsVolume(UUID pcsId) {
        var logs = repository.findAllByPhysicalCoffeeStockIdAndNotInputProcess(pcsId);
        return getVolume(logs);
    }

    /**
     * Returns the initial volume of the provided PCS. This is basically a sum of all inputs into the PCS
     * that don't have a corresponding output.
     */
    public int getPcsInitialVolume(UUID pcsId) {
        var logs = repository.findAllByPhysicalCoffeeStockId(pcsId);
        return getVolume(logs);
    }

    public int getWarehouseVolume(UUID pcsId, String warehouseId) {
        var logs = repository.findAllByPhysicalCoffeeStockIdAndWarehouseId(pcsId, warehouseId);
        return getVolume(logs);
    }

    public int getTransportVolume(UUID pcsId, UUID transportId) {
        var logs = repository.findAllByPhysicalCoffeeStockIdAndTransportId(pcsId, transportId);
        return getVolume(logs);
    }

    public int getProcessVolume(UUID pcsId, UUID processId) {
        var logs = repository.findAllByPhysicalCoffeeStockIdAndProcessId(pcsId, processId);
        return getVolume(logs);
    }

    private int getVolume(List<InputOutputLogEntity> logs) {
        return logs.stream()
                .map(entity -> entity.getType() == InputOutputLogEntity.Type.INPUT ? entity.getVolume() : -entity.getVolume())
                .reduce(0, Integer::sum);
    }
}
