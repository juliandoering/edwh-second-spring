package net.nkg.nitro.edwh.pcs.application.allocation;

import net.nkg.nitro.edwh.pcs.application.base.BaseEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(schema = "dwh_spring", name = "pcs_allocation")
public class AllocationEntity extends BaseEntity<AllocationEntity> {

    @NotNull
    private UUID physicalCoffeeStockId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AllocationType type;

    private String allocatedContractId;

    private UUID allocatedProcessId;

    private Date allocationDate;

    @Positive
    private int volume;

    public UUID getPhysicalCoffeeStockId() {
        return physicalCoffeeStockId;
    }

    public void setPhysicalCoffeeStockId(UUID physicalCoffeeStockId) {
        this.physicalCoffeeStockId = physicalCoffeeStockId;
    }

    public AllocationType getType() {
        return type;
    }

    public void setType(AllocationType type) {
        this.type = type;
    }

    public String getAllocatedContractId() {
        return allocatedContractId;
    }

    public void setAllocatedContractId(String allocatedContractId) {
        this.allocatedContractId = allocatedContractId;
    }

    public UUID getAllocatedProcessId() {
        return allocatedProcessId;
    }

    public void setAllocatedProcessId(UUID allocatedProcessId) {
        this.allocatedProcessId = allocatedProcessId;
    }

    public Date getAllocationDate() {
        return allocationDate;
    }

    public void setAllocationDate(Date allocationDate) {
        this.allocationDate = allocationDate;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
