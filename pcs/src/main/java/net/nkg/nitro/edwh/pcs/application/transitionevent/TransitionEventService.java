package net.nkg.nitro.edwh.pcs.application.transitionevent;

import net.nkg.nitro.edwh.pcs.application.physicalcoffeestock.PhysicalCoffeeStockEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TransitionEventService {

    private final TransitionEventRepository transitionEventRepository;
    private final TransitionEventToEntitiesMapper eventToEntitiesMapper;
    private final InputOutputLogService inputOutputLogService;

    public TransitionEventService(TransitionEventRepository transitionEventRepository,
                                  TransitionEventToEntitiesMapper eventToEntitiesMapper,
                                  InputOutputLogService inputOutputLogService) {
        this.transitionEventRepository = transitionEventRepository;
        this.eventToEntitiesMapper = eventToEntitiesMapper;
        this.inputOutputLogService = inputOutputLogService;
    }

    public void processInitialPcs(PhysicalCoffeeStockEntity entity) {
        var eventEntity = createFromPcs(entity);
        eventEntity = transitionEventRepository.save(eventEntity);

        inputOutputLogService.createInputFromPcs(entity, eventEntity.getId());
    }

    public void processTransitionEvent(UUID pcsId, TransitionEvent transitionEvent) {
        var eventEntity = eventToEntitiesMapper.mapToEventEntity(transitionEvent);
        eventEntity = transitionEventRepository.save(eventEntity);

        inputOutputLogService.createInputOutputsFromTransitionEvent(pcsId, eventEntity.getId(), transitionEvent);
    }

    private TransitionEventEntity createFromPcs(PhysicalCoffeeStockEntity entity) {
        var eventEntity = new TransitionEventEntity();
        eventEntity.setSubmissionTime(entity.getCreatedAt());
        eventEntity.setType(TransitionEventEntity.Type.INPUT);
        return eventEntity;
    }


}
