package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import net.nkg.nitro.edwh.pcs.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "coffeestocks", itemResourceRel = "coffeestock", path = "coffeestocks")
public interface PhysicalCoffeeStockRepository extends BaseRepository<PhysicalCoffeeStockEntity> {
}
