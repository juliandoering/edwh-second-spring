package net.nkg.nitro.edwh.pcs.application.transport;

import net.nkg.nitro.edwh.pcs.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "transports", itemResourceRel = "transport", path = "transports")
public interface TransportRepository extends BaseRepository<TransportEntity> {
}
