package net.nkg.nitro.edwh.pcs.application.allocation;

import net.nkg.nitro.edwh.pcs.application.base.BaseRepository;
import net.nkg.nitro.edwh.pcs.application.physicalcoffeestock.PhysicalCoffeeStockEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "allocations", itemResourceRel = "allocation", path = "allocations")
public interface AllocationRepository extends BaseRepository<AllocationEntity> {

    List<AllocationEntity> findAllByPhysicalCoffeeStockIdAndType(UUID physicalCoffeeStockId, AllocationType type);
}
