package net.nkg.nitro.edwh.pcs.infrastructure.error;

import org.springframework.http.HttpStatus;

public class InvalidRequestException extends RuntimeException {

    private final HttpStatus httpStatus;

    public InvalidRequestException(HttpStatus httpStatus) {
        super();
        this.httpStatus = httpStatus;
    }

    public InvalidRequestException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public InvalidRequestException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public InvalidRequestException(Throwable cause, HttpStatus httpStatus) {
        super(cause);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
