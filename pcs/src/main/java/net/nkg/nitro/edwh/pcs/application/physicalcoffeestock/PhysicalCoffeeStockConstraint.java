package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PhysicalCoffeeStockValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface PhysicalCoffeeStockConstraint {
    String message() default "Invalid Transition Event";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
