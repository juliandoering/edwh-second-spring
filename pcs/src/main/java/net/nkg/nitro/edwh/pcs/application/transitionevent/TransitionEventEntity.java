package net.nkg.nitro.edwh.pcs.application.transitionevent;

import net.nkg.nitro.edwh.pcs.application.base.BaseEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(schema = "dwh_spring", name = "pcs_transition_event")
public class TransitionEventEntity extends BaseEntity<TransitionEventEntity> {

    public enum Type {
        INPUT, OUTPUT, TRANSPORT, STORE, PROCESS, SHIP
    }

    @Enumerated(EnumType.STRING)
    private Type type;

    private Date submissionTime;

    private String comment;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Date submissionTime) {
        this.submissionTime = submissionTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
