package net.nkg.nitro.edwh.pcs.application.transitionevent;

import net.nkg.nitro.edwh.pcs.application.base.BaseRepository;
import net.nkg.nitro.edwh.pcs.application.physicalcoffeestock.PhysicalCoffeeStockEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "transitionevents", itemResourceRel = "transitionevent", path = "transitionevents")
public interface TransitionEventRepository extends BaseRepository<TransitionEventEntity> {
}
