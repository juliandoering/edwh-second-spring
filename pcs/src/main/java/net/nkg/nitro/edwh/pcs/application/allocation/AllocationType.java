package net.nkg.nitro.edwh.pcs.application.allocation;

public enum AllocationType {
    PURCHASE_CONTRACT, SALES_CONTRACT, PROCESS_INPUT, PROCESS_OUTPUT
}
