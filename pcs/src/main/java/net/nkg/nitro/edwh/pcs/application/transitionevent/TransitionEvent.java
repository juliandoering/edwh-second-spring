package net.nkg.nitro.edwh.pcs.application.transitionevent;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

public class TransitionEvent {

    public enum Transition {
        NONE, WAREHOUSE, TRANSPORT, PROCESS
    }

    @NotNull
    private Date submissionTime;

    @NotNull
    private Date transitionDate;

    @NotNull
    private Transition transitionFrom;

    private String transitionFromId;

    @NotNull
    private Transition transitionTo;

    private String transitionToId;

    @Positive
    private int volume;

    private String comment;

    public Date getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Date submissionTime) {
        this.submissionTime = submissionTime;
    }

    public Date getTransitionDate() {
        return transitionDate;
    }

    public void setTransitionDate(Date transitionDate) {
        this.transitionDate = transitionDate;
    }

    public Transition getTransitionFrom() {
        return transitionFrom;
    }

    public void setTransitionFrom(Transition transitionFrom) {
        this.transitionFrom = transitionFrom;
    }

    public String getTransitionFromId() {
        return transitionFromId;
    }

    public void setTransitionFromId(String transitionFromId) {
        this.transitionFromId = transitionFromId;
    }

    public Transition getTransitionTo() {
        return transitionTo;
    }

    public void setTransitionTo(Transition transitionTo) {
        this.transitionTo = transitionTo;
    }

    public String getTransitionToId() {
        return transitionToId;
    }

    public void setTransitionToId(String transitionToId) {
        this.transitionToId = transitionToId;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
