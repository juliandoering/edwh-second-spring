package net.nkg.nitro.edwh.pcs.application.process;

import net.nkg.nitro.edwh.pcs.application.allocation.AllocationEntity;
import net.nkg.nitro.edwh.pcs.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "processes", itemResourceRel = "process", path = "processes")
public interface ProcessRepository extends BaseRepository<ProcessEntity> {
}
