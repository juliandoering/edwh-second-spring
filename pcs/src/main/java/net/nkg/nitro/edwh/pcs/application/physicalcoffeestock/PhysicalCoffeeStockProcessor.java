package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class PhysicalCoffeeStockProcessor implements RepresentationModelProcessor<EntityModel<PhysicalCoffeeStockEntity>> {

    @Override
    public EntityModel<PhysicalCoffeeStockEntity> process(EntityModel<PhysicalCoffeeStockEntity> model) {
        var id = model.getContent().getId();

        var transitionLink = linkTo(
                methodOn(PhysicalCoffeeStockController.class).transitionEvent(id, null)
        ).withRel("transition");
        model.add(transitionLink);

        var allocationLink = linkTo(
                methodOn(PhysicalCoffeeStockController.class).allocationEvent(id, null)
        ).withRel("allocate");
        model.add(allocationLink);

        return model;
    }

}
