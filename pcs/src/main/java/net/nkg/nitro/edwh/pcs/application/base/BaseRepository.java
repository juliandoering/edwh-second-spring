package net.nkg.nitro.edwh.pcs.application.base;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.UUID;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity<T>>
        extends PagingAndSortingRepository<T, UUID>, RevisionRepository<T, UUID, Integer> {

}
