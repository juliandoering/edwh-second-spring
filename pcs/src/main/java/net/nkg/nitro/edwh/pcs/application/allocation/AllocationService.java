package net.nkg.nitro.edwh.pcs.application.allocation;

import net.nkg.nitro.edwh.pcs.application.transitionevent.InputOutputLogService;
import net.nkg.nitro.edwh.pcs.infrastructure.error.InvalidRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class AllocationService {

    private final AllocationRepository repository;
    private final InputOutputLogService inputOutputLogService;

    public AllocationService(AllocationRepository repository, InputOutputLogService inputOutputLogService) {
        this.repository = repository;
        this.inputOutputLogService = inputOutputLogService;
    }

    public void createAllocationFromEvent(UUID pcsId, AllocationEvent event) {
        validate(pcsId, event.getAllocationType(), event.getVolume());

        var entity = new AllocationEntity();
        entity.setPhysicalCoffeeStockId(pcsId);
        entity.setAllocationDate(event.getAllocationDate());
        entity.setType(event.getAllocationType());
        entity.setVolume(event.getVolume());

        switch (event.getAllocationType()) {
            case PROCESS_INPUT:
            case PROCESS_OUTPUT:
                entity.setAllocatedProcessId(UUID.fromString(event.getAllocationId()));
                break;
            case SALES_CONTRACT:
            case PURCHASE_CONTRACT:
                entity.setAllocatedContractId(event.getAllocationId());
                break;
            default:
                throw new IllegalArgumentException("Unknown Allocation Type!");
        }

        repository.save(entity);
    }

    private void validate(UUID pcsId, AllocationType type, int volume) {
        if (type == AllocationType.PROCESS_INPUT || type == AllocationType.SALES_CONTRACT) {
            validateOutputs(pcsId, volume);
        }
    }

    private void validateOutputs(UUID pcsId, int volume) {
        var initialPcsVolume = inputOutputLogService.getPcsInitialVolume(pcsId);
        var currentOutputAllocatedVolume = getCurrentOutputAllocatedVolume(pcsId);
        var outputAllocatedVolume = currentOutputAllocatedVolume + volume;
        if (initialPcsVolume < outputAllocatedVolume) {
            throw new InvalidRequestException(
                    String.format("Cannot allocate a volume of %d! This Physical Coffee Stock currently has an allocated volume of %d and an available volume of %d",
                            volume, currentOutputAllocatedVolume, initialPcsVolume),
                    HttpStatus.BAD_REQUEST
            );
        }
    }

    public int getCurrentInputAllocatedVolume(UUID pcsId) {
        return getCurrentAllocatedVolume(pcsId, AllocationType.PROCESS_OUTPUT, AllocationType.PURCHASE_CONTRACT);
    }

    public int getCurrentOutputAllocatedVolume(UUID pcsId) {
        return getCurrentAllocatedVolume(pcsId, AllocationType.PROCESS_INPUT, AllocationType.SALES_CONTRACT);
    }

    public int getCurrentAllocatedVolume(UUID pcsId, AllocationType... types) {
        return getByTypes(pcsId, types).stream()
                .map(AllocationEntity::getVolume)
                .reduce(0, Integer::sum);
    }

    public List<AllocationEntity> getByTypes(UUID pcsId, AllocationType... types) {
        var allocations = new ArrayList<AllocationEntity>();
        for (var type : types) {
            allocations.addAll(repository.findAllByPhysicalCoffeeStockIdAndType(pcsId, type));
        }
        return allocations;
    }


}
