package net.nkg.nitro.edwh.pcs.application.transitionevent;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(schema = "dwh_spring", name = "pcs_input_output_log")
public class InputOutputLogEntity {

    public enum Type {
        INPUT, OUTPUT
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private long version;

    private UUID physicalCoffeeStockId;

    private UUID transitionEventId;

    @Enumerated(EnumType.STRING)
    private Type type;

    private Date executedAt;

    private String warehouseId;

    private UUID transportId;

    private UUID processId;

    private int volume;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public UUID getPhysicalCoffeeStockId() {
        return physicalCoffeeStockId;
    }

    public void setPhysicalCoffeeStockId(UUID physicalCoffeeStockId) {
        this.physicalCoffeeStockId = physicalCoffeeStockId;
    }

    public UUID getTransitionEventId() {
        return transitionEventId;
    }

    public void setTransitionEventId(UUID transitionEventId) {
        this.transitionEventId = transitionEventId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getExecutedAt() {
        return executedAt;
    }

    public void setExecutedAt(Date executedAt) {
        this.executedAt = executedAt;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public UUID getTransportId() {
        return transportId;
    }

    public void setTransportId(UUID transportId) {
        this.transportId = transportId;
    }

    public UUID getProcessId() {
        return processId;
    }

    public void setProcessId(UUID processId) {
        this.processId = processId;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
