package net.nkg.nitro.edwh.pcs.application.transitionevent;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface InputOutputLogRepository extends CrudRepository<InputOutputLogEntity, Long> {

    List<InputOutputLogEntity> findAllByPhysicalCoffeeStockIdAndWarehouseId(UUID physicalCoffeeStockId, String warehouseId);

    List<InputOutputLogEntity> findAllByPhysicalCoffeeStockIdAndTransportId(UUID physicalCoffeeStockId, UUID transportId);

    List<InputOutputLogEntity> findAllByPhysicalCoffeeStockIdAndProcessId(UUID physicalCoffeeStockId, UUID processId);

    List<InputOutputLogEntity> findAllByPhysicalCoffeeStockId(UUID physicalCoffeeStockId);

    @Query("select e from InputOutputLogEntity e where e.physicalCoffeeStockId = ?1 and not (e.processId is not null and e.type = 'INPUT')")
    List<InputOutputLogEntity> findAllByPhysicalCoffeeStockIdAndNotInputProcess(UUID physicalCoffeeStockId);

}
