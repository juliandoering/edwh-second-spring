package net.nkg.nitro.edwh.pcs.application.transport;

import net.nkg.nitro.edwh.pcs.application.base.BaseEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(schema = "dwh_spring", name = "pcs_transport")
public class TransportEntity extends BaseEntity<TransportEntity> {

    @NotBlank
    private String fromWarehouseId;

    @NotBlank
    private String toWarehouseId;

    private Date departureDate;

    public String getFromWarehouseId() {
        return fromWarehouseId;
    }

    public void setFromWarehouseId(String fromWarehouseId) {
        this.fromWarehouseId = fromWarehouseId;
    }

    public String getToWarehouseId() {
        return toWarehouseId;
    }

    public void setToWarehouseId(String toWarehouseId) {
        this.toWarehouseId = toWarehouseId;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
}
