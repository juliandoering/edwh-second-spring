package net.nkg.nitro.edwh.pcs.infrastructure.error;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { InvalidRequestException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        var exImpl = (InvalidRequestException) ex;
        return handleExceptionInternal(ex, exImpl.getMessage(), new HttpHeaders(), exImpl.getHttpStatus(), request);
    }

}
