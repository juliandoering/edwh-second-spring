package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import net.nkg.nitro.edwh.pcs.application.transitionevent.TransitionEventService;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler
public class PhysicalCoffeeStockEventHandler {

    private final TransitionEventService transitionEventService;

    public PhysicalCoffeeStockEventHandler(TransitionEventService transitionEventService) {
        this.transitionEventService = transitionEventService;
    }

    @HandleAfterCreate
    public void handleBeforeCreate(PhysicalCoffeeStockEntity entity) {
        transitionEventService.processInitialPcs(entity);
    }
}
