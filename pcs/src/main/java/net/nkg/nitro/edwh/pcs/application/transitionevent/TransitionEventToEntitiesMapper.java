package net.nkg.nitro.edwh.pcs.application.transitionevent;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class TransitionEventToEntitiesMapper {

    public TransitionEventEntity mapToEventEntity(TransitionEvent transitionEvent) {
        var entity = new TransitionEventEntity();
        entity.setSubmissionTime(transitionEvent.getSubmissionTime());
        entity.setType(mapEventType(transitionEvent.getTransitionTo()));
        entity.setComment(transitionEvent.getComment());
        return entity;
    }

    private TransitionEventEntity.Type mapEventType(TransitionEvent.Transition transitionTo) {
        switch (transitionTo) {
            case WAREHOUSE:
                return TransitionEventEntity.Type.STORE;
            case PROCESS:
                return TransitionEventEntity.Type.PROCESS;
            case TRANSPORT:
                return TransitionEventEntity.Type.TRANSPORT;
            case NONE:
                return TransitionEventEntity.Type.SHIP;
            default:
                throw new IllegalArgumentException("Unknown Transition: " + transitionTo.name());
        }
    }

    public List<InputOutputLogEntity> mapToInputOutputLog(UUID pcsId, UUID eventId, TransitionEvent transitionEvent) {
        var inputOutputs = new ArrayList<Optional<InputOutputLogEntity>>();

        inputOutputs.add(mapToOutput(transitionEvent));
        inputOutputs.add(mapToInput(transitionEvent));

        return inputOutputs.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(entity -> setGeneralFields(entity, pcsId, eventId, transitionEvent))
                .collect(Collectors.toList());
    }

    private Optional<InputOutputLogEntity> mapToOutput(TransitionEvent transitionEvent) {
        if (transitionEvent.getTransitionFrom() == TransitionEvent.Transition.NONE) {
            return Optional.empty();
        }
        var output = new InputOutputLogEntity();
        output.setType(InputOutputLogEntity.Type.OUTPUT);
        setTransitionFields(output, transitionEvent.getTransitionFrom(), transitionEvent.getTransitionFromId());
        return Optional.of(output);
    }

    private Optional<InputOutputLogEntity> mapToInput(TransitionEvent transitionEvent) {
        if (transitionEvent.getTransitionTo() == TransitionEvent.Transition.NONE) {
            return Optional.empty();
        }
        var input = new InputOutputLogEntity();
        input.setType(InputOutputLogEntity.Type.INPUT);
        setTransitionFields(input, transitionEvent.getTransitionTo(), transitionEvent.getTransitionToId());
        return Optional.of(input);
    }

    private void setTransitionFields(InputOutputLogEntity entity,
                                     TransitionEvent.Transition transition,
                                     String transitionId) {
        switch (transition) {
            case WAREHOUSE:
                entity.setWarehouseId(transitionId);
                break;
            case TRANSPORT:
                entity.setTransportId(UUID.fromString(transitionId));
                break;
            case PROCESS:
                entity.setProcessId(UUID.fromString(transitionId));
                break;
        }
    }

    private InputOutputLogEntity setGeneralFields(InputOutputLogEntity entity,
                                                  UUID pcsId, UUID eventId,
                                                  TransitionEvent transitionEvent) {
        entity.setPhysicalCoffeeStockId(pcsId);
        entity.setTransitionEventId(eventId);
        entity.setExecutedAt(transitionEvent.getTransitionDate());
        entity.setVolume(transitionEvent.getVolume());
        return entity;
    }
}
