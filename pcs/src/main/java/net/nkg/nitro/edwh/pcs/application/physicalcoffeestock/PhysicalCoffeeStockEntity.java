package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.nkg.nitro.edwh.pcs.application.base.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(schema = "dwh_spring", name = "physical_coffee_stock")
@PhysicalCoffeeStockConstraint
public class PhysicalCoffeeStockEntity extends BaseEntity<PhysicalCoffeeStockEntity> {

    @Column(name = "groupcompany_internal_id")
    private String groupCompanyInternalId;

    @NotNull
    private Date appearanceDate;

    private UUID originProcessId;

    private String originContractId;

    private boolean isConsignment;

    private Date disappearanceDate;

    @NotBlank
    @Column(name = "holder_groupcompany_id")
    private String holderGroupCompanyId;

    @Column(name = "owner_groupcompany_id")
    private String ownerGroupCompanyId;

    @Column(name = "owner_businessthirdparty_id")
    private String ownerBusinessThirdPartyId;

    @NotBlank
    @Column(name = "quantityunit_id")
    private String quantityUnitId;

    private String packingId;

    @NotBlank
    @Column(name = "coffeequality_id")
    private String coffeeQualityId;

    @Min(2000)
    @Max(2100)
    private int cropYear;

    @Positive
    @Transient
    private int initialVolume;

    @NotBlank
    @Transient
    private String initialWarehouseId;

    public String getGroupCompanyInternalId() {
        return groupCompanyInternalId;
    }

    public void setGroupCompanyInternalId(String groupCompanyInternalId) {
        this.groupCompanyInternalId = groupCompanyInternalId;
    }

    public Date getAppearanceDate() {
        return appearanceDate;
    }

    public void setAppearanceDate(Date creationDate) {
        this.appearanceDate = creationDate;
    }

    public UUID getOriginProcessId() {
        return originProcessId;
    }

    public void setOriginProcessId(UUID originProcessId) {
        this.originProcessId = originProcessId;
    }

    public String getOriginContractId() {
        return originContractId;
    }

    public void setOriginContractId(String originContractId) {
        this.originContractId = originContractId;
    }

    public boolean isConsignment() {
        return isConsignment;
    }

    public void setConsignment(boolean consignment) {
        isConsignment = consignment;
    }

    public Date getDisappearanceDate() {
        return disappearanceDate;
    }

    public void setDisappearanceDate(Date disappearanceDate) {
        this.disappearanceDate = disappearanceDate;
    }

    public String getHolderGroupCompanyId() {
        return holderGroupCompanyId;
    }

    public void setHolderGroupCompanyId(String groupCompanyHolderId) {
        this.holderGroupCompanyId = groupCompanyHolderId;
    }

    public String getOwnerGroupCompanyId() {
        return ownerGroupCompanyId;
    }

    public void setOwnerGroupCompanyId(String ownerGroupCompanyId) {
        this.ownerGroupCompanyId = ownerGroupCompanyId;
    }

    public String getOwnerBusinessThirdPartyId() {
        return ownerBusinessThirdPartyId;
    }

    public void setOwnerBusinessThirdPartyId(String ownerBusinessThirdPartyId) {
        this.ownerBusinessThirdPartyId = ownerBusinessThirdPartyId;
    }

    public String getQuantityUnitId() {
        return quantityUnitId;
    }

    public void setQuantityUnitId(String quantityUnitId) {
        this.quantityUnitId = quantityUnitId;
    }

    public String getPackingId() {
        return packingId;
    }

    public void setPackingId(String packingId) {
        this.packingId = packingId;
    }

    public String getCoffeeQualityId() {
        return coffeeQualityId;
    }

    public void setCoffeeQualityId(String coffeeQualityId) {
        this.coffeeQualityId = coffeeQualityId;
    }

    public int getCropYear() {
        return cropYear;
    }

    public void setCropYear(int cropYear) {
        this.cropYear = cropYear;
    }

    @JsonIgnore
    public int getInitialVolume() {
        return initialVolume;
    }

    @JsonProperty
    public void setInitialVolume(int initialVolume) {
        this.initialVolume = initialVolume;
    }

    @JsonIgnore
    public String getInitialWarehouseId() {
        return initialWarehouseId;
    }

    @JsonProperty
    public void setInitialWarehouseId(String initialWarehouseId) {
        this.initialWarehouseId = initialWarehouseId;
    }
}
