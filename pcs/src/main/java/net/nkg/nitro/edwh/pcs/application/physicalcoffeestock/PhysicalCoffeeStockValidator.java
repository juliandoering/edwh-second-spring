package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import net.nkg.nitro.edwh.pcs.infrastructure.util.ValidationErrorMessage;
import net.nkg.nitro.edwh.pcs.infrastructure.util.ValidationResult;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class PhysicalCoffeeStockValidator
        implements ConstraintValidator<PhysicalCoffeeStockConstraint, PhysicalCoffeeStockEntity> {

    @Override
    public void initialize(PhysicalCoffeeStockConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(PhysicalCoffeeStockEntity entity, ConstraintValidatorContext constraintValidatorContext) {
        var message = ValidationErrorMessage.fromContext(constraintValidatorContext);
        var result = new ValidationResult();

        result.add(validateOrigin(entity, message));
        result.add(validateOwner(entity, message));

        return result.isValid();
    }

    private boolean validateOrigin(PhysicalCoffeeStockEntity entity, ValidationErrorMessage message) {
        var origins = Arrays.asList(
                entity.getOriginContractId() != null,
                entity.getOriginProcessId() != null,
                entity.isConsignment()
        );
        var originCount = origins.stream().filter(hasOrigin -> hasOrigin).count();
        if (originCount < 1) {
            message.addMessageKey("pcs.origin.missing");
            return false;
        }
        if (originCount > 1) {
            message.addMessageKey("pcs.origin.toomany");
            return false;
        }
        return true;
    }

    private boolean validateOwner(PhysicalCoffeeStockEntity entity, ValidationErrorMessage message) {
        if (entity.getOwnerGroupCompanyId() == null && entity.getOwnerBusinessThirdPartyId() == null) {
            message.addMessageKey("pcs.owner.missing");
            return false;
        }
        if (entity.getOwnerGroupCompanyId() != null && entity.getOwnerBusinessThirdPartyId() != null) {
            message.addMessageKey("pcs.owner.toomany");
            return false;
        }
        return true;
    }

}
