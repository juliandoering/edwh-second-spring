package net.nkg.nitro.edwh.pcs.application.physicalcoffeestock;

import net.nkg.nitro.edwh.pcs.application.allocation.AllocationEvent;
import net.nkg.nitro.edwh.pcs.application.allocation.AllocationService;
import net.nkg.nitro.edwh.pcs.application.transitionevent.TransitionEvent;
import net.nkg.nitro.edwh.pcs.application.transitionevent.TransitionEventService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.UUID;

@RepositoryRestController
public class PhysicalCoffeeStockController {

    private final TransitionEventService transitionEventService;
    private final AllocationService allocationService;

    public PhysicalCoffeeStockController(TransitionEventService transitionEventService,
                                         AllocationService allocationService) {
        this.transitionEventService = transitionEventService;
        this.allocationService = allocationService;
    }

    @PostMapping("coffeestocks/{id}/transition")
    @ResponseBody
    public ResponseEntity<?> transitionEvent(@PathVariable("id") UUID id, @Valid @RequestBody TransitionEvent event) {
        transitionEventService.processTransitionEvent(id, event);
        return ResponseEntity.ok().build();
    }

    @PostMapping("coffeestocks/{id}/allocate")
    @ResponseBody
    public ResponseEntity<?> allocationEvent(@PathVariable("id") UUID id, @Valid @RequestBody AllocationEvent event) {
        allocationService.createAllocationFromEvent(id, event);
        return ResponseEntity.ok().build();
    }
}
