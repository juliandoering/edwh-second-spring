package net.nkg.nitro.edwh.pcs.application.process;

import net.nkg.nitro.edwh.pcs.application.base.BaseEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(schema = "dwh_spring", name = "pcs_process")
public class ProcessEntity extends BaseEntity<ProcessEntity> {

    @Column(name = "groupcompany_internal_id")
    private String groupCompanyInternalId;

    @NotNull
    private Date startingDate;

    @NotNull
    private Date closingDate;

    @NotBlank
    @Column(name = "groupcompany_id")
    private String groupCompanyId;

    @NotBlank
    @Column(name = "processingtype_id")
    private String processTypeId;

    public String getGroupCompanyInternalId() {
        return groupCompanyInternalId;
    }

    public void setGroupCompanyInternalId(String groupCompanyInternalId) {
        this.groupCompanyInternalId = groupCompanyInternalId;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public String getGroupCompanyId() {
        return groupCompanyId;
    }

    public void setGroupCompanyId(String groupCompanyId) {
        this.groupCompanyId = groupCompanyId;
    }

    public String getProcessTypeId() {
        return processTypeId;
    }

    public void setProcessTypeId(String processTypeId) {
        this.processTypeId = processTypeId;
    }
}
