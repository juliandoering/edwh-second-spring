ALTER TABLE holidays ADD COLUMN futuresmarket_id bigint;

-- Add reference to futures market by country
UPDATE holidays SET futuresmarket_id = (SELECT id FROM futuremarket WHERE code = 'LKD' AND is_version = false) WHERE country = 'GB';
UPDATE holidays SET futuresmarket_id = (SELECT id FROM futuremarket WHERE code = 'KC' AND is_version = false) WHERE country = 'US';

-- Field country is not requiered, due to the link to futures market
ALTER TABLE holidays DROP COLUMN country;