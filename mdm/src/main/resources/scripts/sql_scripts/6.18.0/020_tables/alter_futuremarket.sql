ALTER TABLE futuremarket ADD COLUMN days_before_end integer;
-- If not London, then it is NY
ALTER TABLE futuremarket ADD COLUMN is_first_notice_day_london boolean default false;