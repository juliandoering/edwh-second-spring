CREATE TABLE businessthirdpartyreferenceidtype
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    code varchar,
    description varchar,
    idsuppliermandatory boolean,

    CONSTRAINT pk_businessthirdpartyreferenceidtype PRIMARY KEY (id)
);

CREATE INDEX idx_businessthirdpartyreferenceidtype_uuid on businessthirdpartyreferenceidtype(uuid);
