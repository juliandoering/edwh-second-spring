-- Insert GERMANY
INSERT INTO location (uuid,deleted,record_version,version,is_version,parent,country_code,location_code,name,location_type,latitude,longitude,created_by,updated_by,si_unlocode) 
VALUES ('25d8c748-9eef-11e9-a2a3-2a2ae2dbcce4',false,0,0,false,0,'DE',NULL,'GERMANY',NULL,NULL,NULL,'edwh_admin','edwh_admin','DE');


insert into location (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, si_unlocode)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, country_code, location_code, name, location_type, latitude, longitude, si_unlocode from location
  where is_version is false
  and uuid in ('d11270fc-7a27-11e8-b8b2-0242ac120006', 'ff98df88-7247-11e9-a923-1681be663d3e', '25d8c748-9eef-11e9-a2a3-2a2ae2dbcce4');


-- Insert trades

  INSERT INTO freightratetrade (UUID, name)
VALUES ( 'ff98be5e-7247-11e9-a923-1681be663d3e', 'Argentina tea (FOB)');

insert into freightratetrade (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from freightratetrade 
  where is_version is false and uuid = 'ff98be5e-7247-11e9-a923-1681be663d3e';

-- Insert loadings

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( 'ff98c0f2-7247-11e9-a923-1681be663d3e', 'Buenos Aires',
    (select min(id) from freightratetrade where uuid = 'ff98be5e-7247-11e9-a923-1681be663d3e' and is_version is false) );


INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( '44e70fc6-7250-11e9-a923-1681be663d3e', 'Santos',
    (select min(id) from freightratetrade where uuid = 'ff98be5e-7247-11e9-a923-1681be663d3e' and is_version is false) );


insert into freightrateloading (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name, freightratetrade_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name,
    (select min(freightratetrade.id) from freightratetrade where freightratetrade.parent = freightrateloading.freightratetrade_id)
  from freightrateloading where is_version is false and uuid in ('ff98c0f2-7247-11e9-a923-1681be663d3e', '44e70fc6-7250-11e9-a923-1681be663d3e');

  
INSERT INTO freightrateloading_location (UUID, freightrateloading_id, location_id)
VALUES ( '44e70fc6-7250-11e9-a923-1681be663d3e',
	(select min(id) from freightrateloading where uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e' and is_version is false),
	(select min(id) from location where uuid = 'ff98df88-7247-11e9-a923-1681be663d3e' and is_version is false) );


INSERT INTO freightrateloading_location (UUID, freightrateloading_id, location_id)
VALUES ( 'd60189c4-9f09-11e9-a2a3-2a2ae2dbcce4',
	(select min(id) from freightrateloading where uuid = '44e70fc6-7250-11e9-a923-1681be663d3e' and is_version is false),
	(select min(id) from location where uuid = 'e66300a9-f9d4-11e7-a9bb-99401455bd6c' and is_version is false) );


insert into freightrateloading_location (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, freightrateloading_id, location_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, freightrateloading_id, location_id
  from freightrateloading_location where is_version is false and uuid in ('44e70fc6-7250-11e9-a923-1681be663d3e', 'd60189c4-9f09-11e9-a2a3-2a2ae2dbcce4');
	

-- add line

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '2094ce1a-9f14-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'HAPAG') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '2094d090-9f14-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'HAPAG') );

insert into freightrateline (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, freightrateloading_id, provider_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id,
  (select min(freightrateloading.id) from freightrateloading where freightrateloading.is_version is true and freightrateloading.parent = freightrateline.freightrateloading_id),
  (select min(businessthirdpartygroup.id) from businessthirdpartygroup where businessthirdpartygroup.is_version is true and businessthirdpartygroup.parent = freightrateline.provider_id)
  from freightrateline where is_version is false
  and uuid in ('2094ce1a-9f14-11e9-a2a3-2a2ae2dbcce4', '2094d090-9f14-11e9-a2a3-2a2ae2dbcce4');

-- add route

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( 'eb9bb69e-9f16-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '2094ce1a-9f14-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( 'eb9bb8f6-9f16-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '2094d090-9f14-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );

insert into freightratetraderoute (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, frequency, freightrateline_id, podlocation_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, frequency,
  (select min(freightrateline.id) from freightrateline where freightrateline.is_version is true and freightrateline.parent = freightratetraderoute.freightrateline_id),
  (select min(location.id) from location where location.is_version is true and location.parent = freightratetraderoute.podlocation_id)
  from freightratetraderoute where is_version is false
  and uuid in ('eb9bb69e-9f16-11e9-a2a3-2a2ae2dbcce4', 'eb9bb8f6-9f16-11e9-a2a3-2a2ae2dbcce4');



-- add rates
INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '3be82dd0-9f17-11e9-a2a3-2a2ae2dbcce4', 1449, 27, 200, false, 'Hamburg-City', 100, true, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'eb9bb69e-9f16-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '3be8301e-9f17-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'eb9bb8f6-9f16-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

insert into freightrate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, thccurrency_id, oceanfreightcurrency_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to,
  (select min(freightratetraderoute.id) from freightratetraderoute where freightratetraderoute.is_version is true and freightratetraderoute.parent = freightrate.freightratetraderoute_id),
  (select min(containersize.id) from containersize where containersize.is_version is true and containersize.parent = freightrate.containersize_id),
  (select min(containermovement.id) from containermovement where containermovement.is_version is true and containermovement.parent = freightrate.containermovement_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.oceanfreightcurrency_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.thccurrency_id)
  from freightrate where is_version is false
  and uuid in ('3be82dd0-9f17-11e9-a2a3-2a2ae2dbcce4', '3be8301e-9f17-11e9-a2a3-2a2ae2dbcce4');
