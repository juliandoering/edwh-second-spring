insert into businessthirdpartyreferenceidtype (uuid, code, description, idsuppliermandatory)
values ('4fd0a514-9988-11e9-a2a3-2a2ae2dbcce4', 'IID', 'International Identifier', false);

insert into businessthirdpartyreferenceidtype (uuid, code, description, idsuppliermandatory)
values ('5700c12a-9988-11e9-a2a3-2a2ae2dbcce4', 'INS', 'Insurance', true);

insert into businessthirdpartyreferenceidtype (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description, idsuppliermandatory) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description, idsuppliermandatory from businessthirdpartyreferenceidtype where is_version is false;
