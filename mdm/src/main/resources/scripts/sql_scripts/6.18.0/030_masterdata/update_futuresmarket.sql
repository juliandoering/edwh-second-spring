UPDATE futuremarket SET days_before_end = 3 WHERE code = 'LKD';
UPDATE futuremarket SET days_before_end = 8 WHERE code = 'KC';
UPDATE futuremarket SET is_first_notice_day_london = true WHERE code = 'LKD';
-- Replace wrong code LKD with RC
UPDATE futuremarket SET code = 'RC' WHERE code = 'LKD';