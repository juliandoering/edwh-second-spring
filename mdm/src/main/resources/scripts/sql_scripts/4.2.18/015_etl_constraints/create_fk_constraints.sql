--changeset generated_nf:1497621355 
-- Foreign key constraints of the ETL import schema ordered almost alphabetically by table name 

-- party on etl import schema
ALTER TABLE etl.bookingconfir_party ADD CONSTRAINT fk_party_bookingconfirmation FOREIGN KEY (booking_confirmation_id) REFERENCES etl.bookingconfirmation;

-- transportation detail on etl schema
ALTER TABLE etl.bookingconfir_transportationdetails ADD CONSTRAINT fk_transportationdetails_bookingconfirmation FOREIGN KEY (booking_confirmation_id) REFERENCES etl.bookingconfirmation;

-- transportation location on etl schema
ALTER TABLE etl.bookingconfir_transportationlocation ADD CONSTRAINT fk_transportationlocation_bookingconfir_transportationdetails FOREIGN KEY (bookingconfir_transportation_details_id) REFERENCES etl.bookingconfir_transportationdetails;

-- goods detail on etl import schema
ALTER TABLE etl.bookingconfir_goodsdetail ADD CONSTRAINT fk_goodsdetail_bookingconfirmation FOREIGN KEY (booking_confirmation_id) REFERENCES etl.bookingconfirmation;

-- equipment on etl import schema
ALTER TABLE etl.bookingconfir_equipment ADD CONSTRAINT fk_equipment_bookingconfirmation FOREIGN KEY (booking_confirmation_id) REFERENCES etl.bookingconfirmation;

-- individual on etl import schema
ALTER TABLE etl.individual ADD CONSTRAINT fk_citizenhip FOREIGN KEY (citizenship_id) REFERENCES location;