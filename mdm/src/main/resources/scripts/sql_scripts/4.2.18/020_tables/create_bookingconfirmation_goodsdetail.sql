CREATE TABLE bookingconfirmation_goodsdetail
(
    id bigserial,
    uuid varchar,
    
    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
        
    -- business fields
    line_number bigint NOT NULL, 
    number_of_packages bigint  NULL, 
    package_type_code varchar  NULL,
    package_type_description varchar NULL,
    gross_volume bigint  NULL,
    gross_weight bigint  NULL,
    gross_volume_type varchar  NULL,
    gross_weight_type varchar  NULL,
    goods_description varchar NULL,
    commodity_classification bigint  NULL,
    commodity_classification_type varchar  NULL,
    bookingconfirmation_id bigint NULL,
  
    CONSTRAINT PK_bookingconfirmation_goodsdetail PRIMARY KEY (id)
);