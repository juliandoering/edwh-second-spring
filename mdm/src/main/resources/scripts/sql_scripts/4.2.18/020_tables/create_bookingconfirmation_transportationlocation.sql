--changeset generated_nf:1497621354
CREATE TABLE bookingconfirmation_transportationlocation
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    type varchar NULL ,
    location varchar NULL ,
    date_type varchar NULL ,
    date_time timestamp NULL ,
    bookingconfirmation_transportationdetails_id bigint NULL ,

    CONSTRAINT PK_bookingconfirmation_transportationlocation PRIMARY KEY (id)
);
