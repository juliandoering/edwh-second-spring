ALTER TABLE location ADD si_unlocode VARCHAR;

-- fill with calculated data
UPDATE location SET si_unlocode = CONCAT(country_code, location_code);