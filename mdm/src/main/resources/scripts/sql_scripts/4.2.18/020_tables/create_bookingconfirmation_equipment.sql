--changeset generated_nf:1497621354 

CREATE TABLE bookingconfirmationequipment
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
        
    -- business fields
    identifier varchar NULL , 
    type_code varchar  NULL  ,
    number_of_equipment bigint  NULL , 
    cargo_movement_type varchar  NULL ,
    haulage_arrangements varchar NULL ,
    reference_information varchar NULL ,

    booking_confirmation_id bigint NULL, 
  
    CONSTRAINT PK_bookingconfir_equipment PRIMARY KEY (id)
);

