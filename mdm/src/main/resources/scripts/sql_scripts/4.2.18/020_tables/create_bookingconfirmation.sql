--changeset generated_nf:1497621354 
CREATE TABLE bookingconfirmation
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
        
    -- business fields
    sender_id varchar NOT NULL , 
    receiver_id varchar NOT NULL , 
    request_date_timestamp timestamp NOT NULL , 
    request_message_version decimal NOT NULL , 
    transaction_type char (7) NOT NULL , 
    transaction_status varchar NOT NULL ,
    transaction_split_indicator boolean NULL ,
    shipment_number varchar NULL ,
    inttra_ref_number varchar NULL ,
    bl_number varchar NULL ,
    purchase_order_number varchar NULL ,
    
    CONSTRAINT PK_bookingconfirmation PRIMARY KEY (id)
);
