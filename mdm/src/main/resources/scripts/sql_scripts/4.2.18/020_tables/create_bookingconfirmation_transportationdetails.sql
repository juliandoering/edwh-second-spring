CREATE TABLE bookingconfirmation_transportationdetails
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    transport_mode varchar NULL,
    transport_stage varchar NULL,
    transport_type varchar NULL,
    identifier_type varchar NULL,
    identifier varchar NULL,
    operator_identifier varchar NULL,
    bookingconfirmation_id bigint NULL,


    CONSTRAINT PK_bookingconfirmation_transportationdetails PRIMARY KEY (id)
);
