--changeset generated_nf:1497621354 

CREATE TABLE bookingconfirmationparty
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
        
    -- business fields
    role varchar  NOT NULL , 
    name varchar  NULL , 
    identifier char (25) NULL ,
    identifier_type char (25) NULL ,
    address varchar  NULL ,
    booking_confirmation_id bigint NULL , 
  
    CONSTRAINT PK_bookingconfirmationparty PRIMARY KEY (id)
);

