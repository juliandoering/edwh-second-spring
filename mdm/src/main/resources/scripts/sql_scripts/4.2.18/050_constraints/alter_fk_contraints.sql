ALTER TABLE bankaccount ADD CONSTRAINT fk_bankaccount_number_type FOREIGN KEY (bankaccount_number_type_id) REFERENCES bankaccount_number_type;

-- party on mdm schema
ALTER TABLE bookingconfirmationparty ADD CONSTRAINT fk_party_bookingconfirmation FOREIGN KEY (booking_confirmation_id) REFERENCES bookingconfirmation;

-- goodsdetail on mdm schema
ALTER TABLE bookingconfirmation_goodsdetail ADD CONSTRAINT fk_goodsdetail_bookingconfirmation FOREIGN KEY (bookingconfirmation_id) REFERENCES bookingconfirmation;

-- confirmation on mdm schema
ALTER TABLE bookingconfirmationequipment ADD CONSTRAINT fk_equipment_bookingconfirmation FOREIGN KEY (booking_confirmation_id) REFERENCES bookingconfirmation;

-- confirmation on mdm schema
ALTER TABLE bookingconfirmation_transportationdetails ADD CONSTRAINT fk_transportationdetails_bookingconfirmation FOREIGN KEY (bookingconfirmation_id) REFERENCES bookingconfirmation;

-- confirmation on mdm schema
ALTER TABLE bookingconfirmation_transportationlocation ADD CONSTRAINT fk_transportationlocation_bookingconfirmation FOREIGN KEY (bookingconfirmation_transportationdetails_id) REFERENCES bookingconfirmation_transportationdetails;

-- individual
ALTER TABLE individual ADD CONSTRAINT fk_citizenship FOREIGN KEY (citizenship_id) REFERENCES location;