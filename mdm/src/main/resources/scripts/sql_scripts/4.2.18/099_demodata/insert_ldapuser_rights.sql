INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id, created_by, updated_by)
VALUES (
  'f1b7176a-806e-464e-8cca-05b4f16f2829',
  (select id from userprofile where username = 'vatterodt'),
  (select id from groupcompany where tagetik_code = 'STBR-COEX' and is_version = FALSE ),
  (select id from userprofile where username = 'edwh_admin'),
  (select id from userprofile where username = 'edwh_admin')
);

INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id, created_by, updated_by)
VALUES (
  'f1b7176a-806e-464e-8cca-05b4f16f2829',
  (select id from userprofile where username = 'elvers'),
  (select id from groupcompany where tagetik_code = 'STBR-COEX' and is_version = FALSE ),
  (select id from userprofile where username = 'edwh_admin'),
  (select id from userprofile where username = 'edwh_admin')
);

INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id, created_by, updated_by)
VALUES (
  'f1b7176a-806e-464e-8cca-05b4f16f2829',
  (select id from userprofile where username = 'lips'),
  (select id from groupcompany where tagetik_code = 'STBR-COEX' and is_version = FALSE ),
  (select id from userprofile where username = 'edwh_admin'),
  (select id from userprofile where username = 'edwh_admin')
);

INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id, created_by, updated_by)
VALUES (
  'f1b7176a-806e-464e-8cca-05b4f16f2829',
  (select id from userprofile where username = 'foede'),
  (select id from groupcompany where tagetik_code = 'STBR-COEX' and is_version = FALSE ),
  (select id from userprofile where username = 'edwh_admin'),
  (select id from userprofile where username = 'edwh_admin')
);

INSERT INTO userprofile (uuid, username, email, userrole_id)
VALUES (
  'cefe6682-b365-4eb0-9934-494620a68e18',
  'edwh-admin-test-user',
  'edwh@nkg.coffee',
  (SELECT id from userrole WHERE rolename = 'edwh_admin')
);