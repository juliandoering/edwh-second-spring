INSERT INTO individual (uuid, last_name, first_name, addressing_form, academic_title, gender, citizenship_id, date_of_birth,
  personal_id, tax_id, place_of_birth_id, created_by, updated_by)
VALUES (
  '1a1ca26e-e4c4-49eb-bc21-79bfd630b1e8',
  'Janeway',
  'Kathryn',
  'Ms',
  'Captain',
  'FEMALE',
  (SELECT id from location where name = 'Vietnam' AND is_version = FALSE),
  '2344-05-20',
  '74656',
  '1701',
  (SELECT id from location where name = 'Santos' AND is_version = FALSE),
  (SELECT id from userprofile where username = 'edwh_admin'),
  (SELECT id from userprofile where username = 'edwh_admin')
);

INSERT INTO postaladdress (uuid, street, house_number, postal_code, location_id, created_by, updated_by)
VALUES (
  'd0d144f9-0466-4f8d-a767-aa29fb415b62',
  'Federation Street',
  '1',
  '9876',
  (SELECT id from location WHERE NAME = 'Santo Antônio do Amparo' AND is_version = FALSE),
  (SELECT id from userprofile where username = 'edwh-admin'),
  (SELECT id from userprofile where username = 'edwh-admin')
);

UPDATE individual SET postal_address_id = (SELECT id from postaladdress where uuid = 'd0d144f9-0466-4f8d-a767-aa29fb415b62')
  WHERE uuid = '1a1ca26e-e4c4-49eb-bc21-79bfd630b1e8';

