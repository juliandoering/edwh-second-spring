--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingconfir_goodsdetail
(
    id bigserial,
        
    -- business fields
    line_number bigint NOT NULL, 
    number_of_packages bigint  NULL, 
    package_type_code varchar NULL,
    package_type_description varchar NULL,
    gross_volume bigint  NULL,
    gross_weight bigint  NULL,
    gross_volume_type varchar  NULL,
    gross_weight_type varchar  NULL,
    goods_description varchar NULL,
    commodity_classification bigint  NULL,
    commodity_classification_type varchar  NULL,
    booking_confirmation_id bigint NOT NULL, 
  
    CONSTRAINT PK_bookingconfir_goodsdetail PRIMARY KEY (id)
);

ALTER TABLE etl.bookingconfir_goodsdetail OWNER TO etl;