--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingconfir_equipment
(
    id bigserial,
        
    -- business fields
    identifier varchar NULL , 
    type_code varchar  NULL  ,
    number_of_equipment bigint  NULL , 
    cargo_movement_type varchar  NULL  ,
    haulage_arrangements varchar NULL  ,
    reference_information varchar NULL  ,
    booking_confirmation_id bigint NOT NULL , 
  
    CONSTRAINT PK_bookingconfir_equipment PRIMARY KEY (id)
);

ALTER TABLE etl.bookingconfir_equipment OWNER TO etl;