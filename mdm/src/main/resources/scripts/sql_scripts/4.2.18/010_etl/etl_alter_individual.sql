ALTER TABLE etl.individual DROP COLUMN citizenship;
ALTER TABLE etl.individual ADD COLUMN citizenship_id BIGINT;