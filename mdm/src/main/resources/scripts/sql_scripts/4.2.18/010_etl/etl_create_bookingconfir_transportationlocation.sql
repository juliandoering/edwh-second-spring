--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingconfir_transportationlocation
(
    id bigserial,
        
    -- business fields
    type varchar NULL ,
    location varchar NULL ,
    date_type varchar NULL ,
    date_time timestamp NOT NULL ,
    bookingconfir_transportation_details_id bigint NOT NULL ,
  
    CONSTRAINT PK_bookingconfir_transportationlocation PRIMARY KEY (id)
);

ALTER TABLE etl.bookingconfir_transportationlocation OWNER TO etl;