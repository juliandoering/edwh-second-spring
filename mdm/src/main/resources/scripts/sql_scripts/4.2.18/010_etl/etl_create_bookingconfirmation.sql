--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingconfirmation
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    -- booking confirmation message filename
    filename varchar NOT NULL ,
    
    -- business fields
    sender_id varchar NOT NULL , 
    receiver_id varchar NOT NULL , 
    request_date_timestamp timestamp NOT NULL , 
    request_message_version decimal NOT NULL , 
    transaction_type char (7) NOT NULL , 
    transaction_status varchar NOT NULL ,
    transaction_split_indicator boolean NULL ,
    shipment_number varchar NULL ,
    inttra_ref_number varchar NULL ,
    bl_number varchar NULL ,
    purchase_order_number varchar NULL ,
    
    CONSTRAINT PK_bookingconfirmation PRIMARY KEY (id)
);

ALTER TABLE etl.bookingconfirmation OWNER TO etl;
