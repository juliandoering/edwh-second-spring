--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingconfir_transportationdetails
(
    id bigserial,
        
    -- business fields
    transport_mode varchar NULL,
    transport_stage varchar NULL,
    transport_type varchar NULL,
    identifier_type varchar NULL,
    identifier varchar NULL,
    operator_identifier varchar NULL,
    booking_confirmation_id bigint NOT NULL,
  
    CONSTRAINT PK_bookingconfir_transportationdetails PRIMARY KEY (id)
);

ALTER TABLE etl.bookingconfir_transportationdetails OWNER TO etl;