--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingconfir_party
(
    id bigserial,
        
    -- business fields
    role varchar  NOT NULL , 
    name varchar  NULL , 
    identifier char (25) NULL ,
    identifier_type char (25) NULL ,
    address varchar  NULL ,
    booking_confirmation_id bigint NOT NULL , 
  
    CONSTRAINT PK_bookingconfir_party PRIMARY KEY (id)
);

ALTER TABLE etl.bookingconfir_party OWNER TO etl;
