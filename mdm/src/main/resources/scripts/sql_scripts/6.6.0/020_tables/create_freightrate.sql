CREATE TABLE freightratetrade
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    name varchar,

    CONSTRAINT PK_freightratetrade PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetrade_uuid on freightratetrade(uuid);

CREATE TABLE freightratetraderoute
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    thc decimal,
    inlandcost decimal,
    inlandtransport varchar,
    transittime numeric,
    frequency numeric,
    preference boolean,
    validity_from date,
    validity_to date,
    freightratetrade_id bigint,
    portofloading_id bigint,
    portofdischarge_id bigint,
    thccurrency_id bigint,
    inlandcostcurrency_id bigint,
    provider_id bigint,
    oceanfreightcurrency_id bigint,

    CONSTRAINT PK_freightratetraderoute PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetraderoute_uuid on freightratetraderoute(uuid);

CREATE TABLE freightratetraderoute_location
(
    id bigserial,

    freightratetraderoute_id bigint,
    location_id bigint,

    CONSTRAINT PK_freightratetraderoute_location PRIMARY KEY (id)
);

CREATE TABLE freightrate
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    rate decimal,
    quantity decimal,
    freightratetraderoute_id bigint,
    packing_id bigint,
    weightunit_id bigint,
    containersize_id bigint,
    containermovement_id bigint,

    CONSTRAINT PK_freightrate PRIMARY KEY (id)
);

CREATE INDEX idx_freightrate_uuid on freightrate(uuid);


ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_freightratetrade FOREIGN KEY (freightratetrade_id) REFERENCES FreightRateTrade;
CREATE INDEX idx_freightratetraderoute_freightratetrade_id on freightratetraderoute(freightratetrade_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_portofloading FOREIGN KEY (portofloading_id) REFERENCES Location;
CREATE INDEX idx_freightratetraderoute_portofloading_id on freightratetraderoute(portofloading_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_portofdischarge FOREIGN KEY (portofdischarge_id) REFERENCES Location;
CREATE INDEX idx_freightratetraderoute_portofdischarge_id on freightratetraderoute(portofdischarge_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_thccurrency FOREIGN KEY (thccurrency_id) REFERENCES currency;
CREATE INDEX idx_freightratetraderoute_thccurrency_id on freightratetraderoute(thccurrency_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_inlandcostcurrency FOREIGN KEY (inlandcostcurrency_id) REFERENCES currency;
CREATE INDEX idx_freightratetraderoute_inlandcostcurrency_id on freightratetraderoute(inlandcostcurrency_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_provider FOREIGN KEY (provider_id) REFERENCES BusinessThirdParty;
CREATE INDEX idx_freightratetraderoute_provider_id on freightratetraderoute(provider_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_oceanfreightcurrency FOREIGN KEY (oceanfreightcurrency_id) REFERENCES currency;
CREATE INDEX idx_freightratetraderoute_oceanfreightcurrency_id on freightratetraderoute(oceanfreightcurrency_id);


ALTER TABLE freightratetraderoute_location ADD CONSTRAINT fk_freightratetraderoute_location_freightratetraderoute FOREIGN KEY (freightratetraderoute_id) REFERENCES FreightRateTradeRoute;
CREATE INDEX idx_freightratetraderoute_location_freightratetraderoute_id on freightratetraderoute_location(freightratetraderoute_id);

ALTER TABLE freightratetraderoute_location ADD CONSTRAINT fk_freightratetraderoute_location_location FOREIGN KEY (location_id) REFERENCES Location;
CREATE INDEX idx_freightratetraderoute_location_location_id on freightratetraderoute_location(location_id);


ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_freightratetraderoute FOREIGN KEY (freightratetraderoute_id) REFERENCES FreightRateTradeRoute;
CREATE INDEX idx_freightrate_freightratetraderoute_id on freightrate(freightratetraderoute_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_packing FOREIGN KEY (packing_id) REFERENCES Packing;
CREATE INDEX idx_freightrate_packing_id on freightrate(packing_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_weightunit FOREIGN KEY (weightunit_id) REFERENCES QuantityUnit;
CREATE INDEX idx_freightrate_weightunit_id on freightrate(weightunit_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_containersize FOREIGN KEY (containersize_id) REFERENCES ContainerSize;
CREATE INDEX idx_freightrate_containersize_id on freightrate(containersize_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_containermovement FOREIGN KEY (containermovement_id) REFERENCES ContainerMovement;
CREATE INDEX idx_freightrate_containermovement_id on freightrate(containermovement_id);
