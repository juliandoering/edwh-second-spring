INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '75122ad4-134e-11e9-ab14-d663bd873d93',
  -- key
  'bulk.upload.subject',
  -- template
  'Some master data has been updated.',
  -- template description
  'Some master data has been updated.');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '75122da4-134e-11e9-ab14-d663bd873d93',
  -- key
  'bulk.upload.body',
  -- template
  'The following resources have been updated:

<#list changes as change>
${change}
</#list>',
  -- template description
  'The following resources have been updated.');
