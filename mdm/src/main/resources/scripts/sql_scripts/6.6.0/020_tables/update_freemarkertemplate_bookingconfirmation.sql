DELETE
FROM mdm.freemarkertemplate
WHERE template_key = 'bookingconfirmation.created.unknown.body';;

DELETE
FROM mdm.freemarkertemplate
WHERE template_key = 'bookingconfirmation.created.unknown.subject';;

DELETE
FROM mdm.freemarkertemplate
WHERE template_key = 'bookingconfirmation.updated.unknown.body';;

DELETE
FROM mdm.freemarkertemplate
WHERE template_key = 'bookingconfirmation.updated.unknown.subject';

UPDATE mdm.freemarkertemplate
   SET template = '[Booking Confirmation] New Booking Confirmation'
WHERE template_key = 'bookingconfirmation.created.subject';

UPDATE mdm.freemarkertemplate
   SET template = '[Booking Confirmation] Update of Booking Confirmation'
WHERE template_key = 'bookingconfirmation.updated.subject';

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear customer,<br>
		a new Booking confirmation for your contract <@mark mark=changes.purchaseOrderNumber>"${entity.purchaseOrderNumber!"No Data"}"</@mark> has been uploaded to INTTRA. The INTTRA reference number is <@mark mark=changes.inttraRefNumber>${entity.inttraRefNumber!"No Data"}</@mark>.

		<p>
		Carrier Response Type: <b>${entity.transactionType!"No Data"}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity.booker! partychanges=changes.involvedParties.booker></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity.shipper! partychanges=changes.involvedParties.shipper></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity.carrier! partychanges=changes.involvedParties.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span><@mark mark=changes.inttraRefNumber>${(entity.inttraRefNumber)!"No Data"}</@mark></span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Booking Number</b></span>
					<br>
					<span><@mark mark=changes.bookingNumber>${entity.bookingNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span><@mark mark=changes.blNumber>${entity.blNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span><@mark mark=changes.purchaseOrderNumber>${entity.purchaseOrderNumber!"No Data"}</@mark></span>
				</td>
				<td>
					<@party name="Booking Office" party=entity.bookingOffice! partychanges=changes.involvedParties.bookingOffice></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity.forwarder! partychanges=changes.involvedParties.forwarder></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity.consignee! partychanges=changes.involvedParties.consignee></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity.contractParty! partychanges=changes.involvedParties.contractParty></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity.mainNotifyParty! partychanges=changes.involvedParties.mainNotifyParty></@party>
					<#if entity.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity.firstAdditionalNotifyParty! partychanges=changes.involvedParties.firstAdditionalNotifyParty></@party>
					</#if>
					<#if entity.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity.secondAdditionalNotifyParty! partychanges=changes.involvedParties.secondAdditionalNotifyParty></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity.freightPayerParty! partychanges=changes.involvedParties.freightPayerParty></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s, Voyage</b></span>
					<br>
					<span><@mark mark=changes.involvedParties.carrier.name>${(entity.carrier.name)!"No data"}</@mark>, <@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.vesselName)!"No data"}, ${(entity.mainTransportDetails.lloydsCode)!"No data"}, ${(entity.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity.mainTransportDetails changed=changes.transportationDetails.main></@plandetails>
					</td>
				</#if>
				<#if entity.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity.onCarriageTransportDetails changed=changes.transportationDetails.onCarriage></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list changes.goodsDetails.goodsDetails as cargo>
					<tr>				
						<td>
							<@mark mark=cargo.lineNumber>
								(${cargo.goodsDetail.lineNumber!"No Data"}) "Unknown Packing"
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.numberOfPackages>
								${cargo.goodsDetail.numberOfPackages!"No Data"} ${cargo.goodsDetail.packageTypeDescription!"Unknown Type"}
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.goodsDescription>
								${cargo.goodsDetail.goodsDescription!"No Data"}
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.grossWeight>
								${cargo.goodsDetail.grossWeight!"No Data"} ${cargo.goodsDetail.grossWeightType!"Unknown Type"}
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.grossVolume>
								${cargo.goodsDetail.grossVolume!"No Data"} ${cargo.goodsDetail.grossVolumeType!"Unknown Type"}
							</@mark>
						</td>
					</tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list changes.equipments.containers as equipment>
					<tr>				
						<td>
							<@mark mark=equipment.containerNumber>
								Container Number: ${equipment.equipment.equipmentNumber!"No Data"}
							</@mark>
                            <br>
							<@mark mark=equipment.typeCode>
								Type: ${equipment.equipment.typeCode!"No Data"}
							</@mark>
                            <br>
							<@mark mark=equipment.cargoMovementType>
								Service Arrangement: ${equipment.equipment.cargoMovementType!"No Data"}
							</@mark>
                            <br>
							<@mark mark=equipment.haulageArrangements>
								Haulage Arrangement: ${equipment.equipment.haulageArrangements!"No Data"}
							</@mark>
						</td>
						<td>-</td>
						<td>-</td>
					</tr>
				</#list>
			</table>
		</p>

		<#macro party name party partychanges>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><@mark mark=partychanges.identifierType><span>${party.identifierType}: ${party.identifier}</span></@mark><br></#if>
				<@mark mark=partychanges.name><span>${party.name!""}<br></span></@mark>
				<@mark mark=partychanges.address><span>${party.address!"N/A"}<br></span></@mark>
			</#if>
		</#macro>

		<#macro plandetails name details changed>
			<@mark mark=changed>
			<b>${name}</b>
				<br>
				<#if entity.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode!"No Data"}</span>
				</#if>
				<#if entity.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier!"No Data"}</span>
				</#if>
				<br>
				<#if entity.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName!"No Data"}
				</#if>
				<#-- Lloyd''s code + voyage number -->
				<br>
				<#if entity.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
			</@mark>
		</#macro>

		<#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
		<#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

	</body>
</html>'
WHERE template_key = 'bookingconfirmation.created.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
        Dear customer,<br>
        a new version of a Booking confirmation for your contract <@mark mark=changes.purchaseOrderNumber>"${entity.purchaseOrderNumber!"No Data"}"</@mark> has been uploaded to INTTRA. The INTTRA reference number is <@mark mark=changes.inttraRefNumber>${entity.inttraRefNumber!"No Data"}</@mark>.

		<p>
		Carrier Response Type: <b>${entity.transactionType!"No Data"}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity.booker! partychanges=changes.involvedParties.booker></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity.shipper! partychanges=changes.involvedParties.shipper></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity.carrier! partychanges=changes.involvedParties.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span><@mark mark=changes.inttraRefNumber>${(entity.inttraRefNumber)!"No Data"}</@mark></span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Booking Number</b></span>
					<br>
					<span><@mark mark=changes.bookingNumber>${entity.bookingNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span><@mark mark=changes.blNumber>${entity.blNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span><@mark mark=changes.purchaseOrderNumber>${entity.purchaseOrderNumber!"No Data"}</@mark></span>
				</td>
				<td>
					<@party name="Booking Office" party=entity.bookingOffice! partychanges=changes.involvedParties.bookingOffice></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity.forwarder! partychanges=changes.involvedParties.forwarder></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity.consignee! partychanges=changes.involvedParties.consignee></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity.contractParty! partychanges=changes.involvedParties.contractParty></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity.mainNotifyParty! partychanges=changes.involvedParties.mainNotifyParty></@party>
					<#if entity.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity.firstAdditionalNotifyParty! partychanges=changes.involvedParties.firstAdditionalNotifyParty></@party>
					</#if>
					<#if entity.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity.secondAdditionalNotifyParty! partychanges=changes.involvedParties.secondAdditionalNotifyParty></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity.freightPayerParty! partychanges=changes.involvedParties.freightPayerParty></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s, Voyage</b></span>
					<br>
					<span><@mark mark=changes.involvedParties.carrier.name>${(entity.carrier.name)!"No data"}</@mark>, <@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.vesselName)!"No data"}, ${(entity.mainTransportDetails.lloydsCode)!"No data"}, ${(entity.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity.mainTransportDetails changed=changes.transportationDetails.main></@plandetails>
					</td>
				</#if>
				<#if entity.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity.onCarriageTransportDetails changed=changes.transportationDetails.onCarriage></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list changes.goodsDetails.goodsDetails as cargo>
					<tr>				
						<td>
							<@mark mark=cargo.lineNumber>
								(${cargo.goodsDetail.lineNumber!"No Data"}) "Unknown Packing"
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.numberOfPackages>
								${cargo.goodsDetail.numberOfPackages!"No Data"} ${cargo.goodsDetail.packageTypeDescription!"Unknown Type"}
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.goodsDescription>
								${cargo.goodsDetail.goodsDescription!"No Data"}
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.grossWeight>
								${cargo.goodsDetail.grossWeight!"No Data"} ${cargo.goodsDetail.grossWeightType!"Unknown Type"}
							</@mark>
						</td>
						<td>
							<@mark mark=cargo.grossVolume>
								${cargo.goodsDetail.grossVolume!"No Data"} ${cargo.goodsDetail.grossVolumeType!"Unknown Type"}
							</@mark>
						</td>
					</tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list changes.equipments.containers as equipment>
					<tr>				
						<td>
							<@mark mark=equipment.containerNumber>
								Container Number: ${equipment.equipment.equipmentNumber!"No Data"}
							</@mark>
                            <br>
							<@mark mark=equipment.typeCode>
								Type: ${equipment.equipment.typeCode!"No Data"}
							</@mark>
                            <br>
							<@mark mark=equipment.cargoMovementType>
								Service Arrangement: ${equipment.equipment.cargoMovementType!"No Data"}
							</@mark>
                            <br>
							<@mark mark=equipment.haulageArrangements>
								Haulage Arrangement: ${equipment.equipment.haulageArrangements!"No Data"}
							</@mark>
						</td>
						<td>-</td>
						<td>-</td>
					</tr>
				</#list>
			</table>
		</p>

		<#macro party name party partychanges>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><@mark mark=partychanges.identifierType><span>${party.identifierType}: ${party.identifier}</span></@mark><br></#if>
				<@mark mark=partychanges.name><span>${party.name!""}<br></span></@mark>
				<@mark mark=partychanges.address><span>${party.address!"N/A"}<br></span></@mark>
			</#if>
		</#macro>

		<#macro plandetails name details changed>
			<@mark mark=changed>
			<b>${name}</b>
				<br>
				<#if entity.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode!"No Data"}</span>
				</#if>
				<#if entity.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier!"No Data"}</span>
				</#if>
				<br>
				<#if entity.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName!"No Data"}
				</#if>
				<#-- Lloyd''s code + voyage number -->
				<br>
				<#if entity.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
			</@mark>
		</#macro>

		<#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
		<#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

	</body>
</html>'
WHERE template_key = 'bookingconfirmation.updated.body';

