-- Userprofile
ALTER TABLE userprofile ADD COLUMN is_version boolean DEFAULT false NOT NULL;
ALTER TABLE userprofile ADD COLUMN parent     bigint  DEFAULT 0 NOT NULL;
ALTER TABLE userprofile ADD COLUMN version    bigint  DEFAULT 0 NOT NULL;

-- Userprofile GroupCompany
ALTER TABLE userprofile_groupcompany ADD COLUMN is_version boolean DEFAULT false NOT NULL;
ALTER TABLE userprofile_groupcompany ADD COLUMN parent     bigint  DEFAULT 0 NOT NULL;
ALTER TABLE userprofile_groupcompany ADD COLUMN version    bigint  DEFAULT 0 NOT NULL;

-- Userrole
ALTER TABLE userrole ADD COLUMN is_version boolean DEFAULT false NOT NULL;
ALTER TABLE userrole ADD COLUMN parent     bigint  DEFAULT 0 NOT NULL;
ALTER TABLE userrole ADD COLUMN version    bigint  DEFAULT 0 NOT NULL;