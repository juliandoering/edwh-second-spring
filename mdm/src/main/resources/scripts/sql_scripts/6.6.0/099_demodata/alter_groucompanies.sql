-- update empty acronym fields for the new selection field in the group company selection in the trade tables
UPDATE mdm.groupcompany
   SET acronym = 'FZBR'
WHERE tagetik_code = 'FZBR-FARM';

UPDATE mdm.groupcompany
   SET acronym = 'STBR'
WHERE tagetik_code = 'STBR-COEX';

UPDATE mdm.groupcompany
   SET acronym = 'NGVN'
WHERE tagetik_code = 'NGVN';

UPDATE mdm.groupcompany
   SET acronym = 'BRIC'
WHERE tagetik_code = 'BRIC';
