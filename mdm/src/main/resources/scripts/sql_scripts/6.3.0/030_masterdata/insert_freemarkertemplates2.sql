/*
 * Script to insert freemarker templates that are used to build e.g. email notification text
 * */

/* <DEFAULT> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '9987480e-e829-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.downloaded.body',
  -- template
  'A master data has been marked as downloaded
 
  ${(entity.businessModel)!""}: ${(entity.businessKey)!""}
  ${UriSelf}',
  -- template description
  'default downloaded email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'a1f3e466-e829-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.downloaded.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] marked as downloaded',
  -- template description
  'default downloaded email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'de2854a8-e829-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.warning.body',
  -- template
  'A master data upload created a warning:
  ${message}
 
  ${(entity.businessModel)!""}: ${(entity.businessKey)!""}
  ${UriSelf}',
  -- template description
  'default warning email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'e4c8e7aa-e829-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.warning.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] warning',
  -- template description
  'default warning email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '30874910-e82b-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.bulkconflict.body',
  -- template
  'The upload of old master data has been rejected, due to differences to the current version inside of EDWH: 
 
  ${(entity.businessModel)!""}: ${(entity.businessKey)!""}
  ${UriSelf}',
  -- template description
  'default bulkconflict email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '3934eab8-e82b-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.bulkconflict.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] upload conflict',
  -- template description
  'default bulkconflict email subject');

/* </DEFAULT> */
