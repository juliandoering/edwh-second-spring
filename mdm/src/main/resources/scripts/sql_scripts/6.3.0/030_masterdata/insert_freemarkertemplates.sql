/*
 * Script to insert freemarker templates that are used to build e.g. email notification text
 * */

/* <DEFAULT> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1a34f59a-de03-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.updated.body',
  -- template
  'A master data has been updated
 
  ${(entity.businessModel)!""}: ${(entity.businessKey)!""}
  ${UriSelf}',
  -- template description
  'default updated email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '337f0936-de04-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.created.body',
  -- template
  'A master data has been created

  ${(entity.businessModel)!""}: ${(entity.businessKey)!""}
  ${UriSelf}',
  -- template description
  'default created email body');
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '730588dc-de04-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.updated.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] updated',
  -- template description
  'default updated email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'aef921d2-de04-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.created.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] created',
  -- template description
  'default created email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'd1aa3ae0-de04-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.deleted.body',
  -- template
  'A master data was deleted

  ${(entity.businessModel)!""}: ${(entity.businessKey)!""}
  ${UriSelf}',
  -- template description
  'delete individual email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7db82-b74b-11e8-96f8-529269fb1459',
  -- key
  'default.deleted.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] deleted',
  -- template description
  'default deleted email subject');
  
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '5f91415c-dea3-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.replaced.body',
  -- template
'A ${(entity.businessModel)!""} has been replaced

  old UUID: ${(entity.uuid)!"UUID is missing"}
  old link: ${(UriSelf)!"Link is missing"}

  new UUID: ${(newEntity.uuid)!"UUID is missing"}
  new link: ${(newUriSelf)!"Link is missing"}',
  -- template description
  'default replaced email body');
  
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '8b142a50-dea4-11e8-9f32-f2801f1b9fd1',
  -- key
  'default.replaced.subject',
  -- template
  '[${(entity.businessModel)!"Master data"}] replaced',
  -- template description
  'default replaced email subject');

/* </DEFAULT> */
