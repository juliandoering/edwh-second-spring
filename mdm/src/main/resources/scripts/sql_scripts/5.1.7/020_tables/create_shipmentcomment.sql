CREATE TABLE shipmentcomment
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    type varchar,
    shipmentcomment varchar,
    bookingstatus_id bigint,

    CONSTRAINT PK_shipmentcomment PRIMARY KEY (id)
);

CREATE INDEX idx_shipmentcomment_uuid on shipmentcomment(uuid);
