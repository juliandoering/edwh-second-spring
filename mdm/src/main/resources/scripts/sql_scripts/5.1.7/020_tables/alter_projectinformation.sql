ALTER TABLE projectinformation ADD COLUMN sustainabilityproject_id BIGINT;

ALTER TABLE projectinformation RENAME TO sustainabilityprojectinformation;