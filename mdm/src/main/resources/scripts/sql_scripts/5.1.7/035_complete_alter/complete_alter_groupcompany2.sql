UPDATE groupcompany gc SET businessfunction_id = (select max(bf.id) from businessfunction bf where gc.is_version = bf.is_version and upper(gc.business_function) = bf.code);

ALTER TABLE groupcompany DROP COLUMN business_function;