INSERT INTO bookingstatuslocation (uuid, location_id, type, datetime, datetimetype, created_by, updated_by)
VALUES (
  '9dfda7c3-0a01-4f95-8890-272690b4ea22',
  (SELECT id from location where si_unlocode = 'DEHAM' AND is_version = FALSE ),
  'ActivityLocation',
  '2018-05-28 10:08:00',
  'StatusChange',
  (SELECT id FROM userprofile where username = 'edwh_admin'),
  (SELECT id FROM userprofile where username = 'edwh_admin')
);

INSERT INTO bookingstatus (uuid, documentidentifier, datetime, eventcode, bookingnumber, billofladingnumber, eventlocation_id,created_by, updated_by)
VALUES (
  '2469889d-05bb-442e-a2f5-5d135155ba54',
  '1091798897',
  '2018-05-29 15:05:00',
  'I',
  '964987141',
  '964987141',
  (SELECT id FROM bookingstatuslocation WHERE uuid = '9dfda7c3-0a01-4f95-8890-272690b4ea22'),
  (SELECT id FROM userprofile where username = 'edwh_admin'),
  (SELECT id FROM userprofile where username = 'edwh_admin')
);

INSERT INTO bookingstatustransportdetails (uuid, conveyancename, voyagetripNumber, carrierscac, transportidentification, transportidentificationtype, bookingstatus_id, created_by, updated_by)
VALUES (
  '13a165a5-d09f-463b-bb20-5dd575b67297',
  'EVER DAINTY',
  '04I0AE1MA',
  'CMDU',
  'EDATN',
  'LloydsCode',
  (SELECT id FROM bookingstatus WHERE uuid = '2469889d-05bb-442e-a2f5-5d135155ba54'),
  (SELECT id FROM userprofile where username = 'edwh_admin'),
  (SELECT id FROM userprofile where username = 'edwh_admin')
);

INSERT INTO bookingstatuslocation (uuid, location_id, type, datetime, datetimetype, bookingstatustransportdetails_id, created_by, updated_by)
VALUES (
  '281f0cae-1eb9-4845-9cd4-9a105ff394c5',
  (SELECT id from location where si_unlocode = 'BRSSZ' AND is_version = FALSE ),
  'PortOfLoading',
  '2018-05-31 16:00:00',
  'DepartureEstimated',
  (SELECT id FROM bookingstatustransportdetails WHERE uuid = '13a165a5-d09f-463b-bb20-5dd575b67297'),
  (SELECT id FROM userprofile where username = 'edwh_admin'),
  (SELECT id FROM userprofile where username = 'edwh_admin')
);

INSERT INTO equipmentdetails (uuid, linenumber, containernumber, loadtype, containertype, bookingstatus_id, created_by, updated_by)
VALUES (
  '50323b78-47c8-4c1f-8a10-fe16245a2539',
  '1',
  'CMAU0267455',
  'Full',
  '22G0',
  (SELECT id FROM bookingstatus WHERE uuid = '2469889d-05bb-442e-a2f5-5d135155ba54'),
  (SELECT id FROM userprofile where username = 'edwh_admin'),
  (SELECT id FROM userprofile where username = 'edwh_admin')
);

INSERT INTO shipmentcomment (uuid, type, shipmentcomment, bookingstatus_id, created_by, updated_by)
VALUES (
  '1cff7cef-65e0-494c-a907-e6de2e305a4f',
  'General',
  'BILL OF LADING. MSG=EVENT GENERATED',
  (SELECT id FROM bookingstatus WHERE uuid = '2469889d-05bb-442e-a2f5-5d135155ba54'),
  (SELECT id FROM userprofile where username = 'edwh_admin'),
  (SELECT id FROM userprofile where username = 'edwh_admin')
);

INSERT INTO bookingstatuslocation (uuid, created_by, updated_by, is_version, parent, location_id, type,  datetime, datetimetype)
SELECT uuid, created_by, updated_by, TRUE, id,
(SELECT location.id from location where location.parent = bookingstatuslocation.location_id and location.is_version = TRUE),
 type, datetime, datetimetype FROM bookingstatuslocation WHERE is_version = FALSE ;

INSERT INTO bookingstatus (uuid, created_by, updated_by, is_version, parent, documentidentifier, datetime, eventcode, bookingnumber, billofladingnumber, eventlocation_id)
SELECT uuid, created_by, updated_by, TRUE, id, documentidentifier, datetime, eventcode, bookingnumber, billofladingnumber,
(SELECT bookingstatuslocation.id FROM bookingstatuslocation WHERE bookingstatuslocation.parent = bookingstatus.eventlocation_id AND bookingstatuslocation.is_version = TRUE)
 FROM bookingstatus WHERE is_version = FALSE ;

INSERT INTO bookingstatustransportdetails (uuid, created_by, updated_by, is_version, parent, conveyancename, voyagetripNumber,  carrierscac, transportidentification, transportidentificationtype, bookingstatus_id)
SELECT uuid, created_by, updated_by, TRUE, id, conveyancename, voyagetripNumber, carrierscac, transportidentification, transportidentificationtype,
 (SELECT bookingstatus.id from bookingstatus where bookingstatus.parent = bookingstatustransportdetails.bookingstatus_id and bookingstatus.is_version = TRUE)
FROM bookingstatustransportdetails WHERE is_version = FALSE ;

-- UPDATE bookingstatuslocation SET bookingstatustransportdetails_id =
--   (SELECT bookingstatustransportdetails.id from bookingstatustransportdetails WHERE (SELECT bookingstatuslocation.bookingstatustransportdetails_id from bookingstatuslocation)  AND bookingstatustransportdetails.is_version = TRUE)
--   WHERE bookingstatuslocation.is_version = TRUE;
UPDATE bookingstatuslocation SET bookingstatustransportdetails_id = 2 WHERE bookingstatuslocation.id = 4;

INSERT INTO equipmentdetails (uuid, created_by, updated_by, is_version, parent, linenumber, containernumber,  loadtype, containertype, bookingstatus_id)
SELECT uuid, created_by, updated_by, TRUE, id, linenumber, containernumber, loadtype, containertype,
 (SELECT bookingstatus.id from bookingstatus where bookingstatus.parent = equipmentdetails.bookingstatus_id and bookingstatus.is_version = TRUE)
FROM equipmentdetails WHERE is_version = FALSE ;

INSERT INTO shipmentcomment (uuid, created_by, updated_by, is_version, parent, shipmentcomment, bookingstatus_id)
SELECT uuid, created_by, updated_by, TRUE, id, shipmentcomment,
 (SELECT bookingstatus.id from bookingstatus where bookingstatus.parent = shipmentcomment.bookingstatus_id and bookingstatus.is_version = TRUE)
FROM shipmentcomment WHERE is_version = FALSE ;
