INSERT INTO sustainabilityproject (uuid, code, description, created_by, updated_by)
VALUES ('1c57adc0-ecb9-4e9c-b907-59c1f7afbce6', 'COFFEEKIDS', 'Coffee Kids',
  (SELECT id from userprofile WHERE username = 'edwh_admin'),
  (SELECT id from userprofile WHERE username = 'edwh_admin')
);

INSERT INTO sustainabilityproject (uuid, parent, version, is_version, code, description, created_by, updated_by)
SELECT uuid, id, version, TRUE, code, description, created_by, updated_by FROM sustainabilityproject WHERE is_version = FALSE AND code = 'COFFEEKIDS';