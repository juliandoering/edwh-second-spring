INSERT INTO sustainabilityproject (uuid, code, description, created_by, updated_by)
VALUES (
  '7631a380-0a4b-4447-9a0a-e72fe6e0034e',
  'NKGBLOOM',
  'NKG Bloom Uganda',
  (SELECT id from userprofile WHERE username = 'edwh_admin'),
  (SELECT id from userprofile WHERE username = 'edwh_admin')
);

INSERT INTO sustainabilityproject (uuid, code, description, created_by, updated_by)
VALUES (
  '0b6c6c46-6ce0-4a05-890c-56d8efc0dc4a',
  'ICPALTAVERA',
  'ICP Project Alta Verapaz',
  (SELECT id from userprofile WHERE username = 'edwh_admin'),
  (SELECT id from userprofile WHERE username = 'edwh_admin')
);

INSERT INTO sustainabilityproject (uuid, code, description, created_by, updated_by)
VALUES (
  '7c53f4e1-a71a-41d8-900f-38ee62c0c46d',
  'PORMAS',
  'Por Mas Café',
  (SELECT id from userprofile WHERE username = 'edwh_admin'),
  (SELECT id from userprofile WHERE username = 'edwh_admin')
);

INSERT INTO sustainabilityproject (uuid, version, is_version, parent, created_by, updated_by, code, description)
SELECT uuid, version, TRUE, id, created_by, updated_by, code, description
FROM sustainabilityproject where is_version = false;