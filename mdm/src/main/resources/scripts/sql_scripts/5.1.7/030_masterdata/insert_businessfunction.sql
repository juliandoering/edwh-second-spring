INSERT INTO businessfunction (uuid, code, description, created_by, updated_by)
    VALUES ('dccb53ea-698e-11e8-adc0-fa7ae01bbebc', 'EXPORTER', 'Exporter',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO businessfunction (uuid, code, description, created_by, updated_by)
    VALUES ('dccb5688-698e-11e8-adc0-fa7ae01bbebc', 'IMPORTER', 'Importer',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO businessfunction (uuid, code, description, created_by, updated_by)
    VALUES ('dccb59e4-698e-11e8-adc0-fa7ae01bbebc', 'SERVICE', 'Service',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO businessfunction (uuid, code, description, created_by, updated_by)
    VALUES ('dccb5b38-698e-11e8-adc0-fa7ae01bbebc', 'HOLDING', 'Holding',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO businessfunction (uuid, code, description, created_by, updated_by)
    VALUES ('dccb5c5a-698e-11e8-adc0-fa7ae01bbebc', 'OTHER', 'Other',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
 
 
insert into businessfunction (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description from businessfunction where is_version is false;