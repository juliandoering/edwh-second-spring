UPDATE freemarkertemplate SET template =
'Dear ${entity1.contactPersonName},
a new Booking confirmation for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA reference number is ${entity0.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.created.body';

UPDATE freemarkertemplate SET template =
'Dear ${entity1.contactPersonName},
a new version of a Booking confirmation for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA reference number is ${entity0.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.updated.body';

UPDATE freemarkertemplate SET template =
'[Booking Confirmation] Update of Booking Confirmation for ${entity1.groupCompany.legalName}'
WHERE template_key = 'bookingconfirmation.updated.subject';

UPDATE freemarkertemplate SET template =
'Dear ${entity1.contactPersonName},
an update for an existing Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. This Booking Confirmation is missing a Purchase Order Number and can therefore not be automatically assigned to a Group Company. The INTTRA reference number is ${entity0.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.updated.unknown.body';

UPDATE freemarkertemplate SET template =
'Dear ${entity1.contactPersonName},
a new version of an existing Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. This Booking Confirmation is missing a Purchase Order Number and can therefore not be automatically assigned to a Group Company. The INTTRA reference number is ${entity0.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.created.unknown.body';