ALTER TABLE etl.businessthirdpartyindividual
  ADD COLUMN identification_counter integer default 1;
ALTER TABLE etl.businessthirdpartyindividual
  ADD COLUMN first_name varchar;
ALTER TABLE etl.businessthirdpartyindividual
  ADD COLUMN last_name varchar;