CREATE TABLE limittype
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    code varchar,
    description varchar,

    CONSTRAINT PK_limittype PRIMARY KEY (id)
);

CREATE INDEX idx_limittype_uuid on limittype(uuid);


CREATE TABLE groupcompanybusinesslimit
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    limitlowend decimal,
    limithighend decimal,
    validfrom date,
    validto date,
    limittype_id bigint,
    groupcompany_id bigint,
    limitunitquantity_id bigint,
    limitunitcurrency_id bigint,

    CONSTRAINT PK_groupcompanybusinesslimit PRIMARY KEY (id)
);

CREATE INDEX idx_groupcompanybusinesslimit_uuid on groupcompanybusinesslimit(uuid);


ALTER TABLE groupcompanybusinesslimit ADD CONSTRAINT fk_groupcompanybusinesslimit_limittype FOREIGN KEY (limittype_id) REFERENCES LimitType;
CREATE INDEX idx_groupcompanybusinesslimit_limittype_id on groupcompanybusinesslimit(limittype_id);

ALTER TABLE groupcompanybusinesslimit ADD CONSTRAINT fk_groupcompanybusinesslimit_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES GroupCompany;
CREATE INDEX idx_groupcompanybusinesslimit_groupcompany_id on groupcompanybusinesslimit(groupcompany_id);

ALTER TABLE groupcompanybusinesslimit ADD CONSTRAINT fk_groupcompanybusinesslimit_limitunitquantity FOREIGN KEY (limitunitquantity_id) REFERENCES QuantityUnit;
CREATE INDEX idx_groupcompanybusinesslimit_limitunitquantity_id on groupcompanybusinesslimit(limitunitquantity_id);

ALTER TABLE groupcompanybusinesslimit ADD CONSTRAINT fk_groupcompanybusinesslimit_limitunitcurrency FOREIGN KEY (limitunitcurrency_id) REFERENCES Currency;
CREATE INDEX idx_groupcompanybusinesslimit_limitunitcurrency_id on groupcompanybusinesslimit(limitunitcurrency_id);
