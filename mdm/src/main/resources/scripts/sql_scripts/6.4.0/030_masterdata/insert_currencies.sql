-- Insert all currencies for exchange rates
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('c5ee6d18-a446-4af3-837c-28e84d5e16e1', 'GBP', 'British Pound Sterling', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('5a98159a-f908-4050-8fce-fee0d7775ca3', 'RUB', 'Russian Ruble', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('4b33432a-ec69-424b-8fae-fd3885e73776', 'PLN', 'Polish Zloty', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('2fba4487-8be9-4211-86c3-fa3f0aa9d8fd', 'CHF', 'Swiss Franc', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('cd6cb3e9-ea08-4991-9e67-dee82245487f', 'VND', 'Vietnamese Dong', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('55fff893-35f0-4374-8637-bcdcbbe8e69d', 'SGD', 'Singapore Dollar', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('7d1e02c5-fbd7-4874-a4d8-884ec37fad33', 'PGK', 'Papua New Guinean Kina', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('2f679ac4-3da9-48d1-95ea-f2cb975210eb', 'JPY', 'Japanese Yen', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('97a9f19c-33b2-4852-9099-2c5cfdbd65b5', 'IDR', 'Indonesian Rupiah', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('025758ab-f77e-443b-8f51-e98e7abd88c5', 'INR', 'Indian Rupee', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('1474dca8-6287-4bf1-b1da-edca421cfaf0', 'PEN', 'Peruvian Nuevo Sol', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('767e7fd0-f996-49cd-9f20-081142eed662', 'MXN', 'Mexican Peso', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('e8aa6c74-519c-4c04-8959-5d7eba8604b3', 'HNL', 'Honduran Lempira', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('ef052c1b-cc53-4bf3-97b9-bb7314ba40ef', 'GTQ', 'Guatemalan Quetzal', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('478439af-c9ab-4669-ad13-fdf84c7c9e6a', 'CRC', 'Costa Rican Colón', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('6813dc4b-c9e7-4571-9183-15da58846a06', 'COP', 'Colombian Peso', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('55a96175-065a-438f-baf7-5d307303b0c2', 'BRL', 'Brazilian Real', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('f51c7b3a-6436-4c79-bcc5-775cdd00b8dd', 'TZS', 'Tanzanian Shilling', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('e6429e2f-c19e-49e4-95fc-a51c51f12656', 'UGX', 'Ugandan Shilling', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO currency (uuid, code, description, created_by, updated_by)
VALUES ('bf41657e-3060-41b4-aa3e-9019188b7b03', 'KES', 'Kenyan Shilling', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));

-- insert immutable versions   
INSERT INTO currency (uuid, version, is_version, parent, code, description, created_by, updated_by) 
  SELECT uuid, version, true, id, code, description, created_by, updated_by from currency WHERE is_version = false AND code NOT IN ('USD', 'EUR');
