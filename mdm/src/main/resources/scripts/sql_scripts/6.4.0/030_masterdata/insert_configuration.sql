-- Set configuration for base currencies EUR, USD, GBP
INSERT INTO configuration (uuid, code, config_value, description, created_by, updated_by)
VALUES ('ef4f6196-59c5-42bc-9607-438335b43592', 'oer.base.curreny', 'EUR,USD,GBP', 'Used base currencies for the Open Exchange Rates API', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO configuration (uuid, code, config_value, description, created_by, updated_by)
VALUES ('2b6184a0-e8da-11e8-9f32-f2801f1b9fd1', 'oer.curreny.pair.EUR', 'CHF,USD', 'This property key is concatinated by oer.curreny.pair. and one of the base currencies listed in oer.base.curreny. The values represent the currency symbols for the Open Exchange Rates API.', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO configuration (uuid, code, config_value, description, created_by, updated_by)
VALUES ('31a7ace0-e8da-11e8-9f32-f2801f1b9fd1', 'oer.curreny.pair.USD', 'KES,UGX,TZS,BRL,COP,CRC,GTQ,HNL,MXN,PEN,INR,IDR,JPY,PGK,SGD,VND,PLN,RUB,CHF', 'This property key is concatinated by oer.curreny.pair. and one of the base currencies listed in oer.base.curreny. The values represent the currency symbols for the Open Exchange Rates API.', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO configuration (uuid, code, config_value, description, created_by, updated_by)
VALUES ('31a7ae34-e8da-11e8-9f32-f2801f1b9fd1', 'oer.curreny.pair.GBP', 'EUR,USD', 'This property key is concatinated by oer.curreny.pair. and one of the base currencies listed in oer.base.curreny. The values represent the currency symbols for the Open Exchange Rates API.', (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE), (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));

-- insert immutable versions
INSERT INTO configuration (uuid, version, is_version, parent, code, config_value, description, created_by, updated_by)  
  SELECT uuid, version, true, id, code, config_value, description, created_by, updated_by FROM configuration WHERE is_version = false;