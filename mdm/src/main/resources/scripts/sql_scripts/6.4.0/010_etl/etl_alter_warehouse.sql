/*
 * Script amend etl.warehouse
 */

ALTER TABLE etl.warehouse
    ADD iis_code numeric;

ALTER TABLE etl.warehouse
    ADD comment VARCHAR;

ALTER TABLE etl.warehouse
    ADD limit_value decimal;

ALTER TABLE etl.warehouse
    ADD limit_validity_from date;

ALTER TABLE etl.warehouse
    ADD limit_currency_id bigint;

ALTER TABLE etl.warehouse
    DROP owner_id;

ALTER TABLE etl.warehouse
    ADD owner_groupcompany_id bigint;

ALTER TABLE etl.warehouse
    ADD owner_businessthirdparty_id bigint;
    
    
ALTER TABLE etl.warehousecontact
    ADD job_id VARCHAR;

    
CREATE TABLE etl.warehousepostaladdress (
    id bigserial,
    warehouse_id bigint,
    job_id varchar,
    line1 varchar,
    line2 varchar,
    postal_code varchar,
    location_id bigint,
    postaladdress_type_id bigint,

    CONSTRAINT PK_warehousepostaladdress PRIMARY KEY (id)
);

ALTER TABLE etl.warehousepostaladdress OWNER TO etl;


DROP TABLE etl.warehousepersonincharge;

CREATE TABLE etl.warehousecontactperson
(
    id bigserial,
    warehouse_id bigint,
    job_id varchar,

    -- business fields
    role varchar,
    individual_id bigint,

    CONSTRAINT PK_warehousecontactperson PRIMARY KEY (id)
);

ALTER TABLE etl.warehousecontactperson OWNER TO etl;
