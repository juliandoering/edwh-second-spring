/*
 * Script to amend Warehouse
 */

/* amend table warehouse */
ALTER TABLE warehouse
    ADD iis_code numeric;

ALTER TABLE warehouse
    ADD comment VARCHAR;

ALTER TABLE warehouse
    ADD limit_value decimal;

ALTER TABLE warehouse
    ADD limit_validity_from date;

ALTER TABLE warehouse
    ADD limit_currency_id bigint;

-- replace owner_id with owner_groupcompany_id
ALTER TABLE warehouse
    ADD owner_groupcompany_id bigint;    
UPDATE warehouse set owner_groupcompany_id = owner_id;
ALTER TABLE warehouse
    DROP owner_id;

ALTER TABLE warehouse
    ADD owner_businessthirdparty_id bigint;

-- replace location_id with postaladdress
ALTER TABLE warehouse
    ADD postaladdress_id bigint;
-- create for each warehouse a postal address in case mark it as deleted, this is needed for older versions of the warehouse with a postal address
-- use addressline1 to determine later the postal address created for a certain warehouse
INSERT INTO postaladdress (uuid, created_by, updated_by, deleted, version, location_id, addressline1) 
    SELECT uuid, created_by, updated_by, location_id IS NULL, version, location_id, 'warehouse update'||id 
    FROM warehouse 
    WHERE is_version is false;
INSERT INTO postaladdress (uuid, created_by, updated_by, deleted, version, location_id, addressline1, parent) 
    SELECT uuid, created_by, updated_by, deleted, version, location_id, 'warehouse update'||id,
        (SELECT min(a2.id) FROM postaladdress a2 WHERE a2.addressline1 = 'warehouse update'||warehouse.parent)
    FROM warehouse 
    WHERE is_version is true AND location_id IS NOT NULL;
UPDATE warehouse SET postaladdress_id =
    (SELECT postaladdress.id FROM postaladdress WHERE postaladdress.addressline1 = 'warehouse update'||warehouse.id)
    WHERE location_id IS NOT NULL OR is_version is false;
UPDATE postaladdress SET addressline1 = null WHERE addressline1 like 'warehouse update%';
ALTER TABLE warehouse
    DROP location_id;

/* add warehousecontactperson to warehouses */

-- replace many-to-many table warehouse_individual with warehousecontactperson
CREATE TABLE warehousecontactperson
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    role varchar,
    warehouse_id bigint,
    individual_id bigint,

    CONSTRAINT PK_warehousecontactperson PRIMARY KEY (id)
);
INSERT INTO warehousecontactperson (uuid, created_at, created_by, updated_at, updated_by, deleted, version, is_version, warehouse_id, individual_id) 
    SELECT warehouse.uuid, warehouse.created_at, warehouse.created_by, warehouse.updated_at, warehouse.updated_by, warehouse.deleted, warehouse.version, warehouse.is_version, warehouse.id, individual.id 
    FROM warehouse_individual
    JOIN warehouse ON warehouse.id = warehouse_individual.warehouse_id
    JOIN individual ON individual.id = warehouse_individual.individual_id
    WHERE warehouse.is_version is false;
INSERT INTO warehousecontactperson (uuid, created_at, created_by, updated_at, updated_by, deleted, version, is_version, warehouse_id, individual_id, parent) 
    SELECT warehouse.uuid, warehouse.created_at, warehouse.created_by, warehouse.updated_at, warehouse.updated_by, warehouse.deleted, warehouse.version, warehouse.is_version, warehouse.id, individual.id,
        (SELECT min(wi2.id) FROM warehousecontactperson wi2 WHERE wi2.warehouse_id = warehouse.parent AND wi2.individual_id = individual.parent)
    FROM warehouse_individual
    JOIN warehouse ON warehouse.id = warehouse_individual.warehouse_id
    JOIN individual ON individual.id = warehouse_individual.individual_id
    WHERE warehouse.is_version is true;
-- add missing versions
INSERT INTO warehousecontactperson (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, warehouse_id, individual_id) 
  SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, 
      (select max(id) from warehouse where warehouse.parent = warehousecontactperson.warehouse_id and warehouse.is_version is true),
      (select max(id) from individual where individual.parent = warehousecontactperson.individual_id and individual.is_version is true)
  FROM warehousecontactperson WHERE is_version is false AND not exists (select 1 from warehousecontactperson wi2 where wi2.parent = warehousecontactperson.id);
DROP TABLE warehouse_individual;

CREATE INDEX idx_warehousecontactperson_uuid on warehousecontactperson(uuid);

/* CREATE FK-constraints and INDEX for the FK-constraint columns */

ALTER TABLE warehouse ADD CONSTRAINT fk_warehouse_currency FOREIGN KEY (limit_currency_id) REFERENCES currency;
CREATE INDEX idx_warehouse_limit_currency_id on warehouse(limit_currency_id);

ALTER TABLE warehouse ADD CONSTRAINT fk_warehouse_groupcompany FOREIGN KEY (owner_groupcompany_id) REFERENCES groupcompany;
CREATE INDEX idx_warehouse_owner_groupcompany_id on warehouse(owner_groupcompany_id);

ALTER TABLE warehouse ADD CONSTRAINT fk_warehouse_businessthirdparty FOREIGN KEY (owner_businessthirdparty_id) REFERENCES BusinessThirdParty;
CREATE INDEX idx_warehouse_owner_businessthirdparty_id on warehouse(owner_businessthirdparty_id);

ALTER TABLE warehouse ADD CONSTRAINT fk_warehouse_postaladdress FOREIGN KEY (postaladdress_id) REFERENCES postaladdress;
CREATE INDEX idx_warehouse_postaladdress_id on warehouse(postaladdress_id);


ALTER TABLE warehousecontactperson ADD CONSTRAINT fk_warehousecontactperson_warehouse FOREIGN KEY (warehouse_id) REFERENCES warehouse;
CREATE INDEX idx_warehousecontactperson_warehouse_id on warehousecontactperson(warehouse_id);

ALTER TABLE warehousecontactperson ADD CONSTRAINT fk_warehousecontactperson_individual FOREIGN KEY (individual_id) REFERENCES individual;
CREATE INDEX idx_warehousecontactperson_individual_id on warehousecontactperson(individual_id);
