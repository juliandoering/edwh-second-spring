UPDATE mdm.contractrole
SET code = 'SELLER'
WHERE code = 'S';

UPDATE mdm.contractrole
SET code = 'SHIPPER'
WHERE code = 'SH';

UPDATE mdm.contractrole
SET code = 'BUYER'
WHERE code = 'B';

UPDATE mdm.contractrole
SET code = 'AGENT'
WHERE code = 'A';

