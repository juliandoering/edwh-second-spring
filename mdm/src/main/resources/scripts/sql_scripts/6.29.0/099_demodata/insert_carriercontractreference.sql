-- new entries for Carrier Contract Reference
INSERT INTO carriercontractreference (uuid, infotextglobal, infotextusa, carrier_id)
VALUES (
  '7591f5ea-202a-11ea-978f-2e728ce88125'
, '134983 (for Asia, India, Africa to Europe and Intra Asia) ...'
, 'S17EGE034'
, (SELECT id FROM BusinessThirdPartyGroup WHERE is_version is false AND uuid = 'ff98df88-7247-11e9-a923-1681be663d3f') -- HAPAG
);
INSERT INTO carriercontractreference (uuid, infotextglobal, infotextusa, carrier_id)
VALUES (
  'f4df69d6-202a-11ea-978f-2e728ce88125'
, '7722217'
, '294900256'
, (SELECT id FROM BusinessThirdPartyGroup WHERE is_version is false AND uuid = 'ff98df88-7247-11e9-a923-1681be663d3e') -- Maersk Line
);

-- immutable versions for Carrier Contract Reference

INSERT INTO carriercontractreference (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, infotextglobal, infotextusa, carrier_id) 
  SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, infotextglobal, infotextusa
, (SELECT max(BusinessThirdPartyGroup.id) FROM BusinessThirdPartyGroup WHERE BusinessThirdPartyGroup.is_version is true AND BusinessThirdPartyGroup.parent = carriercontractreference.carrier_id) 
  FROM carriercontractreference WHERE is_version is false;


