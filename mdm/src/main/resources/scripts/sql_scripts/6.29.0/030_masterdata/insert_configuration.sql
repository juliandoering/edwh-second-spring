-- Set configuration for base currencies EUR, USD, GBP
INSERT INTO configuration (uuid, code, config_value, description, created_by, updated_by)
VALUES ('f6c92306-28b1-11ea-978f-2e728ce88125', 'env.host.url', 'https://host.edwh.ng.local', 'The protocol and host name of EDWH to create correct links in emails and HTTP responses, e.g. https://sbox.edwh.nkg.net or https://edwh.nkg.net.', 'edwh_admin', 'edwh_admin');

-- insert immutable versions
INSERT INTO configuration (uuid, version, is_version, parent, code, config_value, description, created_by, updated_by)  
  SELECT uuid, version, true, id, code, config_value, description, created_by, updated_by FROM configuration WHERE is_version = false AND uuid = 'f6c92306-28b1-11ea-978f-2e728ce88125';