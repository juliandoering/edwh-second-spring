CREATE TABLE carriercontractreference
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    carrier_id bigint,
    infotextglobal varchar,
    infotextusa varchar,

    CONSTRAINT PK_carriercontractreference PRIMARY KEY (id)
);

CREATE INDEX idx_carriercontractreference_uuid on carriercontractreference(uuid);



ALTER TABLE carriercontractreference ADD CONSTRAINT fk_carriercontractreference_carrier FOREIGN KEY (carrier_id) REFERENCES businessthirdpartygroup;
CREATE INDEX idx_carriercontractreference_carrier_id on carriercontractreference(carrier_id);
