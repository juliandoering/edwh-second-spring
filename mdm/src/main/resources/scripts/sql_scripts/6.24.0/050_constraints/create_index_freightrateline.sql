-- New Index for ocean freights on freightrateline parent
CREATE INDEX idx_freightrateline_parent ON mdm.freightrateline (parent);
CREATE INDEX idx_location_parent ON mdm.location (parent);
