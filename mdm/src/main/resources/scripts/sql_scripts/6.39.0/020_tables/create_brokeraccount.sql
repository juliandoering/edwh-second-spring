CREATE TABLE brokeraccount
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    familygroup varchar,
    accountid varchar,
    description varchar,
    groupcompany_id bigint,

    CONSTRAINT PK_brokeraccount PRIMARY KEY (id)
);

CREATE INDEX idx_brokeraccount_uuid on brokeraccount(uuid);



ALTER TABLE brokeraccount ADD CONSTRAINT fk_brokeraccount_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES GroupCompany;
CREATE INDEX idx_brokeraccount_groupcompany_id on brokeraccount(groupcompany_id);
