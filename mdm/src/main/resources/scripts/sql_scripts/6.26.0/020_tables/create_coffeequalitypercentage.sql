DROP TABLE IF EXISTS coffeequalitycompositionpart;

CREATE TABLE coffeequalitypercentage
(
   id                              bigserial   NOT NULL,
   uuid                            varchar,
   
   -- technical fields
   created_at                      timestamp   DEFAULT CURRENT_TIMESTAMP,
   updated_at                      timestamp   DEFAULT CURRENT_TIMESTAMP,
   deleted                         boolean     DEFAULT false NOT NULL,
   record_version                  integer     DEFAULT 0,
   version                         bigint      DEFAULT 0 NOT NULL,
   is_version                      boolean     DEFAULT false NOT NULL,
   parent                          bigint      DEFAULT 0 NOT NULL,
   created_by                      varchar,
   updated_by                      varchar,
   
   -- business fields
   coffeequality_id                bigint,
   factor                          numeric,
   referenced_by_coffeequality_id  bigint,
   
   CONSTRAINT PK_coffeequalitypercentage PRIMARY KEY (id)
);