INSERT INTO coffeequalitypercentage (uuid, created_by, updated_by, coffeequality_id, factor)
VALUES (
'21598750', 'edwh_admin', 'edwh_admin',
(SELECT id from coffeequality WHERE name = 'Misty Mountain' and is_version = false),
'0.6'
);
INSERT INTO coffeequalitypercentage (uuid, created_by, updated_by, coffeequality_id, factor)
VALUES (
'21598751', 'edwh_admin', 'edwh_admin',
(SELECT id from coffeequality WHERE name = 'Byte Beans' and is_version = false),
'0.25'
);

INSERT INTO coffeequalitypercentage (uuid, created_by, updated_by, coffeequality_id, factor)
VALUES (
'21598752', 'edwh_admin', 'edwh_admin',
(SELECT id from coffeequality WHERE name = 'Belgium Chocolate Beans' and is_version = false),
'0.48'
);

-- add composition part to coffee quality
UPDATE coffeequalitypercentage
SET referenced_by_coffeequality_id = (SELECT id from coffeequality WHERE name = 'Delta Quadrant' and is_version = false)
WHERE uuid = '21598751';
UPDATE coffeequalitypercentage
SET referenced_by_coffeequality_id = (SELECT id from coffeequality WHERE name = 'Delta Quadrant' and is_version = false)
WHERE uuid = '21598752';
UPDATE coffeequalitypercentage
SET referenced_by_coffeequality_id = (SELECT id from coffeequality WHERE name = 'Belgium Chocolate Beans' and is_version = false)
WHERE uuid = '21598750';

INSERT INTO coffeequalitypercentage(uuid, created_by, updated_by, version, is_version, parent, coffeequality_id, factor, referenced_by_coffeequality_id)
 SELECT uuid, created_by, updated_by, version, true, id,
 (SELECT id from coffeequality WHERE coffeequality.parent = coffeequality_id and coffeequality.is_version = TRUE ),
 factor,
 (SELECT id from coffeequality WHERE coffeequality.parent = referenced_by_coffeequality_id and coffeequality.is_version = TRUE )
FROM coffeequalitypercentage WHERE is_version = FALSE;