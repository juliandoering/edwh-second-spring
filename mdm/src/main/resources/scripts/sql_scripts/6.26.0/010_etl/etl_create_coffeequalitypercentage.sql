DROP TABLE IF EXISTS etl.coffeequalitycompositionpart;

CREATE TABLE etl.coffeequalitypercentage
(
   id                                       bigserial   NOT NULL,
   job_id                                   varchar,
   coffeequality_id                         bigint,
   -- Business Fields
   mdm_coffeequality_id                     bigint,
   factor                                   numeric,
   
   CONSTRAINT pk_coffeequalitypercentage PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequalitypercentage OWNER TO etl;