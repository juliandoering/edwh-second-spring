ALTER TABLE businessorganisationtype ADD COLUMN checkowner BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE businessorganisationtype ADD COLUMN checkUltimateBeneficiaryOwner BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE businessorganisationtype ADD COLUMN checkrepresentative BOOLEAN DEFAULT FALSE NOT NULL;
ALTER TABLE businessorganisationtype ADD COLUMN checkcontactperson BOOLEAN DEFAULT FALSE NOT NULL;

UPDATE businessorganisationtype
   SET checkowner = (SELECT checkowner
                     FROM businessorganisationtype_checks
                     WHERE businessorganisationtype_checks.businesstypecode = businessorganisationtype.code),
       checkultimatebeneficiaryowner = (SELECT checkbeneficiary
                                        FROM businessorganisationtype_checks
                                        WHERE businessorganisationtype_checks.businesstypecode = businessorganisationtype.code),
       checkrepresentative = (SELECT checkmanagement_representatives
                              FROM businessorganisationtype_checks
                              WHERE businessorganisationtype_checks.businesstypecode = businessorganisationtype.code),
       checkcontactperson = (SELECT checkcontactperson
                             FROM businessorganisationtype_checks
                             WHERE businessorganisationtype_checks.businesstypecode = businessorganisationtype.code)
WHERE EXISTS (SELECT 1
              FROM businessorganisationtype_checks
              WHERE businessorganisationtype_checks.businesstypecode = businessorganisationtype.code);

DROP TABLE businessorganisationtype_checks;
