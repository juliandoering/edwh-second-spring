UPDATE businessthirdparty
   SET legal_country_id = (SELECT MIN(location.id)
                           FROM location
                           WHERE location.si_unlocode = businessthirdparty.si_legaladdresscountrycode
                           AND   location.is_version = businessthirdparty.is_version)
WHERE legal_country_id IS NULL;
