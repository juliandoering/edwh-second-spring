/*
    DWH-1369 - Change Coffee Quality Name / Soft-Delete a Coffee Quality in Excel ETL Upload
    add the column _uuid_ to the etl.coffeequality table
 */
 alter table etl.coffeequality add column uuid varchar;
