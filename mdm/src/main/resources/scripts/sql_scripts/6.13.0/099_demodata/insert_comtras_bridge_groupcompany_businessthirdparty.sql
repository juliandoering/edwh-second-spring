-- Provide group companies for Comtras Bridge 
INSERT INTO groupcompany (uuid,deleted,record_version,version,is_version,parent,legal_name,acronym,tagetik_code,postal_address_id,created_by,updated_by,comtras_prefix,businessfunction_id) 
VALUES
  ('4f9d8d35-92a3-11e8-af8b-1765ec13386a',false,4,2,false,0,'Beneficio de Caf� Montecristo S.A. (BECAMO)','BECA','BECA-COEX',4,'edwh_admin','edwh_admin',NULL,1),
  ('4f9d8d35-92a3-11e8-af8b-1765ec13386a',false,0,0,true,0,'Beneficio de Caf� Montecristo S.A. (BECAMO)','BECA','BECA-COEX',7,'edwh_admin','edwh_admin',NULL,6),
  ('4f0e1e5e-92a3-11e8-af8b-1765ec13386a',false,0,0,true,0,'Bernhard Rothfos Intercaf� AG','BRIC HN','BRIC-BECA',7,'edwh_admin','edwh_admin',NULL,6),
  ('4f0e1e5e-92a3-11e8-af8b-1765ec13386a',false,4,2,false,0,'Bernhard Rothfos Intercaf� AG','BRIC HN','BRIC-BECA',4,'edwh_admin','edwh_admin',NULL,1),
  ('4f538bd9-92a3-11e8-af8b-1765ec13386a',false,0,0,true,0,'Bernhard Rothfos Intercaf� AG','BRIC VN','BRIC-NGVN',7,'edwh_admin','edwh_admin',NULL,6),
  ('4f538bd9-92a3-11e8-af8b-1765ec13386a',false,4,2,false,0,'Bernhard Rothfos Intercaf� AG','BRIC VN','BRIC-NGVN',4,'edwh_admin','edwh_admin',NULL,1),
  ('4ff08f42-92a3-11e8-af8b-1765ec13386a',false,0,0,true,0,'Neumann Gruppe Vietnam Limited','NGVN','NGVN-LOCA',7,'edwh_admin','edwh_admin',NULL,6),
  ('4ff08f42-92a3-11e8-af8b-1765ec13386a',false,4,2,false,0,'Neumann Gruppe Vietnam Limited','NGVN','NGVN-LOCA',4,'edwh_admin','edwh_admin',NULL,1),
  ('6b672a15-f9d6-11e7-9169-dd535ad1ee5b',false,4,2,false,0,'Bero Coffee Singapore Pte Ltd.','BSIN','BSIN-BSIN',4,'edwh_admin','edwh_admin',NULL,1),
  ('6b672a15-f9d6-11e7-9169-dd535ad1ee5b',false,2,2,true,0,'Bero Coffee Singapore Pte Ltd.','BSIN','BSIN-BSIN',7,'edwh_admin','edwh_admin',NULL,6);

-- Provide business third parties for Comtras Bridge
INSERT INTO businessthirdparty (uuid,record_version,deleted,version,is_version,parent,legalname,commercialname,acronym,businessorganisationtype_id,presentation_of_documents_id,paymentterm_id,created_by,updated_by,si_legaladdresscountrycode) 
VALUES
  ('c73cebbc-8501-11e8-9ab6-0242ac120006',7,false,3,false,0,'Tin Nghia Corporation (TIMEXCORP)','Tin Nghia Corporation (TIMEXCORP)','TIN NGHIA',2,1,3,'edwh_admin','edwh_admin','VN'),
  ('c73cebbc-8501-11e8-9ab6-0242ac120006',2,false,3,true,0,'Tin Nghia Corporation (TIMEXCORP)','Tin Nghia Corporation (TIMEXCORP)','TIN NGHIA',14,3,9,'edwh_admin','edwh_admin','VN'),
  ('c359be36-8501-11e8-9ab6-0242ac120006',7,false,3,false,0,'Phuc Sinh Corporation','Phuc Sinh Corporation','PHUC',2,1,3,'edwh_admin','edwh_admin','VN'),
  ('c359be36-8501-11e8-9ab6-0242ac120006',2,false,3,true,0,'Phuc Sinh Corporation','Phuc Sinh Corporation','PHUC',14,3,9,'edwh_admin','edwh_admin','VN');
