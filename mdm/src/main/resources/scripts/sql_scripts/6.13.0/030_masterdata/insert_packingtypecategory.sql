insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('9d095f56-579d-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'NFB', 'Natural Fiber Bags', 'Sisal, Jute, Henequen, Burlap or similar bag');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('9ca2cece-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'PPB', 'Woven Polypropylene Bags', 'Any kind of PP bag');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('a409d09a-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'SP', 'Specialty Packing', 'Any kind of material in small packing units (e.g. plastic, foil, paper)');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('ab21e098-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'GPB', 'GrainPro Bags', 'Specialty packing');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('b0f08b82-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'FIBC BB', 'Flexible Intermediate Bulk big bag', 'Woven polypropylene big bag');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('b7100aa6-579f-11e9-a66b-d663bd873d93', 'edwh_admin', 'edwh_admin', 'FIBC LB', 'Flexible Intermediate Bulk liner bag', 'Woven polypropylene liner bag');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('bce18b3a-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'WB', 'Wooden Barrel', 'Any size Specialty packing');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('c2369b70-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'CTN', 'Cardboard Box', 'Any size cardboard box');
insert into packingtypecategory(uuid, created_by, updated_by, code, description, comment) 
values('c8e286d2-579f-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'OTH', 'Other', 'Any other not declared packing');

insert into packingtypecategory (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description, comment) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description, comment from packingtypecategory where is_version is false;