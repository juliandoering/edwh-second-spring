-- Insert new currency SAR
INSERT INTO mdm.currency (uuid, code, description, created_by, updated_by)
VALUES ('ab7df21a-5b9c-11e9-8647-d663bd873d93', 'SAR', 'Saudi Riyal', 'edwh_admin', 'edwh_admin');

-- insert immutable versions   
INSERT INTO mdm.currency (uuid, version, is_version, parent, code, description, created_by, updated_by) 
  SELECT uuid, version, true, id, code, description, created_by, updated_by from mdm.currency WHERE is_version = false AND code IN ('SAR');
