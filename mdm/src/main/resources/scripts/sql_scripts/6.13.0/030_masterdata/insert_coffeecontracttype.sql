insert into coffeecontracttype(uuid, created_by, updated_by, code, description) 
values('ab7df21a-5b9c-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'ESCC', 'European Standard Contract for Coffee');
insert into coffeecontracttype(uuid, created_by, updated_by, code, description) 
values('ab7df4cc-5b9c-11e9-8647-d663bd873d93', 'edwh_admin', 'edwh_admin', 'GCA', 'Green Coffee Association');

insert into coffeecontracttype (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description from coffeecontracttype where is_version is false;