ALTER TABLE packing ADD COLUMN tare decimal;
ALTER TABLE packing ADD COLUMN unitsize decimal;
ALTER TABLE packing ADD COLUMN category_id bigint;
