ALTER TABLE packing ADD CONSTRAINT fk_packing_category FOREIGN KEY (category_id) REFERENCES PackingTypeCategory;
CREATE INDEX idx_packing_category_id on packing(category_id);
