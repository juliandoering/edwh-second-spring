INSERT INTO shipment_advice (UUID, contract_id, sequential_number, vessel_name, shipping_company_name, name_of_shippers, bill_of_lading_number, on_board_date, shipped_date, port_of_shipment, port_of_destination, quantity, description, main_marks)

VALUES
(
  'd22122a8-352f-11e9-d210-ae63bd873d9f',
  1,
  1,
  'Brauner Kutter',
  'Name d. Shipping Company',
  'Kurzname',
  1111,
  '2021-12-25'::DATE,
  '2021-12-25'::DATE,
  'ICO DODOTRANS',
  'Pasir Gudang',
  5000,
  'Kranke Description',
  'III-232323'
);

insert into shipment_advice (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, contract_id, sequential_number, vessel_name, shipping_company_name, name_of_shippers, bill_of_lading_number, on_board_date, shipped_date, port_of_shipment, port_of_destination, quantity, description, main_marks)
  select
    uuid, created_at, created_by, updated_at, updated_by, version, true, id,
    (select min(contract.id) from contract where contract.parent = shipment_advice.contract_id and contract.is_version is true),
    sequential_number,
    vessel_name,
    shipping_company_name,
    name_of_shippers,
    bill_of_lading_number,
    on_board_date,
    shipped_date,
    port_of_shipment,
    port_of_destination,
    quantity,
    description,
    main_marks
  from shipment_advice
  where uuid = 'd22122a8-352f-11e9-d210-ae63bd873d9f' and is_version is false;

INSERT INTO shipment_advice (UUID, contract_id, sequential_number, vessel_name, shipping_company_name, name_of_shippers, bill_of_lading_number, on_board_date, shipped_date, port_of_shipment, port_of_destination, quantity, description, main_marks)

VALUES
(
  'd33133a8-352f-11e9-d210-ae63bd873d9f',
  3,
  2,
  'Weißer Kutter',
  'NoNamed. Shipping Company',
  'Langname',
  2222,
  '2020-12-25'::DATE,
  '2020-12-25'::DATE,
  'ICO LALATRANS',
  'Besir Gudang',
  10000,
  'Gesunde Description',
  'LLL-343434'
);

insert into shipment_advice (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, contract_id, sequential_number, vessel_name, shipping_company_name, name_of_shippers, bill_of_lading_number, on_board_date, shipped_date, port_of_shipment, port_of_destination, quantity, description, main_marks)
  select
    uuid, created_at, created_by, updated_at, updated_by, version, true, id,
    (select min(contract.id) from contract where contract.parent = shipment_advice.contract_id and contract.is_version is true),
    sequential_number,
    vessel_name,
    shipping_company_name,
    name_of_shippers,
    bill_of_lading_number,
    on_board_date,
    shipped_date,
    port_of_shipment,
    port_of_destination,
    quantity,
    description,
    main_marks
  from shipment_advice
  where uuid = 'd33133a8-352f-11e9-d210-ae63bd873d9f' and is_version is false;
