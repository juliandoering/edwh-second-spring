ALTER TABLE etl.businessthirdpartybankaccount rename COLUMN bank_branch_address_street to bank_branch_addressline1;
ALTER TABLE etl.businessthirdpartybankaccount add COLUMN bank_branch_addressline2 varchar;
