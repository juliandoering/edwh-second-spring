INSERT INTO compliance_rating_type (uuid, code, description, created_by, updated_by)
VALUES ('28536729-6feb-49d3-b2c4-ae41d2534d8e', 'DISAPPROVED', 'Disapproved',
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE));

INSERT INTO compliance_rating_type (uuid, code, description, created_by, updated_by)
VALUES ('67fe80c1-edc9-42b0-818e-12d07268dba7', 'APPROVED', 'Approved',
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE));

INSERT INTO compliance_rating_type (uuid, version, is_version, parent, code, description, created_by, updated_by)
  select uuid, version, true, id, code, description, created_by, updated_by from compliance_rating_type
  where is_version is false and code IN ('APPROVED', 'DISAPPROVED');
-- set old value references to new values
-- rejected, unrated -> disapproved, high, low, medium -> approved
UPDATE compliancerating
SET complianceratingtype_id = (SELECT id from compliance_rating_type WHERE code = 'APPROVED' AND is_version = compliancerating.is_version),
  version = version + 1
WHERE (SELECT code from compliance_rating_type WHERE id = compliancerating.id) IN ('HIGH', 'MEDIUM', 'LOW')
AND is_version = FALSE;

UPDATE compliancerating
SET complianceratingtype_id = (SELECT id from compliance_rating_type WHERE code = 'DISAPPROVED' AND is_version = compliancerating.is_version),
  version = version + 1
WHERE (SELECT code from compliance_rating_type WHERE id = compliancerating.id) IN ('REJECTED', 'UNRATED')
AND is_version IS FALSE;

UPDATE compliance_rating_type SET deleted = TRUE WHERE code IN ('HIGH', 'MEDIUM', 'LOW', 'REJECTED', 'UNRATED');

ALTER TABLE compliancerating RENAME COLUMN datefrom to dateofrating;
ALTER TABLE compliancerating DROP COLUMN dateto;
