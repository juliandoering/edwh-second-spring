ALTER TABLE packing ADD COLUMN description VARCHAR;

-- concatenate data
UPDATE packing SET description = concat_ws(' ', packingtype, extended);

ALTER TABLE packing DROP COLUMN extended;
ALTER TABLE packing DROP COLUMN packingtype;