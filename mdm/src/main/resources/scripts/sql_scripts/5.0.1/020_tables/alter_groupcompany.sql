ALTER TABLE groupcompany ADD COLUMN business_key varchar;
ALTER TABLE groupcompany ADD COLUMN si_business_key varchar;

update groupcompany set
  business_key = COALESCE(tagetik_code, legal_name),
  si_business_key = regexp_replace(upper(COALESCE(tagetik_code, legal_name)), '[^A-Z]', '', 'g');
