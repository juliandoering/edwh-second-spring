ALTER TABLE etl.bookingconfir_transportationlocation DROP COLUMN date_type;
ALTER TABLE etl.bookingconfir_transportationlocation DROP COLUMN date_time;

ALTER TABLE etl.bookingconfir_transportationlocation ADD COLUMN earliestdeparture TIMESTAMP;
ALTER TABLE etl.bookingconfir_transportationlocation ADD COLUMN estimatedarrival TIMESTAMP;
ALTER TABLE etl.bookingconfir_transportationlocation ADD COLUMN latestdelivery TIMESTAMP;