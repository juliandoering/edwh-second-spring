-- add new reference

ALTER TABLE freightratetrade
  ADD COLUMN tradename_id bigint;

ALTER TABLE freightratetrade ADD CONSTRAINT fk_freightratetrade_tradename FOREIGN KEY (tradename_id) REFERENCES tradename;
CREATE INDEX idx_freightratetrade_tradename_id on freightratetrade(tradename_id);

-- create missing trade names as deleted

-- no version
INSERT INTO tradename (uuid, code, description, deleted)
SELECT max(uuid), freightratetrade.name, freightratetrade.name, true FROM freightratetrade
WHERE not exists (select 1 from tradename where lower(tradename.code) like '%' || lower(freightratetrade.name) || '%')
GROUP BY freightratetrade.name;

-- version
INSERT INTO tradename (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description
  FROM tradename WHERE is_version is false
  AND not uuid in (select distinct uuid from tradename where is_version is true);

-- add references

-- no version
UPDATE freightratetrade SET tradename_id = (
  select min(tradename.id) from tradename 
  where lower(tradename.code) like '%' || lower(freightratetrade.name) || '%'
  and freightratetrade.is_version = tradename.is_version)
WHERE is_version is false;

-- version
UPDATE freightratetrade SET tradename_id = (
  SELECT max(tradename.id) FROM tradename WHERE tradename.is_version is true AND tradename.parent = (select frt2.tradename_id from freightratetrade frt2 where freightratetrade.uuid = frt2.uuid and frt2.is_version is false))
WHERE is_version is true;
