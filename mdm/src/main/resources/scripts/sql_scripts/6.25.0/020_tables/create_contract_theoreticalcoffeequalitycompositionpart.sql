CREATE TABLE contract_theoreticalcoffeequalitycompositionpart
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    factor decimal,
    coffeequality_id bigint,
    contract_id bigint,

    CONSTRAINT PK_contract_theoreticalcoffeequalitycompositionpart PRIMARY KEY (id)
);

CREATE INDEX idx_contract_theoreticalcoffeequalitycompositionpart_uuid on contract_theoreticalcoffeequalitycompositionpart(uuid);

CREATE INDEX idx_contract_theoreticalcoffeequalitycompositionpart_contract_id on contract_theoreticalcoffeequalitycompositionpart(contract_id);
CREATE INDEX idx_contract_theoreticalcoffeequalitycompositionpart_coffeequality_id on contract_theoreticalcoffeequalitycompositionpart(coffeequality_id);