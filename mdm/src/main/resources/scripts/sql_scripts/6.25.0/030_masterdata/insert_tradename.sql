-- new entries for Trade Name
INSERT INTO tradename (uuid, code, description)
VALUES ('41896e1a-0098-11ea-8d71-362b9e155667', 'Argentina tea (FOB)', 'Argentina tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189714e-0098-11ea-8d71-362b9e155667', 'Bangladesh jute (FOB)', 'Bangladesh jute (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418972c0-0098-11ea-8d71-362b9e155667', 'Brazil (FOB)', 'Brazil (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418974aa-0098-11ea-8d71-362b9e155667', 'Brazil spices, seeds, husks (FOB)', 'Brazil spices, seeds, husks (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418975f4-0098-11ea-8d71-362b9e155667', 'Cameroon (FOB)', 'Cameroon (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418977c0-0098-11ea-8d71-362b9e155667', 'China (FOB)', 'China (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41897978-0098-11ea-8d71-362b9e155667', 'China poly bags, sheets, cloth and FAK (FOB)', 'China poly bags, sheets, cloth and FAK (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41897b94-0098-11ea-8d71-362b9e155667', 'China tea (FOB)', 'China tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41897cd4-0098-11ea-8d71-362b9e155667', 'Colombia East Coast (FOB)', 'Colombia East Coast (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41897e00-0098-11ea-8d71-362b9e155667', 'Colombia West Coast (FOB)', 'Colombia West Coast (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41898468-0098-11ea-8d71-362b9e155667', 'Costa Rica, Guatemala (FOB)', 'Costa Rica, Guatemala (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41898666-0098-11ea-8d71-362b9e155667', 'Costa Rica, Guatemala and Honduras (FOB)', 'Costa Rica, Guatemala and Honduras (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418987a6-0098-11ea-8d71-362b9e155667', 'El Salvador (FCA)', 'El Salvador (FCA)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418989d6-0098-11ea-8d71-362b9e155667', 'Ethiopia (FOB)', 'Ethiopia (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41898b16-0098-11ea-8d71-362b9e155667', 'Germany tea (FOB)', 'Germany tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41898cba-0098-11ea-8d71-362b9e155667', 'India (FOB)', 'India (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41898df0-0098-11ea-8d71-362b9e155667', 'India jute (FOB)', 'India jute (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41898f62-0098-11ea-8d71-362b9e155667', 'India poly bags, sheets,cloth (FOB)', 'India poly bags, sheets,cloth (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41899192-0098-11ea-8d71-362b9e155667', 'India spices, seeds, husks (FOB)', 'India spices, seeds, husks (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418992e6-0098-11ea-8d71-362b9e155667', 'India tea (FOB)', 'India tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418994bc-0098-11ea-8d71-362b9e155667', 'Indonesia (FOB)', 'Indonesia (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418996ce-0098-11ea-8d71-362b9e155667', 'Indonesia poly bags, sheets, cloth (FOB)', 'Indonesia poly bags, sheets, cloth (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('418998a4-0098-11ea-8d71-362b9e155667', 'Indonesia spices, seeds, husks (FOB)', 'Indonesia spices, seeds, husks (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41899a5c-0098-11ea-8d71-362b9e155667', 'Indonesia tea (FOB)', 'Indonesia tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41899b9c-0098-11ea-8d71-362b9e155667', 'Ivory Coast (FOB)', 'Ivory Coast (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41899d72-0098-11ea-8d71-362b9e155667', 'Kenya, Tanzania tea (FOB)', 'Kenya, Tanzania tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('41899eb2-0098-11ea-8d71-362b9e155667', 'Kenya,Tanzania,Burundi,Rwanda,Uganda,Congo (FOB)', 'Kenya,Tanzania,Burundi,Rwanda,Uganda,Congo (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189a092-0098-11ea-8d71-362b9e155667', 'Mexico (FOB)', 'Mexico (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189a236-0098-11ea-8d71-362b9e155667', 'Mozambique tea (FOB)', 'Mozambique tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189a36c-0098-11ea-8d71-362b9e155667', 'Nicaragua (FCA)', 'Nicaragua (FCA)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189a556-0098-11ea-8d71-362b9e155667', 'Papua New Guinea (FOB)', 'Papua New Guinea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189a718-0098-11ea-8d71-362b9e155667', 'Peru (FOB)', 'Peru (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189a858-0098-11ea-8d71-362b9e155667', 'Sri Lanka tea (FOB)', 'Sri Lanka tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189aa42-0098-11ea-8d71-362b9e155667', 'Turkey spices, seeds, husks (FOB)', 'Turkey spices, seeds, husks (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189ab6e-0098-11ea-8d71-362b9e155667', 'Vietnam (FOB)', 'Vietnam (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189ad1c-0098-11ea-8d71-362b9e155667', 'Vietnam spices, seeds, husks (FOB)', 'Vietnam spices, seeds, husks (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189aeb6-0098-11ea-8d71-362b9e155667', 'Vietnam tea (FOB)', 'Vietnam tea (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189b0dc-0098-11ea-8d71-362b9e155667', 'Zambia, Zimbabwe, Malawi (FOB)', 'Zambia, Zimbabwe, Malawi (FOB)');
INSERT INTO tradename (uuid, code, description)
VALUES ('4189b226-0098-11ea-8d71-362b9e155667', 'Zambia, Zimbabwe, Malawi tea (FOB)', 'Zambia, Zimbabwe, Malawi tea (FOB)');

-- immutable versions for Trade Name

INSERT INTO tradename (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description
  FROM tradename WHERE is_version is false;
