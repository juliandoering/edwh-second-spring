UPDATE coffeequality_commercialname
    SET groupcompany_id = (select id from groupcompany where tagetik_code = 'STBR-COEX' and is_version =
      (SELECT is_version from coffeequality where coffeequality.id = coffeequality_commercialname.coffeequality_id))
    WHERE name = 'Replicated Coffee';

UPDATE coffeequality_commercialname
    SET groupcompany_id = (select id from groupcompany where tagetik_code = 'NGVN' and is_version =
      (SELECT is_version from coffeequality where coffeequality.id = coffeequality_commercialname.coffeequality_id))
    WHERE name = 'Prime Dwarf Coffee';
