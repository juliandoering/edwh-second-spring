UPDATE freemarkertemplate SET template =
'A contract has been replaced

  old UUID: ${(entity.uuid)!"UUID is missing"}
  old link: ${(UriSelf0)!"Link is missing"}

  new UUID: ${(entity1.uuid)!"UUID is missing"}
  new link: ${(UriSelf1)!"Link is missing"}'
WHERE template_key = 'contract.replaced.body.long';

UPDATE freemarkertemplate SET template =
'There is a new price fixation:

  ${UriSelf0}'
WHERE template_key = 'fixation.created.body';
