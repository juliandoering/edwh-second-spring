-- Insert new BTP Group for Freight Rates
INSERT INTO businessthirdpartygroup ( UUID, deleted, record_version, version, is_version, parent, name )
VALUES ( 'ff98df88-7247-11e9-a923-1681be663d3e', FALSE, 0, 0, FALSE, 0, 'Maersk Line');
INSERT INTO businessthirdpartygroup ( UUID, deleted, record_version, version, is_version, parent, name )
VALUES ( 'ff98df88-7247-11e9-a923-1681be663d3f', FALSE, 0, 0, FALSE, 0, 'HAPAG');

insert into businessthirdpartygroup (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from businessthirdpartygroup where is_version is false;
