-- reference to the provider has been changed from BTP to BTP group
ALTER TABLE mdm.freightrateline
  DROP CONSTRAINT IF EXISTS fk_freightrateline_provider;

ALTER TABLE mdm.freightrateline
  ADD CONSTRAINT fk_freightrateline_provider FOREIGN KEY (provider_id)
  REFERENCES mdm.businessthirdpartygroup (id);
  
CREATE INDEX idx_freightline_provider_id on mdm.freightrateline(provider_id);
