-- insert contact data
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('ee87c0be-bdc6-4422-9954-8bc89d723290', 'stockler@stocklerltda.com.br', 'EMAIL',
	(SELECT id FROM groupcompany where tagetik_code = 'STBR-COEX' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('740feb44-d182-4661-bf53-c931afdb7764', 'lagoa@nkgfazendas.com', 'EMAIL',
	(SELECT id FROM groupcompany where tagetik_code = 'FZBR-FARM' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('1d853284-4a35-48f9-8217-bef805cb66d1', 'cuong@vn.com', 'EMAIL',
	(SELECT id FROM groupcompany where tagetik_code = 'NGVN' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('738c232f-b65b-4778-99c2-b5800698e7f6', 'ms@bric.ch', 'EMAIL',
	(SELECT id FROM groupcompany where tagetik_code = 'BRIC' and is_version is false));


insert into contact (uuid, version, is_version, parent, contactvalue, contacttype, groupcompany_id)
  select uuid, version, true, id, contactvalue, contacttype,
        (SELECT groupcompany.id FROM groupcompany where groupcompany.parent = contact.groupcompany_id and is_version is true)
  from contact where is_version is false and contacttype = 'EMAIL';

