INSERT INTO settlementterminalrate (
    uuid,
    futuremarket_id,
    futuremonth_id,
    futures_year,
    settlement_date,
    settlement_value
) VALUES (
    '329990',
	(SELECT id FROM futuremarket WHERE code = 'KC' AND is_version is false),
	(SELECT id FROM futuremonth WHERE code = 'K' AND is_version is false),
	23,
	date '2017-10-01',
	2.34
);

insert into settlementterminalrate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, futuremarket_id, futuremonth_id, futures_year, settlement_date, settlement_value) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, futuremarket_id, futuremonth_id, futures_year, settlement_date, settlement_value from settlementterminalrate where is_version is false;
  