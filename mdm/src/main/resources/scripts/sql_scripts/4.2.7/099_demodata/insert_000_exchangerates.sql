INSERT INTO exchangerate (
    uuid,
    currency_from_id,
    currency_to_id,
    settlement_date,
    exchange_rate
) VALUES (
    '321110',
	(SELECT id FROM currency WHERE code = 'EUR' AND is_version is false),
	(SELECT id FROM currency WHERE code = 'USD' AND is_version is false),
	date '2017-10-01',
	2.34
);

insert into exchangerate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, currency_from_id, currency_to_id, settlement_date, exchange_rate) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, currency_from_id, currency_to_id, settlement_date, exchange_rate from exchangerate where is_version is false;