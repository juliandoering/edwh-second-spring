CREATE TABLE settlementterminalrate
(
    id bigserial,
    uuid varchar,
    
    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    -- business fields
    futuremarket_id bigint,
    futuremonth_id  bigint,
    futures_year     integer,
    settlement_date  date,
    settlement_value decimal,
    
    CONSTRAINT PK_settlementterminalrate PRIMARY KEY (id)
);

ALTER TABLE settlementterminalrate ADD CONSTRAINT fk_futuremarket FOREIGN KEY (futuremarket_id) REFERENCES futuremarket;
ALTER TABLE settlementterminalrate ADD CONSTRAINT fk_futuremonth FOREIGN KEY (futuremonth_id) REFERENCES futuremonth;
