ALTER TABLE currency RENAME COLUMN currency TO code;
ALTER TABLE currency ADD COLUMN description varchar;

UPDATE currency SET description = 'Euro' where code = 'EUR';
UPDATE currency SET description = 'US Dollar' where code = 'USD';
