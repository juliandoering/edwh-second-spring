CREATE TABLE exchangerate
(
    id bigserial,
    uuid varchar,
    
    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    -- business fields
    currency_from_id  bigint,
    currency_to_id    bigint,
    settlement_date   date,
    exchange_rate     decimal,
    
    CONSTRAINT PK_exchangerate PRIMARY KEY (id)
);

ALTER TABLE exchangerate ADD CONSTRAINT fk_currency_from FOREIGN KEY (currency_from_id) REFERENCES currency;
ALTER TABLE exchangerate ADD CONSTRAINT fk_currency_to FOREIGN KEY (currency_to_id) REFERENCES currency;
