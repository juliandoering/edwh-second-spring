/*
 * Script update etl.bookingconfir_party
 */

ALTER TABLE etl.bookingconfir_party ALTER COLUMN identifier TYPE varchar;
ALTER TABLE etl.bookingconfir_party ALTER COLUMN identifier_type TYPE varchar;
