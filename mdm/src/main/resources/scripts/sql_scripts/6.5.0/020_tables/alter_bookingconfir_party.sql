/*
 * Script update mdm.bookingconfir_party
 */

ALTER TABLE mdm.bookingconfirmationparty ALTER COLUMN identifier TYPE varchar;
ALTER TABLE mdm.bookingconfirmationparty ALTER COLUMN identifier_type TYPE varchar;
