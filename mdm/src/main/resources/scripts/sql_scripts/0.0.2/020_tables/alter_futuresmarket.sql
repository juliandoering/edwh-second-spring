-- sic: future market as in the original table (not futureS market)
ALTER TABLE futuremarket
ADD lot_quantityunit_id bigint;

ALTER TABLE futuremarket
ADD lot_size integer;


-- insert the quantity unit for LKD and KC
UPDATE futuremarket 
	SET lot_quantityunit_id = (SELECT id FROM quantityunit WHERE code = 'MTS' AND is_version = true)
	WHERE code = 'LKD';
UPDATE futuremarket 
	SET lot_quantityunit_id = (SELECT id FROM quantityunit WHERE code = 'LBS' AND is_version = true)
	WHERE code = 'KC';
-- insert the lot size for LKD and KC
UPDATE futuremarket 
	SET lot_size = 10
	WHERE code = 'LKD';
UPDATE futuremarket 
	SET lot_size = 37500
	WHERE code = 'KC';



-- set constraints
ALTER TABLE futuremarket ADD CONSTRAINT fk_lot_quantityunit FOREIGN KEY (lot_quantityunit_id) REFERENCES quantityunit;

