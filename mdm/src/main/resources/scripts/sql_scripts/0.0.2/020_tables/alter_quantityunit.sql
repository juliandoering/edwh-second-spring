-- DWH-232 dwh reporting - coffee risk position report - add value in kg to be able to change from one quantity unit to another 
ALTER TABLE mdm.quantityunit
    ADD kg decimal;
