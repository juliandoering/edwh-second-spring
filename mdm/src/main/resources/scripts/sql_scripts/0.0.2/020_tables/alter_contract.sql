-- DWH-232 dwh reporting - coffee risk position report - flags for extracting the correct data for dwh reporting 
ALTER TABLE mdm.contract
    ADD fixed boolean NOT NULL default FALSE;
ALTER TABLE mdm.contract
    ADD fixed_date date;
ALTER TABLE mdm.contract
    ADD shipped boolean NOT NULL default FALSE;
ALTER TABLE mdm.contract
    ADD shipped_date date;

ALTER TABLE mdm.contract RENAME COLUMN coffeew_buyersellercode_id TO coffeew_contractrole_id;
ALTER TABLE mdm.contract RENAME COLUMN instr_info_buyersellercode_id TO instr_info_contractrole_id;
    
CREATE INDEX IF NOT EXISTS idx_contract_fixed on mdm.contract (fixed);
CREATE INDEX IF NOT EXISTS idx_contract_shipped on mdm.contract (shipped);
