--changeset generated_nf:1497621355 
CREATE TABLE mdm.delivery_futuresmonth
(
  id               BIGSERIAL,
  futuresmarket_id BIGINT,
  deliverymonth    INTEGER,
  futuresmonth     INTEGER,

  CONSTRAINT PK_delivery_futuresmonth PRIMARY KEY (id)
);
