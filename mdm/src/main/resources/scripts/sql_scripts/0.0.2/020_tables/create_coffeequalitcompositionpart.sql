CREATE TABLE coffeequalitycompositionpart (

    id bigserial,
    uuid varchar,
    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    coffeequality_id bigint,
    factor decimal,
    referenced_by_coffeequality_id bigint,

    CONSTRAINT PK_coffeequalitycompositionpart PRIMARY KEY (id)
);