CREATE TABLE futuremarket_futuremonth (
  id              BIGSERIAL,
  futuremarket_id BIGINT,
  futuremonth_id  BIGINT,

  CONSTRAINT PK_futuremarket_futuremonth PRIMARY KEY (id)
);