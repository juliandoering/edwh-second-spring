INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204121',
	'Misty Mountain',
	(SELECT id FROM coffeequality_origintype WHERE code = 'S' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'VAPOR' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'ROB' AND is_version is false),
	'best grade',
	'no defects whatsoever',
	'practically wet',
	'artistic'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204122',
	'Secretly Tea',
	(SELECT id FROM coffeequality_origintype WHERE code = 'SO' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'GREGRA' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'SEMIWAARAB' AND is_version is false),
	'awesome',
	'tea is always perfect',
	'very dry',
	'tasty'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204123',
	'Ghostwood Variety',
	(SELECT id FROM coffeequality_origintype WHERE code = 'BL' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'WETPOL' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'ARABROB' AND is_version is false),
	'black as the night',
	'i can see no flaws',
	'just right',
	'most delicious drink'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204124',
	'Delta Quadrant',
	(SELECT id FROM coffeequality_origintype WHERE code = 'SO' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'DRYCH' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'WAROB' AND is_version is false),
	'half leeola root',
	'only replicated is worse',
	'10%',
	'finest organic suspension ever devised'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204125',
	'Byte Beans',
	(SELECT id FROM coffeequality_origintype WHERE code = 'U' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'DECAF' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'UNROB' AND is_version is false),
	'digital',
	'some glitches',
	'dry silicon',
	'full of pixels'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204126',
	'Kryptonian High Ground',
	(SELECT id FROM coffeequality_origintype WHERE code = 'BMO' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'DECAF' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'WAARAB' AND is_version is false),
	'was great once',
	'destroyed',
	'spacey',
	'only dust'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204127',
	'Belgium Chocolate Beans',
	(SELECT id FROM coffeequality_origintype WHERE code = 'BL' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'WETPOL' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'UNROB' AND is_version is false),
	'white choc- er, coffee',
	'should have been chocolate',
	'very wet',
	'milky'
);

INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile
) VALUES (
	'12204128',
	'Blue Skies Growth',
	(SELECT id FROM coffeequality_origintype WHERE code = 'SO' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'SOLA' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'UNWAARAB' AND is_version is false),
	'sky high',
	'3 small clouds',
	'0% rain probability',
	'endless'
);

insert into coffeequality (uuid, version, is_version, parent, name, coffeequality_origintype_id, coffee_type_id , coffee_variety_id , grade , defectcount , humidity , cup_profile )
  select uuid, version, true, id, name,
        (SELECT coffeequality_origintype.id FROM coffeequality_origintype WHERE coffeequality_origintype.parent = coffeequality.coffeequality_origintype_id AND is_version is true),
        (SELECT coffee_type.id FROM coffee_type WHERE coffee_type.parent = coffeequality.coffee_type_id AND coffee_type.is_version is true),
        (SELECT coffee_variety.id FROM coffee_variety WHERE coffee_variety.parent = coffeequality.coffee_variety_id AND is_version is true),
        grade , defectcount , humidity , cup_profile
  from coffeequality where is_version is false and uuid IN ('12204121', '12204122', '12204123', '12204124', '12204125', '12204126', '12204127', '12204128');