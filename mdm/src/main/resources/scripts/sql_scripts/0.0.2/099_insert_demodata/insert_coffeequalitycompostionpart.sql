INSERT INTO coffeequalitycompositionpart (uuid, coffeequality_id, factor)
VALUES (
'21598750',
(SELECT id from coffeequality WHERE name = 'Misty Mountain' and is_version = false),
'0.6'
);
INSERT INTO coffeequalitycompositionpart (uuid, coffeequality_id, factor)
VALUES (
'21598751',
(SELECT id from coffeequality WHERE name = 'Byte Beans' and is_version = false),
'0.25'
);

INSERT INTO coffeequalitycompositionpart (uuid, coffeequality_id, factor)
VALUES (
'21598752',
(SELECT id from coffeequality WHERE name = 'Belgium Chocolate Beans' and is_version = false),
'0.48'
);

-- add composition part to coffee quality
UPDATE coffeequalitycompositionpart
SET referenced_by_coffeequality_id = (SELECT id from coffeequality WHERE name = 'Delta Quadrant' and is_version = false)
WHERE uuid = '21598751';
UPDATE coffeequalitycompositionpart
SET referenced_by_coffeequality_id = (SELECT id from coffeequality WHERE name = 'Delta Quadrant' and is_version = false)
WHERE uuid = '21598752';
UPDATE coffeequalitycompositionpart
SET referenced_by_coffeequality_id = (SELECT id from coffeequality WHERE name = 'Belgium Chocolate Beans' and is_version = false)
WHERE uuid = '21598750';

INSERT INTO coffeequalitycompositionpart(uuid, version, is_version, parent, coffeequality_id, factor, referenced_by_coffeequality_id)
 SELECT uuid, version, true, id,
 (SELECT id from coffeequality WHERE coffeequality.parent = coffeequality_id and coffeequality.is_version = TRUE ),
 factor,
 (SELECT id from coffeequality WHERE coffeequality.parent = referenced_by_coffeequality_id and coffeequality.is_version = TRUE )
FROM coffeequalitycompositionpart WHERE is_version = FALSE;