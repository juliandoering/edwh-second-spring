INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217041',
		(SELECT id FROM productcertificationprogram WHERE code = 'FAIR' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Misty Mountain' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217042',
		(SELECT id FROM productcertificationprogram WHERE code = 'ORG' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Secretly Tea' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217043',
		(SELECT id FROM productcertificationprogram WHERE code = 'SBUX' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Ghostwood Variety' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217044',
		(SELECT id FROM productcertificationprogram WHERE code = '4C' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Byte Beans' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217045',
		(SELECT id FROM productcertificationprogram WHERE code = 'UTZ' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Byte Beans' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217046',
		(SELECT id FROM productcertificationprogram WHERE code = 'NES3A' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Kryptonian High Ground' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217047',
		(SELECT id FROM productcertificationprogram WHERE code = 'RA' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Belgium Chocolate Beans' and is_version is false)
	);