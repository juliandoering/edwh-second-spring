INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221041',
	(SELECT id FROM coffeequality WHERE name = 'Misty Mountain' and is_version is false),
	'Prime Dwarf Coffee'
);

INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221042',
	(SELECT id FROM coffeequality WHERE name = 'Secretly Tea' and is_version is false),
	'Not-Really-Coffee'
);
INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221043',
	(SELECT id FROM coffeequality WHERE name = 'Secretly Tea' and is_version is false),
	'Better-Than-Coffee'
);

INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221044',
	(SELECT id FROM coffeequality WHERE name = 'Ghostwood Variety' and is_version is false),
	'Cooper''s Best Coffee'
);

INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221045',
	(SELECT id FROM coffeequality WHERE name = 'Byte Beans' and is_version is false),
	'Digital Decaffeinated Drink'
);

INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221046',
	(SELECT id FROM coffeequality WHERE name = 'Delta Quadrant' and is_version is false),
	'Replicated Coffee'
);

INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221047',
	(SELECT id FROM coffeequality WHERE name = 'Kryptonian High Ground' and is_version is false),
	'Super-Coffee'
);

INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221048',
	(SELECT id FROM coffeequality WHERE name = 'Kryptonian High Ground' and is_version is false),
	'Heroes'' Coffee'
);
insert into coffeequality_commercialname (uuid, version, is_version, parent, coffeequality_id, name)
  select uuid, version, true, id,
        (SELECT coffeequality.id FROM coffeequality WHERE coffeequality.parent = coffeequality_commercialname.coffeequality_id AND coffeequality.is_version is true),
        name from coffeequality_commercialname where is_version is false;