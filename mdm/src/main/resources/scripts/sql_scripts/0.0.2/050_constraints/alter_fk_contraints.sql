ALTER TABLE contract DROP CONSTRAINT fk_instr_info_buyersellercode;
ALTER TABLE contract ADD CONSTRAINT fk_instr_info_contractrole FOREIGN KEY (instr_info_contractrole_id) REFERENCES contractrole;
ALTER TABLE contract DROP CONSTRAINT fk_coffeew_buyersellercode;
ALTER TABLE contract ADD CONSTRAINT fk_coffeew_contractrole FOREIGN KEY (coffeew_contractrole_id) REFERENCES contractrole;
ALTER TABLE contractrole_businessfunction DROP CONSTRAINT fk_buyersellercode;
ALTER TABLE contractrole_businessfunction ADD CONSTRAINT fk_contractrole FOREIGN KEY (contractrole_id) REFERENCES contractrole;
ALTER TABLE contractrole RENAME CONSTRAINT PK_buyersellercode TO PK_contractrole;
