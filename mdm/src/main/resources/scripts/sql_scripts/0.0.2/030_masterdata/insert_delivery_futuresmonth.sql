-- DWH-232 dwh reporting - coffee risk position report - create on table that can be joined with futuremarket 
INSERT INTO mdm.delivery_futuresmonth (futuresmarket_id, deliverymonth, futuresmonth)
 (SELECT m.id, d.deliverymonth, d.futuresmonth 
  FROM mdm.futuremarket m, mdm.delivery_futuresmonth_london d
  WHERE m.code = 'LKD');
  
INSERT INTO mdm.delivery_futuresmonth (futuresmarket_id, deliverymonth, futuresmonth)
 (SELECT m.id, d.deliverymonth, d.futuresmonth 
  FROM mdm.futuremarket m, mdm.delivery_futuresmonth_ny d
  WHERE m.code = 'KC');
