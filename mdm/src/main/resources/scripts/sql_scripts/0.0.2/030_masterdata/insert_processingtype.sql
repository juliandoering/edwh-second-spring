INSERT INTO processingtype (uuid,code,name)
	VALUES ('a69068ac-9ef4-11e7-be27-00ff0b1c94f7','MILLING','Milling');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a69068ad-9ef4-11e7-be27-00ff0b1c94f7','CONSOLIDATION','Consolidation');

insert into processingtype (version, is_version, parent, uuid, code, name) 
  select version, true, id, uuid, code, name from processingtype 
  where is_version is false and uuid in ('1332030', '1333030');
