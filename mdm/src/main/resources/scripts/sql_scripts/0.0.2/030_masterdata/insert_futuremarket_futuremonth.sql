-- New York
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'KC' and is_version = FALSE ), (SELECT id from futuremonth where code = 'H' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'KC' and is_version = FALSE ), (SELECT id from futuremonth where code = 'K' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'KC' and is_version = FALSE), (SELECT id from futuremonth where code = 'N' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'KC' and is_version = FALSE), (SELECT id from futuremonth where code = 'U' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'KC' and is_version = FALSE), (SELECT id from futuremonth where code = 'Z' and is_version = FALSE));
-- London
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'LKD'  and is_version = FALSE), (SELECT id from futuremonth where code = 'F' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'LKD' and is_version = FALSE), (SELECT id from futuremonth where code = 'H' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'LKD' and is_version = FALSE), (SELECT id from futuremonth where code = 'K' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'LKD' and is_version = FALSE), (SELECT id from futuremonth where code = 'N' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'LKD' and is_version = FALSE), (SELECT id from futuremonth where code = 'U' and is_version = FALSE));
INSERT INTO futuremarket_futuremonth (futuremarket_id, futuremonth_id)
VALUES ((SELECT id from futuremarket where code = 'LKD' and is_version = FALSE), (SELECT id from futuremonth where code = 'X' and is_version = FALSE));
