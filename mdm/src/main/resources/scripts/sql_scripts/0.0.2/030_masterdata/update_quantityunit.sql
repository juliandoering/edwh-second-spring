--changeset generated_nf:1497621355 
UPDATE quantityunit set kg = 1 
	WHERE code = 'KGS';
UPDATE quantityunit set kg = 1000 
	WHERE code = 'MTS';
UPDATE quantityunit set kg = 0.45359237 
	WHERE code = 'LBS';
UPDATE quantityunit set kg = 45.359237 
	WHERE code = '46KB';
UPDATE quantityunit set kg = 50 
	WHERE code = '50KB';
UPDATE quantityunit set kg = 60 
	WHERE code = '60KB';
UPDATE quantityunit set kg = 69 
	WHERE code = '69KB';
UPDATE quantityunit set kg = 70 
	WHERE code = '70KB';
