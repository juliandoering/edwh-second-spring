-- insert new 'Big Bag 7.2' (Nestlé empty 7.2kg , 1600kg bag)
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('f498a945-ac4e-4857-af62-f22576af7c8f','1600KGBB','1600kg Big-Bags',null);

INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('f498a945-ac4e-4857-af62-f22576af7c8f','1600KGBB','1600kg Big-Bags',null, true, 
	(SELECT id FROM mdm.packing WHERE code = '1600KGBB') );

-- update basic set of packing types

UPDATE mdm.packing 
	SET code = '60KGB'
	WHERE packingtype = '60kg Jute Bags';
UPDATE mdm.packing 
	SET code = '69KGB'
	WHERE packingtype = '69kg Jute Bags';
UPDATE mdm.packing 
	SET code = '70KGB'
	WHERE packingtype = '70kg Jute Bags';
UPDATE mdm.packing 
	SET code = '1MTBB'
	WHERE packingtype = '1MT PP Big-Bags';
UPDATE mdm.packing 
	SET code = '21MTCL'
	WHERE packingtype = '21MT PP Container-Liners';

-- insert 'new' types that came up after cuong provided his packing types (create immutable versions below)
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('f8b1d0c5-477e-4009-85f4-660a41bfd704','60KGB_FBDL','60kg Jute Bags','Fancy Bag Dep Lam');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('53ca890c-8942-43bd-8736-8bcd6916aff1','60KGB_FBTD','60kg Jute Bags','Fancy Bag Tuoi Dep');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('3fbf5a2c-a602-4d79-b76a-c3ed435e99c0','60KGB_NJB','60kg Jute Bags','New jute bag');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('5993daec-bc35-4cf6-a118-169c0790d105','60KGB_FBXT','60kg Jute Bags','Fancy Bag XT');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('354fcbb8-5ea5-474e-b3d8-d02348b25aa9','60KGB_FBMM','60kg Jute Bags','Fancy Bag Manh Me');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('fbd65c02-3d0d-4953-810c-41d2aad108f1','60KGB_FBTT','60kg Jute Bags','Fancy Bag Than Thien');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('5800ee4e-6106-4967-8741-47d6b5b9c081','60KGB_FBTV','60kg Jute Bags','Fancy Bag Tuyet Voi');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('9f1dc74d-28cd-4360-8c38-926432d9f275','60KGB_GSB' ,'60kg Jute Bags','Green Stripe Bag');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('c0615126-5e51-420a-a2d8-971802703558','1MTBB_110','1MT PP Big-Bags','110cm wide');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('6b3ad9f1-455b-410f-9ba6-72570013a375','1MTBB_120','1MT PP Big-Bags','120cm wide');
INSERT INTO mdm.packing (uuid,code,packingtype,extended)
VALUES ('fa56bc23-4b43-4db3-a208-a5ed11cf34dd','1600KGBB_72N','1600kg Big-Bags','type Nestlé 7.2');

-- create immutable versions of the above set of 'new' types
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('fc0d8ced-0f7d-4c64-8d5b-4ea6e9bb7d02','60KGB_FBDL','60kg Jute Bags','Fancy Bag Dep Lam',
	true,(SELECT id FROM mdm.packing WHERE extended ='Fancy Bag Dep Lam'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('6d2c5a1d-bdf0-44bc-8f62-4832e1ff0d11','60KGB_FBTD','60kg Jute Bags','Fancy Bag Tuoi Dep',
	true,(SELECT id FROM mdm.packing WHERE extended ='Fancy Bag Tuoi Dep'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('eadb67ca-c82e-4b80-932b-bacda804f8f4','60KGB_NJB','60kg Jute Bags','New jute bag',
	true,(SELECT id FROM mdm.packing WHERE extended ='New jute bag'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('90d7a247-e1f2-45bf-b7e0-ac0ed3989f90','60KGB_FBXT','60kg Jute Bags','Fancy Bag XT',
	true,(SELECT id FROM mdm.packing WHERE extended ='Fancy Bag XT'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('4386a41b-d430-467b-879c-63d07206191b','60KGB_FBMM','60kg Jute Bags','Fancy Bag Manh Me',
	true,(SELECT id FROM mdm.packing WHERE extended ='Fancy Bag Manh Me'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('99ecc590-00d8-4d5d-b4bd-31bf6357df59','60KGB_FBTT','60kg Jute Bags','Fancy Bag Than Thien',
	true,(SELECT id FROM mdm.packing WHERE extended ='Fancy Bag Than Thien'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('a208e633-8141-451a-bd30-91717aa13187','60KGB_FBTV','60kg Jute Bags','Fancy Bag Tuyet Voi',
	true,(SELECT id FROM mdm.packing WHERE extended ='Fancy Bag Tuyet Voi'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('3bbcf6e4-01f5-4170-985e-73d8fdccec0b','60KGB_GSB' ,'60kg Jute Bags','Green Stripe Bag',
	true,(SELECT id FROM mdm.packing WHERE extended ='Green Stripe Bag'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('1869fa1b-bf80-4b28-a9cb-a46799af2716','1MTBB_110','1MT PP Big-Bags','110cm wide',
	true,(SELECT id FROM mdm.packing WHERE extended ='110cm wide'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('70bbbd39-7b41-45c6-8e16-3d2967b5745a','1MTBB_120','1MT PP Big-Bags','120cm wide',
	true,(SELECT id FROM mdm.packing WHERE extended ='120cm wide'));
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version, parent)
VALUES ('3164864a-181b-428d-b017-44f5fbc1c215','1600KGBB_72N','1600kg Big-Bags','type Nestlé 7.2',
	true,(SELECT id FROM mdm.packing WHERE extended ='type Nestlé 7.2'));




