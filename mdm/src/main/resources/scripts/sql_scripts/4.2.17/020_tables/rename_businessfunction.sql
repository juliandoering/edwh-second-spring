ALTER TABLE businessfunction RENAME TO thirdpartytype;
ALTER TABLE contractrole_businessfunction RENAME TO contractrole_thirdpartytype;

ALTER TABLE mdm.businessthirdparty RENAME COLUMN businessfunction_id TO thirdpartytype_id;
ALTER TABLE contractrole_thirdpartytype RENAME COLUMN businessfunction_id TO thirdpartytype_id;
ALTER TABLE etl.businessthirdparty RENAME COLUMN businessfunction_id TO thirdpartytype_id;