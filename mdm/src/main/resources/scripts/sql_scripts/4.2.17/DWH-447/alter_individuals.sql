-- many to many table
CREATE TABLE groupcompany_individual
(
    id bigserial,
    groupcompany_id bigint,
    individual_id bigint,
    
    CONSTRAINT PK_groupcompany_individual PRIMARY KEY (id)
);

-- reference groupcompany -> individual
ALTER TABLE groupcompany_individual ADD CONSTRAINT fk_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES groupcompany;
ALTER TABLE groupcompany_individual ADD CONSTRAINT fk_individual FOREIGN KEY (individual_id) REFERENCES individual;

-- speed up lookups from groupcompany to individuals
CREATE INDEX idx_groupcompany_individual_groupcompany_id on groupcompany_individual (groupcompany_id);

-- move existing references to the new table
INSERT INTO groupcompany_individual(groupcompany_id, individual_id) 
  (SELECT groupcompany_id, id FROM individual WHERE groupcompany_id IS NOT NULL);
  
-- remove old foreign key field
ALTER TABLE individual DROP groupcompany_id;