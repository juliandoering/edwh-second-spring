-- Ocean/Logistic Events
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', 'AE', 'Loaded on Vessel',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', 'AO', 'Less than one container',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9258-4937f0a81d18', 'AP', 'Loaded on Feeder Vessel',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9268-4937f0a81d18', 'C', 'Estimated Time of Departure (ETD) Delay',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9278-4937f0a81d18', 'E', 'Estimated Time of Arrival (ETA) Delay',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9288-4937f0a81d18', 'EE', 'Empty Equipment Dispatched',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9298-4937f0a81d18', 'I', 'In-Gate',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-92a8-4937f0a81d18', 'OA', 'Out-Gate',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-92b8-4937f0a81d18', 'RD', 'Empty Equipment Returned',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-92c8-4937f0a81d18', 'UV', 'Unloaded From Vessel',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-92d8-4937f0a81d18', 'VA', 'Vessel Arrival, Vessel scheduled to arrive or has arrived',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-92e8-4937f0a81d18', 'VD', 'Vessel Departure, Vessel scheduled to depart or has departed',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));

-- Administrative Events
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-92f8-4937f0a81d18', 'AV', 'Container Available',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9318-4937f0a81d18', 'CH', 'Customs Hold',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9328-4937f0a81d18', 'CR', 'Carrier Release',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9338-4937f0a81d18', 'CT', 'Customs Released',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9348-4937f0a81d18', 'HR', 'Carrier Un-Release',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9358-4937f0a81d18', 'IB', 'U.S. Customs, In-bond Movement Authorized',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));

-- Rail Events
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9368-4937f0a81d18', 'A', 'Rail Arrival at In-Transit Location',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9378-4937f0a81d18', 'AL', 'Loaded on Rail',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9388-4937f0a81d18', 'AR', 'Rail Arrival at Destination Intermodal Ramp',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9398-4937f0a81d18', 'J', 'Delivered to Connecting Line',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-93a8-4937f0a81d18', 'NF', 'Free Time to Expire',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-93b8-4937f0a81d18', 'NT', 'Notification',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-93c8-4937f0a81d18', 'P', 'Rail Departed from In-Transit Location',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-93d8-4937f0a81d18', 'RL', 'Rail Departure from Origin Intermodal Ramp',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-93e8-4937f0a81d18', 'UR', 'Unloaded from a Rail Car',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));

-- Truck Events
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-93f8-4937f0a81d18', 'AA', 'Pickup Appointment Date and Time',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9418-4937f0a81d18', 'AD', 'Delivery Appointment Date and Time',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9428-4937f0a81d18', 'AF', 'Departed Pickup Location',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9438-4937f0a81d18', 'X', 'Removed from Customer Dock or Siding',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9448-4937f0a81d18', 'AM', 'Loaded on Truck',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9458-4937f0a81d18', 'D', 'Completed unloading at delivery location',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9468-4937f0a81d18', 'X!', 'Arrived at Delivery Location, the carrier has arrived at the shipment delivery location',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('613e63de-8450-4bd2-9478-4937f0a81d18', 'X3', 'Arrived at Pick-up Location',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
    
-- insert immutable versions   
insert into eventcode (uuid, version, is_version, parent, code, description, created_by, updated_by) 
  select uuid, version, true, id, code, description, created_by, updated_by from eventcode where is_version is false;