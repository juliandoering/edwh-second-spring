INSERT INTO processingtype (uuid, code, description, created_by, updated_by)
    VALUES ('e06cb608-d38b-11e8-a8d5-f2801f1b9fd1', 'TRANSFER', 'Transfer',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
 
 insert into processingtype (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description from processingtype where is_version is false and code in ('TRANSFER', 'CONSOLIDATION', 'MILLING');