-- Mapping Table contract_productcertificationprogram
ALTER TABLE contract_productcertificationprogram
  ADD CONSTRAINT fk_contract_productcertificationprogram_contract FOREIGN KEY (contract_id)
  REFERENCES contract (id);

ALTER TABLE contract_productcertificationprogram
  ADD CONSTRAINT fk_contract_productcertificationprogram_productcertificationprogram FOREIGN KEY (productcertificationprogram_id)
  REFERENCES productcertificationprogram (id);