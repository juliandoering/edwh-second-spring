CREATE TABLE contract_productcertificationprogram
(
    id bigserial,
    
    /* payload */ 
    contract_id bigint,
    productcertificationprogram_id bigint,
    
    CONSTRAINT PK_contract_productcertificationprogram PRIMARY KEY (id)
);