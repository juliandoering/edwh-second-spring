CREATE TABLE eventcode
(
   id              bigserial   NOT NULL,
   uuid            varchar,
   created_at      timestamp   DEFAULT now(),
   updated_at      timestamp   DEFAULT now(),
   record_version  integer     DEFAULT 0,
   deleted         boolean     DEFAULT false NOT NULL,
   version         bigint      DEFAULT 0 NOT NULL,
   is_version      boolean     DEFAULT false NOT NULL,
   parent          bigint      DEFAULT 0 NOT NULL,
   code            varchar,
   description     varchar,
   created_by      bigint,
   updated_by      bigint,
   
   CONSTRAINT PK_eventcode PRIMARY KEY (id)
);