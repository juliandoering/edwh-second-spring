--changeset generated_af:1497621354 
CREATE TABLE etl.oceanfreightrate
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of entries to one upload job
    job_id varchar,
    -- tells what to do with this entry (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    rate decimal,
    transittime decimal,
    inlandcost decimal,
    inlandcostincluded boolean,
    inlandtransport varchar,
    thc decimal,
    thcincluded boolean,
    preference boolean,
    comment varchar,
    administrationcomment varchar,
    carriertraderoute_id bigint,
    containersize_id bigint,
    containermovement_id bigint,
    inlandcostcurrency_id bigint,
    oceanfreightcurrency_id bigint,
    thccurrency_id bigint,
    
    CONSTRAINT PK_oceanfreightrate PRIMARY KEY (id)
);

ALTER TABLE etl.oceanfreightrate OWNER TO etl;

CREATE TABLE etl.carriertraderoute
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of entries to one upload job
    job_id varchar,
    -- tells what to do with this entry (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    frequency numeric,
    transshipment varchar,
    valid_from date,
    valid_to date,
    tradename_id bigint,
    provider_id bigint,
    portofloading_id bigint,
    portofdischarge_id bigint,
    
    CONSTRAINT PK_carriertraderoute PRIMARY KEY (id)
);

ALTER TABLE etl.carriertraderoute OWNER TO etl;
