CREATE TABLE carriertraderoute
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    frequency numeric,
    transshipment varchar,
    valid_from date,
    valid_to date,
    tradename_id bigint,
    provider_id bigint,
    portofloading_id bigint,
    portofdischarge_id bigint,

    CONSTRAINT PK_carriertraderoute PRIMARY KEY (id)
);

CREATE INDEX idx_carriertraderoute_uuid on carriertraderoute(uuid);

ALTER TABLE carriertraderoute ADD CONSTRAINT fk_carriertraderoute_tradename FOREIGN KEY (tradename_id) REFERENCES TradeName;
CREATE INDEX idx_carriertraderoute_tradename_id on carriertraderoute(tradename_id);

ALTER TABLE carriertraderoute ADD CONSTRAINT fk_carriertraderoute_provider FOREIGN KEY (provider_id) REFERENCES BusinessThirdPartyGroup;
CREATE INDEX idx_carriertraderoute_provider_id on carriertraderoute(provider_id);

ALTER TABLE carriertraderoute ADD CONSTRAINT fk_carriertraderoute_portofloading FOREIGN KEY (portofloading_id) REFERENCES Location;
CREATE INDEX idx_carriertraderoute_portofloading_id on carriertraderoute(portofloading_id);

ALTER TABLE carriertraderoute ADD CONSTRAINT fk_carriertraderoute_portofdischarge FOREIGN KEY (portofdischarge_id) REFERENCES Location;
CREATE INDEX idx_carriertraderoute_portofdischarge_id on carriertraderoute(portofdischarge_id);


CREATE TABLE oceanfreightrate
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    rate decimal,
    transittime decimal,
    inlandcost decimal,
    inlandcostincluded boolean,
    inlandtransport varchar,
    thc decimal,
    thcincluded boolean,
    preference boolean,
    comment varchar,
    administrationcomment varchar,
    carriertraderoute_id bigint,
    containersize_id bigint,
    containermovement_id bigint,
    inlandcostcurrency_id bigint,
    oceanfreightcurrency_id bigint,
    thccurrency_id bigint,

    CONSTRAINT PK_oceanfreightrate PRIMARY KEY (id)
);

CREATE INDEX idx_oceanfreightrate_uuid on oceanfreightrate(uuid);

ALTER TABLE oceanfreightrate ADD CONSTRAINT fk_oceanfreightrate_carriertraderoute FOREIGN KEY (carriertraderoute_id) REFERENCES CarrierTradeRoute;
CREATE INDEX idx_oceanfreightrate_carriertraderoute_id on oceanfreightrate(carriertraderoute_id);

ALTER TABLE oceanfreightrate ADD CONSTRAINT fk_oceanfreightrate_containersize FOREIGN KEY (containersize_id) REFERENCES ContainerSize;
CREATE INDEX idx_oceanfreightrate_containersize_id on oceanfreightrate(containersize_id);

ALTER TABLE oceanfreightrate ADD CONSTRAINT fk_oceanfreightrate_containermovement FOREIGN KEY (containermovement_id) REFERENCES ContainerMovement;
CREATE INDEX idx_oceanfreightrate_containermovement_id on oceanfreightrate(containermovement_id);

ALTER TABLE oceanfreightrate ADD CONSTRAINT fk_oceanfreightrate_inlandcostcurrency FOREIGN KEY (inlandcostcurrency_id) REFERENCES Currency;
CREATE INDEX idx_oceanfreightrate_inlandcostcurrency_id on oceanfreightrate(inlandcostcurrency_id);

ALTER TABLE oceanfreightrate ADD CONSTRAINT fk_oceanfreightrate_oceanfreightcurrency FOREIGN KEY (oceanfreightcurrency_id) REFERENCES Currency;
CREATE INDEX idx_oceanfreightrate_oceanfreightcurrency_id on oceanfreightrate(oceanfreightcurrency_id);

ALTER TABLE oceanfreightrate ADD CONSTRAINT fk_oceanfreightrate_thccurrency FOREIGN KEY (thccurrency_id) REFERENCES Currency;
CREATE INDEX idx_oceanfreightrate_thccurrency_id on oceanfreightrate(thccurrency_id);
