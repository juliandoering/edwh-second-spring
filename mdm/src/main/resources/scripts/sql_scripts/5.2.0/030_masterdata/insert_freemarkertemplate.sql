INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.created.subject',
  '[Booking Status] New Booking Status uploaded',
  'Booking Status created subject'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.updated.subject',
  '[Booking Status] New Version of a Booking Status uploaded',
  'Booking Status updated subject'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.created.unknown.subject',
  '[Booking Status] New Booking Status for unknown Group Company uploaded',
  'Booking Status created unknown Group Company subject'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.updated.unknown.subject',
  '[Booking Status] New version of a Booking Status for unknown Group Company uploaded',
  'Booking Status updated unknown Group Company subject'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.created.body',
  'Dear ${entity1.contactPersonName},
   a new Booking Status for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA.',
  'Booking Status created body'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.updated.body',
  'Dear ${entity1.contactPersonName},
   a new version of a Booking Status for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA.',
  'Booking Status updated body'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.created.unknown.body',
  'Dear ${entity1.contactPersonName},
   a new Booking Status for an unknown Group Company has been uploaded to INTTRA.',
  'Booking Status created unknown Group Company body'
);

INSERT INTO freemarkertemplate (template_key, template, description)
VALUES (
  'bookingstatus.updated.unknown.body',
  'Dear ${entity1.contactPersonName},
   a new version of a Booking Status for an unknown Group Company has been uploaded to INTTRA.',
  'Booking Status updated unknown Group Company body'
);