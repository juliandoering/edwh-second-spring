ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN job_id;
ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN update_operation;
ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN import_date;
ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN source;
ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN source_pk;
ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN mdm_id;
ALTER TABLE etl.bkngstatustransportdetail DROP COLUMN mdm_version;