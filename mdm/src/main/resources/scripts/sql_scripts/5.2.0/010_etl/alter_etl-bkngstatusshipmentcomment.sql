ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN job_id;
ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN update_operation;
ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN import_date;
ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN source;
ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN source_pk;
ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN mdm_id;
ALTER TABLE etl.bkngstatusshipmentcomment DROP COLUMN mdm_version;