DROP TABLE IF EXISTS etl.coffeequalitycommercialname;

CREATE TABLE etl.coffeequalitycommercialname
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    coffeequality_id bigint,
    commercialname varchar,
    groupcompany_id bigint,
    
    CONSTRAINT PK_coffeequalitycommercialname PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequalitycommercialname OWNER TO etl;
