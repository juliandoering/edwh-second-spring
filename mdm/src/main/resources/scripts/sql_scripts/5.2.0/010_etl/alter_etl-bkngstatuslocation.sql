ALTER TABLE etl.bkngstatuslocation DROP COLUMN job_id;
ALTER TABLE etl.bkngstatuslocation DROP COLUMN update_operation;
ALTER TABLE etl.bkngstatuslocation DROP COLUMN import_date;
ALTER TABLE etl.bkngstatuslocation DROP COLUMN source;
ALTER TABLE etl.bkngstatuslocation DROP COLUMN source_pk;
ALTER TABLE etl.bkngstatuslocation DROP COLUMN mdm_id;
ALTER TABLE etl.bkngstatuslocation DROP COLUMN mdm_version;