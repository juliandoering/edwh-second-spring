ALTER TABLE etl.bkngstatusequipment DROP COLUMN job_id;
ALTER TABLE etl.bkngstatusequipment DROP COLUMN update_operation;
ALTER TABLE etl.bkngstatusequipment DROP COLUMN import_date;
ALTER TABLE etl.bkngstatusequipment DROP COLUMN source;
ALTER TABLE etl.bkngstatusequipment DROP COLUMN source_pk;
ALTER TABLE etl.bkngstatusequipment DROP COLUMN mdm_id;
ALTER TABLE etl.bkngstatusequipment DROP COLUMN mdm_version;