ALTER TABLE etl.bookingstatus ADD COLUMN purchaseorder_number VARCHAR;

ALTER TABLE etl.bookingstatus ADD COLUMN filename VARCHAR;

ALTER TABLE etl.bookingstatus DROP COLUMN source_pk;