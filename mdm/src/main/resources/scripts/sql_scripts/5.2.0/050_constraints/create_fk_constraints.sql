-- Mapping Table futuremarket_futuremonth
ALTER TABLE futuremarket_futuremonth
  ADD CONSTRAINT fk_futuremarket_futuremonth_futuremarket FOREIGN KEY (futuremarket_id)
  REFERENCES futuremarket (id);

ALTER TABLE futuremarket_futuremonth
  ADD CONSTRAINT fk_futuremarket_futuremonth_futuremonth FOREIGN KEY (futuremonth_id)
  REFERENCES futuremonth (id);