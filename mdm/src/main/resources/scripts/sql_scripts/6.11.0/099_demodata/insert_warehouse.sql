INSERT INTO warehouse
(
  UUID, created_at, updated_at, 
  record_version, deleted, version, is_version, parent, 
  name, created_by, updated_by, latitude, longitude, iis_code, COMMENT,
  limit_value, limit_validity_from, limit_currency_id, owner_groupcompany_id, owner_businessthirdparty_id, postaladdress_id, limit_validity_to, active
)
VALUES
(
  '980a9f66-ae12-44b3-8e8d-6d81f4241758',
  TIMESTAMP '2019-03-11 15:00:00.000',
  TIMESTAMP '2019-03-11 15:00:00.000',
  0, FALSE, 0, FALSE, 0, 
  'HBT Warehouse', 'edwh_admin', 'edwh_admin', 53.5753, 10.0153, 42, 'This is a test warehouse in Hamburg', 
  1234, TIMESTAMP '2019-03-11 00:00:00.000', 4, null, 1, 1, TIMESTAMP '2020-01-01 00:00:00.000', TRUE);
  
  
  
insert into warehouse (
	UUID, created_at, updated_at, 
  record_version, deleted, version, is_version, parent, 
  name, created_by, updated_by, latitude, longitude, iis_code, COMMENT,
  limit_value, limit_validity_from, limit_currency_id, owner_groupcompany_id, owner_businessthirdparty_id, postaladdress_id, limit_validity_to, active
  ) 
  select UUID, created_at, updated_at, 
  record_version, deleted, version, true, id, 
  name, created_by, updated_by, latitude, longitude, iis_code, COMMENT,
  limit_value, limit_validity_from, limit_currency_id, owner_groupcompany_id, owner_businessthirdparty_id, postaladdress_id, limit_validity_to, active
  from warehouse where uuid = '980a9f66-ae12-44b3-8e8d-6d81f4241758';