-- Insert trades

INSERT INTO freightratetrade (UUID, name)
VALUES ( 'e977830a-2f97-11e9-b210-d663bd873d93', 'Brazil');

-- Insert loadings

insert into freightratetrade (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from freightratetrade where is_version is false;

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( 'aff8f342-2f98-11e9-b210-d663bd873d93', '1',
    (select min(id) from freightratetrade where uuid = 'e977830a-2f97-11e9-b210-d663bd873d93' and is_version is false) );

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( 'b9124dc0-2f98-11e9-b210-d663bd873d93', '2',
    (select min(id) from freightratetrade where uuid = 'e977830a-2f97-11e9-b210-d663bd873d93' and is_version is false) );

insert into freightrateloading (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name, freightratetrade_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name,
    (select min(freightratetrade.id) from freightratetrade where freightratetrade.parent = freightrateloading.freightratetrade_id)
  from freightrateloading where is_version is false;

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid = 'e65bfd40-f9d4-11e7-a9bb-99401455bd6c')
  from freightrateloading where uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93';

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid = 'e662d999-f9d4-11e7-a9bb-99401455bd6c')
  from freightrateloading where uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93';

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.si_unlocode = 'BRSSZ')
  from freightrateloading where uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93';

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid ='e66300a9-f9d4-11e7-a9bb-99401455bd6c')
  from freightrateloading where uuid = 'b9124dc0-2f98-11e9-b210-d663bd873d93';


INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '1e5e3f0a-2fac-11e9-b210-d663bd873d93',
  (select id from freightrateloading where is_version is false and uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93'),
  (select id from businessthirdparty where is_version is false and uuid = '0ea1d0f1-89bd-11e8-899d-0242ac120006') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '638d6888-2fb3-11e9-b210-d663bd873d93',
  (select id from freightrateloading where is_version is false and uuid = 'b9124dc0-2f98-11e9-b210-d663bd873d93'),
  (select id from businessthirdparty where is_version is false and uuid = '0ea1d0f1-89bd-11e8-899d-0242ac120006') );

insert into freightrateline (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, freightrateloading_id, provider_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id,
  (select min(freightrateloading.id) from freightrateloading where freightrateloading.is_version is true and freightrateloading.parent = freightrateline.freightrateloading_id),
  (select min(businessthirdparty.id) from businessthirdparty where businessthirdparty.is_version is true and businessthirdparty.parent = freightrateline.provider_id)
  from freightrateline where is_version is false;

-- add route

INSERT INTO freightratetraderoute ( UUID, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency, freightrateline_id, inlandcostcurrency_id, podlocation_id )
VALUES ( '90733602-2fb3-11e9-b210-d663bd873d93', 200, false, 'Hamburg-City', 27, 7,
  (select id from freightrateline where is_version is false and uuid = '1e5e3f0a-2fac-11e9-b210-d663bd873d93'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency, freightrateline_id, inlandcostcurrency_id, podlocation_id )
VALUES ( 'd6911a80-2fb5-11e9-b210-d663bd873d93', 210, false, 'Hamburg-City', 38, 7,
  (select id from freightrateline where is_version is false and uuid = '638d6888-2fb3-11e9-b210-d663bd873d93'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from location where is_version is false and si_unlocode = 'BRRIO') );

insert into freightratetraderoute (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency, freightrateline_id, inlandcostcurrency_id, podlocation_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency,
  (select min(freightrateline.id) from freightrateline where freightrateline.is_version is true and freightrateline.parent = freightratetraderoute.freightrateline_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightratetraderoute.inlandcostcurrency_id),
  (select min(location.id) from location where location.is_version is true and location.parent = freightratetraderoute.podlocation_id)
  from freightratetraderoute where is_version is false;


-- add rates

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '17667a9a-2fbc-11e9-b210-d663bd873d93', 1449, 100, true, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '90733602-2fb3-11e9-b210-d663bd873d93'),
  (select id from containersize where is_version is false and code = '20FTDC'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'a9661eca-2fbf-11e9-b210-d663bd873d93', 2249, 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'd6911a80-2fb5-11e9-b210-d663bd873d93'),
  (select id from containersize where is_version is false and code = '20FTDC'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

insert into freightrate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, thccurrency_id, oceanfreightcurrency_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, rate, thc, preference, valid_from, valid_to,
  (select min(freightratetraderoute.id) from freightratetraderoute where freightratetraderoute.is_version is true and freightratetraderoute.parent = freightrate.freightratetraderoute_id),
  (select min(containersize.id) from containersize where containersize.is_version is true and containersize.parent = freightrate.containersize_id),
  (select min(containermovement.id) from containermovement where containermovement.is_version is true and containermovement.parent = freightrate.containermovement_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.oceanfreightcurrency_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.thccurrency_id)
  from freightrate where is_version is false;
