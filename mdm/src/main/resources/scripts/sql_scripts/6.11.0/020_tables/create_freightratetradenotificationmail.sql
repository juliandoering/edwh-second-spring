CREATE TABLE mdm.freightratetradenotificationmail
(
   id                 bigserial   NOT NULL,
   uuid               varchar,
   created_at         timestamp   DEFAULT CURRENT_TIMESTAMP,
   updated_at         timestamp   DEFAULT CURRENT_TIMESTAMP,
   deleted            boolean     DEFAULT false NOT NULL,
   record_version     integer     DEFAULT 0,
   version            bigint      DEFAULT 0 NOT NULL,
   is_version         boolean     DEFAULT false NOT NULL,
   parent             bigint      DEFAULT 0 NOT NULL,
   created_by         varchar,
   updated_by         varchar,

   contactpersonname  varchar,
   emailaddress       varchar
);
