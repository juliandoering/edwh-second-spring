/*
 * Script to create ETL freight rate tables
 */
--changeset generated_af:1497621354 
CREATE TABLE etl.freightrate
(
    id bigserial,
    job_id varchar,

    -- business fields
    rate decimal,
    thc decimal,
    thcincluded boolean,
    preference boolean,
    comment varchar,
    valid_from date,
    valid_to date,
    freightratetraderoute_id bigint,
    containersize_id bigint,
    containermovement_id bigint,
    oceanfreightcurrency_id bigint,
    thccurrency_id bigint,

    CONSTRAINT PK_freightrate PRIMARY KEY (id)
);

ALTER TABLE etl.freightrate OWNER TO etl;

CREATE TABLE etl.freightratetrade
(
    id bigserial,
    job_id varchar,
    update_operation char(1),

    -- business fields
    name varchar,

    CONSTRAINT PK_freightratetrade PRIMARY KEY (id)
);

ALTER TABLE etl.freightratetrade OWNER TO etl;

CREATE TABLE etl.freightrateloading
(
    id bigserial,
    job_id varchar,

    -- business fields
    name varchar,
    freightratetrade_id bigint,

    CONSTRAINT PK_freightrateloading PRIMARY KEY (id)
);

ALTER TABLE etl.freightrateloading OWNER TO etl;

CREATE TABLE etl.freightrateloading_location
(
    id bigserial,
    job_id varchar,

    -- business fields
    freightrateloading_id bigint,
    location_id bigint,

    CONSTRAINT PK_freightrateloading_location PRIMARY KEY (id)
);

ALTER TABLE etl.freightrateloading_location OWNER TO etl;

CREATE TABLE etl.freightrateline
(
    id bigserial,
    job_id varchar,

    -- business fields
    provider_id bigint,
    freightrateloading_id bigint,

    CONSTRAINT PK_freightrateline PRIMARY KEY (id)
);

ALTER TABLE etl.freightrateline OWNER TO etl;

CREATE TABLE etl.freightratetraderoute
(
    id bigserial,
    job_id varchar,

    -- business fields
    portofdischarge varchar,
    inlandcost decimal,
    inlandcostincluded boolean,
    inlandtransport varchar,
    transittime numeric,
    transshipment varchar,
    frequency numeric,
    freightrateLine_id bigint,
    inlandcostcurrency_id bigint,
    podlocation_id bigint,

    CONSTRAINT PK_freightratetraderoute PRIMARY KEY (id)
);

ALTER TABLE etl.freightratetraderoute OWNER TO etl;
