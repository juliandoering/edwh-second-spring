-- Insert trades

INSERT INTO freightratetrade (UUID, name)
VALUES ( 'e977830a-2f97-11e9-b210-d663bd873d93', 'Brazil');

-- Insert loadings

insert into freightratetrade (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from freightratetrade where is_version is false;

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( 'aff8f342-2f98-11e9-b210-d663bd873d93', '1',
    (select min(id) from freightratetrade where uuid = 'e977830a-2f97-11e9-b210-d663bd873d93' and is_version is false) );

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( 'b9124dc0-2f98-11e9-b210-d663bd873d93', '2',
    (select min(id) from freightratetrade where uuid = 'e977830a-2f97-11e9-b210-d663bd873d93' and is_version is false) );

insert into freightrateloading (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name, freightratetrade_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name,
    (select min(freightratetrade.id) from freightratetrade where freightratetrade.parent = freightrateloading.freightratetrade_id)
  from freightrateloading where is_version is false;

-- add port of loadings

INSERT INTO location ( UUID, created_at, updated_at, deleted, record_version, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, created_by, updated_by, si_unlocode )
VALUES ( 'e65bfd40-f9d4-11e7-a9bb-99401455bd6c', TIMESTAMP '2018-01-15 10:18:54.522', TIMESTAMP '2018-01-15 10:18:54.522', FALSE, 0, 0, FALSE, 0, 'BR', 'IGI', 'Itaguaí', NULL, '2252S', '04346W', NULL, NULL, 'BRIGI' );

INSERT INTO location ( UUID, created_at, updated_at, deleted, record_version, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, created_by, updated_by, si_unlocode )
VALUES ( 'e662d999-f9d4-11e7-a9bb-99401455bd6c', TIMESTAMP '2018-01-15 10:18:54.846', TIMESTAMP '2018-01-15 10:18:54.846', FALSE, 0, 0, FALSE, 0, 'BR', 'RIO', 'Rio de Janeiro', NULL, NULL, NULL, NULL, NULL, 'BRRIO' );

INSERT INTO location ( UUID, created_at, updated_at, deleted, record_version, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, created_by, updated_by, si_unlocode )
VALUES ( 'e66300a9-f9d4-11e7-a9bb-99401455bd6c', TIMESTAMP '2018-01-15 10:18:54.858', TIMESTAMP '2018-01-15 10:18:54.858', FALSE, 0, 0, FALSE, 0, 'BR', 'VIX', 'Vitoria', NULL, NULL, NULL, NULL, NULL, 'BRVIX' );

insert into location (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, si_unlocode)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, country_code, location_code, name, location_type, latitude, longitude, si_unlocode from location
  where is_version is false
  and uuid in ('e65bfd40-f9d4-11e7-a9bb-99401455bd6c', 'e662d999-f9d4-11e7-a9bb-99401455bd6c', 'e66300a9-f9d4-11e7-a9bb-99401455bd6c');


INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid = 'e65bfd40-f9d4-11e7-a9bb-99401455bd6c')
  from freightrateloading where uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93';

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid = 'e662d999-f9d4-11e7-a9bb-99401455bd6c')
  from freightrateloading where uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93';

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.si_unlocode = 'BRSSZ')
  from freightrateloading where uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93';

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid ='e66300a9-f9d4-11e7-a9bb-99401455bd6c')
  from freightrateloading where uuid = 'b9124dc0-2f98-11e9-b210-d663bd873d93';

-- add Shipping Line

INSERT INTO businessthirdparty (uuid,created_at,updated_at,record_version,deleted,version,is_version,parent,legalname,legalnameinlatinalphabet,commercialname,acronym,
    businessorganisationtype_id,
    legaladdress_id,postaladdress_id,incoterm_id,
    presentation_of_documents_id,
    paymentterm_id,
    preferred_terms_and_conditions,si_legaladdresscountrycode)
VALUES ('0ea1d0f1-89bd-11e8-899d-0242ac120006',TIMESTAMP '2018-07-17 12:29:32.211',TIMESTAMP '2018-07-23 15:10:05.931',5,false,2,false,0,'Hapag Lloyd AG Area Germany',NULL,'Hapag Lloyd AG Area Germany','HAPAG',
    (select id from businessorganisationtype where is_version is false and code = 'LP'),
    NULL,NULL,NULL,
    (select id from presentation_of_documents where is_version is false and code = 'BANK'),
    (select id from paymentterm where is_version is false and code = 'CAD'),
    NULL,'DE');

insert into businessthirdparty (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, legalname, legalnameinlatinalphabet, commercialname, acronym, businessorganisationtype_id, legaladdress_id,postaladdress_id,incoterm_id, presentation_of_documents_id, paymentterm_id, preferred_terms_and_conditions,si_legaladdresscountrycode)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, legalname, legalnameinlatinalphabet, commercialname, acronym,
  (select min(businessorganisationtype.id) from businessorganisationtype where businessorganisationtype.is_version is true and businessorganisationtype.parent = businessthirdparty.businessorganisationtype_id),
  legaladdress_id,postaladdress_id,incoterm_id,
  (select min(presentation_of_documents.id) from presentation_of_documents where presentation_of_documents.is_version is true and presentation_of_documents.parent = businessthirdparty.presentation_of_documents_id),
  (select min(paymentterm.id) from paymentterm where paymentterm.is_version is true and paymentterm.parent = businessthirdparty.paymentterm_id),
  preferred_terms_and_conditions,si_legaladdresscountrycode
  from businessthirdparty where is_version is false and uuid in ('0ea1d0f1-89bd-11e8-899d-0242ac120006');

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '1e5e3f0a-2fac-11e9-b210-d663bd873d93',
  (select id from freightrateloading where is_version is false and uuid = 'aff8f342-2f98-11e9-b210-d663bd873d93'),
  (select id from businessthirdparty where is_version is false and uuid = '0ea1d0f1-89bd-11e8-899d-0242ac120006') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '638d6888-2fb3-11e9-b210-d663bd873d93',
  (select id from freightrateloading where is_version is false and uuid = 'b9124dc0-2f98-11e9-b210-d663bd873d93'),
  (select id from businessthirdparty where is_version is false and uuid = '0ea1d0f1-89bd-11e8-899d-0242ac120006') );

insert into freightrateline (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, freightrateloading_id, provider_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id,
  (select min(freightrateloading.id) from freightrateloading where freightrateloading.is_version is true and freightrateloading.parent = freightrateline.freightrateloading_id),
  (select min(businessthirdparty.id) from businessthirdparty where businessthirdparty.is_version is true and businessthirdparty.parent = freightrateline.provider_id)
  from freightrateline where is_version is false;

-- add route

INSERT INTO freightratetraderoute ( UUID, thc, inlandcost, inlandtransport, transittime, frequency, freightrateline_id, thccurrency_id, inlandcostcurrency_id )
VALUES ( '90733602-2fb3-11e9-b210-d663bd873d93', 100, 200, 'Hamburg-City', 27, 7,
  (select id from freightrateline where is_version is false and uuid = '1e5e3f0a-2fac-11e9-b210-d663bd873d93'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD') );

INSERT INTO freightratetraderoute ( UUID, thc, inlandcost, inlandtransport, transittime, frequency, freightrateline_id, thccurrency_id, inlandcostcurrency_id )
VALUES ( 'd6911a80-2fb5-11e9-b210-d663bd873d93', 110, 210, 'Hamburg-City', 38, 7,
  (select id from freightrateline where is_version is false and uuid = '638d6888-2fb3-11e9-b210-d663bd873d93'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD') );

insert into freightratetraderoute (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, thc, inlandcost, inlandtransport, transittime, frequency, freightrateline_id, thccurrency_id, inlandcostcurrency_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, thc, inlandcost, inlandtransport, transittime, frequency,
  (select min(freightrateline.id) from freightrateline where freightrateline.is_version is true and freightrateline.parent = freightratetraderoute.freightrateline_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightratetraderoute.thccurrency_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightratetraderoute.inlandcostcurrency_id)
  from freightratetraderoute where is_version is false;

  -- add port of discharge and transshipment

INSERT INTO freightratetraderoute_location_pod ( freightratetraderoute_id, location_id )
select
  id,
  (select min(location.id) from location where location.is_version = freightratetraderoute.is_version
   and location.si_unlocode = 'DEHAM')
  from freightratetraderoute where uuid in ('90733602-2fb3-11e9-b210-d663bd873d93', 'd6911a80-2fb5-11e9-b210-d663bd873d93');

INSERT INTO freightratetraderoute_location_ts ( freightratetraderoute_id, location_id )
select
  id,
  (select min(location.id) from location where location.is_version = freightratetraderoute.is_version
   and location.si_unlocode = 'BRRIO')
  from freightratetraderoute where uuid in ('d6911a80-2fb5-11e9-b210-d663bd873d93');

-- add rates

INSERT INTO freightrate ( UUID, rate, quantity, weightquantity, preference, valid_from, valid_to, freightratetraderoute_id, packing_id, weightunit_id, containersize_id, containermovement_id, oceanfreightcurrency_id )
VALUES ( '17667a9a-2fbc-11e9-b210-d663bd873d93', 1449, 1, 21.6, true, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '90733602-2fb3-11e9-b210-d663bd873d93'),
  (select id from packing where is_version is false and code = '1MTBB'), -- Bulk
  (select id from quantityunit where is_version is false and code = 'MTS'),
  (select id from containersize where is_version is false and code = '20FTDC'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, quantity, weightquantity, preference, valid_from, valid_to, freightratetraderoute_id, packing_id, weightunit_id, containersize_id, containermovement_id, oceanfreightcurrency_id )
VALUES ( 'a9661eca-2fbf-11e9-b210-d663bd873d93', 2249, 1, 21.6, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'd6911a80-2fb5-11e9-b210-d663bd873d93'),
  (select id from packing where is_version is false and code = '1MTBB'), -- Bulk
  (select id from quantityunit where is_version is false and code = 'MTS'),
  (select id from containersize where is_version is false and code = '20FTDC'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD')
);

insert into freightrate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, rate, quantity, weightquantity, preference, valid_from, valid_to, freightratetraderoute_id, packing_id, weightunit_id, containersize_id, containermovement_id, oceanfreightcurrency_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, rate, quantity, weightquantity, preference, valid_from, valid_to,
  (select min(freightratetraderoute.id) from freightratetraderoute where freightratetraderoute.is_version is true and freightratetraderoute.parent = freightrate.freightratetraderoute_id),
  (select min(packing.id) from packing where packing.is_version is true and packing.parent = freightrate.packing_id),
  (select min(quantityunit.id) from quantityunit where quantityunit.is_version is true and quantityunit.parent = freightrate.weightunit_id),
  (select min(containersize.id) from containersize where containersize.is_version is true and containersize.parent = freightrate.containersize_id),
  (select min(containermovement.id) from containermovement where containermovement.is_version is true and containermovement.parent = freightrate.containermovement_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.oceanfreightcurrency_id)
  from freightrate where is_version is false;
