INSERT INTO exchangerate (uuid,currency_from_id,currency_to_id,settlement_date,exchange_rate) 
VALUES (
    '9f49f2d8-25e6-11e9-af2f-0242c0a84008',
    (SELECT id FROM currency WHERE code = 'GBP' AND is_version is false),
    (SELECT id FROM currency WHERE code = 'RUB' AND is_version is false),
    date '2019-01-31',
    1.310869
);

insert into exchangerate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, currency_from_id, currency_to_id, settlement_date, exchange_rate) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, currency_from_id, currency_to_id, settlement_date, exchange_rate from exchangerate where is_version is false AND uuid = '9f49f2d8-25e6-11e9-af2f-0242c0a84008';