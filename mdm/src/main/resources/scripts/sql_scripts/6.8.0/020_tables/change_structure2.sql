ALTER TABLE freightratetrade ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightrateloading ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightrateloading_location ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightrateline ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightratetraderoute ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightratetraderoute_location_pod ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightratetraderoute_location_ts ALTER COLUMN created_by TYPE varchar;
ALTER TABLE freightrate ALTER COLUMN created_by TYPE varchar;

ALTER TABLE freightratetrade ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightrateloading ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightrateloading_location ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightrateline ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightratetraderoute ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightratetraderoute_location_pod ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightratetraderoute_location_ts ALTER COLUMN updated_by TYPE varchar;
ALTER TABLE freightrate ALTER COLUMN updated_by TYPE varchar;
