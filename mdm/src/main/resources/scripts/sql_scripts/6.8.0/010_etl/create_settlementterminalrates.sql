/*
 * Script to create etl.settlementterminalrate
 */
--changeset generated_af:1497621354 
CREATE TABLE etl.settlementterminalrate
(
    id bigserial,
    job_id varchar,
    update_operation char(1),

    -- business fields
	settlement_value numeric,
	settlement_date date,
    futures_month_id bigint,
    futures_market_id bigint,
    futures_year int,

    CONSTRAINT PK_settlementterminalrate PRIMARY KEY (id)
);

ALTER TABLE etl.settlementterminalrate OWNER TO etl;
