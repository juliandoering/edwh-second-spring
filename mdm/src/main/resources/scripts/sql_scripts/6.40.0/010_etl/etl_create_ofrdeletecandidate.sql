--changeset generated_af:1497621354 
drop table if exists etl.ofrdeletecandidate;

CREATE TABLE etl.ofrdeletecandidate
(
    id bigserial,
    
    -- technical fields
    job_id varchar,
    id_counter bigint,
    
    -- business fields
    mdm_freightrate_uuid varchar,
        
    CONSTRAINT PK_ofrdeletecandidate PRIMARY KEY (id)
);

ALTER TABLE etl.ofrdeletecandidate OWNER TO etl;
