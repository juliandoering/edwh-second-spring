UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear ${entity1.contactPersonName},<br>
		a new Booking confirmation for your contract "${entity0.purchaseOrderNumber!}" has been uploaded to INTTRA. The INTTRA reference number is ${entity0.inttraRefNumber}.

		<p>
		Carrier Response Type: <b>${entity0.transactionType}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity0.booker!></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity0.shipper!></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity0.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span>${(entity0.inttraRefNumber)!"No Data"}</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Contract Number</b></span>
					<br>
					<span>No Data</span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span>${entity0.blNumber!"No data"}</span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span>${entity0.purchaseOrderNumber}</span>
				</td>
				<td>
					<@party name="Booking Office" party=entity0.bookingOffice!></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity0.forwarder!></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity0.consignee!></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity0.contractParty!></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity0.mainNotifyParty!></@party>
					<#if entity0.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity0.firstAdditionalNotifyParty!></@party>
					</#if>
					<#if entity0.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity0.secondAdditionalNotifyParty!></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity0.freightPayerParty!></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s Code, Voyage</b></span>
					<br>
					<span>${entity0.carrier.name!"No data"}, ${entity0.mainTransportDetails.vesselName!"No data"}, ${entity0.mainTransportDetails.lloydsCode!"No data"}, ${entity0.mainTransportDetails.voyageNumber!"No data"}</span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span>${(entity0.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span>${(entity0.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span>${(entity0.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity0.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity0.mainTransportDetails></@plandetails>
					</td>
				</#if>
				<#if entity0.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity0.onCarriageTransportDetails></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list entity0.goodsDetails as cargo>
					<tr>
						<td>
							(${cargo.lineNumber!"No Data"}) "Unknown Packing"
						</td>
						<td>
							${cargo.numberOfPackages!"No Data"} ${cargo.packageTypeDescription!"Unknown Type"}
						</td>
						<td>
							${cargo.goodsDescription!"No Data"}
						</td>
						<td>
							${cargo.grossWeight!"No Data"} ${cargo.grossWeightType!"Unknown Type"}
						</td>
						<td>
							${cargo.grossVolume!"No Data"} ${cargo.grossVolumeType!"Unknown Type"}
						</td>
					</tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list entity0.equipments as equipment>
					<tr>
						<td>
							Container Number: No Data
							<br>
							<#if equipment.typeCode??>
								Type: ${equipment.typeCode!"No Data"}
								<br>
							</#if>
							<#if equipment.cargoMovementType??>
								Service Arrangement: ${equipment.cargoMovementType}
								<br>
							</#if>
							<#if equipment.haulageArrangement??>
								Haulage Arrangement: ${equipment.haulageArrangement}
								<br>
							</#if>
						</td>
						<td>-</td>
						<td>-</td>
					</tr>
				</#list>
			</table>
		</p>

		<#macro party name party>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><span>${party.identifierType}: ${party.identifier}</span><br></#if>
				<span>${party.name!""}<br></span>
				<span>${party.address!"N/A"}<br></span>
			</#if>
		</#macro>

		<#macro plandetails name details>
			<b>${name}</b>
				<br>
				<#if entity0.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode}</span>
				</#if>
				<#if entity0.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier}</span>
				</#if>
				<br>
				<#if entity0.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName}
				</#if>
				<#-- lloyd''s code + voyage number -->
				<br>
				<#if entity0.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity0.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
		</#macro>
	</body>
</html>'
WHERE template_key = 'bookingconfirmation.created.body';
