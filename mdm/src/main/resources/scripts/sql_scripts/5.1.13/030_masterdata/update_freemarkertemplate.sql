UPDATE freemarkertemplate SET template =
'There is a new contract

  buyer: ${(buyer.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(seller.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(seller.transactionReference)!"(no seller''s contract ID provided!)"}


  ${UriSelf0} '
WHERE template_key = 'contract.created.body.long';

UPDATE freemarkertemplate SET template =
'[Contract] Contract "${(party.transactionReference)!""}" (${(entity0.uuid)!"?"}) has been replaced.'
WHERE template_key = 'contract.replaced.subject';

UPDATE freemarkertemplate SET template =
'A contract has been replaced

  old UUID: ${(entity0.uuid)!"UUID is missing"}
  old link: ${(UriSelf0)!"Link is missing"}

  new UUID: ${(entity1.uuid)!"UUID is missing"}
  new link: ${(UriSelf1)!"Link is missing"}'
WHERE template_key = 'contract.replaced.body.long';

UPDATE freemarkertemplate SET template =
'[Contract] Contract ${(contract.buyer.transactionReference)!"(?)"} (${(contract.uuid?c)!"?"}) was updated.'
WHERE template_key = 'contract.update.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Contract] Contract ${(contract.seller.transactionReference)!"(?)"} (${(contract.uuid?c)!"?"}) was updated.'
WHERE template_key = 'contract.update.subject.seller';

UPDATE freemarkertemplate SET template =
'There are updates for contract

  buyer: ${(buyer.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(seller.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(contract.seller.transactionReference)!"(no seller''s contract ID provided!)"}

  ${UriSelf0} '
WHERE template_key = 'contract.update.body.long';

UPDATE freemarkertemplate SET template =
'[Contract] Bulk upload of contract "${(entity0.buyer.transactionReference)!""} (${(entity0.uuid)!"?"})" is not consistent.'
WHERE template_key = 'contract.bulk.upload.conflict.subject';