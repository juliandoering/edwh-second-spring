--changeset generated_nf:1497621354 
CREATE TABLE etl.bookingstatus
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    document_identifier varchar NULL , 
    date_timestamp timestamp NULL , 
    event_code varchar NULL ,
    booking_number varchar NULL ,
    bl_number varchar NULL ,
    
    CONSTRAINT PK_bookingstatus PRIMARY KEY (id)
);

ALTER TABLE etl.bookingstatus OWNER TO etl;
