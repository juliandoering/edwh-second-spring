--changeset generated_nf:1497621354 
CREATE TABLE etl.bkngstatustransportdetail
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of messages to one upload job
    job_id varchar,
    -- tells what to do with this message (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    conveyance_name varchar NULL ,
    voyage_trip_number varchar NULL ,
    carrier_scac varchar NULL ,
    transportation_identification varchar NULL ,
    transportat_identification_type varchar NULL ,

    booking_status_id bigint NOT NULL , 
    
    CONSTRAINT PK_bkngstatustransportdetail PRIMARY KEY (id)
);

ALTER TABLE etl.bkngstatustransportdetail OWNER TO etl;
