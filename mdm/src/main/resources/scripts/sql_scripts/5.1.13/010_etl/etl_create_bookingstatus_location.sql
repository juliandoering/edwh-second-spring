--changeset generated_nf:1497621354 
CREATE TABLE etl.bkngstatuslocation
(
	id bigserial NOT NULL,

    -- technical fields
    -- aggregates a number of messages to one upload job
    job_id varchar,
    -- tells what to do with this message (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,

    -- business fields
   type                                     varchar,
   status                                   varchar,
   unLocationCode                           varchar,
   dateTimeType                             varchar,
   dateTime                                 timestamp,
   bkngstatus_details_id                    bigint,

    CONSTRAINT PK_bkngstatuslocation PRIMARY KEY (id)
   );

ALTER TABLE etl.bkngstatuslocation OWNER TO etl;
