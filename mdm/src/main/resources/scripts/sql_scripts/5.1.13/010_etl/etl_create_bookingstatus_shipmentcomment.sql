--changeset generated_nf:1497621354 
CREATE TABLE etl.bkngstatusshipmentcomment
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    shipment_comment varchar NULL ,
    shipment_comment_type varchar NULL ,

    booking_status_id bigint NOT NULL , 
    
    CONSTRAINT PK_bkngstatusshipmentcomment PRIMARY KEY (id)
);

ALTER TABLE etl.bkngstatusshipmentcomment OWNER TO etl;
