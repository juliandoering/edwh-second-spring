DROP TABLE freightratetraderoute_location;
DROP TABLE freightrate;
DROP TABLE freightratetraderoute;
DROP TABLE freightratetrade;

CREATE TABLE freightratetrade
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    name varchar,

    CONSTRAINT PK_freightratetrade PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetrade_uuid on freightratetrade(uuid);

CREATE TABLE freightrateloading
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    name varchar,
    freightratetrade_id bigint,

    CONSTRAINT PK_freightrateloading PRIMARY KEY (id)
);

CREATE INDEX idx_freightrateloading_uuid on freightrateloading(uuid);

CREATE TABLE freightrateloading_location
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    freightrateloading_id bigint,
    location_id bigint,

    CONSTRAINT PK_freightrateloading_location PRIMARY KEY (id)
);

CREATE INDEX idx_freightrateloading_location_uuid on freightrateloading_location(uuid);

CREATE TABLE freightrateline
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    freightrateloading_id bigint,
    provider_id bigint,

    CONSTRAINT PK_freightrateline PRIMARY KEY (id)
);

CREATE INDEX idx_freightrateline_uuid on freightrateline(uuid);

CREATE TABLE freightratetraderoute
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    thc decimal,
    inlandcost decimal,
    inlandtransport varchar,
    transittime numeric,
    frequency numeric,
    freightrateLine_id bigint,
    thccurrency_id bigint,
    inlandcostcurrency_id bigint,

    CONSTRAINT PK_freightratetraderoute PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetraderoute_uuid on freightratetraderoute(uuid);

CREATE TABLE freightratetraderoute_location_pod
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    freightratetraderoute_id bigint,
    location_id bigint,

    CONSTRAINT PK_freightratetraderoute_location_pod PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetraderoute_location_pod_uuid on freightratetraderoute_location_pod(uuid);

CREATE TABLE freightratetraderoute_location_ts
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    freightratetraderoute_id bigint,
    location_id bigint,

    CONSTRAINT PK_freightratetraderoute_location_ts PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetraderoute_location_ts_uuid on freightratetraderoute_location_ts(uuid);

CREATE TABLE freightrate
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by bigint,
    updated_at timestamp default current_timestamp,
    updated_by bigint,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    rate decimal,
    quantity decimal,
    weightquantity decimal,
    preference boolean,
    valid_from date,
    valid_to date,
    freightratetraderoute_id bigint,
    packing_id bigint,
    weightunit_id bigint,
    containersize_id bigint,
    containermovement_id bigint,
    oceanfreightcurrency_id bigint,

    CONSTRAINT PK_freightrate PRIMARY KEY (id)
);

CREATE INDEX idx_freightrate_uuid on freightrate(uuid);




ALTER TABLE freightrateloading ADD CONSTRAINT fk_freightrateloading_freightratetrade FOREIGN KEY (freightratetrade_id) REFERENCES FreightRateTrade;
CREATE INDEX idx_freightrateloading_freightratetrade_id on freightrateloading(freightratetrade_id);


ALTER TABLE freightrateloading_location ADD CONSTRAINT fk_freightrateloading_location_freightrateloading FOREIGN KEY (freightrateloading_id) REFERENCES FreightRateLoading;
CREATE INDEX idx_freightrateloading_location_freightrateloading_id on freightrateloading_location(freightrateloading_id);

ALTER TABLE freightrateloading_location ADD CONSTRAINT fk_freightrateloading_location_location FOREIGN KEY (location_id) REFERENCES Location;
CREATE INDEX idx_freightrateloading_location_location_id on freightrateloading_location(location_id);


ALTER TABLE freightrateline ADD CONSTRAINT fk_freightrateline_freightrateloading FOREIGN KEY (freightrateloading_id) REFERENCES FreightRateLoading;
CREATE INDEX idx_freightrateline_freightrateloading_id on freightrateline(freightrateloading_id);

ALTER TABLE freightrateline ADD CONSTRAINT fk_freightrateline_provider FOREIGN KEY (provider_id) REFERENCES BusinessThirdParty;
CREATE INDEX idx_freightrateline_provider_id on freightrateline(provider_id);


ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_freightrateLine FOREIGN KEY (freightrateLine_id) REFERENCES FreightRateLine;
CREATE INDEX idx_freightratetraderoute_freightrateLine_id on freightratetraderoute(freightrateLine_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_thccurrency FOREIGN KEY (thccurrency_id) REFERENCES Currency;
CREATE INDEX idx_freightratetraderoute_thccurrency_id on freightratetraderoute(thccurrency_id);

ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_inlandcostcurrency FOREIGN KEY (inlandcostcurrency_id) REFERENCES Currency;
CREATE INDEX idx_freightratetraderoute_inlandcostcurrency_id on freightratetraderoute(inlandcostcurrency_id);


ALTER TABLE freightratetraderoute_location_pod ADD CONSTRAINT fk_freightratetraderoute_location_pod_freightratetraderoute FOREIGN KEY (freightratetraderoute_id) REFERENCES FreightRateTradeRoute;
CREATE INDEX idx_freightratetraderoute_location_pod_freightratetraderoute_id on freightratetraderoute_location_pod(freightratetraderoute_id);

ALTER TABLE freightratetraderoute_location_pod ADD CONSTRAINT fk_freightratetraderoute_location_pod_location FOREIGN KEY (location_id) REFERENCES Location;
CREATE INDEX idx_freightratetraderoute_location_pod_location_id on freightratetraderoute_location_pod(location_id);


ALTER TABLE freightratetraderoute_location_ts ADD CONSTRAINT fk_freightratetraderoute_location_ts_freightratetraderoute FOREIGN KEY (freightratetraderoute_id) REFERENCES FreightRateTradeRoute;
CREATE INDEX idx_freightratetraderoute_location_ts_freightratetraderoute_id on freightratetraderoute_location_ts(freightratetraderoute_id);

ALTER TABLE freightratetraderoute_location_ts ADD CONSTRAINT fk_freightratetraderoute_location_ts_location FOREIGN KEY (location_id) REFERENCES Location;
CREATE INDEX idx_freightratetraderoute_location_ts_location_id on freightratetraderoute_location_ts(location_id);


ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_freightratetraderoute FOREIGN KEY (freightratetraderoute_id) REFERENCES FreightRateTradeRoute;
CREATE INDEX idx_freightrate_freightratetraderoute_id on freightrate(freightratetraderoute_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_packing FOREIGN KEY (packing_id) REFERENCES Packing;
CREATE INDEX idx_freightrate_packing_id on freightrate(packing_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_weightunit FOREIGN KEY (weightunit_id) REFERENCES QuantityUnit;
CREATE INDEX idx_freightrate_weightunit_id on freightrate(weightunit_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_containersize FOREIGN KEY (containersize_id) REFERENCES ContainerSize;
CREATE INDEX idx_freightrate_containersize_id on freightrate(containersize_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_containermovement FOREIGN KEY (containermovement_id) REFERENCES ContainerMovement;
CREATE INDEX idx_freightrate_containermovement_id on freightrate(containermovement_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_oceanfreightcurrency FOREIGN KEY (oceanfreightcurrency_id) REFERENCES Currency;
CREATE INDEX idx_freightrate_oceanfreightcurrency_id on freightrate(oceanfreightcurrency_id);
