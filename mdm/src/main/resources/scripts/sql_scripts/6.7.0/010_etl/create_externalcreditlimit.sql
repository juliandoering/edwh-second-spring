/*
 * Script to create etl.externalcreditlimit
 */
--changeset generated_af:1497621354 
CREATE TABLE etl.externalcreditlimit
(
    id bigserial,
    job_id varchar,
    
    -- business fields
    businessthirdparty_id bigint,
    insurer_id bigint,
    insurer_country_id varchar,
    currency_id bigint,
    insurerid varchar,
    amount numeric,
    valid_from date,
    valid_to date,
    
    CONSTRAINT PK_externalcreditlimit PRIMARY KEY (id)
);

ALTER TABLE etl.externalcreditlimit OWNER TO etl;
