ALTER TABLE mdm.oceanfreightrate ADD COLUMN place_of_receipt_id bigint;
ALTER TABLE mdm.oceanfreightrate ADD COLUMN place_of_delivery_id bigint;

ALTER TABLE mdm.oceanfreightrate ADD CONSTRAINT fk_place_of_receipt_location FOREIGN KEY (place_of_receipt_id) REFERENCES location;
CREATE INDEX idx_place_of_receipt_location_id on oceanfreightrate(place_of_receipt_id);

ALTER TABLE mdm.oceanfreightrate ADD CONSTRAINT fk_place_of_delivery_location FOREIGN KEY (place_of_delivery_id) REFERENCES location;
CREATE INDEX idx_place_of_delivery_location_id on oceanfreightrate(place_of_delivery_id);
