ALTER TABLE etl.oceanfreightrate ADD COLUMN place_of_receipt_id bigint;
ALTER TABLE etl.oceanfreightrate ADD COLUMN place_of_delivery_id bigint;
