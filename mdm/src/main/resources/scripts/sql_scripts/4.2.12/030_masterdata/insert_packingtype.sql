-- Insert Initial
INSERT INTO mdm.packing (uuid,code,packingtype,extended,created_by,updated_by)
VALUES ('29fe1e8d-1c87-4fed-89ad-b7594cf1bb5b','0.1KGB_PPB','0.1kg PP Bag','0.1 kg Polypropylene Bag', (SELECT id FROM userprofile WHERE username = 'edwh_admin'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'));

INSERT INTO mdm.packing (uuid,code,packingtype,extended,created_by,updated_by)
VALUES ('d91a3baa-5a48-4a90-a77d-596d0bd6f5ee','0.12KGB_PPB','0.12kg PP Bag','0.12 kg Polypropylene Bag', (SELECT id FROM userprofile WHERE username = 'edwh_admin'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'));

-- Insert Version
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version,parent,created_by,updated_by)
VALUES ('29fe1e8d-1c87-4fed-89ad-b7594cf1bb5b','0.1KGB_PPB','0.1kg PP Bag','0.1 kg Polypropylene Bag', true, 
	(SELECT id FROM mdm.packing WHERE code = '0.1KGB_PPB'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'));
	
INSERT INTO mdm.packing (uuid,code,packingtype,extended,is_version,parent,created_by,updated_by)
VALUES ('d91a3baa-5a48-4a90-a77d-596d0bd6f5ee','0.12KGB_PPB','0.12kg PP Bag','0.12 kg Polypropylene Bag', true, 
	(SELECT id FROM mdm.packing WHERE code = '0.12KGB_PPB'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'));