INSERT INTO samplehandling (uuid, sastype, created_by, updated_by)
VALUES ('db7f745a-7d3d-44b8-b7f8-fc8fbdd6769e', 'PSS, Pre-Shipment Sample', (SELECT id FROM userprofile WHERE username = 'edwh_admin'), (SELECT id FROM userprofile WHERE username = 'edwh_admin'));

INSERT INTO samplehandling (uuid, version, is_version, parent, sastype, created_by, updated_by)
  SELECT
    uuid,
    version,
    true,
    id,
    sastype,
    created_by,
    updated_by
  FROM samplehandling
  WHERE is_version IS false AND sastype = 'PSS, Pre-Shipment Sample';