REVOKE ALL PRIVILEGES ON schema mdm FROM dwh_reporting;

GRANT USAGE ON SCHEMA mdm to dwh_reporting;
GRANT SELECT ON ALL TABLES IN SCHEMA mdm TO dwh_reporting;