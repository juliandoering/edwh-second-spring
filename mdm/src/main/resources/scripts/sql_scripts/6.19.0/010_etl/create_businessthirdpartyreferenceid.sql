CREATE TABLE etl.businessthirdpartyreferenceid
(
    id bigserial,
    job_id varchar,

    -- business fields
    referenceid varchar,
    businessthirdpartyreferenceidtype_code varchar,
    businessthirdpartyreferenceidtype_mdm_id bigint,
    supplier_groupcompany_tagetik_code varchar,
    supplier_groupcompany_mdm_id bigint,
    supplier_businessthirdparty_mdm_id bigint,
    supplier_businessthirdparty_legalname varchar,
    supplier_businessthirdparty_country_code varchar,
    parent_businessthirdparty_id bigint,

    CONSTRAINT pk_businessthirdpartyreferenceid PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartyreferenceid OWNER TO etl;
