CREATE TABLE businessthirdpartyreferenceid
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    referenceid varchar,
    businessthirdpartyreferenceidtype_id bigint,
    supplier_groupcompany_id bigint,
    supplier_businessthirdparty_id bigint,
    parent_businessthirdparty_id bigint,

    CONSTRAINT pk_businessthirdpartyreferenceid PRIMARY KEY (id)
);

CREATE INDEX idx_businessthirdpartyreferenceid_uuid on businessthirdpartyreferenceid(uuid);

ALTER TABLE businessthirdpartyreferenceid ADD CONSTRAINT fk_businessthirdpartyreferenceid_groupcompany FOREIGN KEY (supplier_groupcompany_id) REFERENCES groupcompany;
CREATE INDEX idx_businessthirdpartyreferenceid_supplier_groupcompany_id on businessthirdpartyreferenceid(supplier_groupcompany_id);

ALTER TABLE businessthirdpartyreferenceid ADD CONSTRAINT fk_businessthirdpartyreferenceid_businessthirdparty FOREIGN KEY (supplier_businessthirdparty_id) REFERENCES businessthirdparty;
CREATE INDEX idx_businessthirdpartyreferenceid_supplier_businessthirdparty_id on businessthirdpartyreferenceid(supplier_businessthirdparty_id);