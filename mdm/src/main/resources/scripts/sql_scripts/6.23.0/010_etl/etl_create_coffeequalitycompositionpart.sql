CREATE TABLE etl.coffeequalitycompositionpart
(
   id                                       bigserial   NOT NULL,
   job_id                                   varchar,
   coffeequality_id                         bigint,
   -- Business Fields
   mdm_coffeequality_id                     bigint,
   factor                                   numeric,
   
   CONSTRAINT pk_coffeequalitycompositionpart PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequalitycompositionpart OWNER TO etl;