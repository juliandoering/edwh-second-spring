ALTER TABLE groupcompany_bookingconfirmationmails ADD COLUMN contactpersonname VARCHAR;

UPDATE groupcompany_bookingconfirmationmails SET contactpersonname = CONCAT_WS(' ', (SELECT first_name FROM individual where individual.id = groupcompany_bookingconfirmationmails.contactperson_id), (SELECT last_name FROM individual where individual.id = groupcompany_bookingconfirmationmails.contactperson_id));

ALTER TABLE groupcompany_bookingconfirmationmails DROP COLUMN contactperson_id;