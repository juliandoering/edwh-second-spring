UPDATE freemarkertemplate SET template =
'A business third party was updated

  ${(entity0.legalName)!""}
  <#if entity1??>
    ${entity1.addressLine1!""}
    ${entity1.addressLine2!""}
    ${entity1.postalCode!""}
    ${entity1.location.unLocode!""}
    </#if>
  ${UriSelf0}'
WHERE template_key = 'businessthirdparty.updated.body';

UPDATE freemarkertemplate SET template =
'A business third party was created

    ${(entity0.legalName)!""}
  <#if entity1??>
    ${entity1.addressLine1!""}
    ${entity1.addressLine2!""}
    ${entity1.postalCode!""}
    ${entity1.location.unLocode!""}
    </#if>
  ${UriSelf0}'
WHERE template_key = 'businessthirdparty.created.body';

UPDATE freemarkertemplate SET template =
'A business third party was deleted

   ${(entity0.legalName)!""}
  <#if entity1??>
    ${entity1.addressLine1!""}
    ${entity1.addressLine2!""}
    ${entity1.postalCode!""}
    ${entity1.location.unLocode!""}
    </#if>
  ${UriSelf0}'
WHERE template_key = 'businessthirdparty.deleted.body';

UPDATE freemarkertemplate SET template =
'There is a new contract

  buyer: ${(contract.buyer.identification.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(contract.buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(contract.seller.identification.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(contract.seller.transactionReference)!"(no seller''s contract ID provided!)"}


  ${UriSelf0} '
WHERE template_key = 'contract.created.body.long';

UPDATE freemarkertemplate SET template =
'There are updates for contract

  buyer: ${(contract.buyer.identification.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(contract.buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(contract.seller.identification.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(contract.seller.transactionReference)!"(no seller''s contract ID provided!)"}


  ${UriSelf0} ',
  template_key = 'contract.updated.body.long'
WHERE template_key = 'contract.update.body.long';



UPDATE freemarkertemplate SET template =
'[Contract] Download of contract "${(entity0.seller.transactionReference)!""} (${(entity0.longId?c)!"?"})" has been confirmed.'
WHERE template_key = 'contract.download.confirmation.seller.subject';

UPDATE freemarkertemplate SET template =
'[Contract] Download of contract "${(entity0.buyer.transactionReference)!""} (${(entity0.longId?c)!"?"})" has been confirmed.'
WHERE template_key = 'contract.download.confirmation.buyer.subject';

UPDATE freemarkertemplate SET template =
'The contract download has been confirmed

  ${UriSelf0} '
WHERE template_key = 'contract.download.confirmation.body';

UPDATE freemarkertemplate SET template =
'[Contract] Bulk upload of contract "${(entity0.buyer.transactionReference)!""} (${(entity0.longId?c)!"?"})" is not consistent.'
WHERE template_key = 'contract.bulk.upload.conflict.subject';

UPDATE freemarkertemplate SET template =
'The contract has been uploaded by the second counterpart but the content differs.

  ${UriSelf0} '
WHERE template_key = 'contract.bulk.upload.conflict.body';

UPDATE freemarkertemplate SET template =
'The contract that was uploaded contains unexpected but still acceptable data.

  ${UriSelf0} ' ||
  'Please consider the following warning(s):'
WHERE template_key = 'contract.created.warning.body';

UPDATE freemarkertemplate SET template =
'New fixation for contract "${entity0.contract.buyer.transactionReference}" created.'
WHERE template_key = 'fixation.created.subject.buyer';

UPDATE freemarkertemplate SET template =
'New fixation for contract "${entity0.contract.seller.transactionReference}" created.'
WHERE template_key = 'fixation.created.subject.seller';

UPDATE freemarkertemplate SET template =
'There is a new price fixation:

  ${UriSelf0} '
WHERE template_key = 'fixation.created.subject.seller';

UPDATE freemarkertemplate SET template =
'The download of fixation with number ${entity0.fixationNumber} for contract "${contract.buyer.transactionReference}" has been confirmed.'
WHERE template_key = 'fixation.download.confirmation.subject.buyer';

UPDATE freemarkertemplate SET template =
'The download of fixation with number ${entity0.fixationNumber} for contract "${contract.seller.transactionReference}" has been confirmed.'
WHERE template_key = 'fixation.download.confirmation.subject.seller';

UPDATE freemarkertemplate SET template =
'The price fixation has been downloaded successfully:

  ${UriSelf0} '
WHERE template_key = 'fixation.download.confirmation.body';

UPDATE freemarkertemplate SET template =
'[Price Fixation] Price Fixation "${contract.buyer.transactionReference}" already uploaded'
WHERE template_key = 'fixation.upload.existed.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Price Fixation] Price Fixation "${contract.seller.transactionReference}" already uploaded'
WHERE template_key = 'fixation.upload.existed.subject.seller';

UPDATE freemarkertemplate SET template =
'This Price Fixation was uploaded was already uploaded by the counterpart

  ${UriSelf0}'
WHERE template_key = 'fixation.upload.existed.body';

UPDATE freemarkertemplate SET template =
 '[Price Fixation] Uploaded Price Fixation "${contract.buyer.transactionReference}" in conflict' ||
  ' with already uploaded Price Fixation'
WHERE template_key = 'fixation.upload.conflict.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Price Fixation] Uploaded Price Fixation "${contract.seller.transactionReference}" in conflict' ||
  ' with already uploaded Price Fixation'
WHERE template_key = 'fixation.upload.conflict.subject.seller';

UPDATE freemarkertemplate SET template =
'The Price Fixation that was uploaded is not identical with previously uploaded Price Fixations. Please resolve' ||
  'this conflict and adjust the data if necessary

  ${UriSelf0}'
WHERE template_key = 'fixation.upload.conflict.body';

UPDATE freemarkertemplate SET template =
'[Shipment] New shipping instructions "${entity0.shippingInstructionNumber}" created'
WHERE template_key = 'shipping.instructions.created.subject.seller';

UPDATE freemarkertemplate SET template =
'[Shipment] New shipping instructions "${entity0.shippingInstructionNumber}" created'
WHERE template_key = 'shipping.instructions.created.subject.buyer';

UPDATE freemarkertemplate SET template =
'A new shipping instructions has been created

  ${UriSelf0}'
WHERE template_key = 'shipping.instructions.created.body';

UPDATE freemarkertemplate SET template =
'[Shipping] Shipping instructions "${entity0.shippingInstructionNumber}" updated'
WHERE template_key = 'shipping.instructions.updated.subject.seller';

UPDATE freemarkertemplate SET template =
'[Shipping] Shipping instructions "${entity0.shippingInstructionNumber}" updated'
WHERE template_key = 'shipping.instructions.updated.subject.buyer';

UPDATE freemarkertemplate SET template =
'A shipping instructions was updated

  ${UriSelf0}'
WHERE template_key = 'shipping.instructions.updated.subject.buyer';

UPDATE freemarkertemplate SET template =
'The download of shipping instructions with number ${entity0.shippingInstructionNumber} for contract "${contract.buyer.transactionReference}" has been confirmed.'
WHERE template_key = 'shipping.instructions.download.confirmation.subject.buyer';

UPDATE freemarkertemplate SET template =
'The download of shipping instructions with number ${entity0.shippingInstructionNumber} for contract "${contract.seller.transactionReference}" has been confirmed.'
WHERE template_key = 'shipping.instructions.download.confirmation.subject.seller';

UPDATE freemarkertemplate SET template =
'The shipping instructions have been downloaded successfully:

  ${UriSelf0} '
WHERE template_key = 'shipping.instructions.download.confirmation.body';

UPDATE freemarkertemplate SET template =
'The shipping instruction with number ${entity0.shippingInstructionNumber} for contract "${contract.buyer.transactionReference}" already exists'
WHERE template_key = 'shipping.instructions.upload.existed.subject.buyer';

UPDATE freemarkertemplate SET template =
'The shipping instruction with number ${entity0.shippingInstructionNumber} for contract "${contract.seller.transactionReference}" already exists'
WHERE template_key = 'shipping.instructions.upload.existed.subject.seller';

UPDATE freemarkertemplate SET template =
'The shipping instruction you have uploaded already exists:

  ${UriSelf0} '
WHERE template_key = 'shipping.instructions.upload.existed.body';

UPDATE freemarkertemplate SET template =
'Uploaded shipping instruction ${entity0.shippingInstructionNumber} for contract "${contract.buyer.transactionReference}" in conflict'
WHERE template_key = 'shipping.instructions.upload.conflict.subject.buyer';

UPDATE freemarkertemplate SET template =
'Uploaded shipping instruction ${entity0.shippingInstructionNumber} for contract "${contract.seller.transactionReference}" in conflict'
WHERE template_key = 'shipping.instructions.upload.conflict.subject.seller';

UPDATE freemarkertemplate SET template =
'The shipping instruction you have is in conflict with another shipping instruction that was uploaded previously:

  ${UriSelf0} '
WHERE template_key = 'shipping.instructions.upload.conflict.body';

UPDATE freemarkertemplate SET template =
'A new shipment advice has been created

  ${UriSelf0}  '
WHERE template_key = 'shipmentadvice.created.body';

UPDATE freemarkertemplate SET template =
'A shipment advice was updated

  ${UriSelf0}'
WHERE template_key = 'shipmentadvice.updated.body';

UPDATE freemarkertemplate SET template =
'The download of shipment advice with number ${entity0.sequentialNumber} for contract "${contract.buyer.transactionReference}" has been confirmed.'
WHERE template_key = 'shipmentadvice.download.confirmation.subject.buyer';

UPDATE freemarkertemplate SET template =
'The download of shipment advice with number ${entity0.sequentialNumber} for contract "${contract.seller.transactionReference}" has been confirmed.'
WHERE template_key = 'shipmentadvice.download.confirmation.subject.seller';

UPDATE freemarkertemplate SET template =
'The shipment advice download has been confirmed

${UriSelf0} '
WHERE template_key = 'shipmentadvice.download.confirmation.body';

UPDATE freemarkertemplate SET template =
'[Shipment] Uploaded Shipment Advice "${contract.buyer.transactionReference}" in conflict' ||
  ' with already uploaded Shipment Advice'
WHERE template_key = 'shipmentadvice.upload.conflict.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Shipment] Uploaded Shipment Advice "${contract.seller.transactionReference}" in conflict' ||
  ' with already uploaded Shipment Advice'
WHERE template_key = 'shipmentadvice.upload.conflict.subject.seller';

UPDATE freemarkertemplate SET template =
'The Shipment Advices that was uploaded is not identical with previously uploaded Shipment Advices. Please resolve' ||
  'this conflict and adjust the data if necessary

  ${UriSelf0}'
WHERE template_key = 'shipmentadvice.upload.conflict.body';

UPDATE freemarkertemplate SET template =
'[Shipment] Shipment Advice "${contract.buyer.transactionReference}" already uploaded'
WHERE template_key = 'shipmentadvice.upload.existed.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Shipment] Shipment Advice "${contract.seller.transactionReference}" already uploaded'
WHERE template_key = 'shipmentadvice.upload.existed.subject.seller';

UPDATE freemarkertemplate SET template =
'The Shipment Advice that was uploaded was already uploaded by the counterpart

  ${UriSelf0}'
WHERE template_key = 'shipmentadvice.upload.existed.body';

UPDATE freemarkertemplate set template = 'A business third party entry was deleted

  ${(entity0.legalName)!""}'
WHERE uuid = '12110030';

UPDATE freemarkertemplate set template = 'A business third party was updated

  ${(entity0.legalName)!""}
  ${UriSelf0}'
WHERE uuid = '12106030';

UPDATE freemarkertemplate set template = 'A business third party entry was created

  ${(entity0.legalName)!""}
  ${UriSelf0}'
WHERE uuid = '12107030';

UPDATE freemarkertemplate SET template =
'Dear ${entity1},
 a new Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. This Booking Confirmation is ' ||
   'missing a Purchase Order Number and can therefore not be automatically assigned to a Group Company. The INTTRA reference ' ||
    'number is ${entity0.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.created.unknown.body';

UPDATE freemarkertemplate SET template =
'Dear ${entity1},
  an update for an existing Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. ' ||
   'This Booking Confirmation is missing a Purchase Order Number and can therefore not be automatically assigned' ||
    ' to a Group Company. The INTTRA reference number is ${entity1.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.updated.unknown.body';

UPDATE freemarkertemplate SET template =
'[Booking Confirmation] New Booking Confirmation for ${entity1.groupCompany.legalName}'
WHERE template_key = 'bookingconfirmation.created.subject';

UPDATE freemarkertemplate SET template =
'Dear ${entity1},
  a new Booking confirmation for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA reference number' ||
   ' is ${entity1.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.created.body';

UPDATE freemarkertemplate SET template =
'[Booking Confirmation] Update of a Booking Confirmation for ${entity1.groupCompany.legalName}'
WHERE template_key = 'bookingconfirmation.updated.subject';

UPDATE freemarkertemplate SET template =
'Dear ${entity1,
 a new version of a Booking confirmation for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA reference number' ||
   ' is ${entity1.inttraRefNumber}.'
WHERE template_key = 'bookingconfirmation.updated.subject';