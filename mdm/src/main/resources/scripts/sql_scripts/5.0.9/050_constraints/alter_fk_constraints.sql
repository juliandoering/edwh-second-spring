ALTER TABLE businessthirdparty DROP CONSTRAINT IF EXISTS fk_businessfunction;
ALTER TABLE postaladdress ADD CONSTRAINT fk_postaladdress_addresstype FOREIGN KEY (postaladdresstype_id) REFERENCES postaladdresstype;
