-- Shipping Instruction
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_portofloading_location FOREIGN KEY (portofloading_id) REFERENCES location;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_portofdischarge_location FOREIGN KEY (portofdischarge_id) REFERENCES location;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_placeofdelivery_location FOREIGN KEY (placeofdelivery_id) REFERENCES location;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_placeofreceipt_location FOREIGN KEY (placeofreceipt_id) REFERENCES location;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_containersize FOREIGN KEY (containersize_id) REFERENCES containersize;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_containermovement FOREIGN KEY (containermovement_id) REFERENCES containermovement;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_modeoftransport FOREIGN KEY (modeoftransport_id) REFERENCES modeoftransport;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_typeofshipment FOREIGN KEY (typeofshipment_id) REFERENCES typeofshipment;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_freightpaymentterm FOREIGN KEY (freightpaymentterm_id) REFERENCES freightpaymentterm;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_shippingline_thirdparty FOREIGN KEY (shippingline_id) REFERENCES businessthirdparty;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_quantityunit FOREIGN KEY (quantityunit_id) REFERENCES quantityunit;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_packageunit_packing FOREIGN KEY (packing_id) REFERENCES packing;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_payableparty_thirdparty FOREIGN KEY (freightpayableby_thirdparty_id) REFERENCES businessthirdparty;
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_payableparty_company FOREIGN KEY (freightpayableby_groupcompany_id) REFERENCES groupcompany;

-- Mapping Table businessthirdparty_thirdpartytype
ALTER TABLE businessthirdparty_thirdpartytype
  ADD CONSTRAINT fk_businessthirdparty_thirdpartytype_businessthirdparty FOREIGN KEY (businessthirdparty_id)
  REFERENCES businessthirdparty (id);

ALTER TABLE businessthirdparty_thirdpartytype
  ADD CONSTRAINT fk_businessthirdparty_thirdpartytype_thirdpartytype FOREIGN KEY (thirdpartytype_id)
  REFERENCES thirdpartytype (id);
  
  
-- Bank Account
ALTER TABLE bankaccount ADD CONSTRAINT fk_bankaccount_bankaccount_usagetype FOREIGN KEY (bankaccount_usage_type_id) REFERENCES bankaccount_usagetype;

-- Project Information
ALTER TABLE projectinformation ADD CONSTRAINT fk_projectinformation_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;

-- groupcompany email addresses for booking confirmation
ALTER TABLE groupcompany_bookingconfirmationmails ADD CONSTRAINT fk_groupcompany_bookingconfirmationmails_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES groupcompany;
ALTER TABLE groupcompany_bookingconfirmationmails ADD CONSTRAINT fk_groupcompany_bookingconfirmationmails_individual FOREIGN KEY (contactperson_id) REFERENCES individual;