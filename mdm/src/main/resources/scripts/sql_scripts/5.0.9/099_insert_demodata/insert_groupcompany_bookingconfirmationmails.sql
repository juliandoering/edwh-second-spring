INSERT INTO groupcompany_bookingconfirmationmails (uuid, created_by, updated_by, groupcompany_id, emailaddress, contactperson_id)
VALUES (
  '001f3ca8-f58d-4807-9cd0-7366eed68346',
  (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
  (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
  (SELECT id from groupcompany WHERE business_key = 'ICL' and is_version IS FALSE),
  'icl@nkg.coffee',
  (SELECT id from individual where first_name = 'Kay' and last_name = 'Lohse' and is_version = FALSE)
);

INSERT INTO groupcompany_bookingconfirmationmails (uuid, created_by, updated_by, groupcompany_id, emailaddress, contactperson_id)
VALUES (
  '001f3ca8-f58d-4807-0cd0-7366eed68346',
  (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
  (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
  (SELECT id from groupcompany WHERE business_key = 'NGVN' and is_version IS FALSE),
  'traffic.nr.zug@nkg.coffee',
  (SELECT id from individual where first_name = 'Kay' and last_name = 'Lohse' and is_version = FALSE)
);

INSERT into groupcompany_bookingconfirmationmails (uuid, is_version, parent, created_by, updated_by, groupcompany_id, emailaddress, contactperson_id)
select uuid, true, id, created_by, updated_by,
(select groupcompany.id from groupcompany where groupcompany.parent = groupcompany_bookingconfirmationmails.groupcompany_id and is_version = true),
emailaddress,
(select individual.id from individual where individual.parent = groupcompany_bookingconfirmationmails.contactperson_id and is_version = true)
from groupcompany_bookingconfirmationmails WHERE is_version = false AND uuid IN ('001f3ca8-f58d-4807-9cd0-7366eed68346', '001f3ca8-f58d-4807-0cd0-7366eed68346');

