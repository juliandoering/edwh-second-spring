insert into location (uuid, country_code, name, created_by, updated_by, si_unlocode)
  VALUES ('64fffdff-4840-4bb5-aed0-4d8df0760497', 'CH', 'Switzerland',
  (SELECT id from userprofile where username = 'edwh_admin'),
  (SELECT id from userprofile where username = 'edwh_admin'),
  'CH'
  );

insert into location (uuid, is_version, parent, country_code, name, created_by, updated_by, si_unlocode)
 SELECT uuid, true, id, country_code, name, created_by, updated_by, si_unlocode
  FROM location where uuid = '64fffdff-4840-4bb5-aed0-4d8df0760497';