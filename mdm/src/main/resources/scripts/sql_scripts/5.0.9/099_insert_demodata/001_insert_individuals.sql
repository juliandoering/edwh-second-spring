INSERT INTO individual (uuid, first_name, last_name, citizenship_id, created_by, updated_by)
  VALUES ( '43f442e1-2acd-4547-b996-9f8d3ecc9808', 'Lotti', 'Bolliger',
    (SELECT id from location where country_code = 'CH' AND location_code = null AND is_version = false),
    (SELECT id from userprofile where username = 'edwh_admin'),
    (SELECT id from userprofile where username = 'edwh_admin')
  );

INSERT INTO individual (uuid, first_name, last_name, citizenship_id, created_by, updated_by)
  VALUES ( 'd2696d1e-5a05-45cb-ae1a-20c0c5da2599', 'Kay', 'Lohse',
    (SELECT id from location where country_code = 'DE' AND location_code = null AND is_version = false),
    (SELECT id from userprofile where username = 'edwh_admin'),
    (SELECT id from userprofile where username = 'edwh_admin')
  );

 INSERT INTO individual (uuid, is_version, parent, first_name, last_name, citizenship_id, created_by, updated_by)
  SELECT uuid, true, id, first_name, last_name,
  (SELECT location.id from location where location.parent = individual.citizenship_id and is_version = true),
  created_by, updated_by FROM individual
  WHERE is_version is false and uuid IN ('43f442e1-2acd-4547-b996-9f8d3ecc9808', 'd2696d1e-5a05-45cb-ae1a-20c0c5da2599');
