INSERT INTO etl.bookingconfirmation
  (job_id,
   update_operation,
   filename,
   sender_id,
   receiver_id,
   request_date_timestamp,
   request_message_version,
   transaction_type,
   transaction_status,
   inttra_ref_number,
   purchase_order_number
  )
  VALUES (
  'abcd-job-id_123',
   'I',
   'inttraa1.txt',
   'senderA',
   'receiverA',
   '2018-02-28 05:55:55.123456789',
   1,
   'Booking',
   'Confirmed',
   'inttra-ref-number-1',
   'purchase_number_1'
);
