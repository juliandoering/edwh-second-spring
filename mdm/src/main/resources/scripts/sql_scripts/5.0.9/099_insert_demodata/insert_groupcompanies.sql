INSERT INTO postaladdress
(
  UUID,
  addressline1,
  addressline2,
  postal_code,
  location_id,
  created_by,
  updated_by
)
VALUES
(
  '6bcb1c17-f9d6-11e7-9169-dd535ad1ee5b',
  'Coffee Plaza',
  'Am Sandtorpark 4',
  '20457',
  (select id from location where si_unlocode = 'DEHAM' and is_version IS FALSE),
  (select id from userprofile where username = 'edwh_admin'),
  (select id from userprofile where username = 'edwh_admin')
);

INSERT INTO postaladdress (uuid, version, is_version, parent,  created_by, updated_by, addressline1, addressline2, postal_code, postaladdresstype_id, location_id)
  select uuid, version, true, id, created_by, updated_by, addressline1, addressline2, postal_code,
  (SELECT postaladdresstype.id FROM postaladdresstype WHERE postaladdresstype.parent = postaladdress.postaladdresstype_id AND is_version is true),
  (SELECT location.id FROM location WHERE location.parent = postaladdress.location_id AND is_version is true)
   from postaladdress where is_version is false and uuid = '6bcb1c17-f9d6-11e7-9169-dd535ad1ee5b';

INSERT INTO groupcompany
 (
   UUID,
   legal_name,
   acronym,
   tagetik_code,
   business_function,
   postal_address_id,
   created_by,
   updated_by,
   business_key,
   si_business_key
  )
 VALUES
 (
   'd4669db9-f0d9-4d8c-8890-f7f071a6ff6e',
   'ICL Internationale Commodity Logistik GmbH',
   'ICLG',
   'ICLG-SERV',
   'Service',
   (select id from postaladdress where uuid = '6bcb1c17-f9d6-11e7-9169-dd535ad1ee5b' and is_version = false),
   (select id from userprofile where username = 'edwh_admin'),
   (select id from userprofile where username = 'edwh_admin'),
   'ICL',
   'ICL'
 );

 INSERT INTO groupcompany (uuid, is_version, parent,legal_name, acronym, tagetik_code, business_function, postal_address_id, created_by, updated_by, business_key, si_business_key)
  SELECT uuid, true, id, legal_name, acronym, tagetik_code, business_function,
  (SELECT postaladdress.id from postaladdress where postaladdress.parent = groupcompany.postal_address_id and is_version = true),
 created_by, updated_by, business_key, si_business_key FROM groupcompany WHERE is_version is false and uuid = 'd4669db9-f0d9-4d8c-8890-f7f071a6ff6e';
