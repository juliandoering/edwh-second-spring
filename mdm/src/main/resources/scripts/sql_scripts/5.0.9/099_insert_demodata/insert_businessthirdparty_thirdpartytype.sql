-- Hamburger Berater Team
INSERT INTO  businessthirdparty_thirdpartytype
(
  businessthirdparty_id,
  thirdpartytype_id
)
VALUES (
  (SELECT id
   FROM businessthirdparty WHERE legalName = 'Hamburger Berater Team' AND is_version = false),
  (SELECT id
   FROM thirdpartytype WHERE code = 'CC-IND' AND is_version = false)
);
INSERT INTO  businessthirdparty_thirdpartytype
(
  businessthirdparty_id,
  thirdpartytype_id
)
VALUES (
  (SELECT id
   FROM businessthirdparty WHERE legalName = 'Hamburger Berater Team' AND is_version = true),
  (SELECT id
   FROM thirdpartytype WHERE code = 'CC-IND' AND is_version = true)
);

-- Euler Hermes
INSERT INTO  businessthirdparty_thirdpartytype
(
  businessthirdparty_id,
  thirdpartytype_id
)
VALUES (
  (SELECT id
   FROM businessthirdparty WHERE legalName = 'Euler Hermes' AND is_version = false),
  (SELECT id
   FROM thirdpartytype WHERE code = 'C-A' AND is_version = false)
);
INSERT INTO  businessthirdparty_thirdpartytype
(
  businessthirdparty_id,
  thirdpartytype_id
)
VALUES (
  (SELECT id
   FROM businessthirdparty WHERE legalName = 'Euler Hermes' AND is_version = true),
  (SELECT id
   FROM thirdpartytype WHERE code = 'C-A' AND is_version = true)
);