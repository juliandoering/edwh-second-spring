ALTER TABLE postaladdress ADD COLUMN addressline1 VARCHAR;
ALTER TABLE postaladdress ADD COLUMN addressline2 VARCHAR;

UPDATE postaladdress SET addressline1 = street;

ALTER TABLE postaladdress DROP COLUMN street;
ALTER TABLE postaladdress DROP COLUMN house_number;

ALTER TABLE postaladdress ADD COLUMN postaladdresstype_id BIGINT;

-- multiple addresses for business third party
ALTER TABLE postaladdress ADD COLUMN businessthirdparty_id BIGINT;
