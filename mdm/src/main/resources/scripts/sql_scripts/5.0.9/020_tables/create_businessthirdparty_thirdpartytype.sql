CREATE TABLE businessthirdparty_thirdpartytype
(
    id bigserial,
    
    /* payload */ 
    businessthirdparty_id bigint,
    thirdpartytype_id bigint,
    
    CONSTRAINT PK_businessthirdparty_thirdpartytype PRIMARY KEY (id)
);