CREATE TABLE projectinformation
(
    id bigserial,
    uuid varchar,
    
    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    created_by      bigint,
    updated_by      bigint,
    
    -- business fields
    businessthirdparty_id bigint,
    certification varchar,
    thirdpartyidnumber varchar,
    validityPeriodFrom date,
    validityPeriodTo date,
    
    CONSTRAINT PK_projectinformation PRIMARY KEY (id)
);