ALTER TABLE locationfunction ADD COLUMN version bigint NOT NULL default 0;
ALTER TABLE locationfunction ADD COLUMN is_version boolean NOT NULL default FALSE;
ALTER TABLE locationfunction ADD COLUMN parent bigint NOT NULL default 0;
