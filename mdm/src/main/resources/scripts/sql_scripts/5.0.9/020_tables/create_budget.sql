CREATE TABLE budget
(
    id              bigserial,
    uuid            varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    created_by bigint,
    updated_by bigint,

    -- business fields
    quantityunit_id bigint,
    volume bigint,
    validity_from DATE,
    validity_to DATE,
    businessthirdparty_commerciallimit_id bigint,

    CONSTRAINT pk_budget PRIMARY KEY (id)
);