ALTER TABLE etl.businessthirdpartytaxid ADD COLUMN job_id varchar;

CREATE INDEX idx_businessthirdpartytaxid_job_id on etl.businessthirdpartytaxid(job_id);
