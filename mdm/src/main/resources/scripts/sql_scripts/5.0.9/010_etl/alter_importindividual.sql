ALTER TABLE etl.individual ADD COLUMN postaladdress_line_1 VARCHAR;
ALTER TABLE etl.individual ADD COLUMN postaladdress_line_2 VARCHAR;
ALTER TABLE etl.individual ADD COLUMN postaladdress_type_id BIGINT;

ALTER TABLE etl.individual DROP COLUMN postaladdress_street;
