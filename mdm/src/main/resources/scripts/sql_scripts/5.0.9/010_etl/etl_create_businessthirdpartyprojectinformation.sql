CREATE TABLE etl.businessthirdpartyprojectinformation
(
    id bigserial,
    
    job_id varchar,

    businessthirdparty_id BIGINT,
    validityperiodfrom DATE,
    validityperiodto DATE,
    thirdpartyidnumber VARCHAR,
    certification VARCHAR,

    CONSTRAINT PK_businessthirdpartyprojectinformation PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartyprojectinformation OWNER TO etl;

CREATE INDEX idx_businessthirdpartyprojectinformation_job_id on etl.businessthirdpartyprojectinformation(job_id);
