ALTER TABLE etl.businessthirdpartycommercialid ADD COLUMN job_id varchar;

CREATE INDEX idx_businessthirdpartycommercialid_job_id on etl.businessthirdpartycommercialid(job_id);
