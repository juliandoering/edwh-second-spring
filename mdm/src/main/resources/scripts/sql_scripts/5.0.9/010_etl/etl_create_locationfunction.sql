CREATE TABLE etl.locationfunction
(
    id bigserial,
    
    job_id varchar,

    location_id BIGINT,
    function_type VARCHAR,

    CONSTRAINT PK_locationfunction PRIMARY KEY (id)
);

ALTER TABLE etl.locationfunction OWNER TO etl;

CREATE INDEX idx_locationfunction_job_id on etl.locationfunction(job_id);
