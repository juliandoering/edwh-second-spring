CREATE TABLE etl.location
(
    id bigserial,

    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk varchar,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,

    -- business fields
    country_code varchar(2),
    location_code varchar(3),
    name varchar,
    latitude varchar,
    longitude varchar,

    CONSTRAINT PK_location PRIMARY KEY (id)
);

ALTER TABLE etl.location OWNER TO etl;

CREATE INDEX idx_location_job_id on etl.location(job_id);
