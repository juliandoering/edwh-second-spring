ALTER TABLE etl.businessthirdparty ADD COLUMN preferred_terms_and_conditions varchar;
ALTER TABLE etl.businessthirdparty DROP COLUMN thirdpartytype_id;
ALTER TABLE etl.businessthirdparty DROP COLUMN compliance_rating_to_date;
ALTER TABLE etl.businessthirdparty RENAME COLUMN compliance_rating_from_date to date_of_rating;
