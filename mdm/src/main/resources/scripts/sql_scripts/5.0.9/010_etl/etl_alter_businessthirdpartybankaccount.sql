ALTER TABLE etl.businessthirdpartybankaccount ADD COLUMN job_id varchar;
ALTER TABLE etl.businessthirdpartybankaccount ADD COLUMN bankaccount_usage_type_id bigint;

CREATE INDEX idx_businessthirdpartybankaccount_job_id on etl.businessthirdpartybankaccount(job_id);
