ALTER TABLE etl.businessthirdpartyproductcert ADD COLUMN job_id varchar;

CREATE INDEX idx_businessthirdpartyproductcert_job_id on etl.businessthirdpartyproductcert(job_id);
