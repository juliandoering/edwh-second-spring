ALTER TABLE etl.businessthirdparty DROP COLUMN postal_address_street;
ALTER TABLE etl.businessthirdparty DROP COLUMN postal_address_postal_code;
ALTER TABLE etl.businessthirdparty DROP COLUMN postal_address_location_id;
ALTER TABLE etl.businessthirdparty DROP COLUMN legal_address_street;
ALTER TABLE etl.businessthirdparty DROP COLUMN legal_address_postal_code;
ALTER TABLE etl.businessthirdparty DROP COLUMN legal_address_location_id;