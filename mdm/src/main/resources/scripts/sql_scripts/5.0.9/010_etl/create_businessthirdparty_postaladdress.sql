CREATE TABLE etl.businessthirdpartypostaladdress
(
  id bigserial,

  -- business fields
  businessthirdparty_id bigint,
	line1 varchar,
	line2 varchar,
	postal_code varchar,
	location_id bigint,
	postaladdress_type_id bigint,

  CONSTRAINT PK_businessthirdpartypostaladdress PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartypostaladdress OWNER TO etl;