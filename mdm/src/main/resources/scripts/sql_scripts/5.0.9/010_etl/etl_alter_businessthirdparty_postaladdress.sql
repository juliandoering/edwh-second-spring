ALTER TABLE etl.businessthirdpartypostaladdress ADD COLUMN job_id varchar;

CREATE INDEX idx_businessthirdpartypostaladdress_job_id on etl.businessthirdpartypostaladdress(job_id);
