ALTER TABLE etl.businessthirdpartycontact ADD COLUMN job_id varchar;

CREATE INDEX idx_businessthirdpartycontact_job_id on etl.businessthirdpartycontact(job_id);
