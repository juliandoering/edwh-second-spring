ALTER TABLE etl.businessthirdpartyindividual ADD COLUMN job_id varchar;

CREATE INDEX idx_businessthirdpartyindividual_job_id on etl.businessthirdpartyindividual(job_id);
