CREATE TABLE etl.businessthirdpartytype
(
    id bigserial,
    
    job_id varchar,

    businessthirdparty_id BIGINT,
    thirdpartytype_id BIGINT,

    CONSTRAINT PK_businessthirdpartytype PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartytype OWNER TO etl;

CREATE INDEX idx_businessthirdpartytype_job_id on etl.businessthirdpartytype(job_id);
