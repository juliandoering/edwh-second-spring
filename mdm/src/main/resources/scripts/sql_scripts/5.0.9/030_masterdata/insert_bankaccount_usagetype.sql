INSERT INTO bankaccount_usagetype (uuid, code, description, created_by, updated_by)
    VALUES ('900077f1-3f41-4696-8bdc-8f7e57c84462', 'PC', 'Pay Coffee',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO bankaccount_usagetype (uuid, code, description, created_by, updated_by)
    VALUES ('3cddac3e-60a3-44c3-b7fe-7041401d0eb1', 'PS', 'Pay Services',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
    
INSERT INTO bankaccount_usagetype (uuid, version, is_version, parent, code, description, created_by, updated_by) 
  select uuid, version, true, id, code, description, created_by, updated_by from bankaccount_usagetype where is_version is false;