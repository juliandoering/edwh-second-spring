INSERT INTO postaladdresstype (uuid, code, description)
VALUES ('e674419e-ef84-46a0-b0ec-6b2c8963387c', 'LEGAL', 'Legal Address');

INSERT INTO postaladdresstype (uuid, code, description)
VALUES ('ba0b2067-fd65-4898-9945-c130e79501bb', 'BILLING', 'Billing Address');

INSERT INTO postaladdresstype (uuid, code, description)
VALUES ('610c23b6-d4f2-4cfd-a62f-26016f675535', 'POSTAL', 'Postal Address');

INSERT INTO postaladdresstype (uuid, code, description)
VALUES ('dfd1756e-fc9a-4d28-a6f4-824fae51bc61', 'SAMPLES', 'Address for sending coffee samples');

-- add 'created_by' and 'updated_by' to all rows
UPDATE postaladdresstype SET created_by = (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
  updated_by = (SELECT id FROM userprofile WHERE username = 'edwh_admin');

insert into postaladdresstype (uuid, version, is_version, parent, code, description, created_by, updated_by)
  select uuid, version, true, id, code, description, created_by, updated_by from postaladdresstype where is_version is false;

-- migrate old data to types
UPDATE postaladdress SET postaladdresstype_id = (SELECT id from postaladdresstype WHERE postaladdresstype.code = 'LEGAL' AND postaladdresstype.is_version = postaladdress.is_version)
  WHERE EXISTS (SELECT id FROM businessthirdparty WHERE legaladdress_id = postaladdress.id);
UPDATE postaladdress SET postaladdresstype_id = (SELECT id from postaladdresstype WHERE postaladdresstype.code = 'POSTAL' AND postaladdresstype.is_version = postaladdress.is_version)
  WHERE EXISTS (SELECT id FROM businessthirdparty WHERE postaladdress_id = postaladdress.id);
UPDATE postaladdress SET postaladdresstype_id = (SELECT id from postaladdresstype WHERE postaladdresstype.code = 'POSTAL' AND postaladdresstype.is_version = postaladdress.is_version)
  WHERE EXISTS (SELECT id FROM groupcompany WHERE postal_address_id = postaladdress.id);
UPDATE postaladdress SET postaladdresstype_id = (SELECT id from postaladdresstype WHERE postaladdresstype.code = 'POSTAL' AND postaladdresstype.is_version = postaladdress.is_version)
  WHERE EXISTS (SELECT id FROM  bankaccount WHERE postaladdress_id = postaladdress.id);
UPDATE postaladdress SET postaladdresstype_id = (SELECT id from postaladdresstype WHERE postaladdresstype.code = 'POSTAL' AND postaladdresstype.is_version = postaladdress.is_version)
  WHERE EXISTS (SELECT id FROM individual WHERE postal_address_id = postaladdress.id);
