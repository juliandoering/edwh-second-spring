/* <BUSINESS THIRD PARTY> */
UPDATE freemarkertemplate set template = 'A business third party entry was deleted

  ${(entity.legalName)!""}'
WHERE uuid = '12110030';

UPDATE freemarkertemplate set template = 'A business third party was updated

  ${(entity.legalName)!""}
  ${UriSelf}'
WHERE uuid = '12106030';

UPDATE freemarkertemplate set template = 'A business third party entry was created

  ${(entity.legalName)!""}
  ${UriSelf}'
WHERE uuid = '12107030';