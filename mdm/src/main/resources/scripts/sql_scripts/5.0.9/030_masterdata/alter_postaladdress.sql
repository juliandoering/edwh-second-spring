-- add btp association from btp table
UPDATE postaladdress SET businessthirdparty_id = (SELECT id from businessthirdparty WHERE businessthirdparty.legaladdress_id = postaladdress.id) WHERE EXISTS (SELECT id from businessthirdparty WHERE businessthirdparty.legaladdress_id = postaladdress.id);
UPDATE postaladdress SET businessthirdparty_id = (SELECT id from businessthirdparty WHERE businessthirdparty.postaladdress_id = postaladdress.id) WHERE EXISTS (SELECT id from businessthirdparty WHERE businessthirdparty.postaladdress_id = postaladdress.id);

UPDATE postaladdress SET businessthirdparty_id = -2 WHERE id = 12;