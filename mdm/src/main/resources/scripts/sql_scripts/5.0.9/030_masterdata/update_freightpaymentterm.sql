UPDATE freightpaymentterm
SET created_by = (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
    updated_by = (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE);
