INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '367749b0-ed4e-40f1-93e4-a8c62d6f2293',
  -- key
  'bookingconfirmation.created.unknown.subject',
  -- template
  '[Booking Confirmation] New Booking Confirmation for unknown Group Company',
  -- template description
  'Booking Confirmation created unknown Group Company subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '4e1a2d78-6d03-4ecf-bc3b-2423e05bdf63',
  -- key
  'bookingconfirmation.created.unknown.body',
  -- template
  'A new Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. This Booking Confirmation is ' ||
   'missing a Purchase Order Number and can therefore not be automatically assigned to a Group Company. The INTTRA reference ' ||
    'number is ${entity.inttraRefNumber}.',
  -- template description
  'Booking Confirmation created unknown Group Company body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '367749b0-ed4e-40f1-93e4-a8c62d6f2293',
  -- key
  'bookingconfirmation.updated.unknown.subject',
  -- template
  '[Booking Confirmation] Update of Booking Confirmation for unknown Group Company',
  -- template description
  'Booking Confirmation updated unknown Group Company subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '4e1a2d78-6d03-4ecf-bc3b-2423e05bdf63',
  -- key
  'bookingconfirmation.updated.unknown.body',
  -- template
  'Dear ${entity2.individual.firstName} ${entity2.individual.lastName}, ' ||
   'an update for an existing Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. ' ||
   'This Booking Confirmation is missing a Purchase Order Number and can therefore not be automatically assigned' ||
    ' to a Group Company. The INTTRA reference number is ${entity.inttraRefNumber}.',
  -- template description
  'Booking Confirmation updated unknown Group Company body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '6dd2965f-70e3-42af-b137-78c0363b0fda',
  -- key
  'bookingconfirmation.created.subject',
  -- template
  '[Booking Confirmation] New Booking Confirmation for ${entity2.groupCompany.legalName}',
  -- template description
  'Booking Confirmation created subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '619f5641-fb59-44ae-8c58-5c0a6e733ad9',
  -- key
  'bookingconfirmation.created.body',
  -- template
  'Dear ${entity2.individual.firstName} ${entity2.individual.lastName}, ' ||
  'a new Booking confirmation for your contract ${entity.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA reference number' ||
   ' is ${entity2.inttraRefNumber}.',
  -- template description
  'Booking Confirmation created body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '5ab7415a-35f5-4572-a8a4-1c901d6db15d',
  -- key
  'bookingconfirmation.updated.subject',
  -- template
  '[Booking Confirmation] Update of a Booking Confirmation for ${entity2.groupCompany.legalName}',
  -- template description
  'Booking Confirmation updated subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '477707bb-05f2-4322-9ae9-6ac5e83b2fbc',
  -- key
  'bookingconfirmation.updated.body',
  -- template
  'Dear ${entity2.individual.firstName} ${entity2.individual.lastName}, ' ||
  'a new version of a Booking confirmation for your contract ${entity.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA reference number' ||
   ' is ${entity2.inttraRefNumber}.',
  -- template description
  'Booking Confirmation updated body');