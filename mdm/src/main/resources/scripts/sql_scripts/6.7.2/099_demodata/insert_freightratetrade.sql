-- insert freightratetrade
INSERT INTO freightratetrade (uuid, name)
VALUES ('a02cee88-dafa-46ae-be58-d2b961abc2c9', 'Test Freight Rate Trade');

insert into freightratetrade (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from freightratetrade where is_version is false;
  