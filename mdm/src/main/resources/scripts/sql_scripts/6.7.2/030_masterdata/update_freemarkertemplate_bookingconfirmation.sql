UPDATE freemarkertemplate SET template =
 '<html>
    <head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px;
        }
        td {
            vertical-align: top;
        }
        table {
            width: 100%;
            table-layout: fixed;
        }
    </style>
    </head>
    <body>
        Dear customer,<br>
        a new Booking confirmation for your contract <@mark mark=changes.purchaseOrderNumber!false>"${entity.purchaseOrderNumber!"No Data"}"</@mark> has been uploaded to INTTRA. The INTTRA reference number is <@mark mark=changes.inttraRefNumber!false>${entity.inttraRefNumber!"No Data"}</@mark>.

        <p>
        Carrier Response Type: <b>${entity.transactionType!"No Data"}</b>
        </p>
        <p>
        <table>
            <tr>
                <td rowspan="3">
                    <@party name="Booker" party=entity.booker! partychanges=changes.booker!></@party>
                </td>
                <td rowspan="3">
                    <@party name="Shipper" party=entity.shipper! partychanges=changes.shipper!></@party>
                </td>
                <td>
                    <@party name="Carrier/NVOC/Booking Agent" party=entity.carrier! partychanges=changes.carrier!></@party>
                </td>
                <td>
                    <span><b>INTTRA Reference Number</b></span>
                    <br>
                    <span><@mark mark=changes.inttraRefNumber!false>${entity.inttraRefNumber!"No Data"}</@mark></span>
                </td>
            </tr>
            <tr>
                <td>
                    <span><b>Carrier Booking Number</b></span>
                    <br>
                    <span>No Data</span>
                </td>
            </tr>
            <tr>
                <td>
                    <span><b>Reference Numbers</b></span>
                    <br>
                    <br>
                    <span><b>Booking Number</b></span>
                    <br>
                    <span><@mark mark=changes.bookingNumber!false>${entity.bookingNumber!"No data"}</@mark></span>
                    <br>
                    <br>
                    <span><b>B/L Reference Number</b></span>
                    <br>
                    <span><@mark mark=changes.blNumber!false>${entity.blNumber!"No data"}</@mark></span>
                    <br>
                    <br>
                    <span><b>Purchase Order Number</b></span>
                    <br>
                    <span><@mark mark=changes.purchaseOrderNumber!false>${entity.purchaseOrderNumber!"No Data"}</@mark></span>
                </td>
                <td>
                    <@party name="Booking Office" party=entity.bookingOffice! partychanges=changes.bookingOffice!></@party>
                </td>
            </tr>
            <tr>
                <td>
                    <@party name="Forwarder" party=entity.forwarder! partychanges=changes.forwarder!></@party>
                </td>
                <td>
                    <@party name="Consignee" party=entity.consignee! partychanges=changes.consignee!></@party>
                </td>
            </tr>
            <tr>
                <td>
                    <@party name="Contract Party" party=entity.contractParty! partychanges=changes.contractParty!></@party>
                </td>
                <td>
                    <span><b>Notify Party Details</b></span>
                    <br>
                    <@party name="Main Notify Party" party=entity.mainNotifyParty! partychanges=changes.mainNotifyParty!></@party>
                    <#if entity.firstAdditionalNotifyParty??>
                        <br>
                        <@party name="First Additional Notify Party" party=entity.firstAdditionalNotifyParty! partychanges=changes.firstAdditionalNotifyParty!></@party>
                    </#if>
                    <#if entity.secondAdditionalNotifyParty??>
                        <br>
                        <@party name="Second Additional Notify Party" party=entity.secondAdditionalNotifyParty! partychanges=changes.secondAdditionalNotifyParty!></@party>
                    </#if>
                </td>
                <td colspan="2">
                    <@party name="Freight Payer Party" party=entity.freightPayerParty! partychanges=changes.freightPayerParty!></@party>
                </td>
            </tr>
            <tr>
                <td>
                    <span><b>Carrier, Vessel, Lloyd''s, Voyage</b></span>
                    <br>
                    <span><@mark mark=(changes.carrier.name)!false>${(entity.carrier.name)!"No data"}</@mark>, <@mark mark=(changes.mainTransportDetails.vesselName)!false>${(entity.mainTransportDetails.vesselName)!"No data"}, ${(entity.mainTransportDetails.lloydsCode)!"No data"}, ${(entity.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
                </td>
                <td>
                    <span><b>Main Port of Load</b></span>
                    <br>
                    <span><@mark mark=(changes.mainTransportDetails.mainPortOfLoad.location)!false>${(entity.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
                    <br>
                    <span><b>Sail Date</b></span>
                    <br>
                    <span>No Data</span>
                </td>
                <td colspan="2">
                    <span><b>Main Port of Discharge</b></span>
                    <br>
                    <span><@mark mark=(changes.mainTransportDetails.mainPortOfDischarge.location)!false>${(entity.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
                    <br>
                    <span><b>Estimated Arrival Date</b></span>
                    <br>
                    <span><@mark mark=(changes.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!false>${(entity.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
                </td>
            </tr>
        </table>
        </p>
        <br>
        <p>
            <table>
                <tr>
                    <th><b>Transport Plan Details</b></th>
                </tr>
                <tr>
                <#if entity.mainTransportDetails??>
                    <td>
                        <@plandetails name="Main Carriage 1" details=entity.mainTransportDetails changed=changes.mainTransportDetails></@plandetails>
                    </td>
                </#if>
                <#if entity.onCarriageTransportDetails??>
                    <td>
                        <@plandetails name="On Carriage 1" details=entity.onCarriageTransportDetails changed=changes.onCarriageTransportDetails></@plandetails>
                    </td>
                </#if>
                </tr>
            </table>
        </p>
        <br>

        <p>
            <table>
                <tr>
                    <th><b>Cargo/Packing</b></th>
                    <th><b>Packages</b></th>
                    <th><b>Cargo Description</b></th>
                    <th><b>Cargo Weight (Excluding Tare)</b></th>
                    <th><b>Gross Volume</b></th>
                </tr>
                <#if entity.goodsDetails?size gt 0>
                    <#list 0..entity.goodsDetails?size-1 as i>
                        <tr>                
                            <td>
                                <@mark mark=changes.goodsDetails[i].lineNumber!false>
                                    (${entity.goodsDetails[i].lineNumber!"No Data"}) "Unknown Packing"
                                </@mark>
                            </td>
                            <td>
                                <@mark mark=changes.goodsDetails[i].numberOfPackages!false>${entity.goodsDetails[i].numberOfPackages!"No Data"}</@mark> <@mark mark=changes.goodsDetails[i].packageTypeDescription!false>${entity.goodsDetails[i].packageTypeDescription!"Unknown Type"}</@mark>
                            </td>
                            <td>
                                <@mark mark=changes.goodsDetails[i].goodsDescription!false>
                                    ${entity.goodsDetails[i].goodsDescription!"No Data"}
                                </@mark>
                            </td>
                            <td>
                                <@mark mark=changes.goodsDetails[i].grossWeight!false>${entity.goodsDetails[i].grossWeight!"No Data"}</@mark> <@mark mark=changes.goodsDetails[i].grossWeightType!false>${entity.goodsDetails[i].grossWeightType!"Unknown Type"}</@mark>
                            </td>
                            <td>
                                <@mark mark=changes.goodsDetails[i].grossVolume!false>
                                    ${entity.goodsDetails[i].grossVolume!"No Data"}</@mark> <@mark mark=changes.goodsDetails[i].grossVolumeType!false>${entity.goodsDetails[i].grossVolumeType!"Unknown Type"}
                                </@mark>
                            </td>
                        </tr>
                    </#list>
                </#if>
            </table>
        </p>
        <br>
        <p>
            <table>
                <tr>
                    <th><b>Container</b></th>
                    <th><b>Net Weight</b></th>
                    <th><b>Net Volume</b></th>
                </tr>
                <#if entity.equipments?size gt 0>
                    <#list 0..entity.equipments?size-1 as i>
                        <tr>                
                            <td>
                                <@mark mark=changes.equipments[i].equipmentNumber!false>
                                    Container Number: ${entity.equipments[i].equipmentNumber!"No Data"}
                                </@mark>
                                <br>
                                <@mark mark=changes.equipments[i].typeCode!false>
                                    Type: ${entity.equipments[i].typeCode!"No Data"}
                                </@mark>
                                <br>
                                <@mark mark=changes.equipments[i].cargoMovementType!false>
                                    Service Arrangement: ${entity.equipments[i].cargoMovementType!"No Data"}
                                </@mark>
                                <br>
                                <@mark mark=changes.equipments[i].haulageArrangements!false>
                                    Haulage Arrangement: ${entity.equipments[i].haulageArrangements!"No Data"}
                                </@mark>
                            </td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    </#list>
                </#if>
            </table>
        </p>

        <#macro party name party partychanges>
            <span><b>${name}</b></span>
            <#if party??>
                <br/>
                <#if party.identifierType??><span><@mark mark=partychanges.identifierType!false>${party.identifierType}</@mark>: <@mark mark=partychanges.identifier!false>${party.identifier}</@mark></span><br></#if>
                <@mark mark=partychanges.name!false><span>${party.name!""}<br></span></@mark>
                <@mark mark=partychanges.address!false><span>${party.address!"N/A"}<br></span></@mark>
            </#if>
        </#macro>

        <#macro plandetails name details changed>
            <b>${name}</b>
            <br>
            <#if details.transportMode??>
                <span>Transport Mode: <@mark mark=changed.transportMode!false>${details.transportMode!"No Data"}</@mark></span>
            </#if>
            <#if details.carrier??>
                <span>/ Carrier SCAC: <@mark mark=changed.carrier!false>${details.carrier!"No Data"}</@mark></span>
            </#if>
            <br>
            <#if details.vesselName??>
                <span>Vessel: <@mark mark=changed.vesselName!false>${details.vesselName!"No Data"}</@mark></span>
            </#if>
            <#-- Lloyd''s code + voyage number -->
            <br>
            <#if details.mainPortOfLoad??>
                <span>Start Location: <@mark mark=changed.mainPortOfLoad.location!false>${(details.mainPortOfLoad.location)!"No Data"}/ </@mark></span>
                <span>Estimated Arrival: <@mark mark=changed.mainPortOfLoad.earliestDeparture!false>${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</@mark></span>
                <br>
            </#if>
            <#if details.mainPortOfDischarge??>
                <span>Start Location: <@mark mark=changed.mainPortOfDischarge.location!false>${(details.mainPortOfDischarge.location)!"No Data"}/ </@mark></span>
                <span>Estimated Arrival: <@mark mark=changed.mainPortOfDischarge.estimatedArrival!false>${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</@mark></span>
            </#if>
        </#macro>

        <#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
        <#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

    </body>
</html>'
WHERE template_key = 'bookingconfirmation.created.body';
