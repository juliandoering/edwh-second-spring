CREATE TABLE limittype_coffeevariety
(
    id bigserial,

    -- business fields
    limittype_id bigint,
    coffeevariety_id bigint,

    CONSTRAINT PK_limittype_coffeevariety PRIMARY KEY (id)
);

ALTER TABLE limittype_coffeevariety ADD CONSTRAINT fk_limittype_coffeevariety_limittype FOREIGN KEY (limittype_id) REFERENCES limittype;
CREATE INDEX idx_limittype_coffeevariety_limittype_id on limittype_coffeevariety(limittype_id);

ALTER TABLE limittype_coffeevariety ADD CONSTRAINT fk_limittype_coffeevariety_coffeevariety FOREIGN KEY (coffeevariety_id) REFERENCES coffee_variety;
CREATE INDEX idx_limittype_coffeevariety_coffeevariety_id on limittype_coffeevariety(coffeevariety_id);
