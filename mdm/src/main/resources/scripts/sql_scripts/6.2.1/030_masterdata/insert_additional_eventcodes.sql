-- additional events
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', 'AW', 'Awaiting Export',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
INSERT INTO eventcode (uuid, code, description, created_by, updated_by)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', 'CU', 'Carrier and Customs Release',
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edwh_admin' AND deleted IS FALSE));
    
-- insert immutable versions   
insert into eventcode (uuid, version, is_version, parent, code, description, created_by, updated_by) 
  select uuid, version, true, id, code, description, created_by, updated_by from eventcode where code = 'AW';
insert into eventcode (uuid, version, is_version, parent, code, description, created_by, updated_by) 
  select uuid, version, true, id, code, description, created_by, updated_by from eventcode where code = 'CU';