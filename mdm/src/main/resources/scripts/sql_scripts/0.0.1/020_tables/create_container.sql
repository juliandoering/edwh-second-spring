--changeset generated_nf:1497621355 
CREATE TABLE container
(
    id bigserial,
    uuid varchar,
    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    -- business fields
    shipment_advice_id bigint,
    container_number varchar,
    seal_number varchar,
    shippers_seal_number varchar,
    
    CONSTRAINT PK_container PRIMARY KEY (id)
);
