--changeset generated_nf:1497621355 
CREATE TABLE email
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    to_recipient varchar,
    subject varchar,
    body varchar,
    send boolean NOT NULL default FALSE,
    CONSTRAINT PK_email PRIMARY KEY (id)
);
