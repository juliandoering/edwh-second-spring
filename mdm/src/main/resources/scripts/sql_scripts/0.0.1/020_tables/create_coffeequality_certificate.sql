--changeset generated_nf:1497621355 
CREATE TABLE coffeequality_certificate
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,

    record_version integer default 0,

    productcertificationprogram_id bigint,
    coffeequality_id bigint,
    CONSTRAINT PK_coffeequalities_certificate PRIMARY KEY (id),
    CONSTRAINT UK_coffeequalities_certificate UNIQUE (productcertificationprogram_id, coffeequality_id)
);
