--changeset generated_nf:1497621355 
CREATE TABLE postaladdress
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    /* content */
    street varchar,
    house_number varchar,
    postal_code varchar,
    location_id bigint NOT NULL,
    CONSTRAINT PK_postaladdress PRIMARY KEY (id)
);
