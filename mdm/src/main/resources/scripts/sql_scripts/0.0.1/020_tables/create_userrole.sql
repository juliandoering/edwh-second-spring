--changeset generated_nf:1497621355 
CREATE TABLE userrole
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    rolename varchar,
    admin boolean NOT NULL default FALSE,
    CONSTRAINT PK_userrole PRIMARY KEY (id)
);
