CREATE TABLE countrycallingcodes (
  id             BIGSERIAL,
  uuid           VARCHAR,
  created_at     TIMESTAMP        DEFAULT current_timestamp,
  updated_at     TIMESTAMP        DEFAULT current_timestamp,

  record_version INTEGER          DEFAULT 0,

  -- soft delete information
  deleted        BOOLEAN NOT NULL DEFAULT FALSE,
  /* version information */
  version        BIGINT  NOT NULL DEFAULT 0,
  is_version     BOOLEAN NOT NULL DEFAULT FALSE,
  parent         BIGINT  NOT NULL DEFAULT 0,

  code           INTEGER,

  CONSTRAINT PK_countrycallingcodes PRIMARY KEY (id)
);