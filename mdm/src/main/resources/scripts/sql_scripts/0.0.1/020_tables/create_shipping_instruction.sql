--changeset generated_nf:1497621355 
CREATE TABLE shippinginstruction
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    contract_id bigint,
    shippinginstruction_number bigint,
    
    batch_id bigint,
    
    placeofloading_id bigint,
    placeofdelivery_id bigint,
    
    CONSTRAINT PK_shippinginstruction PRIMARY KEY (id)
);