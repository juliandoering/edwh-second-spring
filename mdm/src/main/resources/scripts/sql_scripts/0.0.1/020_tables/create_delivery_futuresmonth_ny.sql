--changeset generated_nf:1497621355 
CREATE TABLE delivery_futuresmonth_ny
(
  id            BIGSERIAL,
  deliverymonth INTEGER,
  futuresmonth  INTEGER,

  CONSTRAINT PK_delivery_futuresmonth_ny PRIMARY KEY (id)
);