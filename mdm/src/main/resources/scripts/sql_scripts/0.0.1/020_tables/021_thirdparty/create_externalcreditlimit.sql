--changeset generated_nf:1497621355 
CREATE TABLE externalcreditlimit
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
	
	/* payload */
	businessthirdparty_commerciallimit_id bigint,
    insurer_id bigint,
    insurerid varchar,
    currency_id bigint,
    amountOfCurrency decimal,
    dateFrom date,
    dateTo date,
	
    CONSTRAINT PK_externalcreditlimit PRIMARY KEY (id)
);
