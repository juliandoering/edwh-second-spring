--changeset generated_nf:1497621355 
CREATE TABLE businessthirdparty_individual
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
    
   	/* payload */ 
	businessthirdparty_id bigint,
	individual_id bigint,
	-- the information in what relation businessthirdparty and individual are, e.g. Owner of business
	businessthirdparty_individual_relation_id bigint,
		
    
    CONSTRAINT PK_businessthirdparty_individual PRIMARY KEY (id)
);
