--changeset generated_nf:1497621355 
CREATE TABLE firstnoticedays
(
  id             BIGSERIAL,
  uuid           VARCHAR,
  -- technical fields
  created_at     TIMESTAMP        DEFAULT current_timestamp,
  updated_at     TIMESTAMP        DEFAULT current_timestamp,
  deleted        BOOLEAN NOT NULL DEFAULT FALSE,
  record_version INTEGER          DEFAULT 0,
  version        BIGINT  NOT NULL DEFAULT 0,
  is_version     BOOLEAN NOT NULL DEFAULT FALSE,
  parent         BIGINT  NOT NULL DEFAULT 0,

  -- business fields
  firstnoticeday DATE,
  terminalmonth  INTEGER,
  terminalyear   INTEGER,
  market         VARCHAR,

  CONSTRAINT PK_firstnoticeday PRIMARY KEY (id)
);