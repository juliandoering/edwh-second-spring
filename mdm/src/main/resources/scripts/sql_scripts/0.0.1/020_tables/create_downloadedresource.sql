--changeset generated_nf:1497621355 
CREATE TABLE downloadedresource
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    
	userprofile_id bigint,
    entity varchar,
    entity_id bigint,
    resource_version bigint,
    downloaded boolean,
    
    CONSTRAINT PK_downloadedresource PRIMARY KEY (id)
);
