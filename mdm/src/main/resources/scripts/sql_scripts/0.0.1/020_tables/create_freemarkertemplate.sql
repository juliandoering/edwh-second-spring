--changeset generated_nf:1497621355 
/* text templates in freemarker style to generate text for e.g. emails */
CREATE TABLE freemarkertemplate
(
	id bigserial,
	uuid varchar,
    /* activeJdbc audit information */
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    
    /* optimistic locking */
    record_version integer default 0,
    
    -- soft delete information
    -- deleted boolean NOT NULL default FALSE,    
	/* version information */
    -- version bigint NOT NULL default 0,    
    -- is_version boolean NOT NULL default FALSE,    
    -- parent bigint NOT NULL default 0,
    
	-- an unique 'key' so that this template can be 'addressed' 
	template_key varchar NOT NULL UNIQUE,
	template varchar,
	-- a description for admin convenience
	description varchar,
    
    CONSTRAINT PK_freemarkertemplate PRIMARY KEY (id)
);