--changeset generated_nf:1497621355 
CREATE TABLE warehouse
(
    id bigserial,
    uuid varchar,
    /* activeJdbc audit information */
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    /* optimistic locking */
    record_version integer default 0,    
    -- soft delete information
    deleted boolean NOT NULL default FALSE,    
	/* version information */
    version bigint NOT NULL default 0,    
    is_version boolean NOT NULL default FALSE,    
    parent bigint NOT NULL default 0,
    
    -- Name of the warehouse possibly (FYI: up to three names seperated by semicolon)
	name varchar,    
    
    -- owner/operator <business partner> (group company or third person)
	businesspartner_id bigint,
	
    -- "Address <Location>"
    location_id bigint,
            
    CONSTRAINT PK_warehouse PRIMARY KEY (id)
);