--changeset generated_nf:1497621355 
CREATE TABLE location
(
    id bigserial,
    uuid varchar,
    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    -- business fields
    country_code varchar(2),    
    location_code varchar(3),    
    name varchar,
    location_type varchar,
    latitude varchar,
    longitude varchar,
    
    CONSTRAINT PK_location PRIMARY KEY (id)
);
