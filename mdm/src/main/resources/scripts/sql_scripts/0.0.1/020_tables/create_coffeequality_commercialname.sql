--changeset generated_nf:1497621355 
CREATE TABLE coffeequality_commercialname
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
    
	/* payload */
	coffeequality_id bigint,
	name varchar,

        CONSTRAINT PK_coffeequality_commercialname PRIMARY KEY (id)
);
