--changeset generated_nf:1497621355 
CREATE TABLE locationfunction
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    function_type varchar,    
    location_id bigint,    
    CONSTRAINT PK_locationfunction PRIMARY KEY (id)
);
