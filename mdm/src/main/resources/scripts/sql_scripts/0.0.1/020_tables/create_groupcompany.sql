--changeset generated_nf:1497621355 
CREATE TABLE groupcompany
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    legal_name varchar,
    acronym varchar,
    tagetik_code varchar,
    business_function varchar,
    email_address varchar,
    postal_address_id bigint NOT NULL,
    CONSTRAINT PK_groupcompany PRIMARY KEY (id)
);
