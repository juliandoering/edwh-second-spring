CREATE TABLE contractrole_businessfunction
(
    id bigserial,
    uuid varchar,
    buyersellercode_id bigint,
    businessfunction_id bigint,

    CONSTRAINT PK_contractrole_businessfunction PRIMARY KEY (id)
);
