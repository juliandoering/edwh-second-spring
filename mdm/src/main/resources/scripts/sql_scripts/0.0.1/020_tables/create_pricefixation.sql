--changeset generated_nf:1497621355 
/* Table to hold price fixations to contracts but seperated from the contract itself */
CREATE TABLE pricefixation
(
	id bigserial,
    uuid varchar,
	-- technical fields
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
	deleted boolean NOT NULL default FALSE,
	record_version integer default 0,
	version bigint NOT NULL default 0,
	is_version boolean NOT NULL default FALSE,
	parent bigint NOT NULL default 0,

	-- business fields
	contract_id bigint,
	fixation_number bigint,

	-- the fixation value
	fixation decimal,
	-- the amount of quantity to fixate at once
	quantity decimal,
	fixation_date date,
    
	CONSTRAINT PK_pricefixation PRIMARY KEY (id)

);

