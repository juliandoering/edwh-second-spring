--changeset generated_nf:1497621355 
CREATE TABLE coffeequality
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
    
	/* payload */
	name varchar,
	-- commercial names are one to many in 'quality_commercialname'
	coffeequality_origintype_id bigint,
	-- origins are in many to many table 'quality_origins'
	coffee_type_id bigint,
	coffee_variety_id bigint,
	-- certificates are in many to many table 'quality_certificate'
	grade varchar,
	standard_conversion_factor_to_green_graded decimal,
	defectcount varchar,
	humidity varchar,
	cup_profile varchar,

        CONSTRAINT PK_coffeequality PRIMARY KEY (id)
);
