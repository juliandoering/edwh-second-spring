--changeset generated_nf:1497621355 
CREATE TABLE warehouse_individual
(
    id bigserial,
    uuid varchar,
    warehouse_id bigint,
    individual_id bigint,

    CONSTRAINT PK_warehouse_individual PRIMARY KEY (id)
);