--changeset generated_nf:1497621355 
CREATE TABLE businessthirdparty_commerciallimit
(
    id bigserial,
    uuid varchar,
    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    /* BUSINESS FIELDS */
	signature_date date,

    groupcompany_id bigint,
    businessthirdparty_id bigint,

    -- Credit Terms {only in case of commercial credit}
    -- private CommercialCreditTerms creditTerms;

    -- #External Credit Limit:
    -- External Party: Business Third Party {Service Vendor - Credit Insurance}

    -- Internal Credit Limit:
    -- Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable)
	-- Validity period should be Business Plan period in general.  Shorter if so required.
	
	-- Collaterals
	-- Only needed when an Internal Credit Limit is set
	
	-- Maximum Open Position
	-- Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable)
	-- Validity period should be Business Plan period in general.  Shorter if so required.

	-- Maximum Open Priced Position
	-- Suggestion by the company and/or recommendation from the coach and/or method used by other group companies (if applicable)
	-- Validity period should be Business Plan period in general.  Shorter if so required.
        
    CONSTRAINT PK_businessthirdparty_commerciallimit PRIMARY KEY (id)
);

