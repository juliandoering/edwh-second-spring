--changeset generated_nf:1497621355 
/* Holding ids of warehouses that have been changed and that need to be broadcasted */
CREATE TABLE warehouseupdates
(
	id bigserial,
    uuid varchar,
	-- ids of warehouses that were updated
    warehouse_id bigint,
    
    CONSTRAINT PK_warehouseupdate PRIMARY KEY (id)
);