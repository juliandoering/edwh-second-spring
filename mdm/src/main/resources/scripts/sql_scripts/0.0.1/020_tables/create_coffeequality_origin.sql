--changeset generated_nf:1497621355 
CREATE TABLE coffeequality_origin
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	/* payload */
	coffeequality_id bigint,
	location_id bigint,

        CONSTRAINT PK_quality_origin PRIMARY KEY (id)
);
