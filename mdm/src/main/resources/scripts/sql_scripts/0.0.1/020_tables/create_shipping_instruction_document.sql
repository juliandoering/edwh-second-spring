--changeset generated_nf:1497621355 
CREATE TABLE shippinginstructiondocument
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    shippinginstruction_id bigint,
    
    document text,
    
    CONSTRAINT PK_shippinginstructiondocument PRIMARY KEY (id)
);