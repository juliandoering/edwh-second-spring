--changeset generated_nf:1497621355 
CREATE TABLE bankaccount
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
	
	/* payload */
	businessthirdparty_id bigint,
	bankaccount_name varchar,
	bankaccount_number varchar,
	bankaccount_number_type_id bigint,
	bank_legalname varchar,
	bank_branchname varchar,
	postaladdress_id bigint,
    
    CONSTRAINT PK_bankaccount PRIMARY KEY (id)
);
