--changeset generated_nf:1497621355 
CREATE TABLE productcertification
(
    id bigserial,
    uuid varchar,
    
    -- technical fields
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    
    -- business fields
    businessthirdparty_id bigint,
    productcertificationprogram_id bigint,
    thirdpartyidnumber varchar,
    validityPeriodFrom date,
    validityPeriodTo date,
    
    CONSTRAINT PK_productcertification PRIMARY KEY (id)
);
