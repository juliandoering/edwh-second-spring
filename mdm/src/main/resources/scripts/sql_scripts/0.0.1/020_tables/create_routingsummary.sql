--changeset generated_nf:1497621355 
CREATE TABLE routingsummary
(
    id bigserial,
    uuid varchar,
    -- business fields
    contract_id bigint,
	location_id bigint,
    
    CONSTRAINT UK_routingsummary UNIQUE (contract_id, location_id)
);
