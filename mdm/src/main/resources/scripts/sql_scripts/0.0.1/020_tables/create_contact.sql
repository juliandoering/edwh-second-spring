--changeset generated_nf:1497621355 
/* Table holding contact information */
CREATE TABLE contact
(
	id bigserial,
	uuid varchar,
    /* activeJdbc audit information */
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    /* optimistic locking */
    record_version integer default 0,    
    -- soft delete information
    deleted boolean NOT NULL default FALSE,    
	/* version information */
    version bigint NOT NULL default 0,    
    is_version boolean NOT NULL default FALSE,    
    parent bigint NOT NULL default 0,
    
	-- the contact, e.g. telephone number, email, fax number
	contactvalue varchar,
	-- the type of what the information above is, e.g. tel, email, fax
    contacttype varchar,
    
    /* the possible places where contact information is held */
    warehouse_id bigint,
    businessthirdparty_id bigint,
    individual_id bigint,
    groupcompany_id bigint,
    
    CONSTRAINT PK_contact PRIMARY KEY (id)
);