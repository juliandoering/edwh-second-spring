--changeset generated_nf:1497621355 
CREATE TABLE businessthirdparty_individual_relation
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
    
   	/* payload */ 
	-- the information in what relation businessthirdparty and individual are, e.g. Owner of business
	shortcode varchar,	
	relation varchar,	
    
	CONSTRAINT PK_businessthirdparty_individual_relation PRIMARY KEY (id)
);
