--changeset generated_nf:1497621355 
/* text templates in freemarker style to generate text for e.g. emails */
CREATE TABLE businessthirdparty 
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
    
	/* payload */

    -- Legal Name: String
    legalName varchar,

    -- Legal Name In Latin Alphabet: String
    legalNameInLatinAlphabet varchar,

    -- Commercial Name: String
    commercialName varchar,

    -- Acronym: String
    acronym varchar,

    -- Type of Business Entity: {only one of}
	-- private BusinessOrganisationType businessorganisationtype;
	businessorganisationtype_id bigint,

    -- Legal Address: Address
    legaladdress_id bigint,

    -- Postal Address: Address
    postaladdress_id bigint,

    -- #Contact
    -- Telephone(s): {one or more} Telephone
    -- Fax: {none or more} Telephone
    -- E-mail address: {one or more} E-mail
    -- Website: {none or more} Website
    -- private List<Contact> contacts;
    -- FK is in contact!

    -- Commercial ID {none or more}

    -- Tax ID {none or more}

    -- Owner: {one or more } Individuals(MD)
    -- {only in case of Sole Proprietorship and Partnerships. Only one in case of sole proprietorship, two or more in case of Partnerships}
    -- private List<Individual> owner;
    -- FK in table businessthirdparty_individual

    -- Ultimate Beneficiary Owner(UBO): {one or more } Individuals(MD) {only in case of Corporations and Others with exception of Public/state owned entities}
    -- FK in table businessthirdparty_individual

    -- Management/ Legal Representatives: {one or more } Individuals(MD) {Not for Sole Proprietorship}
    -- private Individual representative;
    -- FK in table businessthirdparty_individual

    -- Contact Person: {one or more } Individuals(MD) {for all cases even for sole proprietorship}
	-- private Individual contactPerson;
    -- FK in table businessthirdparty_individual

    -- Bank account: {one or more} Bank Account
	-- private BankAccountNumber bankAccount;
	-- FK in bankaccount
	
    -- Type of Third Party: {only one of}
    -- One must be specified.
	-- In the case of Coffee Supplier – Producer and Coffee Supplier – Producer/Intermediary additional separate information about the farm is to be provided.
    businessfunction_id bigint,
    
    -- International Standard Certifications
    
    -- Product Certifications
   
	-- Compliance Rating
	-- Outcome of the compliance assessment
	-- If not done by default is “Unassessed”
    
    -- Preferred term o contract
	-- Can be blank = no preferred term
    incoterm_id bigint,
    
    -- Price fixation Terms: {none or more}
    -- Preferred term of pricing for contracts
	-- Can be blank = no preferred fixation Term

    -- Presentation of documents: {one of}
    -- Direct in Trust
    -- Via Bank
    -- private PresentationOfDocuments presentationOfDocuments;
    presentation_of_documents_id bigint,

    -- Payment Terms: {one of}
    -- Prepayment
    -- Cash Against presentation of documents before delivery
    -- Cash Against presentation of documents
    -- Commercial credit
    -- On specific Date
    -- private PaymentTermsType paymentTerms;
    thirdpartypaymentterms_id bigint,
    
    CONSTRAINT PK_businessthirdparty PRIMARY KEY (id)
);
