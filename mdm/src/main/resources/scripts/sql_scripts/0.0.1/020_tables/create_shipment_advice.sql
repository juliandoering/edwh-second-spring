--changeset generated_nf:1497621355 
CREATE TABLE shipment_advice
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,
    contract_id bigint,
    sequential_number bigint,
    vessel_name varchar,
    shipping_company_name varchar,
    name_of_shippers varchar,
    bill_of_lading_number varchar,
    on_board_date date,
    shipped_date date,
    port_of_shipment varchar,
    port_of_destination varchar,
    quantity integer,
    description varchar,
    main_marks varchar,
    CONSTRAINT PK_shipment_advice PRIMARY KEY (id)
);
