--changeset generated_nf:1497621355 
/* Create the payment terms for third parties
 * */
CREATE TABLE thirdpartypaymentterms
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
	
	/* payload */
	code varchar,
	paymentterms_type varchar,    
    
    CONSTRAINT PK_thirdpartypaymentterms PRIMARY KEY (id)
);
