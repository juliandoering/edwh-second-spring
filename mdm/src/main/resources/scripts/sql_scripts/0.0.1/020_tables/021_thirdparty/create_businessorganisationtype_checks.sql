--changeset generated_nf:1497621354 
CREATE TABLE businessorganisationtype_checks (
	id                              BIGSERIAL,
	uuid                            VARCHAR,
	/* activeJdbc audit information */
	created_at                      TIMESTAMP        DEFAULT current_timestamp,
	updated_at                      TIMESTAMP        DEFAULT current_timestamp,

	/* optimistic locking */
	record_version                  INTEGER          DEFAULT 0,

	-- soft delete information
	deleted                         BOOLEAN NOT NULL DEFAULT FALSE,
	/* version information */
	version                         BIGINT  NOT NULL DEFAULT 0,
	is_version                      BOOLEAN NOT NULL DEFAULT FALSE,
	parent                          BIGINT  NOT NULL DEFAULT 0,

	businesstypecode                VARCHAR,
	checkowner                      BOOLEAN,
	checkbeneficiary                BOOLEAN,
	checkmanagement_representatives BOOLEAN,
	checkcontactperson              BOOLEAN,

	CONSTRAINT PK_businessorganisationtype_checks PRIMARY KEY (id)

);

