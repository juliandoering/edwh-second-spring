--changeset generated_nf:1497621355 
/* type of bank account number (IBAN / BIC / other) */
CREATE TABLE bankaccount_number_type
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,
    
	-- soft delete information
	deleted boolean NOT NULL default FALSE,    
	/* version information */
	version bigint NOT NULL default 0,    
	is_version boolean NOT NULL default FALSE,    
	parent bigint NOT NULL default 0,
	
	/* payload */
	typename varchar,
    
    CONSTRAINT PK_bankaccount_number_type PRIMARY KEY (id)
);