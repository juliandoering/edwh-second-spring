
CREATE TABLE individual
(
    id bigserial,
    uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
	  parent bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,    

    /* FOR NOW places where individuals are used - an individual can only work for one GC */
    groupcompany_id bigint,
    last_name varchar,
    first_name varchar,    
    addressing_form varchar,
    academic_title varchar,
    gender varchar,
    citizenship varchar,
    date_of_birth date,
    personal_id varchar,
    tax_id varchar,
    postal_address_id bigint,

	/* Individuals are referenced from businessthirdparty_individual */

    place_of_birth_id bigint,

    
    CONSTRAINT PK_individual PRIMARY KEY (id)
);
