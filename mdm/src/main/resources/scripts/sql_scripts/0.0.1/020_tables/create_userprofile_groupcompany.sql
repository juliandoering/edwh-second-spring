--changeset generated_nf:1497621355 
CREATE TABLE userprofile_groupcompany (
	id bigserial,
	uuid varchar,
    created_at timestamp default current_timestamp,
    updated_at timestamp default current_timestamp,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
	userprofile_id bigint,
    groupcompany_id bigint,
	CONSTRAINT PK_userprofile_groupcompany PRIMARY KEY (id),
    CONSTRAINT UK_userprofile_groupcompany UNIQUE (userprofile_id, groupcompany_id)
);
