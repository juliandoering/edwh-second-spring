--changeset generated_nf:1497621354 
CREATE TABLE etl.individualcontact
(
    id bigserial,
    
    -- business fields
    individual_id bigint,
    contacttype varchar,
    contactvalue varchar,
    
    CONSTRAINT PK_individualcontact PRIMARY KEY (id)
);

ALTER TABLE etl.individualcontact OWNER TO etl;
