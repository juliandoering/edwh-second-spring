--changeset generated_nf:1497621354 
CREATE TABLE etl.coffeequalityorigin
(
    id bigserial,
    
    -- business fields
    coffeequality_id bigint,
    location_id bigint,
    
    CONSTRAINT PK_coffeequalityorigin PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequalityorigin OWNER TO etl;
