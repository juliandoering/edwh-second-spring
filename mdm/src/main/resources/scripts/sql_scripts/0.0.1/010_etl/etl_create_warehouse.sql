--changeset generated_nf:1497621355 
CREATE TABLE etl.warehouse
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk varchar,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    -- Name of the warehouse possibly (FYI: up to three names seperated by semicolon)
	name varchar,    
    -- Address <Location>
    location_id bigint,
        
    CONSTRAINT PK_warehouse PRIMARY KEY (id)
);

ALTER TABLE etl.warehouse OWNER TO etl;
