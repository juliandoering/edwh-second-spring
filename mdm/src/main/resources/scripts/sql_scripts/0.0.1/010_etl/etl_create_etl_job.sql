--changeset generated_nf:1497621354 
CREATE TABLE etl.job
(
    id bigserial,
    
    job_id varchar,
    start_at timestamp default current_timestamp,
    stop_at timestamp,
    
    CONSTRAINT PK_job PRIMARY KEY (id)
);
