--changeset generated_nf:1497621354 
CREATE TABLE etl.businessthirdpartyindividual
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    individual_id bigint,
    individual_type varchar,
    
    CONSTRAINT PK_businessthirdpartyindividual PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartyindividual OWNER TO etl;
