--changeset generated_nf:1497621355 
CREATE TABLE etl.warehousepersonincharge
(
    id bigserial,
    
    -- business fields
    warehouse_id bigint,
    last_name varchar,
    first_name varchar,
    gender varchar,
    date_of_birth date,
    
    CONSTRAINT PK_warehousepersonincharge PRIMARY KEY (id)
);

ALTER TABLE etl.warehousepersonincharge OWNER TO etl;
