CREATE TABLE etl.businessthirdpartyinterstandardcert
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    interstandardcertprogram_id bigint,
    certificateversion varchar,
    thirdpartyidnumber varchar,
    validityPeriodFrom date,
    validityPeriodTo date,
    
    CONSTRAINT PK_businessthirdpartyinterstandardcert PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartyinterstandardcert OWNER TO etl;
