CREATE TABLE etl.businessthirdpartytaxid
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    taxid varchar,
    
    CONSTRAINT PK_businessthirdpartytaxid PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartytaxid OWNER TO etl;
