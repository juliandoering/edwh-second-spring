--changeset generated_nf:1497621354 
CREATE TABLE etl.businessthirdparty
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    legal_name varchar,
    legal_name_latin_alphabet varchar,
    acronym varchar,
    commercial_name varchar,
    business_organisation_type_id bigint,
    postal_address_street varchar,
    postal_address_postal_code varchar,
    postal_address_location_id bigint,
    legal_address_street varchar,
    legal_address_postal_code varchar,
    legal_address_location_id bigint,
    businessfunction_id bigint,
    presentation_of_documents_id bigint,
    compliance_rating varchar,
    compliance_rating_from_date date,
    compliance_rating_to_date date,
    incoterm_id bigint,    
    third_party_payment_terms_id bigint,
    
    CONSTRAINT PK_businessthirdparty PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdparty OWNER TO etl;
