CREATE TABLE etl.businessthirdpartyproductcert
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    productcertificationprogram_id bigint,
    thirdpartyidnumber varchar,
    validityPeriodFrom date,
    validityPeriodTo date,
    
    CONSTRAINT PK_businessthirdpartyproductcert PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartyproductcert OWNER TO etl;
