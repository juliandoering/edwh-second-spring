--changeset generated_nf:1497621354 
CREATE TABLE etl.coffeequalitycommercialname
(
    id bigserial,
    
    -- business fields
    coffeequality_id bigint,
    commercialname varchar,
    
    CONSTRAINT PK_coffeequalitycommercialname PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequalitycommercialname OWNER TO etl;
