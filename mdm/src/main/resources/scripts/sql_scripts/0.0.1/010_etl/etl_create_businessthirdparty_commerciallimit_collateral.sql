CREATE TABLE etl.businessthirdparty_commerciallimitcollateral
(
    id bigserial,
    
    -- business fields
    businessthirdparty_commerciallimit_id bigint,
    collateraltype_id bigint,
    currency_id bigint,
    amount decimal,
        
    CONSTRAINT PK_businessthirdparty_commerciallimitcollateral PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdparty_commerciallimitcollateral OWNER TO etl;
