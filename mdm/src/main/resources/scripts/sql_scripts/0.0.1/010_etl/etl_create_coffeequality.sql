--changeset generated_nf:1497621354 
CREATE TABLE etl.coffeequality
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    coffeequality_name varchar,
    origin_type_id bigint,
    coffee_type_id bigint,
    coffee_variety_id bigint, 
    grade_specification varchar,
    defectcount_specification varchar,
    humidity_specification varchar,
    cupprofile_specification varchar,
    standard_conversion_factor_to_green_graded decimal,
    
    CONSTRAINT PK_coffeequality PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequality OWNER TO etl;
