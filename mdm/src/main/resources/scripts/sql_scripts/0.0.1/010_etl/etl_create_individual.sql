--changeset generated_nf:1497621354 
CREATE TABLE etl.individual
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk bigint,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    addressing_form varchar,
    academic_title varchar,
    last_name varchar,
    first_name varchar,
    gender varchar,
    date_of_birth date,
    place_of_birth_id bigint,
    citizenship varchar,
    postaladdress_street varchar,
    postaladdress_postalcode varchar,
    postaladdress_location_id bigint,
    personal_id_number varchar,
    tax_number varchar,
    
    CONSTRAINT PK_individual PRIMARY KEY (id)
);

ALTER TABLE etl.individual OWNER TO etl;
