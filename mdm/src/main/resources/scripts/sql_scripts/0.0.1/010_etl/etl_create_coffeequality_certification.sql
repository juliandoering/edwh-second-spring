--changeset generated_nf:1497621354 
CREATE TABLE etl.coffeequalitycertification
(
    id bigserial,
    
    -- business fields
    coffeequality_id bigint,
    productcertificationprogram_id bigint,
    
    CONSTRAINT PK_coffeequalitycertification PRIMARY KEY (id)
);

ALTER TABLE etl.coffeequalitycertification OWNER TO etl;
