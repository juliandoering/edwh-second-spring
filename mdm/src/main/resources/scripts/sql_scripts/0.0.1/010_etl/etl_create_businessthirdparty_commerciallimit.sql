--changeset generated_nf:1497621354
CREATE TABLE etl.businessthirdparty_commerciallimit
(
  id                                    BIGSERIAL,

  -- technical fields
  -- aggregates a number of individuals to one upload job
  job_id                                VARCHAR,
  -- tells what to do with this individual (create(I)/update(U)/delete(D))
  update_operation                      CHAR(1),
  -- timestamp when the etl has been imported
  import_date                           TIMESTAMP,
  -- name of the source application
  source                                VARCHAR,
  -- ID in the source application
  source_pk                             BIGINT,

  -- mdm id
  mdm_id                                BIGINT,
  -- mdm version
  mdm_version                           BIGINT,

  -- business fields
  groupcompany_id                       BIGINT,
  businessthirdparty_id                 BIGINT,
  signature_date                        DATE,
  maxopen_position_unit_id              BIGINT,
  maxopen_position_amount               DECIMAL,
  maxopen_position_forwardPeriod        INTEGER,
  maxopen_position_valid_from           DATE,
  maxopen_position_valid_to             DATE,
  maxopen_priced_position_unit_id       BIGINT,
  maxopen_priced_position_amount        DECIMAL,
  maxopen_priced_position_forwardPeriod INTEGER,
  maxopen_priced_position_valid_from    DATE,
  maxopen_priced_position_valid_to      DATE,
  commercial_credit_terms_days          BIGINT,
  commercial_credit_terms_from_id       BIGINT,
  ext_credit_limit_insurer_id           BIGINT,
  ext_credit_limit_insurerid            INTEGER,
  ext_credit_limit_currency_id          BIGINT,
  ext_credit_limit_amount               DECIMAL,
  ext_credit_limit_valid_from           DATE,
  ext_credit_limit_valid_to             DATE,
  int_credit_limit_currency_id          BIGINT,
  int_credit_limit_amount               DECIMAL,
  int_credit_limit_valid_from           DATE,
  int_credit_limit_valid_to             DATE,

  CONSTRAINT PK_businessthirdparty_commerciallimit PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdparty_commerciallimit
  OWNER TO etl;
