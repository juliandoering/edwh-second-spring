--changeset generated_nf:1497621354 
CREATE TABLE etl.businessthirdpartycontact
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    contacttype varchar,
    contactvalue varchar,
    
    CONSTRAINT PK_businessthirdpartycontact PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartycontact OWNER TO etl;
