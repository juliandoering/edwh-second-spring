--changeset generated_nf:1497621354 
CREATE TABLE etl.businessthirdpartybankaccount
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
	bankaccount_name varchar,
	bankaccount_number varchar,
	bankaccount_number_type_id bigint,
	bank_legalname varchar,
	bank_branchname varchar,
	bank_branch_address_street varchar,
	bank_branch_address_postal_code varchar,
	bank_branch_address_location_id bigint,
	
    CONSTRAINT PK_businessthirdpartybankaccount PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartybankaccount OWNER TO etl;
