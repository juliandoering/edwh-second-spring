CREATE TABLE etl.businessthirdpartypricefixationterm
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    pricefixationtype_id bigint,
    fixationoption_id bigint,
    
    CONSTRAINT PK_businessthirdpartypricefixationterm PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartypricefixationterm OWNER TO etl;
