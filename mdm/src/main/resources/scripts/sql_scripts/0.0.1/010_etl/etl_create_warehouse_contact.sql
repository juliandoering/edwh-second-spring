--changeset generated_nf:1497621355 
CREATE TABLE etl.warehousecontact
(
    id bigserial,
    
    -- business fields
    warehouse_id bigint,
    contacttype varchar,
    contactvalue varchar,
    
    CONSTRAINT PK_warehousecontact PRIMARY KEY (id)
);

ALTER TABLE etl.warehousecontact OWNER TO etl;
