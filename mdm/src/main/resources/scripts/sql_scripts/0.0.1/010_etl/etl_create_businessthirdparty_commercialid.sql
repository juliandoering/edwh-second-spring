CREATE TABLE etl.businessthirdpartycommercialid
(
    id bigserial,
    
    -- business fields
    businessthirdparty_id bigint,
    commercialid varchar,
    
    CONSTRAINT PK_businessthirdpartycommercialid PRIMARY KEY (id)
);

ALTER TABLE etl.businessthirdpartycommercialid OWNER TO etl;
