--changeset generated_nf:1497621355 
INSERT INTO userrole (uuid, rolename, admin)
	VALUES ('a67fc670-9ef4-11e7-be27-00ff0b1c94f7', 'edwh_admin', true);
	
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a67fc671-9ef4-11e7-be27-00ff0b1c94f7', 'rodriguez', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a6821062-9ef4-11e7-be27-00ff0b1c94f7', 'foede', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a6821063-9ef4-11e7-be27-00ff0b1c94f7', 'elvers', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a6821064-9ef4-11e7-be27-00ff0b1c94f7', 'finck', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a6821065-9ef4-11e7-be27-00ff0b1c94f7', 'lips', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a6821066-9ef4-11e7-be27-00ff0b1c94f7', 'vatterodt', (select id from userrole where rolename = 'edwh_admin'));

INSERT INTO userrole (uuid, rolename, admin)
    VALUES ('a6821067-9ef4-11e7-be27-00ff0b1c94f7', 'edwh_technical', false);
    
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a6821068-9ef4-11e7-be27-00ff0b1c94f7', 'app-ngvn', (select id from userrole where rolename = 'edwh_technical'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a6821069-9ef4-11e7-be27-00ff0b1c94f7', 'bric-vietnam', (select id from userrole where rolename = 'edwh_technical'));