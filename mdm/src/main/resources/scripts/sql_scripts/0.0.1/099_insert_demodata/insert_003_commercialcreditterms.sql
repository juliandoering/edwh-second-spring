INSERT INTO commercialcreditterms
(
  UUID,
  businessthirdparty_commerciallimit_id,
  days,
  commercialcredittermsfromtype_id
)
VALUES
(
  '1230040',
  (SELECT id FROM businessthirdparty_commerciallimit WHERE groupcompany_id = 2 and businessthirdparty_id = 1 and is_version is false),
  30,
  (SELECT id FROM commercialcredittermsfromtype WHERE code = 'POD' and is_version is false)
);

--insert into commercialcreditterms (uuid, version, is_version, parent, businessthirdparty_commerciallimit_id, days, commercialcredittermsfromtype_id) 
--  select uuid, version, true, id, businessthirdparty_commerciallimit_id, days, commercialcredittermsfromtype_id from commercialcreditterms where is_version is false;