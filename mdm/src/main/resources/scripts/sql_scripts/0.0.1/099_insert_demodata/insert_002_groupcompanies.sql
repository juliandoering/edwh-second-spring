--changeset generated_nf:1497621355 
	-- insert postal address data
INSERT INTO postaladdress (uuid, street, postal_code, location_id)
	VALUES ('a6848185-9ef4-11e7-be27-00ff0b1c94f7', 'Rua XV de Novembro', '11010-151', (SELECT id FROM location where name = 'Santos' and is_version is false));
INSERT INTO postaladdress (uuid, street, postal_code, location_id)
	VALUES ('a686cb76-9ef4-11e7-be27-00ff0b1c94f7', 'Rod. Fernando Dias BR 381 KM 632', '37.262-000', (SELECT id FROM location where name = 'Santo Antônio do Amparo' and is_version is false));
	
insert into postaladdress (uuid, version, is_version, parent, street, postal_code, location_id) 
  select uuid, version, true, id, street, postal_code, 
        (SELECT location.id FROM location where location.parent = postaladdress.location_id and location.is_version is true)
  from postaladdress where is_version is false;

-- insert group company
INSERT INTO groupcompany (uuid, legal_name, acronym, tagetik_code, business_function, email_address, postal_address_id)
	VALUES ('a686cb77-9ef4-11e7-be27-00ff0b1c94f7', 'Stockler Comercial e Exportadora Ltda.', null, 'STBR-COEX', null,'stockler@stocklerltda.com.br',
	(SELECT id FROM postaladdress where street = 'Rua XV de Novembro' and is_version is false));

INSERT INTO groupcompany (uuid, legal_name, acronym, tagetik_code, business_function, email_address, postal_address_id)
	VALUES ('a686cb78-9ef4-11e7-be27-00ff0b1c94f7', 'NKG Fazendas Brasileiras S.A.', null, 'FZBR-FARM', null, 'lagoa@nkgfazendas.com',
	(SELECT id FROM postaladdress where street = 'Rod. Fernando Dias BR 381 KM 632' and is_version is false));
	
INSERT INTO groupcompany (uuid, legal_name, acronym, tagetik_code, business_function, email_address, postal_address_id)
	VALUES ('a686cb79-9ef4-11e7-be27-00ff0b1c94f7', 'BR Vietnam', null, 'NGVN', null, 'cuong@vn.com',
	(SELECT id FROM postaladdress where street = 'Rod. Fernando Dias BR 381 KM 632' and is_version is false));

INSERT INTO groupcompany (uuid, legal_name, acronym, tagetik_code, business_function, email_address, postal_address_id)
	VALUES ('a686cb7a-9ef4-11e7-be27-00ff0b1c94f7', 'Bernhard Rothfos Inter Coffee', null, 'BRIC', null, 'ms@bric.ch',
	(SELECT id FROM postaladdress where street = 'Rod. Fernando Dias BR 381 KM 632' and is_version is false));
	
insert into groupcompany (uuid, version, is_version, parent, legal_name, acronym, tagetik_code, business_function, email_address, postal_address_id) 
  select uuid, version, true, id, legal_name, acronym, tagetik_code, business_function, email_address, 
        (SELECT postaladdress.id FROM postaladdress where postaladdress.parent = groupcompany.postal_address_id and postaladdress.is_version is true)
  from groupcompany where is_version is false;

-- insert managers
INSERT INTO individual (uuid, first_name, last_name, gender, groupcompany_id)
	VALUES ('a686cb7b-9ef4-11e7-be27-00ff0b1c94f7', 'Günter', 'Haeusler', 'Male',
	(SELECT id FROM groupcompany where tagetik_code = 'STBR-COEX' and is_version is false));
INSERT INTO individual (uuid, first_name, last_name, gender, groupcompany_id)
	VALUES ('a686cb7c-9ef4-11e7-be27-00ff0b1c94f7', 'Joaquím', 'Paiva', 'Male',
	(SELECT id FROM groupcompany where tagetik_code = 'FZBR-FARM' and is_version is false));
	
insert into individual (uuid, version, is_version, parent, first_name, last_name, gender, groupcompany_id)
  select uuid, version, true, id, first_name, last_name, gender,
        (SELECT groupcompany.id FROM groupcompany where groupcompany.parent = individual.groupcompany_id and groupcompany.is_version is true)
  from individual where is_version is false;

-- insert contact data
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('a686cb7d-9ef4-11e7-be27-00ff0b1c94f7', '+55-13-3213 8201', 'TELEPHONE',
	(SELECT id FROM groupcompany where tagetik_code = 'STBR-COEX' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('a686cb7e-9ef4-11e7-be27-00ff0b1c94f7', '+55-13-3213 8200', 'TELEPHONE',
	(SELECT id FROM groupcompany where tagetik_code = 'STBR-COEX' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('a686cb7f-9ef4-11e7-be27-00ff0b1c94f7', '+55-13-3213 8233', 'FAX',
	(SELECT id FROM groupcompany where tagetik_code = 'STBR-COEX' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('a686cb80-9ef4-11e7-be27-00ff0b1c94f7', ' +55-35-386 32950', 'TELEPHONE',
	(SELECT id FROM groupcompany where tagetik_code = 'FZBR-FARM' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, groupcompany_id)
	VALUES ('a686cb81-9ef4-11e7-be27-00ff0b1c94f7', '+55-35-386 32999', 'FAX',
	(SELECT id FROM groupcompany where tagetik_code = 'FZBR-FARM' and is_version is false));

	
insert into contact (uuid, version, is_version, parent, contactvalue, contacttype, groupcompany_id)
  select uuid, version, true, id, contactvalue, contacttype,
        (SELECT groupcompany.id FROM groupcompany where groupcompany.parent = contact.groupcompany_id and is_version is true)
  from contact where is_version is false;

