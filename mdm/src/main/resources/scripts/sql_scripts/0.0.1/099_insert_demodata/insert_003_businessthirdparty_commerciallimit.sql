--changeset generated_nf:1497621355 
INSERT INTO businessthirdparty_commerciallimit
(
    uuid,
    groupcompany_id ,
    businessthirdparty_id
) VALUES (
    '1243040',
	(SELECT id FROM groupcompany WHERE tagetik_code = 'STBR-COEX' AND is_version is false),
    (SELECT id FROM businessthirdparty WHERE acronym = 'HBT' AND is_version is false)
);

INSERT INTO businessthirdparty_commerciallimit
(
    uuid,
    groupcompany_id ,
    businessthirdparty_id
) VALUES (
    '1243041',
	(SELECT id FROM groupcompany WHERE tagetik_code = 'STBR-COEX' AND is_version is false),
    (SELECT id FROM businessthirdparty WHERE acronym = 'EH' AND is_version is false)
);


INSERT INTO internalcreditlimit
(
    uuid,
    businessthirdparty_commerciallimit_id,
    currency_id,
    amount,
    validto,
    validfrom 
) VALUES (
    '1243040',
    (SELECT id FROM businessthirdparty_commerciallimit WHERE uuid = '1243040' AND is_version is false),
    (SELECT id FROM currency WHERE currency = 'USD' AND is_version is false),
    73000,
    '2018-01-01' ,
    '2017-01-01' 
);

INSERT INTO maximumopenposition
(
    uuid,
    businessthirdparty_commerciallimit_id,
    quantityunit_id,
    quantity,
    validfrom,
    validto
) VALUES (
    '1243040',
    (SELECT id FROM businessthirdparty_commerciallimit WHERE uuid = '1243040' AND is_version is false),
    (SELECT id FROM quantityunit WHERE name = 'kg' AND is_version is false),
    322,
    '2017-03-01' ,
    '2017-08-01'
);

INSERT INTO maximumopenpricedposition
(
    uuid,
    businessthirdparty_commerciallimit_id,
    quantityunit_id,
    quantity,
    validfrom,
    validto
) VALUES (
    '1243040',
    (SELECT id FROM businessthirdparty_commerciallimit WHERE uuid = '1243040' AND is_version is false),
    (SELECT id FROM quantityunit WHERE name = 'kg' AND is_version is false),
	120,
	'2017-01-01' ,
	'2019-01-01'
);

/* Versioning */
insert into businessthirdparty_commerciallimit (uuid, version, is_version, parent, groupcompany_id, businessthirdparty_id ) 
  select uuid, version, true, id, 
    (SELECT groupcompany.id FROM groupcompany WHERE groupcompany.parent = businessthirdparty_commerciallimit.groupcompany_id AND groupcompany.is_version is true),
    (SELECT businessthirdparty.id FROM businessthirdparty WHERE businessthirdparty.parent = businessthirdparty_commerciallimit.businessthirdparty_id AND businessthirdparty.is_version is true)  
  from businessthirdparty_commerciallimit where is_version is false;

insert into internalcreditlimit (version, is_version, parent, uuid, businessthirdparty_commerciallimit_id, currency_id, amount, validto, validfrom) 
  select version, true, id, uuid, (SELECT id FROM businessthirdparty_commerciallimit WHERE parent = businessthirdparty_commerciallimit_id AND is_version is true), currency_id, amount, validto, validfrom from internalcreditlimit where is_version is false;
  
insert into maximumopenposition (version, is_version, parent, uuid, businessthirdparty_commerciallimit_id, quantityunit_id, quantity, forwardPeriod, validfrom, validto) 
  select version, true, id, uuid, (SELECT id FROM businessthirdparty_commerciallimit WHERE parent = businessthirdparty_commerciallimit_id AND is_version is true), quantityunit_id, quantity, forwardPeriod, validfrom, validto from maximumopenposition where is_version is false;  
  
insert into maximumopenpricedposition (version, is_version, parent, uuid, businessthirdparty_commerciallimit_id, quantityunit_id, quantity, forwardPeriod, validfrom, validto) 
  select version, true, id, uuid, (SELECT id FROM businessthirdparty_commerciallimit WHERE parent = businessthirdparty_commerciallimit_id AND is_version is true), quantityunit_id, quantity, forwardPeriod, validfrom, validto from maximumopenpricedposition where is_version is false;  
  
