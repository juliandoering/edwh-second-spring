--changeset generated_nf:1497621355 
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1215040',
		(SELECT id FROM productcertificationprogram WHERE code = '4C' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Blue Mountain' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1216040',
		(SELECT id FROM productcertificationprogram WHERE code = 'ORG' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Blue Mountain' and is_version is false)
	);
INSERT INTO coffeequality_certificate (uuid, productcertificationprogram_id, coffeequality_id)
	VALUES (
	    '1217040',
		(SELECT id FROM productcertificationprogram WHERE code = 'UTZ' and is_version is false),
		(SELECT id FROM coffeequality WHERE name = 'Blue Mountain' and is_version is false)
	);
