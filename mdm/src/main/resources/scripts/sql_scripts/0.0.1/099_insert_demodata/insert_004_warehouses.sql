--changeset generated_nf:1497621355 
-- insert warehouse
INSERT INTO warehouse (uuid, name, businesspartner_id, location_id)
    VALUES ('a6893c85-9ef4-11e7-be27-00ff0b1c94f7', 'Best Warehouse', 
            (SELECT id FROM groupcompany WHERE legal_name = 'Stockler Comercial e Exportadora Ltda.' and is_version is false),
            (SELECT id FROM location WHERE name = 'Santos' and is_version is false));
INSERT INTO warehouse (uuid, name, businesspartner_id, location_id)
    VALUES ('a6893c86-9ef4-11e7-be27-00ff0b1c94f7', 'Second Best Warehouse', (SELECT id FROM groupcompany WHERE legal_name = 'NKG Fazendas Brasileiras S.A.' and is_version is false),
(SELECT id FROM location WHERE name = 'Santo Antônio do Amparo' and is_version is false));

insert into warehouse (uuid, version, is_version, parent, name, businesspartner_id, location_id) 
  select uuid, version, true, id, name, 
        (SELECT id FROM groupcompany WHERE groupcompany.parent = warehouse.businesspartner_id and groupcompany.is_version is true), 
        (SELECT id FROM location WHERE location.parent = warehouse.location_id and location.is_version is true) 
  from warehouse where is_version is false;

-- set contact information
INSERT INTO contact (uuid, contactvalue, contacttype, warehouse_id)
    VALUES ('a6893c87-9ef4-11e7-be27-00ff0b1c94f7', '+12 345 678 90', 'tel', (SELECT id FROM warehouse WHERE name = 'Best Warehouse' and is_version is false));
INSERT INTO contact (uuid, contactvalue, contacttype, warehouse_id)
VALUES ('a6893c88-9ef4-11e7-be27-00ff0b1c94f7', '+98 765 4321', 'tel', (SELECT id FROM warehouse WHERE name = 'Second Best Warehouse' and is_version is false));

insert into contact (uuid, version, is_version, parent, contactvalue, contacttype, warehouse_id) 
  select uuid, version, true, id, contactvalue, contacttype, 
        (SELECT id FROM warehouse WHERE warehouse.parent = contact.warehouse_id and warehouse.is_version is true) 
  from contact where is_version is false;

-- set person in charge
INSERT INTO individual (uuid, first_name, last_name, gender)
    VALUES ('a6893c89-9ef4-11e7-be27-00ff0b1c94f7', 'Lord', 'Voldemort', 'male');
INSERT INTO individual (uuid, first_name, last_name, gender)
VALUES ('a6893c8a-9ef4-11e7-be27-00ff0b1c94f7', 'Darth', 'Vader', 'male');

insert into individual (uuid, version, is_version, parent, first_name, last_name, gender) 
  select uuid, version, true, id, first_name, last_name, gender from individual where is_version is false and uuid in ('1248040', '1249040');

INSERT INTO warehouse_individual (uuid, warehouse_id, individual_id)
VALUES ('a6893c8b-9ef4-11e7-be27-00ff0b1c94f7',
        (SELECT id FROM warehouse WHERE name = 'Best Warehouse' and is_version is false),
        (SELECT id FROM individual WHERE last_name = 'Voldemort' and is_version is false));
INSERT INTO warehouse_individual (uuid, warehouse_id, individual_id)
VALUES ('a6893c8c-9ef4-11e7-be27-00ff0b1c94f7',
        (SELECT id FROM warehouse WHERE name = 'Second Best Warehouse' and is_version is false),
        (SELECT id FROM individual WHERE last_name = 'Vader' and is_version is false));

insert into warehouse_individual (uuid, warehouse_id, individual_id) 
  select
        uuid,
        (SELECT id FROM warehouse WHERE warehouse.parent = warehouse_individual.warehouse_id and warehouse.is_version is true), 
        (SELECT id FROM individual WHERE individual.parent = warehouse_individual.individual_id and individual.is_version is true) 
  from warehouse_individual;