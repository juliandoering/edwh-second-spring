-- create individuals
 
INSERT INTO individual (
  uuid,
	last_name ,    
	first_name ,    
	addressing_form ,
	date_of_birth,
	groupcompany_id
) VALUES (
	'122440', 'Finck','Frieda','Mrs.', '2017-05-07', '1'
);

INSERT INTO individual (
  uuid,
	last_name ,    
	first_name ,    
	addressing_form ,
	date_of_birth,
	groupcompany_id
) VALUES (
	'1225040', 'González','Matheus','Mr.', '1973-01-02', '2'
);

INSERT INTO individual (
  uuid,
	last_name ,    
	first_name ,    
	addressing_form ,
	date_of_birth,
	groupcompany_id
) VALUES (
	'1226040', 'Romero','Gabriela','Mrs.', '1979-09-02', '1'
);

INSERT INTO individual (
  uuid,
	last_name ,    
	first_name ,    
	addressing_form ,
	date_of_birth
) VALUES (
	'1227040', 'Fernández','Pedro','Mr.', '1953-03-09'
);

INSERT INTO individual (
  uuid,
	last_name ,    
	first_name ,    
	addressing_form ,
	date_of_birth,
	groupcompany_id
) VALUES (
	'1228040', 'Díaz','Maria','Mrs.', '1960-01-12','1'
);

INSERT INTO individual (
  uuid,
	last_name ,    
	first_name ,    
	addressing_form ,
	date_of_birth
) VALUES (
	'1229040', 'Fernández','João','Mr.', '1963-12-22'
);

insert into individual (uuid, version, is_version, parent, last_name, first_name, addressing_form, date_of_birth, groupcompany_id)
  (select uuid, version, true, id, last_name, first_name, addressing_form, date_of_birth, groupcompany_id from individual where is_version is false);