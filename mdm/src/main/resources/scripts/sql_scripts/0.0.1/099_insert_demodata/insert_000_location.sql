--changeset generated_nf:1497621355 
-- insert location data
INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude)
	VALUES ('a682106e-9ef4-11e7-be27-00ff0b1c94f7', 'BR', 'SSZ', 'Santos', '2357S', '04620W');
	
INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude) 
	VALUES ('a682106f-9ef4-11e7-be27-00ff0b1c94f7', 'BR', 'STP', 'Santo Antônio do Amparo', '2057S', '04455W');
	
INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude) 
	VALUES ('a6821070-9ef4-11e7-be27-00ff0b1c94f7', 'BR', null , null , '2057S', '04455W');
	
INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude) 
	VALUES ('a6821071-9ef4-11e7-be27-00ff0b1c94f7', 'CO', null , null , '123S', '456W');
	
INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude) 
	VALUES ('a6821072-9ef4-11e7-be27-00ff0b1c94f7', 'JM', null , null , '12S', '34W');

INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude) 
	VALUES ('a6821073-9ef4-11e7-be27-00ff0b1c94f7', 'VN', null , 'Vietnam' , '', '');

INSERT INTO location (uuid, country_code, location_code, name, latitude, longitude) 
	VALUES ('a6848174-9ef4-11e7-be27-00ff0b1c94f7', 'DE', 'HAM' , null , '888S', '888W');
	
insert into location (uuid, version, is_version, parent, country_code, location_code, name, latitude, longitude) 
  select uuid, version, true, id, country_code, location_code, name, latitude, longitude from location where is_version is false;

-- insert location function data	
INSERT INTO locationfunction (uuid, function_type, location_id) 
	VALUES ('a6848175-9ef4-11e7-be27-00ff0b1c94f7', 'PORT', (SELECT id FROM location where name = 'Santos' and is_version is false));
INSERT INTO locationfunction (uuid, function_type, location_id) 
	VALUES ('a6848176-9ef4-11e7-be27-00ff0b1c94f7', 'RAIL TERMINAL', (SELECT id FROM location where name = 'Santos' and is_version is false));
INSERT INTO locationfunction (uuid, function_type, location_id) 
	VALUES ('a6848177-9ef4-11e7-be27-00ff0b1c94f7', 'ROAD TERMINAL', (SELECT id FROM location where name = 'Santos' and is_version is false));
INSERT INTO locationfunction (uuid, function_type, location_id) 
	VALUES ('a6848178-9ef4-11e7-be27-00ff0b1c94f7', 'AIRPORT', (SELECT id FROM location where name = 'Santos' and is_version is false));
INSERT INTO locationfunction (uuid, function_type, location_id) 
	VALUES ('a6848179-9ef4-11e7-be27-00ff0b1c94f7', 'ROAD TERMINAL', (SELECT id FROM location where name = 'Santo Antônio do Amparo' and is_version is false));
