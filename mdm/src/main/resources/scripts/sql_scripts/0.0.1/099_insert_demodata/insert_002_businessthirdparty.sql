--changeset generated_nf:1497621355 
INSERT INTO businessthirdparty
(
  uuid,
  legalName,
  commercialName,
  acronym,
  businessorganisationtype_id,
  legaladdress_id,
  postaladdress_id,
  presentation_of_documents_id,
  thirdpartypaymentterms_id,
  incoterm_id,
  businessfunction_id
)
VALUES (
  '1230040',
  'Hamburger Berater Team',
  'HBT GmbH',
  'HBT',
  (SELECT id
   FROM businessorganisationtype
   WHERE code = 'PRIVLLC' AND is_version IS FALSE),
  (SELECT id
   FROM postaladdress
   WHERE street = 'Stadthausbrücke' AND is_version IS FALSE),
  (SELECT id
   FROM postaladdress
   WHERE id = 2 AND is_version IS FALSE),
  (SELECT id
   FROM presentation_of_documents
   WHERE code = 'TRUST' AND is_version IS FALSE),
  (SELECT id
   FROM paymentterm
   WHERE code = 'CAD' AND is_version IS FALSE),
  (SELECT id 
   FROM incoterm
   WHERE code='FOB' and is_version is false),
  (SELECT id
   FROM businessfunction
   WHERE code='CC-IND' and is_version is false)
);

INSERT INTO commercialid (uuid, businessthirdparty_id, commercialid)
VALUES ('a6848180-9ef4-11e7-be27-00ff0b1c94f7',
        (SELECT id
         FROM businessthirdparty
         WHERE uuid = '1230040'),
        'HRB 31629');

INSERT INTO taxid (uuid, businessthirdparty_id, taxid)
VALUES ('a6848181-9ef4-11e7-be27-00ff0b1c94f7',
        (SELECT id
         FROM businessthirdparty
         WHERE uuid = '1230040'),
        'DE118562664');

-- Insurer
INSERT INTO businessthirdparty
(
  uuid,
  legalName,
  commercialName,
  acronym,
  businessorganisationtype_id,
  legaladdress_id,
  postaladdress_id,
  presentation_of_documents_id,
  thirdpartypaymentterms_id,
  businessfunction_id
)
VALUES (
  '1231040',
  'Euler Hermes',
  'Euler Hermes',
  'EH',
  (SELECT id
   FROM businessorganisationtype
   WHERE code = 'PRIVLLC' AND is_version IS FALSE),
  (SELECT id
   FROM postaladdress
   WHERE id = 1 AND is_version IS FALSE),
  (SELECT id
   FROM postaladdress
   WHERE id = 2 AND is_version IS FALSE),
  (SELECT id
   FROM presentation_of_documents
   WHERE code = 'TRUST' AND is_version IS FALSE),
  (SELECT id
   FROM paymentterm
   WHERE code = 'CAD' AND is_version IS FALSE),
   (SELECT id
   FROM businessfunction
   WHERE code='C-A' and is_version is false)
);

INSERT INTO commercialid (uuid, businessthirdparty_id, commercialid)
VALUES ('a6848182-9ef4-11e7-be27-00ff0b1c94f7',
        (SELECT id
         FROM businessthirdparty
         WHERE uuid = '1231040'),
        'HRB 31629');

INSERT INTO taxid (uuid, businessthirdparty_id, taxid)
VALUES ('a6848183-9ef4-11e7-be27-00ff0b1c94f7',
        (SELECT id
         FROM businessthirdparty
         WHERE uuid = '1231040'),
        'DE118562664');


/* Version */
INSERT INTO businessthirdparty (uuid, version, is_version, parent, legalName, commercialName, acronym,
                                businessorganisationtype_id,
                                legaladdress_id,
                                postaladdress_id,
                                presentation_of_documents_id,
                                thirdpartypaymentterms_id)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    legalName,
    commercialName,
    acronym,
    (SELECT businessorganisationtype.id
     FROM businessorganisationtype
     WHERE businessorganisationtype.parent = businessthirdparty.businessorganisationtype_id AND
           businessorganisationtype.is_version IS TRUE),
    (SELECT postaladdress.id
     FROM postaladdress
     WHERE postaladdress.id = businessthirdparty.legaladdress_id AND postaladdress.is_version IS TRUE),
    (SELECT postaladdress.id
     FROM postaladdress
     WHERE postaladdress.id = businessthirdparty.postaladdress_id AND postaladdress.is_version IS TRUE),
    (SELECT presentation_of_documents.id
     FROM presentation_of_documents
     WHERE presentation_of_documents.parent = businessthirdparty.presentation_of_documents_id AND
           presentation_of_documents.is_version IS TRUE),
    (SELECT paymentterm.id
     FROM paymentterm
     WHERE paymentterm.parent = businessthirdparty.thirdpartypaymentterms_id AND paymentterm.is_version IS TRUE)
  FROM businessthirdparty
  WHERE is_version IS FALSE;

INSERT INTO commercialid (uuid, version, is_version, parent, businessthirdparty_id, commercialid)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    businessthirdparty_id,
    commercialid
  FROM commercialid
  WHERE is_version IS FALSE;

INSERT INTO taxid (uuid, version, is_version, parent, businessthirdparty_id, taxid)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    businessthirdparty_id,
    taxid
  FROM taxid
  WHERE is_version IS FALSE;
  
