--changeset generated_nf:1497621355 
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2016-12-21', '1', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-01-23', '2', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-02-17', '3', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-03-23', '4', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-04-20', '5', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-05-22', '6', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-06-22', '7', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-07-21', '8', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-08-23', '9', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-09-21', '10', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-10-23', '11', '2017', 'US');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-11-21', '12', '2017', 'US');

INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-01-03', '1', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-02-01', '2', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-03-01', '3', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-04-03', '4', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-05-02', '5', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-06-01', '6', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-07-03', '7', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-08-01', '8', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-09-01', '9', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-10-02', '10', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-11-01', '11', '2017', 'GB');
INSERT INTO firstnoticedays (firstnoticeday, terminalmonth, terminalyear, market)
VALUES ('2017-12-01', '12', '2017', 'GB');
