--changeset generated_nf:1497621355 
INSERT INTO coffeequality_origin (uuid, coffeequality_id, location_id)
	VALUES (
	'1222040',
	(SELECT id FROM coffeequality WHERE name = 'Blue Mountain' and is_version is false),
	(SELECT id FROM location WHERE country_code = 'JM' AND location_code IS NULL and is_version is false)
	);

INSERT INTO coffeequality_origin (uuid, coffeequality_id, location_id)
	VALUES (
	'1223040',
	(SELECT id FROM coffeequality WHERE name = 'Vietn.Rob Vapo Standard' and is_version is false),
	(SELECT id FROM location WHERE country_code = 'VN' AND location_code IS NULL and is_version is false)
	);

