--changeset generated_nf:1497621355 
INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile 
) VALUES (
    '121040',
	'Blue Mountain',
	(SELECT id FROM coffeequality_origintype WHERE code = 'SO' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'GREGRA' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'WAARAB' AND is_version is false),
	'H.G. organic grown',
	'no blacks/brown/sour',
	'low humidity',
	'fruity'
);
INSERT INTO coffeequality (
    uuid,
	name,
        -- commercial names are one to many in 'quality_commercialname'
        coffeequality_origintype_id,
        -- origins are in many to many table 'quality_origins'
        coffee_type_id ,
        coffee_variety_id ,
        -- certificates are in many to many table 'quality_certificate'
        grade ,
        defectcount ,
        humidity ,
        cup_profile 
) VALUES (
	'122040',
	'Vietn.Rob Vapo Standard',
	(SELECT id FROM coffeequality_origintype WHERE code = 'BL' AND is_version is false),
	(SELECT id FROM coffee_type WHERE code = 'GREGRA' AND is_version is false),
	(SELECT id FROM coffee_variety WHERE code = 'ROB' AND is_version is false),
	'clean & color sorted',
	'max. 0.2% blacks, 0.1% foreign matter, 95% on screen 18',
	'',
	''
);


insert into coffeequality (uuid, version, is_version, parent, name, coffeequality_origintype_id, coffee_type_id , coffee_variety_id , grade , defectcount , humidity , cup_profile ) 
  select uuid, version, true, id, name, 
        (SELECT coffeequality_origintype.id FROM coffeequality_origintype WHERE coffeequality_origintype.parent = coffeequality.coffeequality_origintype_id AND is_version is true),
        (SELECT coffee_type.id FROM coffee_type WHERE coffee_type.parent = coffeequality.coffee_type_id AND coffee_type.is_version is true), 
        (SELECT coffee_variety.id FROM coffee_variety WHERE coffee_variety.parent = coffeequality.coffee_variety_id AND is_version is true),
        grade , defectcount , humidity , cup_profile  
  from coffeequality where is_version is false;
