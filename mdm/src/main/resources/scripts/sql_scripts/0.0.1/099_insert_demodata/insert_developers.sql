--changeset generated_nf:1497621355 
-- Give developers the developer role
INSERT INTO userrole (uuid, id, rolename)
	VALUES ('a68bad9a-9ef4-11e7-be27-00ff0b1c94f7', -1, 'developer');
INSERT INTO userrole (uuid, id, rolename)
	VALUES ('a68bad9b-9ef4-11e7-be27-00ff0b1c94f7', -2, 'test trader');
	
-- Insert developes as users
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68bad9c-9ef4-11e7-be27-00ff0b1c94f7', -1, 'nf', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68bad9d-9ef4-11e7-be27-00ff0b1c94f7', -2, 'elv', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68bad9e-9ef4-11e7-be27-00ff0b1c94f7', -3, 'af', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68bad9f-9ef4-11e7-be27-00ff0b1c94f7', -4, 'app-bric', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68bada0-9ef4-11e7-be27-00ff0b1c94f7', -5, 'app-ceis', -2);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68bada1-9ef4-11e7-be27-00ff0b1c94f7', -6, 'sl', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68df792-9ef4-11e7-be27-00ff0b1c94f7', -7, 'cva', -1);
INSERT INTO userprofile (uuid, id, username, userrole_id)
	VALUES ('a68df793-9ef4-11e7-be27-00ff0b1c94f7', -8, 'tst', -1);


-- Assign developers to developer company
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
	VALUES ('a68df794-9ef4-11e7-be27-00ff0b1c94f7', -1, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
	VALUES ('a68df795-9ef4-11e7-be27-00ff0b1c94f7', -2, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
	VALUES ('a68df796-9ef4-11e7-be27-00ff0b1c94f7', -3, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
	VALUES ('a68df797-9ef4-11e7-be27-00ff0b1c94f7', -4, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
	VALUES ('a68df798-9ef4-11e7-be27-00ff0b1c94f7', -5, 2);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
    VALUES ('a68df799-9ef4-11e7-be27-00ff0b1c94f7', -6, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
    VALUES ('a68df79a-9ef4-11e7-be27-00ff0b1c94f7', -7, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
    VALUES ('a68df79b-9ef4-11e7-be27-00ff0b1c94f7', -8, 1);
INSERT INTO userprofile_groupcompany (uuid, userprofile_id, groupcompany_id)
    VALUES ('a68df79c-9ef4-11e7-be27-00ff0b1c94f7', -8, 2);

