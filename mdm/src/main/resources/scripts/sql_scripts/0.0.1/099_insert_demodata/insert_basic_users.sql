--changeset generated_nf:1497621355 
INSERT INTO userrole (uuid, rolename, admin)
	VALUES ('a6893c8e-9ef4-11e7-be27-00ff0b1c94f7', 'edwh_admin', true);
	
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a68bad8f-9ef4-11e7-be27-00ff0b1c94f7', 'rodriguez', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a68bad90-9ef4-11e7-be27-00ff0b1c94f7', 'foede', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a68bad91-9ef4-11e7-be27-00ff0b1c94f7', 'elvers', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
	VALUES ('a68bad92-9ef4-11e7-be27-00ff0b1c94f7', 'finck', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a68bad93-9ef4-11e7-be27-00ff0b1c94f7', 'lips', (select id from userrole where rolename = 'edwh_admin'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a68bad94-9ef4-11e7-be27-00ff0b1c94f7', 'vatterodt', (select id from userrole where rolename = 'edwh_admin'));

INSERT INTO userrole (uuid, rolename, admin)
    VALUES ('a68bad95-9ef4-11e7-be27-00ff0b1c94f7', 'edwh_technical', false);
    
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a68bad96-9ef4-11e7-be27-00ff0b1c94f7', 'comtras', (select id from userrole where rolename = 'edwh_technical'));
INSERT INTO userprofile (uuid, username, userrole_id)
    VALUES ('a68bad97-9ef4-11e7-be27-00ff0b1c94f7', 'vcms', (select id from userrole where rolename = 'edwh_technical'));