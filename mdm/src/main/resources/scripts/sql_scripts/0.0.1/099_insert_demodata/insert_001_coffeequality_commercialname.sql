--changeset generated_nf:1497621355 
INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1218040',
	(SELECT id FROM coffeequality WHERE name = 'Blue Mountain' and is_version is false),
	'Jamaica Blue Mountain'
);
INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1219040',
	(SELECT id FROM coffeequality WHERE name = 'blurry blendy blendy' and is_version is false),
	'Mc. Master Blurr'
);
INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1220040',
	(SELECT id FROM coffeequality WHERE name = 'blurry blendy blendy' and is_version is false),
	'bluRRR'
);
INSERT INTO coffeequality_commercialname(
    uuid,
	coffeequality_id,
	name
) VALUES (
    '1221040',
	(SELECT id FROM coffeequality WHERE name = 'blurry blendy blendy' and is_version is false),
	'blurator'
);



insert into coffeequality_commercialname (uuid, version, is_version, parent, coffeequality_id, name) 
  select uuid, version, true, id, 
        (SELECT coffeequality.id FROM coffeequality WHERE coffeequality.parent = coffeequality_commercialname.coffeequality_id AND coffeequality.is_version is true), 
        name from coffeequality_commercialname where is_version is false;

