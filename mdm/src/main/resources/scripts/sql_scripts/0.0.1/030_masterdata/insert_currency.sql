--changeset generated_nf:1497621355 
INSERT INTO currency (uuid, currency)
	VALUES ('a6716f24-9ef4-11e7-be27-00ff0b1c94f7','USD');
INSERT INTO currency (uuid, currency)
	VALUES ('a6716f25-9ef4-11e7-be27-00ff0b1c94f7','EUR');

insert into currency (uuid, version, is_version, parent, currency) 
  select uuid, version, true, id, currency from currency where is_version is false;