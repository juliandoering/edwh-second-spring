--changeset generated_nf:1497858527 
INSERT INTO compliancerating_value (uuid, code, compliancerating) VALUES ('a667d1fe-9ef4-11e7-be27-00ff0b1c94f7', 'HIGH','HIGH');
INSERT INTO compliancerating_value (uuid, code, compliancerating) VALUES ('a667d1ff-9ef4-11e7-be27-00ff0b1c94f7', 'MEDIUM','MEDIUM');
INSERT INTO compliancerating_value (uuid, code, compliancerating) VALUES ('a667d200-9ef4-11e7-be27-00ff0b1c94f7', 'LOW','LOW');
INSERT INTO compliancerating_value (uuid, code, compliancerating) VALUES ('a667d201-9ef4-11e7-be27-00ff0b1c94f7', 'UNRATED','Not rated yet');
INSERT INTO compliancerating_value (uuid, code, compliancerating) VALUES ('a667d202-9ef4-11e7-be27-00ff0b1c94f7', 'REJECTED','REJECTED');

insert into compliancerating_value (uuid, version, is_version, parent, code, compliancerating) 
  select uuid, version, true, id, code, compliancerating from compliancerating_value where is_version is false;