--changeset generated_nf:1497621355 
/*
 * Script to insert freemarker templates that are used to build e.g. email notification text
 * */

/* <CONTRACT> */
-- subject for buyer
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1249030',
  -- key
  'contract.update.subject.buyer',
  -- template
  '[Contract] Contract ${(contract.buyer.transactionReference)!"(?)"} (${(contract.longId?c)!"?"}) was updated.',
  -- template description
  'Contract updated subject for buyer');

-- subject for seller
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1251030',
  -- key
  'contract.update.subject.seller',
  -- template
  '[Contract] Contract ${(contract.seller.transactionReference)!"(?)"} (${(contract.longId?c)!"?"}) was updated.',
  -- template description
  'Contract updated subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1252030',
  -- key
  'contract.update.body.long',
  -- template
  'There are updates for contract

  buyer: ${(contract.buyer.identification.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(contract.buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(contract.seller.identification.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(contract.seller.transactionReference)!"(no seller''s contract ID provided!)"}


  ${UriSelf} ',
  -- template description
  'Contract update email body with a long description');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1253030',
  -- key
  'contract.created.subject.buyer',
  -- template
  '[Contract] New Contract "${(contract.buyer.transactionReference)!""}" (${(contract.longId?c)!"?"}) created.',
  -- template description
  'Contract created email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1254030',
  -- key
  'contract.created.subject.seller',
  -- template
  '[Contract] New Contract "${(contract.seller.transactionReference)!""} (${(contract.longId?c)!"?"})" created.',
  -- template description
  'Contract created email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1255030',
  -- key
  'contract.created.body.long',
  -- template
  'There is a new contract

  buyer: ${(contract.buyer.identification.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(contract.buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(contract.seller.identification.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(contract.seller.transactionReference)!"(no seller''s contract ID provided!)"}


  ${UriSelf} ',
  -- template description
  'New contract email body with a long description');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1256030',
  -- key
  'contract.download.confirmation.seller.subject',
  -- template
  '[Contract] Download of contract "${(entity.seller.transactionReference)!""} (${(entity.longId?c)!"?"})" has been confirmed.',
  -- template description
  'Contract download confirmation email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1257030',
  -- key
  'contract.download.confirmation.buyer.subject',
  -- template
  '[Contract] Download of contract "${(entity.buyer.transactionReference)!""} (${(entity.longId?c)!"?"})" has been confirmed.',
  -- template description
  'Contract download confirmation email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1258030',
  -- key
  'contract.download.confirmation.body',
  -- template
  'The contract download has been confirmed

  ${UriSelf} ',
  -- template description
  'Contract download confirmation email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1259030',
  -- key
  'contract.bulk.upload.conflict.subject',
  -- template
  '[Contract] Bulk upload of contract "${(entity.buyer.transactionReference)!""} (${(entity.longId?c)!"?"})" is not consistent.',
  -- template description
  'Contract bulk upload conflict email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1260030',
  -- key
  'contract.bulk.upload.conflict.body',
  -- template
  'The contract has been uploaded by the second counterpart but the content differs.

  ${UriSelf} ',
  -- template description
  'Contract bulk upload conflict email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12601030',
  -- key
  'contract.created.warning.subject.buyer',
  -- template
  '[Contract] New Contract "${(contract.buyer.transactionReference)!""}" (${(contract.longId?c)!"?"}) with warning created.',
  -- template description
  'Contract created with warning email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12602030',
  -- key
  'contract.created.warning.subject.seller',
  -- template
  '[Contract] New Contract "${(contract.seller.transactionReference)!""} (${(contract.longId?c)!"?"}) with warning created.',
  -- template description
  'Contract created with warning email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12603030',
  -- key
  'contract.created.warning.body',
  -- template
  'The contract that was uploaded contains unexpected but still acceptable data.

  ${UriSelf} ' ||
  'Please consider the following warning(s):',
  -- template description
  'New contract with warning email body');
/* </CONTRACT> */

/* <PRICEFIXATION> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1261030',
  -- key
  'fixation.created.subject.buyer',
  -- template
  'New fixation for contract "${entity.contract.buyer.transactionReference}" created.',
  -- template description
  'New price fixation notification email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1262030',
  -- key
  'fixation.created.subject.seller',
  -- template
  'New fixation for contract "${entity.contract.seller.transactionReference}" created.',
  -- template description
  'New price fixation notification email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1263030',
  -- key
  'fixation.created.body',
  -- template
  'There is a new price fixation:

  ${UriSelf} ',
  -- template description
  'New price fixation notification email body with description');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1264030',
  -- key
  'fixation.download.confirmation.subject.buyer',
  -- template
  'The download of fixation with number ${entity.fixationNumber} for contract "${entity.contract.buyer.transactionReference}" has been confirmed.',
  -- template description
  'Price fixation download confirmation notification email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1265030',
  -- key
  'fixation.download.confirmation.subject.seller',
  -- template
  'The download of fixation with number ${entity.fixationNumber} for contract "${entity.contract.seller.transactionReference}" has been confirmed.',
  -- template description
  'Price fixation download confirmation notification email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1266030',
  -- key
  'fixation.download.confirmation.body',
  -- template
  'The price fixation has been downloaded successfully:

  ${UriSelf} ',
  -- template description
  'Price fixation download confirmation notification email body with description');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1267030',
  -- key
  'fixation.upload.existed.subject.buyer',
  -- template
  '[Price Fixation] Price Fixation "${entity.contract.buyer.transactionReference}" already uploaded',
  -- template description
  'second upload of existing price fixation email subject for buyers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1268030',
  -- key
  'fixation.upload.existed.subject.seller',
  -- template
  '[Price Fixation] Price Fixation "${entity.contract.seller.transactionReference}" already uploaded',
  -- template description
  'second upload of existing price fixation email subject for sellers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1269030',
  -- key
  'fixation.upload.existed.body',
  -- template
  'The Price Fixation that was uploaded was already uploaded by the counterpart

  ${UriSelf}',
  -- template description
  'upload of existing price fixation not necessary');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1270030',
  -- key
  'fixation.upload.conflict.subject.buyer',
  -- template
  '[Price Fixation] Uploaded Price Fixation "${entity.contract.buyer.transactionReference}" in conflict' ||
  ' with already uploaded Price Fixation',
  -- template description
  'conflicting upload of existing price fixation email subject for buyers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1271030',
  -- key
  'fixation.upload.conflict.subject.seller',
  -- template
  '[Price Fixation] Uploaded Price Fixation "${entity.contract.seller.transactionReference}" in conflict' ||
  ' with already uploaded Price Fixation',
  -- template description
  'conflicting upload of existing price fixation email subject for sellers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1272030',
  -- key
  'fixation.upload.conflict.subject.admin',
  -- template
  '[Price Fixation] Uploaded Price Fixation in conflict' ||
  ' with already uploaded Price Fixation',
  -- template description
  'conflicting upload of existing price fixation email subject for admins');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1273030',
  -- key
  'fixation.upload.conflict.body',
  -- template
  'The Price Fixation that was uploaded is not identical with previously uploaded Price Fixations. Please resolve' ||
  'this conflict and adjust the data if necessary

  ${UriSelf}',
  -- template description
  'conflicting upload of existing price fixation body');
/* </PRICEFIXATION> */

/* <SHIPPING INSTRUCTIONS> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1274030',
  -- key
  'shipping.instructions.created.subject.seller',
  -- template
  '[Shipment] New shipping instructions "${entity.shippingInstructionNumber}" created',
  -- template description
  'New shipping instructions email subject for seller (only sellers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1275030',
  -- key
  'shipping.instructions.created.subject.buyer',
  -- template
  '[Shipment] New shipping instructions "${entity.shippingInstructionNumber}" created',
  -- template description
  'New shipping instructions email subject for buyer (only buyer get mail)');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1276030',
  -- key
  'shipping.instructions.created.body',
  -- template
  'A new shipping instructions has been created

  ${UriSelf}
  ',
  -- template description
  'New shipping instructions email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1277030',
  -- key
  'shipping.instructions.updated.subject.seller',
  -- template
  '[Shipping] Shipping instructions "${entity.shippingInstructionNumber}" updated',
  -- template description
  'update shipping instructions email subject for seller (only sellers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1278030',
  -- key
  'shipping.instructions.updated.subject.buyer',
  -- template
  '[Shipping] Shipping instructions "${entity.shippingInstructionNumber}" updated',
  -- template description
  'update shipping instructions email subject for buyer (only buyers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1279030',
  -- key
  'shipping.instructions.updated.body',
  -- template
  'A shipping instructions was updated

  ${UriSelf}',
  -- template description
  'update shipping instructions email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1280030',
  -- key
  'shipping.instructions.download.confirmation.subject.buyer',
  -- template
  'The download of shipping instructions with number ${entity.shippingInstructionNumber} for contract "${entity.contract.buyer.transactionReference}" has been confirmed.',
  -- template description
  'Shipping instructions download confirmation notification email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1281030',
  -- key
  'shipping.instructions.download.confirmation.subject.seller',
  -- template
  'The download of shipping instructions with number ${entity.shippingInstructionNumber} for contract "${entity.contract.seller.transactionReference}" has been confirmed.',
  -- template description
  'Shipping instructions download confirmation notification email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1282030',
  -- key
  'shipping.instructions.download.confirmation.body',
  -- template
  'The shipping instructions have been downloaded successfully:

  ${UriSelf} ',
  -- template description
  'Shipping instructions download confirmation notification email body with description');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1283030',
  -- key
  'shipping.instructions.upload.existed.subject.buyer',
  -- template
  'The shipping instruction with number ${entity.shippingInstructionNumber} for contract "${entity.contract.buyer.transactionReference}" already exists',
  -- template description
  'Shipping instructions second upload email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1284030',
  -- key
  'shipping.instructions.upload.existed.subject.seller',
  -- template
  'The shipping instruction with number ${entity.shippingInstructionNumber} for contract "${entity.contract.seller.transactionReference}" already exists',
  -- template description
  'Shipping instructions second upload email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1285030',
  -- key
  'shipping.instructions.upload.existed.body',
  -- template
  'The shipping instruction you have uploaded already exists:

  ${UriSelf} ',
  -- template description
  'Shipping instructions second upload');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1286030',
  -- key
  'shipping.instructions.upload.conflict.subject.buyer',
  -- template
  'Uploaded shipping instruction ${entity.shippingInstructionNumber} for contract "${entity.contract.buyer.transactionReference}" in conflict',
  -- template description
  'Shipping instructions upload conflict email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1287030',
  -- key
  'shipping.instructions.upload.conflict.subject.seller',
  -- template
  'Uploaded shipping instruction ${entity.shippingInstructionNumber} for contract "${entity.contract.seller.transactionReference}" in conflict',
  -- template description
  'Shipping instructions upload conflict email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1288030',
  -- key
  'shipping.instructions.upload.conflict.subject.admin',
  -- template
  'Uploaded shipping instruction in conflict with already uploaded shipping instruction',
  -- template description
  'Shipping instructions upload conflict email subject for admin');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1289030',
  -- key
  'shipping.instructions.upload.conflict.body',
  -- template
  'The shipping instruction you have is in conflict with another shipping instruction that was uploaded previously:

  ${UriSelf} ',
  -- template description
  'Shipping instructions upload conflict');

/* </SHIPPING INSTRUCTIONS> */

/* <SHIPMENTADVICE> */
INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1290030',
-- key
'shipmentadvice.created.subject.buyer', 
-- template
'[Shipment] New Shipmentadvice "${(shipmentadvice.longId?c)!"?"}" created',
-- template description
'New shipmentadvice email subject for buyer (only buyers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1291030',
-- key
'shipmentadvice.created.subject.seller', 
-- template
'[Shipment] New Shipmentadvice "${(shipmentadvice.longId?c)!"?"}" created',
-- template description
'New shipmentadvice email subject for seller (only sellers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1292030',
-- key
'shipmentadvice.created.body', 
-- template
'A new shipmentadvice has been created

  ${UriSelf}
  ',
  -- template description
  'New shipmentadvice email body for buyer');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1293030',
-- key
'shipmentadvice.updated.subject.buyer', 
-- template
'[Shipment] Shipmentadvice "${(shipmentadvice.longId?c)!"?"}" updated',
-- template description
'update shipmentadvice email subject for buyer (only buyers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1294030',
-- key
'shipmentadvice.updated.subject.seller', 
-- template
'[Shipment] Shipmentadvice "${(shipmentadvice.longId?c)!"?"}" updated',
-- template description
'update shipmentadvice email subject for seller (only sellers get mail)');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1295030',
-- key
'shipmentadvice.updated.body', 
-- template
'A shipment advice was updated

  ${UriSelf}',
  -- template description
  'update shipmentadvice email body for buyer');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1296030',
-- key
'shipmentadvice.download.confirmation.subject.buyer', 
-- template
'The download of shipment advice with number ${entity.sequentialNumber} for contract "${entity.contract.buyer.transactionReference}" has been confirmed.',
-- template description
'Shipment advice download confirmation notification email subject for buyer');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1297030',
-- key
'shipmentadvice.download.confirmation.subject.seller', 
-- template
'The download of shipment advice with number ${entity.sequentialNumber} for contract "${entity.contract.seller.transactionReference}" has been confirmed.',
-- template description
'Shipment advice download confirmation notification email subject for seller');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '1298030',
-- key
'shipmentadvice.download.confirmation.body', 
-- template
'The shipment advice download has been confirmed 
 
${UriSelf} ',
-- template description
'Shipment advice download confirmation email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '1299030',
  -- key
  'shipmentadvice.upload.conflict.subject.buyer',
  -- template
  '[Shipment] Uploaded Shipment Advice "${entity.contract.buyer.transactionReference}" in conflict' ||
  ' with already uploaded Shipment Advice',
  -- template description
  'conflicting upload of existing Shipment Advice email subject for buyers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12100030',
  -- key
  'shipmentadvice.upload.conflict.subject.seller',
  -- template
  '[Shipment] Uploaded Shipment Advice "${entity.contract.seller.transactionReference}" in conflict' ||
  ' with already uploaded Shipment Advice',
  -- template description
  'conflicting upload of existing Shipment Advice email subject for sellers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12101030',
  -- key
  'shipmentadvice.upload.conflict.subject.admin',
  -- template
  '[Shipment] Uploaded Shipment Advice in conflict' ||
  ' with already uploaded Shipment Advice',
  -- template description
  'conflicting upload of existing Shipment Advice email subject for admins');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12102030',
  -- key
  'shipmentadvice.upload.conflict.body',
  -- template
  'The Shipment Advices that was uploaded is not identical with previously uploaded Shipment Advices. Please resolve' ||
  'this conflict and adjust the data if necessary

  ${UriSelf}',
  -- template description
  'conflicting upload of existing Shipment Advices body');


INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12103030',
  -- key
  'shipmentadvice.upload.existed.subject.buyer',
  -- template
  '[Shipment] Shipment Advice "${entity.contract.buyer.transactionReference}" already uploaded',
  -- template description
  'second upload of existing Shipment Advice email subject for buyers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12104030',
  -- key
  'shipmentadvice.upload.existed.subject.seller',
  -- template
  '[Shipment] Shipment Advice "${entity.contract.seller.transactionReference}" already uploaded',
  -- template description
  'second upload of existing Shipment Advice email subject for sellers');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12105030',
  -- key
  'shipmentadvice.upload.existed.body',
  -- template
  'The Shipment Advice that was uploaded was already uploaded by the counterpart

  ${UriSelf}',
  -- template description
  'upload of existing Shipment Advice not necessary');
/* </SHIPMENTADVICE> */

/* <BUSINESS THIRD PARTY> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12106030',
  -- key
  'businessthirdparty.updated.body',
  -- template
  'A business third party was updated

  ${(entity.legalName)!""}
  ${(entity.legalAddress)!""}
  ${UriSelf}',
  -- template description
  'update business third party email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12107030',
  -- key
  'businessthirdparty.created.body',
  -- template
  'A business third party entry was created

  ${(entity.legalName)!""}
  ${(entity.legalAddress)!""}
  ${UriSelf}',
  -- template description
  'new business third party email body');
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12108030',
  -- key
  'businessthirdparty.updated.subject',
  -- template
  '[business third party]updated',
  -- template description
  'update business third party email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12109030',
  -- key
  'businessthirdparty.created.subject',
  -- template
  '[business third party] created',
  -- template description
  'new business third party email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12110030',
  -- key
  'businessthirdparty.deleted.body',
  -- template
  'A business third party entry was deleted

  ${(entity.legalName)!""}
  ${(entity.legalAddress)!""}',
  -- template description
  'delete business third party email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '12111030',
  -- key
  'businessthirdparty.deleted.subject',
  -- template
  '[business third party]deleted',
  -- template description
  'delete business third party email subject');
/* </BUSINESS THIRD PARTY> */