--changeset generated_nf:1497621355 
INSERT INTO countryqualifier (uuid, code, description)
	VALUES ('a66efe1e-9ef4-11e7-be27-00ff0b1c94f7','27', 'Country of Origin, which is the name of the country where items have been produced, manufactured or have undergone a substantial transformation.');
INSERT INTO countryqualifier (uuid, code, description)
	VALUES ('a66efe1f-9ef4-11e7-be27-00ff0b1c94f7','35', 'Country of Dispatch, i.e. the Country from which goods were initially dispatched.');
INSERT INTO countryqualifier (uuid, code, description)
	VALUES ('a66efe20-9ef4-11e7-be27-00ff0b1c94f7','28', 'Country of Destination, which is the name of a country where final delivery of items are made. For example, the name of the country where goods are to be finally delivered even if the voyage includes several stops.');
INSERT INTO countryqualifier (uuid, code, description)
	VALUES ('a66efe21-9ef4-11e7-be27-00ff0b1c94f7','89', 'Country of Registration, i.e. the Nationality of the means of transport.');
INSERT INTO countryqualifier (uuid, code, description)
	VALUES ('a6716f22-9ef4-11e7-be27-00ff0b1c94f7','149', 'From Country, which is the country, from which a sea or air voyage for the transportation of goods is stated to commence.');

insert into countryqualifier (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from countryqualifier where is_version is false;