--changeset generated_nf:1497858527 
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0521-9ef4-11e7-b90a-00ff0b1c94f7', 'CS-P', 'Coffee Supplier – Producer', 'Coffee Supplier');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0522-9ef4-11e7-b90a-00ff0b1c94f7', 'CS-I', 'Coffee Supplier – Intermediary', 'Coffee Supplier');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0523-9ef4-11e7-b90a-00ff0b1c94f7', 'CS-PI', 'Coffee Supplier – Producer/Intermediary', 'Coffee Supplier');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0524-9ef4-11e7-b90a-00ff0b1c94f7', 'CC-IND', 'Coffee Client – Industry', 'Coffee Client');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0525-9ef4-11e7-b90a-00ff0b1c94f7', 'CC-INT', 'Coffee Client – Intermediary', 'Coffee Client');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0526-9ef4-11e7-b90a-00ff0b1c94f7', 'CC-II', 'Coffee Client – Industry/Intermediary', 'Coffee Client');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0527-9ef4-11e7-b90a-00ff0b1c94f7', 'OG-S', 'Other goods Vendor/Supplier/Seller', 'Other goods');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0528-9ef4-11e7-b90a-00ff0b1c94f7', 'OG-C', 'Other goods Client/Buyer/Purchaser', 'Other goods');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d0529-9ef4-11e7-b90a-00ff0b1c94f7', 'S-S', 'Service Vendor/Supplier/Seller', 'Service');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d052a-9ef4-11e7-b90a-00ff0b1c94f7', 'S-C', 'Service Client/Buyer/Purchaser', 'Service');
INSERT INTO businessfunction (uuid, code, name, category) VALUES ('4f1d052b-9ef4-11e7-b90a-00ff0b1c94f7', 'C-A', 'Coffee Agent', 'Agent');

insert into businessfunction (uuid, version, is_version, parent, code, name, category) 
  select uuid, version, true, id, code, name, category from businessfunction where is_version is false;
