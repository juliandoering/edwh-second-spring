-- Add Seller relation
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2796ecc4-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'S' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CS-P' and is_version is false));
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('279761f5-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'S' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CS-I' and is_version is false));
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2797b016-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'S' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CS-PI' and is_version is false));

-- Add Shipper relation
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2797b017-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'SH' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CS-P' and is_version is false));
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2797d728-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'SH' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CS-I' and is_version is false));
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('27982549-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'SH' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CS-PI' and is_version is false));

-- Add Buyer relation   
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('27984c5a-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'B' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CC-IND' and is_version is false));
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2798736b-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'B' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CC-INT' and is_version is false));
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2798c18c-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'B' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'CC-II' and is_version is false));

-- Add Agent relation
INSERT INTO contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id)
    VALUES ('2798e89d-9f72-11e7-a4ff-00ff0b1c94f7',
        (SELECT id FROM buyersellercode WHERE code = 'A' and is_version is false),
        (SELECT id FROM businessfunction WHERE code = 'C-A' and is_version is false));

-- version
insert into contractrole_businessfunction (uuid, buyersellercode_id, businessfunction_id) 
  select
        uuid,
        (SELECT id FROM buyersellercode WHERE buyersellercode.parent = contractrole_businessfunction.buyersellercode_id and buyersellercode.is_version is true), 
        (SELECT id FROM businessfunction WHERE businessfunction.parent = contractrole_businessfunction.businessfunction_id and businessfunction.is_version is true) 
  from contractrole_businessfunction;