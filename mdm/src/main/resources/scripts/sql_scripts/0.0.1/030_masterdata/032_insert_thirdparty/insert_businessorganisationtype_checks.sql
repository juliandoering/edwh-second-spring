INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('SP', FALSE, FALSE, FALSE, FALSE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('LP', TRUE, FALSE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('PS', TRUE, FALSE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('PRIVLLC', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('PUBLLC', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('SOC', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('COOP', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('PRIVF', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('PUBF', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('NPA', FALSE, TRUE, TRUE, TRUE);
INSERT INTO businessorganisationtype_checks (businesstypecode, checkowner, checkbeneficiary, checkmanagement_representatives, checkcontactperson)
VALUES ('TRUST', FALSE, TRUE, TRUE, TRUE);
