--changeset generated_nf:1497621355 
INSERT INTO coffeeweightscode (uuid, code, description)
VALUES ('a66cb417-9ef4-11e7-be27-00ff0b1c94f7', 'W 0.5%', 'Ship weights at 0.5%');
INSERT INTO coffeeweightscode (uuid, code, description)
VALUES ('a66cb418-9ef4-11e7-be27-00ff0b1c94f7', 'SW 1%', 'Ship weights at 1%');
INSERT INTO coffeeweightscode (uuid, code, description)
VALUES ('a66cb419-9ef4-11e7-be27-00ff0b1c94f7', 'LW', 'Landed weights');
INSERT INTO coffeeweightscode (uuid, code, description)
VALUES ('a66cb41a-9ef4-11e7-be27-00ff0b1c94f7', 'SiW', 'Silo weights');
INSERT INTO coffeeweightscode (uuid, code, description)
VALUES ('a66cb41b-9ef4-11e7-be27-00ff0b1c94f7', 'PW', 'Plant weights');
INSERT INTO coffeeweightscode (uuid, code, description)
VALUES ('a66cb41c-9ef4-11e7-be27-00ff0b1c94f7', 'RW', 'Re-weights');

INSERT INTO coffeeweightscode (uuid, version, is_version, parent, code, description)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    code,
    description
  FROM coffeeweightscode
  WHERE is_version IS FALSE;
