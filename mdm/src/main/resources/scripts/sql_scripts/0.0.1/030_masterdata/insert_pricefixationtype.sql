--changeset generated_nf:1497621355 
/* There are enum values corresponding to this table - be aware and check them before editing */
INSERT INTO pricefixationtype (uuid, pricefixationtype, description)
	VALUES ('a67ae545-9ef4-11e7-be27-00ff0b1c94f7', 'PTBF','Price To Be Fixed');

INSERT INTO pricefixationtype (uuid, pricefixationtype, description)
	VALUES ('a67ae546-9ef4-11e7-be27-00ff0b1c94f7', 'Outright','Outright contracts');

INSERT INTO pricefixationtype (uuid, pricefixationtype, description)
	VALUES ('a67ae547-9ef4-11e7-be27-00ff0b1c94f7', 'PTF','Price To Follow');

INSERT INTO pricefixationtype (uuid, pricefixationtype, description)
	VALUES ('a67ae548-9ef4-11e7-be27-00ff0b1c94f7', 'DTBF','Differential To Be Fixed');

insert into pricefixationtype (uuid, version, is_version, parent, pricefixationtype, description) 
  select uuid, version, true, id, pricefixationtype, description from pricefixationtype where is_version is false;