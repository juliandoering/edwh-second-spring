--changeset generated_nf:1497621355 
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a673b92f-9ef4-11e7-be27-00ff0b1c94f7', 'EXW', 'Ex Works');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a673b930-9ef4-11e7-be27-00ff0b1c94f7', 'FCA', 'Free Carrier');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a673b931-9ef4-11e7-be27-00ff0b1c94f7', 'FAS', 'Free Alongside Ship');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a673b932-9ef4-11e7-be27-00ff0b1c94f7', 'FOB', 'Free On Board');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a33-9ef4-11e7-be27-00ff0b1c94f7', 'CFR', 'Cost And Freight');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a34-9ef4-11e7-be27-00ff0b1c94f7', 'CIF', 'Cost Insurance Freight');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a35-9ef4-11e7-be27-00ff0b1c94f7', 'DAT', 'Delivered At Terminal');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a36-9ef4-11e7-be27-00ff0b1c94f7', 'DAP', 'Delivered At Place');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a37-9ef4-11e7-be27-00ff0b1c94f7', 'CPT', 'Carriage Paid To');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a38-9ef4-11e7-be27-00ff0b1c94f7', 'CIP', 'Carriage Insurance Paid');
INSERT INTO incoterm (uuid, code, description)
	VALUES ('a6762a39-9ef4-11e7-be27-00ff0b1c94f7', 'DDP', 'Delivered Duty Paid');

insert into incoterm (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from incoterm where is_version is false;