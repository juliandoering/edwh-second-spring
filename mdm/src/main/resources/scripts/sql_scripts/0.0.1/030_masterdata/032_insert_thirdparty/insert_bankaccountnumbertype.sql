--changeset generated_nf:1497858527 
INSERT INTO bankaccount_number_type (uuid, typename) VALUES ('814714b4-9ef3-11e7-a20c-00ff0b1c94f7', 'IBAN');
INSERT INTO bankaccount_number_type (uuid, typename) VALUES ('814714b5-9ef3-11e7-a20c-00ff0b1c94f7', 'BIC+NUMBER');
INSERT INTO bankaccount_number_type (uuid, typename) VALUES ('814714b6-9ef3-11e7-a20c-00ff0b1c94f7', 'OTHER');

insert into bankaccount_number_type (uuid, version, is_version, parent, typename) 
  select uuid, version, true, id, typename from bankaccount_number_type where is_version is false;