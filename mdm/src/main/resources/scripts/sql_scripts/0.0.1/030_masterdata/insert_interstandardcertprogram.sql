--changeset generated_nf:1497621355 
INSERT INTO interstandardcertprogram (uuid, code, name)
	VALUES ('a6762a3b-9ef4-11e7-be27-00ff0b1c94f7', 'ISO', 'ISO');
INSERT INTO interstandardcertprogram (uuid, code, name)
	VALUES ('a6762a3c-9ef4-11e7-be27-00ff0b1c94f7', 'BRC', 'BRC');
INSERT INTO interstandardcertprogram (uuid, code, name)
	VALUES ('a6762a3d-9ef4-11e7-be27-00ff0b1c94f7', 'IFS', 'IFS');
INSERT INTO interstandardcertprogram (uuid, code, name)
	VALUES ('a6762a3e-9ef4-11e7-be27-00ff0b1c94f7', 'HACCP', 'HACCP');
INSERT INTO interstandardcertprogram (uuid, code, name)
	VALUES ('a6762a3f-9ef4-11e7-be27-00ff0b1c94f7', 'TCPAT', 'TCPAT');
INSERT INTO interstandardcertprogram (uuid, code, name)
	VALUES ('a6789b40-9ef4-11e7-be27-00ff0b1c94f7', 'OHSAS', 'OHSAS');

insert into interstandardcertprogram (uuid, version, is_version, parent, code, name) 
  select uuid, version, true, id, code, name from interstandardcertprogram where is_version is false;
  