--changeset generated_nf:1497621355 
-- TODO verify the codes and replace with the real values
INSERT INTO fixationoption (uuid, code, description)
	VALUES ('a6716f29-9ef4-11e7-be27-00ff0b1c94f7','BiS', 'Buyers Call in Sellers Account');
INSERT INTO fixationoption (uuid, code, description)
	VALUES ('a6716f2a-9ef4-11e7-be27-00ff0b1c94f7','SiB', 'Sellers Call in Buyers Account');
INSERT INTO fixationoption (uuid, code, description)
	VALUES ('a6716f2b-9ef4-11e7-be27-00ff0b1c94f7','AA', 'Against Actuals');

insert into fixationoption (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from fixationoption where is_version is false;