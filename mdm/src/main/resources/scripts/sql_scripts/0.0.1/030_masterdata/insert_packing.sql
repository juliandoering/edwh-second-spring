--changeset generated_nf:1497621355 
INSERT INTO packing (uuid, packingtype)
VALUES ('a6789b42-9ef4-11e7-be27-00ff0b1c94f7', '60kg Jute Bags');
INSERT INTO packing (uuid, packingtype)
VALUES ('a6789b43-9ef4-11e7-be27-00ff0b1c94f7', '69kg Jute Bags');
INSERT INTO packing (uuid, packingtype)
VALUES ('a6789b44-9ef4-11e7-be27-00ff0b1c94f7', '70kg Jute Bags');
INSERT INTO packing (uuid, packingtype)
VALUES ('a6789b45-9ef4-11e7-be27-00ff0b1c94f7', '1MT PP Big-Bags');
INSERT INTO packing (uuid, packingtype)
VALUES ('a6789b46-9ef4-11e7-be27-00ff0b1c94f7', '21MT PP Container-Liners');

INSERT INTO packing (uuid, version, is_version, parent, packingtype)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    packingtype
  FROM packing
  WHERE is_version IS FALSE;