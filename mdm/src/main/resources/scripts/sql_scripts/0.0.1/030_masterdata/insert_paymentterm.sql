--changeset generated_nf:1497621355 
-- TODO verify code and replace with the correct values
INSERT INTO paymentterm (uuid, code, description)
	VALUES ('a6789b48-9ef4-11e7-be27-00ff0b1c94f7', 'PP', 'Prepayment');
INSERT INTO paymentterm (uuid, code, description)
	VALUES ('a6789b49-9ef4-11e7-be27-00ff0b1c94f7', 'CADBD', 'Cash Against presentation of documents before delivery');
INSERT INTO paymentterm (uuid, code, description)
	VALUES ('a6789b4a-9ef4-11e7-be27-00ff0b1c94f7', 'CAD', 'Cash Against presentation of documents');
INSERT INTO paymentterm (uuid, code, description)
	VALUES ('a6789b4b-9ef4-11e7-be27-00ff0b1c94f7', 'LOC', 'Letter of Credit');
INSERT INTO paymentterm (uuid, code, description)
	VALUES ('a6789b4c-9ef4-11e7-be27-00ff0b1c94f7', 'CC', 'Commercial credit');
INSERT INTO paymentterm (uuid, code, description)
	VALUES ('a6789b4d-9ef4-11e7-be27-00ff0b1c94f7', 'OSD', 'On specific Date');

insert into paymentterm (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from paymentterm where is_version is false;
