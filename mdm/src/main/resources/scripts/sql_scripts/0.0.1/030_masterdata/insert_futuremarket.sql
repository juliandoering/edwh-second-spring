--changeset generated_nf:1497621355 
INSERT INTO futuremarket (uuid, code, description)
	VALUES ('a6716f2e-9ef4-11e7-be27-00ff0b1c94f7', 'KC', 'New York Board of Trade Coffee Contract');
INSERT INTO futuremarket (uuid, code, description)
	VALUES ('a6716f2f-9ef4-11e7-be27-00ff0b1c94f7', 'LKD', 'London Liffe Robusta Futures');

INSERT INTO futuremarket (uuid, version, is_version, parent, code, description)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    code,
    description
  FROM futuremarket
  WHERE is_version IS FALSE;
