--changeset generated_nf:1497621355 
INSERT INTO coffee_variety(uuid,code,name) 
	VALUES ('a66cb41f-9ef4-11e7-be27-00ff0b1c94f7','WAARAB','Washed Arabica');
INSERT INTO coffee_variety(uuid,code,name) 
	VALUES ('a66cb420-9ef4-11e7-be27-00ff0b1c94f7','SEMIWAARAB','Semiwashed Arabica');
INSERT INTO coffee_variety(uuid,code,name) 
	VALUES ('a66cb421-9ef4-11e7-be27-00ff0b1c94f7','UNWAARAB','Unwashed Arabica');
INSERT INTO coffee_variety(uuid,code,name) 
	VALUES ('a66cb422-9ef4-11e7-be27-00ff0b1c94f7','WAROB','Washed Robusta');
INSERT INTO coffee_variety(uuid,code,name) 
	VALUES ('a66cb423-9ef4-11e7-be27-00ff0b1c94f7','UNROB','Unwashed Robusta');
INSERT INTO coffee_variety(uuid,code,name) 
	VALUES ('a66efe14-9ef4-11e7-be27-00ff0b1c94f7','ARABROB','Arabica-Robusta blend');

insert into coffee_variety (uuid, version, is_version, parent, code, name) 
  select uuid, version, true, id, code, name from coffee_variety where is_version is false;