--changeset generated_nf:1497621355 
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('dee6b248-ca57-424e-89a7-02752f379e97','WETCH','Wet Cherry');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('c52a4609-3455-41a8-89da-084f4f91ca07','DRYCH','Dry Cherry');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('0957b3a9-eff2-4887-b7d0-3399307ec3f3','WETPA','Wet Parchment');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('fe99bff7-f4b1-4807-ad69-a1fe30bc90ff','DRYPA','Dry Parchment');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('b963dd41-cb8f-4a7b-9c48-d85e4a240fd4','WETUNGRE','Wet Unprocessed Green');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('8c347a2f-f43c-4498-b16c-3777b859f262','DRYUNGRE','Dry Unprocessed Green');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('b578d322-486c-429e-ae44-b3ac4e309353','GREGRA','Green Grade (processed green)');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('a07c710c-0637-4c7b-a181-013f1c204947','WETPOL','Value added - Wet polished');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('ba002319-7212-4016-b244-3de1798010df','VAPOR','Value added - Vaporized');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('db8a6ddd-9d41-403a-8f3e-85ab480e0bf8','DECAF','decaffeinated');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('fa446801-4545-4a27-b323-8d142b1deafb','SOLA','Soluble Coffee - Agglomerated (Spray dried powder)');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('14d53c9e-4c71-4309-b9b9-b574a619e3e2','SOLSPRAD','Soluble Coffee - Spray dried');
INSERT INTO coffee_type (uuid,code,name)
	VALUES ('85142eda-a178-49a3-8235-319057d10fc2','SOLFRED','Soluble Coffee - Freeze dried');

insert into coffee_type (uuid, version, is_version, parent, code, name) 
  select uuid, version, true, id, code, name from coffee_type where is_version is false;