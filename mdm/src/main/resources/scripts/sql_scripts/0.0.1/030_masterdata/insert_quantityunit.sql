--changeset generated_nf:1497621355 
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc660-9ef4-11e7-be27-00ff0b1c94f7', 'KGS', 'kg');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc661-9ef4-11e7-be27-00ff0b1c94f7', 'MTS', 'MT (1000kg)');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc662-9ef4-11e7-be27-00ff0b1c94f7', 'LBS', 'pound (0.453592kg)');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc663-9ef4-11e7-be27-00ff0b1c94f7', '46KB', 'Quintal QQ (46kg)');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc664-9ef4-11e7-be27-00ff0b1c94f7', '50KB', 'Bag (~50kg)');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc665-9ef4-11e7-be27-00ff0b1c94f7', '60KB', 'Bag (60kg)');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc666-9ef4-11e7-be27-00ff0b1c94f7', '69KB', 'Bag (69kg)');
INSERT INTO quantityunit (uuid, code, name)
	VALUES ('a67fc667-9ef4-11e7-be27-00ff0b1c94f7', '70KB', 'Bag (~70kg)');

insert into quantityunit (uuid, version, is_version, parent, code, name) 
  select uuid, version, true, id, code, name from quantityunit where is_version is false;
	