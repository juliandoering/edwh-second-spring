--changeset generated_nf:1497621355 
INSERT INTO samplehandling (uuid, sastype)
VALUES ('a67fc669-9ef4-11e7-be27-00ff0b1c94f7', 'no sample');
INSERT INTO samplehandling (uuid, sastype)
VALUES ('a67fc66a-9ef4-11e7-be27-00ff0b1c94f7', 'SAS');
INSERT INTO samplehandling (uuid, sastype)
VALUES ('a67fc66b-9ef4-11e7-be27-00ff0b1c94f7', 'SAS, repeat');
INSERT INTO samplehandling (uuid, sastype)
VALUES ('a67fc66c-9ef4-11e7-be27-00ff0b1c94f7', 'SAS, no approval no sale');
INSERT INTO samplehandling (uuid, sastype)
VALUES ('a67fc66d-9ef4-11e7-be27-00ff0b1c94f7', 'as per type sample');
INSERT INTO samplehandling (uuid, sastype)
VALUES ('a67fc66e-9ef4-11e7-be27-00ff0b1c94f7', 'as per approved sample');

INSERT INTO samplehandling (uuid, version, is_version, parent, sastype)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    sastype
  FROM samplehandling
  WHERE is_version IS FALSE;