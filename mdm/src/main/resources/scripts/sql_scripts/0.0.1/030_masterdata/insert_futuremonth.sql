--changeset generated_nf:1497621355 
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a6716f31-9ef4-11e7-be27-00ff0b1c94f7', 'F', 'January', '1');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b922-9ef4-11e7-be27-00ff0b1c94f7', 'G', 'February', '2');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b923-9ef4-11e7-be27-00ff0b1c94f7', 'H', 'March', '3');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b924-9ef4-11e7-be27-00ff0b1c94f7', 'J', 'April', '4');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b925-9ef4-11e7-be27-00ff0b1c94f7', 'K', 'May', '5');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b926-9ef4-11e7-be27-00ff0b1c94f7', 'M', 'June', '6');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b927-9ef4-11e7-be27-00ff0b1c94f7', 'N', 'July', '7');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b928-9ef4-11e7-be27-00ff0b1c94f7', 'Q', 'August', '8');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b929-9ef4-11e7-be27-00ff0b1c94f7', 'U', 'September', '9');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b92a-9ef4-11e7-be27-00ff0b1c94f7', 'V', 'October', '10');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b92b-9ef4-11e7-be27-00ff0b1c94f7', 'X', 'November', '11');
INSERT INTO futuremonth (uuid, code, description, int_representation)
VALUES ('a673b92c-9ef4-11e7-be27-00ff0b1c94f7', 'Z', 'December', '12');

INSERT INTO futuremonth (uuid, version, is_version, parent, code, description, int_representation)
  SELECT
    uuid,
    version,
    TRUE,
    id,
    code,
    description,
    int_representation
  FROM futuremonth
  WHERE is_version IS FALSE;
