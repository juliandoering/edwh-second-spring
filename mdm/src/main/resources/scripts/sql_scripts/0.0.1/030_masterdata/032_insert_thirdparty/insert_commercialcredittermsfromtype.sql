--changeset generated_nf:1497858527 
INSERT INTO commercialcredittermsfromtype (uuid, code, typename) VALUES ('a667d1fa-9ef4-11e7-be27-00ff0b1c94f7', 'POD','Presentation of original documents');
INSERT INTO commercialcredittermsfromtype (uuid, code, typename) VALUES ('a667d1fb-9ef4-11e7-be27-00ff0b1c94f7', 'POSCD','Presentation of scanned copy of documents');
INSERT INTO commercialcredittermsfromtype (uuid, code, typename) VALUES ('a667d1fc-9ef4-11e7-be27-00ff0b1c94f7', 'RGS','Receipt of Goods or Service');

insert into commercialcredittermsfromtype (uuid, version, is_version, parent, code, typename) 
  select uuid, version, true, id, code, typename from commercialcredittermsfromtype where is_version is false;