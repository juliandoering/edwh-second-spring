--changeset generated_nf:1497621355 
INSERT INTO collateraltype (uuid, code, name)
	VALUES ('a66efe16-9ef4-11e7-be27-00ff0b1c94f7', 'FIDUCIARY', 'Fiduciary');
INSERT INTO collateraltype (uuid, code, name)
	VALUES ('a66efe17-9ef4-11e7-be27-00ff0b1c94f7', 'PLEDGE', 'Pledge');
INSERT INTO collateraltype (uuid, code, name)
	VALUES ('a66efe18-9ef4-11e7-be27-00ff0b1c94f7', 'MORTGAGE', 'Mortgage');
INSERT INTO collateraltype (uuid, code, name)
	VALUES ('a66efe19-9ef4-11e7-be27-00ff0b1c94f7', '3PG', '3rd Party Guarantee');
INSERT INTO collateraltype (uuid, code, name)
	VALUES ('a66efe1a-9ef4-11e7-be27-00ff0b1c94f7', 'BANK', 'Bank Guarantee');

insert into collateraltype (uuid, version, is_version, parent, code, name) 
  select uuid, version, true, id, code, name from collateraltype where is_version is false;
  