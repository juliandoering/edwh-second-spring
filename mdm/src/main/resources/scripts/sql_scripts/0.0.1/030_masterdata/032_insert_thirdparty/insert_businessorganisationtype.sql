--changeset generated_nf:1497858527 
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66316f7-9ef4-11e7-be27-00ff0b1c94f7', 'SP','Sole Proprietorship','Sole Proprietorship');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66316f8-9ef4-11e7-be27-00ff0b1c94f7', 'LP','Limited partnership', 'Partnership');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66316f9-9ef4-11e7-be27-00ff0b1c94f7', 'PS','Unlimited/General partnership', 'Partnership');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66316fa-9ef4-11e7-be27-00ff0b1c94f7', 'PRIVLLC','Private Limited Liability Company', 'Corporation');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66316fb-9ef4-11e7-be27-00ff0b1c94f7', 'PUBLLC','Public Limited Liability Company', 'Corporation');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66316fc-9ef4-11e7-be27-00ff0b1c94f7', 'SOC','State-owned Company', 'Corporation');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66587fd-9ef4-11e7-be27-00ff0b1c94f7', 'FA','Farmer Association', 'Farmer Association');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66587fe-9ef4-11e7-be27-00ff0b1c94f7', 'COOP','Cooperative', 'Corporation');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a66587ff-9ef4-11e7-be27-00ff0b1c94f7', 'PRIVF','Private Foundation', 'Other');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a6658800-9ef4-11e7-be27-00ff0b1c94f7', 'PUBF','Public Foundation', 'Other');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a6658801-9ef4-11e7-be27-00ff0b1c94f7', 'NPA','Non-profit Association', 'Other');
INSERT INTO businessorganisationtype (uuid, code, businesstype, category) VALUES ('a6658802-9ef4-11e7-be27-00ff0b1c94f7', 'TRUST','Trust', 'Other');

insert into businessorganisationtype (uuid, version, is_version, parent, code, businesstype, category) 
  select uuid, version, true, id, code, businesstype, category from businessorganisationtype where is_version is false;