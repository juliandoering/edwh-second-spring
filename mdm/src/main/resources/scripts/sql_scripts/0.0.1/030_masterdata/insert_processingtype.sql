INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae54a-9ef4-11e7-be27-00ff0b1c94f7','PRE-CLEANING','Pre-Cleaning');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae54b-9ef4-11e7-be27-00ff0b1c94f7','CLEANING','Cleaning');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae54c-9ef4-11e7-be27-00ff0b1c94f7','DE-STONING','De-Stoning');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae54d-9ef4-11e7-be27-00ff0b1c94f7','HULLING','Hulling');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae54e-9ef4-11e7-be27-00ff0b1c94f7','POLISHING','Polishing');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae54f-9ef4-11e7-be27-00ff0b1c94f7','GRADING','Grading');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae550-9ef4-11e7-be27-00ff0b1c94f7','DENSITY-SEPARATION','Density separation');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae551-9ef4-11e7-be27-00ff0b1c94f7','COLOR-SORTING','Color Sorting');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae552-9ef4-11e7-be27-00ff0b1c94f7','WET-POLISHING','Wet Polishing');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae553-9ef4-11e7-be27-00ff0b1c94f7','BLENDING','Blending');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67ae554-9ef4-11e7-be27-00ff0b1c94f7','DECAFEINATION','Decafeination');
INSERT INTO processingtype (uuid,code,name)
	VALUES ('a67d5555-9ef4-11e7-be27-00ff0b1c94f7','VAPORIZATION','Vaporization');

insert into processingtype (version, is_version, parent, uuid, code, name) 
  select version, true, id, uuid, code, name from processingtype where is_version is false;
