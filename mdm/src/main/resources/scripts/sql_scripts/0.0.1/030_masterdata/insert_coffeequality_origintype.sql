--changeset generated_nf:1497621355 
/* Coffee origin types that may be used to validate amount of origins in coffee quality entity - be advised to check before changing values  */
INSERT INTO coffeequality_origintype(uuid, code, name, description ) 
	VALUES ('a66a4312-9ef4-11e7-be27-00ff0b1c94f7','SO','Single Origin', 'Used for coffee quality validation - check before changing');

INSERT INTO coffeequality_origintype(uuid, code, name , description ) 
	VALUES ('a66a4313-9ef4-11e7-be27-00ff0b1c94f7','BMO','Basket/Multiple Origin', 'Used for coffee quality validation - check before changing');

INSERT INTO coffeequality_origintype(uuid, code, name , description ) 
	VALUES ('a66a4314-9ef4-11e7-be27-00ff0b1c94f7','BL','Blend', 'Used for coffee quality validation - check before changing');

INSERT INTO coffeequality_origintype(uuid, code, name , description ) 
	VALUES ('a66a4315-9ef4-11e7-be27-00ff0b1c94f7','U','Unspecified', 'Used for coffee quality validation - check before changing');

insert into coffeequality_origintype (uuid, version, is_version, parent, code, name, description) 
  select uuid, version, true, id, code, name, description from coffeequality_origintype where is_version is false;