--changeset generated_nf:1497858527 
/* Code in BusinessThirdPartyRepository relies on relies on values in this table
 be cautious with changes! */

INSERT INTO businessthirdparty_individual_relation (uuid, shortcode,relation) 
VALUES ('a6658805-9ef4-11e7-be27-00ff0b1c94f7', 'ubo','Ultimate Beneficiary Owner');

INSERT INTO businessthirdparty_individual_relation (uuid, shortcode,relation) 
VALUES ('a6658806-9ef4-11e7-be27-00ff0b1c94f7', 'owner','Owner');

-- management/representative not for sole proprietorship

INSERT INTO businessthirdparty_individual_relation (uuid, shortcode,relation) 
VALUES ('a6658807-9ef4-11e7-be27-00ff0b1c94f7', 'representative','Management/ Legal Representative');

INSERT INTO businessthirdparty_individual_relation (uuid, shortcode,relation) 
VALUES ('a667d1f8-9ef4-11e7-be27-00ff0b1c94f7', 'contact','Contact Person');
