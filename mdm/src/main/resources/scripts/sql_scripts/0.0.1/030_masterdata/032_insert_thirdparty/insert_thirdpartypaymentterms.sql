--changeset generated_nf:1497858527 
INSERT INTO thirdpartypaymentterms (uuid, code, paymentterms_type)
	VALUES ('a667d207-9ef4-11e7-be27-00ff0b1c94f7', 'PP', 'Prepayment');

INSERT INTO thirdpartypaymentterms (uuid, code, paymentterms_type)
	VALUES ('a667d208-9ef4-11e7-be27-00ff0b1c94f7', 'CADBD', 'Cash Against presentation of documents before delivery');

INSERT INTO thirdpartypaymentterms (uuid, code, paymentterms_type)
	VALUES ('a66a4309-9ef4-11e7-be27-00ff0b1c94f7', 'CAD', 'Cash Against presentation of documents');

INSERT INTO thirdpartypaymentterms (uuid, code, paymentterms_type)
	VALUES ('a66a430a-9ef4-11e7-be27-00ff0b1c94f7', 'CC', 'Commercial credit');

INSERT INTO thirdpartypaymentterms (uuid, code, paymentterms_type)
	VALUES ('a66a430b-9ef4-11e7-be27-00ff0b1c94f7', 'OSD', 'On specific Date');

insert into thirdpartypaymentterms (uuid, version, is_version, parent, code, paymentterms_type) 
  select uuid, version, true, id, code, paymentterms_type from thirdpartypaymentterms where is_version is false;