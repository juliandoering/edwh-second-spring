INSERT INTO holidays (holiday, description, country)
VALUES ('2017-01-02', 'New Year''s Day substitute', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-04-14', 'Good Friday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-04-17', 'Easter Monday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-05-01', 'May Day', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-05-29', 'Spring Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-08-17', 'Summer Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-12-25', 'Christmas Day', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-12-26', 'Boxing Day', 'GB');

INSERT INTO holidays (holiday, description, country)
VALUES ('2018-01-01', 'New Year''s Day', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-03-30', 'Good Friday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-04-02', 'Easter Monday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-05-07', 'Early May Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-05-28', 'Spring Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-08-27', 'Summer Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-12-25', 'Christmas Day', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-12-26', 'Boxing Day', 'GB');

INSERT INTO holidays (holiday, description, country)
VALUES ('2019-01-01', 'New Year''s Day', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-04-19', 'Good Friday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-04-22', 'Easter Monday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-05-06', 'Early May Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-05-27', 'Spring Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-08-26', 'Summer Bank Holiday', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-12-25', 'Christmas Day', 'GB');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-12-26', 'Boxing Day', 'GB');

INSERT INTO holidays (holiday, description, country)
VALUES ('2017-01-02', 'New Year''s Day substitute', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-01-16', 'Martin Luther King, Jr. Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-02-20', 'Washington''s Birthday', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-04-14', 'Good Friday', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-05-29', 'Memorial Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-07-04', 'Independence Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-09-04', 'Labor Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-11-23', 'Thanksgiving Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2017-12-25', 'Christmas', 'US');

INSERT INTO holidays (holiday, description, country)
VALUES ('2018-01-01', 'New Year''s Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-01-15', 'Martin Luther King, Jr. Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-02-19', 'Washington''s Birthday', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-03-30', 'Good Friday', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-05-28', 'Memorial Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-07-04', 'Independence Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-09-03', 'Labor Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-11-23', 'Thanksgiving Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2018-12-25', 'Christmas', 'US');

INSERT INTO holidays (holiday, description, country)
VALUES ('2019-01-01', 'New Year''s Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-01-21', 'Martin Luther King, Jr. Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-02-18', 'Washington''s Birthday', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-04-19', 'Good Friday', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-05-27', 'Memorial Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-07-04', 'Independence Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-09-02', 'Labor Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-11-28', 'Thanksgiving Day', 'US');
INSERT INTO holidays (holiday, description, country)
VALUES ('2019-12-25', 'Christmas', 'US');