--changeset generated_nf:1497621355 
/* Master Data Certificates */

/* 
No certificate means the below
INSERT INTO certificate (uuid,code,name)
	VALUES ('a67d5557-9ef4-11e7-be27-00ff0b1c94f7','NONE','Non-certified / Regular,'');
*/

INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d5558-9ef4-11e7-be27-00ff0b1c94f7','4C','4C');
INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d5559-9ef4-11e7-be27-00ff0b1c94f7','UTZ','UTZ');
INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d555a-9ef4-11e7-be27-00ff0b1c94f7','RA','Rainforest Alliance');
INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d555b-9ef4-11e7-be27-00ff0b1c94f7','FAIR','Fairtrade');
INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d555c-9ef4-11e7-be27-00ff0b1c94f7','ORG','Organic');
INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d555d-9ef4-11e7-be27-00ff0b1c94f7','SBUX','Starbucks C.A.F.E. Practices');
INSERT INTO productcertificationprogram (uuid,code,name)
	VALUES ('a67d555e-9ef4-11e7-be27-00ff0b1c94f7','NES3A','Nespresso AAA');

insert into productcertificationprogram (uuid, version, is_version, parent, code, name) 
  select uuid, version, true, id, code, name from productcertificationprogram where is_version is false;