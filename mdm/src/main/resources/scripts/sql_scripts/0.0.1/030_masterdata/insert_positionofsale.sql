--changeset generated_nf:1497621355 
INSERT INTO positionofsale (uuid, code, description)
	VALUES ('a6789b4f-9ef4-11e7-be27-00ff0b1c94f7', 'Ship', 'Ship');
INSERT INTO positionofsale (uuid, code, description)
	VALUES ('a6789b50-9ef4-11e7-be27-00ff0b1c94f7', 'Delivery', 'Delivery');
INSERT INTO positionofsale (uuid, code, description)
	VALUES ('a6789b51-9ef4-11e7-be27-00ff0b1c94f7', 'Arrival', 'Arrival');
INSERT INTO positionofsale (uuid, code, description)
	VALUES ('a6789b52-9ef4-11e7-be27-00ff0b1c94f7', 'Spot', 'Spot');
INSERT INTO positionofsale (uuid, code, description)
	VALUES ('a67ae543-9ef4-11e7-be27-00ff0b1c94f7', 'Afloat', 'Afloat');

insert into positionofsale (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from positionofsale where is_version is false;