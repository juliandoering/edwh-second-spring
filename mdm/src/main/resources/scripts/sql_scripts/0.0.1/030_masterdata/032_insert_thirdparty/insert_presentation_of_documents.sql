--changeset generated_nf:1497858527 
INSERT INTO presentation_of_documents (uuid, code, typename) VALUES ('a667d204-9ef4-11e7-be27-00ff0b1c94f7', 'BANK','Via Bank');
INSERT INTO presentation_of_documents (uuid, code, typename) VALUES ('a667d205-9ef4-11e7-be27-00ff0b1c94f7', 'TRUST','Direct in Trust');

insert into presentation_of_documents (uuid, version, is_version, parent, code, typename) 
  select uuid, version, true, id, code, typename from presentation_of_documents where is_version is false;