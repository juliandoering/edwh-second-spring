--changeset generated_nf:1497621355 
INSERT INTO buyersellercode (uuid, code, description)
    VALUES ('a66a430d-9ef4-11e7-be27-00ff0b1c94f7', 'S', 'Seller');
INSERT INTO buyersellercode (uuid, code, description)
    VALUES ('a66a430e-9ef4-11e7-be27-00ff0b1c94f7', 'B', 'Buyer');
INSERT INTO buyersellercode (uuid, code, description)
    VALUES ('a66a430f-9ef4-11e7-be27-00ff0b1c94f7', 'A', 'Agent');
INSERT INTO buyersellercode (uuid, code, description)
    VALUES ('a66a4310-9ef4-11e7-be27-00ff0b1c94f7', 'SH', 'Shipper');

insert into buyersellercode (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from buyersellercode where is_version is false;