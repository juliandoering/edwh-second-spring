--changeset generated_nf:1497621355 
-- Foreign key constraints ordered almost alphabetically by table name 

-- Additional Information
ALTER TABLE additionalinformation ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Amendment
ALTER TABLE amendment ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Bank account
ALTER TABLE bankaccount ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;
ALTER TABLE bankaccount ADD CONSTRAINT fk_postaladdress FOREIGN KEY (postaladdress_id) REFERENCES postaladdress;


-- business third party
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_businessorganisationtype FOREIGN KEY (businessorganisationtype_id) REFERENCES businessorganisationtype;
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_legaladdress FOREIGN KEY (legaladdress_id) REFERENCES postaladdress;
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_postaladdress FOREIGN KEY (postaladdress_id) REFERENCES postaladdress;
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_businessfunction FOREIGN KEY (businessfunction_id) REFERENCES businessfunction;
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_incoterm FOREIGN KEY (incoterm_id) REFERENCES incoterm;
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_presentation_of_documents FOREIGN KEY (presentation_of_documents_id) REFERENCES presentation_of_documents;
ALTER TABLE businessthirdparty ADD CONSTRAINT fk_thirdpartypaymentterms FOREIGN KEY (thirdpartypaymentterms_id) REFERENCES thirdpartypaymentterms;

-- Business third party commercial limit
ALTER TABLE businessthirdparty_commerciallimit ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;
ALTER TABLE businessthirdparty_commerciallimit ADD CONSTRAINT fk_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES groupcompany;

-- Clause
ALTER TABLE clause ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Compliance rating
ALTER TABLE compliancerating ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;


-- Contract
ALTER TABLE contract ADD CONSTRAINT fk_buyer FOREIGN KEY (buyer_id) REFERENCES groupcompany;
ALTER TABLE contract ADD CONSTRAINT fk_seller FOREIGN KEY (seller_id) REFERENCES groupcompany;
ALTER TABLE contract ADD CONSTRAINT fk_agent FOREIGN KEY (agent_id) REFERENCES groupcompany;
ALTER TABLE contract ADD CONSTRAINT fk_shipper FOREIGN KEY (shipper_id) REFERENCES groupcompany;
ALTER TABLE contract ADD CONSTRAINT fk_buyer_btp FOREIGN KEY (buyer_third_party_id) REFERENCES businessthirdparty;
ALTER TABLE contract ADD CONSTRAINT fk_seller_btp FOREIGN KEY (seller_third_party_id) REFERENCES businessthirdparty;
ALTER TABLE contract ADD CONSTRAINT fk_agent_btp FOREIGN KEY (agent_third_party_id) REFERENCES businessthirdparty;
ALTER TABLE contract ADD CONSTRAINT fk_shipper_btp FOREIGN KEY (shipper_third_party_id) REFERENCES businessthirdparty;
ALTER TABLE contract ADD CONSTRAINT fk_incoterm FOREIGN KEY (incoterm_id) REFERENCES incoterm;
ALTER TABLE contract ADD CONSTRAINT fk_instr_info_buyersellercode FOREIGN KEY (instr_info_buyersellercode_id) REFERENCES buyersellercode;
ALTER TABLE contract ADD CONSTRAINT fk_fixationoption FOREIGN KEY (fixationoption_id) REFERENCES fixationoption;
ALTER TABLE contract ADD CONSTRAINT fk_positionofsale FOREIGN KEY (positionofsale_id) REFERENCES positionofsale;
ALTER TABLE contract ADD CONSTRAINT fk_instr_info_location FOREIGN KEY (instr_info_location_id) REFERENCES location;
ALTER TABLE contract ADD CONSTRAINT fk_samplehandling FOREIGN KEY (samplehandling_id) REFERENCES samplehandling;
ALTER TABLE contract ADD CONSTRAINT fk_quantityunit FOREIGN KEY (quantityunit_id) REFERENCES quantityunit;
ALTER TABLE contract ADD CONSTRAINT fk_currency FOREIGN KEY (currency_id) REFERENCES currency;
ALTER TABLE contract ADD CONSTRAINT fk_price_quantityunit FOREIGN KEY (price_quantityunit_id) REFERENCES quantityunit;
ALTER TABLE contract ADD CONSTRAINT fk_packing FOREIGN KEY (packing_id) REFERENCES packing;
ALTER TABLE contract ADD CONSTRAINT fk_coffeequality FOREIGN KEY (coffeequality_id) REFERENCES coffeequality;
ALTER TABLE contract ADD CONSTRAINT fk_futuremarket FOREIGN KEY (futuremarket_id) REFERENCES futuremarket;
ALTER TABLE contract ADD CONSTRAINT fk_futuremonth FOREIGN KEY (futuremonth_id) REFERENCES futuremonth;
ALTER TABLE contract ADD CONSTRAINT fk_coffeew_buyersellercode FOREIGN KEY (coffeew_buyersellercode_id) REFERENCES buyersellercode;
ALTER TABLE contract ADD CONSTRAINT fk_coffeeweightscode FOREIGN KEY (coffeeweightscode_id) REFERENCES coffeeweightscode;
ALTER TABLE contract ADD CONSTRAINT fk_paymentterm FOREIGN KEY (paymentterm_id) REFERENCES paymentterm;
ALTER TABLE contract ADD CONSTRAINT fk_countryqualifier FOREIGN KEY (countryqualifier_id) REFERENCES countryqualifier;
ALTER TABLE contract ADD CONSTRAINT fk_countrylocation FOREIGN KEY (countrylocation_id) REFERENCES location;


-- Coffee Quality
ALTER TABLE coffeequality ADD CONSTRAINT fk_coffeequality_origintype FOREIGN KEY (coffeequality_origintype_id) REFERENCES coffeequality_origintype;
ALTER TABLE coffeequality ADD CONSTRAINT fk_coffeequality_coffeetype FOREIGN KEY (coffee_type_id) REFERENCES coffee_type;
ALTER TABLE coffeequality ADD CONSTRAINT fk_coffeequality_variety FOREIGN KEY (coffee_variety_id) REFERENCES coffee_variety;


-- Coffee Quality Commercialname 
ALTER TABLE coffeequality_commercialname ADD CONSTRAINT fk_coffeequality FOREIGN KEY (coffeequality_id) REFERENCES coffeequality;


-- Coffee Quality certificate
ALTER TABLE coffeequality_certificate ADD CONSTRAINT fk_productcertificationprogram FOREIGN KEY (productcertificationprogram_id) REFERENCES productcertificationprogram;
ALTER TABLE coffeequality_certificate ADD CONSTRAINT fk_coffeequality FOREIGN KEY (coffeequality_id) REFERENCES coffeequality;


-- Coffee Quality Origin
ALTER TABLE coffeequality_origin ADD CONSTRAINT fk_coffeequality FOREIGN KEY (coffeequality_id) REFERENCES coffeequality;
ALTER TABLE coffeequality_origin ADD CONSTRAINT fk_origin FOREIGN KEY (location_id) REFERENCES location;


-- Collateral
ALTER TABLE collateral ADD CONSTRAINT fk_businessthirdparty_commerciallimit FOREIGN KEY (businessthirdparty_commerciallimit_id) REFERENCES businessthirdparty_commerciallimit;
ALTER TABLE collateral ADD CONSTRAINT fk_collateraltype FOREIGN KEY (collateraltype_id) REFERENCES collateraltype;
ALTER TABLE collateral ADD CONSTRAINT fk_currency FOREIGN KEY (currency_id) REFERENCES currency;


-- Commercial credit terms
ALTER TABLE commercialcreditterms ADD CONSTRAINT fk_businessthirdparty_commerciallimit FOREIGN KEY (businessthirdparty_commerciallimit_id) REFERENCES businessthirdparty_commerciallimit;
ALTER TABLE commercialcreditterms ADD CONSTRAINT fk_commercialcredittermsfromtype FOREIGN KEY (commercialcredittermsfromtype_id) REFERENCES commercialcredittermsfromtype;


-- Commercial ID
ALTER TABLE commercialid ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;


-- Contrainer
ALTER TABLE container ADD CONSTRAINT fk_shipment_advice FOREIGN KEY (shipment_advice_id) REFERENCES shipment_advice;


-- Contact
ALTER TABLE contact ADD CONSTRAINT fk_warehouse FOREIGN KEY (warehouse_id) REFERENCES warehouse;


-- Employe
ALTER TABLE individual ADD CONSTRAINT fk_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES groupcompany;


-- Exceptions and Other Conditions
ALTER TABLE exceptionsandotherconditions ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- External credit limit
ALTER TABLE externalcreditlimit ADD CONSTRAINT fk_businessthirdparty_commerciallimit FOREIGN KEY (businessthirdparty_commerciallimit_id) REFERENCES businessthirdparty_commerciallimit;
ALTER TABLE externalcreditlimit ADD CONSTRAINT fk_currency FOREIGN KEY (currency_id) REFERENCES currency;


-- GroupCompany
ALTER TABLE groupcompany ADD CONSTRAINT fk_postaladdress FOREIGN KEY (postal_address_id) REFERENCES postaladdress;


-- Internal Credit Limit
ALTER TABLE internalcreditlimit ADD CONSTRAINT fk_businessthirdparty_commerciallimit FOREIGN KEY (businessthirdparty_commerciallimit_id) REFERENCES businessthirdparty_commerciallimit;
ALTER TABLE internalcreditlimit ADD CONSTRAINT fk_currency FOREIGN KEY (currency_id) REFERENCES currency;


-- International Standard Certification
ALTER TABLE interstandardcert ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;
ALTER TABLE interstandardcert ADD CONSTRAINT fk_interstandardcertprogram FOREIGN KEY (interstandardcertprogram_id) REFERENCES interstandardcertprogram;


-- LocationFunction
ALTER TABLE locationfunction ADD CONSTRAINT fk_location FOREIGN KEY (location_id) REFERENCES location;


-- Mark or Number
ALTER TABLE markornumber ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Maximum Open Position
ALTER TABLE maximumopenposition ADD CONSTRAINT fk_businessthirdparty_commerciallimit FOREIGN KEY (businessthirdparty_commerciallimit_id) REFERENCES businessthirdparty_commerciallimit;
ALTER TABLE maximumopenposition ADD CONSTRAINT fk_quantityunit FOREIGN KEY (quantityunit_id) REFERENCES quantityunit;


-- Maximum Open Priced Position
ALTER TABLE maximumopenpricedposition ADD CONSTRAINT fk_businessthirdparty_commerciallimit FOREIGN KEY (businessthirdparty_commerciallimit_id) REFERENCES businessthirdparty_commerciallimit;
ALTER TABLE maximumopenpricedposition ADD CONSTRAINT fk_quantityunit FOREIGN KEY (quantityunit_id) REFERENCES quantityunit;


-- Other Approval Terms
ALTER TABLE otherapprovalterms ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;

-- Price Fixation
ALTER TABLE pricefixation ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Price Fixation Term
ALTER TABLE pricefixationterm ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;
ALTER TABLE pricefixationterm ADD CONSTRAINT fk_pricefixationtype FOREIGN KEY (pricefixationtype_id) REFERENCES pricefixationtype;
ALTER TABLE pricefixationterm ADD CONSTRAINT fk_fixationoption FOREIGN KEY (fixationoption_id) REFERENCES fixationoption;


-- Product Certification
ALTER TABLE productcertification ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;
ALTER TABLE productcertification ADD CONSTRAINT fk_productcertificationprogram FOREIGN KEY (productcertificationprogram_id) REFERENCES productcertificationprogram;


-- Product Description
ALTER TABLE productdescription ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;

-- Shipment Advice
ALTER TABLE shipment_advice ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Tax ID
ALTER TABLE taxid ADD CONSTRAINT fk_businessthirdparty FOREIGN KEY (businessthirdparty_id) REFERENCES businessthirdparty;


-- User Defined Payment Terms
ALTER TABLE userdefinedpaymentterms ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;


-- Userprofile groupcompany
ALTER TABLE userprofile_groupcompany ADD CONSTRAINT fk_userprofile FOREIGN KEY (userprofile_id) REFERENCES userprofile;
ALTER TABLE userprofile_groupcompany ADD CONSTRAINT fk_groupcompany FOREIGN KEY (groupcompany_id) REFERENCES groupcompany;


-- Userprofile 
ALTER TABLE userprofile ADD CONSTRAINT fk_userrole FOREIGN KEY (userrole_id) REFERENCES userrole;


-- Warehouse
ALTER TABLE warehouse ADD CONSTRAINT fk_businesspartner FOREIGN KEY (businesspartner_id) REFERENCES groupcompany;


-- Warehouse Individual
ALTER TABLE warehouse_individual ADD CONSTRAINT fk_warehouse FOREIGN KEY (warehouse_id) REFERENCES warehouse;
ALTER TABLE warehouse_individual ADD CONSTRAINT fk_individual FOREIGN KEY (individual_id) REFERENCES individual;

ALTER TABLE contractrole_businessfunction ADD CONSTRAINT fk_businessfunction FOREIGN KEY (businessfunction_id) REFERENCES businessfunction;
ALTER TABLE contractrole_businessfunction ADD CONSTRAINT fk_buyersellercode FOREIGN KEY (buyersellercode_id) REFERENCES buyersellercode;
