DROP TABLE IF EXISTS groupcompany_bookingconfirmationmails
    , bookingconfirmationparty
    , bookingconfirmationequipment
    , bookingconfirmation_transportationlocation
    , bookingconfirmation_transportationdetails
    , bookingconfirmation_goodsdetail
    , bookingconfirmation
CASCADE;
