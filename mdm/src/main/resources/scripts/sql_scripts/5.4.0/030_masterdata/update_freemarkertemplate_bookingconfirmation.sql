UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear ${entity1.contactPersonName},<br>
		a new Booking confirmation for your contract <@mark mark=changes.purchaseOrderNumber>"${entity0.purchaseOrderNumber!"No Data"}"</@mark> has been uploaded to INTTRA. The INTTRA reference number is <@mark mark=changes.inttraRefNumber>${entity0.inttraRefNumber!"No Data"}</@mark>.

		<p>
		Carrier Response Type: <b>${entity0.transactionType!"No Data"}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity0.booker! partychanges=changes.involvedParties.booker></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity0.shipper! partychanges=changes.involvedParties.shipper></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity0.carrier! partychanges=changes.involvedParties.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span><@mark mark=changes.inttraRefNumber>${(entity0.inttraRefNumber)!"No Data"}</@mark></span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Contract Number</b></span>
					<br>
					<span>No Data</span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span><@mark mark=changes.blNumber>${entity0.blNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span><@mark mark=changes.purchaseOrderNumber>${entity0.purchaseOrderNumber!"No Data"}</@mark></span>
				</td>
				<td>
					<@party name="Booking Office" party=entity0.bookingOffice! partychanges=changes.involvedParties.bookingOffice></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity0.forwarder! partychanges=changes.involvedParties.forwarder></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity0.consignee! partychanges=changes.involvedParties.consignee></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity0.contractParty! partychanges=changes.involvedParties.contractParty></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity0.mainNotifyParty! partychanges=changes.involvedParties.mainNotifyParty></@party>
					<#if entity0.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity0.firstAdditionalNotifyParty! partychanges=changes.involvedParties.firstAdditionalNotifyParty></@party>
					</#if>
					<#if entity0.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity0.secondAdditionalNotifyParty! partychanges=changes.involvedParties.secondAdditionalNotifyParty></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity0.freightPayerParty! partychanges=changes.involvedParties.freightPayerParty></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s Code, Voyage</b></span>
					<br>
					<span><@mark mark=changes.involvedParties.carrier.name>${(entity0.carrier.name)!"No data"}</@mark>, <@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.vesselName)!"No data"}, ${(entity0.mainTransportDetails.lloydsCode)!"No data"}, ${(entity0.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity0.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity0.mainTransportDetails changed=changes.transportationDetails.main></@plandetails>
					</td>
				</#if>
				<#if entity0.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity0.onCarriageTransportDetails changed=changes.transportationDetails.onCarriage></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list entity0.goodsDetails as cargo>
					<@tr mark=changes.goodsDetails>
						<td>
							(${cargo.lineNumber!"No Data"}) "Unknown Packing"
						</td>
						<td>
							${cargo.numberOfPackages!"No Data"} ${cargo.packageTypeDescription!"Unknown Type"}
						</td>
						<td>
							${cargo.goodsDescription!"No Data"}
						</td>
						<td>
							${cargo.grossWeight!"No Data"} ${cargo.grossWeightType!"Unknown Type"}
						</td>
						<td>
							${cargo.grossVolume!"No Data"} ${cargo.grossVolumeType!"Unknown Type"}
						</td>
					</@tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list entity0.equipments as equipment>
					<@tr mark=changes.equipments>
						<td>
							Container Number: No Data
							<br>
							<#if equipment.typeCode??>
								Type: ${equipment.typeCode!"No Data"}
								<br>
							</#if>
							<#if equipment.cargoMovementType??>
								Service Arrangement: ${equipment.cargoMovementType}
								<br>
							</#if>
							<#if equipment.haulageArrangements??>
								Haulage Arrangements: ${equipment.haulageArrangements}
								<br>
							</#if>
						</td>
						<td>-</td>
						<td>-</td>
					</@tr>
				</#list>
			</table>
		</p>

		<#macro party name party partychanges>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><@mark mark=partychanges.identifierType><span>${party.identifierType}: ${party.identifier}</span></@mark><br></#if>
				<@mark mark=partychanges.name><span>${party.name!""}<br></span></@mark>
				<@mark mark=partychanges.address><span>${party.address!"N/A"}<br></span></@mark>
			</#if>
		</#macro>

		<#macro plandetails name details changed>
			<@mark mark=changed>
			<b>${name}</b>
				<br>
				<#if entity0.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode}</span>
				</#if>
				<#if entity0.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier}</span>
				</#if>
				<br>
				<#if entity0.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName}
				</#if>
				<#-- lloyd''s code + voyage number -->
				<br>
				<#if entity0.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity0.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
			</@mark>
		</#macro>

		<#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
		<#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

	</body>
</html>'
WHERE template_key = 'bookingconfirmation.created.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
        Dear ${entity1.contactPersonName},<br>
        a new version of a Booking confirmation for your contract <@mark mark=changes.purchaseOrderNumber>"${entity0.purchaseOrderNumber!"No Data"}"</@mark> has been uploaded to INTTRA. The INTTRA reference number is <@mark mark=changes.inttraRefNumber>${entity0.inttraRefNumber!"No Data"}</@mark>.

		<p>
		Carrier Response Type: <b>${entity0.transactionType!"No Data"}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity0.booker! partychanges=changes.involvedParties.booker></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity0.shipper! partychanges=changes.involvedParties.shipper></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity0.carrier! partychanges=changes.involvedParties.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span><@mark mark=changes.inttraRefNumber>${(entity0.inttraRefNumber)!"No Data"}</@mark></span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Contract Number</b></span>
					<br>
					<span>No Data</span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span><@mark mark=changes.blNumber>${entity0.blNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span><@mark mark=changes.purchaseOrderNumber>${entity0.purchaseOrderNumber!"No Data"}</@mark></span>
				</td>
				<td>
					<@party name="Booking Office" party=entity0.bookingOffice! partychanges=changes.involvedParties.bookingOffice></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity0.forwarder! partychanges=changes.involvedParties.forwarder></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity0.consignee! partychanges=changes.involvedParties.consignee></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity0.contractParty! partychanges=changes.involvedParties.contractParty></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity0.mainNotifyParty! partychanges=changes.involvedParties.mainNotifyParty></@party>
					<#if entity0.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity0.firstAdditionalNotifyParty! partychanges=changes.involvedParties.firstAdditionalNotifyParty></@party>
					</#if>
					<#if entity0.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity0.secondAdditionalNotifyParty! partychanges=changes.involvedParties.secondAdditionalNotifyParty></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity0.freightPayerParty! partychanges=changes.involvedParties.freightPayerParty></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s Code, Voyage</b></span>
					<br>
					<span><@mark mark=changes.involvedParties.carrier.name>${(entity0.carrier.name)!"No data"}</@mark>, <@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.vesselName)!"No data"}, ${(entity0.mainTransportDetails.lloydsCode)!"No data"}, ${(entity0.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity0.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity0.mainTransportDetails changed=changes.transportationDetails.main></@plandetails>
					</td>
				</#if>
				<#if entity0.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity0.onCarriageTransportDetails changed=changes.transportationDetails.onCarriage></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list entity0.goodsDetails as cargo>
					<@tr mark=changes.goodsDetails>
						<td>
							(${cargo.lineNumber!"No Data"}) "Unknown Packing"
						</td>
						<td>
							${cargo.numberOfPackages!"No Data"} ${cargo.packageTypeDescription!"Unknown Type"}
						</td>
						<td>
							${cargo.goodsDescription!"No Data"}
						</td>
						<td>
							${cargo.grossWeight!"No Data"} ${cargo.grossWeightType!"Unknown Type"}
						</td>
						<td>
							${cargo.grossVolume!"No Data"} ${cargo.grossVolumeType!"Unknown Type"}
						</td>
					</@tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list entity0.equipments as equipment>
					<@tr mark=changes.equipments>
						<td>
							Container Number: No Data
							<br>
							<#if equipment.typeCode??>
								Type: ${equipment.typeCode!"No Data"}
								<br>
							</#if>
							<#if equipment.cargoMovementType??>
								Service Arrangement: ${equipment.cargoMovementType}
								<br>
							</#if>
							<#if equipment.haulageArrangements??>
								Haulage Arrangements: ${equipment.haulageArrangements}
								<br>
							</#if>
						</td>
						<td>-</td>
						<td>-</td>
					</@tr>
				</#list>
			</table>
		</p>

		<#macro party name party partychanges>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><@mark mark=partychanges.identifierType><span>${party.identifierType}: ${party.identifier}</span></@mark><br></#if>
				<@mark mark=partychanges.name><span>${party.name!""}<br></span></@mark>
				<@mark mark=partychanges.address><span>${party.address!"N/A"}<br></span></@mark>
			</#if>
		</#macro>

		<#macro plandetails name details changed>
			<@mark mark=changed>
			<b>${name}</b>
				<br>
				<#if entity0.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode}</span>
				</#if>
				<#if entity0.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier}</span>
				</#if>
				<br>
				<#if entity0.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName}
				</#if>
				<#-- lloyd''s code + voyage number -->
				<br>
				<#if entity0.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity0.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
			</@mark>
		</#macro>

		<#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
		<#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

	</body>
</html>'
WHERE template_key = 'bookingconfirmation.updated.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
        Dear ${entity1.contactPersonName},<br>
        a new version of an existing Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. This Booking Confirmation is missing a Purchase Order Number and can therefore not be automatically assigned to a Group Company. The INTTRA reference number is <@mark mark=changes.inttraRefNumber>${entity0.inttraRefNumber!"No Data"}</@mark>.

		<p>
		Carrier Response Type: <b>${entity0.transactionType!"No Data"}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity0.booker! partychanges=changes.involvedParties.booker></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity0.shipper! partychanges=changes.involvedParties.shipper></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity0.carrier! partychanges=changes.involvedParties.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span><@mark mark=changes.inttraRefNumber>${(entity0.inttraRefNumber)!"No Data"}</@mark></span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Contract Number</b></span>
					<br>
					<span>No Data</span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span><@mark mark=changes.blNumber>${entity0.blNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span><@mark mark=changes.purchaseOrderNumber>${entity0.purchaseOrderNumber!"No Data"}</@mark></span>
				</td>
				<td>
					<@party name="Booking Office" party=entity0.bookingOffice! partychanges=changes.involvedParties.bookingOffice></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity0.forwarder! partychanges=changes.involvedParties.forwarder></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity0.consignee! partychanges=changes.involvedParties.consignee></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity0.contractParty! partychanges=changes.involvedParties.contractParty></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity0.mainNotifyParty! partychanges=changes.involvedParties.mainNotifyParty></@party>
					<#if entity0.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity0.firstAdditionalNotifyParty! partychanges=changes.involvedParties.firstAdditionalNotifyParty></@party>
					</#if>
					<#if entity0.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity0.secondAdditionalNotifyParty! partychanges=changes.involvedParties.secondAdditionalNotifyParty></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity0.freightPayerParty! partychanges=changes.involvedParties.freightPayerParty></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s Code, Voyage</b></span>
					<br>
					<span><@mark mark=changes.involvedParties.carrier.name>${(entity0.carrier.name)!"No data"}</@mark>, <@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.vesselName)!"No data"}, ${(entity0.mainTransportDetails.lloydsCode)!"No data"}, ${(entity0.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity0.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity0.mainTransportDetails changed=changes.transportationDetails.main></@plandetails>
					</td>
				</#if>
				<#if entity0.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity0.onCarriageTransportDetails changed=changes.transportationDetails.onCarriage></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list entity0.goodsDetails as cargo>
					<@tr mark=changes.goodsDetails>
						<td>
							(${cargo.lineNumber!"No Data"}) "Unknown Packing"
						</td>
						<td>
							${cargo.numberOfPackages!"No Data"} ${cargo.packageTypeDescription!"Unknown Type"}
						</td>
						<td>
							${cargo.goodsDescription!"No Data"}
						</td>
						<td>
							${cargo.grossWeight!"No Data"} ${cargo.grossWeightType!"Unknown Type"}
						</td>
						<td>
							${cargo.grossVolume!"No Data"} ${cargo.grossVolumeType!"Unknown Type"}
						</td>
					</@tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list entity0.equipments as equipment>
					<@tr mark=changes.equipments>
						<td>
							Container Number: No Data
							<br>
							<#if equipment.typeCode??>
								Type: ${equipment.typeCode!"No Data"}
								<br>
							</#if>
							<#if equipment.cargoMovementType??>
								Service Arrangement: ${equipment.cargoMovementType}
								<br>
							</#if>
							<#if equipment.haulageArrangements??>
								Haulage Arrangements: ${equipment.haulageArrangements}
								<br>
							</#if>
						</td>
						<td>-</td>
						<td>-</td>
					</@tr>
				</#list>
			</table>
		</p>

		<#macro party name party partychanges>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><@mark mark=partychanges.identifierType><span>${party.identifierType}: ${party.identifier}</span></@mark><br></#if>
				<@mark mark=partychanges.name><span>${party.name!""}<br></span></@mark>
				<@mark mark=partychanges.address><span>${party.address!"N/A"}<br></span></@mark>
			</#if>
		</#macro>

		<#macro plandetails name details changed>
			<@mark mark=changed>
			<b>${name}</b>
				<br>
				<#if entity0.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode}</span>
				</#if>
				<#if entity0.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier}</span>
				</#if>
				<br>
				<#if entity0.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName}
				</#if>
				<#-- lloyd''s code + voyage number -->
				<br>
				<#if entity0.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity0.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
			</@mark>
		</#macro>

		<#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
		<#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

	</body>
</html>'
WHERE template_key = 'bookingconfirmation.created.unknown.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
        Dear ${entity1.contactPersonName},<br>
        an update for an existing Booking Confirmation has been recorded in INTTRA and uploaded into the EDWH. This Booking Confirmation is missing a Purchase Order Number and can therefore not be automatically assigned to a Group Company. The INTTRA reference number is <@mark mark=changes.inttraRefNumber>${entity0.inttraRefNumber!"No Data"}</@mark>.

		<p>
		Carrier Response Type: <b>${entity0.transactionType!"No Data"}</b>
		</p>
		<p>
		<table>
			<tr>
				<td rowspan="3">
					<@party name="Booker" party=entity0.booker! partychanges=changes.involvedParties.booker></@party>
				</td>
				<td rowspan="3">
					<@party name="Shipper" party=entity0.shipper! partychanges=changes.involvedParties.shipper></@party>
				</td>
				<td>
					<@party name="Carrier/NVOC/Booking Agent" party=entity0.carrier! partychanges=changes.involvedParties.carrier></@party>
				</td>
				<td>
					<span><b>INTTRA Reference Number</b></span>
					<br>
					<span><@mark mark=changes.inttraRefNumber>${(entity0.inttraRefNumber)!"No Data"}</@mark></span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier Booking Number</b></span>
					<br>
					<span>No Data</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>Contract Number</b></span>
					<br>
					<span>No Data</span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span><@mark mark=changes.blNumber>${entity0.blNumber!"No data"}</@mark></span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span><@mark mark=changes.purchaseOrderNumber>${entity0.purchaseOrderNumber!"No Data"}</@mark></span>
				</td>
				<td>
					<@party name="Booking Office" party=entity0.bookingOffice! partychanges=changes.involvedParties.bookingOffice></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Forwarder" party=entity0.forwarder! partychanges=changes.involvedParties.forwarder></@party>
				</td>
				<td>
					<@party name="Consignee" party=entity0.consignee! partychanges=changes.involvedParties.consignee></@party>
				</td>
			</tr>
			<tr>
				<td>
					<@party name="Contract Party" party=entity0.contractParty! partychanges=changes.involvedParties.contractParty></@party>
				</td>
				<td>
					<span><b>Notify Party Details</b></span>
					<br>
					<@party name="Main Notify Party" party=entity0.mainNotifyParty! partychanges=changes.involvedParties.mainNotifyParty></@party>
					<#if entity0.firstAdditionalNotifyParty??>
						<br>
						<@party name="First Additional Notify Party" party=entity0.firstAdditionalNotifyParty! partychanges=changes.involvedParties.firstAdditionalNotifyParty></@party>
					</#if>
					<#if entity0.secondAdditionalNotifyParty??>
						<br>
						<@party name="Second Additional Notify Party" party=entity0.secondAdditionalNotifyParty! partychanges=changes.involvedParties.secondAdditionalNotifyParty></@party>
					</#if>
				</td>
				<td colspan="2">
					<@party name="Freight Payer Party" party=entity0.freightPayerParty! partychanges=changes.involvedParties.freightPayerParty></@party>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Carrier, Vessel, Lloyd''s Code, Voyage</b></span>
					<br>
					<span><@mark mark=changes.involvedParties.carrier.name>${(entity0.carrier.name)!"No data"}</@mark>, <@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.vesselName)!"No data"}, ${(entity0.mainTransportDetails.lloydsCode)!"No data"}, ${(entity0.mainTransportDetails.voyageNumber)!"No data"}</@mark></span>
				</td>
				<td>
					<span><b>Main Port of Load</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfLoad.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Sail Date</b></span>
					<br>
					<span>No Data</span>
				</td>
				<td colspan="2">
					<span><b>Main Port of Discharge</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.location)!"No Location"}</@mark></span>
					<br>
					<span><b>Estimated Arrival Date</b></span>
					<br>
					<span><@mark mark=changes.transportationDetails.main>${(entity0.mainTransportDetails.mainPortOfDischarge.estimatedArrival)!"No data"}</@mark></span>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
				<tr>
				<#if entity0.mainTransportDetails??>
					<td>
						<@plandetails name="Main Carriage 1" details=entity0.mainTransportDetails changed=changes.transportationDetails.main></@plandetails>
					</td>
				</#if>
				<#if entity0.onCarriageTransportDetails??>
					<td>
						<@plandetails name="On Carriage 1" details=entity0.onCarriageTransportDetails changed=changes.transportationDetails.onCarriage></@plandetails>
					</td>
				</#if>
				</tr>
			</table>
		</p>
		<br>

		<p>
			<table>
				<tr>
					<th><b>Cargo/Packing</b></th>
					<th><b>Packages</b></th>
					<th><b>Cargo Description</b></th>
					<th><b>Cargo Weight (Excluding Tare)</b></th>
					<th><b>Gross Volume</b></th>
				</tr>
				<#list entity0.goodsDetails as cargo>
					<@tr mark=changes.goodsDetails>
						<td>
							(${cargo.lineNumber!"No Data"}) "Unknown Packing"
						</td>
						<td>
							${cargo.numberOfPackages!"No Data"} ${cargo.packageTypeDescription!"Unknown Type"}
						</td>
						<td>
							${cargo.goodsDescription!"No Data"}
						</td>
						<td>
							${cargo.grossWeight!"No Data"} ${cargo.grossWeightType!"Unknown Type"}
						</td>
						<td>
							${cargo.grossVolume!"No Data"} ${cargo.grossVolumeType!"Unknown Type"}
						</td>
					</@tr>
				</#list>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Container</b></th>
					<th><b>Net Weight</b></th>
					<th><b>Net Volume</b></th>
				</tr>
				<#list entity0.equipments as equipment>
					<@tr mark=changes.equipments>
						<td>
							Container Number: No Data
							<br>
							<#if equipment.typeCode??>
								Type: ${equipment.typeCode!"No Data"}
								<br>
							</#if>
							<#if equipment.cargoMovementType??>
								Service Arrangement: ${equipment.cargoMovementType}
								<br>
							</#if>
							<#if equipment.haulageArrangements??>
								Haulage Arrangements: ${equipment.haulageArrangements}
								<br>
							</#if>
						</td>
						<td>-</td>
						<td>-</td>
					</@tr>
				</#list>
			</table>
		</p>

		<#macro party name party partychanges>
			<span><b>${name}</b></span>
			<#if party??>
				<br/>
				<#if party.identifierType??><@mark mark=partychanges.identifierType><span>${party.identifierType}: ${party.identifier}</span></@mark><br></#if>
				<@mark mark=partychanges.name><span>${party.name!""}<br></span></@mark>
				<@mark mark=partychanges.address><span>${party.address!"N/A"}<br></span></@mark>
			</#if>
		</#macro>

		<#macro plandetails name details changed>
			<@mark mark=changed>
			<b>${name}</b>
				<br>
				<#if entity0.mainTransportDetails.transportMode??>
					<span>Transport Mode: ${details.transportMode}</span>
				</#if>
				<#if entity0.mainTransportDetails.carrier??>
					<span>/ Carrier SCAC: ${details.carrier}</span>
				</#if>
				<br>
				<#if entity0.mainTransportDetails.vesselName??>
					<span>Vessel: ${details.vesselName}
				</#if>
				<#-- lloyd''s code + voyage number -->
				<br>
				<#if entity0.mainTransportDetails.mainPortOfLoad??>
					<span>Start Location: ${(details.mainPortOfLoad.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfLoad.earliestDeparture)!"No Data"}</span>
					<br>
				</#if>
				<#if entity0.mainTransportDetails.mainPortOfDischarge??>
					<span>Start Location: ${(details.mainPortOfDischarge.location)!"No Data"}/ </span>
					<span>Estimated Arrival: ${(details.mainPortOfDischarge.estimatedArrival)!"No Data"}</span>
				</#if>
			</@mark>
		</#macro>

		<#macro mark mark><#if mark><span style="background-color:yellow"><#nested></span><#else><#nested></#if></#macro>
		<#macro tr mark><#if mark><tr style="background-color:yellow"><#nested></tr><#else><tr><#nested></tr></#if></#macro>

	</body>
</html>'
WHERE template_key = 'bookingconfirmation.updated.unknown.body';