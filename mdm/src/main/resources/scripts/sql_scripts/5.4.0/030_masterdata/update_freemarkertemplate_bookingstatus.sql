UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear ${(entity1.contactPersonName)!""},<br>
		a new Booking Status for your contract ${(entity0.purchaseOrderNumber)!"No Data"} has been uploaded to INTTRA. The INTTRA document identifier is ${(entity0.documentIdentifier)!"No Data"}.

		<p>
		<table>
			<tr>
				<td>
					<span><b>Event Code</b></span>
					<br>
					<span>${(entity0.eventCode)!"No Data"}</span>
				</td>
				<td>
					<span><b>INTTRA Document Identifier</b></span>
					<br>
					<span>${(entity0.documentIdentifier)!"No Data"}</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span>${(entity0.billOfLadingNumber)!"No data"}</span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span>${(entity0.purchaseOrderNumber)!"No Data"}</span>
				</td>
				<td>
					<span><b>Event Location</b></span>
          <br>
          <br>
          <span><b>Location</b></span>
          <br>
          <span>${(entity0.eventLocation.location.unLocode)!"No data"}</span>
          <br>
          <br>
          <span><b>Location Type</b></span>
          <br>
          <span>${(entity0.eventLocation.type)!"No data"}</span>
          <br>
          <br>
          <span><b>Date</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTime)!"No data"}</span>
          <br>
          <br>
          <span><b>Date Type</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTimeType)!"No data"}</span>
          <br>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
        <#if entity0.transportDetails??>
        <#list entity0.transportDetails as transportDetail>
				<tr>
					<td>
						<span><b>Vessel Name</b></span>
            <br>
            <span>${(transportDetail.vesselName)!"No data"}</span>
            <br>
            <br>
            <span><b>Voyage Trip Number</b></span>
            <br>
            <span>${(transportDetail.voyageTripNumber)!"No data"}</span>
            <br>
            <br>
            <span><b>Carrier SCAC</b></span>
            <br>
            <span>${(transportDetail.carrierSCAC)!"No data"}</span>
            <br>
					</td>
          <td>
            <span><b>Locations</b></span>
            <ul>
              <#if transportDetail.locations??>
              <#list transportDetail.locations as location>
              <li>${(location.location.unLocode)!"No data"} (${(location.type)!"No data"}) - ${(location.dateTime)!"No data"} (${(location.dateTimeType)!"No data"})</li>
              </#list>
              </#if>
            </ul>
          </td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Equipment Details</b></th>
				</tr>
				<tr>
					<th><b>Line Number</b></th>
					<th><b>Container Number</b></th>
					<th><b>Load Type</b></th>
					<th><b>Container Type</b></th>
				</tr>
        <#if entity0.equipments??>
        <#list entity0.equipments as equipment>
				<tr>
					<td>${(equipment.lineNumber)!"No data"}</td>
					<td>${(equipment.containerNumber)!"No data"}</td>
					<td>${(equipment.loadType)!"No data"}</td>
					<td>${(equipment.containerType)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Shipment Comments</b></th>
				</tr>
				<tr>
					<th><b>Type</b></th>
					<th><b>Comment</b></th>
				</tr>
        <#if entity0.shipmentComment??>
        <#list entity0.shipmentComment as comment>
				<tr>
					<td>${(comment.type)!"No data"}</td>
					<td>${(comment.shipmentComment)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>

	</body>
</html>'
WHERE template_key = 'bookingstatus.created.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear ${entity1.contactPersonName},
		a new version of a Booking Status for your contract ${entity0.purchaseOrderNumber} has been uploaded to INTTRA. The INTTRA document identifier is ${(entity0.documentIdentifier)!"No Data"}.

		<p>
		<table>
			<tr>
				<td>
					<span><b>Event Code</b></span>
					<br>
					<span>${(entity0.eventCode)!"No Data"}</span>
				</td>
				<td>
					<span><b>INTTRA Document Identifier</b></span>
					<br>
					<span>${(entity0.documentIdentifier)!"No Data"}</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span>${(entity0.billOfLadingNumber)!"No data"}</span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span>${(entity0.purchaseOrderNumber)!"No Data"}</span>
				</td>
				<td>
					<span><b>Event Location</b></span>
          <br>
          <br>
          <span><b>Location</b></span>
          <br>
          <span>${(entity0.eventLocation.location.unLocode)!"No data"}</span>
          <br>
          <br>
          <span><b>Location Type</b></span>
          <br>
          <span>${(entity0.eventLocation.type)!"No data"}</span>
          <br>
          <br>
          <span><b>Date</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTime)!"No data"}</span>
          <br>
          <br>
          <span><b>Date Type</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTimeType)!"No data"}</span>
          <br>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
        <#if entity0.transportDetails??>
        <#list entity0.transportDetails as transportDetail>
				<tr>
					<td>
						<span><b>Vessel Name</b></span>
            <br>
            <span>${(transportDetail.vesselName)!"No data"}</span>
            <br>
            <br>
            <span><b>Voyage Trip Number</b></span>
            <br>
            <span>${(transportDetail.voyageTripNumber)!"No data"}</span>
            <br>
            <br>
            <span><b>Carrier SCAC</b></span>
            <br>
            <span>${(transportDetail.carrierSCAC)!"No data"}</span>
            <br>
					</td>
          <td>
            <span><b>Locations</b></span>
            <ul>
              <#if transportDetail.locations??>
              <#list transportDetail.locations as location>
              <li>${(location.location.unLocode)!"No data"} (${(location.type)!"No data"}) - ${(location.dateTime)!"No data"} (${(location.dateTimeType)!"No data"})</li>
              </#list>
              </#if>
            </ul>
          </td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Equipment Details</b></th>
				</tr>
				<tr>
					<th><b>Line Number</b></th>
					<th><b>Container Number</b></th>
					<th><b>Load Type</b></th>
					<th><b>Container Type</b></th>
				</tr>
        <#if entity0.equipments??>
        <#list entity0.equipments as equipment>
				<tr>
					<td>${(equipment.lineNumber)!"No data"}</td>
					<td>${(equipment.containerNumber)!"No data"}</td>
					<td>${(equipment.loadType)!"No data"}</td>
					<td>${(equipment.containerType)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Shipment Comments</b></th>
				</tr>
				<tr>
					<th><b>Type</b></th>
					<th><b>Comment</b></th>
				</tr>
        <#if entity0.shipmentComment??>
        <#list entity0.shipmentComment as comment>
				<tr>
					<td>${(comment.type)!"No data"}</td>
					<td>${(comment.shipmentComment)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>

	</body>
</html>'
WHERE template_key = 'bookingstatus.updated.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear ${entity1.contactPersonName},
		a new Booking Status for an unknown Group Company has been uploaded to INTTRA. The INTTRA document identifier is ${(entity0.documentIdentifier)!"No Data"}.

		<p>
		<table>
			<tr>
				<td>
					<span><b>Event Code</b></span>
					<br>
					<span>${(entity0.eventCode)!"No Data"}</span>
				</td>
				<td>
					<span><b>INTTRA Document Identifier</b></span>
					<br>
					<span>${(entity0.documentIdentifier)!"No Data"}</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span>${(entity0.billOfLadingNumber)!"No data"}</span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span>${(entity0.purchaseOrderNumber)!"No Data"}</span>
				</td>
				<td>
					<span><b>Event Location</b></span>
          <br>
          <br>
          <span><b>Location</b></span>
          <br>
          <span>${(entity0.eventLocation.location.unLocode)!"No data"}</span>
          <br>
          <br>
          <span><b>Location Type</b></span>
          <br>
          <span>${(entity0.eventLocation.type)!"No data"}</span>
          <br>
          <br>
          <span><b>Date</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTime)!"No data"}</span>
          <br>
          <br>
          <span><b>Date Type</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTimeType)!"No data"}</span>
          <br>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
        <#if entity0.transportDetails??>
        <#list entity0.transportDetails as transportDetail>
				<tr>
					<td>
						<span><b>Vessel Name</b></span>
            <br>
            <span>${(transportDetail.vesselName)!"No data"}</span>
            <br>
            <br>
            <span><b>Voyage Trip Number</b></span>
            <br>
            <span>${(transportDetail.voyageTripNumber)!"No data"}</span>
            <br>
            <br>
            <span><b>Carrier SCAC</b></span>
            <br>
            <span>${(transportDetail.carrierSCAC)!"No data"}</span>
            <br>
					</td>
          <td>
            <span><b>Locations</b></span>
            <ul>
              <#if transportDetail.locations??>
              <#list transportDetail.locations as location>
              <li>${(location.location.unLocode)!"No data"} (${(location.type)!"No data"}) - ${(location.dateTime)!"No data"} (${(location.dateTimeType)!"No data"})</li>
              </#list>
              </#if>
            </ul>
          </td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Equipment Details</b></th>
				</tr>
				<tr>
					<th><b>Line Number</b></th>
					<th><b>Container Number</b></th>
					<th><b>Load Type</b></th>
					<th><b>Container Type</b></th>
				</tr>
        <#if entity0.equipments??>
        <#list entity0.equipments as equipment>
				<tr>
					<td>${(equipment.lineNumber)!"No data"}</td>
					<td>${(equipment.containerNumber)!"No data"}</td>
					<td>${(equipment.loadType)!"No data"}</td>
					<td>${(equipment.containerType)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Shipment Comments</b></th>
				</tr>
				<tr>
					<th><b>Type</b></th>
					<th><b>Comment</b></th>
				</tr>
        <#if entity0.shipmentComment??>
        <#list entity0.shipmentComment as comment>
				<tr>
					<td>${(comment.type)!"No data"}</td>
					<td>${(comment.shipmentComment)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>

	</body>
</html>'
WHERE template_key = 'bookingstatus.created.unknown.body';

-- =====================================================

UPDATE freemarkertemplate SET template =
 '<html>
	<head>
	<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px;
		}
		td {
			vertical-align: top;
		}
		table {
			width: 100%;
			table-layout: fixed;
		}
	</style>
	</head>
	<body>
		Dear ${entity1.contactPersonName},
		a new version of a Booking Status for an unknown Group Company has been uploaded to INTTRA. The INTTRA document identifier is ${(entity0.documentIdentifier)!"No Data"}.

		<p>
		<table>
			<tr>
				<td>
					<span><b>Event Code</b></span>
					<br>
					<span>${(entity0.eventCode)!"No Data"}</span>
				</td>
				<td>
					<span><b>INTTRA Document Identifier</b></span>
					<br>
					<span>${(entity0.documentIdentifier)!"No Data"}</span>
				</td>
			</tr>
			<tr>
				<td>
					<span><b>Reference Numbers</b></span>
					<br>
					<br>
					<span><b>B/L Reference Number</b></span>
					<br>
					<span>${(entity0.billOfLadingNumber)!"No data"}</span>
					<br>
					<br>
					<span><b>Purchase Order Number</b></span>
					<br>
					<span>${(entity0.purchaseOrderNumber)!"No Data"}</span>
				</td>
				<td>
					<span><b>Event Location</b></span>
          <br>
          <br>
          <span><b>Location</b></span>
          <br>
          <span>${(entity0.eventLocation.location.unLocode)!"No data"}</span>
          <br>
          <br>
          <span><b>Location Type</b></span>
          <br>
          <span>${(entity0.eventLocation.type)!"No data"}</span>
          <br>
          <br>
          <span><b>Date</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTime)!"No data"}</span>
          <br>
          <br>
          <span><b>Date Type</b></span>
          <br>
          <span>${(entity0.eventLocation.dateTimeType)!"No data"}</span>
          <br>
				</td>
			</tr>
		</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Transport Plan Details</b></th>
				</tr>
        <#if entity0.transportDetails??>
        <#list entity0.transportDetails as transportDetail>
				<tr>
					<td>
						<span><b>Vessel Name</b></span>
            <br>
            <span>${(transportDetail.vesselName)!"No data"}</span>
            <br>
            <br>
            <span><b>Voyage Trip Number</b></span>
            <br>
            <span>${(transportDetail.voyageTripNumber)!"No data"}</span>
            <br>
            <br>
            <span><b>Carrier SCAC</b></span>
            <br>
            <span>${(transportDetail.carrierSCAC)!"No data"}</span>
            <br>
					</td>
          <td>
            <span><b>Locations</b></span>
            <ul>
              <#if transportDetail.locations??>
              <#list transportDetail.locations as location>
              <li>${(location.location.unLocode)!"No data"} (${(location.type)!"No data"}) - ${(location.dateTime)!"No data"} (${(location.dateTimeType)!"No data"})</li>
              </#list>
              </#if>
            </ul>
          </td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Equipment Details</b></th>
				</tr>
				<tr>
					<th><b>Line Number</b></th>
					<th><b>Container Number</b></th>
					<th><b>Load Type</b></th>
					<th><b>Container Type</b></th>
				</tr>
        <#if entity0.equipments??>
        <#list entity0.equipments as equipment>
				<tr>
					<td>${(equipment.lineNumber)!"No data"}</td>
					<td>${(equipment.containerNumber)!"No data"}</td>
					<td>${(equipment.loadType)!"No data"}</td>
					<td>${(equipment.containerType)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>
		<p>
			<table>
				<tr>
					<th><b>Shipment Comments</b></th>
				</tr>
				<tr>
					<th><b>Type</b></th>
					<th><b>Comment</b></th>
				</tr>
        <#if entity0.shipmentComment??>
        <#list entity0.shipmentComment as comment>
				<tr>
					<td>${(comment.type)!"No data"}</td>
					<td>${(comment.shipmentComment)!"No data"}</td>
        </tr>
        </#list>
				</#if>
			</table>
		</p>
		<br>

	</body>
</html>'
WHERE template_key = 'bookingstatus.updated.unknown.body';
