DROP TABLE IF EXISTS etl.exchangerate;

CREATE TABLE etl.exchangerate
(
    id bigserial,
    
    -- technical fields
    -- aggregates a number of individuals to one upload job
    job_id varchar,
    -- tells what to do with this individual (create(I)/update(U)/delete(D))
    update_operation char(1),
    -- timestamp when the etl has been imported
    import_date timestamp,
    -- name of the source application
    source varchar,
    -- ID in the source application
    source_pk varchar,

    -- mdm id
    mdm_id bigint,
    -- mdm version
    mdm_version bigint,
    
    -- business fields
    currency_from_id  bigint,
    currency_to_id    bigint,
    settlement_date   date,
    exchange_rate     decimal,
    
    CONSTRAINT PK_exchangerate PRIMARY KEY (id)
);

ALTER TABLE etl.exchangerate OWNER TO etl;
