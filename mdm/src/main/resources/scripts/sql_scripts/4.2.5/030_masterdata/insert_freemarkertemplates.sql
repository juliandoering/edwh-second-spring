INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '8ebf3bf9-ffa9-4810-b3f3-bbd3a114c0b2',
  -- key
  'contract.replaced.subject',
  -- template
  '[Contract] Contract "${(party.transactionReference)!""}" (${(contract.uuid)!"?"}) has been replaced.',
  -- template description
  'Contract replaced email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '2f2ebb8d-e9e4-4728-8f75-863a978a7948',
  -- key
  'contract.replaced.body.long',
  -- template
  'A contract has been replaced

  old UUID: ${(entity.uuid)!"UUID is missing"} 
  old link: ${(UriSelf)!"Link is missing"}

  new UUID: ${(entity2.uuid)!"UUID is missing"}
  new link: ${(UriSelf2)!"Link is missing"}

  ',
  -- template description
  'Contract replaced email body');