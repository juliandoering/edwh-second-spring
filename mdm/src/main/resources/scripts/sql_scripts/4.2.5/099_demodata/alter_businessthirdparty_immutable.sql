UPDATE businessthirdparty
  SET businessfunction_id =
     (SELECT bf.id
       FROM businessfunction bf
       WHERE bf.parent = (SELECT btp2.businessfunction_id from businessthirdparty btp2 where btp2.id = businessthirdparty.parent)  AND bf.is_version IS TRUE),
  legaladdress_id =
     (SELECT pa.id
       FROM postaladdress pa
       WHERE pa.parent = (SELECT btp2.legaladdress_id from businessthirdparty btp2 where btp2.id = businessthirdparty.parent)  AND pa.is_version IS TRUE),
  incoterm_id =
     (SELECT it.id
       FROM incoterm it
       WHERE it.parent = (SELECT btp2.incoterm_id from businessthirdparty btp2 where btp2.id = businessthirdparty.parent)  AND it.is_version IS TRUE)
  WHERE is_version = TRUE;