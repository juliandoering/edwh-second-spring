DROP TABLE IF EXISTS freightrate CASCADE;
DROP TABLE IF EXISTS freightratetraderoute CASCADE;
DROP TABLE IF EXISTS freightrateline CASCADE;
DROP TABLE IF EXISTS freightrateloading CASCADE;
DROP TABLE IF EXISTS freightrateloading_location CASCADE;
DROP TABLE IF EXISTS freightratetrade CASCADE;
