-- CAUTION takes about 7m for 40k business third parties
DO $$
DECLARE

btp_owners CURSOR FOR 
    SELECT
      btp.id AS id,
      i.identification_counter AS identification_counter,
      CONCAT(i.first_name, ' ', i.last_name) AS legalname,
      i.citizenship_id AS legal_country_id,
      i.tax_id AS tax_id,
      i.personal_id AS personal_id
    FROM mdm.businessthirdparty btp
    JOIN mdm.businessorganisationtype bot ON btp.businessorganisationtype_id = bot.id AND bot.code = 'SP'
    JOIN mdm.businessthirdparty_individual bi ON btp.id = bi.businessthirdparty_id AND bi.relation = 'owner'
    JOIN mdm.individual i ON bi.individual_id = i.id
    WHERE NOT btp.is_version;
    
next_uuid varchar;
refid_type_id bigint;


BEGIN
    
FOR btp_owner IN btp_owners LOOP
    UPDATE mdm.businessthirdparty 
    SET identification_counter=btp_owner.identification_counter,
        legalname=btp_owner.legalname,
        legal_country_id=btp_owner.legal_country_id
    WHERE id = btp_owner.id;
    
    IF NOT EXISTS
        (SELECT 1 FROM mdm.taxid WHERE businessthirdparty_id=btp_owner.id AND taxid=btp_owner.tax_id)
        AND btp_owner.tax_id IS NOT NULL
    THEN
        SELECT uuid_generate_v1() INTO next_uuid;
        INSERT INTO mdm.taxid (uuid, created_by, updated_by, businessthirdparty_id, taxid)
        VALUES (next_uuid, 'edwh_admin', 'edwh_admin', btp_owner.id, btp_owner.tax_id);
        
        INSERT INTO mdm.taxid (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, businessthirdparty_id, taxid) 
          SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, 
          (SELECT max(btp.id) FROM mdm.businessthirdparty btp WHERE btp.is_version AND btp.parent = businessthirdparty_id), 
          taxid FROM mdm.taxid 
          WHERE NOT is_version AND uuid = next_uuid;
    END IF;
    
    IF NOT EXISTS
        (SELECT 1 FROM mdm.businessthirdpartyreferenceid WHERE parent_businessthirdparty_id=btp_owner.id AND referenceid=btp_owner.personal_id)
        AND btp_owner.personal_id IS NOT NULL
    THEN
        SELECT uuid_generate_v1() INTO next_uuid;
        SELECT max(id) INTO refid_type_id FROM mdm.businessthirdpartyreferenceidtype WHERE code = 'PIN' AND NOT is_version;
        
        INSERT INTO mdm.businessthirdpartyreferenceid (uuid, created_by, updated_by, parent_businessthirdparty_id, businessthirdpartyreferenceidtype_id, referenceid)
        VALUES (next_uuid, 'edwh_admin', 'edwh_admin', btp_owner.id, refid_type_id, btp_owner.personal_id);
        
        INSERT INTO mdm.businessthirdpartyreferenceid (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, parent_businessthirdparty_id, businessthirdpartyreferenceidtype_id, referenceid) 
          SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, 
          (SELECT max(btp.id) FROM mdm.businessthirdparty btp WHERE btp.is_version AND btp.parent = parent_businessthirdparty_id), 
          (SELECT max(rit.id) FROM mdm.businessthirdpartyreferenceidtype rit WHERE rit.is_version AND rit.parent = businessthirdpartyreferenceidtype_id),
          referenceid FROM mdm.businessthirdpartyreferenceid 
          WHERE NOT is_version AND uuid = next_uuid;
    END IF;
END LOOP;

END $$