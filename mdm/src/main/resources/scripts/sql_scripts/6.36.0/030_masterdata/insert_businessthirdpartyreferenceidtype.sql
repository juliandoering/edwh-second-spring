DO $$
BEGIN
IF NOT EXISTS 
  (SELECT 1 FROM businessthirdpartyreferenceidtype WHERE code='PIN') 
THEN 
  insert into businessthirdpartyreferenceidtype (uuid, created_by, updated_by, code, description, idsuppliermandatory) 
  values ('3e5d9d06-75b4-11ea-bc55-0242ac130003', 'edwh_admin', 'edwh_admin', 'PIN', 'Personal Identification Number', false);
  
  insert into businessthirdpartyreferenceidtype (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description from businessthirdpartyreferenceidtype 
  where not is_version and uuid = '3e5d9d06-75b4-11ea-bc55-0242ac130003'; 
END IF;
END $$