DO $$
DECLARE
sortedlocationfunctions CURSOR FOR SELECT location_id, function_type FROM mdm.locationfunction ORDER BY function_type, location_id;

BEGIN
-- can be used later for updating the search index	
UPDATE mdm.location SET si_locationfunctions=null;

FOR function IN sortedlocationfunctions LOOP
    UPDATE mdm.location SET si_locationfunctions=CASE WHEN location.si_locationfunctions is null THEN function.function_type ELSE CONCAT(location.si_locationfunctions,', ', function.function_type) END
    WHERE mdm.location.id = function.location_id;
END LOOP;

END $$