-- Wahrehouse Replcace foreign key and index
ALTER TABLE warehouse DROP CONSTRAINT IF EXISTS fk_businesspartner;
DROP INDEX IF EXISTS idx_warehouse_businesspartner_id;
ALTER TABLE warehouse ADD CONSTRAINT fk_warehouse_groupcompany FOREIGN KEY (owner_id) REFERENCES groupcompany;
CREATE INDEX idx_warehouse_owner_id on warehouse(owner_id);