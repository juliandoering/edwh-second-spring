ALTER TABLE warehouse 
  ADD COLUMN latitude decimal;
ALTER TABLE warehouse 
  ADD COLUMN longitude decimal;

-- Rename reference
ALTER TABLE warehouse 
  RENAME COLUMN businesspartner_id TO owner_id;