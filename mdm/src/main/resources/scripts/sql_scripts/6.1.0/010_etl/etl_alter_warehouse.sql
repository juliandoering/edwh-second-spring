ALTER TABLE etl.warehouse 
  ADD COLUMN latitude decimal;
ALTER TABLE etl.warehouse 
  ADD COLUMN longitude decimal;
ALTER TABLE etl.warehouse 
  RENAME COLUMN businesspartner_id TO owner_id;