--changeset generated_nf:1497621355 
INSERT INTO delta_coffee_type (uuid,code,description)
	VALUES ('3138dfa6-79b7-41d5-96ee-4c841a712f6d','DELTA_HIGH','Delta High-Grades');
INSERT INTO delta_coffee_type (uuid,code,description)
	VALUES ('02ae6fe7-8c4e-4ef4-9b0a-6d3689a16510','DELTA_LOW_INT','Delta Low-Grades for international trade');
INSERT INTO delta_coffee_type (uuid,code,description)
	VALUES ('24c5058e-f8b0-4379-9513-4ca6d0c94bec','DELTA_LOW_LOCAL','Delta Low-Grades for local market');

insert into delta_coffee_type (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from delta_coffee_type where is_version is false;
