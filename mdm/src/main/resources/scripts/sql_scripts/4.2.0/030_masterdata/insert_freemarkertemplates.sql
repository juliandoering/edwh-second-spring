INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '8b414706-8cac-11e7-bb31-be2e44b06b34',
  -- key
  'contract.update.subject',
  -- template
  '[Contract] Contract ${(party.transactionReference)!"(?)"} (${(contract.uuid)!"?"}) was updated.',
  -- template description
  'Contract updated subject');

UPDATE freemarkertemplate
SET template='There are updates for contract

  buyer: ${(buyer.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(seller.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(seller.transactionReference)!"(no seller''s contract ID provided!)"}

  ${UriSelf} '
WHERE template_key='contract.update.body.long';

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'c414c548-8cad-11e7-bb31-be2e44b06b34',
  -- key
  'contract.created.subject',
  -- template
  '[Contract] New Contract "${(party.transactionReference)!""}" (${(contract.uuid)!"?"}) created.',
  -- template description
  'Contract created email subject');

UPDATE freemarkertemplate
SET template='There is a new contract

  buyer: ${(buyer.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(seller.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(seller.transactionReference)!"(no seller''s contract ID provided!)"}

  ${UriSelf} '
WHERE template_key='contract.created.body.long';

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'ca5fbf1a-8cae-11e7-bb31-be2e44b06b34',
  -- key
  'contract.download.confirmation.subject',
  -- template
  '[Contract] Download of contract "${(party.transactionReference)!""} (${(contract.uuid)!"?"})" has been confirmed.',
  -- template description
  'Contract download confirmation email subject');

UPDATE freemarkertemplate
SET template='[Contract] Bulk upload of contract "${(party.transactionReference)!""} (${(contract.uuid)!"?"})" is not consistent.'
WHERE template_key='contract.bulk.upload.conflict.subject';

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '62cfc9d4-8caf-11e7-bb31-be2e44b06b34',
  -- key
  'contract.created.warning.subject',
  -- template
  '[Contract] New Contract "${(party.transactionReference)!""}" (${(contract.uuid)!"?"}) with warning created.',
  -- template description
  'Contract created with warning email subject');


INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'eb6f0a72-8cb2-11e7-bb31-be2e44b06b34',
  -- key
  'fixation.created.subject',
  -- template
  'New fixation for contract "${party.transactionReference}" created.',
  -- template description
  'New price fixation notification email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '208ec490-8cb3-11e7-bb31-be2e44b06b34',
  -- key
  'fixation.download.confirmation.subject',
  -- template
  'The download of fixation with number ${entity.fixationNumber} for contract "${party.transactionReference}" has been confirmed.',
  -- template description
  'Price fixation download confirmation notification email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '77d58414-8cb3-11e7-bb31-be2e44b06b34',
  -- key
  'fixation.upload.existed.subject',
  -- template
  '[Price Fixation] Price Fixation "${party.transactionReference}" already uploaded',
  -- template description
  'second upload of existing price fixation email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'cab8e3b0-8cb3-11e7-bb31-be2e44b06b34',
  -- key
  'fixation.upload.conflict.subject',
  -- template
  '[Price Fixation] Uploaded Price Fixation "${party.transactionReference}" in conflict with already uploaded Price Fixation',
  -- template description
  'conflicting upload of existing price fixation email subject');


INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '97e18254-8cb7-11e7-bb31-be2e44b06b34',
  -- key
  'shipping.instructions.created.subject',
  -- template
  '[Shipment] New shipping instructions "${entity.shippingInstructionNumber}" created',
  -- template description
  'New shipping instructions email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'cc876c76-8cb7-11e7-bb31-be2e44b06b34',
  -- key
  'shipping.instructions.updated.subject',
  -- template
  '[Shipping] Shipping instructions "${entity.shippingInstructionNumber}" updated',
  -- template description
  'update shipping instructions email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'f8314ee6-8cb7-11e7-bb31-be2e44b06b34',
  -- key
  'shipping.instructions.download.confirmation.subject',
  -- template
  'The download of shipping instructions with number ${entity.shippingInstructionNumber} for contract "${party.transactionReference}" has been confirmed.',
  -- template description
  'Shipping instructions download confirmation notification email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '28207e24-8cb8-11e7-bb31-be2e44b06b34',
  -- key
  'shipping.instructions.upload.existed.subject',
  -- template
  'The shipping instruction with number ${entity.shippingInstructionNumber} for contract "${party.transactionReference}" already exists',
  -- template description
  'Shipping instructions second upload email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '43490ce8-8cb8-11e7-bb31-be2e44b06b34',
  -- key
  'shipping.instructions.upload.conflict.subject',
  -- template
  'Uploaded shipping instruction ${entity.shippingInstructionNumber} for contract "${party.transactionReference}" in conflict',
  -- template description
  'Shipping instructions upload conflict email subject');


INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '74dbb44a-8cb8-11e7-bb31-be2e44b06b34',
-- key
'shipmentadvice.created.subject', 
-- template
'[Shipment] New Shipmentadvice "${(shipmentadvice.uuid)!"?"}" created',
-- template description
'New shipmentadvice email subject');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  '9f9524e6-8cb8-11e7-bb31-be2e44b06b34',
-- key
'shipmentadvice.updated.subject', 
-- template
'[Shipment] Shipmentadvice "${(shipmentadvice.uuid)!"?"}" updated',
-- template description
'update shipmentadvice email subject');

INSERT INTO freemarkertemplate (uuid, template_key,template,description)
VALUES (
  'bc0c59b4-8cb8-11e7-bb31-be2e44b06b34',
-- key
'shipmentadvice.download.confirmation.subject', 
-- template
'The download of shipment advice with number ${entity.sequentialNumber} for contract "${party.transactionReference}" has been confirmed.',
-- template description
'Shipment advice download confirmation notification email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  'd2cc3c5a-8cb8-11e7-bb31-be2e44b06b34',
  -- key
  'shipmentadvice.upload.conflict.subject',
  -- template
  '[Shipment] Uploaded Shipment Advice "${party.transactionReference}" in conflict with already uploaded Shipment Advice',
  -- template description
  'conflicting upload of existing Shipment Advice email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '09168be4-8cb9-11e7-bb31-be2e44b06b34',
  -- key
  'shipmentadvice.upload.existed.subject',
  -- template
  '[Shipment] Shipment Advice "${party.transactionReference}" already uploaded',
  -- template description
  'second upload of existing Shipment Advice email subject');
