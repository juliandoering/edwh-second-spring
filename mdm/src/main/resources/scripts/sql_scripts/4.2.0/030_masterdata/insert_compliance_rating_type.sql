INSERT INTO compliance_rating_type (uuid, code, description) VALUES ('dfbcaa91-9d86-452a-980d-f1b6c7ccf77a', 'HIGH','HIGH');
INSERT INTO compliance_rating_type (uuid, code, description) VALUES ('e09c3b58-26a9-4ce1-a2b0-e267be7d5923', 'MEDIUM','MEDIUM');
INSERT INTO compliance_rating_type (uuid, code, description) VALUES ('4c4d8467-4a69-4322-be96-6dbc48d8253c', 'LOW','LOW');
INSERT INTO compliance_rating_type (uuid, code, description) VALUES ('13ce6635-e35f-4dc9-a007-5f8072fa9b0e', 'UNRATED','Not rated yet');
INSERT INTO compliance_rating_type (uuid, code, description) VALUES ('58e88452-a856-4379-8c9b-fe68b4885704', 'REJECTED','REJECTED');

insert into compliance_rating_type (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from compliance_rating_type where is_version is false;