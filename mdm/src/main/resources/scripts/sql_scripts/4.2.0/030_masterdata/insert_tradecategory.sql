INSERT INTO tradecategory(uuid, code, description)
  VALUES ('a692d9ce-9ef4-11e7-be27-00ff0b1c94f7', 'Normal', 'Normal');
INSERT INTO tradecategory(uuid, code, description)
  VALUES ('a692d9cf-9ef4-11e7-be27-00ff0b1c94f7', 'EFP', 'Exchange for Physical');
INSERT INTO tradecategory(uuid, code, description)
  VALUES ('a692d9d0-9ef4-11e7-be27-00ff0b1c94f7', 'EFS', 'Exchange for Swaps');
INSERT INTO tradecategory(uuid, code, description)
  VALUES ('a692d9d1-9ef4-11e7-be27-00ff0b1c94f7', 'EOO', 'Exchange of Options for Options');

insert into tradecategory (uuid, version, is_version, parent, code, description)
  select uuid, version, true, id, code, description from tradecategory where is_version is false;