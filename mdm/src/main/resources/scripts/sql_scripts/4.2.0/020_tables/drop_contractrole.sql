ALTER TABLE contractrole_businessfunction ADD contractrole varchar;

UPDATE contractrole_businessfunction cb SET contractrole = (SELECT cr.description FROM contractrole cr WHERE cr.id = cb.contractrole_id);

ALTER TABLE contractrole_businessfunction DROP contractrole_id;


ALTER TABLE contract ADD instr_info_contractrole varchar;

UPDATE contract c SET instr_info_contractrole = (SELECT cr.description FROM contractrole cr WHERE cr.id = c.instr_info_contractrole_id);

ALTER TABLE contract DROP instr_info_contractrole_id;


ALTER TABLE contract ADD coffeew_contractrole varchar;

UPDATE contract c SET coffeew_contractrole = (SELECT cr.description FROM contractrole cr WHERE cr.id = c.coffeew_contractrole_id);

ALTER TABLE contract DROP coffeew_contractrole_id;


DROP TABLE contractrole;
