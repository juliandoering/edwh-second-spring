-- create the table for delta coffee types
CREATE TABLE delta_coffee_type (
	id bigserial,
        uuid varchar,
        /* activeJdbc audit information */
        created_at timestamp default current_timestamp,
        updated_at timestamp default current_timestamp,

        /* optimistic locking */
        record_version integer default 0,

        -- soft delete information
        deleted boolean NOT NULL default FALSE,
        /* version information */
        version bigint NOT NULL default 0,
        is_version boolean NOT NULL default FALSE,
        parent bigint NOT NULL default 0,

        /* payload */
        code varchar,
        description varchar,

        CONSTRAINT PK_delta_coffee_type PRIMARY KEY (id)
	
);
