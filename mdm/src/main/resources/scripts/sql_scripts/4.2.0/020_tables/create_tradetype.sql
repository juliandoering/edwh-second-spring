--changeset af:1498058286
/* Type of trade, e.g. 'Purchase', "Sales" */
CREATE TABLE tradetype
(
	id bigserial,
	uuid varchar,
	/* activeJdbc audit information */
	created_at timestamp default current_timestamp,
	updated_at timestamp default current_timestamp,
    
	/* optimistic locking */
	record_version integer default 0,

	-- soft delete information
        deleted boolean NOT NULL default FALSE,
        /* version information */
        version bigint NOT NULL default 0,
        is_version boolean NOT NULL default FALSE,
        parent bigint NOT NULL default 0,
        
	/* payload */
	trade_type varchar,

    CONSTRAINT PK_trade_type PRIMARY KEY (id)
);
