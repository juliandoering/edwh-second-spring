UPDATE bankaccount_number_type SET description = 'International Bank Account Number' WHERE code = 'IBAN';
UPDATE bankaccount_number_type SET description = 'OTHER' WHERE code = 'OTHER';
UPDATE bankaccount_number_type SET description = 'Bank Identifier Code & Number' WHERE code = 'BIC+NUMBER';