CREATE TABLE bankholiday
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    holiday date,
    description varchar,
    futuresmarket_id bigint,

    CONSTRAINT PK_bankholiday PRIMARY KEY (id)
);

CREATE INDEX idx_bankholiday_uuid on bankholiday(uuid);

ALTER TABLE bankholiday ADD CONSTRAINT fk_bankholiday_futuresmarket FOREIGN KEY (futuresmarket_id) REFERENCES FutureMarket;
CREATE INDEX idx_bankholiday_futuresmarket_id on bankholiday(futuresmarket_id);
