-- by hand ajustment, because ALTER TABLE ... ALTER COLUMN ... TYPE ... USING ... always ran into a disconnnect

ALTER TABLE mdm.businessorganisationtype
  RENAME checkowner TO checkowner_old;
ALTER TABLE mdm.businessorganisationtype
  RENAME checkultimatebeneficiaryowner TO checkultimatebeneficiaryowner_old;
ALTER TABLE mdm.businessorganisationtype
  RENAME checkrepresentative TO checkrepresentative_old;
ALTER TABLE mdm.businessorganisationtype
  RENAME checkcontactperson TO checkcontactperson_old;

ALTER TABLE mdm.businessorganisationtype
  ADD COLUMN checkowner varchar;
ALTER TABLE mdm.businessorganisationtype
  ADD COLUMN checkultimatebeneficiaryowner varchar;
ALTER TABLE mdm.businessorganisationtype
  ADD COLUMN checkrepresentative varchar;
ALTER TABLE mdm.businessorganisationtype
  ADD COLUMN checkcontactperson varchar;

UPDATE mdm.businessorganisationtype SET
  checkowner=CASE WHEN checkowner_old THEN 'ONE_OR_MORE' ELSE 'ANY_NUMBER' END,
  checkultimatebeneficiaryowner=CASE WHEN checkultimatebeneficiaryowner_old THEN 'ONE_OR_MORE' ELSE 'ANY_NUMBER' END,
  checkrepresentative=CASE WHEN checkrepresentative_old THEN 'ONE_OR_MORE' ELSE 'ANY_NUMBER' END,
  checkcontactperson=CASE WHEN checkcontactperson_old THEN 'ONE_OR_MORE' ELSE 'ANY_NUMBER' END;

ALTER TABLE mdm.businessorganisationtype
  DROP checkowner_old;
ALTER TABLE mdm.businessorganisationtype
  DROP checkultimatebeneficiaryowner_old;
ALTER TABLE mdm.businessorganisationtype
  DROP checkrepresentative_old;
ALTER TABLE mdm.businessorganisationtype
  DROP checkcontactperson_old;
