INSERT INTO bankholiday (uuid, holiday, description, futuresmarket_id, created_by, updated_by)
  SELECT uuid_generate_v1(), holiday, description, futuresmarket_id, 'edwh_admin', 'edwh_admin' FROM holidays WHERE is_version is false;

-- immutable versions for Bank Holiday

INSERT INTO bankholiday (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, holiday, description, futuresmarket_id) 
  SELECT uuid, created_at, created_by, updated_at, updated_by, version, true, id, holiday, description
, (SELECT max(FutureMarket.id) FROM FutureMarket WHERE FutureMarket.is_version is true AND FutureMarket.parent = bankholiday.futuresmarket_id) 
  FROM bankholiday WHERE is_version is false;