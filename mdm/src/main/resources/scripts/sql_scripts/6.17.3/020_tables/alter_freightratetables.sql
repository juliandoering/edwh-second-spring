DROP TABLE freightrateloading_location;
DROP TABLE freightrate;
DROP TABLE freightratetraderoute;
DROP TABLE freightrateline;
DROP TABLE freightrateloading;
DROP TABLE freightratetrade;


CREATE TABLE freightratetrade
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    name varchar,

    CONSTRAINT PK_freightratetrade PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetrade_uuid on freightratetrade(uuid);

CREATE TABLE freightrateloading
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    name varchar,
    freightratetrade_id bigint,

    CONSTRAINT PK_freightrateloading PRIMARY KEY (id)
);

CREATE INDEX idx_freightrateloading_uuid on freightrateloading(uuid);

CREATE TABLE freightrateloading_location
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    freightrateloading_id bigint,
    location_id bigint,

    CONSTRAINT PK_freightrateloading_location PRIMARY KEY (id)
);

CREATE INDEX idx_freightrateloading_location_uuid on freightrateloading_location(uuid);

CREATE TABLE freightrateline
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    freightrateloading_id bigint,
    provider_id bigint,

    CONSTRAINT PK_freightrateline PRIMARY KEY (id)
);

CREATE INDEX idx_freightrateline_uuid on freightrateline(uuid);

CREATE TABLE freightratetraderoute
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    transshipment varchar,
    frequency numeric,
    freightrateLine_id bigint,
    podlocation_id bigint,

    CONSTRAINT PK_freightratetraderoute PRIMARY KEY (id)
);

CREATE INDEX idx_freightratetraderoute_uuid on freightratetraderoute(uuid);

CREATE TABLE freightrate
(
    id bigserial,
    uuid varchar,

    -- technical fields
    created_at timestamp default current_timestamp,
    created_by varchar,
    updated_at timestamp default current_timestamp,
    updated_by varchar,
    deleted boolean NOT NULL default FALSE,
    record_version integer default 0,
    version bigint NOT NULL default 0,
    is_version boolean NOT NULL default FALSE,
    parent bigint NOT NULL default 0,

    -- business fields
    rate decimal,
    transittime numeric,
    inlandcost decimal,
    inlandcostincluded boolean,
    inlandtransport varchar,
    thc decimal,
    thcincluded boolean,
    preference boolean,
    comment varchar,
    valid_from date,
    valid_to date,
    freightratetraderoute_id bigint,
    containersize_id bigint,
    containermovement_id bigint,
    oceanfreightcurrency_id bigint,
    inlandcostcurrency_id bigint,
    thccurrency_id bigint,

    CONSTRAINT PK_freightrate PRIMARY KEY (id)
);

CREATE INDEX idx_freightrate_uuid on freightrate(uuid);




ALTER TABLE freightrateloading ADD CONSTRAINT fk_freightrateloading_freightratetrade FOREIGN KEY (freightratetrade_id) REFERENCES FreightRateTrade;
CREATE INDEX idx_freightrateloading_freightratetrade_id on freightrateloading(freightratetrade_id);


ALTER TABLE freightrateloading_location ADD CONSTRAINT fk_freightrateloading_location_freightrateloading FOREIGN KEY (freightrateloading_id) REFERENCES FreightRateLoading;
CREATE INDEX idx_freightrateloading_location_freightrateloading_id on freightrateloading_location(freightrateloading_id);

ALTER TABLE freightrateloading_location ADD CONSTRAINT fk_freightrateloading_location_location FOREIGN KEY (location_id) REFERENCES Location;
CREATE INDEX idx_freightrateloading_location_location_id on freightrateloading_location(location_id);


ALTER TABLE freightrateline ADD CONSTRAINT fk_freightrateline_freightrateloading FOREIGN KEY (freightrateloading_id) REFERENCES FreightRateLoading;
CREATE INDEX idx_freightrateline_freightrateloading_id on freightrateline(freightrateloading_id);


ALTER TABLE freightrateline ADD CONSTRAINT fk_freightrateline_provider FOREIGN KEY (provider_id) REFERENCES businessthirdpartygroup (id);
CREATE INDEX idx_freightline_provider_id on mdm.freightrateline(provider_id);


ALTER TABLE freightratetraderoute ADD CONSTRAINT fk_freightratetraderoute_freightrateLine FOREIGN KEY (freightrateLine_id) REFERENCES FreightRateLine;
CREATE INDEX idx_freightratetraderoute_freightrateLine_id on freightratetraderoute(freightrateLine_id);


ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_freightratetraderoute FOREIGN KEY (freightratetraderoute_id) REFERENCES FreightRateTradeRoute;
CREATE INDEX idx_freightrate_freightratetraderoute_id on freightrate(freightratetraderoute_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_containersize FOREIGN KEY (containersize_id) REFERENCES ContainerSize;
CREATE INDEX idx_freightrate_containersize_id on freightrate(containersize_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_containermovement FOREIGN KEY (containermovement_id) REFERENCES ContainerMovement;
CREATE INDEX idx_freightrate_containermovement_id on freightrate(containermovement_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_inlandcostcurrency FOREIGN KEY (inlandcostcurrency_id) REFERENCES Currency;
CREATE INDEX idx_freightrate_inlandcostcurrency_id on freightrate(inlandcostcurrency_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_oceanfreightcurrency FOREIGN KEY (oceanfreightcurrency_id) REFERENCES Currency;
CREATE INDEX idx_freightrate_oceanfreightcurrency_id on freightrate(oceanfreightcurrency_id);

ALTER TABLE freightrate ADD CONSTRAINT fk_freightrate_thccurrency FOREIGN KEY (thccurrency_id) REFERENCES Currency;
CREATE INDEX idx_freightrate_thccurrency_id on freightrate(thccurrency_id);
