ALTER TABLE futuremarket
ADD trading_months integer;

ALTER TABLE futuremarket
ADD price_interval decimal;

-- insert the max trading months for LKD and KC
UPDATE futuremarket 
	SET trading_months = 10
	WHERE code = 'LKD';
UPDATE futuremarket 
	SET trading_months = 15
	WHERE code = 'KC';
-- insert the price interval for LKD and KC
UPDATE futuremarket 
	SET price_interval = 1.00
	WHERE code = 'LKD';
UPDATE futuremarket 
	SET price_interval = 0.01
	WHERE code = 'KC';

	