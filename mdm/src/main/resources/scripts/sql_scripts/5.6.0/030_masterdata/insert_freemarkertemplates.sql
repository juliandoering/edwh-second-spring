/*
 * Script to insert freemarker templates that are used to build e.g. email notification text
 * */

/* <INDIVIDUALS> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7c926-b74b-11e8-96f8-529269fb1459',
  -- key
  'individual.updated.body',
  -- template
  'An individual was updated

  ${(entity0.lastName)!""}, ${(entity0.firstName)!""}, ${(entity0.placeOfBirth.countryCode)!""}
  ${UriSelf}',
  -- template description
  'update individual email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7cd54-b74b-11e8-96f8-529269fb1459',
  -- key
  'individual.created.body',
  -- template
  'An individual entry was created

${(entity0.lastName)!""}, ${(entity0.firstName)!""}, ${(entity0.placeOfBirth.countryCode)!""}
  ${UriSelf}',
  -- template description
  'new individual email body');
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7d39e-b74b-11e8-96f8-529269fb1459',
  -- key
  'individual.updated.subject',
  -- template
  '[individual] updated',
  -- template description
  'update individual email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7d650-b74b-11e8-96f8-529269fb1459',
  -- key
  'individual.created.subject',
  -- template
  '[individual] created',
  -- template description
  'new individual email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7d8f8-b74b-11e8-96f8-529269fb1459',
  -- key
  'individual.deleted.body',
  -- template
  'An individual entry was deleted

 ${(entity0.lastName)!""}, ${(entity0.firstName)!""}, ${(entity0.placeOfBirth.countryCode)!""}',
  -- template description
  'delete individual email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7db82-b74b-11e8-96f8-529269fb1459',
  -- key
  'individual.deleted.subject',
  -- template
  '[individual] deleted',
  -- template description
  'delete individual email subject');
/* </INDIVIDUAL> */
  
  /*
 * Script to insert freemarker templates that are used to build e.g. email notification text
 * */

/* <COFFEE QUALITY COMMERCIAL NAME> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7f162-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequalitycommercialname.updated.body',
  -- template
  'A coffee quality commercial name was updated

  ${(entity0.name)!""}
  ${UriSelf}',
  -- template description
  'update coffeequalitycommercialname email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7f400-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequalitycommercialname.created.body',
  -- template
  'An coffee quality commercial name entry was created

  ${(entity0.name)!""}
  ${UriSelf}',
  -- template description
  'new coffeequalitycommercialname email body');
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7f680-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequalitycommercialname.updated.subject',
  -- template
  '[coffeequalitycommercialname] updated',
  -- template description
  'update coffeequalitycommercialname email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7f90a-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequalitycommercialname.created.subject',
  -- template
  '[coffee quality commercial name] created',
  -- template description
  'new coffeequalitycommercialname email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7fee6-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequalitycommercialname.deleted.body',
  -- template
  'A coffee quality commercial name entry was deleted

   ${(entity0.name)!""}',
  -- template description
  'delete coffeequalitycommercialname email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a80170-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequalitycommercialname.deleted.subject',
  -- template
  '[coffee quality commercial name] deleted',
  -- template description
  'delete coffeequalitycommercialname email subject');
/* </COFFEE QUALITY COMMERCIAL NAME> */
 
/*
 * Script to insert freemarker templates that are used to build e.g. email notification text
 * */

/* <COFFEE QUALITY> */
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7de34-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequality.updated.body',
  -- template
  'A coffee quality was updated

  ${(entity0.name)!""}
  ${UriSelf}',
  -- template description
  'update coffeequality email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7e0d2-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequality.created.body',
  -- template
  'An coffee quality entry was created

  ${(entity0.name)!""}
  ${UriSelf}',
  -- template description
  'new coffeequality email body');
INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7e3ca-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequality.updated.subject',
  -- template
  '[coffeequality] updated',
  -- template description
  'update coffeequality email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7e992-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequality.created.subject',
  -- template
  '[coffee quality] created',
  -- template description
  'new coffeequality email subject');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7ec3a-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequality.deleted.body',
  -- template
  'A coffee quality entry was deleted

   ${(entity0.name)!""}',
  -- template description
  'delete coffeequality email body');

INSERT INTO freemarkertemplate (uuid, template_key, template, description)
VALUES (
  '35a7eece-b74b-11e8-96f8-529269fb1459',
  -- key
  'coffeequality.deleted.subject',
  -- template
  '[coffee quality] deleted',
  -- template description
  'delete coffeequality email subject');
/* </COFFEE QUALITY> */