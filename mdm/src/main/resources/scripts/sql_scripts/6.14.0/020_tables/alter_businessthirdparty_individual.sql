ALTER TABLE businessthirdparty_individual ADD COLUMN relation varchar;

UPDATE businessthirdparty_individual set relation = (
  SELECT rel.code FROM businessthirdparty_individual_relation rel 
  WHERE rel.id = businessthirdparty_individual.businessthirdparty_individual_relation_id);
  
ALTER TABLE businessthirdparty_individual DROP COLUMN businessthirdparty_individual_relation_id;

ALTER TABLE businessthirdparty_individual DROP COLUMN record_version;
ALTER TABLE businessthirdparty_individual DROP COLUMN deleted;
ALTER TABLE businessthirdparty_individual DROP COLUMN version;
ALTER TABLE businessthirdparty_individual DROP COLUMN is_version;
ALTER TABLE businessthirdparty_individual DROP COLUMN parent;

DROP TABLE businessthirdparty_individual_relation;
