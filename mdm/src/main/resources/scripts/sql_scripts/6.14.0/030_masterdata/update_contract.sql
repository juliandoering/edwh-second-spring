-- remove the columns which have been referenced the country qualifier
alter table mdm.contract drop column countryqualifier_id;
alter table mdm.contract drop column countrylocation_id;

-- enable real numbers for number of lots
alter table mdm.contract alter column number_of_lots type decimal;