-- Update existing quanity units with new values
UPDATE quantityunit  SET code = '1KG',  description = 'kilo' WHERE code = 'KGS';
UPDATE quantityunit  SET code = '1MT',  description = 'metric ton' WHERE code = 'MTS';
UPDATE quantityunit  SET code = '1LB',  description = 'pound' WHERE code = 'LBS';
UPDATE quantityunit  SET code = '46KG', description = '46 kilo bag (Quintal)', kg = 46 WHERE code = '46KB';
UPDATE quantityunit  SET code = '50KG', description = '50 kilo bag' WHERE code = '50KB';
UPDATE quantityunit  SET code = '60KG', description = '60 kilo bag' WHERE code = '60KB';
UPDATE quantityunit  SET code = '69KG', description = '69 kilo bag' WHERE code = '69KB';
UPDATE quantityunit  SET code = '70KG', description = '70 kilo bag' WHERE code = '70KB';

-- Add new quantity units
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6984a-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '100LB', '100 pound', 45.35923700);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e69ac0-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '5KG', '5 kilo bag', 5);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e69c14-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '15KG', '15 kilo bag', 15);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e69d4a-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '20KG', '20 kilo bag', 20);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a02e-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '35KG', '35 kilo bag', 35);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a18c-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '57.5KG', '57.5 kilo bag', 57.50000000);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a3c6-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '64KG', '64 kilo bag', 64);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a542-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', '100KG', '100 kilo bag', 100);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a678-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB250KG', '250 kilo big bag', 250);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a7a4-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB300KG', '300 kilo big bag', 300);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a8c6-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB500KG', '500 kilo big bag', 500);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6a9fc-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB600KG', '600 kilo big bag', 600);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6ab28-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB900KG', '900 kilo big bag', 900);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6ad6c-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB1MT', '1 metric ton big bag', 1000);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6aeb6-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'BB2MT', '2 metric ton big bag', 2000);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6afe2-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'LB16.905MT', '16.905 metric ton liner bag', 16905);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6b118-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'LB18.975MT', '18.975 metric ton liner bag', 18975);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6b244-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'LB20.7MT', '20.7 metric ton liner bag', 20700);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6b370-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'LB20.976MT', '20.976 metric ton liner bag', 20976);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6b62c-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'LB21.39MT', '21.39 metric ton liner bag', 21390);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6b938-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'LB21.6MT', '21.6 metric ton liner bag', 21600);
INSERT INTO quantityunit (uuid, created_by, updated_by, code, description, kg) VALUES ('79e6ba8c-65df-11e9-a923-1681be663d3e', 'edwh_admin', 'edwh_admin', 'CTN1MT', '1 metric ton carton', 1000);

insert into quantityunit (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description from quantityunit where is_version is false and code not in ('1KG',  '1MT',  '1LB',  '46KG', '50KG', '60KG', '69KG', '70KG');
