-- Insert Port of Loading & Port of Discharge
INSERT INTO location ( UUID, deleted, record_version, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, created_by, updated_by, si_unlocode )
VALUES ( 'ff98df88-7247-11e9-a923-1681be663d3e', FALSE, 0, 0, FALSE, 0, 'AR', 'BUE', 'Buenos Aires', NULL, '3435S','05840W', 'edwh_admin', 'edwh_admin', 'ARBUE');
INSERT INTO location (uuid,deleted,record_version,version,is_version,parent,country_code,location_code,name,location_type,latitude,longitude,created_by,updated_by,si_unlocode) 
VALUES ('d11270fc-7a27-11e8-b8b2-0242ac120006',false,0,0,false,0,'US','NYC','New York',NULL,'4042N','07400W','edwh_admin','edwh_admin','USNYC');

insert into location (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, country_code, location_code, name, location_type, latitude, longitude, si_unlocode)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, country_code, location_code, name, location_type, latitude, longitude, si_unlocode from location
  where is_version is false
  and uuid in ('d11270fc-7a27-11e8-b8b2-0242ac120006', 'ff98df88-7247-11e9-a923-1681be663d3e');
  
-- Insert trades

INSERT INTO freightratetrade (UUID, name)
VALUES ( 'ff98be5e-7247-11e9-a923-1681be663d3e', 'Argentina tea (FOB)');
-- e977830a-2f97-11e9-b210-d663bd873d93
-- Insert loadings

insert into freightratetrade (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from freightratetrade where is_version is false;

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( 'ff98c0f2-7247-11e9-a923-1681be663d3e', 'Buenos Aires',
    (select min(id) from freightratetrade where uuid = 'ff98be5e-7247-11e9-a923-1681be663d3e' and is_version is false) );
--    aff8f342-2f98-11e9-b210-d663bd873d93

INSERT INTO freightrateloading (UUID, name, freightratetrade_id)
VALUES ( '44e70fc6-7250-11e9-a923-1681be663d3e', 'Hamburg',
    (select min(id) from freightratetrade where uuid = 'ff98be5e-7247-11e9-a923-1681be663d3e' and is_version is false) );
-- b9124dc0-2f98-11e9-b210-d663bd873d93
insert into freightrateloading (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name, freightratetrade_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name,
    (select min(freightratetrade.id) from freightratetrade where freightratetrade.parent = freightrateloading.freightratetrade_id)
  from freightrateloading where is_version is false;

INSERT INTO freightrateloading_location (freightrateloading_id, location_id)
  select
    id,
    (select min(location.id) from location
     where location.is_version = freightrateloading.is_version
     and location.uuid = 'ff98df88-7247-11e9-a923-1681be663d3e')
  from freightrateloading where uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e';

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( 'fbcd3490-7250-11e9-a923-1681be663d3e',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdparty where is_version is false and uuid = '0ea1d0f1-89bd-11e8-899d-0242ac120006') );
-- 1e5e3f0a-2fac-11e9-b210-d663bd873d93

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( 'e1df3d58-7250-11e9-a923-1681be663d3e',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdparty where is_version is false and uuid = '0ea1d0f1-89bd-11e8-899d-0242ac120006') );
-- 638d6888-2fb3-11e9-b210-d663bd873d93  

insert into freightrateline (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, freightrateloading_id, provider_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id,
  (select min(freightrateloading.id) from freightrateloading where freightrateloading.is_version is true and freightrateloading.parent = freightrateline.freightrateloading_id),
  (select min(businessthirdparty.id) from businessthirdparty where businessthirdparty.is_version is true and businessthirdparty.parent = freightrateline.provider_id)
  from freightrateline where is_version is false;

-- add route

-- add route

INSERT INTO freightratetraderoute ( UUID, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency, freightrateline_id, inlandcostcurrency_id, podlocation_id )
VALUES ( '030bb2ea-7251-11e9-a923-1681be663d3e', 200, false, 'Hamburg-City', 27, 7,
  (select id from freightrateline where is_version is false and uuid = 'fbcd3490-7250-11e9-a923-1681be663d3e'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  -- 90733602-2fb3-11e9-b210-d663bd873d93

INSERT INTO freightratetraderoute ( UUID, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency, freightrateline_id, inlandcostcurrency_id, podlocation_id )
VALUES ( '0f4c44a2-7251-11e9-a923-1681be663d3e', 210, false, 'Hamburg-City', 38, 7,
  (select id from freightrateline where is_version is false and uuid = 'e1df3d58-7250-11e9-a923-1681be663d3e'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );
  -- d6911a80-2fb5-11e9-b210-d663bd873d93

insert into freightratetraderoute (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency, freightrateline_id, inlandcostcurrency_id, podlocation_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, inlandcost, inlandcostincluded, inlandtransport, transittime, frequency,
  (select min(freightrateline.id) from freightrateline where freightrateline.is_version is true and freightrateline.parent = freightratetraderoute.freightrateline_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightratetraderoute.inlandcostcurrency_id),
  (select min(location.id) from location where location.is_version is true and location.parent = freightratetraderoute.podlocation_id)
  from freightratetraderoute where is_version is false;


-- add rates

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '36436e6e-7251-11e9-a923-1681be663d3e', 2441, 100, true, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '030bb2ea-7251-11e9-a923-1681be663d3e'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '281ee924-7253-11e9-a923-1681be663d3e', 3441, 100, true, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '030bb2ea-7251-11e9-a923-1681be663d3e'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '36436e6e-7251-11e9-a923-1681be663d3e', 4441, 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '030bb2ea-7251-11e9-a923-1681be663d3e'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '281ee924-7253-11e9-a923-1681be663d3e', 6441, 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '030bb2ea-7251-11e9-a923-1681be663d3e'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);


INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '36436e6e-7251-11e9-a923-1681be663d3e', 3242, 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '0f4c44a2-7251-11e9-a923-1681be663d3e'),
  (select id from containersize where is_version is false and code = '40fthc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '281ee64a-7253-11e9-a923-1681be663d3e', 3242, 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '0f4c44a2-7251-11e9-a923-1681be663d3e'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

insert into freightrate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, rate, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, thccurrency_id, oceanfreightcurrency_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, rate, thc, preference, valid_from, valid_to,
  (select min(freightratetraderoute.id) from freightratetraderoute where freightratetraderoute.is_version is true and freightratetraderoute.parent = freightrate.freightratetraderoute_id),
  (select min(containersize.id) from containersize where containersize.is_version is true and containersize.parent = freightrate.containersize_id),
  (select min(containermovement.id) from containermovement where containermovement.is_version is true and containermovement.parent = freightrate.containermovement_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.oceanfreightcurrency_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.thccurrency_id)
  from freightrate where is_version is false;
