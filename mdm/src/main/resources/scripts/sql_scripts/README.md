# SQL scripts here

Scripts to create the databases and tables we use.

Tablenames are singular per convention.

This directory includes further directories to organize the sql scripts for liquibase.
On this level the next directories are named after the mdm version they are created for.


We want to be able to run just one script instead of each script in each folder inidividually.
Therefore there is a shell script to create two files which run all scripts.
One uses \i syntax and the other plain sql.
The files each contain all information of the scripts in the folders that start with digits in alphabetical order ('ls' order).
In each folder is supposed to be a readme.md which is used to create comments
to make the file sections and folder content better understandable.
The generated files are in the 'edwh-database-init-setup' folder.



We agreed on the one changing something in the sql to check the generated files in.
This way only one developer has to run the script and others can rely on having the most up to date version.

## How to find missing Index for FK columns
```sql
WITH fk_actions ( code, action ) AS (
    VALUES ( 'a', 'error' ),
        ( 'r', 'restrict' ),
        ( 'c', 'cascade' ),
        ( 'n', 'set null' ),
        ( 'd', 'set default' )
),
fk_list AS (
    SELECT pg_constraint.oid as fkoid, conrelid, confrelid as parentid,
        conname, relname, nspname,
        fk_actions_update.action as update_action,
        fk_actions_delete.action as delete_action,
        conkey as key_cols
    FROM pg_constraint
        JOIN pg_class ON conrelid = pg_class.oid
        JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid
        JOIN fk_actions AS fk_actions_update ON confupdtype = fk_actions_update.code
        JOIN fk_actions AS fk_actions_delete ON confdeltype = fk_actions_delete.code
    WHERE contype = 'f'
),
fk_attributes AS (
    SELECT fkoid, conrelid, attname, attnum
    FROM fk_list
        JOIN pg_attribute
            ON conrelid = attrelid
            AND attnum = ANY( key_cols )
    ORDER BY fkoid, attnum
),
fk_cols_list AS (
    SELECT fkoid, array_agg(attname) as cols_list
    FROM fk_attributes
    GROUP BY fkoid
),
index_list AS (
    SELECT indexrelid as indexid,
        pg_class.relname as indexname,
        indrelid,
        indkey,
        indpred is not null as has_predicate,
        pg_get_indexdef(indexrelid) as indexdef
    FROM pg_index
        JOIN pg_class ON indexrelid = pg_class.oid
    WHERE indisvalid
),
fk_index_match AS (
    SELECT fk_list.*,
        indexid,
        indexname,
        indkey::int[] as indexatts,
        has_predicate,
        indexdef,
        array_length(key_cols, 1) as fk_colcount,
        array_length(indkey,1) as index_colcount,
        round(pg_relation_size(conrelid)/(1024^2)::numeric) as table_mb,
        cols_list
    FROM fk_list
        JOIN fk_cols_list USING (fkoid)
        LEFT OUTER JOIN index_list
            ON conrelid = indrelid
            AND (indkey::int2[])[0:(array_length(key_cols,1) -1)] @> key_cols

),
fk_perfect_match AS (
    SELECT fkoid
    FROM fk_index_match
    WHERE (index_colcount - 1) <= fk_colcount
        AND NOT has_predicate
        AND indexdef LIKE '%USING btree%'
),
fk_index_check AS (
    SELECT 'no index' as issue, *, 1 as issue_sort
    FROM fk_index_match
    WHERE indexid IS NULL
    UNION ALL
    SELECT 'questionable index' as issue, *, 2
    FROM fk_index_match
    WHERE indexid IS NOT NULL
        AND fkoid NOT IN (
            SELECT fkoid
            FROM fk_perfect_match)
)
SELECT nspname as schema_name,
    relname as table_name,
    conname as fk_name,
    issue,
    table_mb,
    cols_list,
    indexdef,
    'CREATE INDEX idx_' || relname || '_' || array_to_string(cols_list, ',', '*') || ' on ' || relname || '(' || array_to_string(cols_list, ',', '*')  || ');'
FROM fk_index_check
WHERE nspname = 'mdm'
ORDER BY issue_sort, table_mb DESC, table_name, fk_name;
```

## How to find missing FK constraints
Check if the columns are really used for references. 

```sql
WITH fk_list AS
(
  SELECT conrelid,
         relname,
         nspname,
         conkey AS key_cols
  FROM pg_constraint
    JOIN pg_class ON conrelid = pg_class.oid
    JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid
  WHERE contype = 'f'
),
fk_attr_list AS
(
  SELECT nspname,
         relname,
         attname
  FROM fk_list
    JOIN pg_attribute
      ON conrelid = attrelid
     AND attnum = ANY (key_cols)
),
id_column_list AS
(
  SELECT t.table_schema,
         t.table_name,
         c.column_name
  FROM information_schema.tables t
    INNER JOIN information_schema.columns c
            ON c.table_name = t.table_name
           AND c.table_schema = t.table_schema
  WHERE c.column_name LIKE '%\_id'
  AND   t.table_schema IN ('mdm','dwh')
)
SELECT *,
       'ALTER TABLE ' || table_name || ' ADD CONSTRAINT fk_' || table_name || '_' || column_name || ' FOREIGN KEY (' || column_name || ') REFERENCES {{WHO KNOWS}};' AS add_constraint,
       'CREATE INDEX idx_' || table_name || '_' || column_name || ' on ' || table_name || '(' || column_name || ');' AS add_index
FROM id_column_list
WHERE NOT EXISTS (SELECT 1
                  FROM fk_attr_list
                  WHERE nspname = table_schema
                  AND   relname = table_name
                  AND   attname = column_name)
ORDER BY 1,
         2,
         3

```
