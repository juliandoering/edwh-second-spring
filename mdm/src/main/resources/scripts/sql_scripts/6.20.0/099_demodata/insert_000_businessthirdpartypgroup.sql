INSERT INTO businessthirdpartygroup (uuid, name) 
VALUES
  ('c67f077c-ff8f-11e8-8b41-0242ac130008', 'Pacorini'),
  ('36b4c970-01ef-11e9-8b41-0242ac130008', 'Dreyfus'),
  ('e2b9b31a-020a-11e9-8b41-0242ac130008', 'SGS'),
  ('03856766-7bcc-11e9-8929-0242c0a84007', 'Grimaldi'),
  ('ef4b0750-7bcb-11e9-8929-0242c0a84007', 'DAL'),
  ('0a0f9928-7bcc-11e9-8929-0242c0a84007', 'HH Süd'),
  ('f46d6bb2-7bcb-11e9-8929-0242c0a84007', 'APL'),
  ('fc8c2024-7bcb-11e9-8929-0242c0a84007', 'Evergreen'),
  ('ea9f0b6e-7bcb-11e9-8929-0242c0a84007', 'Wan Hai'),
  ('d1bfdc2e-7bcb-11e9-8929-0242c0a84007', 'CMA CGM'),
  ('e10094cc-7bcb-11e9-8929-0242c0a84007', 'COSCO');

insert into businessthirdpartygroup (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, name)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, name from businessthirdpartygroup where is_version is false;