-- add line

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f80ac-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'Wan Hai') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f8368-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'SGS') );
  
INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f84c6-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'SGS') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f85fc-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'Wan Hai') );
  
INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f8962-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'Evergreen') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f8aac-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'Pacorini') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f8bec-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'Pacorini') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f8d22-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'Evergreen') );
  
INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f907e-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = 'ff98c0f2-7247-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'APL') );

INSERT INTO freightrateline ( UUID, freightrateloading_id, provider_id )
VALUES ( '812f91d2-b455-11e9-a2a3-2a2ae2dbcce4',
  (select id from freightrateloading where is_version is false and uuid = '44e70fc6-7250-11e9-a923-1681be663d3e'),
  (select id from businessthirdpartygroup where is_version is false and name = 'APL') );

insert into freightrateline (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, freightrateloading_id, provider_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id,
  (select min(freightrateloading.id) from freightrateloading where freightrateloading.is_version is true and freightrateloading.parent = freightrateline.freightrateloading_id),
  (select min(businessthirdpartygroup.id) from businessthirdpartygroup where businessthirdpartygroup.is_version is true and businessthirdpartygroup.parent = freightrateline.provider_id)
  from freightrateline where is_version is false
  and uuid in ('812f80ac-b455-11e9-a2a3-2a2ae2dbcce4', '812f8368-b455-11e9-a2a3-2a2ae2dbcce4', '812f84c6-b455-11e9-a2a3-2a2ae2dbcce4', '812f85fc-b455-11e9-a2a3-2a2ae2dbcce4', '812f8962-b455-11e9-a2a3-2a2ae2dbcce4', '812f8aac-b455-11e9-a2a3-2a2ae2dbcce4', '812f8bec-b455-11e9-a2a3-2a2ae2dbcce4', '812f8d22-b455-11e9-a2a3-2a2ae2dbcce4', '812f907e-b455-11e9-a2a3-2a2ae2dbcce4', '812f91d2-b455-11e9-a2a3-2a2ae2dbcce4');

-- add route

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664af2c-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f80ac-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664b2c4-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f80ac-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664b436-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8368-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664b7a6-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f84c6-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664b8f0-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f85fc-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664ba1c-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8962-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664bb48-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8aac-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664bc74-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8bec-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664bea4-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8d22-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c250-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f907e-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c3ae-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f91d2-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c4e4-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f91d2-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c610-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f907e-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c732-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8d22-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c85e-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8bec-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664c980-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8bec-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664ccd2-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8962-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664ce1c-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f8aac-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );
  
INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664cf3e-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f85fc-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'DEHAM') );

INSERT INTO freightratetraderoute ( UUID, frequency, freightrateline_id, podlocation_id )
VALUES ( '5664d074-b456-11e9-a2a3-2a2ae2dbcce4', 7,
  (select id from freightrateline where is_version is false and uuid = '812f84c6-b455-11e9-a2a3-2a2ae2dbcce4'),
  (select id from location where is_version is false and si_unlocode = 'USNYC') );


insert into freightratetraderoute (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, frequency, freightrateline_id, podlocation_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, frequency,
  (select min(freightrateline.id) from freightrateline where freightrateline.is_version is true and freightrateline.parent = freightratetraderoute.freightrateline_id),
  (select min(location.id) from location where location.is_version is true and location.parent = freightratetraderoute.podlocation_id)
  from freightratetraderoute where is_version is false
  and uuid in (
  '5664af2c-b456-11e9-a2a3-2a2ae2dbcce4', '5664b2c4-b456-11e9-a2a3-2a2ae2dbcce4', '5664b436-b456-11e9-a2a3-2a2ae2dbcce4', '5664b7a6-b456-11e9-a2a3-2a2ae2dbcce4',
  '5664b8f0-b456-11e9-a2a3-2a2ae2dbcce4', '5664ba1c-b456-11e9-a2a3-2a2ae2dbcce4', '5664bb48-b456-11e9-a2a3-2a2ae2dbcce4', '5664bc74-b456-11e9-a2a3-2a2ae2dbcce4',
  '5664bea4-b456-11e9-a2a3-2a2ae2dbcce4', '5664c250-b456-11e9-a2a3-2a2ae2dbcce4', '5664c3ae-b456-11e9-a2a3-2a2ae2dbcce4', '5664c4e4-b456-11e9-a2a3-2a2ae2dbcce4',
  '5664c610-b456-11e9-a2a3-2a2ae2dbcce4', '5664c732-b456-11e9-a2a3-2a2ae2dbcce4', '5664c85e-b456-11e9-a2a3-2a2ae2dbcce4', '5664c980-b456-11e9-a2a3-2a2ae2dbcce4',
  '5664ccd2-b456-11e9-a2a3-2a2ae2dbcce4', '5664ce1c-b456-11e9-a2a3-2a2ae2dbcce4', '5664cf3e-b456-11e9-a2a3-2a2ae2dbcce4', '5664d074-b456-11e9-a2a3-2a2ae2dbcce4');

-- add rates
INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '4da68c5e-b44f-11e9-a2a3-2a2ae2dbcce4', 1450, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'eb9bb69e-9f16-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '4da68f74-b44f-11e9-a2a3-2a2ae2dbcce4', 1451, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'eb9bb69e-9f16-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40fthc'),
  (select id from containermovement where is_version is false and code = 'LCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '4da6919a-b44f-11e9-a2a3-2a2ae2dbcce4', 1452, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'eb9bb69e-9f16-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'CFS-CY'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '4da68c5e-b44f-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = 'eb9bb8f6-9f16-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e2d3c-b458-11e9-a2a3-2a2ae2dbcce4', 1450, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664af2c-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e2f9e-b458-11e9-a2a3-2a2ae2dbcce4', 1451, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664af2c-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'LCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e31a6-b458-11e9-a2a3-2a2ae2dbcce4', 1452, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664b2c4-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'CFS-CY'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e3692-b458-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664b436-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);


INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e3840-b458-11e9-a2a3-2a2ae2dbcce4', 1450, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664b7a6-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e3980-b458-11e9-a2a3-2a2ae2dbcce4', 1451, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664b8f0-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40fthc'),
  (select id from containermovement where is_version is false and code = 'LCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e3ab6-b458-11e9-a2a3-2a2ae2dbcce4', 1452, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664ba1c-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'CFS-CY'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e3be2-b458-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664bb48-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);


INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e3d0e-b458-11e9-a2a3-2a2ae2dbcce4', 1450, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664bc74-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e41c8-b458-11e9-a2a3-2a2ae2dbcce4', 1451, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664bea4-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40fthc'),
  (select id from containermovement where is_version is false and code = 'LCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e434e-b458-11e9-a2a3-2a2ae2dbcce4', 1452, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c250-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'CFS-CY'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e448e-b458-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c3ae-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);


INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e45c4-b458-11e9-a2a3-2a2ae2dbcce4', 1450, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c4e4-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e46f0-b458-11e9-a2a3-2a2ae2dbcce4', 1451, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c610-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40fthc'),
  (select id from containermovement where is_version is false and code = 'LCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e481c-b458-11e9-a2a3-2a2ae2dbcce4', 1452, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c732-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'CFS-CY'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e4948-b458-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c85e-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);


INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e4dee-b458-11e9-a2a3-2a2ae2dbcce4', 1450, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664c980-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e4f4c-b458-11e9-a2a3-2a2ae2dbcce4', 1451, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664ccd2-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '40fthc'),
  (select id from containermovement where is_version is false and code = 'LCL-LCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e5082-b458-11e9-a2a3-2a2ae2dbcce4', 1452, 27, 200, false, 'Hamburg-City', 100, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664ce1c-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'CFS-CY'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( 'e05e51ae-b458-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664cf3e-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

INSERT INTO freightrate ( UUID, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, oceanfreightcurrency_id, thccurrency_id )
VALUES ( '5acc2524-b459-11e9-a2a3-2a2ae2dbcce4', 2249, 38, 210, false, 'Hamburg-City', 110, false, '2019-01-01'::DATE, null,
  (select id from freightratetraderoute where is_version is false and uuid = '5664d074-b456-11e9-a2a3-2a2ae2dbcce4'),
  (select id from containersize where is_version is false and code = '20ftdc'),
  (select id from containermovement where is_version is false and code = 'FCL-FCL'),
  (select id from currency where is_version is false and code = 'USD'),
  (select id from currency where is_version is false and code = 'USD')
);

insert into freightrate (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to, freightratetraderoute_id, containersize_id, containermovement_id, thccurrency_id, oceanfreightcurrency_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, rate, transittime, inlandcost, inlandcostincluded, inlandtransport, thc, preference, valid_from, valid_to,
  (select min(freightratetraderoute.id) from freightratetraderoute where freightratetraderoute.is_version is true and freightratetraderoute.parent = freightrate.freightratetraderoute_id),
  (select min(containersize.id) from containersize where containersize.is_version is true and containersize.parent = freightrate.containersize_id),
  (select min(containermovement.id) from containermovement where containermovement.is_version is true and containermovement.parent = freightrate.containermovement_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.oceanfreightcurrency_id),
  (select min(currency.id) from currency where currency.is_version is true and currency.parent = freightrate.thccurrency_id)
  from freightrate where is_version is false
  and uuid in (
  'e05e2d3c-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e2f9e-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e31a6-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e3692-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e3840-b458-11e9-a2a3-2a2ae2dbcce4',
  'e05e3980-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e3ab6-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e3be2-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e3d0e-b458-11e9-a2a3-2a2ae2dbcce4', '5acc2524-b459-11e9-a2a3-2a2ae2dbcce4',
  'e05e41c8-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e434e-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e448e-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e45c4-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e46f0-b458-11e9-a2a3-2a2ae2dbcce4',
  'e05e481c-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e4948-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e4dee-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e4f4c-b458-11e9-a2a3-2a2ae2dbcce4', 'e05e5082-b458-11e9-a2a3-2a2ae2dbcce4',
  'e05e51ae-b458-11e9-a2a3-2a2ae2dbcce4',
  '1c9f1462-b451-11e9-a2a3-2a2ae2dbcce4', '1c9f18e0-b451-11e9-a2a3-2a2ae2dbcce4', '4da6919a-b44f-11e9-a2a3-2a2ae2dbcce4', '4da68f74-b44f-11e9-a2a3-2a2ae2dbcce4', '4da68c5e-b44f-11e9-a2a3-2a2ae2dbcce4');