ALTER TABLE etl.individual
  ADD COLUMN identification_counter integer default 1;
ALTER TABLE etl.individual
  ADD COLUMN comment varchar;
