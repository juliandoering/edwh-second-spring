ALTER TABLE etl.businessthirdparty
  ADD COLUMN legal_country_id bigint;
ALTER TABLE etl.businessthirdparty
  ADD COLUMN comment varchar;
ALTER TABLE etl.businessthirdpartybankaccount
  ADD COLUMN bank_branch_address_type_id bigint;