ALTER TABLE businessthirdparty
  ADD COLUMN legal_country_id bigint;
ALTER TABLE businessthirdparty
  ADD COLUMN identification_counter integer default 1;
ALTER TABLE businessthirdparty
  ADD COLUMN comment varchar;
  
-- not used columns
ALTER TABLE businessthirdparty
  DROP COLUMN legaladdress_id;
ALTER TABLE businessthirdparty
  DROP COLUMN postaladdress_id;
