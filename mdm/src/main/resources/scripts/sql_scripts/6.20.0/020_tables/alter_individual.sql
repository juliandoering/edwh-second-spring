ALTER TABLE individual
  ADD COLUMN identification_counter integer default 1;
ALTER TABLE individual
  ADD COLUMN comment varchar;
