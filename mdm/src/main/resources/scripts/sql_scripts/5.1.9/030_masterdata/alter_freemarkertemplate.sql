UPDATE freemarkertemplate SET template =
'[Shipping] New shipping instructions "${entity0.shippingInstructionNumber}" created'
WHERE template_key = 'shipping.instructions.created.subject';

UPDATE freemarkertemplate SET template =
'[Shipping] New shipping instructions "${entity0.shippingInstructionNumber}" created'
WHERE template_key = 'shipping.instructions.created.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Shipping] New shipping instructions "${entity0.shippingInstructionNumber}" created'
WHERE template_key = 'shipping.instructions.created.subject.seller';

UPDATE freemarkertemplate SET template =
'[Shipment] New Shipmentadvice "${(entity0.uuid)!"?"}" created'
WHERE template_key = 'shipmentadvice.created.subject';

UPDATE freemarkertemplate SET template =
'[Shipment] New Shipmentadvice "${(entity0.uuid)!"?"}" created'
WHERE template_key = 'shipmentadvice.created.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Shipment] New Shipmentadvice "${(entity0.uuid)!"?"}" created'
WHERE template_key = 'shipmentadvice.created.subject.seller';

UPDATE freemarkertemplate SET template =
'[Shipment] Shipmentadvice "${(entity0.uuid)!"?"}" updated'
WHERE template_key = 'shipmentadvice.updated.subject';

UPDATE freemarkertemplate SET template =
'[Shipment] Shipmentadvice "${(entity0.uuid)!"?"}" updated'
WHERE template_key = 'shipmentadvice.updated.subject.buyer';

UPDATE freemarkertemplate SET template =
'[Shipment] Shipmentadvice "${(entity0.uuid)!"?"}" updated'
WHERE template_key = 'shipmentadvice.updated.subject.seller';

UPDATE freemarkertemplate SET template =
'There is a new contract

  buyer: ${(contract.buyer.legalName)!"(legal name missing)"}
  buyer''s contract Id: ${(contract.buyer.transactionReference)!"(no buyer''s contract ID provided!)"}
  seller: ${(contract.seller.legalName)!"(legal name missing!)"}
  seller''s contract Id: ${(contract.seller.transactionReference)!"(no seller''s contract ID provided!)"}


  ${UriSelf0} '
WHERE template_key = 'contract.created.body.long';

UPDATE freemarkertemplate SET template =
'[Business Third Party] created'
WHERE template_key = 'businessthirdparty.created.subject';

UPDATE freemarkertemplate SET template =
'[Business Third Party] deleted'
WHERE template_key = 'businessthirdparty.deleted.subject';

UPDATE freemarkertemplate SET template =
'[Business Third Party] updated'
WHERE template_key = 'businessthirdparty.updated.subject';
