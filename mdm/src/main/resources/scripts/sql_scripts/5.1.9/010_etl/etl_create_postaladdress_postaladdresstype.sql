CREATE TABLE etl.postaladdress_postaladdresstype
(
    id bigserial,
    
    job_id varchar,

    postaladdress_id bigint,
    postaladdresstype_id bigint,

    CONSTRAINT PK_postaladdress_postaladdresstype PRIMARY KEY (id)
);

ALTER TABLE etl.postaladdress_postaladdresstype OWNER TO etl;

CREATE INDEX idx_postaladdress_postaladdresstype_job_id on etl.postaladdress_postaladdresstype(job_id);
