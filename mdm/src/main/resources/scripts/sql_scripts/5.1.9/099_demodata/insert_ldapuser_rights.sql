UPDATE userprofile SET 
    created_by = (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
    updated_by = (SELECT id FROM userprofile WHERE username = 'edwh_admin')
WHERE
    username = 'edwh-admin-test-user';