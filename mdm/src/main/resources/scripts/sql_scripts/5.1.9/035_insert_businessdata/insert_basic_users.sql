INSERT INTO userprofile
(
  UUID,
  deleted,
  record_version,
  username,
  email,
  userrole_id,
  created_by,
  updated_by
)
SELECT
  'a68bad99-9ef4-11e8-be27-00ff0b1c94f1',
  FALSE,
  0,
  'ncs-edwh-brhh',
  NULL,
  (SELECT id FROM userrole WHERE rolename = 'edwh_technical'),
  (SELECT id FROM userprofile WHERE username = 'edwh_admin'),
  (SELECT id FROM userprofile WHERE username = 'edwh_admin')
WHERE NOT EXISTS (SELECT 1 FROM userprofile WHERE username = 'ncs-edwh-brhh');
