CREATE TABLE postaladdress_postaladdresstype
(
    id bigserial,
    
    /* payload */ 
    postaladdress_id bigint,
    postaladdresstype_id bigint,
    
    CONSTRAINT PK_postaladdress_postaladdresstype PRIMARY KEY (id)
);