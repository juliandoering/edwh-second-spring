ALTER TABLE businessthirdparty ADD COLUMN si_legaladdresscountrycode varchar;

UPDATE businessthirdparty SET si_legaladdresscountrycode =
 (SELECT l.country_code
    FROM  postaladdress pa,
      postaladdresstype pt ,
      location l
    WHERE businessthirdparty.id = pa.businessthirdparty_id
     AND pa.postaladdresstype_id = pt.id
     AND pa.location_id = l.id
     AND pt.code = 'LEGAL');
