-- Mapping Table postaladdress_postaladdresstype
ALTER TABLE postaladdress_postaladdresstype
  ADD CONSTRAINT fk_postaladdress_postaladdresstype_postaladdress FOREIGN KEY (postaladdress_id)
  REFERENCES postaladdress (id);

ALTER TABLE postaladdress_postaladdresstype
  ADD CONSTRAINT fk_postaladdress_postaladdresstype_postaladdresstype FOREIGN KEY (postaladdresstype_id)
  REFERENCES postaladdresstype (id);