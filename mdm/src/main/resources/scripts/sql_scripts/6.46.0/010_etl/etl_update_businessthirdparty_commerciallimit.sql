ALTER TABLE etl.businessthirdparty_commerciallimit ADD COLUMN budget_unit_ID BIGINT;
ALTER TABLE etl.businessthirdparty_commerciallimit ADD COLUMN budget_quantity BIGINT;
ALTER TABLE etl.businessthirdparty_commerciallimit ADD COLUMN budget_valid_from DATE;
ALTER TABLE etl.businessthirdparty_commerciallimit ADD COLUMN budget_valid_to DATE;
