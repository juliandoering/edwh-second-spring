ALTER TABLE mdm.container
DROP CONSTRAINT fk_shipment_advice,
ADD CONSTRAINT fk_shipment_advice FOREIGN KEY (shipment_advice_id)
    REFERENCES mdm.shipment_advice (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;

ALTER TABLE mdm.anyotherinstruction
DROP CONSTRAINT fk_anyotherinstruction_shippinginstruction,
ADD CONSTRAINT fk_anyotherinstruction_shippinginstruction FOREIGN KEY (shippinginstruction_id)
    REFERENCES mdm.shippinginstruction (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;


ALTER TABLE mdm.shipment_advice
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.shippinginstruction
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.additionalinformation
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.amendment
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.clause
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.contract_productcertificationprogram
DROP CONSTRAINT fk_contract_productcertificationprogram_contract,
    ADD CONSTRAINT fk_contract_productcertificationprogram_contract FOREIGN KEY (contract_id)
    REFERENCES mdm.contract (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;

ALTER TABLE mdm.contract_productcertificationprogram
DROP CONSTRAINT fk_contract_productcertificationprogram_productcertificationpro,
    ADD CONSTRAINT fk_contract_productcertificationprogram_productcertificationpro FOREIGN KEY (productcertificationprogram_id)
    REFERENCES mdm.productcertificationprogram (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;

ALTER TABLE mdm.exceptionsandotherconditions
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.markornumber
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.otherapprovalterms
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.pricefixation
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.productdescription
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;

ALTER TABLE mdm.routingsummary
DROP CONSTRAINT fk_routingsummary_contract_id,
    ADD CONSTRAINT fk_routingsummary_contract_id FOREIGN KEY (contract_id)
    REFERENCES mdm.contract (id) MATCH SIMPLE
    ON UPDATE NO ACTION
	ON DELETE CASCADE;

ALTER TABLE mdm.routingsummary
DROP CONSTRAINT fk_routingsummary_location_id,
    ADD CONSTRAINT fk_routingsummary_location_id FOREIGN KEY (location_id)
    REFERENCES mdm.location (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE;

ALTER TABLE mdm.userdefinedpaymentterms
DROP CONSTRAINT fk_contract,
ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id)
REFERENCES mdm.contract (id) MATCH SIMPLE
ON UPDATE NO ACTION
ON DELETE CASCADE;
