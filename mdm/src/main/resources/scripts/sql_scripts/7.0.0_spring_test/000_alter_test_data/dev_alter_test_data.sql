/*Special to dev environment*/
begin;

update contractrole_thirdpartytype set uuid = uuid_generate_v4() where uuid is not null;
update eventcode set uuid = uuid_generate_v4() where uuid is not null and is_version=false and deleted=false;
update exchangerate set uuid = uuid_generate_v4() where uuid is null;
update firstnoticedays set uuid = uuid_generate_v4() where uuid is null;
update freemarkertemplate set uuid = uuid_generate_v4() where id is not null;
update postaladdress set is_version = true where parent != 0;
delete from postaladdress_postaladdresstype; /*good example of why the current ids have a problem or maybe just the dev env data*/
delete from postaladdress where id = 5 or id = 10 or id = 14;
update settlementterminalrate set uuid = uuid_generate_v4() where uuid is not null and is_version=false and deleted=false;

commit;

/*Special to Spring Impl and dev environment*/
begin;

update mdm.individual set gender = upper(gender) where gender is not null;
update mdm.contact set contacttype = upper(contacttype) where contacttype is not null;
update mdm.contact set contacttype = 'TELEPHONE' where contacttype like 'TEL';
update mdm.locationfunction set function_type = replace(function_type, ' ', '_') where function_type is not null;

commit;
