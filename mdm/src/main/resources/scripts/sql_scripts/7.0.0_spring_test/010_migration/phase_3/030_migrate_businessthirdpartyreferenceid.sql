alter table mdm_spring.businessthirdpartyreferenceid
   add constraint fk_supplier_groupcompany
   foreign key (supplier_groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_businessthirdpartyreferenceid_supplier_groupcompany_id cascade;
create index idx_businessthirdpartyreferenceid_supplier_groupcompany_id on mdm_spring.businessthirdpartyreferenceid (supplier_groupcompany_id);
alter table mdm_spring.businessthirdpartyreferenceid
   add constraint fk_businessthirdpartyreferenceidtype
   foreign key (businessthirdpartyreferenceidtype_id)
       references mdm_spring.businessthirdpartyreferenceidtype(id);
drop index if exists idx_businessthirdpartyreferenceid_businessthirdpartyreferenceidtype_id cascade;
create index idx_businessthirdpartyreferenceid_businessthirdpartyreferenceidtype_id on mdm_spring.businessthirdpartyreferenceid (businessthirdpartyreferenceidtype_id);
alter table mdm_spring.businessthirdpartyreferenceid
   add constraint fk_supplier_businessthirdparty
   foreign key (supplier_businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_businessthirdpartyreferenceid_supplier_businessthirdparty_id cascade;
create index idx_businessthirdpartyreferenceid_supplier_businessthirdparty_id on mdm_spring.businessthirdpartyreferenceid (supplier_businessthirdparty_id);
alter table mdm_spring.businessthirdpartyreferenceid
   add constraint fk_parent_businessthirdparty
   foreign key (parent_businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_businessthirdpartyreferenceid_parent_businessthirdparty_id cascade;
create index idx_businessthirdpartyreferenceid_parent_businessthirdparty_id on mdm_spring.businessthirdpartyreferenceid (parent_businessthirdparty_id);
