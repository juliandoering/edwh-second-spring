alter table mdm_spring.businessthirdpartygroupmembership drop column id;
alter table mdm_spring.businessthirdpartygroupmembership rename column uuid to id;
alter table mdm_spring.businessthirdpartygroupmembership add primary key (id);
