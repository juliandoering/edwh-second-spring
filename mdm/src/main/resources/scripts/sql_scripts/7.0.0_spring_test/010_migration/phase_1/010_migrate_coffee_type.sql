create table mdm_spring.coffee_type as table mdm.coffee_type with no data;
insert into mdm_spring.coffee_type select * from mdm.coffee_type where is_version=false and deleted=false;
alter table mdm_spring.coffee_type
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
