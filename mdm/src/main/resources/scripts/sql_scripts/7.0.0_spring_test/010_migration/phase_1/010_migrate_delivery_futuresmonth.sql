create table mdm_spring.delivery_futuresmonth as table mdm.delivery_futuresmonth with no data;
alter table mdm_spring.delivery_futuresmonth alter column futuresmarket_id type varchar;
insert into mdm_spring.delivery_futuresmonth select * from mdm.delivery_futuresmonth;
update mdm_spring.delivery_futuresmonth
set futuresmarket_id = (select ref.uuid from mdm.futuremarket ref where ref.id = futuresmarket_id::bigint)
where futuresmarket_id is not null;
alter table mdm_spring.delivery_futuresmonth
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
