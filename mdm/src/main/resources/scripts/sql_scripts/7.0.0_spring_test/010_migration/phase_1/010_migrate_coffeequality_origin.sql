create table mdm_spring.coffeequality_origin as table mdm.coffeequality_origin with no data;
alter table mdm_spring.coffeequality_origin alter column coffeequality_id type varchar;
alter table mdm_spring.coffeequality_origin alter column location_id type varchar;
insert into mdm_spring.coffeequality_origin select * from mdm.coffeequality_origin;
update mdm_spring.coffeequality_origin
set coffeequality_id = (select ref.uuid from mdm.coffeequality ref where ref.id = coffeequality_id::bigint)
where coffeequality_id is not null;
update mdm_spring.coffeequality_origin
set location_id = (select ref.uuid from mdm.location ref where ref.id = location_id::bigint)
where location_id is not null;
alter table mdm_spring.coffeequality_origin
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
