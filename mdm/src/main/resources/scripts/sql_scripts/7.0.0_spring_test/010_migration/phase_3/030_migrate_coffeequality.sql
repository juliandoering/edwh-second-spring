alter table mdm_spring.coffeequality
   add constraint fk_coffee_variety
   foreign key (coffee_variety_id)
       references mdm_spring.coffee_variety(id);
drop index if exists idx_coffeequality_coffee_variety_id cascade;
create index idx_coffeequality_coffee_variety_id on mdm_spring.coffeequality (coffee_variety_id);
alter table mdm_spring.coffeequality
   add constraint fk_coffee_type
   foreign key (coffee_type_id)
       references mdm_spring.coffee_type(id);
drop index if exists idx_coffeequality_coffee_type_id cascade;
create index idx_coffeequality_coffee_type_id on mdm_spring.coffeequality (coffee_type_id);
alter table mdm_spring.coffeequality
   add constraint fk_coffeequalityorigintype
   foreign key (coffeequalityorigintype_id)
       references mdm_spring.coffeequalityorigintype(id);
drop index if exists idx_coffeequality_coffeequalityorigintype_id cascade;
create index idx_coffeequality_coffeequalityorigintype_id on mdm_spring.coffeequality (coffeequalityorigintype_id);
