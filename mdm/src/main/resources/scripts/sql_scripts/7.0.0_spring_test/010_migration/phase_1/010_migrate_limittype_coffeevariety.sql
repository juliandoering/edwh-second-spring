create table mdm_spring.limittype_coffeevariety as table mdm.limittype_coffeevariety with no data;
alter table mdm_spring.limittype_coffeevariety alter column limittype_id type varchar;
alter table mdm_spring.limittype_coffeevariety alter column coffeevariety_id type varchar;
insert into mdm_spring.limittype_coffeevariety select * from mdm.limittype_coffeevariety;
update mdm_spring.limittype_coffeevariety
set limittype_id = (select ref.uuid from mdm.limittype ref where ref.id = limittype_id::bigint)
where limittype_id is not null;
update mdm_spring.limittype_coffeevariety
set coffeevariety_id = (select ref.uuid from mdm.coffee_variety ref where ref.id = coffeevariety_id::bigint)
where coffeevariety_id is not null;
alter table mdm_spring.limittype_coffeevariety
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
