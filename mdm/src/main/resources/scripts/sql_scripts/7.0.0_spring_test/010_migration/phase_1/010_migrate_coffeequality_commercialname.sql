create table mdm_spring.coffeequality_commercialname as table mdm.coffeequality_commercialname with no data;
alter table mdm_spring.coffeequality_commercialname alter column coffeequality_id type varchar;
alter table mdm_spring.coffeequality_commercialname alter column groupcompany_id type varchar;
insert into mdm_spring.coffeequality_commercialname select * from mdm.coffeequality_commercialname where is_version=false and deleted=false;
update mdm_spring.coffeequality_commercialname
set coffeequality_id = (select ref.uuid from mdm.coffeequality ref where ref.id = coffeequality_id::bigint)
where coffeequality_id is not null;
update mdm_spring.coffeequality_commercialname
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
alter table mdm_spring.coffeequality_commercialname
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
