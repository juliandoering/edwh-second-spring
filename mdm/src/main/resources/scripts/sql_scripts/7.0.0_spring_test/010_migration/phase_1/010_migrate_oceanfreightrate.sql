create table mdm_spring.oceanfreightrate as table mdm.oceanfreightrate with no data;
alter table mdm_spring.oceanfreightrate alter column containermovement_id type varchar;
alter table mdm_spring.oceanfreightrate alter column containersize_id type varchar;
alter table mdm_spring.oceanfreightrate alter column inlandcostcurrency_id type varchar;
alter table mdm_spring.oceanfreightrate alter column oceanfreightcurrency_id type varchar;
alter table mdm_spring.oceanfreightrate alter column carriertraderoute_id type varchar;
alter table mdm_spring.oceanfreightrate alter column place_of_delivery_id type varchar;
alter table mdm_spring.oceanfreightrate alter column thccurrency_id type varchar;
alter table mdm_spring.oceanfreightrate alter column place_of_receipt_id type varchar;
insert into mdm_spring.oceanfreightrate select * from mdm.oceanfreightrate where is_version=false and deleted=false;
update mdm_spring.oceanfreightrate
set containermovement_id = (select ref.uuid from mdm.containermovement ref where ref.id = containermovement_id::bigint)
where containermovement_id is not null;
update mdm_spring.oceanfreightrate
set containersize_id = (select ref.uuid from mdm.containersize ref where ref.id = containersize_id::bigint)
where containersize_id is not null;
update mdm_spring.oceanfreightrate
set inlandcostcurrency_id = (select ref.uuid from mdm.currency ref where ref.id = inlandcostcurrency_id::bigint)
where inlandcostcurrency_id is not null;
update mdm_spring.oceanfreightrate
set oceanfreightcurrency_id = (select ref.uuid from mdm.currency ref where ref.id = oceanfreightcurrency_id::bigint)
where oceanfreightcurrency_id is not null;
update mdm_spring.oceanfreightrate
set carriertraderoute_id = (select ref.uuid from mdm.carriertraderoute ref where ref.id = carriertraderoute_id::bigint)
where carriertraderoute_id is not null;
update mdm_spring.oceanfreightrate
set place_of_delivery_id = (select ref.uuid from mdm.location ref where ref.id = place_of_delivery_id::bigint)
where place_of_delivery_id is not null;
update mdm_spring.oceanfreightrate
set thccurrency_id = (select ref.uuid from mdm.currency ref where ref.id = thccurrency_id::bigint)
where thccurrency_id is not null;
update mdm_spring.oceanfreightrate
set place_of_receipt_id = (select ref.uuid from mdm.location ref where ref.id = place_of_receipt_id::bigint)
where place_of_receipt_id is not null;
alter table mdm_spring.oceanfreightrate
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
