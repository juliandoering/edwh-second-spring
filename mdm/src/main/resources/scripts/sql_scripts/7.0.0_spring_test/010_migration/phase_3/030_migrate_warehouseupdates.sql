alter table mdm_spring.warehouseupdates
   add constraint fk_warehouse
   foreign key (warehouse_id)
       references mdm_spring.warehouse(id);
drop index if exists idx_warehouseupdates_warehouse_id cascade;
create index idx_warehouseupdates_warehouse_id on mdm_spring.warehouseupdates (warehouse_id);
