alter table mdm_spring.businessthirdparty_commerciallimit drop column id;
alter table mdm_spring.businessthirdparty_commerciallimit rename column uuid to id;
alter table mdm_spring.businessthirdparty_commerciallimit add primary key (id);
