create table mdm_spring.coffee_variety as table mdm.coffee_variety with no data;
insert into mdm_spring.coffee_variety select * from mdm.coffee_variety where is_version=false and deleted=false;
alter table mdm_spring.coffee_variety
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
