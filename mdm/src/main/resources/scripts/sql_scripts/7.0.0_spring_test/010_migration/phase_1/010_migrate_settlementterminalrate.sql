create table mdm_spring.settlementterminalrate as table mdm.settlementterminalrate with no data;
alter table mdm_spring.settlementterminalrate alter column futuremonth_id type varchar;
alter table mdm_spring.settlementterminalrate alter column futuremarket_id type varchar;
insert into mdm_spring.settlementterminalrate select * from mdm.settlementterminalrate where is_version=false and deleted=false;
update mdm_spring.settlementterminalrate
set futuremonth_id = (select ref.uuid from mdm.futuremonth ref where ref.id = futuremonth_id::bigint)
where futuremonth_id is not null;
update mdm_spring.settlementterminalrate
set futuremarket_id = (select ref.uuid from mdm.futuremarket ref where ref.id = futuremarket_id::bigint)
where futuremarket_id is not null;
alter table mdm_spring.settlementterminalrate
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
