create table mdm_spring.contract as table mdm.contract with no data;
alter table mdm_spring.contract alter column samplehandling_id type varchar;
alter table mdm_spring.contract alter column agent_id type varchar;
alter table mdm_spring.contract alter column pricefixationtype_id type varchar;
alter table mdm_spring.contract alter column seller_third_party_id type varchar;
alter table mdm_spring.contract alter column quantityunit_id type varchar;
alter table mdm_spring.contract alter column incoterm_id type varchar;
alter table mdm_spring.contract alter column shipper_third_party_id type varchar;
alter table mdm_spring.contract alter column futuremonth_id type varchar;
alter table mdm_spring.contract alter column price_quantityunit_id type varchar;
alter table mdm_spring.contract alter column coffeecontracttype_id type varchar;
alter table mdm_spring.contract alter column buyer_third_party_id type varchar;
alter table mdm_spring.contract alter column buyer_id type varchar;
alter table mdm_spring.contract alter column coffeeweightscode_id type varchar;
alter table mdm_spring.contract alter column agent_third_party_id type varchar;
alter table mdm_spring.contract alter column positionofsale_id type varchar;
alter table mdm_spring.contract alter column packing_id type varchar;
alter table mdm_spring.contract alter column futuremarket_id type varchar;
alter table mdm_spring.contract alter column shipper_id type varchar;
alter table mdm_spring.contract alter column coffeequality_id type varchar;
alter table mdm_spring.contract alter column instr_info_location_id type varchar;
alter table mdm_spring.contract alter column paymentterm_id type varchar;
alter table mdm_spring.contract alter column fixationoption_id type varchar;
alter table mdm_spring.contract alter column currency_id type varchar;
alter table mdm_spring.contract alter column seller_id type varchar;
insert into mdm_spring.contract select * from mdm.contract where is_version=false and deleted=false;
update mdm_spring.contract
set samplehandling_id = (select ref.uuid from mdm.samplehandling ref where ref.id = samplehandling_id::bigint)
where samplehandling_id is not null;
update mdm_spring.contract
set agent_id = (select ref.uuid from mdm.groupcompany ref where ref.id = agent_id::bigint)
where agent_id is not null;
update mdm_spring.contract
set pricefixationtype_id = (select ref.uuid from mdm.pricefixationtype ref where ref.id = pricefixationtype_id::bigint)
where pricefixationtype_id is not null;
update mdm_spring.contract
set seller_third_party_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = seller_third_party_id::bigint)
where seller_third_party_id is not null;
update mdm_spring.contract
set quantityunit_id = (select ref.uuid from mdm.quantityunit ref where ref.id = quantityunit_id::bigint)
where quantityunit_id is not null;
update mdm_spring.contract
set incoterm_id = (select ref.uuid from mdm.incoterm ref where ref.id = incoterm_id::bigint)
where incoterm_id is not null;
update mdm_spring.contract
set shipper_third_party_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = shipper_third_party_id::bigint)
where shipper_third_party_id is not null;
update mdm_spring.contract
set futuremonth_id = (select ref.uuid from mdm.futuremonth ref where ref.id = futuremonth_id::bigint)
where futuremonth_id is not null;
update mdm_spring.contract
set price_quantityunit_id = (select ref.uuid from mdm.quantityunit ref where ref.id = price_quantityunit_id::bigint)
where price_quantityunit_id is not null;
update mdm_spring.contract
set coffeecontracttype_id = (select ref.uuid from mdm.coffeecontracttype ref where ref.id = coffeecontracttype_id::bigint)
where coffeecontracttype_id is not null;
update mdm_spring.contract
set buyer_third_party_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = buyer_third_party_id::bigint)
where buyer_third_party_id is not null;
update mdm_spring.contract
set buyer_id = (select ref.uuid from mdm.groupcompany ref where ref.id = buyer_id::bigint)
where buyer_id is not null;
update mdm_spring.contract
set coffeeweightscode_id = (select ref.uuid from mdm.coffeeweightscode ref where ref.id = coffeeweightscode_id::bigint)
where coffeeweightscode_id is not null;
update mdm_spring.contract
set agent_third_party_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = agent_third_party_id::bigint)
where agent_third_party_id is not null;
update mdm_spring.contract
set positionofsale_id = (select ref.uuid from mdm.positionofsale ref where ref.id = positionofsale_id::bigint)
where positionofsale_id is not null;
update mdm_spring.contract
set packing_id = (select ref.uuid from mdm.packing ref where ref.id = packing_id::bigint)
where packing_id is not null;
update mdm_spring.contract
set futuremarket_id = (select ref.uuid from mdm.futuremarket ref where ref.id = futuremarket_id::bigint)
where futuremarket_id is not null;
update mdm_spring.contract
set shipper_id = (select ref.uuid from mdm.groupcompany ref where ref.id = shipper_id::bigint)
where shipper_id is not null;
update mdm_spring.contract
set coffeequality_id = (select ref.uuid from mdm.coffeequality ref where ref.id = coffeequality_id::bigint)
where coffeequality_id is not null;
update mdm_spring.contract
set instr_info_location_id = (select ref.uuid from mdm.location ref where ref.id = instr_info_location_id::bigint)
where instr_info_location_id is not null;
update mdm_spring.contract
set paymentterm_id = (select ref.uuid from mdm.paymentterm ref where ref.id = paymentterm_id::bigint)
where paymentterm_id is not null;
update mdm_spring.contract
set fixationoption_id = (select ref.uuid from mdm.fixationoption ref where ref.id = fixationoption_id::bigint)
where fixationoption_id is not null;
update mdm_spring.contract
set currency_id = (select ref.uuid from mdm.currency ref where ref.id = currency_id::bigint)
where currency_id is not null;
update mdm_spring.contract
set seller_id = (select ref.uuid from mdm.groupcompany ref where ref.id = seller_id::bigint)
where seller_id is not null;
alter table mdm_spring.contract
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
