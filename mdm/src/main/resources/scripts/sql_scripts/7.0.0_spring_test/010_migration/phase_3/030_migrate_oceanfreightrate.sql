alter table mdm_spring.oceanfreightrate
   add constraint fk_containermovement
   foreign key (containermovement_id)
       references mdm_spring.containermovement(id);
drop index if exists idx_oceanfreightrate_containermovement_id cascade;
create index idx_oceanfreightrate_containermovement_id on mdm_spring.oceanfreightrate (containermovement_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_containersize
   foreign key (containersize_id)
       references mdm_spring.containersize(id);
drop index if exists idx_oceanfreightrate_containersize_id cascade;
create index idx_oceanfreightrate_containersize_id on mdm_spring.oceanfreightrate (containersize_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_inlandcostcurrency
   foreign key (inlandcostcurrency_id)
       references mdm_spring.currency(id);
drop index if exists idx_oceanfreightrate_inlandcostcurrency_id cascade;
create index idx_oceanfreightrate_inlandcostcurrency_id on mdm_spring.oceanfreightrate (inlandcostcurrency_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_oceanfreightcurrency
   foreign key (oceanfreightcurrency_id)
       references mdm_spring.currency(id);
drop index if exists idx_oceanfreightrate_oceanfreightcurrency_id cascade;
create index idx_oceanfreightrate_oceanfreightcurrency_id on mdm_spring.oceanfreightrate (oceanfreightcurrency_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_carriertraderoute
   foreign key (carriertraderoute_id)
       references mdm_spring.carriertraderoute(id);
drop index if exists idx_oceanfreightrate_carriertraderoute_id cascade;
create index idx_oceanfreightrate_carriertraderoute_id on mdm_spring.oceanfreightrate (carriertraderoute_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_place_of_delivery
   foreign key (place_of_delivery_id)
       references mdm_spring.location(id);
drop index if exists idx_oceanfreightrate_place_of_delivery_id cascade;
create index idx_oceanfreightrate_place_of_delivery_id on mdm_spring.oceanfreightrate (place_of_delivery_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_thccurrency
   foreign key (thccurrency_id)
       references mdm_spring.currency(id);
drop index if exists idx_oceanfreightrate_thccurrency_id cascade;
create index idx_oceanfreightrate_thccurrency_id on mdm_spring.oceanfreightrate (thccurrency_id);
alter table mdm_spring.oceanfreightrate
   add constraint fk_place_of_receipt
   foreign key (place_of_receipt_id)
       references mdm_spring.location(id);
drop index if exists idx_oceanfreightrate_place_of_receipt_id cascade;
create index idx_oceanfreightrate_place_of_receipt_id on mdm_spring.oceanfreightrate (place_of_receipt_id);
