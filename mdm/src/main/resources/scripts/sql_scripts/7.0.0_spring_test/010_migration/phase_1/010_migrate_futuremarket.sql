create table mdm_spring.futuremarket as table mdm.futuremarket with no data;
alter table mdm_spring.futuremarket alter column lot_quantityunit_id type varchar;
insert into mdm_spring.futuremarket select * from mdm.futuremarket where is_version=false and deleted=false;
update mdm_spring.futuremarket
set lot_quantityunit_id = (select ref.uuid from mdm.quantityunit ref where ref.id = lot_quantityunit_id::bigint)
where lot_quantityunit_id is not null;
alter table mdm_spring.futuremarket
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
