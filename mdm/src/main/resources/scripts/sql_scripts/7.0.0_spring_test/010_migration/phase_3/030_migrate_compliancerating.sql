alter table mdm_spring.compliancerating
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_compliancerating_businessthirdparty_id cascade;
create index idx_compliancerating_businessthirdparty_id on mdm_spring.compliancerating (businessthirdparty_id);
alter table mdm_spring.compliancerating
   add constraint fk_complianceratingtype
   foreign key (complianceratingtype_id)
       references mdm_spring.compliance_rating_type(id);
drop index if exists idx_compliancerating_complianceratingtype_id cascade;
create index idx_compliancerating_complianceratingtype_id on mdm_spring.compliancerating (complianceratingtype_id);
