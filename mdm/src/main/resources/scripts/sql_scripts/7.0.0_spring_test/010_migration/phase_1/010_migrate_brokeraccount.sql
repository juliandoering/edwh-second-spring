create table mdm_spring.brokeraccount as table mdm.brokeraccount with no data;
alter table mdm_spring.brokeraccount alter column groupcompany_id type varchar;
insert into mdm_spring.brokeraccount select * from mdm.brokeraccount where is_version=false and deleted=false;
update mdm_spring.brokeraccount
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
alter table mdm_spring.brokeraccount
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
