create table mdm_spring.positionofsale as table mdm.positionofsale with no data;
insert into mdm_spring.positionofsale select * from mdm.positionofsale where is_version=false and deleted=false;
alter table mdm_spring.positionofsale
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
