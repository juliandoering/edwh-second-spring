create table mdm_spring.freemarkertemplate as table mdm.freemarkertemplate with no data;
insert into mdm_spring.freemarkertemplate select * from mdm.freemarkertemplate;
alter table mdm_spring.freemarkertemplate
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
