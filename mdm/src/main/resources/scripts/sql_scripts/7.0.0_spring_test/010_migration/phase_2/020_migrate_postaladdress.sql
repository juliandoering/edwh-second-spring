alter table mdm_spring.postaladdress drop column id;
alter table mdm_spring.postaladdress rename column uuid to id;
alter table mdm_spring.postaladdress add primary key (id);
