alter table mdm_spring.groupcompany
   add constraint fk_businessfunction
   foreign key (businessfunction_id)
       references mdm_spring.businessfunction(id);
drop index if exists idx_groupcompany_businessfunction_id cascade;
create index idx_groupcompany_businessfunction_id on mdm_spring.groupcompany (businessfunction_id);
alter table mdm_spring.groupcompany
   add constraint fk_postal_address
   foreign key (postal_address_id)
       references mdm_spring.postaladdress(id);
drop index if exists idx_groupcompany_postal_address_id cascade;
create index idx_groupcompany_postal_address_id on mdm_spring.groupcompany (postal_address_id);
