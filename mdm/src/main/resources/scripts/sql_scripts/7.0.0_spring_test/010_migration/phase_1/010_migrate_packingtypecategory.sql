create table mdm_spring.packingtypecategory as table mdm.packingtypecategory with no data;
insert into mdm_spring.packingtypecategory select * from mdm.packingtypecategory where is_version=false and deleted=false;
alter table mdm_spring.packingtypecategory
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
