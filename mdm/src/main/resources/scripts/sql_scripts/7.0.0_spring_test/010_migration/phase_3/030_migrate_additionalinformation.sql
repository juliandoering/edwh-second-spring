alter table mdm_spring.additionalinformation
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_additionalinformation_contract_id cascade;
create index idx_additionalinformation_contract_id on mdm_spring.additionalinformation (contract_id);
