alter table mdm_spring.locationfunction
   add constraint fk_location
   foreign key (location_id)
       references mdm_spring.location(id);
drop index if exists idx_locationfunction_location_id cascade;
create index idx_locationfunction_location_id on mdm_spring.locationfunction (location_id);
