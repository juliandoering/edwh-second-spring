alter table mdm_spring.maximumopenposition
   add constraint fk_businessthirdparty_commerciallimit
   foreign key (businessthirdparty_commerciallimit_id)
       references mdm_spring.businessthirdparty_commerciallimit(id);
drop index if exists idx_maximumopenposition_businessthirdparty_commerciallimit_id cascade;
create index idx_maximumopenposition_businessthirdparty_commerciallimit_id on mdm_spring.maximumopenposition (businessthirdparty_commerciallimit_id);
alter table mdm_spring.maximumopenposition
   add constraint fk_quantityunit
   foreign key (quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_maximumopenposition_quantityunit_id cascade;
create index idx_maximumopenposition_quantityunit_id on mdm_spring.maximumopenposition (quantityunit_id);
