create table mdm_spring.identificationtype as table mdm.identificationtype with no data;
insert into mdm_spring.identificationtype select * from mdm.identificationtype where is_version=false and deleted=false;
alter table mdm_spring.identificationtype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
