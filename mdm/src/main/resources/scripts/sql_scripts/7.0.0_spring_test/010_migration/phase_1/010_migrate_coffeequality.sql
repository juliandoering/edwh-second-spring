create table mdm_spring.coffeequality as table mdm.coffeequality with no data;
alter table mdm_spring.coffeequality alter column coffee_variety_id type varchar;
alter table mdm_spring.coffeequality alter column coffee_type_id type varchar;
alter table mdm_spring.coffeequality alter column coffeequalityorigintype_id type varchar;
insert into mdm_spring.coffeequality select * from mdm.coffeequality where is_version=false and deleted=false;
update mdm_spring.coffeequality
set coffee_variety_id = (select ref.uuid from mdm.coffee_variety ref where ref.id = coffee_variety_id::bigint)
where coffee_variety_id is not null;
update mdm_spring.coffeequality
set coffee_type_id = (select ref.uuid from mdm.coffee_type ref where ref.id = coffee_type_id::bigint)
where coffee_type_id is not null;
update mdm_spring.coffeequality
set coffeequalityorigintype_id = (select ref.uuid from mdm.coffeequalityorigintype ref where ref.id = coffeequalityorigintype_id::bigint)
where coffeequalityorigintype_id is not null;
alter table mdm_spring.coffeequality
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
