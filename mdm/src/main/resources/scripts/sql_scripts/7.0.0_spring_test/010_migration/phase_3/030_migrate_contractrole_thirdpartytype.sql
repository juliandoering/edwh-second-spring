alter table mdm_spring.contractrole_thirdpartytype
   add constraint fk_thirdpartytype
   foreign key (thirdpartytype_id)
       references mdm_spring.thirdpartytype(id);
drop index if exists idx_contractrole_thirdpartytype_thirdpartytype_id cascade;
create index idx_contractrole_thirdpartytype_thirdpartytype_id on mdm_spring.contractrole_thirdpartytype (thirdpartytype_id);
