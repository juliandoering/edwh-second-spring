create table mdm_spring.businessfunction as table mdm.businessfunction with no data;
insert into mdm_spring.businessfunction select * from mdm.businessfunction where is_version=false and deleted=false;
alter table mdm_spring.businessfunction
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
