create table mdm_spring.maximumopenposition as table mdm.maximumopenposition with no data;
alter table mdm_spring.maximumopenposition alter column businessthirdparty_commerciallimit_id type varchar;
alter table mdm_spring.maximumopenposition alter column quantityunit_id type varchar;
insert into mdm_spring.maximumopenposition select * from mdm.maximumopenposition where is_version=false and deleted=false;
update mdm_spring.maximumopenposition
set businessthirdparty_commerciallimit_id = (select ref.uuid from mdm.businessthirdparty_commerciallimit ref where ref.id = businessthirdparty_commerciallimit_id::bigint)
where businessthirdparty_commerciallimit_id is not null;
update mdm_spring.maximumopenposition
set quantityunit_id = (select ref.uuid from mdm.quantityunit ref where ref.id = quantityunit_id::bigint)
where quantityunit_id is not null;
alter table mdm_spring.maximumopenposition
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
