create table mdm_spring.coffeeweightscode as table mdm.coffeeweightscode with no data;
insert into mdm_spring.coffeeweightscode select * from mdm.coffeeweightscode where is_version=false and deleted=false;
alter table mdm_spring.coffeeweightscode
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
