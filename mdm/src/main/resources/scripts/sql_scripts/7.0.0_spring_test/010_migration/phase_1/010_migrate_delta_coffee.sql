create table mdm_spring.delta_coffee as table mdm.delta_coffee with no data;
alter table mdm_spring.delta_coffee alter column delta_coffee_type_id type varchar;
alter table mdm_spring.delta_coffee alter column coffeequality_id type varchar;
alter table mdm_spring.delta_coffee alter column groupcompany_id type varchar;
insert into mdm_spring.delta_coffee select * from mdm.delta_coffee where is_version=false and deleted=false;
update mdm_spring.delta_coffee
set delta_coffee_type_id = (select ref.uuid from mdm.delta_coffee_type ref where ref.id = delta_coffee_type_id::bigint)
where delta_coffee_type_id is not null;
update mdm_spring.delta_coffee
set coffeequality_id = (select ref.uuid from mdm.coffeequality ref where ref.id = coffeequality_id::bigint)
where coffeequality_id is not null;
update mdm_spring.delta_coffee
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
alter table mdm_spring.delta_coffee
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
