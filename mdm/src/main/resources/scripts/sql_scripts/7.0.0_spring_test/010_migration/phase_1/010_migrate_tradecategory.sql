create table mdm_spring.tradecategory as table mdm.tradecategory with no data;
insert into mdm_spring.tradecategory select * from mdm.tradecategory where is_version=false and deleted=false;
alter table mdm_spring.tradecategory
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
