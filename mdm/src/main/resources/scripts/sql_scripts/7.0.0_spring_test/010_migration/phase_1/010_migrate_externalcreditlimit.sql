create table mdm_spring.externalcreditlimit as table mdm.externalcreditlimit with no data;
alter table mdm_spring.externalcreditlimit alter column businessthirdparty_id type varchar;
alter table mdm_spring.externalcreditlimit alter column insurer_id type varchar;
alter table mdm_spring.externalcreditlimit alter column currency_id type varchar;
insert into mdm_spring.externalcreditlimit select * from mdm.externalcreditlimit where is_version=false and deleted=false;
update mdm_spring.externalcreditlimit
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.externalcreditlimit
set insurer_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = insurer_id::bigint)
where insurer_id is not null;
update mdm_spring.externalcreditlimit
set currency_id = (select ref.uuid from mdm.currency ref where ref.id = currency_id::bigint)
where currency_id is not null;
alter table mdm_spring.externalcreditlimit
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
