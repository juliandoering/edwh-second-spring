create table mdm_spring.containersize as table mdm.containersize with no data;
insert into mdm_spring.containersize select * from mdm.containersize where is_version=false and deleted=false;
alter table mdm_spring.containersize
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
