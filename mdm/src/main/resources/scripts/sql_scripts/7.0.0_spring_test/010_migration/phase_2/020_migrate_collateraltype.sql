alter table mdm_spring.collateraltype drop column id;
alter table mdm_spring.collateraltype rename column uuid to id;
alter table mdm_spring.collateraltype add primary key (id);
