create table mdm_spring.countryqualifier as table mdm.countryqualifier with no data;
insert into mdm_spring.countryqualifier select * from mdm.countryqualifier where is_version=false and deleted=false;
alter table mdm_spring.countryqualifier
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
