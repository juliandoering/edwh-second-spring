create table mdm_spring.freightpaymentterm as table mdm.freightpaymentterm with no data;
insert into mdm_spring.freightpaymentterm select * from mdm.freightpaymentterm where is_version=false and deleted=false;
alter table mdm_spring.freightpaymentterm
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
