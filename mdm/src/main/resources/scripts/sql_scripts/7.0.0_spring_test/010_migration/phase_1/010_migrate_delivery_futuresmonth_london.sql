create table mdm_spring.delivery_futuresmonth_london as table mdm.delivery_futuresmonth_london with no data;
insert into mdm_spring.delivery_futuresmonth_london select * from mdm.delivery_futuresmonth_london;
alter table mdm_spring.delivery_futuresmonth_london
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
