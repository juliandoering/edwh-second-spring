alter table mdm_spring.shippinginstruction drop column id;
alter table mdm_spring.shippinginstruction rename column uuid to id;
alter table mdm_spring.shippinginstruction add primary key (id);
