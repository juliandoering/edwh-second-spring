alter table mdm_spring.shippinginstruction
   add constraint fk_shippingline
   foreign key (shippingline_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_shippinginstruction_shippingline_id cascade;
create index idx_shippinginstruction_shippingline_id on mdm_spring.shippinginstruction (shippingline_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_typeofshipment
   foreign key (typeofshipment_id)
       references mdm_spring.typeofshipment(id);
drop index if exists idx_shippinginstruction_typeofshipment_id cascade;
create index idx_shippinginstruction_typeofshipment_id on mdm_spring.shippinginstruction (typeofshipment_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_modeoftransport
   foreign key (modeoftransport_id)
       references mdm_spring.modeoftransport(id);
drop index if exists idx_shippinginstruction_modeoftransport_id cascade;
create index idx_shippinginstruction_modeoftransport_id on mdm_spring.shippinginstruction (modeoftransport_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_placeofreceipt
   foreign key (placeofreceipt_id)
       references mdm_spring.location(id);
drop index if exists idx_shippinginstruction_placeofreceipt_id cascade;
create index idx_shippinginstruction_placeofreceipt_id on mdm_spring.shippinginstruction (placeofreceipt_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_quantityunit
   foreign key (quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_shippinginstruction_quantityunit_id cascade;
create index idx_shippinginstruction_quantityunit_id on mdm_spring.shippinginstruction (quantityunit_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_shippinginstruction_contract_id cascade;
create index idx_shippinginstruction_contract_id on mdm_spring.shippinginstruction (contract_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_freightpaymentterm
   foreign key (freightpaymentterm_id)
       references mdm_spring.freightpaymentterm(id);
drop index if exists idx_shippinginstruction_freightpaymentterm_id cascade;
create index idx_shippinginstruction_freightpaymentterm_id on mdm_spring.shippinginstruction (freightpaymentterm_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_portofloading
   foreign key (portofloading_id)
       references mdm_spring.location(id);
drop index if exists idx_shippinginstruction_portofloading_id cascade;
create index idx_shippinginstruction_portofloading_id on mdm_spring.shippinginstruction (portofloading_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_portofdischarge
   foreign key (portofdischarge_id)
       references mdm_spring.location(id);
drop index if exists idx_shippinginstruction_portofdischarge_id cascade;
create index idx_shippinginstruction_portofdischarge_id on mdm_spring.shippinginstruction (portofdischarge_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_containermovement
   foreign key (containermovement_id)
       references mdm_spring.containermovement(id);
drop index if exists idx_shippinginstruction_containermovement_id cascade;
create index idx_shippinginstruction_containermovement_id on mdm_spring.shippinginstruction (containermovement_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_containersize
   foreign key (containersize_id)
       references mdm_spring.containersize(id);
drop index if exists idx_shippinginstruction_containersize_id cascade;
create index idx_shippinginstruction_containersize_id on mdm_spring.shippinginstruction (containersize_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_freightpayableby_groupcompany
   foreign key (freightpayableby_groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_shippinginstruction_freightpayableby_groupcompany_id cascade;
create index idx_shippinginstruction_freightpayableby_groupcompany_id on mdm_spring.shippinginstruction (freightpayableby_groupcompany_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_freightpayableby_thirdparty
   foreign key (freightpayableby_thirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_shippinginstruction_freightpayableby_thirdparty_id cascade;
create index idx_shippinginstruction_freightpayableby_thirdparty_id on mdm_spring.shippinginstruction (freightpayableby_thirdparty_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_packing
   foreign key (packing_id)
       references mdm_spring.packing(id);
drop index if exists idx_shippinginstruction_packing_id cascade;
create index idx_shippinginstruction_packing_id on mdm_spring.shippinginstruction (packing_id);
alter table mdm_spring.shippinginstruction
   add constraint fk_placeofdelivery
   foreign key (placeofdelivery_id)
       references mdm_spring.location(id);
drop index if exists idx_shippinginstruction_placeofdelivery_id cascade;
create index idx_shippinginstruction_placeofdelivery_id on mdm_spring.shippinginstruction (placeofdelivery_id);
