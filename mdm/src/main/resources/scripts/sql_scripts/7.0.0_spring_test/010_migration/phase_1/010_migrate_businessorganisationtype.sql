create table mdm_spring.businessorganisationtype as table mdm.businessorganisationtype with no data;
insert into mdm_spring.businessorganisationtype select * from mdm.businessorganisationtype where is_version=false and deleted=false;
alter table mdm_spring.businessorganisationtype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
