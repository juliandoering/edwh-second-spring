alter table mdm_spring.contact
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_contact_businessthirdparty_id cascade;
create index idx_contact_businessthirdparty_id on mdm_spring.contact (businessthirdparty_id);
alter table mdm_spring.contact
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_contact_groupcompany_id cascade;
create index idx_contact_groupcompany_id on mdm_spring.contact (groupcompany_id);
alter table mdm_spring.contact
   add constraint fk_individual
   foreign key (individual_id)
       references mdm_spring.individual(id);
drop index if exists idx_contact_individual_id cascade;
create index idx_contact_individual_id on mdm_spring.contact (individual_id);
alter table mdm_spring.contact
   add constraint fk_warehouse
   foreign key (warehouse_id)
       references mdm_spring.warehouse(id);
drop index if exists idx_contact_warehouse_id cascade;
create index idx_contact_warehouse_id on mdm_spring.contact (warehouse_id);
