create table mdm_spring.commercialid as table mdm.commercialid with no data;
alter table mdm_spring.commercialid alter column businessthirdparty_id type varchar;
insert into mdm_spring.commercialid select * from mdm.commercialid where is_version=false and deleted=false;
update mdm_spring.commercialid
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
alter table mdm_spring.commercialid
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
