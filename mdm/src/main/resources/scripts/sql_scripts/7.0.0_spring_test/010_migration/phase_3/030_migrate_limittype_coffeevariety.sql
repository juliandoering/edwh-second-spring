drop sequence if exists mdm_spring.limittype_coffeevariety_id_seq cascade;
create sequence mdm_spring.limittype_coffeevariety_id_seq owned by mdm_spring.limittype_coffeevariety.id;
select setval('mdm_spring.limittype_coffeevariety_id_seq', 0 + 1, false) FROM mdm_spring.limittype_coffeevariety;
alter table mdm_spring.limittype_coffeevariety
   alter column id set default nextval('mdm_spring.limittype_coffeevariety_id_seq'),
   alter column id set not null;
alter table mdm_spring.limittype_coffeevariety
   add constraint fk_limittype
   foreign key (limittype_id)
       references mdm_spring.limittype(id);
drop index if exists idx_limittype_coffeevariety_limittype_id cascade;
create index idx_limittype_coffeevariety_limittype_id on mdm_spring.limittype_coffeevariety (limittype_id);
alter table mdm_spring.limittype_coffeevariety
   add constraint fk_coffeevariety
   foreign key (coffeevariety_id)
       references mdm_spring.coffee_variety(id);
drop index if exists idx_limittype_coffeevariety_coffeevariety_id cascade;
create index idx_limittype_coffeevariety_coffeevariety_id on mdm_spring.limittype_coffeevariety (coffeevariety_id);
