create table mdm_spring.interstandardcert as table mdm.interstandardcert with no data;
alter table mdm_spring.interstandardcert alter column businessthirdparty_id type varchar;
alter table mdm_spring.interstandardcert alter column interstandardcertprogram_id type varchar;
insert into mdm_spring.interstandardcert select * from mdm.interstandardcert where is_version=false and deleted=false;
update mdm_spring.interstandardcert
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.interstandardcert
set interstandardcertprogram_id = (select ref.uuid from mdm.interstandardcertprogram ref where ref.id = interstandardcertprogram_id::bigint)
where interstandardcertprogram_id is not null;
alter table mdm_spring.interstandardcert
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
