create table mdm_spring.coffeecontracttype as table mdm.coffeecontracttype with no data;
insert into mdm_spring.coffeecontracttype select * from mdm.coffeecontracttype where is_version=false and deleted=false;
alter table mdm_spring.coffeecontracttype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
