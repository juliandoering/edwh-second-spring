create table mdm_spring.warehouse as table mdm.warehouse with no data;
alter table mdm_spring.warehouse alter column limit_currency_id type varchar;
alter table mdm_spring.warehouse alter column owner_businessthirdparty_id type varchar;
alter table mdm_spring.warehouse alter column owner_groupcompany_id type varchar;
alter table mdm_spring.warehouse alter column postaladdress_id type varchar;
insert into mdm_spring.warehouse select * from mdm.warehouse where is_version=false and deleted=false;
update mdm_spring.warehouse
set limit_currency_id = (select ref.uuid from mdm.currency ref where ref.id = limit_currency_id::bigint)
where limit_currency_id is not null;
update mdm_spring.warehouse
set owner_businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = owner_businessthirdparty_id::bigint)
where owner_businessthirdparty_id is not null;
update mdm_spring.warehouse
set owner_groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = owner_groupcompany_id::bigint)
where owner_groupcompany_id is not null;
update mdm_spring.warehouse
set postaladdress_id = (select ref.uuid from mdm.postaladdress ref where ref.id = postaladdress_id::bigint)
where postaladdress_id is not null;
alter table mdm_spring.warehouse
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
