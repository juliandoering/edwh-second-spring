alter table mdm_spring.markornumber
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_markornumber_contract_id cascade;
create index idx_markornumber_contract_id on mdm_spring.markornumber (contract_id);
