create table mdm_spring.businessthirdparty_thirdpartytype as table mdm.businessthirdparty_thirdpartytype with no data;
alter table mdm_spring.businessthirdparty_thirdpartytype alter column businessthirdparty_id type varchar;
alter table mdm_spring.businessthirdparty_thirdpartytype alter column thirdpartytype_id type varchar;
insert into mdm_spring.businessthirdparty_thirdpartytype select * from mdm.businessthirdparty_thirdpartytype;
update mdm_spring.businessthirdparty_thirdpartytype
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.businessthirdparty_thirdpartytype
set thirdpartytype_id = (select ref.uuid from mdm.thirdpartytype ref where ref.id = thirdpartytype_id::bigint)
where thirdpartytype_id is not null;
alter table mdm_spring.businessthirdparty_thirdpartytype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
