create table mdm_spring.warehouseupdates as table mdm.warehouseupdates with no data;
alter table mdm_spring.warehouseupdates alter column warehouse_id type varchar;
insert into mdm_spring.warehouseupdates select * from mdm.warehouseupdates;
update mdm_spring.warehouseupdates
set warehouse_id = (select ref.uuid from mdm.warehouse ref where ref.id = warehouse_id::bigint)
where warehouse_id is not null;
alter table mdm_spring.warehouseupdates
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
