alter table mdm_spring.externalcreditlimit
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_externalcreditlimit_businessthirdparty_id cascade;
create index idx_externalcreditlimit_businessthirdparty_id on mdm_spring.externalcreditlimit (businessthirdparty_id);
alter table mdm_spring.externalcreditlimit
   add constraint fk_insurer
   foreign key (insurer_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_externalcreditlimit_insurer_id cascade;
create index idx_externalcreditlimit_insurer_id on mdm_spring.externalcreditlimit (insurer_id);
alter table mdm_spring.externalcreditlimit
   add constraint fk_currency
   foreign key (currency_id)
       references mdm_spring.currency(id);
drop index if exists idx_externalcreditlimit_currency_id cascade;
create index idx_externalcreditlimit_currency_id on mdm_spring.externalcreditlimit (currency_id);
