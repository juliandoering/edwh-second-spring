create table mdm_spring.productcertification as table mdm.productcertification with no data;
alter table mdm_spring.productcertification alter column businessthirdparty_id type varchar;
alter table mdm_spring.productcertification alter column productcertificationprogram_id type varchar;
insert into mdm_spring.productcertification select * from mdm.productcertification where is_version=false and deleted=false;
update mdm_spring.productcertification
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.productcertification
set productcertificationprogram_id = (select ref.uuid from mdm.productcertificationprogram ref where ref.id = productcertificationprogram_id::bigint)
where productcertificationprogram_id is not null;
alter table mdm_spring.productcertification
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
