create table mdm_spring.carriercontractreference as table mdm.carriercontractreference with no data;
alter table mdm_spring.carriercontractreference alter column carrier_id type varchar;
insert into mdm_spring.carriercontractreference select * from mdm.carriercontractreference where is_version=false and deleted=false;
update mdm_spring.carriercontractreference
set carrier_id = (select ref.uuid from mdm.businessthirdpartygroup ref where ref.id = carrier_id::bigint)
where carrier_id is not null;
alter table mdm_spring.carriercontractreference
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
