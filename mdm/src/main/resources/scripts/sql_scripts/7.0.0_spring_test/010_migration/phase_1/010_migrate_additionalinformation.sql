create table mdm_spring.additionalinformation as table mdm.additionalinformation with no data;
alter table mdm_spring.additionalinformation alter column contract_id type varchar;
insert into mdm_spring.additionalinformation select * from mdm.additionalinformation where is_version=false and deleted=false;
update mdm_spring.additionalinformation
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
alter table mdm_spring.additionalinformation
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
