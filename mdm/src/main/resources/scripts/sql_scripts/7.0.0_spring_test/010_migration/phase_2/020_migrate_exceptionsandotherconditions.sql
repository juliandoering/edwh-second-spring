alter table mdm_spring.exceptionsandotherconditions drop column id;
alter table mdm_spring.exceptionsandotherconditions rename column uuid to id;
alter table mdm_spring.exceptionsandotherconditions add primary key (id);
