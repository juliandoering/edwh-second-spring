alter table mdm_spring.interstandardcertprogram drop column id;
alter table mdm_spring.interstandardcertprogram rename column uuid to id;
alter table mdm_spring.interstandardcertprogram add primary key (id);
