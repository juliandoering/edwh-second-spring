drop sequence if exists mdm_spring.delivery_futuresmonth_london_id_seq cascade;
create sequence mdm_spring.delivery_futuresmonth_london_id_seq owned by mdm_spring.delivery_futuresmonth_london.id;
select setval('mdm_spring.delivery_futuresmonth_london_id_seq', 13 + 1, false) FROM mdm_spring.delivery_futuresmonth_london;
alter table mdm_spring.delivery_futuresmonth_london
   alter column id set default nextval('mdm_spring.delivery_futuresmonth_london_id_seq'),
   alter column id set not null;
