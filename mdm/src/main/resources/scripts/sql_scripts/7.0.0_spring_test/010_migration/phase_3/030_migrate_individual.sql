alter table mdm_spring.individual
   add constraint fk_place_of_birth
   foreign key (place_of_birth_id)
       references mdm_spring.location(id);
drop index if exists idx_individual_place_of_birth_id cascade;
create index idx_individual_place_of_birth_id on mdm_spring.individual (place_of_birth_id);
alter table mdm_spring.individual
   add constraint fk_citizenship
   foreign key (citizenship_id)
       references mdm_spring.location(id);
drop index if exists idx_individual_citizenship_id cascade;
create index idx_individual_citizenship_id on mdm_spring.individual (citizenship_id);
alter table mdm_spring.individual
   add constraint fk_identificationtype
   foreign key (identificationtype_id)
       references mdm_spring.identificationtype(id);
drop index if exists idx_individual_identificationtype_id cascade;
create index idx_individual_identificationtype_id on mdm_spring.individual (identificationtype_id);
alter table mdm_spring.individual
   add constraint fk_postal_address
   foreign key (postal_address_id)
       references mdm_spring.postaladdress(id);
drop index if exists idx_individual_postal_address_id cascade;
create index idx_individual_postal_address_id on mdm_spring.individual (postal_address_id);
