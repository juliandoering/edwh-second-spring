create table mdm_spring.businessthirdpartygroup as table mdm.businessthirdpartygroup with no data;
insert into mdm_spring.businessthirdpartygroup select * from mdm.businessthirdpartygroup where is_version=false and deleted=false;
alter table mdm_spring.businessthirdpartygroup
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
