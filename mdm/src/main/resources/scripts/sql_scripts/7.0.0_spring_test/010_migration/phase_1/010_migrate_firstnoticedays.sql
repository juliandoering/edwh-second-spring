create table mdm_spring.firstnoticedays as table mdm.firstnoticedays with no data;
alter table mdm_spring.firstnoticedays alter column futuresmarket_id type varchar;
insert into mdm_spring.firstnoticedays select * from mdm.firstnoticedays where is_version=false and deleted=false;
update mdm_spring.firstnoticedays
set futuresmarket_id = (select ref.uuid from mdm.futuremarket ref where ref.id = futuresmarket_id::bigint)
where futuresmarket_id is not null;
alter table mdm_spring.firstnoticedays
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
