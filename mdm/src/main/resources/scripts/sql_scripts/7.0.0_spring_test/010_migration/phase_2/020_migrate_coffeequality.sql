alter table mdm_spring.coffeequality drop column id;
alter table mdm_spring.coffeequality rename column uuid to id;
alter table mdm_spring.coffeequality add primary key (id);
