alter table mdm_spring.businessorganisationtype drop column id;
alter table mdm_spring.businessorganisationtype rename column uuid to id;
alter table mdm_spring.businessorganisationtype add primary key (id);
