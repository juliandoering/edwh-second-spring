alter table mdm_spring.exchangerate
   add constraint fk_currency_to
   foreign key (currency_to_id)
       references mdm_spring.currency(id);
drop index if exists idx_exchangerate_currency_to_id cascade;
create index idx_exchangerate_currency_to_id on mdm_spring.exchangerate (currency_to_id);
alter table mdm_spring.exchangerate
   add constraint fk_currency_from
   foreign key (currency_from_id)
       references mdm_spring.currency(id);
drop index if exists idx_exchangerate_currency_from_id cascade;
create index idx_exchangerate_currency_from_id on mdm_spring.exchangerate (currency_from_id);
