create table mdm_spring.businessthirdparty_individual as table mdm.businessthirdparty_individual with no data;
alter table mdm_spring.businessthirdparty_individual alter column businessthirdparty_id type varchar;
alter table mdm_spring.businessthirdparty_individual alter column individual_id type varchar;
insert into mdm_spring.businessthirdparty_individual select * from mdm.businessthirdparty_individual;
update mdm_spring.businessthirdparty_individual
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.businessthirdparty_individual
set individual_id = (select ref.uuid from mdm.individual ref where ref.id = individual_id::bigint)
where individual_id is not null;
alter table mdm_spring.businessthirdparty_individual
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
