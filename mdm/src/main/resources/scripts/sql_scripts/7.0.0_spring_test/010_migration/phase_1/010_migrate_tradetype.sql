create table mdm_spring.tradetype as table mdm.tradetype with no data;
insert into mdm_spring.tradetype select * from mdm.tradetype where is_version=false and deleted=false;
alter table mdm_spring.tradetype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
