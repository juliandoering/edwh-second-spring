create table mdm_historic.revinfo_tmp (
    id bigint not null,
    uuid varchar not null,
    rev int not null,
    revtype smallint not null,
    timestmp bigint not null
);
create sequence envers_rev_seq;
insert into mdm_historic.revinfo_tmp
select nested.id, nested.uuid, nextval('mdm.envers_rev_seq'), nested.revtype, EXTRACT(EPOCH FROM nested.updated_at) from
(
/*contact*/
select id, uuid, 0 as revtype, updated_at from mdm.contact_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.contact_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.contact_old where deleted=true /*entity got deleted*/
union
/*identificationtype*/
select id, uuid, 0 as revtype, updated_at from mdm.identificationtype_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.identificationtype_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.identificationtype_old where deleted=true /*entity got deleted*/
union
/*individual*/
select id, uuid, 0 as revtype, updated_at from mdm.individual_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.individual_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.individual_old where deleted=true /*entity got deleted*/
union
/*location*/
select id, uuid, 0 as revtype, updated_at from mdm.location_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.location_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.location_old where deleted=true /*entity got deleted*/
union
/*locationfunction*/
select id, uuid, 0 as revtype, updated_at from mdm.locationfunction_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.locationfunction_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.locationfunction_old where deleted=true /*entity got deleted*/
union
/*postaladdress*/
select id, uuid, 0 as revtype, updated_at from mdm.postaladdress_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.postaladdress_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.postaladdress_old where deleted=true /*entity got deleted*/
union
/*postaladdresstype*/
select id, uuid, 0 as revtype, updated_at from mdm.postaladdresstype_old where is_version=true and deleted=false and parent=id /*entity got created (investigate parent=id or parent=0)*/
union select id, uuid, 1 as revtype, updated_at from mdm.postaladdresstype_old where deleted=false and parent!=id /*entity got updated (includes the current version of the entity as well)*/
union select id, uuid, 2 as revtype, updated_at from mdm.postaladdresstype_old where deleted=true /*entity got deleted*/
order by updated_at) as nested;
insert into mdm_historic.revinfo
select rev, timestmp from mdm_historic.revinfo_tmp;
