create table mdm_spring.futuremonth as table mdm.futuremonth with no data;
insert into mdm_spring.futuremonth select * from mdm.futuremonth where is_version=false and deleted=false;
alter table mdm_spring.futuremonth
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
