alter table mdm_spring.anyotherinstruction
   add constraint fk_shippinginstruction
   foreign key (shippinginstruction_id)
       references mdm_spring.shippinginstruction(id);
drop index if exists idx_anyotherinstruction_shippinginstruction_id cascade;
create index idx_anyotherinstruction_shippinginstruction_id on mdm_spring.anyotherinstruction (shippinginstruction_id);
