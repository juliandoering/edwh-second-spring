create table mdm_spring.presentation_of_documents as table mdm.presentation_of_documents with no data;
insert into mdm_spring.presentation_of_documents select * from mdm.presentation_of_documents where is_version=false and deleted=false;
alter table mdm_spring.presentation_of_documents
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
