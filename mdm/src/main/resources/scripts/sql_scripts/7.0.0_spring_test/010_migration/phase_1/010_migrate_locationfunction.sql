create table mdm_spring.locationfunction as table mdm.locationfunction with no data;
alter table mdm_spring.locationfunction alter column location_id type varchar;
insert into mdm_spring.locationfunction select * from mdm.locationfunction where is_version=false and deleted=false;
update mdm_spring.locationfunction
set location_id = (select ref.uuid from mdm.location ref where ref.id = location_id::bigint)
where location_id is not null;
alter table mdm_spring.locationfunction
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
