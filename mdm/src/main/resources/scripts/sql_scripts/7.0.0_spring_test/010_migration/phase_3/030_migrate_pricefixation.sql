alter table mdm_spring.pricefixation
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_pricefixation_contract_id cascade;
create index idx_pricefixation_contract_id on mdm_spring.pricefixation (contract_id);
