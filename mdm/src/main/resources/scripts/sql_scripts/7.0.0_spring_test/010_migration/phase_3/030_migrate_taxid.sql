alter table mdm_spring.taxid
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_taxid_businessthirdparty_id cascade;
create index idx_taxid_businessthirdparty_id on mdm_spring.taxid (businessthirdparty_id);
