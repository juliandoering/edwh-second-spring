alter table mdm_spring.settlementterminalrate
   add constraint fk_futuremonth
   foreign key (futuremonth_id)
       references mdm_spring.futuremonth(id);
drop index if exists idx_settlementterminalrate_futuremonth_id cascade;
create index idx_settlementterminalrate_futuremonth_id on mdm_spring.settlementterminalrate (futuremonth_id);
alter table mdm_spring.settlementterminalrate
   add constraint fk_futuremarket
   foreign key (futuremarket_id)
       references mdm_spring.futuremarket(id);
drop index if exists idx_settlementterminalrate_futuremarket_id cascade;
create index idx_settlementterminalrate_futuremarket_id on mdm_spring.settlementterminalrate (futuremarket_id);
