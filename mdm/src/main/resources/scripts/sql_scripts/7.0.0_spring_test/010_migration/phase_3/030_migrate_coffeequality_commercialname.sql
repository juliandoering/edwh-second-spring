alter table mdm_spring.coffeequality_commercialname
   add constraint fk_coffeequality
   foreign key (coffeequality_id)
       references mdm_spring.coffeequality(id);
drop index if exists idx_coffeequality_commercialname_coffeequality_id cascade;
create index idx_coffeequality_commercialname_coffeequality_id on mdm_spring.coffeequality_commercialname (coffeequality_id);
alter table mdm_spring.coffeequality_commercialname
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_coffeequality_commercialname_groupcompany_id cascade;
create index idx_coffeequality_commercialname_groupcompany_id on mdm_spring.coffeequality_commercialname (groupcompany_id);
