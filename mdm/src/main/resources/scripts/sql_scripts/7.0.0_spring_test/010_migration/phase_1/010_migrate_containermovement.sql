create table mdm_spring.containermovement as table mdm.containermovement with no data;
insert into mdm_spring.containermovement select * from mdm.containermovement where is_version=false and deleted=false;
alter table mdm_spring.containermovement
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
