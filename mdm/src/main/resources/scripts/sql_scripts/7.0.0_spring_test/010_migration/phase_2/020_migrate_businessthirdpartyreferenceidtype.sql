alter table mdm_spring.businessthirdpartyreferenceidtype drop column id;
alter table mdm_spring.businessthirdpartyreferenceidtype rename column uuid to id;
alter table mdm_spring.businessthirdpartyreferenceidtype add primary key (id);
