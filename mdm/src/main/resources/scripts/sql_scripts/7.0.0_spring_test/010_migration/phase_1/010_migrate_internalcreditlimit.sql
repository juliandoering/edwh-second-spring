create table mdm_spring.internalcreditlimit as table mdm.internalcreditlimit with no data;
alter table mdm_spring.internalcreditlimit alter column businessthirdparty_commerciallimit_id type varchar;
alter table mdm_spring.internalcreditlimit alter column currency_id type varchar;
insert into mdm_spring.internalcreditlimit select * from mdm.internalcreditlimit where is_version=false and deleted=false;
update mdm_spring.internalcreditlimit
set businessthirdparty_commerciallimit_id = (select ref.uuid from mdm.businessthirdparty_commerciallimit ref where ref.id = businessthirdparty_commerciallimit_id::bigint)
where businessthirdparty_commerciallimit_id is not null;
update mdm_spring.internalcreditlimit
set currency_id = (select ref.uuid from mdm.currency ref where ref.id = currency_id::bigint)
where currency_id is not null;
alter table mdm_spring.internalcreditlimit
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
