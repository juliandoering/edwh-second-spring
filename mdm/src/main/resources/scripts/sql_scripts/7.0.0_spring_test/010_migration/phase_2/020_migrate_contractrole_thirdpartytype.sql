alter table mdm_spring.contractrole_thirdpartytype drop column id;
alter table mdm_spring.contractrole_thirdpartytype rename column uuid to id;
alter table mdm_spring.contractrole_thirdpartytype add primary key (id);
