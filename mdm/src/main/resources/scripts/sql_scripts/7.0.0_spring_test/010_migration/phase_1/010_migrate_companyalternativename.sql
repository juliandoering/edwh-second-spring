create table mdm_spring.companyalternativename as table mdm.companyalternativename with no data;
alter table mdm_spring.companyalternativename alter column groupcompany_id type varchar;
insert into mdm_spring.companyalternativename select * from mdm.companyalternativename where is_version=false and deleted=false;
update mdm_spring.companyalternativename
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
alter table mdm_spring.companyalternativename
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
