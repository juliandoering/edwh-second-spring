create table mdm_spring.businessthirdpartyreferenceidtype as table mdm.businessthirdpartyreferenceidtype with no data;
insert into mdm_spring.businessthirdpartyreferenceidtype select * from mdm.businessthirdpartyreferenceidtype where is_version=false and deleted=false;
alter table mdm_spring.businessthirdpartyreferenceidtype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
