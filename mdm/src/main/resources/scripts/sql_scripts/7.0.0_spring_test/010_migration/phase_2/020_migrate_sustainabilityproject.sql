alter table mdm_spring.sustainabilityproject drop column id;
alter table mdm_spring.sustainabilityproject rename column uuid to id;
alter table mdm_spring.sustainabilityproject add primary key (id);
