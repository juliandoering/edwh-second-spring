create table mdm_spring.bankaccount as table mdm.bankaccount with no data;
alter table mdm_spring.bankaccount alter column businessthirdparty_id type varchar;
alter table mdm_spring.bankaccount alter column bankaccount_usage_type_id type varchar;
alter table mdm_spring.bankaccount alter column bankaccount_number_type_id type varchar;
alter table mdm_spring.bankaccount alter column postaladdress_id type varchar;
insert into mdm_spring.bankaccount select * from mdm.bankaccount where is_version=false and deleted=false;
update mdm_spring.bankaccount
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.bankaccount
set bankaccount_usage_type_id = (select ref.uuid from mdm.bankaccount_usagetype ref where ref.id = bankaccount_usage_type_id::bigint)
where bankaccount_usage_type_id is not null;
update mdm_spring.bankaccount
set bankaccount_number_type_id = (select ref.uuid from mdm.bankaccount_number_type ref where ref.id = bankaccount_number_type_id::bigint)
where bankaccount_number_type_id is not null;
update mdm_spring.bankaccount
set postaladdress_id = (select ref.uuid from mdm.postaladdress ref where ref.id = postaladdress_id::bigint)
where postaladdress_id is not null;
alter table mdm_spring.bankaccount
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
