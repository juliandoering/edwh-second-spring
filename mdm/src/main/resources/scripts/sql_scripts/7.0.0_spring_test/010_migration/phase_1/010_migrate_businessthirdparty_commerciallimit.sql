create table mdm_spring.businessthirdparty_commerciallimit as table mdm.businessthirdparty_commerciallimit with no data;
alter table mdm_spring.businessthirdparty_commerciallimit alter column businessthirdparty_id type varchar;
alter table mdm_spring.businessthirdparty_commerciallimit alter column groupcompany_id type varchar;
insert into mdm_spring.businessthirdparty_commerciallimit select * from mdm.businessthirdparty_commerciallimit where is_version=false and deleted=false;
update mdm_spring.businessthirdparty_commerciallimit
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.businessthirdparty_commerciallimit
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
alter table mdm_spring.businessthirdparty_commerciallimit
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
