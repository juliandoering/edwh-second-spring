insert into mdm_historic.contact_his
select old.uuid, ri.rev, ri.revtype, created_at, created_by, version, updated_at, updated_by, contacttype, contactvalue, ref3.uuid from mdm.contact_old old
inner join mdm_historic.revinfo_tmp ri on old.id=ri.id and old.uuid=ri.uuid
left join (select id, uuid from mdm.businessthirdparty_old) ref1
   on old.businessthirdparty_id=ref1.id
left join (select id, uuid from mdm.groupcompany_old) ref2
   on old.groupcompany_id=ref2.id
left join (select id, uuid from mdm.individual_old) ref3
   on old.individual_id=ref3.id
left join (select id, uuid from mdm.warehouse_old) ref4
   on old.warehouse_id=ref4.id;
