create table mdm_spring.businessthirdpartygroupmembership as table mdm.businessthirdpartygroupmembership with no data;
alter table mdm_spring.businessthirdpartygroupmembership alter column businessthirdparty_id type varchar;
alter table mdm_spring.businessthirdpartygroupmembership alter column businessthirdpartygroup_id type varchar;
insert into mdm_spring.businessthirdpartygroupmembership select * from mdm.businessthirdpartygroupmembership where is_version=false and deleted=false;
update mdm_spring.businessthirdpartygroupmembership
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.businessthirdpartygroupmembership
set businessthirdpartygroup_id = (select ref.uuid from mdm.businessthirdpartygroup ref where ref.id = businessthirdpartygroup_id::bigint)
where businessthirdpartygroup_id is not null;
alter table mdm_spring.businessthirdpartygroupmembership
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
