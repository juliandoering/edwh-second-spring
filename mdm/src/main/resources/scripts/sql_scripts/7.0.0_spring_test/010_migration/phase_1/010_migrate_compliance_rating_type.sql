create table mdm_spring.compliance_rating_type as table mdm.compliance_rating_type with no data;
insert into mdm_spring.compliance_rating_type select * from mdm.compliance_rating_type where is_version=false and deleted=false;
alter table mdm_spring.compliance_rating_type
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
