create table mdm_spring.modeoftransport as table mdm.modeoftransport with no data;
insert into mdm_spring.modeoftransport select * from mdm.modeoftransport where is_version=false and deleted=false;
alter table mdm_spring.modeoftransport
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
