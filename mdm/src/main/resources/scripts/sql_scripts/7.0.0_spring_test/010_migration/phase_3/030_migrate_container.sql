alter table mdm_spring.container
   add constraint fk_shipment_advice
   foreign key (shipment_advice_id)
       references mdm_spring.shipment_advice(id);
drop index if exists idx_container_shipment_advice_id cascade;
create index idx_container_shipment_advice_id on mdm_spring.container (shipment_advice_id);
