insert into mdm_historic.postaladdress_his
select old.uuid, ri.rev, ri.revtype, created_at, created_by, version, updated_at, updated_by, addressline1, addressline2, postal_code, ref2.uuid from mdm.postaladdress_old old
inner join mdm_historic.revinfo_tmp ri on old.id=ri.id and old.uuid=ri.uuid
left join (select id, uuid from mdm.businessthirdparty_old) ref1
   on old.businessthirdparty_id=ref1.id
left join (select id, uuid from mdm.location_old) ref2
   on old.location_id=ref2.id;
