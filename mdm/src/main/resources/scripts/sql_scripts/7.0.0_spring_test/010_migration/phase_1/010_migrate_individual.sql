create table mdm_spring.individual as table mdm.individual with no data;
alter table mdm_spring.individual alter column place_of_birth_id type varchar;
alter table mdm_spring.individual alter column citizenship_id type varchar;
alter table mdm_spring.individual alter column identificationtype_id type varchar;
alter table mdm_spring.individual alter column postal_address_id type varchar;
insert into mdm_spring.individual select * from mdm.individual where is_version=false and deleted=false;
update mdm_spring.individual
set place_of_birth_id = (select ref.uuid from mdm.location ref where ref.id = place_of_birth_id::bigint)
where place_of_birth_id is not null;
update mdm_spring.individual
set citizenship_id = (select ref.uuid from mdm.location ref where ref.id = citizenship_id::bigint)
where citizenship_id is not null;
update mdm_spring.individual
set identificationtype_id = (select ref.uuid from mdm.identificationtype ref where ref.id = identificationtype_id::bigint)
where identificationtype_id is not null;
update mdm_spring.individual
set postal_address_id = (select ref.uuid from mdm.postaladdress ref where ref.id = postal_address_id::bigint)
where postal_address_id is not null;
alter table mdm_spring.individual
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
