alter table mdm_spring.businessthirdparty_commerciallimit
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_businessthirdparty_commerciallimit_businessthirdparty_id cascade;
create index idx_businessthirdparty_commerciallimit_businessthirdparty_id on mdm_spring.businessthirdparty_commerciallimit (businessthirdparty_id);
alter table mdm_spring.businessthirdparty_commerciallimit
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_businessthirdparty_commerciallimit_groupcompany_id cascade;
create index idx_businessthirdparty_commerciallimit_groupcompany_id on mdm_spring.businessthirdparty_commerciallimit (groupcompany_id);
