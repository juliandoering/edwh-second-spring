create table mdm_spring.bankholiday as table mdm.bankholiday with no data;
alter table mdm_spring.bankholiday alter column futuresmarket_id type varchar;
insert into mdm_spring.bankholiday select * from mdm.bankholiday where is_version=false and deleted=false;
update mdm_spring.bankholiday
set futuresmarket_id = (select ref.uuid from mdm.futuremarket ref where ref.id = futuresmarket_id::bigint)
where futuresmarket_id is not null;
alter table mdm_spring.bankholiday
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
