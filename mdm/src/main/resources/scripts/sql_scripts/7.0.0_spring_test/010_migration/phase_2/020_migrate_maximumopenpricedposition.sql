alter table mdm_spring.maximumopenpricedposition drop column id;
alter table mdm_spring.maximumopenpricedposition rename column uuid to id;
alter table mdm_spring.maximumopenpricedposition add primary key (id);
