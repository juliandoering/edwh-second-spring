drop sequence if exists mdm_spring.futuremarket_futuremonth_id_seq cascade;
create sequence mdm_spring.futuremarket_futuremonth_id_seq owned by mdm_spring.futuremarket_futuremonth.id;
select setval('mdm_spring.futuremarket_futuremonth_id_seq', 12 + 1, false) FROM mdm_spring.futuremarket_futuremonth;
alter table mdm_spring.futuremarket_futuremonth
   alter column id set default nextval('mdm_spring.futuremarket_futuremonth_id_seq'),
   alter column id set not null;
alter table mdm_spring.futuremarket_futuremonth
   add constraint fk_futuremonth
   foreign key (futuremonth_id)
       references mdm_spring.futuremonth(id);
drop index if exists idx_futuremarket_futuremonth_futuremonth_id cascade;
create index idx_futuremarket_futuremonth_futuremonth_id on mdm_spring.futuremarket_futuremonth (futuremonth_id);
alter table mdm_spring.futuremarket_futuremonth
   add constraint fk_futuremarket
   foreign key (futuremarket_id)
       references mdm_spring.futuremarket(id);
drop index if exists idx_futuremarket_futuremonth_futuremarket_id cascade;
create index idx_futuremarket_futuremonth_futuremarket_id on mdm_spring.futuremarket_futuremonth (futuremarket_id);
