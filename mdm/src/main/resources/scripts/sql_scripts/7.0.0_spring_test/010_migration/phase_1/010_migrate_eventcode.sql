create table mdm_spring.eventcode as table mdm.eventcode with no data;
insert into mdm_spring.eventcode select * from mdm.eventcode where is_version=false and deleted=false;
alter table mdm_spring.eventcode
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
