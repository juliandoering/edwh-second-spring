alter table mdm_spring.delta_coffee
   add constraint fk_delta_coffee_type
   foreign key (delta_coffee_type_id)
       references mdm_spring.delta_coffee_type(id);
drop index if exists idx_delta_coffee_delta_coffee_type_id cascade;
create index idx_delta_coffee_delta_coffee_type_id on mdm_spring.delta_coffee (delta_coffee_type_id);
alter table mdm_spring.delta_coffee
   add constraint fk_coffeequality
   foreign key (coffeequality_id)
       references mdm_spring.coffeequality(id);
drop index if exists idx_delta_coffee_coffeequality_id cascade;
create index idx_delta_coffee_coffeequality_id on mdm_spring.delta_coffee (coffeequality_id);
alter table mdm_spring.delta_coffee
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_delta_coffee_groupcompany_id cascade;
create index idx_delta_coffee_groupcompany_id on mdm_spring.delta_coffee (groupcompany_id);
