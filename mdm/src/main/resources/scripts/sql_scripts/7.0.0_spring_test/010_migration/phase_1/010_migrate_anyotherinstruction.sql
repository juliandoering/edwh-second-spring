create table mdm_spring.anyotherinstruction as table mdm.anyotherinstruction with no data;
alter table mdm_spring.anyotherinstruction alter column shippinginstruction_id type varchar;
insert into mdm_spring.anyotherinstruction select * from mdm.anyotherinstruction where is_version=false and deleted=false;
update mdm_spring.anyotherinstruction
set shippinginstruction_id = (select ref.uuid from mdm.shippinginstruction ref where ref.id = shippinginstruction_id::bigint)
where shippinginstruction_id is not null;
alter table mdm_spring.anyotherinstruction
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
