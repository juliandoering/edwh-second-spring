alter table mdm_spring.freightpaymentterm drop column id;
alter table mdm_spring.freightpaymentterm rename column uuid to id;
alter table mdm_spring.freightpaymentterm add primary key (id);
