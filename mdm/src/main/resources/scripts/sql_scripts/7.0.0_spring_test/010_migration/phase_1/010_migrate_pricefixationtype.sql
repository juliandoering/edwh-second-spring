create table mdm_spring.pricefixationtype as table mdm.pricefixationtype with no data;
insert into mdm_spring.pricefixationtype select * from mdm.pricefixationtype where is_version=false and deleted=false;
alter table mdm_spring.pricefixationtype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
