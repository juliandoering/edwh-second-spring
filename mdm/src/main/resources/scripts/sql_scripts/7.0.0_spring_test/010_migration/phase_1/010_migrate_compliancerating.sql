create table mdm_spring.compliancerating as table mdm.compliancerating with no data;
alter table mdm_spring.compliancerating alter column businessthirdparty_id type varchar;
alter table mdm_spring.compliancerating alter column complianceratingtype_id type varchar;
insert into mdm_spring.compliancerating select * from mdm.compliancerating where is_version=false and deleted=false;
update mdm_spring.compliancerating
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.compliancerating
set complianceratingtype_id = (select ref.uuid from mdm.compliance_rating_type ref where ref.id = complianceratingtype_id::bigint)
where complianceratingtype_id is not null;
alter table mdm_spring.compliancerating
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
