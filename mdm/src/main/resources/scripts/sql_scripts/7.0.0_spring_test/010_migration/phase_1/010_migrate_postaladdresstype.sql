create table mdm_spring.postaladdresstype as table mdm.postaladdresstype with no data;
insert into mdm_spring.postaladdresstype select * from mdm.postaladdresstype where is_version=false and deleted=false;
alter table mdm_spring.postaladdresstype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
