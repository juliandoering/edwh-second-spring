create table mdm_spring.productcertificationprogram as table mdm.productcertificationprogram with no data;
insert into mdm_spring.productcertificationprogram select * from mdm.productcertificationprogram where is_version=false and deleted=false;
alter table mdm_spring.productcertificationprogram
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
