create table mdm_spring.configuration as table mdm.configuration with no data;
insert into mdm_spring.configuration select * from mdm.configuration where is_version=false and deleted=false;
alter table mdm_spring.configuration
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
