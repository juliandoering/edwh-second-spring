alter table mdm_spring.delta_coffee drop column id;
alter table mdm_spring.delta_coffee rename column uuid to id;
alter table mdm_spring.delta_coffee add primary key (id);
