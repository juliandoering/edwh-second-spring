alter table mdm_spring.futuremarket
   add constraint fk_lot_quantityunit
   foreign key (lot_quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_futuremarket_lot_quantityunit_id cascade;
create index idx_futuremarket_lot_quantityunit_id on mdm_spring.futuremarket (lot_quantityunit_id);
