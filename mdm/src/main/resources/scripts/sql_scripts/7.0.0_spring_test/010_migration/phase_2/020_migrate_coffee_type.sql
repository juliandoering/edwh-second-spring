alter table mdm_spring.coffee_type drop column id;
alter table mdm_spring.coffee_type rename column uuid to id;
alter table mdm_spring.coffee_type add primary key (id);
