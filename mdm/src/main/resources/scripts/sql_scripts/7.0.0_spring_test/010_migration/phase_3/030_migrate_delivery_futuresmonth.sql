drop sequence if exists mdm_spring.delivery_futuresmonth_id_seq cascade;
create sequence mdm_spring.delivery_futuresmonth_id_seq owned by mdm_spring.delivery_futuresmonth.id;
select setval('mdm_spring.delivery_futuresmonth_id_seq', 49 + 1, false) FROM mdm_spring.delivery_futuresmonth;
alter table mdm_spring.delivery_futuresmonth
   alter column id set default nextval('mdm_spring.delivery_futuresmonth_id_seq'),
   alter column id set not null;
alter table mdm_spring.delivery_futuresmonth
   add constraint fk_futuresmarket
   foreign key (futuresmarket_id)
       references mdm_spring.futuremarket(id);
drop index if exists idx_delivery_futuresmonth_futuresmarket_id cascade;
create index idx_delivery_futuresmonth_futuresmarket_id on mdm_spring.delivery_futuresmonth (futuresmarket_id);
