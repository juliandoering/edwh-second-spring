create table mdm_spring.sustainabilityproject as table mdm.sustainabilityproject with no data;
insert into mdm_spring.sustainabilityproject select * from mdm.sustainabilityproject where is_version=false and deleted=false;
alter table mdm_spring.sustainabilityproject
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
