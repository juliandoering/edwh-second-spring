create table mdm_spring.currency as table mdm.currency with no data;
insert into mdm_spring.currency select * from mdm.currency where is_version=false and deleted=false;
alter table mdm_spring.currency
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
