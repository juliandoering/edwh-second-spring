alter table mdm_spring.carriertraderoute
   add constraint fk_tradename
   foreign key (tradename_id)
       references mdm_spring.tradename(id);
drop index if exists idx_carriertraderoute_tradename_id cascade;
create index idx_carriertraderoute_tradename_id on mdm_spring.carriertraderoute (tradename_id);
alter table mdm_spring.carriertraderoute
   add constraint fk_portofloading
   foreign key (portofloading_id)
       references mdm_spring.location(id);
drop index if exists idx_carriertraderoute_portofloading_id cascade;
create index idx_carriertraderoute_portofloading_id on mdm_spring.carriertraderoute (portofloading_id);
alter table mdm_spring.carriertraderoute
   add constraint fk_provider
   foreign key (provider_id)
       references mdm_spring.businessthirdpartygroup(id);
drop index if exists idx_carriertraderoute_provider_id cascade;
create index idx_carriertraderoute_provider_id on mdm_spring.carriertraderoute (provider_id);
alter table mdm_spring.carriertraderoute
   add constraint fk_portofdischarge
   foreign key (portofdischarge_id)
       references mdm_spring.location(id);
drop index if exists idx_carriertraderoute_portofdischarge_id cascade;
create index idx_carriertraderoute_portofdischarge_id on mdm_spring.carriertraderoute (portofdischarge_id);
