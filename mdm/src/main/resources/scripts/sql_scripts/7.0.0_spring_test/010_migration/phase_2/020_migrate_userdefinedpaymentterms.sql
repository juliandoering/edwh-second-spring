alter table mdm_spring.userdefinedpaymentterms drop column id;
alter table mdm_spring.userdefinedpaymentterms rename column uuid to id;
alter table mdm_spring.userdefinedpaymentterms add primary key (id);
