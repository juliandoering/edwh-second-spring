create table mdm_spring.bankaccount_number_type as table mdm.bankaccount_number_type with no data;
insert into mdm_spring.bankaccount_number_type select * from mdm.bankaccount_number_type where is_version=false and deleted=false;
alter table mdm_spring.bankaccount_number_type
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
