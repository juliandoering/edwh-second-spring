create table mdm_spring.postaladdress as table mdm.postaladdress with no data;
alter table mdm_spring.postaladdress alter column businessthirdparty_id type varchar;
alter table mdm_spring.postaladdress alter column location_id type varchar;
insert into mdm_spring.postaladdress select * from mdm.postaladdress where is_version=false and deleted=false;
update mdm_spring.postaladdress
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.postaladdress
set location_id = (select ref.uuid from mdm.location ref where ref.id = location_id::bigint)
where location_id is not null;
alter table mdm_spring.postaladdress
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
