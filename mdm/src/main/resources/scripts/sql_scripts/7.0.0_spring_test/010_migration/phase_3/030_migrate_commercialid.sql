alter table mdm_spring.commercialid
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_commercialid_businessthirdparty_id cascade;
create index idx_commercialid_businessthirdparty_id on mdm_spring.commercialid (businessthirdparty_id);
