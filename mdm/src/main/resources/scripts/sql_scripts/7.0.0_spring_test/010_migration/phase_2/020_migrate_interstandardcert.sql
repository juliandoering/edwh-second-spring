alter table mdm_spring.interstandardcert drop column id;
alter table mdm_spring.interstandardcert rename column uuid to id;
alter table mdm_spring.interstandardcert add primary key (id);
