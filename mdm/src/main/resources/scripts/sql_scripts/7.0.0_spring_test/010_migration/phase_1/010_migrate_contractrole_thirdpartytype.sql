create table mdm_spring.contractrole_thirdpartytype as table mdm.contractrole_thirdpartytype with no data;
alter table mdm_spring.contractrole_thirdpartytype alter column thirdpartytype_id type varchar;
insert into mdm_spring.contractrole_thirdpartytype select * from mdm.contractrole_thirdpartytype;
update mdm_spring.contractrole_thirdpartytype
set thirdpartytype_id = (select ref.uuid from mdm.thirdpartytype ref where ref.id = thirdpartytype_id::bigint)
where thirdpartytype_id is not null;
alter table mdm_spring.contractrole_thirdpartytype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
