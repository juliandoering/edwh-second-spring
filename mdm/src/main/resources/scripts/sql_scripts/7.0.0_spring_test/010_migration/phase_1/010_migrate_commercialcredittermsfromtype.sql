create table mdm_spring.commercialcredittermsfromtype as table mdm.commercialcredittermsfromtype with no data;
insert into mdm_spring.commercialcredittermsfromtype select * from mdm.commercialcredittermsfromtype where is_version=false and deleted=false;
alter table mdm_spring.commercialcredittermsfromtype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
