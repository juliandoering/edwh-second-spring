alter table mdm_spring.identificationtype drop column id;
alter table mdm_spring.identificationtype rename column uuid to id;
alter table mdm_spring.identificationtype add primary key (id);
