alter table mdm_spring.sustainabilityprojectinformation
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_sustainabilityprojectinformation_businessthirdparty_id cascade;
create index idx_sustainabilityprojectinformation_businessthirdparty_id on mdm_spring.sustainabilityprojectinformation (businessthirdparty_id);
alter table mdm_spring.sustainabilityprojectinformation
   add constraint fk_sustainabilityproject
   foreign key (sustainabilityproject_id)
       references mdm_spring.sustainabilityproject(id);
drop index if exists idx_sustainabilityprojectinformation_sustainabilityproject_id cascade;
create index idx_sustainabilityprojectinformation_sustainabilityproject_id on mdm_spring.sustainabilityprojectinformation (sustainabilityproject_id);
