alter table mdm_spring.pricefixationterm
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_pricefixationterm_businessthirdparty_id cascade;
create index idx_pricefixationterm_businessthirdparty_id on mdm_spring.pricefixationterm (businessthirdparty_id);
alter table mdm_spring.pricefixationterm
   add constraint fk_pricefixationtype
   foreign key (pricefixationtype_id)
       references mdm_spring.pricefixationtype(id);
drop index if exists idx_pricefixationterm_pricefixationtype_id cascade;
create index idx_pricefixationterm_pricefixationtype_id on mdm_spring.pricefixationterm (pricefixationtype_id);
alter table mdm_spring.pricefixationterm
   add constraint fk_fixationoption
   foreign key (fixationoption_id)
       references mdm_spring.fixationoption(id);
drop index if exists idx_pricefixationterm_fixationoption_id cascade;
create index idx_pricefixationterm_fixationoption_id on mdm_spring.pricefixationterm (fixationoption_id);
