alter table mdm_spring.oceanfreightrate drop column id;
alter table mdm_spring.oceanfreightrate rename column uuid to id;
alter table mdm_spring.oceanfreightrate add primary key (id);
