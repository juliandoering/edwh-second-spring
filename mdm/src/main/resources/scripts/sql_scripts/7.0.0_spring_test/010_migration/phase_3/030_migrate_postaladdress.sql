alter table mdm_spring.postaladdress
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_postaladdress_businessthirdparty_id cascade;
create index idx_postaladdress_businessthirdparty_id on mdm_spring.postaladdress (businessthirdparty_id);
alter table mdm_spring.postaladdress
   add constraint fk_location
   foreign key (location_id)
       references mdm_spring.location(id);
drop index if exists idx_postaladdress_location_id cascade;
create index idx_postaladdress_location_id on mdm_spring.postaladdress (location_id);
