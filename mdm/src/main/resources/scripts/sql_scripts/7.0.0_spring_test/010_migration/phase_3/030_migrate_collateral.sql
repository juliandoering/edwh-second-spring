alter table mdm_spring.collateral
   add constraint fk_collateraltype
   foreign key (collateraltype_id)
       references mdm_spring.collateraltype(id);
drop index if exists idx_collateral_collateraltype_id cascade;
create index idx_collateral_collateraltype_id on mdm_spring.collateral (collateraltype_id);
alter table mdm_spring.collateral
   add constraint fk_businessthirdparty_commerciallimit
   foreign key (businessthirdparty_commerciallimit_id)
       references mdm_spring.businessthirdparty_commerciallimit(id);
drop index if exists idx_collateral_businessthirdparty_commerciallimit_id cascade;
create index idx_collateral_businessthirdparty_commerciallimit_id on mdm_spring.collateral (businessthirdparty_commerciallimit_id);
alter table mdm_spring.collateral
   add constraint fk_currency
   foreign key (currency_id)
       references mdm_spring.currency(id);
drop index if exists idx_collateral_currency_id cascade;
create index idx_collateral_currency_id on mdm_spring.collateral (currency_id);
