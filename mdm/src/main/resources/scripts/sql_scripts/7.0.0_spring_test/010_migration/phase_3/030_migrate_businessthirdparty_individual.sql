alter table mdm_spring.businessthirdparty_individual
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_businessthirdparty_individual_businessthirdparty_id cascade;
create index idx_businessthirdparty_individual_businessthirdparty_id on mdm_spring.businessthirdparty_individual (businessthirdparty_id);
alter table mdm_spring.businessthirdparty_individual
   add constraint fk_individual
   foreign key (individual_id)
       references mdm_spring.individual(id);
drop index if exists idx_businessthirdparty_individual_individual_id cascade;
create index idx_businessthirdparty_individual_individual_id on mdm_spring.businessthirdparty_individual (individual_id);
