create table mdm_spring.downloadedresource as table mdm.downloadedresource with no data;
insert into mdm_spring.downloadedresource select * from mdm.downloadedresource where deleted=false;
alter table mdm_spring.downloadedresource
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
