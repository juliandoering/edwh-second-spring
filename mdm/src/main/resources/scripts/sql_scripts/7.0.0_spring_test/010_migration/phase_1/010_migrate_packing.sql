create table mdm_spring.packing as table mdm.packing with no data;
alter table mdm_spring.packing alter column category_id type varchar;
insert into mdm_spring.packing select * from mdm.packing where is_version=false and deleted=false;
update mdm_spring.packing
set category_id = (select ref.uuid from mdm.packingtypecategory ref where ref.id = category_id::bigint)
where category_id is not null;
alter table mdm_spring.packing
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
