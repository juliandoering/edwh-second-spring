alter table mdm_spring.delta_coffee_type drop column id;
alter table mdm_spring.delta_coffee_type rename column uuid to id;
alter table mdm_spring.delta_coffee_type add primary key (id);
