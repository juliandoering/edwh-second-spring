create table mdm_spring.collateraltype as table mdm.collateraltype with no data;
insert into mdm_spring.collateraltype select * from mdm.collateraltype where is_version=false and deleted=false;
alter table mdm_spring.collateraltype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
