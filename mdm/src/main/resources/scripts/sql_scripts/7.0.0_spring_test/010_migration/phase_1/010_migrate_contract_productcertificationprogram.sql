create table mdm_spring.contract_productcertificationprogram as table mdm.contract_productcertificationprogram with no data;
alter table mdm_spring.contract_productcertificationprogram alter column contract_id type varchar;
alter table mdm_spring.contract_productcertificationprogram alter column productcertificationprogram_id type varchar;
insert into mdm_spring.contract_productcertificationprogram select * from mdm.contract_productcertificationprogram;
update mdm_spring.contract_productcertificationprogram
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
update mdm_spring.contract_productcertificationprogram
set productcertificationprogram_id = (select ref.uuid from mdm.productcertificationprogram ref where ref.id = productcertificationprogram_id::bigint)
where productcertificationprogram_id is not null;
alter table mdm_spring.contract_productcertificationprogram
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
