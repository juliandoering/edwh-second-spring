drop sequence if exists mdm_spring.postaladdress_postaladdresstype_id_seq cascade;
create sequence mdm_spring.postaladdress_postaladdresstype_id_seq owned by mdm_spring.postaladdress_postaladdresstype.id;
select setval('mdm_spring.postaladdress_postaladdresstype_id_seq', 0 + 1, false) FROM mdm_spring.postaladdress_postaladdresstype;
alter table mdm_spring.postaladdress_postaladdresstype
   alter column id set default nextval('mdm_spring.postaladdress_postaladdresstype_id_seq'),
   alter column id set not null;
alter table mdm_spring.postaladdress_postaladdresstype
   add constraint fk_postaladdresstype
   foreign key (postaladdresstype_id)
       references mdm_spring.postaladdresstype(id);
drop index if exists idx_postaladdress_postaladdresstype_postaladdresstype_id cascade;
create index idx_postaladdress_postaladdresstype_postaladdresstype_id on mdm_spring.postaladdress_postaladdresstype (postaladdresstype_id);
alter table mdm_spring.postaladdress_postaladdresstype
   add constraint fk_postaladdress
   foreign key (postaladdress_id)
       references mdm_spring.postaladdress(id);
drop index if exists idx_postaladdress_postaladdresstype_postaladdress_id cascade;
create index idx_postaladdress_postaladdresstype_postaladdress_id on mdm_spring.postaladdress_postaladdresstype (postaladdress_id);
