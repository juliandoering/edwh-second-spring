alter table mdm_spring.businessthirdparty_individual drop column id;
alter table mdm_spring.businessthirdparty_individual rename column uuid to id;
alter table mdm_spring.businessthirdparty_individual add primary key (id);
