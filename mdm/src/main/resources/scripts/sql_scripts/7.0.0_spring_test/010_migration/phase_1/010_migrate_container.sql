create table mdm_spring.container as table mdm.container with no data;
alter table mdm_spring.container alter column shipment_advice_id type varchar;
insert into mdm_spring.container select * from mdm.container where is_version=false and deleted=false;
update mdm_spring.container
set shipment_advice_id = (select ref.uuid from mdm.shipment_advice ref where ref.id = shipment_advice_id::bigint)
where shipment_advice_id is not null;
alter table mdm_spring.container
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
