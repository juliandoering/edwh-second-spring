create table mdm_spring.taxid as table mdm.taxid with no data;
alter table mdm_spring.taxid alter column businessthirdparty_id type varchar;
insert into mdm_spring.taxid select * from mdm.taxid where is_version=false and deleted=false;
update mdm_spring.taxid
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
alter table mdm_spring.taxid
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
