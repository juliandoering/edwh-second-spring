create table mdm_spring.sustainabilityprojectinformation as table mdm.sustainabilityprojectinformation with no data;
alter table mdm_spring.sustainabilityprojectinformation alter column businessthirdparty_id type varchar;
alter table mdm_spring.sustainabilityprojectinformation alter column sustainabilityproject_id type varchar;
insert into mdm_spring.sustainabilityprojectinformation select * from mdm.sustainabilityprojectinformation where is_version=false and deleted=false;
update mdm_spring.sustainabilityprojectinformation
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.sustainabilityprojectinformation
set sustainabilityproject_id = (select ref.uuid from mdm.sustainabilityproject ref where ref.id = sustainabilityproject_id::bigint)
where sustainabilityproject_id is not null;
alter table mdm_spring.sustainabilityprojectinformation
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
