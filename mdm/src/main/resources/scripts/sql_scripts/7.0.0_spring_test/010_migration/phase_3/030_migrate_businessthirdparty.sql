alter table mdm_spring.businessthirdparty
   add constraint fk_incoterm
   foreign key (incoterm_id)
       references mdm_spring.incoterm(id);
drop index if exists idx_businessthirdparty_incoterm_id cascade;
create index idx_businessthirdparty_incoterm_id on mdm_spring.businessthirdparty (incoterm_id);
alter table mdm_spring.businessthirdparty
   add constraint fk_businessorganisationtype
   foreign key (businessorganisationtype_id)
       references mdm_spring.businessorganisationtype(id);
drop index if exists idx_businessthirdparty_businessorganisationtype_id cascade;
create index idx_businessthirdparty_businessorganisationtype_id on mdm_spring.businessthirdparty (businessorganisationtype_id);
alter table mdm_spring.businessthirdparty
   add constraint fk_presentation_of_documents
   foreign key (presentation_of_documents_id)
       references mdm_spring.presentation_of_documents(id);
drop index if exists idx_businessthirdparty_presentation_of_documents_id cascade;
create index idx_businessthirdparty_presentation_of_documents_id on mdm_spring.businessthirdparty (presentation_of_documents_id);
alter table mdm_spring.businessthirdparty
   add constraint fk_paymentterm
   foreign key (paymentterm_id)
       references mdm_spring.paymentterm(id);
drop index if exists idx_businessthirdparty_paymentterm_id cascade;
create index idx_businessthirdparty_paymentterm_id on mdm_spring.businessthirdparty (paymentterm_id);
alter table mdm_spring.businessthirdparty
   add constraint fk_legal_country
   foreign key (legal_country_id)
       references mdm_spring.location(id);
drop index if exists idx_businessthirdparty_legal_country_id cascade;
create index idx_businessthirdparty_legal_country_id on mdm_spring.businessthirdparty (legal_country_id);
