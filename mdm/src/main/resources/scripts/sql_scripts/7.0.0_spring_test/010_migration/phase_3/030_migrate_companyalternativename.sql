alter table mdm_spring.companyalternativename
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_companyalternativename_groupcompany_id cascade;
create index idx_companyalternativename_groupcompany_id on mdm_spring.companyalternativename (groupcompany_id);
