alter table mdm_spring.typeofshipment drop column id;
alter table mdm_spring.typeofshipment rename column uuid to id;
alter table mdm_spring.typeofshipment add primary key (id);
