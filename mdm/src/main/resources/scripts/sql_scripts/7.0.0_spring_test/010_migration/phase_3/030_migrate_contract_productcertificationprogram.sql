drop sequence if exists mdm_spring.contract_productcertificationprogram_id_seq cascade;
create sequence mdm_spring.contract_productcertificationprogram_id_seq owned by mdm_spring.contract_productcertificationprogram.id;
select setval('mdm_spring.contract_productcertificationprogram_id_seq', 0 + 1, false) FROM mdm_spring.contract_productcertificationprogram;
alter table mdm_spring.contract_productcertificationprogram
   alter column id set default nextval('mdm_spring.contract_productcertificationprogram_id_seq'),
   alter column id set not null;
alter table mdm_spring.contract_productcertificationprogram
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_contract_productcertificationprogram_contract_id cascade;
create index idx_contract_productcertificationprogram_contract_id on mdm_spring.contract_productcertificationprogram (contract_id);
alter table mdm_spring.contract_productcertificationprogram
   add constraint fk_productcertificationprogram
   foreign key (productcertificationprogram_id)
       references mdm_spring.productcertificationprogram(id);
drop index if exists idx_contract_productcertificationprogram_productcertificationprogram_id cascade;
create index idx_contract_productcertificationprogram_productcertificationprogram_id on mdm_spring.contract_productcertificationprogram (productcertificationprogram_id);
