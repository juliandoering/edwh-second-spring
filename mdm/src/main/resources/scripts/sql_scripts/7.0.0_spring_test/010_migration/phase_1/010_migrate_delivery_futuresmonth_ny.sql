create table mdm_spring.delivery_futuresmonth_ny as table mdm.delivery_futuresmonth_ny with no data;
insert into mdm_spring.delivery_futuresmonth_ny select * from mdm.delivery_futuresmonth_ny;
alter table mdm_spring.delivery_futuresmonth_ny
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
