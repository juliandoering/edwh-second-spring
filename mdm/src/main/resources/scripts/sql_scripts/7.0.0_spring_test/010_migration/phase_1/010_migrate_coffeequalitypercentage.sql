create table mdm_spring.coffeequalitypercentage as table mdm.coffeequalitypercentage with no data;
alter table mdm_spring.coffeequalitypercentage alter column referenced_by_coffeequality_id type varchar;
alter table mdm_spring.coffeequalitypercentage alter column coffeequality_id type varchar;
insert into mdm_spring.coffeequalitypercentage select * from mdm.coffeequalitypercentage where is_version=false and deleted=false;
update mdm_spring.coffeequalitypercentage
set referenced_by_coffeequality_id = (select ref.uuid from mdm.coffeequality ref where ref.id = referenced_by_coffeequality_id::bigint)
where referenced_by_coffeequality_id is not null;
update mdm_spring.coffeequalitypercentage
set coffeequality_id = (select ref.uuid from mdm.coffeequality ref where ref.id = coffeequality_id::bigint)
where coffeequality_id is not null;
alter table mdm_spring.coffeequalitypercentage
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
