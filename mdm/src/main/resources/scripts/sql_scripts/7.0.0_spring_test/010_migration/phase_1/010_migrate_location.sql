create table mdm_spring.location as table mdm.location with no data;
insert into mdm_spring.location select * from mdm.location where is_version=false and deleted=false;
alter table mdm_spring.location
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
