alter table mdm_spring.amendment
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_amendment_contract_id cascade;
create index idx_amendment_contract_id on mdm_spring.amendment (contract_id);
