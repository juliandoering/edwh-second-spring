alter table mdm_spring.commercialcreditterms
   add constraint fk_businessthirdparty_commerciallimit
   foreign key (businessthirdparty_commerciallimit_id)
       references mdm_spring.businessthirdparty_commerciallimit(id);
drop index if exists idx_commercialcreditterms_businessthirdparty_commerciallimit_id cascade;
create index idx_commercialcreditterms_businessthirdparty_commerciallimit_id on mdm_spring.commercialcreditterms (businessthirdparty_commerciallimit_id);
alter table mdm_spring.commercialcreditterms
   add constraint fk_commercialcredittermsfromtype
   foreign key (commercialcredittermsfromtype_id)
       references mdm_spring.commercialcredittermsfromtype(id);
drop index if exists idx_commercialcreditterms_commercialcredittermsfromtype_id cascade;
create index idx_commercialcreditterms_commercialcredittermsfromtype_id on mdm_spring.commercialcreditterms (commercialcredittermsfromtype_id);
