alter table mdm_spring.compliance_rating_type drop column id;
alter table mdm_spring.compliance_rating_type rename column uuid to id;
alter table mdm_spring.compliance_rating_type add primary key (id);
