drop sequence if exists mdm_spring.businessthirdparty_thirdpartytype_id_seq cascade;
create sequence mdm_spring.businessthirdparty_thirdpartytype_id_seq owned by mdm_spring.businessthirdparty_thirdpartytype.id;
select setval('mdm_spring.businessthirdparty_thirdpartytype_id_seq', 5 + 1, false) FROM mdm_spring.businessthirdparty_thirdpartytype;
alter table mdm_spring.businessthirdparty_thirdpartytype
   alter column id set default nextval('mdm_spring.businessthirdparty_thirdpartytype_id_seq'),
   alter column id set not null;
alter table mdm_spring.businessthirdparty_thirdpartytype
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_businessthirdparty_thirdpartytype_businessthirdparty_id cascade;
create index idx_businessthirdparty_thirdpartytype_businessthirdparty_id on mdm_spring.businessthirdparty_thirdpartytype (businessthirdparty_id);
alter table mdm_spring.businessthirdparty_thirdpartytype
   add constraint fk_thirdpartytype
   foreign key (thirdpartytype_id)
       references mdm_spring.thirdpartytype(id);
drop index if exists idx_businessthirdparty_thirdpartytype_thirdpartytype_id cascade;
create index idx_businessthirdparty_thirdpartytype_thirdpartytype_id on mdm_spring.businessthirdparty_thirdpartytype (thirdpartytype_id);
