alter table mdm_spring.userdefinedpaymentterms
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_userdefinedpaymentterms_contract_id cascade;
create index idx_userdefinedpaymentterms_contract_id on mdm_spring.userdefinedpaymentterms (contract_id);
