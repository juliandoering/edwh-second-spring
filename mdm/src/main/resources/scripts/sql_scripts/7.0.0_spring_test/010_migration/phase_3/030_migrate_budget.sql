alter table mdm_spring.budget
   add constraint fk_businessthirdparty_commerciallimit
   foreign key (businessthirdparty_commerciallimit_id)
       references mdm_spring.businessthirdparty_commerciallimit(id);
drop index if exists idx_budget_businessthirdparty_commerciallimit_id cascade;
create index idx_budget_businessthirdparty_commerciallimit_id on mdm_spring.budget (businessthirdparty_commerciallimit_id);
alter table mdm_spring.budget
   add constraint fk_quantityunit
   foreign key (quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_budget_quantityunit_id cascade;
create index idx_budget_quantityunit_id on mdm_spring.budget (quantityunit_id);
