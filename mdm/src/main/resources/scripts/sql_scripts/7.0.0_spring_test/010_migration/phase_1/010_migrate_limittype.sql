create table mdm_spring.limittype as table mdm.limittype with no data;
insert into mdm_spring.limittype select * from mdm.limittype where is_version=false and deleted=false;
alter table mdm_spring.limittype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
