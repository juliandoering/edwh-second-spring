alter table mdm_spring.warehouse
   add constraint fk_limit_currency
   foreign key (limit_currency_id)
       references mdm_spring.currency(id);
drop index if exists idx_warehouse_limit_currency_id cascade;
create index idx_warehouse_limit_currency_id on mdm_spring.warehouse (limit_currency_id);
alter table mdm_spring.warehouse
   add constraint fk_owner_businessthirdparty
   foreign key (owner_businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_warehouse_owner_businessthirdparty_id cascade;
create index idx_warehouse_owner_businessthirdparty_id on mdm_spring.warehouse (owner_businessthirdparty_id);
alter table mdm_spring.warehouse
   add constraint fk_owner_groupcompany
   foreign key (owner_groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_warehouse_owner_groupcompany_id cascade;
create index idx_warehouse_owner_groupcompany_id on mdm_spring.warehouse (owner_groupcompany_id);
alter table mdm_spring.warehouse
   add constraint fk_postaladdress
   foreign key (postaladdress_id)
       references mdm_spring.postaladdress(id);
drop index if exists idx_warehouse_postaladdress_id cascade;
create index idx_warehouse_postaladdress_id on mdm_spring.warehouse (postaladdress_id);
