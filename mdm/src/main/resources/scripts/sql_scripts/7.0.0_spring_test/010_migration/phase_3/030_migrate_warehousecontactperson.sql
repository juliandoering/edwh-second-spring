alter table mdm_spring.warehousecontactperson
   add constraint fk_individual
   foreign key (individual_id)
       references mdm_spring.individual(id);
drop index if exists idx_warehousecontactperson_individual_id cascade;
create index idx_warehousecontactperson_individual_id on mdm_spring.warehousecontactperson (individual_id);
alter table mdm_spring.warehousecontactperson
   add constraint fk_warehouse
   foreign key (warehouse_id)
       references mdm_spring.warehouse(id);
drop index if exists idx_warehousecontactperson_warehouse_id cascade;
create index idx_warehousecontactperson_warehouse_id on mdm_spring.warehousecontactperson (warehouse_id);
