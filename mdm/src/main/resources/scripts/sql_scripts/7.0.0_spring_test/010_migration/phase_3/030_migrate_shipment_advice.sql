alter table mdm_spring.shipment_advice
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_shipment_advice_contract_id cascade;
create index idx_shipment_advice_contract_id on mdm_spring.shipment_advice (contract_id);
