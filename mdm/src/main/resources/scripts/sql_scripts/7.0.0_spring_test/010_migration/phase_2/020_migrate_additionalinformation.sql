alter table mdm_spring.additionalinformation drop column id;
alter table mdm_spring.additionalinformation rename column uuid to id;
alter table mdm_spring.additionalinformation add primary key (id);
