alter table mdm_spring.freightratetradenotificationmail drop column id;
alter table mdm_spring.freightratetradenotificationmail rename column uuid to id;
alter table mdm_spring.freightratetradenotificationmail add primary key (id);
