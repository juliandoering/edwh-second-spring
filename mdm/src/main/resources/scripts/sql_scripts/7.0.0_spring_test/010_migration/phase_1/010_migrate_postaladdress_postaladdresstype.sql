create table mdm_spring.postaladdress_postaladdresstype as table mdm.postaladdress_postaladdresstype with no data;
alter table mdm_spring.postaladdress_postaladdresstype alter column postaladdresstype_id type varchar;
alter table mdm_spring.postaladdress_postaladdresstype alter column postaladdress_id type varchar;
insert into mdm_spring.postaladdress_postaladdresstype select * from mdm.postaladdress_postaladdresstype;
update mdm_spring.postaladdress_postaladdresstype
set postaladdresstype_id = (select ref.uuid from mdm.postaladdresstype ref where ref.id = postaladdresstype_id::bigint)
where postaladdresstype_id is not null;
update mdm_spring.postaladdress_postaladdresstype
set postaladdress_id = (select ref.uuid from mdm.postaladdress ref where ref.id = postaladdress_id::bigint)
where postaladdress_id is not null;
alter table mdm_spring.postaladdress_postaladdresstype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
