alter table mdm_spring.brokeraccount
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_brokeraccount_groupcompany_id cascade;
create index idx_brokeraccount_groupcompany_id on mdm_spring.brokeraccount (groupcompany_id);
