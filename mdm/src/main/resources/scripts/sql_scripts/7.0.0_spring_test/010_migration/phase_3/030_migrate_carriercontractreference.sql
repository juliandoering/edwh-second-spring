alter table mdm_spring.carriercontractreference
   add constraint fk_carrier
   foreign key (carrier_id)
       references mdm_spring.businessthirdpartygroup(id);
drop index if exists idx_carriercontractreference_carrier_id cascade;
create index idx_carriercontractreference_carrier_id on mdm_spring.carriercontractreference (carrier_id);
