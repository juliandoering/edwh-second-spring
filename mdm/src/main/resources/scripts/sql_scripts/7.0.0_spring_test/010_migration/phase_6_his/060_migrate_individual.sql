insert into mdm_historic.individual_his
select old.uuid, ri.rev, ri.revtype, created_at, created_by, version, updated_at, updated_by, academic_title, addressing_form, comment, date_of_birth, first_name, gender, identification_counter, pending, last_name, personal_id, tax_id, ref2.uuid, ref3.uuid, ref1.uuid, ref4.uuid from mdm.individual_old old
inner join mdm_historic.revinfo_tmp ri on old.id=ri.id and old.uuid=ri.uuid
left join (select id, uuid from mdm.location_old) ref1
   on old.place_of_birth_id=ref1.id
left join (select id, uuid from mdm.location_old) ref2
   on old.citizenship_id=ref2.id
left join (select id, uuid from mdm.identificationtype_old) ref3
   on old.identificationtype_id=ref3.id
left join (select id, uuid from mdm.postaladdress_old) ref4
   on old.postal_address_id=ref4.id;
