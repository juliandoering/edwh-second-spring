alter table mdm_spring.coffeequality_commercialname drop column id;
alter table mdm_spring.coffeequality_commercialname rename column uuid to id;
alter table mdm_spring.coffeequality_commercialname add primary key (id);
