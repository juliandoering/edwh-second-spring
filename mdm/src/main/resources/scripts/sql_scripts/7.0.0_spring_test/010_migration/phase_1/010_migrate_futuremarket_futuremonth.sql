create table mdm_spring.futuremarket_futuremonth as table mdm.futuremarket_futuremonth with no data;
alter table mdm_spring.futuremarket_futuremonth alter column futuremonth_id type varchar;
alter table mdm_spring.futuremarket_futuremonth alter column futuremarket_id type varchar;
insert into mdm_spring.futuremarket_futuremonth select * from mdm.futuremarket_futuremonth;
update mdm_spring.futuremarket_futuremonth
set futuremonth_id = (select ref.uuid from mdm.futuremonth ref where ref.id = futuremonth_id::bigint)
where futuremonth_id is not null;
update mdm_spring.futuremarket_futuremonth
set futuremarket_id = (select ref.uuid from mdm.futuremarket ref where ref.id = futuremarket_id::bigint)
where futuremarket_id is not null;
alter table mdm_spring.futuremarket_futuremonth
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
