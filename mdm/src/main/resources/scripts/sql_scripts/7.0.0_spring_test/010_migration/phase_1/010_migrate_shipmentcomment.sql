create table mdm_spring.shipmentcomment as table mdm.shipmentcomment with no data;
insert into mdm_spring.shipmentcomment select * from mdm.shipmentcomment where is_version=false and deleted=false;
alter table mdm_spring.shipmentcomment
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
