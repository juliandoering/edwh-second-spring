create table mdm_spring.warehousecontactperson as table mdm.warehousecontactperson with no data;
alter table mdm_spring.warehousecontactperson alter column individual_id type varchar;
alter table mdm_spring.warehousecontactperson alter column warehouse_id type varchar;
insert into mdm_spring.warehousecontactperson select * from mdm.warehousecontactperson where is_version=false and deleted=false;
update mdm_spring.warehousecontactperson
set individual_id = (select ref.uuid from mdm.individual ref where ref.id = individual_id::bigint)
where individual_id is not null;
update mdm_spring.warehousecontactperson
set warehouse_id = (select ref.uuid from mdm.warehouse ref where ref.id = warehouse_id::bigint)
where warehouse_id is not null;
alter table mdm_spring.warehousecontactperson
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
