alter table mdm_spring.interstandardcert
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_interstandardcert_businessthirdparty_id cascade;
create index idx_interstandardcert_businessthirdparty_id on mdm_spring.interstandardcert (businessthirdparty_id);
alter table mdm_spring.interstandardcert
   add constraint fk_interstandardcertprogram
   foreign key (interstandardcertprogram_id)
       references mdm_spring.interstandardcertprogram(id);
drop index if exists idx_interstandardcert_interstandardcertprogram_id cascade;
create index idx_interstandardcert_interstandardcertprogram_id on mdm_spring.interstandardcert (interstandardcertprogram_id);
