alter table mdm_spring.shipment_advice drop column id;
alter table mdm_spring.shipment_advice rename column uuid to id;
alter table mdm_spring.shipment_advice add primary key (id);
