create table mdm_spring.tradename as table mdm.tradename with no data;
insert into mdm_spring.tradename select * from mdm.tradename where is_version=false and deleted=false;
alter table mdm_spring.tradename
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
