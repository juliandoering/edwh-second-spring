create table mdm_spring.interstandardcertprogram as table mdm.interstandardcertprogram with no data;
insert into mdm_spring.interstandardcertprogram select * from mdm.interstandardcertprogram where is_version=false and deleted=false;
alter table mdm_spring.interstandardcertprogram
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
