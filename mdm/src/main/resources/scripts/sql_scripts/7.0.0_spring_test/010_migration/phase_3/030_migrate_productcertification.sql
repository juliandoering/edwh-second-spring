alter table mdm_spring.productcertification
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_productcertification_businessthirdparty_id cascade;
create index idx_productcertification_businessthirdparty_id on mdm_spring.productcertification (businessthirdparty_id);
alter table mdm_spring.productcertification
   add constraint fk_productcertificationprogram
   foreign key (productcertificationprogram_id)
       references mdm_spring.productcertificationprogram(id);
drop index if exists idx_productcertification_productcertificationprogram_id cascade;
create index idx_productcertification_productcertificationprogram_id on mdm_spring.productcertification (productcertificationprogram_id);
