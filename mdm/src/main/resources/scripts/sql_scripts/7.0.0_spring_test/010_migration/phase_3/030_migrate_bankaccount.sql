alter table mdm_spring.bankaccount
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_bankaccount_businessthirdparty_id cascade;
create index idx_bankaccount_businessthirdparty_id on mdm_spring.bankaccount (businessthirdparty_id);
alter table mdm_spring.bankaccount
   add constraint fk_bankaccount_usage_type
   foreign key (bankaccount_usage_type_id)
       references mdm_spring.bankaccount_usagetype(id);
drop index if exists idx_bankaccount_bankaccount_usage_type_id cascade;
create index idx_bankaccount_bankaccount_usage_type_id on mdm_spring.bankaccount (bankaccount_usage_type_id);
alter table mdm_spring.bankaccount
   add constraint fk_bankaccount_number_type
   foreign key (bankaccount_number_type_id)
       references mdm_spring.bankaccount_number_type(id);
drop index if exists idx_bankaccount_bankaccount_number_type_id cascade;
create index idx_bankaccount_bankaccount_number_type_id on mdm_spring.bankaccount (bankaccount_number_type_id);
alter table mdm_spring.bankaccount
   add constraint fk_postaladdress
   foreign key (postaladdress_id)
       references mdm_spring.postaladdress(id);
drop index if exists idx_bankaccount_postaladdress_id cascade;
create index idx_bankaccount_postaladdress_id on mdm_spring.bankaccount (postaladdress_id);
