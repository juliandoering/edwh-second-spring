create table mdm_spring.bankaccount_usagetype as table mdm.bankaccount_usagetype with no data;
insert into mdm_spring.bankaccount_usagetype select * from mdm.bankaccount_usagetype where is_version=false and deleted=false;
alter table mdm_spring.bankaccount_usagetype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
