create table mdm_spring.pricefixationterm as table mdm.pricefixationterm with no data;
alter table mdm_spring.pricefixationterm alter column businessthirdparty_id type varchar;
alter table mdm_spring.pricefixationterm alter column pricefixationtype_id type varchar;
alter table mdm_spring.pricefixationterm alter column fixationoption_id type varchar;
insert into mdm_spring.pricefixationterm select * from mdm.pricefixationterm where is_version=false and deleted=false;
update mdm_spring.pricefixationterm
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.pricefixationterm
set pricefixationtype_id = (select ref.uuid from mdm.pricefixationtype ref where ref.id = pricefixationtype_id::bigint)
where pricefixationtype_id is not null;
update mdm_spring.pricefixationterm
set fixationoption_id = (select ref.uuid from mdm.fixationoption ref where ref.id = fixationoption_id::bigint)
where fixationoption_id is not null;
alter table mdm_spring.pricefixationterm
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
