alter table mdm_spring.warehousecontactperson drop column id;
alter table mdm_spring.warehousecontactperson rename column uuid to id;
alter table mdm_spring.warehousecontactperson add primary key (id);
