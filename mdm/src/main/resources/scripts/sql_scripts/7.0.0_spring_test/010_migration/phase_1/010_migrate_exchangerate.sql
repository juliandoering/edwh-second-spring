create table mdm_spring.exchangerate as table mdm.exchangerate with no data;
alter table mdm_spring.exchangerate alter column currency_to_id type varchar;
alter table mdm_spring.exchangerate alter column currency_from_id type varchar;
insert into mdm_spring.exchangerate select * from mdm.exchangerate where is_version=false and deleted=false;
update mdm_spring.exchangerate
set currency_to_id = (select ref.uuid from mdm.currency ref where ref.id = currency_to_id::bigint)
where currency_to_id is not null;
update mdm_spring.exchangerate
set currency_from_id = (select ref.uuid from mdm.currency ref where ref.id = currency_from_id::bigint)
where currency_from_id is not null;
alter table mdm_spring.exchangerate
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
