alter table mdm_spring.shipmentcomment drop column id;
alter table mdm_spring.shipmentcomment rename column uuid to id;
alter table mdm_spring.shipmentcomment add primary key (id);
