alter table mdm_spring.routingsummary
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_routingsummary_contract_id cascade;
create index idx_routingsummary_contract_id on mdm_spring.routingsummary (contract_id);
alter table mdm_spring.routingsummary
   add constraint fk_location
   foreign key (location_id)
       references mdm_spring.location(id);
drop index if exists idx_routingsummary_location_id cascade;
create index idx_routingsummary_location_id on mdm_spring.routingsummary (location_id);
