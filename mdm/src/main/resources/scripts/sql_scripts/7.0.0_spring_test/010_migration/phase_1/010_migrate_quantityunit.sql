create table mdm_spring.quantityunit as table mdm.quantityunit with no data;
insert into mdm_spring.quantityunit select * from mdm.quantityunit where is_version=false and deleted=false;
alter table mdm_spring.quantityunit
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
