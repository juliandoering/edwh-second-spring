alter table mdm_spring.clause
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_clause_contract_id cascade;
create index idx_clause_contract_id on mdm_spring.clause (contract_id);
