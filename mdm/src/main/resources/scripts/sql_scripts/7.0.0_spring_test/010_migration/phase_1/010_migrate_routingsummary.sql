create table mdm_spring.routingsummary as table mdm.routingsummary with no data;
alter table mdm_spring.routingsummary alter column contract_id type varchar;
alter table mdm_spring.routingsummary alter column location_id type varchar;
insert into mdm_spring.routingsummary select * from mdm.routingsummary;
update mdm_spring.routingsummary
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
update mdm_spring.routingsummary
set location_id = (select ref.uuid from mdm.location ref where ref.id = location_id::bigint)
where location_id is not null;
alter table mdm_spring.routingsummary
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
