create table mdm_spring.contact as table mdm.contact with no data;
alter table mdm_spring.contact alter column businessthirdparty_id type varchar;
alter table mdm_spring.contact alter column groupcompany_id type varchar;
alter table mdm_spring.contact alter column individual_id type varchar;
alter table mdm_spring.contact alter column warehouse_id type varchar;
insert into mdm_spring.contact select * from mdm.contact where is_version=false and deleted=false;
update mdm_spring.contact
set businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = businessthirdparty_id::bigint)
where businessthirdparty_id is not null;
update mdm_spring.contact
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
update mdm_spring.contact
set individual_id = (select ref.uuid from mdm.individual ref where ref.id = individual_id::bigint)
where individual_id is not null;
update mdm_spring.contact
set warehouse_id = (select ref.uuid from mdm.warehouse ref where ref.id = warehouse_id::bigint)
where warehouse_id is not null;
alter table mdm_spring.contact
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
