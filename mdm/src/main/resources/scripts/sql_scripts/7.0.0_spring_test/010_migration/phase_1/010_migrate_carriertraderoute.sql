create table mdm_spring.carriertraderoute as table mdm.carriertraderoute with no data;
alter table mdm_spring.carriertraderoute alter column tradename_id type varchar;
alter table mdm_spring.carriertraderoute alter column portofloading_id type varchar;
alter table mdm_spring.carriertraderoute alter column provider_id type varchar;
alter table mdm_spring.carriertraderoute alter column portofdischarge_id type varchar;
insert into mdm_spring.carriertraderoute select * from mdm.carriertraderoute where is_version=false and deleted=false;
update mdm_spring.carriertraderoute
set tradename_id = (select ref.uuid from mdm.tradename ref where ref.id = tradename_id::bigint)
where tradename_id is not null;
update mdm_spring.carriertraderoute
set portofloading_id = (select ref.uuid from mdm.location ref where ref.id = portofloading_id::bigint)
where portofloading_id is not null;
update mdm_spring.carriertraderoute
set provider_id = (select ref.uuid from mdm.businessthirdpartygroup ref where ref.id = provider_id::bigint)
where provider_id is not null;
update mdm_spring.carriertraderoute
set portofdischarge_id = (select ref.uuid from mdm.location ref where ref.id = portofdischarge_id::bigint)
where portofdischarge_id is not null;
alter table mdm_spring.carriertraderoute
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
