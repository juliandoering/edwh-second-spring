create table mdm_spring.amendment as table mdm.amendment with no data;
alter table mdm_spring.amendment alter column contract_id type varchar;
insert into mdm_spring.amendment select * from mdm.amendment where is_version=false and deleted=false;
update mdm_spring.amendment
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
alter table mdm_spring.amendment
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
