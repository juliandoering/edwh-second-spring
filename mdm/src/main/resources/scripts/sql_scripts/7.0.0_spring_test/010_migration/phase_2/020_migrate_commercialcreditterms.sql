alter table mdm_spring.commercialcreditterms drop column id;
alter table mdm_spring.commercialcreditterms rename column uuid to id;
alter table mdm_spring.commercialcreditterms add primary key (id);
