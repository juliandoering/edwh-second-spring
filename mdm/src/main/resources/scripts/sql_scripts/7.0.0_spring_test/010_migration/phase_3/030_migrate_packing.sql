alter table mdm_spring.packing
   add constraint fk_category
   foreign key (category_id)
       references mdm_spring.packingtypecategory(id);
drop index if exists idx_packing_category_id cascade;
create index idx_packing_category_id on mdm_spring.packing (category_id);
