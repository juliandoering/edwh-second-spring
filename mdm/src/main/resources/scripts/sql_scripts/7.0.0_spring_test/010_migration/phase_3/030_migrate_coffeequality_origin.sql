alter table mdm_spring.coffeequality_origin
   add constraint fk_coffeequality
   foreign key (coffeequality_id)
       references mdm_spring.coffeequality(id);
drop index if exists idx_coffeequality_origin_coffeequality_id cascade;
create index idx_coffeequality_origin_coffeequality_id on mdm_spring.coffeequality_origin (coffeequality_id);
alter table mdm_spring.coffeequality_origin
   add constraint fk_location
   foreign key (location_id)
       references mdm_spring.location(id);
drop index if exists idx_coffeequality_origin_location_id cascade;
create index idx_coffeequality_origin_location_id on mdm_spring.coffeequality_origin (location_id);
