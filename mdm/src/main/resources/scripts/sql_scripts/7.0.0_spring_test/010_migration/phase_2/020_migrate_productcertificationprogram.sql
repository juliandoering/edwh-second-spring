alter table mdm_spring.productcertificationprogram drop column id;
alter table mdm_spring.productcertificationprogram rename column uuid to id;
alter table mdm_spring.productcertificationprogram add primary key (id);
