alter table mdm_spring.businessthirdparty drop column id;
alter table mdm_spring.businessthirdparty rename column uuid to id;
alter table mdm_spring.businessthirdparty add primary key (id);
