create table mdm_spring.groupcompanybusinesslimit as table mdm.groupcompanybusinesslimit with no data;
alter table mdm_spring.groupcompanybusinesslimit alter column limittype_id type varchar;
alter table mdm_spring.groupcompanybusinesslimit alter column limitunitcurrency_id type varchar;
alter table mdm_spring.groupcompanybusinesslimit alter column limitunitquantity_id type varchar;
alter table mdm_spring.groupcompanybusinesslimit alter column groupcompany_id type varchar;
insert into mdm_spring.groupcompanybusinesslimit select * from mdm.groupcompanybusinesslimit where is_version=false and deleted=false;
update mdm_spring.groupcompanybusinesslimit
set limittype_id = (select ref.uuid from mdm.limittype ref where ref.id = limittype_id::bigint)
where limittype_id is not null;
update mdm_spring.groupcompanybusinesslimit
set limitunitcurrency_id = (select ref.uuid from mdm.currency ref where ref.id = limitunitcurrency_id::bigint)
where limitunitcurrency_id is not null;
update mdm_spring.groupcompanybusinesslimit
set limitunitquantity_id = (select ref.uuid from mdm.quantityunit ref where ref.id = limitunitquantity_id::bigint)
where limitunitquantity_id is not null;
update mdm_spring.groupcompanybusinesslimit
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
alter table mdm_spring.groupcompanybusinesslimit
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
