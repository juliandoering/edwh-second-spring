create table mdm_spring.groupcompany_individual as table mdm.groupcompany_individual with no data;
alter table mdm_spring.groupcompany_individual alter column groupcompany_id type varchar;
alter table mdm_spring.groupcompany_individual alter column individual_id type varchar;
insert into mdm_spring.groupcompany_individual select * from mdm.groupcompany_individual;
update mdm_spring.groupcompany_individual
set groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = groupcompany_id::bigint)
where groupcompany_id is not null;
update mdm_spring.groupcompany_individual
set individual_id = (select ref.uuid from mdm.individual ref where ref.id = individual_id::bigint)
where individual_id is not null;
alter table mdm_spring.groupcompany_individual
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
