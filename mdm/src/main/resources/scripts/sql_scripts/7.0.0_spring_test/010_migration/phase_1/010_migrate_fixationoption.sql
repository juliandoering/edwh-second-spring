create table mdm_spring.fixationoption as table mdm.fixationoption with no data;
insert into mdm_spring.fixationoption select * from mdm.fixationoption where is_version=false and deleted=false;
alter table mdm_spring.fixationoption
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
