alter table mdm_spring.coffeequalitypercentage
   add constraint fk_referenced_by_coffeequality
   foreign key (referenced_by_coffeequality_id)
       references mdm_spring.coffeequality(id);
drop index if exists idx_coffeequalitypercentage_referenced_by_coffeequality_id cascade;
create index idx_coffeequalitypercentage_referenced_by_coffeequality_id on mdm_spring.coffeequalitypercentage (referenced_by_coffeequality_id);
alter table mdm_spring.coffeequalitypercentage
   add constraint fk_coffeequality
   foreign key (coffeequality_id)
       references mdm_spring.coffeequality(id);
drop index if exists idx_coffeequalitypercentage_coffeequality_id cascade;
create index idx_coffeequalitypercentage_coffeequality_id on mdm_spring.coffeequalitypercentage (coffeequality_id);
