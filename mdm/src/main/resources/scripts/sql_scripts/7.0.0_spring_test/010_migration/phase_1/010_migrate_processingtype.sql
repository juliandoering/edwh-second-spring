create table mdm_spring.processingtype as table mdm.processingtype with no data;
insert into mdm_spring.processingtype select * from mdm.processingtype where is_version=false and deleted=false;
alter table mdm_spring.processingtype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
