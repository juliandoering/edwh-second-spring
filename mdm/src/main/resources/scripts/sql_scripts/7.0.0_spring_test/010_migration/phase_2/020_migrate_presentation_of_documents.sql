alter table mdm_spring.presentation_of_documents drop column id;
alter table mdm_spring.presentation_of_documents rename column uuid to id;
alter table mdm_spring.presentation_of_documents add primary key (id);
