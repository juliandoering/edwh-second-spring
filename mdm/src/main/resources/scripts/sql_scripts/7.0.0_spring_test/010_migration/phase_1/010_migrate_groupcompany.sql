create table mdm_spring.groupcompany as table mdm.groupcompany with no data;
alter table mdm_spring.groupcompany alter column businessfunction_id type varchar;
alter table mdm_spring.groupcompany alter column postal_address_id type varchar;
insert into mdm_spring.groupcompany select * from mdm.groupcompany where is_version=false and deleted=false;
update mdm_spring.groupcompany
set businessfunction_id = (select ref.uuid from mdm.businessfunction ref where ref.id = businessfunction_id::bigint)
where businessfunction_id is not null;
update mdm_spring.groupcompany
set postal_address_id = (select ref.uuid from mdm.postaladdress ref where ref.id = postal_address_id::bigint)
where postal_address_id is not null;
alter table mdm_spring.groupcompany
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
