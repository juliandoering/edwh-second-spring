create table mdm_spring.incoterm as table mdm.incoterm with no data;
insert into mdm_spring.incoterm select * from mdm.incoterm where is_version=false and deleted=false;
alter table mdm_spring.incoterm
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
