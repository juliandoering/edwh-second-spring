drop sequence if exists mdm_spring.groupcompany_individual_id_seq cascade;
create sequence mdm_spring.groupcompany_individual_id_seq owned by mdm_spring.groupcompany_individual.id;
select setval('mdm_spring.groupcompany_individual_id_seq', 17 + 1, false) FROM mdm_spring.groupcompany_individual;
alter table mdm_spring.groupcompany_individual
   alter column id set default nextval('mdm_spring.groupcompany_individual_id_seq'),
   alter column id set not null;
alter table mdm_spring.groupcompany_individual
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_groupcompany_individual_groupcompany_id cascade;
create index idx_groupcompany_individual_groupcompany_id on mdm_spring.groupcompany_individual (groupcompany_id);
alter table mdm_spring.groupcompany_individual
   add constraint fk_individual
   foreign key (individual_id)
       references mdm_spring.individual(id);
drop index if exists idx_groupcompany_individual_individual_id cascade;
create index idx_groupcompany_individual_individual_id on mdm_spring.groupcompany_individual (individual_id);
