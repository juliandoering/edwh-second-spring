alter table mdm_spring.groupcompanybusinesslimit
   add constraint fk_limittype
   foreign key (limittype_id)
       references mdm_spring.limittype(id);
drop index if exists idx_groupcompanybusinesslimit_limittype_id cascade;
create index idx_groupcompanybusinesslimit_limittype_id on mdm_spring.groupcompanybusinesslimit (limittype_id);
alter table mdm_spring.groupcompanybusinesslimit
   add constraint fk_limitunitcurrency
   foreign key (limitunitcurrency_id)
       references mdm_spring.currency(id);
drop index if exists idx_groupcompanybusinesslimit_limitunitcurrency_id cascade;
create index idx_groupcompanybusinesslimit_limitunitcurrency_id on mdm_spring.groupcompanybusinesslimit (limitunitcurrency_id);
alter table mdm_spring.groupcompanybusinesslimit
   add constraint fk_limitunitquantity
   foreign key (limitunitquantity_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_groupcompanybusinesslimit_limitunitquantity_id cascade;
create index idx_groupcompanybusinesslimit_limitunitquantity_id on mdm_spring.groupcompanybusinesslimit (limitunitquantity_id);
alter table mdm_spring.groupcompanybusinesslimit
   add constraint fk_groupcompany
   foreign key (groupcompany_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_groupcompanybusinesslimit_groupcompany_id cascade;
create index idx_groupcompanybusinesslimit_groupcompany_id on mdm_spring.groupcompanybusinesslimit (groupcompany_id);
