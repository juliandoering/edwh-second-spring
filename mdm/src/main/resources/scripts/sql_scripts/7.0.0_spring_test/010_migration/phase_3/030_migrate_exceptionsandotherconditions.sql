alter table mdm_spring.exceptionsandotherconditions
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_exceptionsandotherconditions_contract_id cascade;
create index idx_exceptionsandotherconditions_contract_id on mdm_spring.exceptionsandotherconditions (contract_id);
