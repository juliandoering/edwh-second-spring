create table mdm_spring.shipment_advice as table mdm.shipment_advice with no data;
alter table mdm_spring.shipment_advice alter column contract_id type varchar;
insert into mdm_spring.shipment_advice select * from mdm.shipment_advice where is_version=false and deleted=false;
update mdm_spring.shipment_advice
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
alter table mdm_spring.shipment_advice
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
