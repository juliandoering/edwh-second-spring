create table mdm_spring.samplehandling as table mdm.samplehandling with no data;
insert into mdm_spring.samplehandling select * from mdm.samplehandling where is_version=false and deleted=false;
alter table mdm_spring.samplehandling
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
