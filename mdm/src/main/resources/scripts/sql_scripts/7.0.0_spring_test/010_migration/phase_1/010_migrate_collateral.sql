create table mdm_spring.collateral as table mdm.collateral with no data;
alter table mdm_spring.collateral alter column collateraltype_id type varchar;
alter table mdm_spring.collateral alter column businessthirdparty_commerciallimit_id type varchar;
alter table mdm_spring.collateral alter column currency_id type varchar;
insert into mdm_spring.collateral select * from mdm.collateral where is_version=false and deleted=false;
update mdm_spring.collateral
set collateraltype_id = (select ref.uuid from mdm.collateraltype ref where ref.id = collateraltype_id::bigint)
where collateraltype_id is not null;
update mdm_spring.collateral
set businessthirdparty_commerciallimit_id = (select ref.uuid from mdm.businessthirdparty_commerciallimit ref where ref.id = businessthirdparty_commerciallimit_id::bigint)
where businessthirdparty_commerciallimit_id is not null;
update mdm_spring.collateral
set currency_id = (select ref.uuid from mdm.currency ref where ref.id = currency_id::bigint)
where currency_id is not null;
alter table mdm_spring.collateral
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
