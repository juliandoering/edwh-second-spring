alter table mdm_spring.otherapprovalterms
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_otherapprovalterms_contract_id cascade;
create index idx_otherapprovalterms_contract_id on mdm_spring.otherapprovalterms (contract_id);
