alter table mdm_spring.internalcreditlimit
   add constraint fk_businessthirdparty_commerciallimit
   foreign key (businessthirdparty_commerciallimit_id)
       references mdm_spring.businessthirdparty_commerciallimit(id);
drop index if exists idx_internalcreditlimit_businessthirdparty_commerciallimit_id cascade;
create index idx_internalcreditlimit_businessthirdparty_commerciallimit_id on mdm_spring.internalcreditlimit (businessthirdparty_commerciallimit_id);
alter table mdm_spring.internalcreditlimit
   add constraint fk_currency
   foreign key (currency_id)
       references mdm_spring.currency(id);
drop index if exists idx_internalcreditlimit_currency_id cascade;
create index idx_internalcreditlimit_currency_id on mdm_spring.internalcreditlimit (currency_id);
