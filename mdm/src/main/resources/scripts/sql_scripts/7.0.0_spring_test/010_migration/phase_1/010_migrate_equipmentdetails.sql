create table mdm_spring.equipmentdetails as table mdm.equipmentdetails with no data;
insert into mdm_spring.equipmentdetails select * from mdm.equipmentdetails where is_version=false and deleted=false;
alter table mdm_spring.equipmentdetails
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
