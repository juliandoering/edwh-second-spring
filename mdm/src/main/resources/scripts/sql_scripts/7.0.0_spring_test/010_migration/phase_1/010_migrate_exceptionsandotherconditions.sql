create table mdm_spring.exceptionsandotherconditions as table mdm.exceptionsandotherconditions with no data;
alter table mdm_spring.exceptionsandotherconditions alter column contract_id type varchar;
insert into mdm_spring.exceptionsandotherconditions select * from mdm.exceptionsandotherconditions where is_version=false and deleted=false;
update mdm_spring.exceptionsandotherconditions
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
alter table mdm_spring.exceptionsandotherconditions
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
