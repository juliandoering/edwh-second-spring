create table mdm_spring.email as table mdm.email with no data;
insert into mdm_spring.email select * from mdm.email where is_version=false and deleted=false;
alter table mdm_spring.email
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
