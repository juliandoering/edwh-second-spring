alter table mdm_spring.businessthirdpartygroupmembership
   add constraint fk_businessthirdparty
   foreign key (businessthirdparty_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_businessthirdpartygroupmembership_businessthirdparty_id cascade;
create index idx_businessthirdpartygroupmembership_businessthirdparty_id on mdm_spring.businessthirdpartygroupmembership (businessthirdparty_id);
alter table mdm_spring.businessthirdpartygroupmembership
   add constraint fk_businessthirdpartygroup
   foreign key (businessthirdpartygroup_id)
       references mdm_spring.businessthirdpartygroup(id);
drop index if exists idx_businessthirdpartygroupmembership_businessthirdpartygroup_id cascade;
create index idx_businessthirdpartygroupmembership_businessthirdpartygroup_id on mdm_spring.businessthirdpartygroupmembership (businessthirdpartygroup_id);
