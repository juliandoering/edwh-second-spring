alter table mdm_spring.coffee_variety drop column id;
alter table mdm_spring.coffee_variety rename column uuid to id;
alter table mdm_spring.coffee_variety add primary key (id);
