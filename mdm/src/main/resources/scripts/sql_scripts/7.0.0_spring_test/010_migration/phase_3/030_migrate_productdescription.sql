alter table mdm_spring.productdescription
   add constraint fk_contract
   foreign key (contract_id)
       references mdm_spring.contract(id);
drop index if exists idx_productdescription_contract_id cascade;
create index idx_productdescription_contract_id on mdm_spring.productdescription (contract_id);
