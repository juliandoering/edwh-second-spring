insert into mdm_historic.locationfunction_his
select old.uuid, ri.rev, ri.revtype, created_at, created_by, version, updated_at, updated_by, function_type, ref1.uuid from mdm.locationfunction_old old
inner join mdm_historic.revinfo_tmp ri on old.id=ri.id and old.uuid=ri.uuid
left join (select id, uuid from mdm.location_old) ref1
   on old.location_id=ref1.id;
