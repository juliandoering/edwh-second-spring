alter table mdm_spring.bankholiday
   add constraint fk_futuresmarket
   foreign key (futuresmarket_id)
       references mdm_spring.futuremarket(id);
drop index if exists idx_bankholiday_futuresmarket_id cascade;
create index idx_bankholiday_futuresmarket_id on mdm_spring.bankholiday (futuresmarket_id);
