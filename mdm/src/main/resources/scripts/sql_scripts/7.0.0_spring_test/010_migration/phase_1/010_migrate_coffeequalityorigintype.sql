create table mdm_spring.coffeequalityorigintype as table mdm.coffeequalityorigintype with no data;
insert into mdm_spring.coffeequalityorigintype select * from mdm.coffeequalityorigintype where is_version=false and deleted=false;
alter table mdm_spring.coffeequalityorigintype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
