create table mdm_spring.freightratetradenotificationmail as table mdm.freightratetradenotificationmail with no data;
insert into mdm_spring.freightratetradenotificationmail select * from mdm.freightratetradenotificationmail where is_version=false and deleted=false;
alter table mdm_spring.freightratetradenotificationmail
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
