create table mdm_spring.commercialcreditterms as table mdm.commercialcreditterms with no data;
alter table mdm_spring.commercialcreditterms alter column businessthirdparty_commerciallimit_id type varchar;
alter table mdm_spring.commercialcreditterms alter column commercialcredittermsfromtype_id type varchar;
insert into mdm_spring.commercialcreditterms select * from mdm.commercialcreditterms where is_version=false and deleted=false;
update mdm_spring.commercialcreditterms
set businessthirdparty_commerciallimit_id = (select ref.uuid from mdm.businessthirdparty_commerciallimit ref where ref.id = businessthirdparty_commerciallimit_id::bigint)
where businessthirdparty_commerciallimit_id is not null;
update mdm_spring.commercialcreditterms
set commercialcredittermsfromtype_id = (select ref.uuid from mdm.commercialcredittermsfromtype ref where ref.id = commercialcredittermsfromtype_id::bigint)
where commercialcredittermsfromtype_id is not null;
alter table mdm_spring.commercialcreditterms
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
