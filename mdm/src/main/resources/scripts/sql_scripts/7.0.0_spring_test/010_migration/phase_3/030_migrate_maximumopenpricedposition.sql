alter table mdm_spring.maximumopenpricedposition
   add constraint fk_businessthirdparty_commerciallimit
   foreign key (businessthirdparty_commerciallimit_id)
       references mdm_spring.businessthirdparty_commerciallimit(id);
drop index if exists idx_maximumopenpricedposition_businessthirdparty_commerciallimit_id cascade;
create index idx_maximumopenpricedposition_businessthirdparty_commerciallimit_id on mdm_spring.maximumopenpricedposition (businessthirdparty_commerciallimit_id);
alter table mdm_spring.maximumopenpricedposition
   add constraint fk_quantityunit
   foreign key (quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_maximumopenpricedposition_quantityunit_id cascade;
create index idx_maximumopenpricedposition_quantityunit_id on mdm_spring.maximumopenpricedposition (quantityunit_id);
