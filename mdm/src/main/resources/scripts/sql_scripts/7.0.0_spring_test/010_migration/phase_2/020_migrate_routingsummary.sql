alter table mdm_spring.routingsummary drop column id;
alter table mdm_spring.routingsummary rename column uuid to id;
alter table mdm_spring.routingsummary add primary key (id);
