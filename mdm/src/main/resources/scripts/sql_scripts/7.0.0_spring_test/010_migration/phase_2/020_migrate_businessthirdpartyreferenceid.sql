alter table mdm_spring.businessthirdpartyreferenceid drop column id;
alter table mdm_spring.businessthirdpartyreferenceid rename column uuid to id;
alter table mdm_spring.businessthirdpartyreferenceid add primary key (id);
