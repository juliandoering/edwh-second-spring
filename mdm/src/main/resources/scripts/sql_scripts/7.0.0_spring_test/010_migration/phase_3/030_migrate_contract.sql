alter table mdm_spring.contract
   add constraint fk_samplehandling
   foreign key (samplehandling_id)
       references mdm_spring.samplehandling(id);
drop index if exists idx_contract_samplehandling_id cascade;
create index idx_contract_samplehandling_id on mdm_spring.contract (samplehandling_id);
alter table mdm_spring.contract
   add constraint fk_agent
   foreign key (agent_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_contract_agent_id cascade;
create index idx_contract_agent_id on mdm_spring.contract (agent_id);
alter table mdm_spring.contract
   add constraint fk_pricefixationtype
   foreign key (pricefixationtype_id)
       references mdm_spring.pricefixationtype(id);
drop index if exists idx_contract_pricefixationtype_id cascade;
create index idx_contract_pricefixationtype_id on mdm_spring.contract (pricefixationtype_id);
alter table mdm_spring.contract
   add constraint fk_seller_third_party
   foreign key (seller_third_party_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_contract_seller_third_party_id cascade;
create index idx_contract_seller_third_party_id on mdm_spring.contract (seller_third_party_id);
alter table mdm_spring.contract
   add constraint fk_quantityunit
   foreign key (quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_contract_quantityunit_id cascade;
create index idx_contract_quantityunit_id on mdm_spring.contract (quantityunit_id);
alter table mdm_spring.contract
   add constraint fk_incoterm
   foreign key (incoterm_id)
       references mdm_spring.incoterm(id);
drop index if exists idx_contract_incoterm_id cascade;
create index idx_contract_incoterm_id on mdm_spring.contract (incoterm_id);
alter table mdm_spring.contract
   add constraint fk_shipper_third_party
   foreign key (shipper_third_party_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_contract_shipper_third_party_id cascade;
create index idx_contract_shipper_third_party_id on mdm_spring.contract (shipper_third_party_id);
alter table mdm_spring.contract
   add constraint fk_futuremonth
   foreign key (futuremonth_id)
       references mdm_spring.futuremonth(id);
drop index if exists idx_contract_futuremonth_id cascade;
create index idx_contract_futuremonth_id on mdm_spring.contract (futuremonth_id);
alter table mdm_spring.contract
   add constraint fk_price_quantityunit
   foreign key (price_quantityunit_id)
       references mdm_spring.quantityunit(id);
drop index if exists idx_contract_price_quantityunit_id cascade;
create index idx_contract_price_quantityunit_id on mdm_spring.contract (price_quantityunit_id);
alter table mdm_spring.contract
   add constraint fk_coffeecontracttype
   foreign key (coffeecontracttype_id)
       references mdm_spring.coffeecontracttype(id);
drop index if exists idx_contract_coffeecontracttype_id cascade;
create index idx_contract_coffeecontracttype_id on mdm_spring.contract (coffeecontracttype_id);
alter table mdm_spring.contract
   add constraint fk_buyer_third_party
   foreign key (buyer_third_party_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_contract_buyer_third_party_id cascade;
create index idx_contract_buyer_third_party_id on mdm_spring.contract (buyer_third_party_id);
alter table mdm_spring.contract
   add constraint fk_buyer
   foreign key (buyer_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_contract_buyer_id cascade;
create index idx_contract_buyer_id on mdm_spring.contract (buyer_id);
alter table mdm_spring.contract
   add constraint fk_coffeeweightscode
   foreign key (coffeeweightscode_id)
       references mdm_spring.coffeeweightscode(id);
drop index if exists idx_contract_coffeeweightscode_id cascade;
create index idx_contract_coffeeweightscode_id on mdm_spring.contract (coffeeweightscode_id);
alter table mdm_spring.contract
   add constraint fk_agent_third_party
   foreign key (agent_third_party_id)
       references mdm_spring.businessthirdparty(id);
drop index if exists idx_contract_agent_third_party_id cascade;
create index idx_contract_agent_third_party_id on mdm_spring.contract (agent_third_party_id);
alter table mdm_spring.contract
   add constraint fk_positionofsale
   foreign key (positionofsale_id)
       references mdm_spring.positionofsale(id);
drop index if exists idx_contract_positionofsale_id cascade;
create index idx_contract_positionofsale_id on mdm_spring.contract (positionofsale_id);
alter table mdm_spring.contract
   add constraint fk_packing
   foreign key (packing_id)
       references mdm_spring.packing(id);
drop index if exists idx_contract_packing_id cascade;
create index idx_contract_packing_id on mdm_spring.contract (packing_id);
alter table mdm_spring.contract
   add constraint fk_futuremarket
   foreign key (futuremarket_id)
       references mdm_spring.futuremarket(id);
drop index if exists idx_contract_futuremarket_id cascade;
create index idx_contract_futuremarket_id on mdm_spring.contract (futuremarket_id);
alter table mdm_spring.contract
   add constraint fk_shipper
   foreign key (shipper_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_contract_shipper_id cascade;
create index idx_contract_shipper_id on mdm_spring.contract (shipper_id);
alter table mdm_spring.contract
   add constraint fk_coffeequality
   foreign key (coffeequality_id)
       references mdm_spring.coffeequality(id);
drop index if exists idx_contract_coffeequality_id cascade;
create index idx_contract_coffeequality_id on mdm_spring.contract (coffeequality_id);
alter table mdm_spring.contract
   add constraint fk_instr_info_location
   foreign key (instr_info_location_id)
       references mdm_spring.location(id);
drop index if exists idx_contract_instr_info_location_id cascade;
create index idx_contract_instr_info_location_id on mdm_spring.contract (instr_info_location_id);
alter table mdm_spring.contract
   add constraint fk_paymentterm
   foreign key (paymentterm_id)
       references mdm_spring.paymentterm(id);
drop index if exists idx_contract_paymentterm_id cascade;
create index idx_contract_paymentterm_id on mdm_spring.contract (paymentterm_id);
alter table mdm_spring.contract
   add constraint fk_fixationoption
   foreign key (fixationoption_id)
       references mdm_spring.fixationoption(id);
drop index if exists idx_contract_fixationoption_id cascade;
create index idx_contract_fixationoption_id on mdm_spring.contract (fixationoption_id);
alter table mdm_spring.contract
   add constraint fk_currency
   foreign key (currency_id)
       references mdm_spring.currency(id);
drop index if exists idx_contract_currency_id cascade;
create index idx_contract_currency_id on mdm_spring.contract (currency_id);
alter table mdm_spring.contract
   add constraint fk_seller
   foreign key (seller_id)
       references mdm_spring.groupcompany(id);
drop index if exists idx_contract_seller_id cascade;
create index idx_contract_seller_id on mdm_spring.contract (seller_id);
