create table mdm_spring.businessthirdpartyreferenceid as table mdm.businessthirdpartyreferenceid with no data;
alter table mdm_spring.businessthirdpartyreferenceid alter column supplier_groupcompany_id type varchar;
alter table mdm_spring.businessthirdpartyreferenceid alter column businessthirdpartyreferenceidtype_id type varchar;
alter table mdm_spring.businessthirdpartyreferenceid alter column supplier_businessthirdparty_id type varchar;
alter table mdm_spring.businessthirdpartyreferenceid alter column parent_businessthirdparty_id type varchar;
insert into mdm_spring.businessthirdpartyreferenceid select * from mdm.businessthirdpartyreferenceid where is_version=false and deleted=false;
update mdm_spring.businessthirdpartyreferenceid
set supplier_groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = supplier_groupcompany_id::bigint)
where supplier_groupcompany_id is not null;
update mdm_spring.businessthirdpartyreferenceid
set businessthirdpartyreferenceidtype_id = (select ref.uuid from mdm.businessthirdpartyreferenceidtype ref where ref.id = businessthirdpartyreferenceidtype_id::bigint)
where businessthirdpartyreferenceidtype_id is not null;
update mdm_spring.businessthirdpartyreferenceid
set supplier_businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = supplier_businessthirdparty_id::bigint)
where supplier_businessthirdparty_id is not null;
update mdm_spring.businessthirdpartyreferenceid
set parent_businessthirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = parent_businessthirdparty_id::bigint)
where parent_businessthirdparty_id is not null;
alter table mdm_spring.businessthirdpartyreferenceid
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
