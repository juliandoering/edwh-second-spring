create table mdm_spring.thirdpartytype as table mdm.thirdpartytype with no data;
insert into mdm_spring.thirdpartytype select * from mdm.thirdpartytype where is_version=false and deleted=false;
alter table mdm_spring.thirdpartytype
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
