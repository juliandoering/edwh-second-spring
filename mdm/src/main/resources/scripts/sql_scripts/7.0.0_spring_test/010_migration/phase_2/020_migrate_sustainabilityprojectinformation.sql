alter table mdm_spring.sustainabilityprojectinformation drop column id;
alter table mdm_spring.sustainabilityprojectinformation rename column uuid to id;
alter table mdm_spring.sustainabilityprojectinformation add primary key (id);
