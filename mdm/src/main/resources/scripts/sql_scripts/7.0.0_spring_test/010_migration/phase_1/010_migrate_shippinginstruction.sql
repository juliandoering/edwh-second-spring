create table mdm_spring.shippinginstruction as table mdm.shippinginstruction with no data;
alter table mdm_spring.shippinginstruction alter column shippingline_id type varchar;
alter table mdm_spring.shippinginstruction alter column typeofshipment_id type varchar;
alter table mdm_spring.shippinginstruction alter column modeoftransport_id type varchar;
alter table mdm_spring.shippinginstruction alter column placeofreceipt_id type varchar;
alter table mdm_spring.shippinginstruction alter column quantityunit_id type varchar;
alter table mdm_spring.shippinginstruction alter column contract_id type varchar;
alter table mdm_spring.shippinginstruction alter column freightpaymentterm_id type varchar;
alter table mdm_spring.shippinginstruction alter column portofloading_id type varchar;
alter table mdm_spring.shippinginstruction alter column portofdischarge_id type varchar;
alter table mdm_spring.shippinginstruction alter column containermovement_id type varchar;
alter table mdm_spring.shippinginstruction alter column containersize_id type varchar;
alter table mdm_spring.shippinginstruction alter column freightpayableby_groupcompany_id type varchar;
alter table mdm_spring.shippinginstruction alter column freightpayableby_thirdparty_id type varchar;
alter table mdm_spring.shippinginstruction alter column packing_id type varchar;
alter table mdm_spring.shippinginstruction alter column placeofdelivery_id type varchar;
insert into mdm_spring.shippinginstruction select * from mdm.shippinginstruction where is_version=false and deleted=false;
update mdm_spring.shippinginstruction
set shippingline_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = shippingline_id::bigint)
where shippingline_id is not null;
update mdm_spring.shippinginstruction
set typeofshipment_id = (select ref.uuid from mdm.typeofshipment ref where ref.id = typeofshipment_id::bigint)
where typeofshipment_id is not null;
update mdm_spring.shippinginstruction
set modeoftransport_id = (select ref.uuid from mdm.modeoftransport ref where ref.id = modeoftransport_id::bigint)
where modeoftransport_id is not null;
update mdm_spring.shippinginstruction
set placeofreceipt_id = (select ref.uuid from mdm.location ref where ref.id = placeofreceipt_id::bigint)
where placeofreceipt_id is not null;
update mdm_spring.shippinginstruction
set quantityunit_id = (select ref.uuid from mdm.quantityunit ref where ref.id = quantityunit_id::bigint)
where quantityunit_id is not null;
update mdm_spring.shippinginstruction
set contract_id = (select ref.uuid from mdm.contract ref where ref.id = contract_id::bigint)
where contract_id is not null;
update mdm_spring.shippinginstruction
set freightpaymentterm_id = (select ref.uuid from mdm.freightpaymentterm ref where ref.id = freightpaymentterm_id::bigint)
where freightpaymentterm_id is not null;
update mdm_spring.shippinginstruction
set portofloading_id = (select ref.uuid from mdm.location ref where ref.id = portofloading_id::bigint)
where portofloading_id is not null;
update mdm_spring.shippinginstruction
set portofdischarge_id = (select ref.uuid from mdm.location ref where ref.id = portofdischarge_id::bigint)
where portofdischarge_id is not null;
update mdm_spring.shippinginstruction
set containermovement_id = (select ref.uuid from mdm.containermovement ref where ref.id = containermovement_id::bigint)
where containermovement_id is not null;
update mdm_spring.shippinginstruction
set containersize_id = (select ref.uuid from mdm.containersize ref where ref.id = containersize_id::bigint)
where containersize_id is not null;
update mdm_spring.shippinginstruction
set freightpayableby_groupcompany_id = (select ref.uuid from mdm.groupcompany ref where ref.id = freightpayableby_groupcompany_id::bigint)
where freightpayableby_groupcompany_id is not null;
update mdm_spring.shippinginstruction
set freightpayableby_thirdparty_id = (select ref.uuid from mdm.businessthirdparty ref where ref.id = freightpayableby_thirdparty_id::bigint)
where freightpayableby_thirdparty_id is not null;
update mdm_spring.shippinginstruction
set packing_id = (select ref.uuid from mdm.packing ref where ref.id = packing_id::bigint)
where packing_id is not null;
update mdm_spring.shippinginstruction
set placeofdelivery_id = (select ref.uuid from mdm.location ref where ref.id = placeofdelivery_id::bigint)
where placeofdelivery_id is not null;
alter table mdm_spring.shippinginstruction
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
