alter table mdm_spring.email drop column id;
alter table mdm_spring.email rename column uuid to id;
alter table mdm_spring.email add primary key (id);
