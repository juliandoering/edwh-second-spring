create table mdm_spring.typeofshipment as table mdm.typeofshipment with no data;
insert into mdm_spring.typeofshipment select * from mdm.typeofshipment where is_version=false and deleted=false;
alter table mdm_spring.typeofshipment
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
