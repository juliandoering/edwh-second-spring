alter table mdm_spring.firstnoticedays
   add constraint fk_futuresmarket
   foreign key (futuresmarket_id)
       references mdm_spring.futuremarket(id);
drop index if exists idx_firstnoticedays_futuresmarket_id cascade;
create index idx_firstnoticedays_futuresmarket_id on mdm_spring.firstnoticedays (futuresmarket_id);
