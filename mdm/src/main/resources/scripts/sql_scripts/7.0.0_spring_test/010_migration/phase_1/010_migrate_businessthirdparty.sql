create table mdm_spring.businessthirdparty as table mdm.businessthirdparty with no data;
alter table mdm_spring.businessthirdparty alter column incoterm_id type varchar;
alter table mdm_spring.businessthirdparty alter column businessorganisationtype_id type varchar;
alter table mdm_spring.businessthirdparty alter column presentation_of_documents_id type varchar;
alter table mdm_spring.businessthirdparty alter column paymentterm_id type varchar;
alter table mdm_spring.businessthirdparty alter column legal_country_id type varchar;
insert into mdm_spring.businessthirdparty select * from mdm.businessthirdparty where is_version=false and deleted=false;
update mdm_spring.businessthirdparty
set incoterm_id = (select ref.uuid from mdm.incoterm ref where ref.id = incoterm_id::bigint)
where incoterm_id is not null;
update mdm_spring.businessthirdparty
set businessorganisationtype_id = (select ref.uuid from mdm.businessorganisationtype ref where ref.id = businessorganisationtype_id::bigint)
where businessorganisationtype_id is not null;
update mdm_spring.businessthirdparty
set presentation_of_documents_id = (select ref.uuid from mdm.presentation_of_documents ref where ref.id = presentation_of_documents_id::bigint)
where presentation_of_documents_id is not null;
update mdm_spring.businessthirdparty
set paymentterm_id = (select ref.uuid from mdm.paymentterm ref where ref.id = paymentterm_id::bigint)
where paymentterm_id is not null;
update mdm_spring.businessthirdparty
set legal_country_id = (select ref.uuid from mdm.location ref where ref.id = legal_country_id::bigint)
where legal_country_id is not null;
alter table mdm_spring.businessthirdparty
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
