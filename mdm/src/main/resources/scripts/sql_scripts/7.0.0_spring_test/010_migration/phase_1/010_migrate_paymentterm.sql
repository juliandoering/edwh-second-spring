create table mdm_spring.paymentterm as table mdm.paymentterm with no data;
insert into mdm_spring.paymentterm select * from mdm.paymentterm where is_version=false and deleted=false;
alter table mdm_spring.paymentterm
   drop column if exists is_version,
   drop column if exists parent,
   drop column if exists record_version,
   drop column if exists deleted;
