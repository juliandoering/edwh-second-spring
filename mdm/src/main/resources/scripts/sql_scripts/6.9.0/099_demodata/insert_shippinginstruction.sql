INSERT INTO shippinginstruction ( UUID, contract_id, shippinginstruction_number, portofloading_id, placeofdelivery_id, quantity, containerquantity, shipmentperiod_fromdate, shipmentperiod_todate, serviceagreementnumber, shippingline_id, quantityunit_id, packing_id, containermovement_id, containersize_id, modeoftransport_id, typeofshipment_id, freightpaymentterm_id, freightpayableby_groupcompany_id )
VALUES
(
  'fd7b7862-3900-11e9-b210-d663bd873d93',
  (select id from contract where is_version is false and uuid = 'a22162a8-352f-11e9-b210-d663bd873d93'),
  2,
  (select id from location where si_unlocode = 'BRSSZ' and is_version is false),
  (select id from location where si_unlocode = 'DEHAM' and is_version is false),
  5,
  2,
  '2021-02-15'::DATE,
  '2021-03-15'::DATE,
  'serviceagreementnumber-123',
  (select id from businessthirdparty where acronym = 'HAPAG' and is_version is false),
  (select id from quantityunit where code = '69KB' and is_version is false),
  (select id from packing where code = '1MTBB' and is_version is false),
  (select id from containermovement where code = 'FCL-FCL' and is_version is false),
  (select id from containersize where code = '40FTDC' and is_version is false),
  (select id from modeoftransport where code = 'Port to Door' and is_version is false),
  (select id from typeofshipment where code = 'In Bulk' and is_version is false),
  (select id from freightpaymentterm where code = 'Prepaid' and is_version is false),
  (select id from groupcompany where tagetik_code = 'BRIC' and is_version is false)
);

INSERT INTO shippinginstruction ( UUID, contract_id, shippinginstruction_number, portofloading_id, placeofdelivery_id, quantity, containerquantity, shipmentperiod_fromdate, shipmentperiod_todate, serviceagreementnumber, shippingline_id, quantityunit_id, packing_id, containermovement_id, containersize_id, modeoftransport_id, typeofshipment_id, freightpaymentterm_id, freightpayableby_groupcompany_id )
VALUES
(
  '123cacc6-3901-11e9-b210-d663bd873d93',
  (select id from contract where is_version is false and uuid = 'a22162a8-352f-11e9-b210-d663bd873d93'),
  1,
  (select id from location where si_unlocode = 'BRRIO' and is_version is false),
  (select id from location where si_unlocode = 'BRSSZ' and is_version is false),
  6,
  1,
  '2021-01-31'::DATE,
  '2021-02-10'::DATE,
  'serviceagreementnumber-111',
  (select id from businessthirdparty where acronym = 'HAPAG' and is_version is false),
  (select id from quantityunit where code = '60KB' and is_version is false),
  (select id from packing where code = '60KGB' and is_version is false),
  (select id from containermovement where code = 'LCL-FCL' and is_version is false),
  (select id from containersize where code = '40FTDC' and is_version is false),
  (select id from modeoftransport where code = 'Port to Door' and is_version is false),
  (select id from typeofshipment where code = 'In Bags' and is_version is false),
  (select id from freightpaymentterm where code = 'Collect' and is_version is false),
  (select id from groupcompany where tagetik_code = 'BRIC' and is_version is false)
);

insert into shippinginstruction (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, contract_id, shippinginstruction_number, portofloading_id, placeofdelivery_id, quantity, containerquantity, shipmentperiod_fromdate, shipmentperiod_todate, serviceagreementnumber, shippingline_id, quantityunit_id, packing_id, containermovement_id, containersize_id, modeoftransport_id, typeofshipment_id, freightpaymentterm_id, freightpayableby_groupcompany_id)
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id,
    (select min(contract.id) from contract where contract.parent = shippinginstruction.contract_id and contract.is_version is true),
    shippinginstruction_number,
    (select min(location.id) from location where location.parent = shippinginstruction.portofloading_id and location.is_version is true),
    (select min(location.id) from location where location.parent = shippinginstruction.placeofdelivery_id and location.is_version is true),
    quantity,
    containerquantity,
    shipmentperiod_fromdate,
    shipmentperiod_todate,
    serviceagreementnumber,
    (select min(businessthirdparty.id) from businessthirdparty where businessthirdparty.parent = shippinginstruction.shippingline_id and businessthirdparty.is_version is true),
    (select min(quantityunit.id) from quantityunit where quantityunit.parent = shippinginstruction.quantityunit_id and quantityunit.is_version is true),
    (select min(packing.id) from packing where packing.parent = shippinginstruction.packing_id and packing.is_version is true),
    (select min(containermovement.id) from containermovement where containermovement.parent = shippinginstruction.containermovement_id and containermovement.is_version is true),
    (select min(containersize.id) from containersize where containersize.parent = shippinginstruction.containersize_id and containersize.is_version is true),
    (select min(modeoftransport.id) from modeoftransport where modeoftransport.parent = shippinginstruction.modeoftransport_id and modeoftransport.is_version is true),
    (select min(typeofshipment.id) from typeofshipment where typeofshipment.parent = shippinginstruction.typeofshipment_id and typeofshipment.is_version is true),
    (select min(freightpaymentterm.id) from freightpaymentterm where freightpaymentterm.parent = shippinginstruction.freightpaymentterm_id and freightpaymentterm.is_version is true),
    (select min(groupcompany.id) from groupcompany where groupcompany.parent = shippinginstruction.freightpayableby_groupcompany_id and groupcompany.is_version is true)
  from shippinginstruction
  where uuid in ('fd7b7862-3900-11e9-b210-d663bd873d93', '123cacc6-3901-11e9-b210-d663bd873d93')
  and is_version is false;
