-- correct reference
UPDATE futuremarket fm
SET lot_quantityunit_id = (SELECT qu.parent FROM quantityunit qu WHERE qu.id = fm.lot_quantityunit_id)
WHERE fm.is_version = FALSE
AND EXISTS (SELECT 1 FROM quantityunit qu WHERE qu.id = fm.lot_quantityunit_id AND qu.is_version = TRUE);
	