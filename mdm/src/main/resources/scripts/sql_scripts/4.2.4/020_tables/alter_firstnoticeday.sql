-- create a immutable version for each entry

INSERT INTO firstnoticedays
(
  uuid,
  created_at,
  updated_at,
  deleted,
  record_version,
  version,
  is_version,
  parent,
  firstnoticeday,
  terminalmonth,
  terminalyear,
  market
)
SELECT
  uuid,
  created_at,
  updated_at,
  deleted,
  record_version,
  version,
  TRUE,
  id,
  firstnoticeday,
  terminalmonth,
  terminalyear,
  market
FROM firstnoticedays fnd
WHERE fnd.is_version = FALSE
AND NOT EXISTS (SELECT 1 FROM firstnoticedays fnd2 WHERE fnd2.is_version = TRUE AND fnd2.parent = fnd.id)
;

-- change table to foreign key reference to futuresmarket

ALTER TABLE firstnoticedays ADD COLUMN futuresmarket_id BIGINT;

UPDATE firstnoticedays fnd SET futuresmarket_id = (
  SELECT max(fm.id) 
  FROM futuremarket fm 
  WHERE fm.code = fnd.market 
    AND fm.is_version = fnd.is_version
);

ALTER TABLE firstnoticedays DROP COLUMN market;
