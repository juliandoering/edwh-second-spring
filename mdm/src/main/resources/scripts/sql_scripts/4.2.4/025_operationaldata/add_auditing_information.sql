DO $$
DECLARE
    tables CURSOR FOR
        SELECT tablename
        FROM pg_tables
        WHERE schemaname = 'mdm'
        ORDER BY tablename;
    admin_id bigint;
BEGIN
    admin_id := (SELECT id FROM userprofile WHERE username = 'edwh_admin');
    FOR table_record IN tables LOOP
        EXECUTE -- do stuff here
        'UPDATE ' || table_record.tablename || ' SET created_by = ' || admin_id || '; ' ||
        'UPDATE ' || table_record.tablename || ' SET updated_by = ' || admin_id || ';';
    END LOOP;
END$$;