UPDATE coffeequality_origintype 
SET description = 'Single Origin'
WHERE code = 'SO';

UPDATE coffeequality_origintype 
SET description = 'Basket/Multiple Origin'
WHERE code = 'BMO';

UPDATE coffeequality_origintype 
SET description = 'Blend'
WHERE code = 'BL';

UPDATE coffeequality_origintype 
SET description = 'Unspecified'
WHERE code = 'U';
