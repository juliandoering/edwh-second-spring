-- owner/operator <business partner> (group company or third person)
ALTER TABLE etl.warehouse 
  ADD COLUMN businesspartner_id bigint;
