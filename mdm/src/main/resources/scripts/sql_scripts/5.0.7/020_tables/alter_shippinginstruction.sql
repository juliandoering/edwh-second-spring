-- business flieds
ALTER TABLE shippinginstruction ADD COLUMN quantity numeric;
ALTER TABLE shippinginstruction ADD COLUMN containerquantity integer;
ALTER TABLE shippinginstruction ADD COLUMN shipmentperiod_fromdate date;
ALTER TABLE shippinginstruction ADD COLUMN shipmentperiod_todate date;
ALTER TABLE shippinginstruction ADD COLUMN serviceagreementnumber varchar;

-- References
ALTER TABLE shippinginstruction ADD COLUMN shippingline_id bigint;

ALTER TABLE shippinginstruction ADD COLUMN quantityunit_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN packing_id bigint;

ALTER TABLE shippinginstruction ADD COLUMN placeofreceipt_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN placeofdischarge_id bigint;

ALTER TABLE shippinginstruction ADD COLUMN containermovement_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN containersize_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN modeoftransport_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN typeofshipment_id bigint;

ALTER TABLE shippinginstruction ADD COLUMN freightpaymentterm_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN freightpayablyby_groupcompany_id bigint;
ALTER TABLE shippinginstruction ADD COLUMN freightpayablyby_thirdparty_id bigint;



