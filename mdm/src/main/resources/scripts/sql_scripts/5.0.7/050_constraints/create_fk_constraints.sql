ALTER TABLE individual ADD CONSTRAINT fk_identificationtype FOREIGN KEY (identificationtype_id) REFERENCES identificationtype;

-- Any Other Instruction
ALTER TABLE anyotherinstruction ADD CONSTRAINT fk_anyotherinstruction_shippinginstruction FOREIGN KEY (shippinginstruction_id) REFERENCES shippinginstruction;

-- Shipping Instruction
ALTER TABLE shippinginstruction ADD CONSTRAINT fk_contract FOREIGN KEY (contract_id) REFERENCES contract;
