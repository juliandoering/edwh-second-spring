INSERT INTO containersize (uuid, code, description)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', '20FTDC', '20 feet Dry Container');
INSERT INTO containersize (uuid, code, description)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', '40FTDC', '40 feet Dry Container');
INSERT INTO containersize (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9258-4937f0a81d18', '40FTHC', '40 feet High Cube DC');

insert into containersize (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from containersize where is_version is false;