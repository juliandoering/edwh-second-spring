INSERT INTO modeoftransport (uuid, code, description)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', 'Port to Port', 'Transport service from port to port');
INSERT INTO modeoftransport (uuid, code, description)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', 'Port to Door', 'Transport service from port to door');
INSERT INTO modeoftransport (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9258-4937f0a81d18', 'Door to Port', 'Transport service from door to port');
INSERT INTO modeoftransport (uuid, code, description)
    VALUES ('613e63de-8450-4bd3-9258-4937f0a81d18', 'Door to Door', 'Transport service from door to door');

insert into modeoftransport (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from modeoftransport where is_version is false;