INSERT INTO freightpaymentterm (uuid, code, description)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', 'Collect', 'Collect');
INSERT INTO freightpaymentterm (uuid, code, description)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', 'Prepaid', 'Prepaid');

insert into freightpaymentterm (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from freightpaymentterm where is_version is false;