INSERT INTO containermovement (uuid, code, description)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', 'LCL-FCL', 'One container packed at multiple sites and delivered to one recipient');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', 'LCL-LCL', 'Less than one container');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9258-4937f0a81d18', 'FCL-LCL', 'One container to one recipient with different parcels from one shipper to multiple recipients');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9268-4937f0a81d18', 'FCL-FCL', 'A full container load sent from one shipper to one recipient');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9278-4937f0a81d18', 'CFS-CY', 'One container packed at multiple sites and delivered to one recipien (US)');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9288-4937f0a81d18', 'CFS-CFS', 'Less than one container (US)');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9298-4937f0a81d18', 'CY-CFS', 'One container to one recipient with different parcels from one shipper to multiple recipients (US)');
INSERT INTO containermovement (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-92a8-4937f0a81d18', 'CY-CY', 'A full container load sent from one shipper to one recipient (US)');

insert into containermovement (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from containermovement where is_version is false;