INSERT INTO typeofshipment (uuid, code, description)
    VALUES ('64b45ef6-f600-4503-bbd0-d041c8fdd07b', 'In Bags', 'Shipped in bags (no big bags)');
INSERT INTO typeofshipment (uuid, code, description)
    VALUES ('6fb23ea9-f3b1-417c-8a04-c884d1fd94ef', 'In Bulk', 'Shipped in bulk');
INSERT INTO typeofshipment (uuid, code, description)
    VALUES ('613e63de-8450-4bd2-9258-4937f0a81d18', 'In Big Bags', 'Shipped in big bags');

insert into typeofshipment (uuid, version, is_version, parent, code, description) 
  select uuid, version, true, id, code, description from typeofshipment where is_version is false;