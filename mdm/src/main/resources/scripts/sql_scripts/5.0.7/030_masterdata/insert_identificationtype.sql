INSERT INTO identificationtype(uuid, code, description, created_by, updated_by) 
VALUES('c156d3e0-d40d-4bbc-8704-f67dc439a34a', 'PP', 'Passport',
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE));
 
INSERT INTO identificationtype(uuid, code, description, created_by, updated_by) 
VALUES('a775143d-db0f-4533-b965-239e58ae43d3', 'IC', 'Identification Card',
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE));
 
INSERT INTO identificationtype(uuid, code, description, created_by, updated_by) 
VALUES('842a3e8a-b8ac-4936-8b4c-d5f6aeb9de09', 'NIC', 'Unique Identification Number',
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE),
 (SELECT id from userprofile WHERE username = 'edhw_admin' AND deleted IS FALSE));

insert into identificationtype (uuid, created_at, created_by, updated_at, updated_by, version, is_version, parent, code, description) 
  select uuid, created_at, created_by, updated_at, updated_by, version, true, id, code, description from identificationtype where is_version is false;
