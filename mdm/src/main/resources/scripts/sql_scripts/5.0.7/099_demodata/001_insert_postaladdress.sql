INSERT INTO postaladdress (
	uuid, street, postal_code, location_id
) VALUES (
	'1214041', 'Am Sandtorpark 4', '20457', (SELECT id FROM location WHERE country_code = 'DE' AND location_code = 'HAM' AND is_version is false)
);

insert into postaladdress (uuid, version, is_version, parent, street, house_number, postal_code, location_id)
  select uuid, version, true, id, street, house_number, postal_code,
        (SELECT location.id FROM location WHERE location.parent = postaladdress.location_id AND location.is_version is true)
  from postaladdress where is_version is false;