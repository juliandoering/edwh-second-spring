package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.compliancerating.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "complianceratings", path = "complianceratings")
public interface ComplianceRatingRepository extends BaseRepository<ComplianceRatingEntity> {
}
