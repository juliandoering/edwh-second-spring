package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "businessthirdpartyassociations", path = "businessthirdpartyassociations")
public interface BusinessThirdPartyIndividualAssociationRepository
        extends BaseRepository<BusinessThirdPartyIndividualAssociationEntity> {
}
