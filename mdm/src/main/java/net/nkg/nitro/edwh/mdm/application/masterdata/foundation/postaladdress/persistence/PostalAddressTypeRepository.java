package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "postaladdresstypes", path = "postaladdresstypes")
public interface PostalAddressTypeRepository extends ReferenceDataRepository<PostalAddressTypeEntity> {
}
