package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence.IndividualEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.persistence.ContactEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence.PostalAddressEntity;
import net.nkg.nitro.edwh.model.foundation.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "groupcompany")
@Audited
public class GroupCompanyEntity extends BaseEntity<GroupCompanyEntity> implements GroupCompany {

    @NotNull
    @OneToOne(targetEntity = PostalAddressEntity.class)
    @JoinColumn(name = "postal_address_id", referencedColumnName = "id")
    private PostalAddress postalAddress;

    @ManyToMany(targetEntity = IndividualEntity.class)
    @JoinTable(
            name = "groupcompany_individual",
            joinColumns = @JoinColumn(name = "individual_id"),
            inverseJoinColumns = @JoinColumn(name = "groupcompany_id")
    )
    private List<Individual> managers;

    @OneToMany(targetEntity = ContactEntity.class, mappedBy = "groupCompany")
    private List<Contact> contacts;

    @NotBlank
    private String legalName;

    private String acronym;

    @NotBlank
    private String tagetikCode;

    @OneToOne(targetEntity = BusinessFunctionEntity.class)
    @JoinColumn(name = "businessfunction_id", referencedColumnName = "id")
    private BusinessFunction businessFunction;

    @Override
    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    @Override
    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    @Override
    public List<Individual> getManagers() {
        return managers;
    }

    @Override
    public void setManagers(List<Individual> managers) {
        this.managers = managers;
    }

    @Override
    public List<Contact> getContacts() {
        return contacts;
    }

    @Override
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String getLegalName() {
        return legalName;
    }

    @Override
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    @Override
    public String getAcronym() {
        return acronym;
    }

    @Override
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @Override
    public String getTagetikCode() {
        return tagetikCode;
    }

    @Override
    public void setTagetikCode(String tagetikCode) {
        this.tagetikCode = tagetikCode;
    }

    @Override
    public BusinessFunction getBusinessFunction() {
        return businessFunction;
    }

    @Override
    public void setBusinessFunction(BusinessFunction businessFunction) {
        this.businessFunction = businessFunction;
    }
}
