package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.lifecycle;

import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyRepository;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.service.ReferenceIdService;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.taxid.service.TaxIdService;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence.IndividualEntity;
import net.nkg.nitro.edwh.model.foundation.Individual;
import net.nkg.nitro.edwh.model.trade.businesspartner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@RepositoryEventHandler
public class SoleProprietorshipEventHandler {

    private final BusinessThirdPartyRepository businessThirdPartyRepository;
    private final TaxIdService taxIdService;
    private final ReferenceIdService referenceIdService;

    @Autowired
    public SoleProprietorshipEventHandler(BusinessThirdPartyRepository businessThirdPartyRepository,
                                          TaxIdService taxIdService,
                                          ReferenceIdService referenceIdService) {
        this.businessThirdPartyRepository = businessThirdPartyRepository;
        this.taxIdService = taxIdService;
        this.referenceIdService = referenceIdService;
    }

    @HandleAfterCreate
    public void handleBusinessThirdPartyCreated(BusinessThirdPartyEntity businessThirdParty) {
        if (businessThirdParty.isSoleProprietorship()) {
            businessThirdParty.getIndividualAssociations().stream()
                    .filter(a -> a.getRelation() == BusinessThirdPartyIndividualAssociation.Relation.OWNER)
                    .findFirst()
                    .map(BusinessThirdPartyIndividualAssociation::getIndividual)
                    .ifPresent(owner -> handleSoleProprietorship(businessThirdParty, owner));
        }
    }

    @HandleAfterSave
    public void handleIndividualUpdated(IndividualEntity individual) {
        individual.getBusinessThirdPartyAssociations().stream()
                .filter(a -> a.getRelation() == BusinessThirdPartyIndividualAssociation.Relation.OWNER)
                .map(BusinessThirdPartyIndividualAssociation::getBusinessThirdParty)
                .filter(BusinessThirdParty::isSoleProprietorship)
                .forEach(businessThirdParty -> handleSoleProprietorship(businessThirdParty, individual));
    }

    private void handleSoleProprietorship(BusinessThirdParty businessThirdParty, Individual owner) {
        businessThirdParty.setIdentificationCounter(owner.getIdentificationCounter());
        businessThirdParty.setLegalName(String.join(" ", owner.getFirstName(), owner.getLastName()));
        businessThirdParty.setLegalCountry(owner.getCitizenship());
        businessThirdParty.setTaxIds(getOwnerTaxIds(owner, businessThirdParty));
        businessThirdParty.setBusinessThirdPartyReferenceIds(getReferenceIds(businessThirdParty, owner));
        businessThirdPartyRepository.save((BusinessThirdPartyEntity) businessThirdParty);
    }

    private List<TaxId> getOwnerTaxIds(Individual owner, BusinessThirdParty businessThirdParty) {
        return Collections.singletonList(taxIdService.createIfNotExists(businessThirdParty, owner.getTaxNumber()));
    }

    private List<BusinessThirdPartyReferenceId> getReferenceIds(BusinessThirdParty businessThirdParty, Individual owner) {
        var hasPersonalId = businessThirdParty.getBusinessThirdPartyReferenceIds().stream()
                .anyMatch(BusinessThirdPartyReferenceId::isPersonalIdentificationNumber);
        if (!hasPersonalId) {
            var refIds = businessThirdParty.getBusinessThirdPartyReferenceIds();
            refIds.add(referenceIdService.createPersonalIdForIndividual(owner));
            return refIds;
        }
        return businessThirdParty.getBusinessThirdPartyReferenceIds();
    }

}
