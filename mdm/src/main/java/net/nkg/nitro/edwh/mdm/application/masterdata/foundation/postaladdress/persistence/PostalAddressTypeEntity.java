package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.PostalAddressType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "postaladdresstype")
@Audited
public class PostalAddressTypeEntity extends ReferenceDataEntity<PostalAddressTypeEntity> implements PostalAddressType {
}
