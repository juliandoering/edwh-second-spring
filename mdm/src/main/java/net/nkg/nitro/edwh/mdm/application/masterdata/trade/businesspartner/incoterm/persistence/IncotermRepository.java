package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.incoterm.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "incoterms", path = "incoterms")
public interface IncotermRepository extends ReferenceDataRepository<IncotermEntity> {
}
