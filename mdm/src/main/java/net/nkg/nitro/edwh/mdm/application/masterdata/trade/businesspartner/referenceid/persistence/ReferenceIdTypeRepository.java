package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "referenceidtypes", path = "referenceidtypes")
public interface ReferenceIdTypeRepository extends ReferenceDataRepository<ReferenceIdTypeEntity> {
}
