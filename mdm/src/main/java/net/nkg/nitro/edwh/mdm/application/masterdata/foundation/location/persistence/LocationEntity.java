package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.LocationFunction;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "location")
@Audited
public class LocationEntity extends BaseEntity<LocationEntity> implements Location {

    @Column(name = "si_unlocode")
    private String unLocode;

    private String locationCode;

    @NotBlank
    private String countryCode;

    private String name;

    private String latitude;

    private String longitude;

    @OneToMany(targetEntity = LocationFunctionEntity.class, mappedBy = "location")
    private List<LocationFunction> functions;

    @Column(name = "si_locationfunctions")
    private String locationFunctionsSearchIndex;

    @Override
    public String getUnLocode() {
        return unLocode;
    }

    public void setUnLocode(String unLocode) {
        this.unLocode = unLocode;
    }

    @Override
    public String getLocationCode() {
        return locationCode;
    }

    @Override
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
        updateUnLocodeSearchIndex();
    }

    @Override
    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        updateUnLocodeSearchIndex();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLatitude() {
        return latitude;
    }

    @Override
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Override
    public String getLongitude() {
        return longitude;
    }

    @Override
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public List<LocationFunction> getFunctions() {
        return functions;
    }

    @Override
    public void setFunctions(List<LocationFunction> functions) {
        this.functions = functions;
        updateLocationFunctionsSearchIndex();
    }

    public String getLocationFunctionsSearchIndex() {
        return locationFunctionsSearchIndex;
    }

    public void setLocationFunctionsSearchIndex(String locationFunctionsSearchIndex) {
        this.locationFunctionsSearchIndex = locationFunctionsSearchIndex;
    }

    private void updateLocationFunctionsSearchIndex() {
        locationFunctionsSearchIndex = functions.stream()
                .map(f -> f.getFunctionType() != null ? f.getFunctionType().name() : "")
                .sorted()
                .reduce((r, n) -> String.join(", ", r, n))
                .orElse("");
    }

    private void updateUnLocodeSearchIndex() {
        if (locationCode == null) {
            setUnLocode(countryCode);
        }
        setUnLocode(countryCode.concat(locationCode));
    }
}
