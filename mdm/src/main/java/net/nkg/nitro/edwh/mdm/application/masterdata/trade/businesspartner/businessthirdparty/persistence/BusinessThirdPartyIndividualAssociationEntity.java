package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence.IndividualEntity;
import net.nkg.nitro.edwh.model.foundation.Individual;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyIndividualAssociation;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema = "mdm_spring", name = "businessthirdparty_individual")
@Audited
public class BusinessThirdPartyIndividualAssociationEntity
        extends BaseEntity<BusinessThirdPartyIndividualAssociationEntity>
        implements BusinessThirdPartyIndividualAssociation {

    @NotNull
    @Enumerated(EnumType.STRING)
    private BusinessThirdPartyIndividualAssociation.Relation relation;

    @NotNull
    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotNull
    @ManyToOne(targetEntity = IndividualEntity.class)
    @JoinColumn(name = "individual_id", referencedColumnName = "id")
    private Individual individual;

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }
}
