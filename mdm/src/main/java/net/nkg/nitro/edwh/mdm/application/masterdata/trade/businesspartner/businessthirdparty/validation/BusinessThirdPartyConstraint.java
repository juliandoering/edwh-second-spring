package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BusinessThirdPartyValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface BusinessThirdPartyConstraint {
    String message() default "Invalid Business Third Party";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
