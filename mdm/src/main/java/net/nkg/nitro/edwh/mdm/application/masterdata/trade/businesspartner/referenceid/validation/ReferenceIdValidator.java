package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.validation;

import net.nkg.nitro.edwh.mdm.application.base.validation.BusinessPartnerValidator;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence.ReferenceIdEntity;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationErrorMessage;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ReferenceIdValidator
        implements ConstraintValidator<ReferenceIdConstraint, ReferenceIdEntity> {

    @Override
    public void initialize(ReferenceIdConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(ReferenceIdEntity referenceIdEntity,
                           ConstraintValidatorContext constraintValidatorContext) {
        var message = ValidationErrorMessage.fromContext(constraintValidatorContext);

        if (referenceIdEntity.getTypeOfReferenceId().getIdSupplierMandatory()) {
            if (!BusinessPartnerValidator.isValid(
                    referenceIdEntity.getSupplierBusinessThirdParty(),
                    referenceIdEntity.getSupplierGroupCompany())) {
                message.addMessageKey("btp.reference-id.no-supplier");
                return false;
            }
        }
        return true;
    }

}
