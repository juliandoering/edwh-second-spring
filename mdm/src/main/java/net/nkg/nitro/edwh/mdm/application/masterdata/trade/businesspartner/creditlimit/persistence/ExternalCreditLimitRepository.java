package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.creditlimit.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "externalcreditlimits", path = "externalcreditlimits")
public interface ExternalCreditLimitRepository extends BaseRepository<ExternalCreditLimitEntity> {
}
