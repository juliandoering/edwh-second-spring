package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ReferenceIdValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ReferenceIdConstraint {
    String message() default "Invalid Reference Id";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
