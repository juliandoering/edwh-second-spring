package net.nkg.nitro.edwh.mdm.application.analyticaldata.hedging;

import net.nkg.nitro.edwh.mdm.application.base.AnalyticalDataEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence.GroupCompanyEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.hedging.future.FutureMarketEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.hedging.future.FutureMonthEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.trade.TradeTypeEntity;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.hedging.FutureMarket;
import net.nkg.nitro.edwh.model.hedging.FutureMonth;
import net.nkg.nitro.edwh.model.hedging.TerminalMarketOptionTrade;
import net.nkg.nitro.edwh.model.trade.TradeType;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(schema = "dwh", name = "terminalmarketoptiontrade")
public class TerminalMarketOptionTradeEntity extends AnalyticalDataEntity<TerminalMarketOptionTradeEntity> implements TerminalMarketOptionTrade {

    private String tmtId;

    private String tradeNumber;

    private LocalDate tradeDate;

    private LocalDate expirationDate;

    private BigDecimal tradePrice;

    private BigDecimal strikePrice;

    private Integer tradeQuantity;

    private Integer futuresYear;

    private boolean excercized;

    @OneToOne(targetEntity = FutureMarketEntity.class)
    @JoinColumn(name = "futures_market_id", referencedColumnName = "id")
    private FutureMarket futuresMarket;

    @OneToOne(targetEntity = TradeTypeEntity.class)
    @JoinColumn(name = "trade_type_id", referencedColumnName = "id")
    private TradeType tradeType;

    private String optionTradeCategory;

    @OneToOne(targetEntity = FutureMonthEntity.class)
    @JoinColumn(name = "futures_month_id", referencedColumnName = "id")
    private FutureMonth futuresMonth;

    @OneToOne(targetEntity = GroupCompanyEntity.class)
    @JoinColumn(name = "groupcompany_id", referencedColumnName = "id")
    private GroupCompany groupCompany;

    @Override
    public String getTmtId() {
        return tmtId;
    }

    @Override
    public void setTmtId(String tmtId) {
        this.tmtId = tmtId;
    }

    @Override
    public String getTradeNumber() {
        return tradeNumber;
    }

    @Override
    public void setTradeNumber(String tradeNumber) {
        this.tradeNumber = tradeNumber;
    }

    @Override
    public LocalDate getTradeDate() {
        return tradeDate;
    }

    @Override
    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    @Override
    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    @Override
    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public BigDecimal getTradePrice() {
        return tradePrice;
    }

    @Override
    public void setTradePrice(BigDecimal tradePrice) {
        this.tradePrice = tradePrice;
    }

    @Override
    public BigDecimal getStrikePrice() {
        return strikePrice;
    }

    @Override
    public void setStrikePrice(BigDecimal strikePrice) {
        this.strikePrice = strikePrice;
    }

    @Override
    public Integer getTradeQuantity() {
        return tradeQuantity;
    }

    @Override
    public void setTradeQuantity(Integer tradeQuantity) {
        this.tradeQuantity = tradeQuantity;
    }

    @Override
    public Integer getFuturesYear() {
        return futuresYear;
    }

    @Override
    public void setFuturesYear(Integer futuresYear) {
        this.futuresYear = futuresYear;
    }

    @Override
    public boolean isExcercized() {
        return excercized;
    }

    @Override
    public void setExcercized(boolean excercized) {
        this.excercized = excercized;
    }

    @Override
    public FutureMarket getFuturesMarket() {
        return futuresMarket;
    }

    @Override
    public void setFuturesMarket(FutureMarket futuresMarket) {
        this.futuresMarket = futuresMarket;
    }

    @Override
    public TradeType getTradeType() {
        return tradeType;
    }

    @Override
    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    @Override
    public String getOptionTradeCategory() {
        return optionTradeCategory;
    }

    @Override
    public void setOptionTradeCategory(String optionTradeCategory) {
        this.optionTradeCategory = optionTradeCategory;
    }

    @Override
    public FutureMonth getFuturesMonth() {
        return futuresMonth;
    }

    @Override
    public void setFuturesMonth(FutureMonth futuresMonth) {
        this.futuresMonth = futuresMonth;
    }

    @Override
    public GroupCompany getGroupCompany() {
        return groupCompany;
    }

    @Override
    public void setGroupCompany(GroupCompany groupCompany) {
        this.groupCompany = groupCompany;
    }
}
