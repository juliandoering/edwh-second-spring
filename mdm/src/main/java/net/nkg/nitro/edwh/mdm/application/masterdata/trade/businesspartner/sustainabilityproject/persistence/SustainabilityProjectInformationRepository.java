package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.sustainabilityproject.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sustainabilityprojectinfos", path = "sustainabilityprojectinfos")
public interface SustainabilityProjectInformationRepository
        extends BaseRepository<SustainabilityProjectInformationEntity> {
}
