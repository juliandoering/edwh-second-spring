package net.nkg.nitro.edwh.mdm.application.base;

import net.nkg.nitro.edwh.model.common.AnalyticalData;
import org.hibernate.envers.Audited;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
@Audited
public class AnalyticalDataEntity<T extends AnalyticalDataEntity<T>> extends BaseEntity<T> implements AnalyticalData {

    @NotNull
    private boolean erroneousData;

    @Override
    public boolean isErroneousData() {
        return erroneousData;
    }

    @Override
    public void setErroneousData(boolean erroneousData) {
        this.erroneousData = erroneousData;
    }
}
