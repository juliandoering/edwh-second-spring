package net.nkg.nitro.edwh.mdm.application.referencedata.foundation.quantity;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(schema = "mdm_spring", name = "quantityunit")
@Audited
public class QuantityUnitEntity extends ReferenceDataEntity<QuantityUnitEntity> implements QuantityUnit {

    private BigDecimal kg;

    @Override
    public BigDecimal getKg() {
        return kg;
    }

    @Override
    public void setKg(BigDecimal kg) {
        this.kg = kg;
    }
}
