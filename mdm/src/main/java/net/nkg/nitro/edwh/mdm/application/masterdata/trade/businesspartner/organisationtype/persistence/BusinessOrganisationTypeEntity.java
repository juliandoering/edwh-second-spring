package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.organisationtype.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessOrganisationType;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity
@Table(schema = "mdm_spring", name = "businessorganisationtype")
@Audited
public class BusinessOrganisationTypeEntity
        extends ReferenceDataEntity<BusinessOrganisationTypeEntity> implements BusinessOrganisationType {

    private String category;

    @Enumerated(EnumType.STRING)
    @Column(name = "checkowner")
    private ExpectedRange checkOwner;

    @Enumerated(EnumType.STRING)
    @Column(name = "checkultimatebeneficiaryowner")
    private ExpectedRange checkUltimateBeneficiaryOwner;

    @Enumerated(EnumType.STRING)
    @Column(name = "checkrepresentative")
    private ExpectedRange checkRepresentative;

    @Enumerated(EnumType.STRING)
    @Column(name = "checkcontactperson")
    private ExpectedRange checkContactPerson;

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    public ExpectedRange getCheckOwner() {
        return checkOwner;
    }

    public void setCheckOwner(ExpectedRange checkOwner) {
        this.checkOwner = checkOwner;
    }

    public ExpectedRange getCheckUltimateBeneficiaryOwner() {
        return checkUltimateBeneficiaryOwner;
    }

    public void setCheckUltimateBeneficiaryOwner(ExpectedRange checkUltimateBeneficiaryOwner) {
        this.checkUltimateBeneficiaryOwner = checkUltimateBeneficiaryOwner;
    }

    public ExpectedRange getCheckRepresentative() {
        return checkRepresentative;
    }

    public void setCheckRepresentative(ExpectedRange checkRepresentative) {
        this.checkRepresentative = checkRepresentative;
    }

    public ExpectedRange getCheckContactPerson() {
        return checkContactPerson;
    }

    public void setCheckContactPerson(ExpectedRange checkContactPerson) {
        this.checkContactPerson = checkContactPerson;
    }

}
