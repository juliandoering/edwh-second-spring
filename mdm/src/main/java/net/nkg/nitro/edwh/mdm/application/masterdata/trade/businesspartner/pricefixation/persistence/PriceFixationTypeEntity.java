package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.pricefixation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.PriceFixationType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "pricefixationtype")
@Audited
public class PriceFixationTypeEntity extends ReferenceDataEntity<PriceFixationTypeEntity> implements PriceFixationType {
}
