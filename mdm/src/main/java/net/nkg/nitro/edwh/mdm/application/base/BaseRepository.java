package net.nkg.nitro.edwh.mdm.application.base;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity<T>>
        extends PagingAndSortingRepository<T, String>, RevisionRepository<T, String, Integer> {

    default boolean existsByBusinessKey(BaseEntity<? extends BaseEntity<?>> entity) {
        return findByBusinessKey((T)entity).isPresent();
    }

    default Optional<T> findByBusinessKey(T entity) {
        return Optional.empty();
    }

}
