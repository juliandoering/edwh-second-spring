package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.pricefixation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "fixationterms", path = "fixationterms")
public interface PriceFixationTermRepository extends BaseRepository<PriceFixationTermEntity> {
}
