package net.nkg.nitro.edwh.mdm.application.referencedata.quality;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import net.nkg.nitro.edwh.mdm.application.referencedata.foundation.quantity.QuantityUnitEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "qualityorigintypes", path = "qualityorigintypes")
public interface CoffeeQualityOriginTypeRepository extends ReferenceDataRepository<CoffeeQualityOriginTypeEntity> {
}
