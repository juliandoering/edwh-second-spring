package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.validation;

import net.nkg.nitro.edwh.mdm.application.base.validation.businesskey.BusinessKeyValidationService;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyRepository;
import org.springframework.stereotype.Service;

@Service
public class BusinessThirdPartyBkValidation extends BusinessKeyValidationService<BusinessThirdPartyEntity> {

    private final BusinessThirdPartyRepository repository;

    public BusinessThirdPartyBkValidation(BusinessThirdPartyRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean existsByBusinessKey(Object entity) {
        var entityImpl = getEntityImplementationSafe(entity, BusinessThirdPartyEntity.class);
        return repository.findByIdentificationCounterAndLegalNameAndLegalCountry(
                entityImpl.getIdentificationCounter(), entityImpl.getLegalName(), entityImpl.getLegalCountry()
        ).isPresent();
    }

    @Override
    public String getBusinessKeyRepresentation(Object entity) {
        var entityImpl = getEntityImplementationSafe(entity, BusinessThirdPartyEntity.class);
        return String.join(" & ",
                entityImpl.getIdentificationCounter().toString(),
                entityImpl.getLegalName(),
                entityImpl.getLegalCountry().getCountryCode()
        );
    }

}
