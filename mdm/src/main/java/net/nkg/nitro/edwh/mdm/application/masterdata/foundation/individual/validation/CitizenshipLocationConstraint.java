package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CitizenshipLocationValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CitizenshipLocationConstraint {
    String message() default "Found location code in citizenship. Only a country code should be provided.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
