package net.nkg.nitro.edwh.mdm.infrastructure.utils.validation;

import javax.validation.ConstraintValidatorContext;

public class ValidationErrorMessage {

    private final ConstraintValidatorContext validatorContext;

    private ValidationErrorMessage(ConstraintValidatorContext validatorContext) {
        this.validatorContext = validatorContext;
        validatorContext.disableDefaultConstraintViolation();
    }

    public static ValidationErrorMessage fromContext(ConstraintValidatorContext context) {
        return new ValidationErrorMessage(context);
    }

    public void addMessageKey(String key) {
        validatorContext.buildConstraintViolationWithTemplate(String.format("{%s}", key))
                .addConstraintViolation();
    }
}
