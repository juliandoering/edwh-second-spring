package net.nkg.nitro.edwh.mdm.application.referencedata.foundation.packing;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "packingcategories", path = "packingcategories")
public interface PackingTypeCategoryRepository extends ReferenceDataRepository<PackingTypeCategoryEntity> {
}
