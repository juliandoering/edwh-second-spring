package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import net.nkg.nitro.edwh.model.foundation.Location;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "businessthirdparties", path = "businessthirdparties")
public interface BusinessThirdPartyRepository extends BaseRepository<BusinessThirdPartyEntity> {

    @Override
    default Optional<BusinessThirdPartyEntity> findByBusinessKey(BusinessThirdPartyEntity entity) {
        return findByIdentificationCounterAndLegalNameAndLegalCountry(
                entity.getIdentificationCounter(), entity.getLegalName(), entity.getLegalCountry()
        );
    }

    @RestResource(exported = false)
    Optional<BusinessThirdPartyEntity> findByIdentificationCounterAndLegalNameAndLegalCountry(
            int identificationCounter, String legalName, Location legalCountry
    );
}
