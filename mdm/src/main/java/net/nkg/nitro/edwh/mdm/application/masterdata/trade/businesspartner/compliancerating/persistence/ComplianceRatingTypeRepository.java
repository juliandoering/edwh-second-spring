package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.compliancerating.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "complianceratingtypes", path = "complianceratingtypes")
public interface ComplianceRatingTypeRepository extends ReferenceDataRepository<ComplianceRatingTypeEntity> {
}
