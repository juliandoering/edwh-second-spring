package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.documentpresentation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "documentpresentations", path = "documentpresentations")
public interface PresentationOfDocumentsRepository extends ReferenceDataRepository<PresentationOfDocumentsEntity> {
}
