package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.InternationalStandardCertificationProgram;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "interstandardcertprogram")
@Audited
public class InternationalStandardCertificationProgramEntity
        extends ReferenceDataEntity<InternationalStandardCertificationProgramEntity>
        implements InternationalStandardCertificationProgram {
}
