package net.nkg.nitro.edwh.mdm.application.base;

import net.nkg.nitro.edwh.model.common.ReferenceData;
import org.hibernate.envers.Audited;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

@MappedSuperclass
@Audited
public class ReferenceDataEntity<T extends ReferenceDataEntity<T>> extends BaseEntity<T> implements ReferenceData {

    @NotBlank
    private String code;

    @NotBlank
    private String description;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
