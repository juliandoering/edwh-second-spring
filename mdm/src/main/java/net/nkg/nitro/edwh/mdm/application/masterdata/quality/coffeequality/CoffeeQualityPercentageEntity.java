package net.nkg.nitro.edwh.mdm.application.masterdata.quality.coffeequality;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.model.quality.CoffeeQuality;
import net.nkg.nitro.edwh.model.quality.CoffeeQualityPercentage;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(schema = "mdm_spring", name = "coffeequalitypercentage")
@Audited
public class CoffeeQualityPercentageEntity
        extends BaseEntity<CoffeeQualityPercentageEntity> implements CoffeeQualityPercentage {

    @ManyToOne(targetEntity = CoffeeQualityEntity.class)
    @JoinColumn(name = "coffeequality_id", referencedColumnName = "id")
    private CoffeeQuality coffeeQuality;

    @Column(name = "factor")
    private BigDecimal gradedConversionFactor;

    @Override
    public CoffeeQuality getCoffeeQuality() {
        return coffeeQuality;
    }

    @Override
    public void setCoffeeQuality(CoffeeQuality coffeeQuality) {
        this.coffeeQuality = coffeeQuality;
    }

    @Override
    public BigDecimal getGradedConversionFactor() {
        return gradedConversionFactor;
    }

    @Override
    public void setGradedConversionFactor(BigDecimal gradedConversionFactor) {
        this.gradedConversionFactor = gradedConversionFactor;
    }
}
