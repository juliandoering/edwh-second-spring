package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.incoterm.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.logistics.Incoterm;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "incoterm")
@Audited
public class IncotermEntity extends ReferenceDataEntity<IncotermEntity> implements Incoterm {
}
