package net.nkg.nitro.edwh.mdm.application.base;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AnalyticalDataRepository<T extends AnalyticalDataEntity<T>> extends BaseRepository<T> {
}
