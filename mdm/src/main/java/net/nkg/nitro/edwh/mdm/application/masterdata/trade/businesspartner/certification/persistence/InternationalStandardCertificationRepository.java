package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "standardcertifications", path = "standardcertifications")
public interface InternationalStandardCertificationRepository
        extends BaseRepository<InternationalStandardCertificationEntity> {
}
