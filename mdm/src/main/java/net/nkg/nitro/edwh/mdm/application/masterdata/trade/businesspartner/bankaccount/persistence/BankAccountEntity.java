package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.bankaccount.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence.PostalAddressEntity;
import net.nkg.nitro.edwh.model.foundation.PostalAddress;
import net.nkg.nitro.edwh.model.trade.businesspartner.BankAccount;
import net.nkg.nitro.edwh.model.trade.businesspartner.BankAccountNumberType;
import net.nkg.nitro.edwh.model.trade.businesspartner.BankAccountUsageType;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema = "mdm_spring", name = "bankaccount")
@Audited
public class BankAccountEntity extends BaseEntity<BankAccountEntity> implements BankAccount {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotBlank
    @Column(name = "bankaccount_number")
    private String bankAccountNumber;

    @OneToOne(targetEntity = BankAccountNumberTypeEntity.class)
    @JoinColumn(name = "bankaccount_number_type_id", referencedColumnName = "id")
    private BankAccountNumberType bankAccountNumberType;

    @Column(name = "bankaccount_description")
    private String bankAccountDescription;

    @NotBlank
    @Column(name = "bank_legalname")
    private String bankLegalName;

    @NotBlank
    @Column(name = "bank_branchname")
    private String bankBranchName;

    @NotNull
    @OneToOne(targetEntity = PostalAddressEntity.class)
    @JoinColumn(name = "postaladdress_id", referencedColumnName = "id")
    private PostalAddress bankAddress;

    @NotNull
    @OneToOne(targetEntity = BankAccountUsageTypeEntity.class)
    @JoinColumn(name = "bankaccount_usage_type_id", referencedColumnName = "id")
    private BankAccountUsageType bankAccountUsageType;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    @Override
    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    @Override
    public BankAccountNumberType getBankAccountNumberType() {
        return bankAccountNumberType;
    }

    @Override
    public void setBankAccountNumberType(BankAccountNumberType bankAccountNumberType) {
        this.bankAccountNumberType = bankAccountNumberType;
    }

    @Override
    public String getBankAccountDescription() {
        return bankAccountDescription;
    }

    @Override
    public void setBankAccountDescription(String bankAccountDescription) {
        this.bankAccountDescription = bankAccountDescription;
    }

    @Override
    public String getBankLegalName() {
        return bankLegalName;
    }

    @Override
    public void setBankLegalName(String bankLegalName) {
        this.bankLegalName = bankLegalName;
    }

    @Override
    public String getBankBranchName() {
        return bankBranchName;
    }

    @Override
    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    @Override
    public PostalAddress getBankAddress() {
        return bankAddress;
    }

    @Override
    public void setBankAddress(PostalAddress bankAddress) {
        this.bankAddress = bankAddress;
    }

    @Override
    public BankAccountUsageType getBankAccountUsageType() {
        return bankAccountUsageType;
    }

    @Override
    public void setBankAccountUsageType(BankAccountUsageType bankAccountUsageType) {
        this.bankAccountUsageType = bankAccountUsageType;
    }
}
