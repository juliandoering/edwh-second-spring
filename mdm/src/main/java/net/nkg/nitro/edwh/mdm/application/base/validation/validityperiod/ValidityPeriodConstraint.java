package net.nkg.nitro.edwh.mdm.application.base.validation.validityperiod;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidityPeriodValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidityPeriodConstraint {
    String message() default "Invalid dates for valid from and valid to";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
