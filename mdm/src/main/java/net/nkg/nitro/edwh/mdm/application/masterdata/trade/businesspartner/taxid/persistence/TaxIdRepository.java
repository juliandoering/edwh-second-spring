package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.taxid.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "taxids", path = "taxids")
public interface TaxIdRepository extends BaseRepository<TaxIdEntity> {

    @RestResource(exported = false)
    Optional<TaxIdEntity> findFirstByBusinessThirdPartyAndTaxId(BusinessThirdParty businessThirdParty, String taxId);

}
