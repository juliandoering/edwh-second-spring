package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyIndividualAssociationEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.persistence.ContactEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence.LocationEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.validation.CitizenshipLocationConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence.PostalAddressEntity;
import net.nkg.nitro.edwh.model.foundation.*;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyIndividualAssociation;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "individual")
@Audited
public class IndividualEntity extends BaseEntity<IndividualEntity> implements Individual {

    private String addressingForm;

    private String academicTitle;

    @NotBlank
    private String lastName;

    @NotBlank
    private String firstName;

    private Integer identificationCounter = 1;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private LocalDate dateOfBirth;

    @OneToOne(targetEntity = LocationEntity.class)
    @JoinColumn(name = "place_of_birth_id", referencedColumnName = "id")
    private Location placeOfBirth;

    @NotNull
    @CitizenshipLocationConstraint
    @OneToOne(targetEntity = LocationEntity.class)
    @JoinColumn(name = "citizenship_id", referencedColumnName = "id")
    private Location citizenship;

    @OneToOne(targetEntity = PostalAddressEntity.class)
    @JoinColumn(name = "postal_address_id", referencedColumnName = "id")
    private PostalAddress postalAddress;

    @Column(name = "personal_id")
    private String personalIdNumber;

    @Column(name = "tax_id")
    private String taxNumber;

    @OneToMany(targetEntity = ContactEntity.class, mappedBy = "individual")
    private List<Contact> contacts;

    @OneToOne(targetEntity = IdentificationTypeEntity.class)
    @JoinColumn(name = "identificationtype_id", referencedColumnName = "id")
    private IdentificationType identificationType;

    @OneToMany(targetEntity = BusinessThirdPartyIndividualAssociationEntity.class, mappedBy = "individual")
    private List<BusinessThirdPartyIndividualAssociation> businessThirdPartyAssociations;

    private String comment;

    @Column(name = "pending")
    private boolean isPending;

    @Override
    public String getAddressingForm() {
        return addressingForm;
    }

    @Override
    public void setAddressingForm(String addressingForm) {
        this.addressingForm = addressingForm;
    }

    @Override
    public String getAcademicTitle() {
        return academicTitle;
    }

    @Override
    public void setAcademicTitle(String academicTitle) {
        this.academicTitle = academicTitle;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public Integer getIdentificationCounter() {
        return identificationCounter;
    }

    @Override
    public void setIdentificationCounter(Integer identificationCounter) {
        this.identificationCounter = identificationCounter;
    }

    @Override
    public Gender getGender() {
        return gender;
    }

    @Override
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public Location getPlaceOfBirth() {
        return placeOfBirth;
    }

    @Override
    public void setPlaceOfBirth(Location placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    @Override
    public Location getCitizenship() {
        return citizenship;
    }

    @Override
    public void setCitizenship(Location citizenship) {
        this.citizenship = citizenship;
    }

    @Override
    public PostalAddress getPostalAddress() {
        return postalAddress;
    }

    @Override
    public void setPostalAddress(PostalAddress postalAddress) {
        this.postalAddress = postalAddress;
    }

    @Override
    public String getPersonalIdNumber() {
        return personalIdNumber;
    }

    @Override
    public void setPersonalIdNumber(String personalIdNumber) {
        this.personalIdNumber = personalIdNumber;
    }

    @Override
    public String getTaxNumber() {
        return taxNumber;
    }

    @Override
    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @Override
    public List<net.nkg.nitro.edwh.model.foundation.Contact> getContacts() {
        return contacts;
    }

    @Override
    public void setContacts(List<net.nkg.nitro.edwh.model.foundation.Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    @Override
    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean isPending() {
        return isPending;
    }

    @Override
    public void setPending(boolean isPending) {
        this.isPending = isPending;
    }

    public List<BusinessThirdPartyIndividualAssociation> getBusinessThirdPartyAssociations() {
        return businessThirdPartyAssociations;
    }

    public void setBusinessThirdPartyAssociations(List<BusinessThirdPartyIndividualAssociation> businessThirdPartyAssociations) {
        this.businessThirdPartyAssociations = businessThirdPartyAssociations;
    }
}
