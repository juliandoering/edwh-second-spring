package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.commercialid.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.CommercialId;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(schema = "mdm_spring", name = "commercialid")
@Audited
public class CommercialIdEntity extends BaseEntity<CommercialIdEntity> implements CommercialId {

    @NotBlank
    @Column(name = "commercialid")
    private String commercialId;

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @Override
    public String getCommercialId() {
        return commercialId;
    }

    @Override
    public void setCommercialId(String commercialId) {
        this.commercialId = commercialId;
    }
}
