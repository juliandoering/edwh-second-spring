package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.bankaccount.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "bankaccountnumbertypes", path = "bankaccountsnumbertypes")
public interface BankAccountNumberTypeRepository extends ReferenceDataRepository<BankAccountNumberTypeEntity> {
}
