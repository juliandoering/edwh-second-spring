package net.nkg.nitro.edwh.mdm.application.masterdata.quality.coffeequality;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "qualitypercentages", path = "qualitypercentages")
public interface CoffeeQualityPercentageRepository extends BaseRepository<CoffeeQualityPercentageEntity> {
}
