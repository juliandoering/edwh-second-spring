package net.nkg.nitro.edwh.mdm.application.referencedata.trade;


import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.TradeType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "tradetype")
@Audited
public class TradeTypeEntity extends ReferenceDataEntity<TradeTypeEntity> implements TradeType {
}
