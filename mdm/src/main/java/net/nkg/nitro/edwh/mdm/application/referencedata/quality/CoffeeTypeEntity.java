package net.nkg.nitro.edwh.mdm.application.referencedata.quality;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.quality.CoffeeType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "coffee_type")
@Audited
public class CoffeeTypeEntity extends ReferenceDataEntity<CoffeeTypeEntity> implements CoffeeType {
}
