package net.nkg.nitro.edwh.mdm.application.base.validation.validityperiod;

import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationErrorMessage;
import net.nkg.nitro.edwh.model.common.ValidityPeriod;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidityPeriodValidator
        implements ConstraintValidator<ValidityPeriodConstraint, ValidityPeriod> {

    @Override
    public void initialize(ValidityPeriodConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(ValidityPeriod validityPeriod,
                           ConstraintValidatorContext constraintValidatorContext) {
        var message = ValidationErrorMessage.fromContext(constraintValidatorContext);

        if (validityPeriod.getValidityPeriodFrom() == null || validityPeriod.getValidityPeriodTo() == null) {
            message.addMessageKey("validityperiod.notnull");
            return false;
        }
        if (validityPeriod.getValidityPeriodFrom().isAfter(validityPeriod.getValidityPeriodTo())) {
            message.addMessageKey("validityperiod.to-before-from");
            return false;
        }
        return true;
    }

}
