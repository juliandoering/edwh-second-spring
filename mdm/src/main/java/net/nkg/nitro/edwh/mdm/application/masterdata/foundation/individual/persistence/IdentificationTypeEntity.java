package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.IdentificationType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "identificationtype")
@Audited
public class IdentificationTypeEntity
        extends ReferenceDataEntity<IdentificationTypeEntity> implements IdentificationType {
}
