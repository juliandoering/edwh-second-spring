package net.nkg.nitro.edwh.mdm.application.base.validation;

import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;

public class BusinessPartnerValidator {

    public static boolean isValid(BusinessThirdParty businessThirdParty, GroupCompany groupCompany) {
        return (businessThirdParty != null && groupCompany == null)
                || (businessThirdParty == null && groupCompany != null);
    }

}
