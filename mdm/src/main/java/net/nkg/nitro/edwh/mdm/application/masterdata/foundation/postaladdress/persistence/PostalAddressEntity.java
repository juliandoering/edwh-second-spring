package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence.LocationEntity;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.PostalAddress;
import net.nkg.nitro.edwh.model.foundation.PostalAddressType;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "postaladdress")
@Audited
public class PostalAddressEntity extends BaseEntity<PostalAddressEntity> implements PostalAddress {

    @NotEmpty
    @ManyToMany(targetEntity = PostalAddressTypeEntity.class)
    @JoinTable(
            name = "postaladdress_postaladdresstype",
            joinColumns = @JoinColumn(name = "postaladdresstype_id"),
            inverseJoinColumns = @JoinColumn(name = "postaladdress_id")
    )
    private List<PostalAddressType> addressTypes;

    @NotBlank
    @Column(name = "addressline1")
    private String addressLine1;

    @Column(name = "addressline2")
    private String addressLine2;

    private String postalCode;

    @NotNull
    @ManyToOne(targetEntity = LocationEntity.class)
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    private Location location;

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @Override
    public List<PostalAddressType> getAddressTypes() {
        return addressTypes;
    }

    @Override
    public void setAddressTypes(List<PostalAddressType> addressTypes) {
        this.addressTypes = addressTypes;
    }

    @Override
    public String getAddressLine1() {
        return addressLine1;
    }

    @Override
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Override
    public String getAddressLine2() {
        return addressLine2;
    }

    @Override
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Override
    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public void setLocation(Location location) {
        this.location = location;
    }
}
