package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ContactValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ContactConstraint {
    String message() default "Contact type to contact value mismatch.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
