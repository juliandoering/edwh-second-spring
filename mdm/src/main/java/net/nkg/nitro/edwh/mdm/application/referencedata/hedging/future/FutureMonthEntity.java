package net.nkg.nitro.edwh.mdm.application.referencedata.hedging.future;


import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.hedging.FutureMonth;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "futuremonth")
@Audited
public class FutureMonthEntity extends ReferenceDataEntity<FutureMonthEntity> implements FutureMonth {

    private int intRepresentation;

    @Override
    public int getIntRepresentation() {
        return intRepresentation;
    }

    @Override
    public void setIntRepresentation(int intRepresentation) {
        this.intRepresentation = intRepresentation;
    }
}
