package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.compliancerating.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.compliance.ComplianceRatingType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "compliance_rating_type")
@Audited
public class ComplianceRatingTypeEntity
        extends ReferenceDataEntity<ComplianceRatingTypeEntity> implements ComplianceRatingType {
}
