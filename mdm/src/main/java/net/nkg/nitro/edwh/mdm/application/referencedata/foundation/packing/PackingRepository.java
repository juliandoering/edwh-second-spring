package net.nkg.nitro.edwh.mdm.application.referencedata.foundation.packing;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.documentpresentation.persistence.PresentationOfDocumentsEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "packings", path = "packings")
public interface PackingRepository extends ReferenceDataRepository<PackingEntity> {
}
