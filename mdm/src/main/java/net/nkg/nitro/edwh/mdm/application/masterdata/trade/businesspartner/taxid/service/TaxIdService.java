package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.taxid.service;

import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.taxid.persistence.TaxIdEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.taxid.persistence.TaxIdRepository;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import org.springframework.stereotype.Service;

@Service
public class TaxIdService {

    private final TaxIdRepository repository;

    public TaxIdService(TaxIdRepository repository) {
        this.repository = repository;
    }

    public TaxIdEntity createIfNotExists(BusinessThirdParty businessThirdParty, String taxNumber) {
        return repository.findFirstByBusinessThirdPartyAndTaxId(businessThirdParty, taxNumber)
                .orElse(createNew(businessThirdParty, taxNumber));
    }

    private TaxIdEntity createNew(BusinessThirdParty businessThirdParty, String taxNumber) {
        var taxId = new TaxIdEntity();
        taxId.setBusinessThirdParty(businessThirdParty);
        taxId.setTaxId(taxNumber);
        return repository.save(taxId);
    }
}
