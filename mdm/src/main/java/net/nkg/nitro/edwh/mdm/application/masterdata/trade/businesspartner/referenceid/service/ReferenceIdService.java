package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.service;

import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence.ReferenceIdEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence.ReferenceIdRepository;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence.ReferenceIdTypeEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence.ReferenceIdTypeRepository;
import net.nkg.nitro.edwh.model.foundation.Individual;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyReferenceIdType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReferenceIdService {

    private final ReferenceIdRepository referenceIdRepository;
    private final ReferenceIdTypeRepository referenceIdTypeRepository;

    @Autowired
    public ReferenceIdService(ReferenceIdRepository referenceIdRepository,
                              ReferenceIdTypeRepository referenceIdTypeRepository) {
        this.referenceIdRepository = referenceIdRepository;
        this.referenceIdTypeRepository = referenceIdTypeRepository;
    }

    public ReferenceIdEntity createPersonalIdForIndividual(Individual individual) {
        var refId = new ReferenceIdEntity();
        refId.setTypeOfReferenceId(getPersonalRefIdType());
        refId.setReferenceId(individual.getPersonalIdNumber());
        return referenceIdRepository.save(refId);
    }

    public ReferenceIdTypeEntity getPersonalRefIdType() {
        return referenceIdTypeRepository.findByCode(BusinessThirdPartyReferenceIdType.CODE_PERSONAL_IDENTIFICATION)
                .orElseGet(this::createNewPersonalRefIdType);
    }

    private ReferenceIdTypeEntity createNewPersonalRefIdType() {
        var personalIdType = new ReferenceIdTypeEntity();
        personalIdType.setCode(BusinessThirdPartyReferenceIdType.CODE_PERSONAL_IDENTIFICATION);
        personalIdType.setDescription(BusinessThirdPartyReferenceIdType.DESCRIPTION_PERSONAL_IDENTIFICATION);
        return referenceIdTypeRepository.save(personalIdType);
    }
}
