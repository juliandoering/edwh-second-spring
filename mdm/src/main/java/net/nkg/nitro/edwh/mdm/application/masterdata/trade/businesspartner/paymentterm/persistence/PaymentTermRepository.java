package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.paymentterm.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "paymentterms", path = "paymentterms")
public interface PaymentTermRepository extends ReferenceDataRepository<PaymentTermEntity> {
}
