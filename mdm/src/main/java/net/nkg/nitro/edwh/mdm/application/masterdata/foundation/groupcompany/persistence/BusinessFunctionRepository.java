package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "businessfunctions", path = "businessfunctions")
public interface BusinessFunctionRepository extends ReferenceDataRepository<BusinessFunctionEntity> {
}
