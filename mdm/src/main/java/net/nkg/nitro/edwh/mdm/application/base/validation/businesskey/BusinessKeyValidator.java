package net.nkg.nitro.edwh.mdm.application.base.validation.businesskey;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationErrorMessage;
import org.springframework.context.ApplicationContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BusinessKeyValidator
        implements ConstraintValidator<BusinessKeyConstraint, BaseEntity<? extends BaseEntity<?>>> {

    private final ApplicationContext context;

    private BusinessKeyValidationService<?> validationService;

    public BusinessKeyValidator(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public void initialize(BusinessKeyConstraint constraintAnnotation) {
        validationService = context.getBean(constraintAnnotation.validator());
    }

    @Override
    public boolean isValid(BaseEntity<? extends BaseEntity<?>> entity, ConstraintValidatorContext constraintValidatorContext) {
        var message = ValidationErrorMessage.fromContext(constraintValidatorContext);
        return validationService.existsByBusinessKey(entity);
    }
}
