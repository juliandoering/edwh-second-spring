package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.pricefixation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.model.trade.FixationOption;
import net.nkg.nitro.edwh.model.trade.PriceFixationType;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.PriceFixationTerm;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema = "mdm_spring", name = "pricefixationterm")
@Audited
public class PriceFixationTermEntity extends BaseEntity<PriceFixationTermEntity> implements PriceFixationTerm {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotNull
    @OneToOne(targetEntity = PriceFixationTypeEntity.class)
    @JoinColumn(name = "pricefixationtype_id", referencedColumnName = "id")
    private PriceFixationType priceFixationType;

    @NotNull
    @OneToOne(targetEntity = FixationOptionEntity.class)
    @JoinColumn(name = "fixationoption_id", referencedColumnName = "id")
    private FixationOption fixationOption;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public PriceFixationType getPriceFixationType() {
        return priceFixationType;
    }

    @Override
    public void setPriceFixationType(PriceFixationType priceFixationType) {
        this.priceFixationType = priceFixationType;
    }

    @Override
    public FixationOption getFixationOption() {
        return fixationOption;
    }

    @Override
    public void setFixationOption(FixationOption fixationOption) {
        this.fixationOption = fixationOption;
    }
}
