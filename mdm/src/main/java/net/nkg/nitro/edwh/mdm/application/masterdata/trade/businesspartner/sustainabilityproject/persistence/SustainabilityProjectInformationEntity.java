package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.sustainabilityproject.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.base.validation.validityperiod.ValidityPeriodConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.model.trade.SustainabilityProject;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.SustainabilityProjectInformation;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(schema = "mdm_spring", name = "sustainabilityprojectinformation")
@Audited
@ValidityPeriodConstraint
public class SustainabilityProjectInformationEntity
        extends BaseEntity<SustainabilityProjectInformationEntity> implements SustainabilityProjectInformation {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotNull
    @OneToOne(targetEntity = SustainabilityProjectEntity.class)
    @JoinColumn(name = "sustainabilityproject_id", referencedColumnName = "id")
    private SustainabilityProject sustainabilityProject;

    @Column(name = "thirdpartyidnumber")
    private String thirdPartyIdNumber;

    @Column(name = "validityperiodfrom")
    private LocalDate validityPeriodFrom;

    @Column(name = "validityperiodto")
    private LocalDate validityPeriodTo;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public SustainabilityProject getSustainabilityProject() {
        return sustainabilityProject;
    }

    @Override
    public void setSustainabilityProject(SustainabilityProject sustainabilityProject) {
        this.sustainabilityProject = sustainabilityProject;
    }

    @Override
    public String getThirdPartyIdNumber() {
        return thirdPartyIdNumber;
    }

    @Override
    public void setThirdPartyIdNumber(String thirdPartyIdNumber) {
        this.thirdPartyIdNumber = thirdPartyIdNumber;
    }

    @Override
    public LocalDate getValidityPeriodFrom() {
        return validityPeriodFrom;
    }

    @Override
    public void setValidityPeriodFrom(LocalDate validityPeriodFrom) {
        this.validityPeriodFrom = validityPeriodFrom;
    }

    @Override
    public LocalDate getValidityPeriodTo() {
        return validityPeriodTo;
    }

    @Override
    public void setValidityPeriodTo(LocalDate validityPeriodTo) {
        this.validityPeriodTo = validityPeriodTo;
    }
}
