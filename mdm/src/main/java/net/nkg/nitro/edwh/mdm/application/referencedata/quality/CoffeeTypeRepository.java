package net.nkg.nitro.edwh.mdm.application.referencedata.quality;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "coffeetypes", path = "coffeetypes")
public interface CoffeeTypeRepository extends ReferenceDataRepository<CoffeeTypeEntity> {
}
