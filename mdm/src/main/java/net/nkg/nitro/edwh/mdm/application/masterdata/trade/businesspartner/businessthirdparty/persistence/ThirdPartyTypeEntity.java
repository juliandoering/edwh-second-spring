package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.businesspartner.ThirdPartyType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(schema = "mdm_spring", name = "thirdpartytype")
@Audited
public class ThirdPartyTypeEntity extends ReferenceDataEntity<ThirdPartyTypeEntity> implements ThirdPartyType {

    @NotBlank
    private String category;

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }
}
