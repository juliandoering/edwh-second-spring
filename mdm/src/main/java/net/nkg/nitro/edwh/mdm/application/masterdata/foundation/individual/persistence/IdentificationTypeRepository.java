package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "identificationtypes", path = "identificationtypes")
public interface IdentificationTypeRepository extends ReferenceDataRepository<IdentificationTypeEntity> {
}
