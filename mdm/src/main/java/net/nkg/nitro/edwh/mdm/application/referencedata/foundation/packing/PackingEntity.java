package net.nkg.nitro.edwh.mdm.application.referencedata.foundation.packing;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.Packing;
import net.nkg.nitro.edwh.model.foundation.PackingTypeCategory;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(schema = "mdm_spring", name = "packing")
@Audited
public class PackingEntity extends ReferenceDataEntity<PackingEntity> implements Packing {

    private BigDecimal tare;

    @Column(name = "unitsize")
    private BigDecimal unitSize;

    @OneToOne(targetEntity = PackingTypeCategoryEntity.class)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private PackingTypeCategory category;

    @Override
    public BigDecimal getTare() {
        return tare;
    }

    @Override
    public void setTare(BigDecimal tare) {
        this.tare = tare;
    }

    @Override
    public BigDecimal getUnitSize() {
        return unitSize;
    }

    @Override
    public void setUnitSize(BigDecimal unitSize) {
        this.unitSize = unitSize;
    }

    @Override
    public PackingTypeCategory getCategory() {
        return category;
    }

    @Override
    public void setCategory(PackingTypeCategory category) {
        this.category = category;
    }
}
