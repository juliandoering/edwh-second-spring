package net.nkg.nitro.edwh.mdm.application.referencedata.hedging.future;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.foundation.quantity.QuantityUnitEntity;
import net.nkg.nitro.edwh.model.foundation.QuantityUnit;
import net.nkg.nitro.edwh.model.hedging.FutureMarket;
import net.nkg.nitro.edwh.model.hedging.FutureMonth;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "futuremarket")
@Audited
public class FutureMarketEntity extends ReferenceDataEntity<FutureMarketEntity> implements FutureMarket {

    private BigDecimal lotSize;

    @OneToOne(targetEntity = QuantityUnitEntity.class)
    @JoinColumn(name = "lot_quantityunit_id", referencedColumnName = "id")
    private QuantityUnit lotSizeUnit;

    private BigDecimal tradingMonths;

    private BigDecimal priceInterval;

    private int daysBeforeEnd;

    private boolean isFirstNoticeDayLondon;

    @ManyToMany(targetEntity = FutureMonthEntity.class)
    @JoinTable(
            name = "futuremarket_futuremonth",
            joinColumns = @JoinColumn(name = "futuremonth_id"),
            inverseJoinColumns = @JoinColumn(name = "futuremarket_id")
    )
    private List<FutureMonth> futuresMonths;

    @Override
    public BigDecimal getLotSize() {
        return lotSize;
    }

    @Override
    public void setLotSize(BigDecimal lotSize) {
        this.lotSize = lotSize;
    }

    @Override
    public QuantityUnit getLotSizeUnit() {
        return lotSizeUnit;
    }

    @Override
    public void setLotSizeUnit(QuantityUnit lotSizeUnit) {
        this.lotSizeUnit = lotSizeUnit;
    }

    @Override
    public BigDecimal getTradingMonths() {
        return tradingMonths;
    }

    @Override
    public void setTradingMonths(BigDecimal tradingMonths) {
        this.tradingMonths = tradingMonths;
    }

    @Override
    public BigDecimal getPriceInterval() {
        return priceInterval;
    }

    @Override
    public void setPriceInterval(BigDecimal priceInterval) {
        this.priceInterval = priceInterval;
    }

    @Override
    public int getDaysBeforeEnd() {
        return daysBeforeEnd;
    }

    @Override
    public void setDaysBeforeEnd(int daysBeforeEnd) {
        this.daysBeforeEnd = daysBeforeEnd;
    }

    @Override
    public boolean isFirstNoticeDayLondon() {
        return isFirstNoticeDayLondon;
    }

    @Override
    public void setFirstNoticeDayLondon(boolean firstNoticeDayLondon) {
        this.isFirstNoticeDayLondon = firstNoticeDayLondon;
    }

    public List<FutureMonth> getFuturesMonths() {
        return futuresMonths;
    }

    public void setFuturesMonths(List<FutureMonth> futuresMonths) {
        this.futuresMonths = futuresMonths;
    }
}
