package net.nkg.nitro.edwh.mdm.application.referencedata.foundation.packing;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.PackingTypeCategory;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "packingtypecategory")
@Audited
public class PackingTypeCategoryEntity
        extends ReferenceDataEntity<PackingTypeCategoryEntity> implements PackingTypeCategory {

    private String comment;

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }
}
