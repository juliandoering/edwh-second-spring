package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.commercialid.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "commercialids", path = "commercialids")
public interface CommercialIdRepository extends BaseRepository<CommercialIdEntity> {
}
