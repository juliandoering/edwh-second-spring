package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.model.foundation.FunctionType;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.LocationFunction;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity
@Table(schema = "mdm_spring", name = "locationfunction")
@Audited
public class LocationFunctionEntity extends BaseEntity<LocationFunctionEntity> implements LocationFunction {

    @ManyToOne(targetEntity = LocationEntity.class)
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    private Location location;

    @Enumerated(EnumType.STRING)
    private FunctionType functionType;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public FunctionType getFunctionType() {
        return functionType;
    }

    @Override
    public void setFunctionType(FunctionType functionType) {
        this.functionType = functionType;
    }

}
