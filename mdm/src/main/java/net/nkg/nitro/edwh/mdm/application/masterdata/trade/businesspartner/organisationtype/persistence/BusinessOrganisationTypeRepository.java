package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.organisationtype.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "organisationtypes", path = "organisationtypes")
public interface BusinessOrganisationTypeRepository extends ReferenceDataRepository<BusinessOrganisationTypeEntity> {
}
