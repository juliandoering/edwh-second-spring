package net.nkg.nitro.edwh.mdm.infrastructure.utils.validation;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class UrlValidation {

    public static boolean isValid(String url) {
        var urlWithScheme = url.startsWith("http") ? url : "http://".concat(url);
        try {
            var testUrl = new URL(urlWithScheme);
            testUrl.toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
        return true;
    }

}
