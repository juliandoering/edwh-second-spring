package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.validation.ReferenceIdConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence.GroupCompanyEntity;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyReferenceId;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyReferenceIdType;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(schema = "mdm_spring", name = "businessthirdpartyreferenceid")
@Audited
@ReferenceIdConstraint
public class ReferenceIdEntity extends BaseEntity<ReferenceIdEntity> implements BusinessThirdPartyReferenceId {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "parent_businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotBlank
    @Column(name = "referenceid")
    private String referenceId;

    @OneToOne(targetEntity = ReferenceIdTypeEntity.class)
    @JoinColumn(name = "businessthirdpartyreferenceidtype_id", referencedColumnName = "id")
    private BusinessThirdPartyReferenceIdType typeOfReferenceId;

    @OneToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "supplier_businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty supplierBusinessThirdParty;

    @OneToOne(targetEntity = GroupCompanyEntity.class)
    @JoinColumn(name = "supplier_groupcompany_id", referencedColumnName = "id")
    private GroupCompany supplierGroupCompany;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public String getReferenceId() {
        return referenceId;
    }

    @Override
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @Override
    public BusinessThirdPartyReferenceIdType getTypeOfReferenceId() {
        return typeOfReferenceId;
    }

    @Override
    public void setTypeOfReferenceId(BusinessThirdPartyReferenceIdType typeOfReferenceId) {
        this.typeOfReferenceId = typeOfReferenceId;
    }

    @Override
    public BusinessThirdParty getSupplierBusinessThirdParty() {
        return supplierBusinessThirdParty;
    }

    @Override
    public void setSupplierBusinessThirdParty(BusinessThirdParty supplierBusinessThirdParty) {
        this.supplierBusinessThirdParty = supplierBusinessThirdParty;
    }

    @Override
    public GroupCompany getSupplierGroupCompany() {
        return supplierGroupCompany;
    }

    @Override
    public void setSupplierGroupCompany(GroupCompany supplierGroupCompany) {
        this.supplierGroupCompany = supplierGroupCompany;
    }
}
