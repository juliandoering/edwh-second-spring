package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.validation;

import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.persistence.ContactEntity;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.UrlValidation;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationErrorMessage;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ContactValidator implements ConstraintValidator<ContactConstraint, ContactEntity> {

    private static final int MAX_LENGTH_EMAIL = 254;

    @Override
    public void initialize(ContactConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(ContactEntity contactEntity, ConstraintValidatorContext constraintValidatorContext) {
        var errorMessage = ValidationErrorMessage.fromContext(constraintValidatorContext);

        switch (contactEntity.getType()) {
            case EMAIL:
                errorMessage.addMessageKey("contact.format.email");
                return isEmailValid(contactEntity.getValue());
            case WEBSITE:
                errorMessage.addMessageKey("contact.format.url");
                return isUrlValid(contactEntity.getValue());
            case TELEPHONE:
            case MOBILE:
            case FAX:
                errorMessage.addMessageKey("contact.format.phone");
                return isPhoneNumberValid(contactEntity.getValue());
            default:
                return true;
        }
    }

    private boolean isEmailValid(String email) {
        return email.matches("[\\w-_.]{1,64}@([\\w-]+\\.)+[a-zA-Z]{2,}") && email.length() < MAX_LENGTH_EMAIL;
    }

    private boolean isUrlValid(String url) {
        return url.contains(".") && UrlValidation.isValid(url);
    }

    private boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.matches("(\\+|00)[0-9]+[0-9\\-\\u0020]+");
    }

}
