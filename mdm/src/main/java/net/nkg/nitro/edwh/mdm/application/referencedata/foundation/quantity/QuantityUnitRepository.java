package net.nkg.nitro.edwh.mdm.application.referencedata.foundation.quantity;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import net.nkg.nitro.edwh.mdm.application.referencedata.foundation.packing.PackingEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "quantityunits", path = "quantityunits")
public interface QuantityUnitRepository extends ReferenceDataRepository<QuantityUnitEntity> {
}
