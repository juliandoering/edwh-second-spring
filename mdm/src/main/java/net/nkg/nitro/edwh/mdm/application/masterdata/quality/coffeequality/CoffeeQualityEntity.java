package net.nkg.nitro.edwh.mdm.application.masterdata.quality.coffeequality;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence.LocationEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.quality.CoffeeQualityOriginTypeEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.quality.CoffeeTypeEntity;
import net.nkg.nitro.edwh.mdm.application.referencedata.quality.CoffeeVarietyEntity;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.quality.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "coffeequality")
@Audited
public class CoffeeQualityEntity extends BaseEntity<CoffeeQualityEntity> implements CoffeeQuality {

    private String name;

    @OneToOne(targetEntity = CoffeeTypeEntity.class)
    @JoinColumn(name = "coffee_type_id", referencedColumnName = "id")
    private CoffeeType coffeeType;

    @OneToOne(targetEntity = CoffeeVarietyEntity.class)
    @JoinColumn(name = "coffee_variety_id", referencedColumnName = "id")
    private CoffeeVariety coffeeVarietyType;

    @Column(name = "grade")
    private String gradeSpecification;

    @Column(name = "defectcount")
    private String defectCountSpecification;

    @Column(name = "humidity")
    private String humiditySpecification;

    @Column(name = "cup_profile")
    private String cupProfileSpecification;

    @OneToOne(targetEntity = CoffeeQualityOriginTypeEntity.class)
    @JoinColumn(name = "coffeequalityorigintype_id", referencedColumnName = "id")
    private CoffeeQualityOriginType originType;

    @ManyToMany(targetEntity = LocationEntity.class)
    @JoinTable(
            name = "coffeequality_origin",
            joinColumns = @JoinColumn(name = "location_id"),
            inverseJoinColumns = @JoinColumn(name = "coffeequality_id")
    )
    private List<Location> origins;

    @OneToMany(targetEntity = CoffeeQualityPercentageEntity.class, mappedBy = "coffeeQuality")
    private List<CoffeeQualityPercentage> conversionToGradedDistribution;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public CoffeeType getCoffeeType() {
        return coffeeType;
    }

    @Override
    public void setCoffeeType(CoffeeType coffeeType) {
        this.coffeeType = coffeeType;
    }

    @Override
    public CoffeeVariety getCoffeeVarietyType() {
        return coffeeVarietyType;
    }

    @Override
    public void setCoffeeVarietyType(CoffeeVariety coffeeVarietyType) {
        this.coffeeVarietyType = coffeeVarietyType;
    }

    @Override
    public String getGradeSpecification() {
        return gradeSpecification;
    }

    @Override
    public void setGradeSpecification(String gradeSpecification) {
        this.gradeSpecification = gradeSpecification;
    }

    @Override
    public String getDefectCountSpecification() {
        return defectCountSpecification;
    }

    @Override
    public void setDefectCountSpecification(String defectCountSpecification) {
        this.defectCountSpecification = defectCountSpecification;
    }

    @Override
    public String getHumiditySpecification() {
        return humiditySpecification;
    }

    @Override
    public void setHumiditySpecification(String humiditySpecification) {
        this.humiditySpecification = humiditySpecification;
    }

    @Override
    public String getCupProfileSpecification() {
        return cupProfileSpecification;
    }

    @Override
    public void setCupProfileSpecification(String cupProfileSpecification) {
        this.cupProfileSpecification = cupProfileSpecification;
    }

    @Override
    public CoffeeQualityOriginType getOriginType() {
        return originType;
    }

    @Override
    public void setOriginType(CoffeeQualityOriginType originType) {
        this.originType = originType;
    }

    @Override
    public List<Location> getOrigins() {
        return origins;
    }

    @Override
    public void setOrigins(List<Location> origins) {
        this.origins = origins;
    }

    @Override
    public List<CoffeeQualityPercentage> getConversionToGradedDistribution() {
        return conversionToGradedDistribution;
    }

    @Override
    public void setConversionToGradedDistribution(List<CoffeeQualityPercentage> conversionToGradedDistribution) {
        this.conversionToGradedDistribution = conversionToGradedDistribution;
    }
}
