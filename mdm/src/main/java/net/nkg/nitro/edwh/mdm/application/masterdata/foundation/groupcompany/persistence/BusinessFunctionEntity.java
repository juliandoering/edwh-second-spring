package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.BusinessFunction;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "businessfunction")
@Audited
public class BusinessFunctionEntity extends ReferenceDataEntity<BusinessFunctionEntity> implements BusinessFunction {
}
