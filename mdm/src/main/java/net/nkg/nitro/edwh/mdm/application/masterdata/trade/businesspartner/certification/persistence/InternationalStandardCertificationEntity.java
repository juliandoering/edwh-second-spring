package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.base.validation.validityperiod.ValidityPeriodConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.model.foundation.InternationalStandardCertificationProgram;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.InternationalStandardCertification;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(schema = "mdm_spring", name = "interstandardcert")
@Audited
@ValidityPeriodConstraint
public class InternationalStandardCertificationEntity
        extends BaseEntity<InternationalStandardCertificationEntity> implements InternationalStandardCertification {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotNull
    @OneToOne(targetEntity = InternationalStandardCertificationProgramEntity.class)
    @JoinColumn(name = "interstandardcertprogram_id", referencedColumnName = "id")
    private InternationalStandardCertificationProgram certification;

    @Column(name = "certificateversion")
    private String certificateVersion;

    @Column(name = "thirdpartyidnumber")
    private String thirdPartyIdNumber;

    @Column(name = "validityperiodfrom")
    private LocalDate validityPeriodFrom;

    @Column(name = "validityperiodto")
    private LocalDate validityPeriodTo;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public InternationalStandardCertificationProgram getCertification() {
        return certification;
    }

    @Override
    public void setCertification(InternationalStandardCertificationProgram certification) {
        this.certification = certification;
    }

    @Override
    public String getCertificateVersion() {
        return certificateVersion;
    }

    @Override
    public void setCertificateVersion(String certificateVersion) {
        this.certificateVersion = certificateVersion;
    }

    @Override
    public String getThirdPartyIdNumber() {
        return thirdPartyIdNumber;
    }

    @Override
    public void setThirdPartyIdNumber(String thirdPartyIdNumber) {
        this.thirdPartyIdNumber = thirdPartyIdNumber;
    }

    @Override
    public LocalDate getValidityPeriodFrom() {
        return validityPeriodFrom;
    }

    @Override
    public void setValidityPeriodFrom(LocalDate validityPeriodFrom) {
        this.validityPeriodFrom = validityPeriodFrom;
    }

    @Override
    public LocalDate getValidityPeriodTo() {
        return validityPeriodTo;
    }

    @Override
    public void setValidityPeriodTo(LocalDate validityPeriodTo) {
        this.validityPeriodTo = validityPeriodTo;
    }

}
