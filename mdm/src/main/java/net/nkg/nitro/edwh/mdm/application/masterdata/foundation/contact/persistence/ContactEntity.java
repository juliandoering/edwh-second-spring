package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence.IndividualEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.validation.ContactConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence.GroupCompanyEntity;
import net.nkg.nitro.edwh.model.foundation.Contact;
import net.nkg.nitro.edwh.model.foundation.GroupCompany;
import net.nkg.nitro.edwh.model.foundation.Individual;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ContactConstraint
@Entity
@Table(schema = "mdm_spring", name = "contact")
@Audited
public class ContactEntity extends BaseEntity<ContactEntity> implements Contact {

    @NotBlank
    @Column(name = "contactvalue")
    private String value;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "contacttype")
    private Contact.Type type;

    @ManyToOne(targetEntity = IndividualEntity.class)
    @JoinColumn(name = "individual_id", referencedColumnName = "id")
    private Individual individual;

    @ManyToOne(targetEntity = GroupCompanyEntity.class)
    @JoinColumn(name = "groupcompany_id", referencedColumnName = "id")
    private GroupCompany groupCompany;

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public void setType(Type type) {
        this.type = type;
    }

    public Individual getIndividual() {
        return individual;
    }

    public void setIndividual(Individual individual) {
        this.individual = individual;
    }
}
