package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "contacts", path = "contacts")
public interface ContactRepository extends BaseRepository<ContactEntity> {

}
