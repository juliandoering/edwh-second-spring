package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.bankaccount.persistence;


import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.businesspartner.BankAccountUsageType;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "bankaccount_usagetype")
@Audited
public class BankAccountUsageTypeEntity
        extends ReferenceDataEntity<BankAccountUsageTypeEntity> implements BankAccountUsageType {
}
