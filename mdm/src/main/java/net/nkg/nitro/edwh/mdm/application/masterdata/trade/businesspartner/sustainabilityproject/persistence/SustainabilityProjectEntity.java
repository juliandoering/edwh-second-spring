package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.sustainabilityproject.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.SustainabilityProject;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "sustainabilityproject")
@Audited
public class SustainabilityProjectEntity
        extends ReferenceDataEntity<SustainabilityProjectEntity> implements SustainabilityProject {
}
