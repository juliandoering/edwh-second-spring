package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.creditlimit.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.currency.persistence.CurrencyUnitEntity;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.ExternalCreditLimit;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(schema = "mdm_spring", name = "externalcreditlimit")
@Audited
public class ExternalCreditLimitEntity extends BaseEntity<ExternalCreditLimitEntity> implements ExternalCreditLimit {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @OneToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "insurer_id", referencedColumnName = "id")
    private BusinessThirdParty insurer;

    @Column(name = "insurerid")
    private String insurerId;

    @OneToOne(targetEntity = CurrencyUnitEntity.class)
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    private CurrencyUnit currency;

    private BigDecimal amount;

    @Column(name = "datefrom")
    private LocalDate validFrom;

    @Column(name = "dateto")
    private LocalDate validTo;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public BusinessThirdParty getInsurer() {
        return insurer;
    }

    @Override
    public void setInsurer(BusinessThirdParty insurer) {
        this.insurer = insurer;
    }

    @Override
    public String getInsurerId() {
        return insurerId;
    }

    @Override
    public void setInsurerId(String insurerId) {
        this.insurerId = insurerId;
    }

    @Override
    public CurrencyUnit getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(CurrencyUnit currency) {
        this.currency = currency;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public LocalDate getValidFrom() {
        return validFrom;
    }

    @Override
    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    @Override
    public LocalDate getValidTo() {
        return validTo;
    }

    @Override
    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }
}
