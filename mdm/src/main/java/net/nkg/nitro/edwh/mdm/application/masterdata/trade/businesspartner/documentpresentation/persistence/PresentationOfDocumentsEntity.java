package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.documentpresentation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.businesspartner.PresentationOfDocuments;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "presentation_of_documents")
@Audited
public class PresentationOfDocumentsEntity
        extends ReferenceDataEntity<PresentationOfDocumentsEntity> implements PresentationOfDocuments {
}
