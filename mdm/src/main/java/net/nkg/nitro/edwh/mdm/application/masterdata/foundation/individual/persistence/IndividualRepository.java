package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "individuals", path = "individuals")
public interface IndividualRepository extends BaseRepository<IndividualEntity> {
}
