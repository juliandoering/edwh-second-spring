package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.pricefixation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.FixationOption;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "fixationoption")
@Audited
public class FixationOptionEntity extends ReferenceDataEntity<FixationOptionEntity> implements FixationOption {
}
