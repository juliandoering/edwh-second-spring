package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.currency.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.sustainabilityproject.persistence.SustainabilityProjectEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "currencies", itemResourceRel = "currency", path = "currencies")
public interface CurrencyUnitRepository extends ReferenceDataRepository<SustainabilityProjectEntity> {
}
