package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "functions", path = "locationfunctions")
public interface LocationFunctionRepository extends BaseRepository<LocationFunctionEntity> {
}
