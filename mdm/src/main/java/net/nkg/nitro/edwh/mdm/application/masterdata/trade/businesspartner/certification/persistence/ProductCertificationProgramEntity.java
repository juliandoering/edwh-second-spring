package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.quality.ProductCertificationProgram;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "productcertificationprogram")
@Audited
public class ProductCertificationProgramEntity
        extends ReferenceDataEntity<ProductCertificationProgramEntity> implements ProductCertificationProgram {
}
