package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyReferenceIdType;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "businessthirdpartyreferenceidtype")
@Audited
public class ReferenceIdTypeEntity
        extends ReferenceDataEntity<ReferenceIdTypeEntity> implements BusinessThirdPartyReferenceIdType {

    @Column(name = "idsuppliermandatory")
    private boolean idSupplierMandatory;

    @Override
    public boolean getIdSupplierMandatory() {
        return idSupplierMandatory;
    }

    @Override
    public void setIdSupplierMandatory(boolean idSupplierMandatory) {
        this.idSupplierMandatory = idSupplierMandatory;
    }
}
