package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.pricefixation.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "fixationtypes", path = "fixationtypes")
public interface PriceFixationTypeRepository extends ReferenceDataRepository<PriceFixationTypeEntity> {
}
