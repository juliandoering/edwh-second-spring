package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "productcertificationprograms", path = "productcertificationprograms")
public interface ProductCertificationProgramRepository
        extends ReferenceDataRepository<ProductCertificationProgramEntity> {
}
