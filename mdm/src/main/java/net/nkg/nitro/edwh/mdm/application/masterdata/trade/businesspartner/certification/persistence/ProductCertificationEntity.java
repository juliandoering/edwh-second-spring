package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.base.validation.validityperiod.ValidityPeriodConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.model.quality.ProductCertificationProgram;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.ProductCertification;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(schema = "mdm_spring", name = "productcertification")
@Audited
@ValidityPeriodConstraint
public class ProductCertificationEntity extends BaseEntity<ProductCertificationEntity> implements ProductCertification {

    @ManyToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotNull
    @OneToOne(targetEntity = ProductCertificationProgramEntity.class)
    @JoinColumn(name = "productcertificationprogram_id", referencedColumnName = "id")
    private ProductCertificationProgram certification;

    @Column(name = "thirdpartyidnumber")
    private String thirdPartyIdNumber;

    @Column(name = "validityperiodfrom")
    private LocalDate validityPeriodFrom;

    @Column(name = "validityperiodto")
    private LocalDate validityPeriodTo;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public ProductCertificationProgram getCertification() {
        return certification;
    }

    @Override
    public void setCertification(ProductCertificationProgram certification) {
        this.certification = certification;
    }

    @Override
    public String getThirdPartyIdNumber() {
        return thirdPartyIdNumber;
    }

    @Override
    public void setThirdPartyIdNumber(String thirdPartyIdNumber) {
        this.thirdPartyIdNumber = thirdPartyIdNumber;
    }

    @Override
    public LocalDate getValidityPeriodFrom() {
        return validityPeriodFrom;
    }

    @Override
    public void setValidityPeriodFrom(LocalDate validityPeriodFrom) {
        this.validityPeriodFrom = validityPeriodFrom;
    }

    @Override
    public LocalDate getValidityPeriodTo() {
        return validityPeriodTo;
    }

    @Override
    public void setValidityPeriodTo(LocalDate validityPeriodTo) {
        this.validityPeriodTo = validityPeriodTo;
    }

}
