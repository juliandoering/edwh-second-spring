package net.nkg.nitro.edwh.mdm.application.base.validation.businesskey;

public abstract class BusinessKeyValidationService<T> {

    public abstract boolean existsByBusinessKey(Object entity);

    public abstract String getBusinessKeyRepresentation(Object entity);

    protected T getEntityImplementationSafe(Object entity, Class<T> type) {
        if (entity.getClass() == type) {
            return (T)entity;
        }
        var message = String.format("Invalid Business Key Validation! Wrong type: %s; Expected Type: %s",
                entity.getClass().getSimpleName(), type.getSimpleName());
        throw new IllegalArgumentException(message);
    }

}
