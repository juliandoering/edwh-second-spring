package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "postaladdresses", path = "postaladdresses")
public interface PostalAddressRepository extends BaseRepository<PostalAddressEntity> {
}
