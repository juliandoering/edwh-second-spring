package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.sustainabilityproject.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sustainabilityprojects", path = "sustainabilityprojects")
public interface SustainabilityProjectRepository extends ReferenceDataRepository<SustainabilityProjectEntity> {
}
