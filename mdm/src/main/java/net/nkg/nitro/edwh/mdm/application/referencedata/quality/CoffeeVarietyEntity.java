package net.nkg.nitro.edwh.mdm.application.referencedata.quality;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.quality.CoffeeVariety;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "coffee_variety")
@Audited
public class CoffeeVarietyEntity extends ReferenceDataEntity<CoffeeVarietyEntity> implements CoffeeVariety {
}
