package net.nkg.nitro.edwh.mdm.application.base.validation.businesskey;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BusinessKeyValidator.class)
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface BusinessKeyConstraint {
    String message() default "Duplicate entity";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Class<? extends BusinessKeyValidationService<?>> validator();
}
