package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.groupcompany.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "groupcompanies", path = "groupcompanies")
public interface GroupCompanyRepository extends BaseRepository<GroupCompanyEntity> {
}
