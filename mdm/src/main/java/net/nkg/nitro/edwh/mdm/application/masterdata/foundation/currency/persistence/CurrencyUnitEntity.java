package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.currency.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.foundation.CurrencyUnit;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "currency")
@Audited
public class CurrencyUnitEntity extends ReferenceDataEntity<CurrencyUnitEntity> implements CurrencyUnit {
}
