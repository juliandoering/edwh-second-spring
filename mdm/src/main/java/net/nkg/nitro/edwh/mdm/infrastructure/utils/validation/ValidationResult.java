package net.nkg.nitro.edwh.mdm.infrastructure.utils.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {

    private final List<Boolean> results = new ArrayList<>();

    public void add(boolean result) {
        results.add(result);
    }

    public boolean isValid() {
        return results.stream().allMatch(isValid -> isValid);
    }
}
