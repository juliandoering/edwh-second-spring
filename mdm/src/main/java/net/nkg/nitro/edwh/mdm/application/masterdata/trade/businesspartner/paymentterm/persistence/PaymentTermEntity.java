package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.paymentterm.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataEntity;
import net.nkg.nitro.edwh.model.trade.PaymentTerm;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "mdm_spring", name = "paymentterm")
@Audited
public class PaymentTermEntity extends ReferenceDataEntity<PaymentTermEntity> implements PaymentTerm {

    private boolean viaBank;

    @Override
    public boolean isViaBank() {
        return viaBank;
    }

    @Override
    public void setViaBank(boolean viaBank) {
        this.viaBank = viaBank;
    }
}
