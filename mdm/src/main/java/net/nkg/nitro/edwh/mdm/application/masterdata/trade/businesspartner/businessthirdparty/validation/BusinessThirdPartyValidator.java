package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.validation;

import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationErrorMessage;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationResult;
import net.nkg.nitro.edwh.model.foundation.Contact;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessOrganisationType;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdPartyIndividualAssociation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class BusinessThirdPartyValidator
        implements ConstraintValidator<BusinessThirdPartyConstraint, BusinessThirdPartyEntity> {

    @Override
    public void initialize(BusinessThirdPartyConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(BusinessThirdPartyEntity businessThirdPartyEntity,
                           ConstraintValidatorContext constraintValidatorContext) {
        var message = ValidationErrorMessage.fromContext(constraintValidatorContext);
        var validationResult = new ValidationResult();

        validationResult.add(areAddressesValid(businessThirdPartyEntity, message));
        validationResult.add(isSoleProprietorshipValid(businessThirdPartyEntity, message));
        validationResult.add(areContactsValid(businessThirdPartyEntity, message));
        validationResult.add(areAssociatedIndividualsValid(businessThirdPartyEntity, message));

        return validationResult.isValid();
    }

    private boolean areAddressesValid(BusinessThirdPartyEntity businessThirdPartyEntity,
                                      ValidationErrorMessage message) {
        var areValid = businessThirdPartyEntity.getAddresses().stream()
                .map(address -> address.getLocation().getCountryCode())
                .anyMatch(countryCode -> countryCode.equals(businessThirdPartyEntity.getLegalCountry().getCountryCode()));
        if (!areValid) {
            message.addMessageKey("btp.addresses.no-address-in-legal-country");
        }
        return areValid;
    }

    private boolean isSoleProprietorshipValid(BusinessThirdPartyEntity businessThirdPartyEntity,
                                             ValidationErrorMessage message) {
        if (businessThirdPartyEntity.isSoleProprietorship()) {
            var hasOwner = businessThirdPartyEntity.getIndividualAssociations().stream()
                    .anyMatch(a -> a.getRelation() == BusinessThirdPartyIndividualAssociation.Relation.OWNER);
            if (!hasOwner) {
                message.addMessageKey("btp.sole-proprietorship.owner.missing");
                return false;
            }
        }
        return true;
    }

    private boolean areContactsValid(BusinessThirdPartyEntity businessThirdPartyEntity,
                                     ValidationErrorMessage message) {
        var valid = true;
        var contacts = businessThirdPartyEntity.getContacts();

        if (contacts.stream().noneMatch(c ->
                c.getType() == Contact.Type.MOBILE
                        || c.getType() == Contact.Type.TELEPHONE)) {
            message.addMessageKey("btp.contacts.phone");
            valid = false;
        }
        if (contacts.stream().noneMatch(c -> c.getType() == Contact.Type.EMAIL)) {
            message.addMessageKey("btp.contacts.email");
            valid = false;
        }

        return valid;
    }

    private boolean areAssociatedIndividualsValid(BusinessThirdPartyEntity businessThirdPartyEntity,
                                                  ValidationErrorMessage message) {
        var valid = true;
        var organisationType = businessThirdPartyEntity.getBusinessType();

        if (outOfRange(organisationType.getCheckOwner(),
                BusinessThirdPartyIndividualAssociation.Relation.OWNER,
                businessThirdPartyEntity.getIndividualAssociations())) {
            valid = false;
            message.addMessageKey("btp.individual-association.owner.size");
        }
        if (outOfRange(organisationType.getCheckUltimateBeneficiaryOwner(),
                BusinessThirdPartyIndividualAssociation.Relation.UBO,
                businessThirdPartyEntity.getIndividualAssociations())) {
            valid = false;
            message.addMessageKey("btp.individual-association.ubo.size");
        }
        if (outOfRange(organisationType.getCheckRepresentative(),
                BusinessThirdPartyIndividualAssociation.Relation.REPRESENTATIVE,
                businessThirdPartyEntity.getIndividualAssociations())) {
            valid = false;
            message.addMessageKey("btp.individual-association.rep.size");
        }
        if (outOfRange(organisationType.getCheckContactPerson(),
                BusinessThirdPartyIndividualAssociation.Relation.CONTACT,
                businessThirdPartyEntity.getIndividualAssociations())) {
            valid = false;
            message.addMessageKey("btp.individual-association.contact.size");
        }

        return valid;
    }

    private boolean outOfRange(BusinessOrganisationType.ExpectedRange expectedRange,
                               BusinessThirdPartyIndividualAssociation.Relation relation,
                               List<BusinessThirdPartyIndividualAssociation> associations) {
        if (expectedRange == null) {
            return false;
        }
        var associationCount = (int)associations.stream().filter(a -> a.getRelation().equals(relation)).count();
        return !expectedRange.isInRange(associationCount);
    }

}
