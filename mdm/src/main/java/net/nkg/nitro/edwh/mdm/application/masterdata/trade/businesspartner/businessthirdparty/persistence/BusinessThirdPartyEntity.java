package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.base.validation.businesskey.BusinessKeyConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.bankaccount.persistence.BankAccountEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence.InternationalStandardCertificationEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.certification.persistence.ProductCertificationEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.commercialid.persistence.CommercialIdEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.compliancerating.persistence.ComplianceRatingEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.creditlimit.persistence.ExternalCreditLimitEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.documentpresentation.persistence.PresentationOfDocumentsEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.incoterm.persistence.IncotermEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.organisationtype.persistence.BusinessOrganisationTypeEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.paymentterm.persistence.PaymentTermEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.pricefixation.persistence.PriceFixationTermEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.referenceid.persistence.ReferenceIdEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.sustainabilityproject.persistence.SustainabilityProjectInformationEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.taxid.persistence.TaxIdEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.validation.BusinessThirdPartyConstraint;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.contact.persistence.ContactEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence.LocationEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.postaladdress.persistence.PostalAddressEntity;
import net.nkg.nitro.edwh.model.foundation.Contact;
import net.nkg.nitro.edwh.model.foundation.Location;
import net.nkg.nitro.edwh.model.foundation.PostalAddress;
import net.nkg.nitro.edwh.model.logistics.Incoterm;
import net.nkg.nitro.edwh.model.trade.PaymentTerm;
import net.nkg.nitro.edwh.model.trade.businesspartner.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(schema = "mdm_spring", name = "businessthirdparty")
@Audited
@BusinessThirdPartyConstraint
@BusinessKeyConstraint(repository = BusinessThirdPartyRepository.class)
public class BusinessThirdPartyEntity extends BaseEntity<BusinessThirdPartyEntity> implements BusinessThirdParty {

    private Integer identificationCounter = 1;

    @NotBlank
    @Column(name = "legalname")
    private String legalName;

    @Column(name = "legalnameinlatinalphabet")
    private String legalNameInLatinAlphabet;

    @NotNull
    @OneToOne(targetEntity = LocationEntity.class)
    @JoinColumn(name = "legal_country_id", referencedColumnName = "id")
    private Location legalCountry;

    @Column(name = "commercialname")
    private String commercialName;

    private String acronym;

    @NotNull
    @OneToOne(targetEntity = BusinessOrganisationTypeEntity.class)
    @JoinColumn(name = "businessorganisationtype_id", referencedColumnName = "id")
    private BusinessOrganisationType businessType;

    @NotEmpty
    @OneToMany(targetEntity = PostalAddressEntity.class, mappedBy = "businessThirdParty")
    private List<PostalAddress> addresses;

    @NotEmpty
    @OneToMany(targetEntity = ContactEntity.class, mappedBy = "businessThirdParty")
    private List<Contact> contacts;

    @OneToMany(targetEntity = CommercialIdEntity.class, mappedBy = "businessThirdParty")
    private List<CommercialId> commercialIds;

    @OneToMany(targetEntity = TaxIdEntity.class, mappedBy = "businessThirdParty")
    private List<TaxId> taxIds;

    @OneToMany(targetEntity = BusinessThirdPartyIndividualAssociationEntity.class, mappedBy = "businessThirdParty")
    private List<BusinessThirdPartyIndividualAssociation> individualAssociations;

    @OneToMany(targetEntity = BankAccountEntity.class, mappedBy = "businessThirdParty")
    private List<BankAccount> bankAccounts;

    @NotEmpty
    @ManyToMany(targetEntity = ThirdPartyTypeEntity.class)
    @JoinTable(
            name = "businessthirdparty_thirdpartytype",
            joinColumns = @JoinColumn(name = "thirdpartytype_id"),
            inverseJoinColumns = @JoinColumn(name = "businessthirdparty_id")
    )
    private List<ThirdPartyType> thirdPartyTypes;

    @OneToMany(targetEntity = InternationalStandardCertificationEntity.class, mappedBy = "businessThirdParty")
    private List<InternationalStandardCertification> internationalStandardCertifications;

    @OneToMany(targetEntity = ProductCertificationEntity.class, mappedBy = "businessThirdParty")
    private List<ProductCertification> productCertifications;

    @NotNull
    @OneToOne(targetEntity = ComplianceRatingEntity.class, mappedBy = "businessThirdParty")
    private ComplianceRating complianceRating;

    @OneToOne(targetEntity = IncotermEntity.class)
    @JoinColumn(name = "incoterm_id", referencedColumnName = "id")
    private Incoterm contractTerms;

    @OneToMany(targetEntity = PriceFixationTermEntity.class, mappedBy = "businessThirdParty")
    private List<PriceFixationTerm> priceFixationTerms;

    @NotNull
    @OneToOne(targetEntity = PresentationOfDocumentsEntity.class)
    @JoinColumn(name = "presentation_of_documents_id", referencedColumnName = "id")
    private PresentationOfDocuments presentationOfDocuments;

    @NotNull
    @OneToOne(targetEntity = PaymentTermEntity.class)
    @JoinColumn(name = "paymentterm_id", referencedColumnName = "id")
    private PaymentTerm paymentTerms;

    private String preferredTermsAndConditions;

    @OneToMany(targetEntity = SustainabilityProjectInformationEntity.class, mappedBy = "businessThirdParty")
    private List<SustainabilityProjectInformation> sustainabilityProjectInformation;

    @OneToMany(targetEntity = ExternalCreditLimitEntity.class, mappedBy = "businessThirdParty")
    private List<ExternalCreditLimit> externalCreditLimits;

    @OneToMany(targetEntity = ReferenceIdEntity.class, mappedBy = "businessThirdParty")
    private List<BusinessThirdPartyReferenceId> businessThirdPartyReferenceIds;

    private String comment;

    private boolean pending;

    @Override
    public Integer getIdentificationCounter() {
        return identificationCounter;
    }

    @Override
    public void setIdentificationCounter(Integer identificationCounter) {
        this.identificationCounter = identificationCounter;
    }

    @Override
    public String getLegalName() {
        return legalName;
    }

    @Override
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    @Override
    public String getLegalNameInLatinAlphabet() {
        return legalNameInLatinAlphabet;
    }

    @Override
    public void setLegalNameInLatinAlphabet(String legalNameInLatinAlphabet) {
        this.legalNameInLatinAlphabet = legalNameInLatinAlphabet;
    }

    @Override
    public Location getLegalCountry() {
        return legalCountry;
    }

    @Override
    public void setLegalCountry(Location legalCountry) {
        this.legalCountry = legalCountry;
    }

    @Override
    public String getCommercialName() {
        return commercialName;
    }

    @Override
    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

    @Override
    public String getAcronym() {
        return acronym;
    }

    @Override
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @Override
    public BusinessOrganisationType getBusinessType() {
        return businessType;
    }

    @Override
    public void setBusinessType(BusinessOrganisationType businessType) {
        this.businessType = businessType;
    }

    @Override
    public List<PostalAddress> getAddresses() {
        return addresses;
    }

    @Override
    public void setAddresses(List<PostalAddress> addresses) {
        this.addresses = addresses;
    }

    @Override
    public List<Contact> getContacts() {
        return contacts;
    }

    @Override
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public List<CommercialId> getCommercialIds() {
        return commercialIds;
    }

    @Override
    public void setCommercialIds(List<CommercialId> commercialIds) {
        this.commercialIds = commercialIds;
    }

    @Override
    public List<TaxId> getTaxIds() {
        return taxIds;
    }

    @Override
    public void setTaxIds(List<TaxId> taxIds) {
        this.taxIds = taxIds;
    }

    @Override
    public List<BusinessThirdPartyIndividualAssociation> getIndividualAssociations() {
        return individualAssociations;
    }

    @Override
    public void setIndividualAssociations(List<BusinessThirdPartyIndividualAssociation> individualAssociations) {
        this.individualAssociations = individualAssociations;
    }

    @Override
    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    @Override
    public void setBankAccounts(List<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    @Override
    public List<ThirdPartyType> getThirdPartyTypes() {
        return thirdPartyTypes;
    }

    @Override
    public void setThirdPartyTypes(List<ThirdPartyType> thirdPartyTypes) {
        this.thirdPartyTypes = thirdPartyTypes;
    }

    @Override
    public List<InternationalStandardCertification> getInternationalStandardCertifications() {
        return internationalStandardCertifications;
    }

    @Override
    public void setInternationalStandardCertifications(List<InternationalStandardCertification> internationalStandardCertifications) {
        this.internationalStandardCertifications = internationalStandardCertifications;
    }

    @Override
    public List<ProductCertification> getProductCertifications() {
        return productCertifications;
    }

    @Override
    public void setProductCertifications(List<ProductCertification> productCertifications) {
        this.productCertifications = productCertifications;
    }

    @Override
    public ComplianceRating getComplianceRating() {
        return complianceRating;
    }

    @Override
    public void setComplianceRating(ComplianceRating complianceRating) {
        this.complianceRating = complianceRating;
    }

    @Override
    public Incoterm getContractTerms() {
        return contractTerms;
    }

    @Override
    public void setContractTerms(Incoterm contractTerms) {
        this.contractTerms = contractTerms;
    }

    @Override
    public List<PriceFixationTerm> getPriceFixationTerms() {
        return priceFixationTerms;
    }

    @Override
    public void setPriceFixationTerms(List<PriceFixationTerm> priceFixationTerms) {
        this.priceFixationTerms = priceFixationTerms;
    }

    @Override
    public PresentationOfDocuments getPresentationOfDocuments() {
        return presentationOfDocuments;
    }

    @Override
    public void setPresentationOfDocuments(PresentationOfDocuments presentationOfDocuments) {
        this.presentationOfDocuments = presentationOfDocuments;
    }

    @Override
    public PaymentTerm getPaymentTerms() {
        return paymentTerms;
    }

    @Override
    public void setPaymentTerms(PaymentTerm paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    @Override
    public String getPreferredTermsAndConditions() {
        return preferredTermsAndConditions;
    }

    @Override
    public void setPreferredTermsAndConditions(String preferredTermsAndConditions) {
        this.preferredTermsAndConditions = preferredTermsAndConditions;
    }

    @Override
    public List<SustainabilityProjectInformation> getSustainabilityProjectInformation() {
        return sustainabilityProjectInformation;
    }

    @Override
    public void setSustainabilityProjectInformation(List<SustainabilityProjectInformation> sustainabilityProjectInformation) {
        this.sustainabilityProjectInformation = sustainabilityProjectInformation;
    }

    @Override
    public List<ExternalCreditLimit> getExternalCreditLimits() {
        return externalCreditLimits;
    }

    @Override
    public void setExternalCreditLimits(List<ExternalCreditLimit> externalCreditLimits) {
        this.externalCreditLimits = externalCreditLimits;
    }

    @Override
    public List<BusinessThirdPartyReferenceId> getBusinessThirdPartyReferenceIds() {
        return businessThirdPartyReferenceIds;
    }

    @Override
    public void setBusinessThirdPartyReferenceIds(List<BusinessThirdPartyReferenceId> businessThirdPartyReferenceIds) {
        this.businessThirdPartyReferenceIds = businessThirdPartyReferenceIds;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean isPending() {
        return pending;
    }

    @Override
    public void setPending(boolean pending) {
        this.pending = pending;
    }

}
