package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.compliancerating.persistence;

import net.nkg.nitro.edwh.mdm.application.base.BaseEntity;
import net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence.BusinessThirdPartyEntity;
import net.nkg.nitro.edwh.model.compliance.ComplianceRatingType;
import net.nkg.nitro.edwh.model.trade.businesspartner.BusinessThirdParty;
import net.nkg.nitro.edwh.model.trade.businesspartner.ComplianceRating;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(schema = "mdm_spring", name = "compliancerating")
@Audited
public class ComplianceRatingEntity extends BaseEntity<ComplianceRatingEntity> implements ComplianceRating {

    @OneToOne(targetEntity = BusinessThirdPartyEntity.class)
    @JoinColumn(name = "businessthirdparty_id", referencedColumnName = "id")
    private BusinessThirdParty businessThirdParty;

    @NotNull
    @OneToOne(targetEntity = ComplianceRatingTypeEntity.class)
    @JoinColumn(name = "complianceratingtype_id", referencedColumnName = "id")
    private ComplianceRatingType complianceRatingType;

    @Column(name = "dateofrating")
    private LocalDate dateOfRating;

    public BusinessThirdParty getBusinessThirdParty() {
        return businessThirdParty;
    }

    public void setBusinessThirdParty(BusinessThirdParty businessThirdParty) {
        this.businessThirdParty = businessThirdParty;
    }

    @Override
    public ComplianceRatingType getComplianceRatingType() {
        return complianceRatingType;
    }

    @Override
    public void setComplianceRatingType(ComplianceRatingType complianceRatingType) {
        this.complianceRatingType = complianceRatingType;
    }

    @Override
    public LocalDate getDateOfRating() {
        return dateOfRating;
    }

    @Override
    public void setDateOfRating(LocalDate dateOfRating) {
        this.dateOfRating = dateOfRating;
    }
}
