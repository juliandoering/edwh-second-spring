package net.nkg.nitro.edwh.mdm.application.base;

import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ReferenceDataRepository<T extends ReferenceDataEntity<T>> extends BaseRepository<T> {
    Optional<T> findByCode(String code);
}
