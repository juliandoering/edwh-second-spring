package net.nkg.nitro.edwh.mdm.application.masterdata.trade.businesspartner.businessthirdparty.persistence;

import net.nkg.nitro.edwh.mdm.application.base.ReferenceDataRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "thirdpartytypes", path = "thirdpartytypes")
public interface ThirdPartyTypeRepository extends ReferenceDataRepository<ThirdPartyTypeEntity> {
}
