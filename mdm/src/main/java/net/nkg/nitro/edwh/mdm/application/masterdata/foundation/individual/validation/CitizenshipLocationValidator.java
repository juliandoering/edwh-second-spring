package net.nkg.nitro.edwh.mdm.application.masterdata.foundation.individual.validation;

import net.nkg.nitro.edwh.mdm.application.masterdata.foundation.location.persistence.LocationEntity;
import net.nkg.nitro.edwh.mdm.infrastructure.utils.validation.ValidationErrorMessage;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CitizenshipLocationValidator implements ConstraintValidator<CitizenshipLocationConstraint, LocationEntity> {

    @Override
    public void initialize(CitizenshipLocationConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(LocationEntity locationEntity, ConstraintValidatorContext constraintValidatorContext) {
        ValidationErrorMessage.fromContext(constraintValidatorContext).addMessageKey("location.citizenship");
        return locationEntity.getLocationCode() == null;
    }

}
